	<div id="content">
				<div id="register_header" class="content_header">
					<?= $this->lang->line('partner_advertisements')?>
				</div>				
				
				<div><?= $this->lang->line('partner_available_amount')?> <?= $remaining_amount;?><?= $this->lang->line('amount_mark')?> (<?= "impressions: ".$impressions_remaining?>)</div>
				<?php 						
						echo form_open_multipart("statics/save_advert");
					?>
					<? if($advert->external == 1):?>
						<div>
							<label><?= $this->lang->line('advert_link')?></label><input type="textbox" name="advert_link" id="advert_link" value="<?= $advert->link; ?>"/>
						</div>
					<? endif;?>
					<? if($advert->image_fname != ""){?>
						<img class="advert_img_holder" src="<?= site_url('items/uploads/partnerimages/'.$advert->image_fname);?>"></img>
					<? }?>
					<div>
						<label><?= $this->lang->line('advert_image')?></label><input type="file" name="advert_image" id="advert_image" value="<?= $advert->image_fname; ?>"/>
					</div>
					<input type="submit" value="Save"></input>
				<?php echo form_close();?>
				<div>
					<label><?= $this->lang->line('advert_impressions')?></label><label><?= $advert->impressions;?></label>
				</div>
				<div>
					<label><?= $this->lang->line('advert_clicks')?></label><label><?= $advert->clicks;?></label>
				</div>	
				<a href="<?= site_url('statics/logoutPartner');?>"><div><?= $this->lang->line('partner_logout')?></div></a>
	</div>
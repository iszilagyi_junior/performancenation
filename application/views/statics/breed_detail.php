<script>	
	$('#sidemenu').css({top: 0});
</script>
			<div id="content">
				<div class="statics_subheader" style="margin-top:35px;">
					<?= $breed->name;?>
				</div>
				<div id="vet_detail_header">
					<div id="breed_detail_header_cover">
						<img style="width:100%;" src="<?= site_url('items/uploads/coverpictures/' . $breed->cover_picture)?>" border="0">
					</div>
					
				</div>
				<div id="vets_details">
					<div id="statics_left" class="statics_half">
						<div id="vet_details_desc">
							<p class="vets_details_subheader"><?= $this->lang->line('vet_detail_desc')?></p>
							
							<?= $breed->content ?>
						</div>
					</div>
					
					<div id="statics_right" class="statics_half">
					
					<? if($gallery_image_count>0){?>
						<div id="vet_details_gallery">
							<p class="vets_details_subheader"><?= $this->lang->line('vet_detail_gallery')?></p>
							
							<?php $i = 0; ?>
													
						    <?php foreach ($gallery->result() as $image): ?>
						    <div class="vet_details_gallery_item <?php if($i < 3) echo "gallery_item_active"?>" gallery_id="<?=$i++?>" >
						        <a href="<?=site_url("items/uploads/images/" . $image->fname); ?>" data-lightbox="image-1" >
						        	<img src="<?=site_url("items/uploads/images/" . $image->fname); ?>" width="100%" border="0">
						    	</a>
						    </div>
						    
						    <?php endforeach; ?>
				    		<script type="text/javascript">
								var gallery_item_count = <?= $i ?>;
							</script>
							<div class="gallery_rotate_left gallery_rotator">
						    	<
						    </div>
							<div class="gallery_rotate_right gallery_rotator">
						    	>
						    </div>						    							
						</div>
					</div>	
					<? }?>				
				</div>
			</div>
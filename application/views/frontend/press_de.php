<script>
	$(document).ready(function()
	{	
		$("#welcomeImg, #welcomeImgHolder").show();
		
	});
</script>
<div id="content">
    <div id="WelcomeHolder">
		<div id="BkgEr" style="display:none;"></div>
		<div id="welcomeText">Presse</div>
	</div>

	<div class="home_italic">	
			Laden Sie unsere Aussendungen, Bilder und Logos hier herunter:
	</div>
	<div style="width:400px;text-align:center;margin:10px auto;">
		<a target="_blank" href="<?= site_url('items/uploads/press/PresseAussendung.pdf')?>">Presseaussendung, Deutsch (PDF 1MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/PresseAussendung_EN.pdf')?>">Presseaussendung, Englisch (PDF 1MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/snoopstr_logo.eps')?>">Logo, (EPS 1 MB)</a><br/>
		<br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC00492_branded.jpg')?>">Testimonial Foto Henry, HD, branded (15 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC00492.jpg')?>">Testimonial Foto Henry, HD, unbranded (15 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC00497_branded.jpg')?>">Testimonial Foto Boy, HD, branded (15 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC00497.jpg')?>">Testimonial Foto Boy, HD, unbranded (15 MB)</a><br/>		
		<a target="_blank" href="<?= site_url('items/uploads/press/iMacScreenshot_de.png')?>">Screenshot des deutschen User Interfaces in Print-Qualität (1 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/Portrait_Istvan.jpg')?>">Gründer von snoopstr (12 MB)</a><br/>
		<br/>
		Bilder vom Gründer und seinen Hunden:<br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC02444.jpg')?>">Foto 1 (12 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC02457.jpg')?>">Foto 2 (12 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC02479.jpg')?>">Foto 3 (13 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC02493.jpg')?>">Foto 4 (14 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC02504.jpg')?>">Foto 5 (13 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC02548.jpg')?>">Foto 6 (14 MB)</a><br/>
	</div>
</div>

	
</div>
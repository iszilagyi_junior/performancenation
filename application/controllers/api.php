<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends MY_Controller {

    function __construct()
    {
        parent::__construct();

		$this->config->load('dog');
		$this->load->model('statics_model');
		$this->load->model('car_model');
		$this->load->model('auth_model');
		$this->load->library('upload');
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
    }  	 
	

	public function index()
	{
		 
		$this->checkIPaddress();
		$this->load->model('statics_model');
		$this->load->model('car_model');
		$user_id = $this->uri->segment(2);
		$cars = $this->car_model->getVehicles($user_id);
		
		
		$session_response = "INVALID";
		if(!empty($_POST['session_id'])){
			
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
			$session_data = $this->auth_model->sessionData($session_id);
		}
		
			
		
		$friends_container = array();
		foreach($cars->result() as $car){
			$friends_container[] = $car->id;
			$friends = $this->car_model->getFriends($car->id);
			foreach($friends->result() as $i=>$friend){				
				if (!in_array($friend->id, $friends_container)) {
			   	 	$friends_container[] = $friend->id;
			    }
			}
	
		}	
		
		shuffle($friends_container);
		$whatsnew_container = array();
		$whatsnew_container['response']['session'] = array("id" => $user_id,
											"status" => $session_response,
											"selected_vehicle" => $session_data->vehicle_id);
											
	
		$event_ids = array();
		$events = $this->statics_model->getAllEvents()->result();
		
		if($session_response == "OK")
		{
		
			if(!empty($friends_container)){
		
			foreach($events as $event){		
				//$result = $this->statics_model->getEvents($friend_id);
				if(in_array($event->vehicle_id, $friends_container))
				{
					$event_item = array();
					$vehicle = $this->car_model->getVehicle($event->vehicle_id)->row();
					$carname = $vehicle->nickname;
					
					$car_cover = $this->statics_model->getImageByID($vehicle->cover_image)->row()->fname;
					
					$vehicleprofile = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
					
					
					
					$event_action = $event->action_id;
					$event_entity =$event->entity_id;
					$event_date = $event->created_date;
					$event_likes = $event->likes;
					$image_title = "";
					$event_image_hq_url = "";
					switch($event_action){
						case 1:{ //nickname
						/*	$friend_dog = $this->car_model->getDog($event->entity_id);
							$pretty = $friend_dog->pretty_url;
							$text = $this->lang->line('event_added_friend');
							$event_text = $this->lang->line('event_new_friend');
							$entity_content ="";
							$entity_link = "profile/".$pretty;
							$entity_name = $this->car_model->getDog($event_entity)->name;
							$event_picture = "";
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "nickname_change";
							*/
						};break;
						case 2:{ //story piece
							
							
							$text = " added new story piece ";
							$event_text = "new Story";
							$entity_content = $this->car_model->getStoryPieceById($event->additional_id)->row()->story;
							$entity_link = "";
							//$entity_name = $this->car_model->getDog($event_entity)->name;
							$event_picture = "";
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "story_piece";
						};break;
						case 3:{ //new vehicle profile
							
							$usr = $this->statics_model->getUserData($vehicle->user_id)->row();
							$text = "new vehicle by ";
							$event_text = "";
							$entity_content ="";						
							$entity_link = "profile/".$usr->username;
							$entity_name = $usr->first_name." ".$usr->last_name;							
							$event_picture = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "new_vehicle";
						};break;
						case 4:{ //new modification
						
					
							switch($vehicle_type)
							{
								case 0:{ $table = "car_mod_history";}break;
								case 1:{ $table = "motorcycle_mod_history";}break;
								case 2:{ $table = "truck_mod_history";}break;
							}
							
							
							$mod_history = $this->car_model->getMod($table,$event->additional_id);
							$t = preg_replace('/_+/', ' ', $mod_history->mod_text);
							$text = " added new modification: ". ucwords($t);
							$event_text = "new Mod";
							$entity_content = $mod_history->mod;
							$entity_link = "";
							$entity_name = "";
							//$entity_name = $this->car_model->getDog($event_entity)->name;
							$event_picture = site_url()."items/frontend/img/mods/".$mod_history->mod_text.".jpg";
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "new_modification";
						};break;
						
						case 5:{ //created event
							$text = " created event";
							$event_text = "Created event";
							$entity_content ="";
							
							$entity_link = "user_event/".$event_entity;
							$entity_name = $this->statics_model->getUserEventById($event_entity)->title;
							$event_picture = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "event_created";
							
						};break;
						case 6:{ //changed profile picture
							
							$vehicleprofile = $this->statics_model->getImageByID($event->additional_id)->row()->fname;

							$image_id = $event->additional_id;
							$text = "Changed profile picture";
							$event_text = "New profile picture";
							$entity_content ="";
							$entity_link = "";
							$entity_name = "";
						//	$event_likes = $this->statics_model->getPicture($event->additional_id)->row()->likes;
							$event_picture = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "new_profile";
						};break;
						case 7:{ //changed cover picture
							$vehicle_cover = $this->statics_model->getImageByID($event->additional_id)->row()->fname;
							
							$image_id = $event->additional_id;
							$text = "Changed cover picture";
							$event_text = "New cover picture";
							$entity_content ="";
							$entity_link = "";
							$entity_name = "";
						//	$event_likes = $this->statics_model->getPicture($event->additional_id)->row()->likes;
							$event_picture = site_url()."items/uploads/coverpictures/".$vehicle_cover;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "new_cover";
						};break;
						case 8:{ //added a picture
							$text = "uploaded image";
							$event_text = "";
							$entity_content ="";
							$album_id = $this->statics_model->getAlbumFromPicture($event_entity)->row()->album_id;
							
							$entity_link = "album/".$album_id;
							$album = $this->statics_model->getSingleAlbum($album_id);
							$entity_name = ""; //$album->title;							
							$picture = $this->statics_model->getPicturebyID($event_entity)->row()->fname;
							$image_id = $this->statics_model->getPicturebyID($event_entity)->row()->id;
							//$event_likes = $this->statics_model->getPicture($image_id)->row()->likes;
							$image_title = $this->statics_model->getAlbumFromPicture($event_entity)->row()->title;
							$event_picture = site_url()."items/uploads/images/".$picture;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "new_photo";		
						};break;
						case 9:{ //tagged on photo (this should go to notifications)
							$text = $this->lang->line('event_tagged_on_photo');
							$event_text = $this->lang->line('event_tagged');
					  		$entity_content ="";														
							$entity_link = "";							
							$entity_name = "";
							$picture = $this->statics_model->getPicture($event_entity)->row()->fname;	
							$image_id = $this->statics_model->getPicture($event_entity)->row()->id;									
							$event_picture = site_url()."items/uploads/images/".$picture;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "tagged";		
						};break;
						case 10:{ // started following xy...
							$vh = $this->car_model->getVehicle($event->additional_id)->row();
							$vh_image = $this->statics_model->getImageByID($vh->profile_image)->row()->fname;
							$veh = $this->car_model->getVehicle($event->entity_id)->row();
							$carname = $veh->nickname;
							$vehicleprofile = $this->statics_model->getImageByID($veh->profile_image)->row()->fname;
							$text = " is following";
							$event_text = "Following";
							$entity_content = "";
							$entity_link = "vehicle_profile/".$vh->pretty_url;
							$entity_name = $vh->nickname;
							$event_picture = site_url()."items/uploads/profilepictures/".$vh_image;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "following";		
						};break;
						case 11:{ // checked in at POI NEED CHANGE
							$text = " checked in at ";
							$event_text = "Checked in";
							$entity_content = "";
							$entity_link = "vets/".$event_entity;
							$entity_name = $this->statics_model->getPOI($event_entity)->name;
							$event_picture = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "checked_in_POI";		
						};break;
						case 12:{ // rated POI
							$text = " rated";
							$event_text = "rated POI";
							$entity_content = "";
							$entity_link = "zones/".$event_entity;
							$entity_name = $this->statics_model->getPOI($event_entity)->name;
							$event_picture = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "rated_POI";	
						};break; 
						case 13:{ //Liked picture (remove?)
							$image = $this->statics_model->getImageByID($event_entity)->row();
							
							$type = "images";
							
							if($image->cover_image == 1)
							{
								$type = "coverpictures";
							}
							else if($image->profile_image == 1)
							{
								$type = "profilepictures";
							}
							$text = $this->lang->line('event_liked_a');
							$event_text = $this->lang->line('event_liked_picture');
							$entity_content = "";
							$entity_link = "photo/".$event_entity;
							$entity_name = "picture";
							//$event_picture = site_url()."items/uploads/".$type."/".$image->fname;
							$event_picture = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "picture_like";	
						};break;
						case 14:{ //Commented (remove/move to notification?)
							$image = $this->statics_model->getImageByID($event_entity)->row();
							
							$type = "images";
							
							if($image->cover_image == 1)
							{
								$type = "coverpictures";
							}
							else if($image->profile_image == 1)
							{
								$type = "profilepictures";
							}
							$text = $this->lang->line('event_commented_on_a');
							$event_text = $this->lang->line('event_commented_picture');
							$entity_content = "";
							$entity_link = "photo/".$event_entity;
							$entity_name = "picture";
							//$event_picture = site_url()."items/uploads/".$type."/".$image->fname;
							$event_picture = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "comment";	
						};break;
						case 15:{ //traffic alert
							
							$alert = $this->statics_model->getAlert($event->entity_id)->row()->the_alert;
							
							$text = "posted a traffic alert";
							$event_text = "";
							$entity_content = $alert;
							$entity_link = "";
							$entity_name = "";					
							$event_picture = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "traffic_alert";	
						};break;
						case 16:{ //update status
							
							$alert = $this->statics_model->getStatus($event->entity_id)->row()->status;
							
							$text = "updated status";
							$event_text = "";
							$entity_content = $alert;
							$entity_link = "";
							$entity_name = "";					
							$event_picture = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "status_update";	
						};break;
						
					}
					
					
						$comments_holder = array();
						$event_comments = $this->statics_model->getEventComments($event->id);
						
						
						
						foreach($event_comments as $comment)
						{
							$vehicle = $this->car_model->getVehicle($comment->vehicle_id)->row();
		
							$profile = $this->statics_model->getImageByID($vehicle->profile_image)->row();
							
							$new = array('comment' => $comment->comment,
											'nickname' => $vehicle->nickname,
											'profile_image' => $profile->fname);
											
										
										
							$comments_holder[] = $new;
						}
					
					
					
					
					
					
					$event_item = array(
												'id' => $event->id,
												'entity_link' => $entity_link,
												'text' => $text,
												'detailed_text_content' => $entity_content, 
												'label' => $label,
												'vehiclename' => $carname,
												'entity_name' => $entity_name,
												'vehicle_id' => $event->vehicle_id,
												'profile' => $the_image,
												'event_image' => $event_picture,
												'event_image_hq_url' => $event_image_hq_url,
												'image_title' => $image_title,
												'created_date' => $event_date,
												'event_type' => $event_type,
												'comments' => count($comments_holder),
												'likes' => strval($event_likes));
					
												
					if(!in_array($event->id, $event_ids) && ($event_type != "nickname_change" && $event_type != "picture_like" && $event_type != "comment")){
					
						$whatsnew_container['response']['events'][] = $event_item;
						$event_ids[] = $event->id;
					}
						
					
				
				}
			}
		}
		}
		else
		{
			
			$event_item = array(
										'id' => "00",
										'entity_link' => "-",
										'text' => "-",
										'detailed_text_content' => "-", 
										'label' => "-",
										'vehiclename' => "-",
										'entity_name' => "-",
										'vehicle_id' => "-",
										'profile' => "-",
										'event_image' => "-",
										'event_image_hq_url' => "-",
										'image_title' => "-",
										'created_date' => "-",
										'event_type' => "-",
										'comment' => 0,
										'likes' => 0);
			
										
				$whatsnew_container['response']['events'][] = $event_item;
				
			
		}
		

		$json_data = json_encode($whatsnew_container, JSON_PRETTY_PRINT);
		
		
		echo $json_data;
	}
	
	
	public function get_event_comments()
	{
		
		
		$user_id = $_POST['user_id'];
		$event_id = $_POST['event_id'];
		
		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$friend_container['response']['session'] = array('id' => $user_id,
														"status" => $session_response);
		$friend_container['response']['comments'] = array();												
		$comments_holder = array();	
		if($session_response == "OK")
		{
			
			$event_comments = $this->statics_model->getEventComments($event_id);
			
			foreach($event_comments as $comment)
			{
				$vehicle = $this->car_model->getVehicle($comment->vehicle_id)->row();

				$profile = $this->statics_model->getImageByID($vehicle->profile_image)->row();
				
				$new = array('comment' => $comment->comment,
							 'name' => $vehicle->nickname,
							 'date' => $comment->comment_date,	
							 'profile_image' => site_url()."items/uploads/profilepictures/".$profile->fname);
								
							
							
				$friend_container['response']['comments'][] = $new;
			}
		}
		else
		{
			$new = array('comment' => '-',
						 'name' => '-',
						 'date' => '-',	
						 'profile_image' => '-');
							
						
						
			$friend_container['response']['comments'][] = $new;
		}
				
		$json_data = json_encode($friend_container, JSON_PRETTY_PRINT);
		echo $json_data;	
		
	}
	
	
	
	
	public function send_event_comment()
	{
	
	/** TEST DATA **/	
	/*	$_POST['user_id'] = 98;
		$_POST['vehicle_id'] = 5;
		$_POST['event_id'] = 361;
		$_POST['comment'] = 'testing';
	*/	
		
		
		$user_id = $_POST['user_id'];
		$vehicle_id = $_POST['vehicle_id'];
		$event_id = $_POST['event_id'];
		$comment = $_POST['comment'];
		
		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array('id' => $user_id,
														"status" => $session_response);
		$container['response']['comments'] = array();												
		
		if($session_response == "OK")
		{
			$event = $this->statics_model->getNewsEventById($event_id);
			
			
			
			
			if($event)
			{
				$data = array('event_id' => $event_id,
						'vehicle_id' => $vehicle_id,
						'comment' => $comment);
		
		
				$this->statics_model->addNewsComment($data);
				
				
				if($event->action_id == 6 || $event->action_id == 7 || $event->action_id == 8 || $event->action_id == 15 || $event->action_id == 16)
				{
					$pic_id = $event->additional_id;
					
					$data = array('image_id' => $pic_id,
								  'vehicle_id' => $vehicle_id,
								  'comment' => $comment);
					
					$this->statics_model->addPhotoComment($data);
				}
			}
			
			
			
			$new = array('status' => 'OK');			
			$container['response']['comments'][] = $new;
		
		}
		else
		{
			$new = array('status' => 'ERROR');
							
						
						
			$container['response']['comments'][] = $new;
		}
				
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
		
			
	}
	
	
	
	public function send_image_comment()
	{
	

		$user_id = $_POST['user_id'];
		$vehicle_id = $_POST['vehicle_id'];
		$image_id = $_POST['image_id'];
		$comment = $_POST['comment'];
		
		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array('id' => $user_id,
														"status" => $session_response);
		$container['response']['comments'] = array();												
		
		if($session_response == "OK")
		{
			
			
			$data = array('image_id' => $image_id,
						  'vehicle_id' => $vehicle_id,
						  'comment' => $comment);
			
			$this->statics_model->addPhotoComment($data);
			
			
			$event = $this->statics_model->getNewsEventByPicId($image_id);
			
			if($event)
			{
	
				$event_id = $event->id;
				
				$data = array('event_id' => $event_id,
					'vehicle_id' => $vehicle_id,
					'comment' => $comment);
	
	
				$this->statics_model->addNewsComment($data);
	
			}
			

					
			$container['response']['status'] = 'OK';
		
		}
		else
		{
			
							
						
						
			$container['response']['status'] = 'ERROR';
		}
				
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
		
			
	}
	
	
	
	
	
	public function getAdvertisement($user_id)
    {
	 /*   $partners = $this->statics_model->getPartners()->result();
	   
		foreach($partners as $partner)
		{
			$partner_id = $partner->id;
		}
		
	    $advertisement = $this->statics_model->getAdvertisement($partner_id)->row();
	    
		$this->statics_model->updateImpressions($partner_id);
		
		return $advertisement;*/
		
		
		$advertisement = array();
		$partners = $this->statics_model->getPartners()->result();
		
		$user = $this->statics_model->getUserData($user_id)->row();
		
		$user_country = $user->country;
		$user_state = $user->state;
		if($user_country === NULL || $user_country == "")
		{
		
			if($user->geo_location !== NULL && $user->geo_location != "")
			{
				
				$geocode=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$user->geo_location.'&sensor=false');

		        $output= json_decode($geocode);
				
				for($j=0;$j<count($output->results[0]->address_components);$j++)
				{
						if($output->results[0]->address_components[$j]->types[0] == "country")
						{
							
							$user_country = $output->results[0]->address_components[$j]->short_name;
							$data = array('id' => $user_id,
									'country' => $user_country);
							$this->statics_model->updateUser($data);
						}
						
		                if($output->results[0]->address_components[$j]->types[0] == "administrative_area_level_1")
						{
							$user_state = $output->results[0]->address_components[$j]->short_name;
							$data = array('id' => $user_id,
										'state' => $user_state);
							$this->statics_model->updateUser($data);
						}
						
									
		        }
 
			}
		}
		
				
		
		
		if($user_country !== null && $user_country !== "")
		{
			
			foreach($partners as $partner)
			{
				if($user_country != "US" && $user_country == $partner->country)
				{
					
					$partner_id = $partner->id;
				}
				else
				{
					if($user_country == "US" && $partner->country == "US")
					{
					
						if($user_state == $partner->state)
						{
						
							$partner_id = $partner->id;
						}
					}
				}
				
			}
			
		    $saved_advertisement = $this->statics_model->getAdvertisement($partner_id)->row();
			
			if($saved_advertisement->accepted == 1)
			{
				 $advertisement = $saved_advertisement;
			}

			$this->statics_model->updateImpressions($partner_id);
		    
		}		
		return $advertisement;
    }
	
	
	
	public function edit_user_profile($id){
		
		$user_container = array();
		
		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
			$session_data = $this->auth_model->sessionData($session_id);
		}
		
		$user_container['response']['session'] = array('id' =>$id,
												"status" => $session_response,
												"selected_vehicle" => $session_data->vehicle_id);
		
		if($session_response == "OK")
		{
			$data['profiledata'] = $this->auth_model->getProfile($id);
		/*	$birthday = $data['profiledata']->birthday;
			$arr = explode("-",$birthday);
			$data['year'] = $arr[2];
			$data['month'] = $arr[1];
			$data['day'] = $arr[0];
		*/	
			$user_item = array('id' =>$id,
									'firstname' => $data['profiledata']->first_name,
									'lastname' => $data['profiledata']->last_name,
									'geo_location' => $data['profiledata']->geo_location,							
									'country' => $data['profiledata']->country,
									'state' => $data['profiledata']->state,							
									'city' => $data['profiledata']->city,
									'zip' => $data['profiledata']->zip,
									'address' => $data['profiledata']->address,
									'profile_picture' => site_url()."items/uploads/profilepictures/".$data['profiledata']->profile_picture,
									'show_hashtag' => $data['profiledata']->show_hashtags,								
									'phone' => $data['profiledata']->phone,
									'fb_link' => $data['profiledata']->fb_link,
									'tw_link' => $data['profiledata']->tw_link,
									'insta_link' => $data['profiledata']->insta_link,
									'skype' => $data['profiledata']->skype,
									'show_email' => $data['profiledata']->email_show,
										);

		}
		else
		{
			$user_item = array('id' => $id,
									'firstname' => "-",
									'lastname' => "-",
									'geo_location' => "-",							
									'country' => "-",
									'state' => "-",							
									'city' => "-",
									'zip' => "-",
									'address' => "-",
									'profile_picture' => "-",
									'show_hashtag' => "-",
									'skype' => "-",
									'phone' => "-",
									'fb_link' => "-",
									'tw_link' => "-",
									'insta_link' => "-",
									'show_email' => "-"
										);
		}
		$user_container['response']['user'][] = $user_item;
		
		$json_data = json_encode($user_container, JSON_PRETTY_PRINT);
		echo $json_data;
	}
	
	public function save_vehicle_to_session(){
		$vehicle_id = $_POST['vehicle_id'];
		$session_response = "INVALID";
		$vehicle_container = array();

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
			
			$this->auth_model->updateSessionVehicle($session_id, $vehicle_id);
		
		}
		
		$vehicle_container['response']['session'] = array('id' =>$vehicle_id,
												"status" => $session_response);
		
		
		$json_data = json_encode($vehicle_container, JSON_PRETTY_PRINT);
		echo $json_data;
	}
	
	public function set_user_profile()
	{
		$container = array();
		$complete = false;
		if(isset($_POST['user_id']))
		{
			$id = $_POST['user_id'];		
			$user = $this->ion_auth->user($id)->row();					
			
			$data = array(
				'first_name' => $_POST['firstname'],
				'last_name'  => $_POST['lastname'],
				'show_hashtags'  => $_POST['hashtag'],
				'country'  => $_POST['country'],
				'state'  => $_POST['state'],
				'zip'  => $_POST['zip'],
				'city'  => $_POST['city'],
				'address'  => $_POST['address'],
				'geo_location'  => $_POST['geolocation'],
				'phone'  => $_POST['phone'],
				'fb_link' => $_POST['fb_link'],
				'tw_link' => $_POST['tw_link'],
				'insta_link' => $_POST['insta_link'],
				'skype' => $_POST['skype'],
				'email_show' => $_POST['show_email'],	
			);
			
			$session_response = "INVALID";
			
			if(!empty($_POST['session_id'])){
				$session_id = $_POST['session_id'];
				$session_response = $this->checkSession($session_id);
				$session_data = $this->auth_model->sessionData($session_id);
			}
			
			$container['response']['session'] = array( "id" => $id,
												"status" => $session_response,
												"selected_vehicle" => $session_data->vehicle_id);
			
			if($session_response == "OK")
			{
				$complete = $this->ion_auth->update($user->id, $data);
			}
			
			
			if($complete){
				$container['response']['response'][] = array("id" => $id,
													"status" => "OK");
			}
			else{
				$container['response']['response'][] = array("id" => $id,
													"status" => "FAIL");
			}
			
			$json_data = json_encode($container, JSON_PRETTY_PRINT);
			echo $json_data;
		}
	}
	

	public function toAscii($str, $replace=array(), $delimiter='_') {
		 if( !empty($replace) ) {
		  $str = str_replace((array)$replace, ' ', $str);
		 }
		
		 $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		 $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		 $clean = strtolower(trim($clean, '-'));
		 $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
		
		 return $clean;
	}
	

	public function my_vehicles(){
		
		
		if(isset($_POST['user_id']))
		{
			$id = $_POST['user_id'];
		
			$car_container = array();
			

			$session_response = "INVALID";
			
			if(!empty($_POST['session_id'])){
				$session_id = $_POST['session_id'];
				$session_response = $this->checkSession($session_id);
				$session_data = $this->auth_model->sessionData($session_id);
			}
			
			
			
			$car_container['response']['session'] = array("id" => $id,
													"status" => $session_response,
													"selected_vehicle" => $session_data->vehicle_id);
			
			if($session_response == "OK")
			{	
				$cars = $this->car_model->getVehicles($id)->result();
				
				foreach($cars as $car)
				{
					
					$carprofile = $this->statics_model->getImageByID($car->profile_image)->row()->fname;
					
					$car_item = array(
						'id' => $car->id,
						'nickname'  => $car->nickname,
						'profile_picture'  => site_url()."items/uploads/profilepictures/".$carprofile);
						
					$car_container['response']['vehicles'][] = $car_item;
				}
			}
			else
			{
				$car_item = array(
						'id' => "00",
						'nickname'  => "-",
						'profile_picture'  => "-");
						
				$car_container['response']['vehicles'][] = $car_item;
			}
			
	
			
			
		}
		else
		{
			if(!empty($_POST['session_id'])){
				$session_id = $_POST['session_id'];
				$session_response = $this->checkSession($session_id);
			}
			
			$car_container['response']['session'] = array("id" => "00",
													"status" => $session_response,
													"selected_vehicle" => "0");
			
			$car_item = array('id' => "00",
							'nickname'  => "-",
							'profile_picture'  => "-");
						
			$car_container['response']['vehicles'][] = $car_item;
		}
		
		$json_data = json_encode($car_container, JSON_PRETTY_PRINT);
		echo $json_data;
	}
	
	public function vehicle_detail($car_id)
	{
			
			
			
		$car_container = array();
		$session_response = "INVALID";
		$user_id = NULL;
		
		
		
		if(!empty($_POST['user_id'])){
			$user_id = $_POST['user_id'];			
		}
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
			$session_data = $this->auth_model->sessionData($session_id);
		}
		
		$car_container['response']['session'] = array("id" =>$car_id,
													"status" => $session_response,
													"selected_vehicle" => $session_data->vehicle_id);
		
		
		
		if($session_response == "OK")
		{
			$car = $this->car_model->getVehicle($car_id)->row();	
			$owner = $this->statics_model->getUserData($car->user_id)->row();
			$followers = $this->car_model->getFriendsTotal($car_id);
			$car_item = array();
			
			
			$editable = 0;
			if($user_id != NULL)
			{
				
				if($owner->id == $user_id){
					$editable = 1;
				}
			}
			
			$show_unfollow = 0;
			
			
			if($this->car_model->checkVehicle($car_id,$session_data->vehicle_id))
			{
				$show_unfollow = 1;
			}
			
			
						
			$car_about = $car->description;
			
			if($car->description == NULL)
			{
				$car_about = "...";
			}
			
			$carprofile = $this->statics_model->getImageByID($car->profile_image)->row()->fname;
			$carcover = $this->statics_model->getImageByID($car->cover_image)->row()->fname;
			

			$show_remove = 0;
			$cars = $this->car_model->getVehicles($user_id)->result();
			$vehicle_type = "";
			switch($car->vehicle_type)
			{
				case 0:{ $model = $this->car_model->getCarModelById($car->model_id)->row();
						 $make_model = $model->year." ".$model->make." ".$model->model." ".$model->model_trim;
					 	 $vehicle_type = "car";
				};break;
				case 1:{ $model = $this->car_model->getMotorModelById($car->model_id)->row();
						 $make_model = $model->year." ".$model->make." ".$model->model;
						 $vehicle_type = "motor";
				};break;
				case 2:{ $model = $this->car_model->getTruckModelById($car->model_id)->row();
						 $make_model = $model->year." ".$model->make." ".$model->model." ".$model->model_trim;
						 $vehicle_type = "truck";
				};break;
			}
			
			$the_mods = array();
			$mods = array();
			$vehicle = $car;
			switch($vehicle->vehicle_type)
			{
				case 0:{
							$mods = $this->car_model->getVehicleMod($vehicle->engine_type."_mods",$car_id);
							
							foreach($mods as $mod)
							{
								
								switch($mod->category)
								{
									case "audio": { $mod_type = $this->car_model->getMod("audio_types", $mod->type);};break;
									case "air_intake": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type); };break;
									case "body": { $mod_type = $this->car_model->getMod("body_types", $mod->type);};break;
									case "brake": { $mod_type = $this->car_model->getMod("brake_types", $mod->type);};break;
									case "chassis": { $mod_type = $this->car_model->getMod("chassis_types", $mod->type);};break;
									case "cooling": { $mod_type = $this->car_model->getMod("gas_coolingsystem_types", $mod->type);};break;
									case "drive_axle": { $mod_type = $this->car_model->getMod("drive_axle_types", $mod->type);};break;
									case "engine": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type);};break;
									case "exhaust": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type); };break;
									case "fuel": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."system_types", $mod->type); };break;
									case "fuellift_pump": { $mod_type = $this->car_model->getMod("diesel_fuel_lift_types", $mod->type); };break;
									case "injection_pump": { $mod_type = $this->car_model->getMod("diesel_injection_pump_types", $mod->type); };break;
									case "injector": { $mod_type = $this->car_model->getMod("diesel_injection_types", $mod->type); };break;
									case "intercooler": { $mod_type = $this->car_model->getMod("diesel_intercooler_types", $mod->type); };break;
									case "ignition": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."system_types", $mod->type); };break;
									case "lighting": { $mod_type = $this->car_model->getMod("lighting_types", $mod->type);};break;
									case "suspension": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type); };break;
									case "transmission": { $mod_type = $this->car_model->getMod("transmission_types", $mod->type);};break;
									case "tuned": { $mod_type = $this->car_model->getMod("tuned_types", $mod->type);};break;
									case "turbo": { $mod_type = $this->car_model->getMod("diesel_turbo_types", $mod->type);};break;
									case "wheels": { $mod_type = $this->car_model->getMod("wheel_types", $mod->type);};break;
									case "other": { $mod_type = $this->car_model->getMod("wheel_types", $mod->type);};break;
								}
								
								$mod_name = "";
								if($mod_type->name != 'Stock' && $mod_type->name != 'Aftermarket')
								{
									$mod_name = $mod_type->name;
								}
								
								
								$modtitle = str_replace("_", " ", $mod->category);
								$modtitle = ucfirst ($modtitle);
								
								$mod_item = array('category' => $modtitle,
													'text' => $mod_name." ".$mod->text,
													'image' => site_url('items/frontend/img/mods')."/".$mod->category.".png");
	
								$the_mods[] = $mod_item;
							}
	
					};break;
				case 1:{
						
							$mods = $this->car_model->getVehicleMod("motorcycle_mods",$car_id);
							
							
							foreach($mods as $mod)
							{
								switch($mod->category)
								{
									case "audio": { $mod_type = $this->car_model->getMod("audio_types", $mod->type); };break;
									case "body": { $mod_type = $this->car_model->getMod("motorcycle_body_types", $mod->type);};break;
									case "brake": { $mod_type = $this->car_model->getMod("motorcycle_brake_types", $mod->type);};break;
									case "chassis": { $mod_type = $this->car_model->getMod("chassis_types", $mod->type);};break;
									case "cooling": { $mod_type = $this->car_model->getMod("gas_coolingsystem_types", $mod->type);};break;
									case "drive_axle": { $mod_type = $this->car_model->getMod("drive_axle_types", $mod->type);};break;
									case "engine": { $mod_type = $this->car_model->getMod("motorcycle_engine_types", $mod->type); };break;
									case "exhaust": { $mod_type = $this->car_model->getMod("motorcycle_exhaust_types", $mod->type); };break;
									case "lighting": { $mod_type = $this->car_model->getMod("motorcycle_lighting_types", $mod->type); };break;
									case "suspension": { $mod_type = $this->car_model->getMod("motorcycle_suspension_types", $mod->type); };break;
									case "transmission": { $mod_type = $this->car_model->getMod("motorcycle_transmission_types", $mod->type); };break;
									case "wheel": { $mod_type = $this->car_model->getMod("motorcycle_wheel_types", $mod->type); };break;
									
								}
								
								$mod_name = "";
								if($mod_type->name != 'Stock' && $mod_type->name != 'Aftermarket')
								{
									$mod_name = $mod_type->name;
								}
								
								
								$modtitle = str_replace("_", " ", $mod->category);
								$modtitle = ucfirst ($modtitle);
								
								$mod_item = array('category' => $modtitle,
													'text' => $mod_name." ".$mod->text,
													'image' => site_url('items/frontend/img/mods')."/".$mod->category.".png");
	
								$the_mods[] = $mod_item;
							}
						};break;
				case 2:{
						 	
							$mods = $this->car_model->getVehicleMod($vehicle->engine_type."_mods",$car_id);
							
							
							foreach($mods as $mod)
							{
								switch($mod->category)
								{
									case "audio": { $mod_type = $this->car_model->getMod("audio_types", $mod->type);};break;
									case "air_intake": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type); };break;
									case "body": { $mod_type = $this->car_model->getMod("body_types", $mod->type);};break;
									case "brake": { $mod_type = $this->car_model->getMod("brake_types", $mod->type);};break;
									case "chassis": { $mod_type = $this->car_model->getMod("chassis_types", $mod->type);};break;
									case "cooling": { $mod_type = $this->car_model->getMod("gas_coolingsystem_types", $mod->type);};break;
									case "drive_axle": { $mod_type = $this->car_model->getMod("drive_axle_types", $mod->type);};break;
									case "engine": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type);};break;
									case "exhaust": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type); };break;
									case "fuel": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."system_types", $mod->type); };break;
									case "fuellift_pump": { $mod_type = $this->car_model->getMod("diesel_fuel_lift_types", $mod->type); };break;
									case "injection_pump": { $mod_type = $this->car_model->getMod("diesel_injection_pump_types", $mod->type); };break;
									case "injector": { $mod_type = $this->car_model->getMod("diesel_injection_types", $mod->type); };break;
									case "intercooler": { $mod_type = $this->car_model->getMod("diesel_intercooler_types", $mod->type); };break;
									case "ignition": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."system_types", $mod->type); };break;
									case "lighting": { $mod_type = $this->car_model->getMod("lighting_types", $mod->type);};break;
									case "suspension": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type); };break;
									case "transmission": { $mod_type = $this->car_model->getMod("transmission_types", $mod->type);};break;
									case "tuned": { $mod_type = $this->car_model->getMod("tuned_types", $mod->type);};break;
									case "turbo": { $mod_type = $this->car_model->getMod("diesel_turbo_types", $mod->type);};break;
									case "wheels": { $mod_type = $this->car_model->getMod("wheel_types", $mod->type);};break;
									case "other": { $mod_type = $this->car_model->getMod("wheel_types", $mod->type);};break;
								}
								
								$mod_name = "";
								if($mod_type->name != 'Stock' && $mod_type->name != 'Aftermarket')
								{
									$mod_name = $mod_type->name;
								}
								
								$modtitle = str_replace("_", " ", $mod->category);
								$modtitle = ucfirst ($modtitle);
								
								$mod_item = array('category' => $modtitle,
													'text' => $mod_name." ".$mod->text,
													'image' => site_url('items/frontend/img/mods')."/".$mod->category.".png");
	
								$the_mods[] = $mod_item;
							
							}				 
						};break;
			}
	
			
			
			function compareByName($a, $b) {
			  return strcmp($a["category"], $b["category"]);
			}
			usort($the_mods, 'compareByName');
			
			
			switch($car->hp_ranges)
			{
				case 50: { $hp_ranges = "50-150 HP";};break;
				case 151: { $hp_ranges = "151-250 HP";};break;
				case 251: { $hp_ranges = "251-400 HP";};break;
				case 401: { $hp_ranges = "401-700 HP";};break;
				case 701: { $hp_ranges = "701-1000 HP";};break;
				case 1001: { $hp_ranges = ">1000 HP";};break;
				default: {$hp_ranges = "";};break;
			}
			
			
			$story_holder = array();
			$stories = $this->car_model->getStoryPieces($car_id);
		
			foreach($stories as $story)
			{
				$gallery_images = $this->statics_model->getStoryImages($story['id']);
				$gallery_holder = array();
				
				
				foreach($gallery_images as $img)
				{
					
					
					$gallery_holder[] = site_url('items/uploads/images/'.$img['fname']);
				}
				
				
				$story_item = array('id' => $story['id'],
									'created_date' => $this->set_user_timezone($story['created_date']),
									'story' => $story['story'],
									'gallery' => $gallery_holder);
				$story_holder[] = $story_item;
			}

			$car_item = array('id' => $car->id,
									'owner' => $owner->first_name." ".$owner->last_name,
									'owner_id' => $owner->id,
									'nickname' => $car->nickname,
									'label' => $car->label,
									'profile_picture' => site_url()."items/uploads/profilepictures/".$carprofile,
									'cover_picture' => site_url()."items/uploads/coverpictures/".$carcover,
									'description' => $car_about,
									'plate' => $car->plate,
									'zerosixty' => $car->zerosixty,
									'hp_ranges' => $hp_ranges,
									'followers_total' => $followers,
									'make_model' => $make_model,
									'editable' => $editable,
									'vehicle_type' => $vehicle_type,
									'show_unfollow' => $show_unfollow,
									'engine_type' => $car->engine_type,
									'mods' => $the_mods,
									'blog_entries' => $story_holder); 
		}
		else
		{
			
			$car_item = array('id' => "00",
									'owner' => "-",
									'owner_id' => "-",
									'nickname' => "-",
									'label' => "-",
									'profile_picture' => "-",
									'cover_picture' => "-",
									'description' => "-",
									'plate' => "-",
									'zerosixty' => "-",
									'hp_ranges' => "-",
									'followers_total' => "-",
									'make_model' => "-",
									'editable' => "-",
									'vehicle_type' => "-",
									'show_unfollow' => "-",
									'engine_type' => "-",
									'mods' => '',
									'blog_entries' => ''); 
									
		}
		
		$car_container['response']['vehicle'][] = $car_item;
		
		$json_data = json_encode($car_container, JSON_PRETTY_PRINT);
		echo $json_data;	
		
		
	}
	
	
		
		
	
	
	public function upload_cover($car_id){
		
		$session_response = "INVALID";
		$container = array();
		$user_id = $_POST['user_id'];
		if(!empty($_POST['session_id'])){
		
			$session_id = $_POST['session_id'];
			
			$session_response = $this->checkSession($session_id);
			$session_data = $this->auth_model->sessionData($session_id);
			
		}
		
		$container['response']['session'] = array("id" => $car_id,
												"status" => $session_response,
												"selected_vehicle" => $session_data->vehicle_id);
		
		if($session_response == "OK")
		{
		
			// if file to upload is selected upload
			if($_FILES['image']['size'] != 0 && $_FILES['image']['error'] == 0)	
			{
				
				$uploadconfig['upload_path'] = 'items/uploads/coverpictures';
				$uploadconfig['allowed_types'] = 'gif|jpg|png';
				$uploadconfig['encrypt_name'] = true;	
				
				$this->upload->initialize($uploadconfig);
				
				$this->upload->do_upload('image');
							
				
				
				
				
				$cover_data = $this->upload->data();
				
								
				$fname = 'items/uploads/coverpictures/'.$cover_data['file_name'];
				$this->load->library('image_moo');
				$this->image_moo->load($fname)->resize(1510,640)->save($fname,true);
				
				$fname_thumb = 'items/uploads/coverpictures/thumb__'.$cover_data['file_name'];
				$this->image_moo->load($fname)->resize(360,360)->save($fname_thumb,true);
				
				$newsfeed_album = $this->statics_model->getNewsfeedAlbum($car_id);
				if(!$newsfeed_album)
				{
					$newsfeed_album_id = $this->statics_model->addAlbumNewsfeed("Uploads","",$car_id);
				}
				else 
				{
					$newsfeed_album_id = $newsfeed_album->row()->id;
				}
				
				
				
				$image_id = $this->statics_model->addPicture($cover_data['file_name'],$newsfeed_album_id,$car_id);
				$image_data = array('cover_image' => 1);
				$this->statics_model->updateImage($image_id,$image_data);
				$event_id = $this->statics_model->addEvent("7", $car_id, $image_id);		
				$this->statics_model->addEventVehicle($car_id,$event_id);
				
				
				// prepare array for db insert
				$data = array('id' => $car_id,
							'cover_image' => $image_id);


				$this->car_model->editVehicle($data);
				
				
				$carcover = $this->statics_model->getImageByID($image_id)->row()->fname;
				$container_item = array("id" => $id,
										"status" => "OK",
										"url" => site_url().'items/uploads/coverpictures/'.$carcover);
										
				$container['response']['image'] = $container_item;
				
			}	
			else{
				$container_item = array("id" => $id,
										"status" => "Error",
										"url" => "-");
														
				$container['response']['image'] = $container_item;
				
			}	
		}
		else
		{
			$container['response']['image'] = array("id" => $car_id,
													"status" => "Error",
													"url" => "-");
													
		}
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		
		echo $json_data;
		
		
		
	}
	
	public function upload_profile($car_id){
		
		
		$session_response = "INVALID";
		$container = array();
		$user_id = $_POST['user_id'];
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
			$session_data = $this->auth_model->sessionData($session_id);
		}
		
		$container['response']['session'] = array("id" => $car_id,
												"status" => $session_response,
												"selected_vehicle" => $session_data->vehicle_id);
		
		if($session_response == "OK")
		{
			// if file to upload is selected upload
			if($_FILES['image']['size'] != 0 && $_FILES['image']['error'] == 0)	
			{
				$uploadconfig['upload_path'] = 'items/uploads/profilepictures';
				$uploadconfig['allowed_types'] = 'gif|jpg|png';
				$uploadconfig['encrypt_name'] = true;	
				
				$this->upload->initialize($uploadconfig);
				
				$this->upload->do_upload('image');
							

				$profile_data = $this->upload->data();
											
				$fname = 'items/uploads/profilepictures/'.$profile_data['file_name'];
				$this->load->library('image_moo');
				
				
				
				
				$this->image_moo->load($fname)->resize(360,360)->save($fname,true);
				
				$fname_thumb = 'items/uploads/profilepictures/thumb__'.$profile_data['file_name'];
				$this->image_moo->load($fname)->resize(360,360)->save($fname_thumb,true);
				
				$newsfeed_album = $this->statics_model->getNewsfeedAlbum($car_id);
				if(!$newsfeed_album)
				{
					$newsfeed_album_id = $this->statics_model->addAlbumNewsfeed("Uploads","",$car_id);
				}
				else 
				{
					$newsfeed_album_id = $newsfeed_album->row()->id;
				}
				
				
				
				$image_id = $this->statics_model->addPicture($profile_data['file_name'],$newsfeed_album_id,$car_id);
				$image_data = array('profile_image' => 1);
				$this->statics_model->updateImage($image_id,$image_data);
				$event_id = $this->statics_model->addEvent("6", $car_id, $image_id);		
				$this->statics_model->addEventVehicle($car_id,$event_id);
				
				
				// prepare array for db insert
				$data = array('id' => $car_id,
							'profile_image' => $image_id);
				
				
				
				
				$carprofile = $this->statics_model->getImageByID($image_id)->row()->fname;
				
				
				
				$this->car_model->editVehicle($data);
				$container_item = array("id" => $car_id,
										"status" => "OK",
										"url" => site_url().'items/uploads/profilepictures/'.$carprofile);
				$container['response']['image'] = $container_item;
			}	
			else{
				$container_item = array("id" => $id,
										"status" => "File failed",
										"url" => "-");
				$container['response']['image'] = $container_item;
													
			}	
		}
		else
		{
			$container['response']['image'] = array("id" => $car_id,
														"status" => "Error",
														"url" => "-");
		}
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
		
	}
	
	public function upload_picture($album_id){
		
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
			$session_data = $this->auth_model->sessionData($session_id);
		}
		
		$container['response']['session'] = array("id" => $album_id,
													"status" => $session_response,
													"selected_vehicle" => $session_data->vehicle_id);
		
		if($session_response == "OK")
		{
			$image_title = $_POST['title'];
			// if file to upload is selected upload
			if($_FILES['image']['size'] != 0 && $_FILES['image']['error'] == 0)	
			{
				$uploadconfig['upload_path'] = 'items/uploads/images';
				$uploadconfig['allowed_types'] = 'gif|jpg|png';
				$uploadconfig['encrypt_name'] = TRUE;				
				$this->upload->initialize($uploadconfig);			
				$this->upload->do_upload('image');
				$image_data = $this->upload->data();
				
				$user_id = $_POST['user_id'];
				$vehicle_id = $_POST['vehicle_id'];
				$cars = $this->car_model->getVehicles($user_id);
				
				$this->load->library('image_moo');
				$fname = 'items/uploads/images/'.$image_data['file_name'];
				
				//FIX IMAGE ROTATION 
				$exif = exif_read_data($fname);
				$ort = $exif['Orientation'];
						
	            switch($ort)
	            {
	
	                case 3: // 180 rotate left
	                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
	                    break;
	                case 6: // 90 rotate right
	                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
	                    break;			
	                case 8:    // 90 rotate left
	                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
	                    break;
					default:{};
	            }
				
				
				
				if($image_data['image_width']>1000 || $image_data['image_height']>1000)
				{

					$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
					
				}
	       	 		$fname_fb = 'items/uploads/images/fb_'.$image_data['file_name'];
					$this->image_moo->load($fname)->resize(400,400)->save($fname_fb,true);
					
					$fname_thumb = 'items/uploads/images/thumb__'.$image_data['file_name'];
					$this->image_moo->load($fname)->resize(380,440)->save($fname_thumb,true);
			
				if($album_id == 0)
				{
					$mobile_album =	$this->statics_model->getMobileAlbum($vehicle_id);
					if($mobile_album)
					{
	
						$entity_id = $this->statics_model->addPictureMobile($image_data['file_name'],$mobile_album->id,$user_id, $image_title,$vehicle_id);
					}
					else
					{
						$title = "Mobile uploads";
						$about = "";
						$insert_id = $this->statics_model->addMobileAlbum($title,$about,$vehicle_id);
						$entity_id = $this->statics_model->addPictureMobile($image_data['file_name'],$insert_id,$user_id, $image_title,$vehicle_id);	
					}												
				}
				else
				{
					$entity_id = $this->statics_model->addPictureMobile($image_data['file_name'],$album_id,$user_id, $image_title,$vehicle_id);
				}
					   	 	
		   	 	$event_id = $this->statics_model->addEvent("8", $entity_id);
				
				$this->statics_model->addEventVehicle($vehicle_id,$event_id);
					
				
				$container_item = array("id" => $album_id,
															"status" => "OK");
				$container['response']['response'][] = $container_item;
			}	
			else{
				
				
				$container_item = array("id" => $album_id,
															"status" => "Error");
				$container['response']['response'][] = $container_item;
			}	
		}
		else
		{
			$container['response']['response'][] = array("id" => $album_id,
														"status" => "Error");
		}
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
	}
	
	public function get_vehicle_albums($vehicle_id)
	{
		
		
		if(isset($_POST['user_id']))
		{
			
			$user_id = $_POST['user_id'];
			
			$session_response = "INVALID";
			
			if(!empty($_POST['session_id'])){
				$session_id = $_POST['session_id'];
				$session_response = $this->checkSession($session_id);
				$session_data = $this->auth_model->sessionData($session_id);
			}
			
			$albums_data['response']['session'] = array("id" => $user_id,
														"status" => $session_response,
														"selected_vehicle" => $session_data->vehicle_id);
				
			if($session_response == "OK")
			{
				
				$albums = $this->statics_model->getAlbums($vehicle_id);
			
				foreach($albums as $album)
				{
				
					$picture = $this->statics_model->getPicturesFromAlbum($album['id'])->row();
					
					if(!empty($picture))
					{
					
							$image = $this->statics_model->getImageByID($picture->id)->row();
							
							$type = "images";
							
							if($image->cover_image == 1)
							{
								$type = "coverpictures";
							}
							else if($image->profile_image == 1)
							{
								$type = "profilepictures";
							}
							$picture_item = array('id' => $album['id'], 
											  'title' => $album['title'],
											   'cdate' => $image->created_date,
											  'url' => site_url()."items/uploads/".$type."/".$picture->fname);
						  
						  $albums_data['response']['albums'][] = 	$picture_item;
						
					
						
										  
						
					}
					
										  
				}
			}
			else
			{
				$picture_item = array('id' => "00",
										  'title' => "-",
										  'cdate' => "-",
										  'url' => "-");
										  
				$albums_data['response']['albums'][] = 	$picture_item;					  	
			}
			
			
			
			$json_data = json_encode($albums_data, JSON_PRETTY_PRINT);
			echo $json_data;	
		}	
	}
	
	public function show_single_album($album_id)
	{
		$album_data = array();
		
		$session_response = "OK";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
			$session_data = $this->auth_model->sessionData($session_id);
		}
		
		$album_data['response']['session'] = array("id" => $album_id,
													"status" => $session_response,
													"selected_vehicle" => $session_data->vehicle_id);
			
		if($session_response == "OK")
		{
			$pictures = $this->statics_model->getPicturesFromAlbum($album_id)->result();	
			
			foreach($pictures as $picture)
			{
				
				$image = $this->statics_model->getImageByID($picture->id)->row();
							
				$type = "images";
				
				if($image->cover_image == 1)
				{
					$type = "coverpictures";
				}
				else if($image->profile_image == 1)
				{
					$type = "profilepictures";
				}
				$album_item = array('id' => $picture->id,
									  'title' => $picture->title,
									  'likes' => $picture->likes,
									  'image' => site_url()."items/uploads/".$type."/".$picture->fname);
									  
				$album_data['response']['pictures'][] = $album_item;
			}
		}
		else
		{
			$album_item = array('id' => "00",
									  'title' => "-",
									  'likes' => "-",
									  'image' => "-");	
			$album_data['response']['pictures'][] = $album_item;
		}
								  
		$json_data = json_encode($album_data, JSON_PRETTY_PRINT);
		echo $json_data;	
				
	}
	
	// LOGGING
	function login()
	{
		
		if(isset($_POST['password']) && isset($_POST['username']) && !empty($_POST['password']) && !empty($_POST['username'])){
			$inputPW = $_POST['password'];
			$inputEmail = $_POST['username'];
			
			
			$stored = $this->auth_model->checkMobilePW($inputEmail);
			
			if($stored)
			{
				$storedPW = $stored->row()->password;
				$pwCheck = $this->check_hash($inputPW, $storedPW);
			}

		}
		else{
		
			$inputPW = "";
			$inputEmail = "";
			$pwCheck = false;
			
			
		}
		
		$response = array();
		$sess_id = "00";
		
		if ($pwCheck && isset($inputPW) && isset($inputEmail))
		{
			//if the login is successful		
			$data = array();
			$ip = $this->input->ip_address();  
			$random_string = $this->rand_string(16);
			$result = $this->auth_model->checkLoginUserFB($inputEmail);
			$user_id = $result->id;
			
			$data = array(	"session_id" => $random_string,
							"ip_address" => $ip,
							"user_id" => $user_id,
							"created_date" => date('Y-m-d H:i:s'),
							"last_used_date" => date('Y-m-d H:i:s'),
							);
			$sess_id = $this->auth_model->saveSession($data);
			$this->addUserPoints($user_id,1);
			
			$response['response'] = array("id" => $sess_id,
											'status' => "OK",
								  			'session_id' => $random_string,
								  			'user_id' => $user_id,
								  			'error' => "-");
		}
		else
		{
			//if the login was un-successful
			$response['response'] = array("id" => $sess_id,
											'status' => "FAIL",
								  			'session_id' => "-",
								  			'user_id' => "-",
								  			'error' => "Invalid email or password!");
			
			
		}
		
		
			  
		$json_data = json_encode($response, JSON_PRETTY_PRINT);
	
		echo $json_data;	
		
	}
	
	function checkSession($session_id, $user_id = null){
		
		if($user_id == null){
			$session = $this->auth_model->checkSession($session_id);
		}
		else{	
			$session = $this->auth_model->checkSessionUser($session_id,$user_id);
		}
		
		if($session == "NO"){
			return "INVALID";
		}
		else{
			if(strtotime($session->last_used_date) >= strtotime("-60 minutes")){
			  $this->auth_model->updateSession($session_id);
			  return "OK";
			}else{
			  return "EXPIRED";
			}
		}
	}
				
	
	public function loginUserFB(){
		
		$fbId = $_POST['fb_id'];
		$email = $_POST['email'];		
		$token = $_POST['token'];
		
		$response = array();
		$sess_id = "00";
		
		
		
		if($this->auth_model->checkFbId($fbId))
		{
			// FOUND FB ID
			
			$token_check = file_get_contents('https://graph.facebook.com/me?access_token=' . $token);
			$fb_id = json_decode($token_check)->id;
			
			$token_check = file_get_contents('https://graph.facebook.com/app?access_token=' . $token);
			$app_id = json_decode($token_check)->id;
			
			$emailCheck = $this->auth_model->checkFbToEmail($fbId, $email);
			
			if($fb_id == $fbId && $app_id == "1680628872212292" && $emailCheck != FALSE)
			{	
				//LOGIN
				$random_string = $this->rand_string(16);	
				$ip = $this->input->ip_address(); 			
				$result = $this->auth_model->checkFbId($fbId)->row();
				
				$user_id = $result->id;
				$this->addUserPoints($result->id,1);
				$data = array(	"session_id" => $random_string,
							"ip_address" => $ip,
							"user_id" => $user_id,	
							"created_date" => date('Y-m-d H:i:s'),
							"last_used_date" => date('Y-m-d H:i:s'),
							);			  			
								  			
				$sess_id = $this->auth_model->saveSession($data);
				
				
				
				$response['response'] = array("id" => $sess_id,
											'status' => "OK",
								  			'session_id' => $random_string,
								  			'user_id' => $user_id,
								  			'error' => "-");
								  			
				
				
				$json_data = json_encode($response, JSON_PRETTY_PRINT);
				echo $json_data;			
				
			}
			else{
				//INVALID FBID
				$response['response'] = array("id" => $sess_id,
											'status' => "FAIL",
								  			'session_id' => "-",
								  			'user_id' => "-",
								  			'error' => "ID or Email invalid!");
				$json_data = json_encode($response, JSON_PRETTY_PRINT);
				echo $json_data;
			}
			
	
		}
		else
		{
			// NO FB ID
			
			if($this->auth_model->checkLoginUserFB($email) != false)
			{
				// FOUND EMAIL
				
				
				
				$token_check = file_get_contents('https://graph.facebook.com/me?access_token=' . $token);
				$fb_id = json_decode($token_check)->id;
				$token_check = file_get_contents('https://graph.facebook.com/app?access_token=' . $token);
				$app_id = json_decode($token_check)->id;
			
				if($fb_id == $fbId && $app_id == "1680628872212292")
				{
					$updated = array('fb_id' => $fbId);
				
					$this->auth_model->updateProfilePic($email, $updated);	
					//LOGIN
					$random_string = $this->rand_string(16);	
					$ip = $this->input->ip_address(); 			
					$result = $this->auth_model->checkFbId($fbId)->row();
					
					$user_id = $result->id;
					$this->addUserPoints($result->id,1);
					$data = array(	"session_id" => $random_string,
								"ip_address" => $ip,
								"user_id" => $user_id,	
								"created_date" => date('Y-m-d H:i:s'),
								"last_used_date" => date('Y-m-d H:i:s'),
								);			  			
									  			
					$sess_id = $this->auth_model->saveSession($data);
					
					
					
					$response['response'] = array("id" => $sess_id,
												'status' => "OK",
									  			'session_id' => $random_string,
									  			'user_id' => $user_id,
									  			'error' => "-");
									  			
					
					
					$json_data = json_encode($response, JSON_PRETTY_PRINT);
					echo $json_data;			
					
				}
				else{
					//INVALID FBID
					$response['response'] = array("id" => $sess_id,
												'status' => "FAIL",
									  			'session_id' => "-",
									  			'user_id' => "-",
									  			'error' => "ID invalid!");
					$json_data = json_encode($response, JSON_PRETTY_PRINT);
					echo $json_data;
				}
			}
			else
			{
				// NO EMAIL - ADD THE USER
				
				$token_check = file_get_contents('https://graph.facebook.com/me?access_token=' . $token);
				$fb_id = json_decode($token_check)->id;			
				$user_data = json_decode($token_check);
				
				$token_check = file_get_contents('https://graph.facebook.com/app?access_token=' . $token);
				$app_id = json_decode($token_check)->id;
				
				
				if($fb_id == $fbId && $app_id == "1680628872212292")
				{
					$username = $user_data->username;
					$pw =  $this->rand_string(16);
					$additional_data = array(
						'first_name' => $user_data->first_name,
						'last_name'  => $user_data->last_name,
					);
					
					
					
					$this->ion_auth->register($username, $pw, $email, $fbId, $additional_data);
					
					
					//LOGIN
					$random_string = $this->rand_string(16);	
					$ip = $this->input->ip_address(); 			
					$result = $this->auth_model->checkFbId($fbId)->row();
					
					$user_id = $result->id;
					
					$data = array(	"session_id" => $random_string,
								"ip_address" => $ip,
								"user_id" => $user_id,	
								"created_date" => date('Y-m-d H:i:s'),
								"last_used_date" => date('Y-m-d H:i:s'),
								);			  			
									  			
					$sess_id = $this->auth_model->saveSession($data);
					
					
					
					$response['response'] = array("id" => $sess_id,
												'status' => "OK",
									  			'session_id' => $random_string,
									  			'user_id' => $user_id,
									  			'error' => "-");
				}
				else
				{
					//INVALID FBID
					$response['response'] = array("id" => $sess_id,
												'status' => "FAIL",
									  			'session_id' => "-",
									  			'user_id' => "-",
									  			'error' => "ID invalid!");
					
				}			  			
				
				
				$json_data = json_encode($response, JSON_PRETTY_PRINT);
				echo $json_data;			
					
				
			}
		}
		
				
		
	}
	
	
	public function register()
	{
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$username = $firstname . ' ' . $lastname;
		$email    = $_POST['email'];
		$password = $_POST['password'];
		$birthday_day = $_POST['birth_day'] != "" ? $_POST['birth_day'] : null;
		$birthday_month = $_POST['birth_month'] != "" ? $_POST['birth_month'] : null; 
		$birthday_year = $_POST['birth_year'] != "" ? $_POST['birth_year'] : null;  
		$fbId	  = $_POST['fbID'];
		$birthday = $birthday_year."-".$birthday_month."-".$birthday_day;
		$birthday = date("Y-m-d", strtotime($birthday));
		$newsletter	  = $_POST['newsletter'];
		$fb_link = $_POST['fb_link'];
		$tw_link = $_POST['tw_link'];
		$insta_link = $_POST['insta_link'];
		$phone = $_POST['phone'];
		$promo_code = $_POST['promo_code'];
		
		$country = "";
		$state = "";
		
		if( isset($_POST['country']) )
		{
			$country = $_POST['country'];
		}
		
		if( isset($_POST['state']) )
		{
			$state = $_POST['state'];
		}

		//validate form input
				
		
			
			$pw = $this->my_hash($password);
			
			if($fbId!=0){
				$content = file_get_contents('http://graph.facebook.com/'.$fbId.'/picture?redirect=0');
				$data = json_decode($content, true);
				
				$picture = $data["data"]["url"];
				
				$filename = substr($picture, strrpos($picture, '/') + 1);
				$arr = explode("?", $filename, 2);
				$filename = $arr[0];
				$img = $_SERVER['DOCUMENT_ROOT'].'/items/uploads/profilepictures/'.$filename;
				
				$url = 'https://graph.facebook.com/'.$fbId.'/picture?width=180&height=180';
			    $file = file_get_contents($url);
			    $fp = fopen($img,"wb");
			    if (!$fp) exit;
			    fwrite($fp, $file);
			    fclose($fp);
				
				$additional_data = array(
					'first_name' => $firstname,
					'last_name'  => $lastname,
					'birthday'  => $birthday,
					'profile_picture'  => $filename,
					'phone'  => $phone,
					'fb_link'  => $fb_link,
					'tw_link'  => $tw_link,
					'insta_link'  => $insta_link,
					'country'  => $country,
					'state'  => $state
				);
			}
			else{
				$additional_data = array(
					'first_name' => $firstname,
					'last_name'  => $lastname,
					'birthday'  => $birthday,
					'phone'  => $phone,
					'fb_link'  => $fb_link,
					'tw_link'  => $tw_link,
					'insta_link'  => $insta_link,
					'country'  => $country,
					'state'  => $state
				);
			}
			
			if($newsletter == 1)
			{
				$additional_data['newsletter'] = 1;
			}
		$additional_data['app_reg'] = 1;
		
		// Validate e-mail
		if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
		{
			$user_id = $this->ion_auth->register($username, $pw, $email, $fbId, $additional_data);
		    if ($user_id !== FALSE)
			{
				$mailname = $firstname." ".$lastname;
				//check to see if we are creating the user
				//redirect them back to the admin page
				//$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->send_mail('registration', $email, $username, $pw, $promo = NULL, $mailname);
				
				//echo $this->ion_auth->messages();
				
			
				$promo_code_rate = $this->auth_model->getPromoRate()->row()->bonus_impressions;
				$userWithPromo = $this->auth_model->checkPromoCode($promo_code);
				
				if($userWithPromo != false)
				{
					
					$promo_data = array('user_id' => $user_id,
										'partner_id' => $userWithPromo->row()->id);
										
					$this->auth_model->savePromoRegistration($promo_data);
					
					$newImpressions = $userWithPromo->row()->impressions_left+$promo_code_rate;
					
					
					$impression_data = array('impressions_left' => $newImpressions);
					$this->auth_model->updateImpressions($userWithPromo->row()->id, $impression_data);
				}
				
			
				
				
				
				
				$picture_item = array('id' => $user_id,
											  'status' => "OK");
											  
				$albums_data['response']['status'][] = $picture_item;
			}
			else
			{
				
				$picture_item = array('id' => "00",
											  'status' => "FAIL");
											  
				$albums_data['response']['status'][] = $picture_item;
		
			}
		} 
		else 
		{
		   $picture_item = array('id' => "00",
								'status' => "EMAIL");
										  
		   $albums_data['response']['status'][] = $picture_item;
		}
		
			
		
		
		
		

		$json_data = json_encode($albums_data, JSON_PRETTY_PRINT);
		echo $json_data;	
	}
	
	

	public function nearbyVehicles()
	{

		/* END TEST */
		
		$search_distance = $_POST['distance']; //in meters	
		$from = $_POST['latlon'];	
		
		$search = $_POST['search'];
		$search = trim($search);
			
			
		$vehicle_type = $_POST['vehicle_type'];
		if($vehicle_type == '')
		{
			$vehicle_type = 'all';
		}

		$engine_type = $_POST['engine_type'];
		if($engine_type == '')
		{
			$engine_type = 'all';
		}
			
		$year_select = $_POST['year_select'];
		if($year_select == '')
		{
			$year_select = 0;
		}
		
		$for_sale = $_POST['for_sale'];
		if($for_sale == '')
		{
			$for_sale = 0;
		}
		
		$hp_ranges = $_POST['hp_ranges'];
		if($hp_ranges == '')
		{
			$hp_ranges = 'all';
		}
		
		$buddy_only = false;
		
	
		
	
		if($_POST['buddy_only'])
		{
			$buddy_only = true;
		}
		
	
		
		$make_input = $_POST['make_input'];
		$make_input = trim($make_input);
		
		$model_input = $_POST['model_input'];
		$model_input = trim($model_input);
		
		$user_id = $_POST['user_id'];

		$session_response = "INVALID";
		

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$location_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$location_data['response']['vehicles']= array();
		if($session_response == "OK")
		{
				
			if(!$buddy_only)
			{
						
				$vehicles = $this->car_model->vehicleSearchProfile($search)->result();
		
				foreach($vehicles as $vehicle)
				{
								
					$to = $vehicle->geo_location;
					
					if($to !== NULL && $to != "")
					{
						
						$to_arr = explode(",", $to);
						$from_arr = explode(",", $from);
						
						$from_lat = $from_arr[0];
						$from_lon = trim($from_arr[1]);
						$to_lat = $to_arr[0];
						$to_lon = trim($to_arr[1]);
						
						
						$distance = $this->distance($to_lat, $to_lon, $from_lat, $from_lon);
						$distance = round($distance, 2);
						$nearby_limit = 10;
						if($search_distance != '')
						{
							//$nearby_limit = $search_distance*1609.344;
							$nearby_limit = $search_distance;
						}
						
						if($distance !== NULL || $search_distance == 0)
						{						
								
							switch($vehicle->vehicle_type)
							{
								case 0:{
											$model = $this->car_model->getCarModelById($vehicle->model_id)->row();
											$make = $model->model." ".$model->model_trim;
								};break;
								
								case 1:{
											$model = $this->car_model->getMotorModelById($vehicle->model_id)->row();
											$make = $model->model;
								};break;
								
								case 2:{
											$model = $this->car_model->getTruckModelById($vehicle->model_id)->row();
											$make = $model->model." ".$model->model_trim;
								};break;
							}	
						

							if($vehicle_type == "all" || $vehicle_type == $vehicle->vehicle_type)
							{
									
								if($engine_type == "all" || $engine_type == $vehicle->engine_type)
								{
									if($hp_ranges == "all" || $hp_ranges == $vehicle->hp_ranges)
									{		
										if($year_select == 0 || $year_select == $model->year)
										{
											if($make_input == '' ||  strpos(strtolower($model->make), strtolower($make_input)) !== false )
											{
												
												if($model_input == '' ||  strpos(strtolower($make), strtolower($model_input)) !== false )
												{	
									
													if($distance <= $nearby_limit || $search_distance == 0)
													{
																				
														$carprofile = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
														
														
														if($for_sale == 0)
														{
															$car_item = array('id' => $vehicle->id,
																		'nickname' => $vehicle->nickname,
																		'profile_picture' => site_url()."items/uploads/profilepictures/".$carprofile,
																		'distance' => $distance,
																		'lat' => $to_lat,
																		'lon' => $to_lon
																		);
																		$location_data['response']['vehicles'][] = $car_item;
														}
														else
														{
															if($vehicle->for_sale == 1)
															{
																$car_item = array('id' => $vehicle->id,
																		'nickname' => $vehicle->nickname,
																		'profile_picture' => site_url()."items/uploads/profilepictures/".$carprofile,
																		'distance' => $distance,
																		'lat' => $to_lat,
																		'lon' => $to_lon
																		);
																$location_data['response']['vehicles'][] = $car_item;
															}
														}
		
													}
														
												}
											
										}
									}
									
									}
								}
							}
							
								
								
								
							
						}
					}
					else 
					{
						switch($vehicle->vehicle_type)
							{
								case 0:{
											$model = $this->car_model->getCarModelById($vehicle->model_id)->row();
											$make = $model->model." ".$model->model_trim;
								};break;
								
								case 1:{
											$model = $this->car_model->getMotorModelById($vehicle->model_id)->row();
											$make = $model->model;
								};break;
								
								case 2:{
											$model = $this->car_model->getTruckModelById($vehicle->model_id)->row();
											$make = $model->model." ".$model->model_trim;
								};break;
							}	
						

							if($vehicle_type == "all" || $vehicle_type == $vehicle->vehicle_type)
							{
									
								if($engine_type == "all" || $engine_type == $vehicle->engine_type)
								{
									if($hp_ranges == "all" || $hp_ranges == $vehicle->hp_ranges)
									{		
										if($year_select == 0 || $year_select == $model->year)
										{
											if($make_input == '' ||  strpos(strtolower($model->make), strtolower($make_input)) !== false )
											{
												
												if($model_input == '' ||  strpos(strtolower($make), strtolower($model_input)) !== false )
												{	
									
													if($search_distance == 0)
													{
																				
														$carprofile = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
														
														
														if($for_sale == 0)
														{
															$car_item = array('id' => $vehicle->id,
																		'nickname' => $vehicle->nickname,
																		'profile_picture' => site_url()."items/uploads/profilepictures/".$carprofile,
																		'distance' => '',
																		'lat' => '',
																		'lon' => ''
																		);
																		$location_data['response']['vehicles'][] = $car_item;
														}
														else
														{
															if($vehicle->for_sale == 1)
															{
																$car_item = array('id' => $vehicle->id,
																		'nickname' => $vehicle->nickname,
																		'profile_picture' => site_url()."items/uploads/profilepictures/".$carprofile,
																		'distance' => '',
																		'lat' => '',
																		'lon' => ''
																		);
																$location_data['response']['vehicles'][] = $car_item;
															}
														}
		
													}
														
												}
											
										}
									}
									
									}
								}
							}
					}		
				}
		
			}
			else
			{
				$buddy_holder = array();
				
				$my_vehicles = $this->car_model->getVehicles($user_id)->result();
				foreach($my_vehicles as $my_vehicle)
				{
					$my_followers = $this->car_model->getFollowers($my_vehicle->id);
			
					foreach($my_followers as $follower)
					{
					
						if(!in_array($follower['follower_id'], $buddy_holder))
						{	
							$buddy_holder[] = $follower['follower_id'];
						}
					}

					$my_follows = $this->car_model->getMyFollows($my_vehicle->id);
					
					foreach($my_follows as $follower)
					{

						if(!in_array($follower['vehicle_id'], $buddy_holder))
						{						
							$buddy_holder[] = $follower['vehicle_id'];
						}
			
					}
				}
								
							
				
					
				$vehicles = $this->car_model->vehicleSearchProfile($search)->result();
		
				foreach($vehicles as $vehicle)
				{
								
					$to = $vehicle->geo_location;
					
					if($to !== NULL && $to != "")
					{
						
						$to_arr = explode(",", $to);
						$from_arr = explode(",", $from);
						
						$from_lat = $from_arr[0];
						$from_lon = trim($from_arr[1]);
						$to_lat = $to_arr[0];
						$to_lon = trim($to_arr[1]);
						
						
						$distance = $this->distance($to_lat, $to_lon, $from_lat, $from_lon);
						$distance = round($distance, 2);
						$nearby_limit = 10;
						if($search_distance != '')
						{
							//$nearby_limit = $search_distance*1609.344;
							$nearby_limit = $search_distance;
						}
						
						if($distance !== NULL || $search_distance == 0)
						{						
								
							switch($vehicle->vehicle_type)
							{
								case 0:{
											$model = $this->car_model->getCarModelById($vehicle->model_id)->row();
											$make = $model->model." ".$model->model_trim;
								};break;
								
								case 1:{
											$model = $this->car_model->getMotorModelById($vehicle->model_id)->row();
											$make = $model->model;
								};break;
								
								case 2:{
											$model = $this->car_model->getTruckModelById($vehicle->model_id)->row();
											$make = $model->model." ".$model->model_trim;
								};break;
							}	
						

							if($vehicle_type == "all" || $vehicle_type == $vehicle->vehicle_type)
							{
									
								if($engine_type == "all" || $engine_type == $vehicle->engine_type)
								{
									if($hp_ranges == "all" || $hp_ranges == $vehicle->hp_ranges)
									{		
										if($year_select == 0 || $year_select == $model->year)
										{
											if($make_input == '' ||  strpos(strtolower($model->make), strtolower($make_input)) !== false )
											{
												
												if($model_input == '' ||  strpos(strtolower($make), strtolower($model_input)) !== false )
												{	
									
													if($distance <= $nearby_limit || $search_distance == 0)
													{
														
														if(in_array($vehicle->id, $buddy_holder))
														{					
															$carprofile = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
															
															
															if($for_sale == 0)
															{
																$car_item = array('id' => $vehicle->id,
																			'nickname' => $vehicle->nickname,
																			'profile_picture' => site_url()."items/uploads/profilepictures/".$carprofile,
																			'distance' => $distance,
																			'lat' => $to_lat,
																			'lon' => $to_lon
																			);
																			$location_data['response']['vehicles'][] = $car_item;
															}
															else
															{
																if($vehicle->for_sale == 1)
																{
																	$car_item = array('id' => $vehicle->id,
																			'nickname' => $vehicle->nickname,
																			'profile_picture' => site_url()."items/uploads/profilepictures/".$carprofile,
																			'distance' => $distance,
																			'lat' => $to_lat,
																			'lon' => $to_lon
																			);
																	$location_data['response']['vehicles'][] = $car_item;
																}
															}
														
														}
		
													}
														
												}
											
										}
									}
									
									}
								}
							}
							
								
								
								
							
						}
					}
					else
					{
						switch($vehicle->vehicle_type)
							{
								case 0:{
											$model = $this->car_model->getCarModelById($vehicle->model_id)->row();
											$make = $model->model." ".$model->model_trim;
								};break;
								
								case 1:{
											$model = $this->car_model->getMotorModelById($vehicle->model_id)->row();
											$make = $model->model;
								};break;
								
								case 2:{
											$model = $this->car_model->getTruckModelById($vehicle->model_id)->row();
											$make = $model->model." ".$model->model_trim;
								};break;
							}	
						

							if($vehicle_type == "all" || $vehicle_type == $vehicle->vehicle_type)
							{
									
								if($engine_type == "all" || $engine_type == $vehicle->engine_type)
								{
									if($hp_ranges == "all" || $hp_ranges == $vehicle->hp_ranges)
									{		
										if($year_select == 0 || $year_select == $model->year)
										{
											if($make_input == '' ||  strpos(strtolower($model->make), strtolower($make_input)) !== false )
											{
												
												if($model_input == '' ||  strpos(strtolower($make), strtolower($model_input)) !== false )
												{	
									
													if($search_distance == 0)
													{
														
														if(in_array($vehicle->id, $buddy_holder))
														{					
															$carprofile = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
															
															
															if($for_sale == 0)
															{
																$car_item = array('id' => $vehicle->id,
																			'nickname' => $vehicle->nickname,
																			'profile_picture' => site_url()."items/uploads/profilepictures/".$carprofile,
																			'distance' => '',
																			'lat' => '',
																			'lon' => ''
																			);
																			$location_data['response']['vehicles'][] = $car_item;
															}
															else
															{
																if($vehicle->for_sale == 1)
																{
																	$car_item = array('id' => $vehicle->id,
																			'nickname' => $vehicle->nickname,
																			'profile_picture' => site_url()."items/uploads/profilepictures/".$carprofile,
																			'distance' => '',
																			'lat' => '',
																			'lon' => ''
																			);
																	$location_data['response']['vehicles'][] = $car_item;
																}
															}
														
														}
		
													}
														
												}
											
										}
									}
									
									}
								}
							}
					}		
				}
			}
		}

		
		$json_data = json_encode($location_data, JSON_PRETTY_PRINT);
		echo $json_data;
		
				
	}
	
	
	public function activities()
	{
		$session_response = "INVALID";
		$user_id = $_POST['user_id'];

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$activities_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$activities_data['response']['pending'] = array();	
		$activities_data['response']['accepted'] = array();
		$activities_data['response']['my_activities'] = array();
		if($session_response == "OK")
		{
						
			$activities = $this->statics_model->getActivities($user_id)->result();
			
			$activity_ids = array();
			$my_dogs = $this->car_model->getVehicles($user_id)->result();
			
			foreach($my_dogs as $car){
				
				$pending_result = $this->statics_model->getPendingActivities($car->id)->result();
				
				foreach($pending_result as $pending)
				{
					$time = $pending->time;
					$time = date('H:i',strtotime($time));
					$date = $pending->date;
					$about = $pending->about;
					$place = $pending->place;
					$user_result = $this->statics_model->getUserData($pending->created_by);
					$name = $user_result->row()->first_name." ".$user_result->row()->last_name;
					if(!in_array($pending->id, $activity_ids)){
						$activities_data['response']['pending'][] = array('id' => $pending->id,
																		  'place' => $place,
																		  'about' => $about,
																		  'time' => $time,
																		  'date' => $date,
																		  'created_by' => $name);
										  
						$activity_ids[] = $pending->id;
					}
				}
		
			}
			
			$accepted_ids = array();
			foreach($my_dogs as $car){
				
				$accepted_result = $this->statics_model->getAcceptedActivities($car->id)->result();
				
				foreach($accepted_result as $accepted)
				{
					$time = $accepted->time;
					$time = date('H:i',strtotime($time));
					$date = $accepted->date;
					$about = $accepted->about;
					$place = $accepted->place;
					$user_result = $this->statics_model->getUserData($accepted->created_by);
					$name = $user_result->row()->first_name." ".$user_result->row()->last_name;
					if(!in_array($accepted->id, $accepted_ids)){
						$activities_data['response']['accepted'][] = array('id' => $accepted->id,
																			  'place' => $place,
																			  'about' => $about,
																			  'time' => $time,
																			  'date' => $date,
																			  'created_by' => $name);
						$accepted_ids[] = $accepted->id;
					}
				}
			
			}
			
			foreach($activities as $activity)
				{
					
					$time = $activity->time;
					$time = date('H:i',strtotime($time));
					$date = $activity->date;
					$about = $activity->about;
					$place = $activity->place;
					$user_result = $this->statics_model->getUserData($activity->created_by);
					$name = $user_result->row()->first_name." ".$user_result->row()->last_name;
	
					if(!in_array($activity->id, $accepted_ids)){
						$activities_data['response']['my_activities'][] = array('id' => $activity->id,
										  'place' => $place,
										  'about' => $about,
										  'time' => $time,
										  'date' => $date,
										  'created_by' => $name);
						$accepted_ids[] = $activity->id;
					}
				}
			
				
			
					
			
		}
		else
		{
			$activity_item = array('id' => "00",
										  'place' => "-",
										  'about' => "-",
										  'time' => "-",
										  'date' => "-",
										  'created_by' => "-");
									  
			$activities_data['response']['response'][] = $activity_item;					  	
		}
		
		$json_data = json_encode($activities_data, JSON_PRETTY_PRINT);
		echo $json_data;
	
	}
	
	public function activity_detail()
	{
		$activity_id = $_POST['activity_id'];
		//$activity_id = 1;

		$session_response = "INVALID";
		

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$activity_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
			
		if($session_response == "OK")
		{
			$activity = $this->statics_model->getActivity($activity_id);
			
			
			$guests = $this->statics_model->getGuestsForActivity($activity_id)->result();
			$cars = array();
			foreach($guests as $car)
			{
				if(is_numeric($car->profile_image))
				{
					$carprofile = $this->statics_model->getImageByID($car->profile_image)->row()->fname;
				}
				else
				{
					$carprofile = $car->profile_image;
				}
				$car_item = array('id' => $car->id,
									'profile_picture' => $carprofile,
									'name' => $car->name,
									'pending' => $car->pending);
				$cars[] = $car_item;
			}
			
			$activity_item = array('id' => $activity_id,
									'created_by' => $activity->created_by,
									'time' => $activity->time,
									'date' => $activity->date,
									'about' => $activity->about,
									'place' => $activity->place,
									'guests' => $cars);
									
			$activity_data['response']['activity'][] = $activity_item;
			
		}
		else
		{
			$activity_item = array('id' => "00",
									'created_by' => "-",
									'time' => "-",
									'date' => "-",
									'about' => "-",
									'place' => "-",
									'guests' => "-",				
									'created_by' => "-");
									  
			$activity_data['response']['activity'][] = $activity_item;					  	
		}
		
		$json_data = json_encode($activity_data, JSON_PRETTY_PRINT);
		echo $json_data;	
	}



	public function update_location()
	{
	/*	$_POST['user_id'] = 101;
		$_POST['vehicle_id'] = 7;
		$_POST['latlon'] = 	"48.220463, 16.355292";	
	*/	
		/* END TEST */

		
		
		$user_id = $_POST['user_id'];
		$location = $_POST['latlon'];
		$vehicle_id = $_POST['vehicle_id'];
		
		$session_response = "INVALID";
		

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$activity_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
			
		if($session_response == "OK")
		{
			
			
			$update_data = array('id' => $vehicle_id,
									'geo_location' => $location);	
				
			$this->statics_model->updateLocationVehicle($update_data);	
			
			
			$activity_item = array('id' => $vehicle_id,
									'status' => "OK");
									  
			$activity_data['response']['response'][] = $activity_item;			
		}
		else
		{
			$activity_item = array('id' => "00",
									'status' => "INVALID SESSION");
									  
			$activity_data['response']['response'] = $activity_item;					  	
		}
		
		$json_data = json_encode($activity_data, JSON_PRETTY_PRINT);
		echo $json_data;	
	}

	public function nearbyEvents()
	{

		
		/* TEST DATA */	
			
	/*	echo "<pre>";
		$_POST['user_id'] = 98;
		$_POST['latlon'] = 	"48.220463, 16.355292";
		$_POST['distance'] = "100000000000000000";
		$_POST['search'] = '';		
		$_POST['name'] = '';
		$_POST['start'] = ''; // m-d-y
		$_POST['end'] = '';
	
	*/	
		/* END TEST */
		
		
		
		$user_id = $_POST['user_id'];
		$search = $_POST['search'];
		$search = trim($search);
		
		$name = $_POST['name'];
		$name = trim($name);
		
		$start = $_POST['start'];
		$start = trim($start);
		
		$end = $_POST['end'];
		$end = trim($end);
		
		$search_distance = $_POST['distance']; //in miles				
		$geo_location = $_POST['latlon'];
		
		
		
		
		$session_response = "INVALID";

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$search_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$search_data['response']['events'] = array();	
		if($session_response == "OK")
		{
			
				$events = array();		
				$table = "user_created_events";
	
				if($start == '' && $end =='')
				{
					
					$user_event_container = $this->statics_model->eventSearchTitle($search, $table)->result_array();
				}
				else if($start != '' && $end =='')
				{
					
					$start = date('y-m-d H:i:s', strtotime($start));	
				
					$user_event_container = $this->statics_model->eventSearchTitleStart($search, $table, $start)->result_array();
				}
				else if($start == '' && $end !='')
				{
					$end = date('y-m-d H:i:s', strtotime($end));	
					$user_event_container = $this->statics_model->eventSearchTitleEnd($search, $table, $end)->result_array();
				}
				else if($start != '' && $end !='')
				{
					$end = date('y-m-d H:i:s', strtotime($end));
					$start = date('y-m-d H:i:s', strtotime($start));
						
					$user_event_container = $this->statics_model->eventSearchTitleBetween($search, $table, $start, $end)->result_array();
				}
			
		
		
				foreach($user_event_container as $event)
				{
				
					$participants = $this->statics_model->getVehiclesFromUserEvent($event['id']);
					$vehicle = $this->car_model->getVehicle($event['vehicle_id'])->row();
					
					$image = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
					
					
					if($name == '' ||  strpos(strtolower($vehicle->nickname), strtolower($name)) !== false )
					{
							
						if($geo_location == "")
						{
									
								$item = array('id' => $event['id'],
											'title' => $event['title'],
											'distance' => '',
											'type' => 'user',
											'lat' => '',
											'lon' => '',
											'address' => $event['address'],
											'date' => $event['start_time'],
											'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
											'logo' => site_url()."items/uploads/profilepictures/".$image,
											'created_by' => $vehicle->nickname,
											'participants' => count($participants));
								
								if($event['public'] == 1)
								{
									$search_data['response']['events'][] = $item;
								}
									
									
								
								
								
								
						}
						else
						{
								$to_arr = explode(",", $event['geo_location']);
								$from_arr = explode(",", $geo_location);
								
			
								$from_lat = $from_arr[0];
								$from_lon = trim($from_arr[1]);
								$to_lat = $to_arr[0];
								$to_lon = trim($to_arr[1]);
								
								$distance = $this->distance($to_lat, $to_lon, $from_lat, $from_lon);
								$distance = round($distance, 2);
								$nearby_limit = 10;
								if($search_distance != '')
								{
									//$nearby_limit = $search_distance*1609.344;
									$nearby_limit = $search_distance;
								}
						
								if($distance !== NULL && $distance <= $nearby_limit)
								{
									$item = array('id' => $event['id'],
												'title' => $event['title'],
												'distance' => $distance,
												'lat' => $to_lat,
												'lon' => $to_lon,
												'type' => 'user',
												'address' => $event['address'],
												'date' => $event['start_time'],
												'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
												'logo' => site_url()."items/uploads/profilepictures/".$image,
												'created_by' => $vehicle->nickname,
												'participants' => count($participants));
						
									if($event['public'] == 1)
									{
										$search_data['response']['events'][] = $item;
									}
								}
						}	
	
				}
			}
									
			
			$table = "managed_group_events";
			
			
			
			if($start == '' && $end =='')
			{
				$group_event_container = $this->statics_model->eventSearchTitle($search, $table)->result_array();
			}
			else if($start != '' && $end =='')
			{
				
				$start = date('y-m-d H:i:s', strtotime($start));	
				$group_event_container = $this->statics_model->eventSearchTitleStart($search, $table, $start)->result_array();
			}
			else if($start == '' && $end !='')
			{
				$end = date('y-m-d H:i:s', strtotime($end));	
				$group_event_container = $this->statics_model->eventSearchTitleEnd($search, $table, $end)->result_array();
			}
			else if($start != '' && $end !='')
			{
				$end = date('y-m-d H:i:s', strtotime($end));
				$start = date('y-m-d H:i:s', strtotime($start));	
				$group_event_container = $this->statics_model->eventSearchTitleBetween($search, $table, $start, $end)->result_array();
			}
			
			
			
			
			foreach($group_event_container as $event){
				
				$participants = $this->statics_model->getVehiclesFromEvent($event['id']);
				$group = $this->statics_model->getGroupById($event['group_id'])->row();
				
				$image = $group->logo;
				if($name == '' ||  strpos(strtolower($group->name), strtolower($name)) !== false )
				{
					if($geo_location == "")
					{
								
							$item = array('id' => $event['id'],
												'title' => $event['title'],
												'distance' => '',
												'lat' => '',
												'lon' => '',
												'type' => 'group',
												'address' => $event['address'],
												'date' => $event['start_time'],
												'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
												'logo' => site_url()."items/uploads/profilepictures/".$image,
												'created_by' => $group->name,
												'participants' => count($participants));
				
							if($event['public'] == 1)
							{
								$search_data['response']['events'][] = $item;
							}	
								
							
							
							
							
					}
					else
					{
						$to_arr = explode(",", $event['geo_location']);
						$from_arr = explode(",", $geo_location);
						
	
						$from_lat = $from_arr[0];
						$from_lon = trim($from_arr[1]);
						$to_lat = $to_arr[0];
						$to_lon = trim($to_arr[1]);
						
						
						$distance = $this->distance($to_lat, $to_lon, $from_lat, $from_lon);
						$distance = round($distance, 2);
						$nearby_limit = 10;
						if($search_distance != '')
						{
							//$nearby_limit = $search_distance*1609.344;
							$nearby_limit = $search_distance;
						}
											
							
						if($distance !== NULL && $distance <= $nearby_limit)
						{
							$item = array('id' => $event['id'],
										'title' => $event['title'],
										'distance' => $distance,
										'lat' => $to_lat,
										'lon' => $to_lon,
										'type' => 'group',
										'address' => $event['address'],
										'date' => $event['start_time'],
										'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
										'logo' => site_url()."items/uploads/profilepictures/".$image,
										'created_by' => $group->name,
										'participants' => count($participants));
				
							if($event['public'] == 1)
							{
								$search_data['response']['events'][] = $item;
							}
						}
					}
		
				}
			}
			

			
						
						
			
			$data['vehicles_count'] = count($search_data['response']['events']);	
			
			
		
		}
		else
		{
			$search_item = array('id' => '-',
									'title' => '-',
									'distance' => '-',
									'type' => '-',
									'address' => '-',
									'start' => '-',
									'date' => '-',
									'logo' => '-',
									'created_by' => '-',
									'participants' => '-');
									  
			$search_data['response']['events'] = $search_item;					  	
		}
		
		
		function date_compare($a, $b)
		{
		    $t1 = strtotime($a['date']);
		    $t2 = strtotime($b['date']);
		    return $t2 - $t1;
		}    
		usort($search_data['response']['events'], 'date_compare');

		
		
		
		$json_data = json_encode($search_data, JSON_PRETTY_PRINT);
		echo $json_data;
	}
	
	
	public function searchPlaceName()
	{

		$user_id = $_POST['user_id'];
		
		$session_response = "INVALID";

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$search_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$search_data['response']['places'] = array();	
		if($session_response == "OK")
		{
			$name = $_POST['name'];
			
			$places = $this->statics_model->PlaceSearchByName($name)->result();
			shuffle($places);
			
			foreach($places as $vet)
			{
			
						
				
				$city = $vet->city;				
				if($city == NULL)
				{
					$city = "";
				}
				$geo_location = $vet->geo_location;				
				if($geo_location == NULL)
				{
					$geo_location = "";
				}
				
				$description = nl2br($vet->description);			
				$description = str_replace(array("\r", "\n", "\t"), "", $description);
				
				$type = $this->statics_model->getPlaceTypeByID($vet->type)->row()->name;
				
				$search_item = array('id' => $vet->id,
									'name' => $vet->name,
									'type' => $type,								
									'description' => $description,
									'address' => $vet->address,								
									'geo_location' => $geo_location,
									'city' => $city,
									'featured' => $vet->featured);
									
				$search_data['response']['places'][] = $search_item;	
			}	
				
		}
		else
		{
			$search_item = array('id' => "00",
									'name' => "-",	
									'type' => "-",							
									'description' => "-",					
									'address' => "-",
									'geo_location' => "-",
									'city' => "-",
									'featured' => "-");
									  
			$search_data['response']['places'] = $search_item;					  	
		}
		
		$json_data = json_encode($search_data, JSON_PRETTY_PRINT);
		echo $json_data;
	}

	
	public function add_like()
	{
		$user_id = $_POST['user_id'];
		$pic_id = $_POST['image_id'];
		$myvehicle_id = $_POST['vehicle_id'];
		
		$session_response = "INVALID";

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$search_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$search_data['response']['status'] = array();	
		
		if($session_response == "OK")
		{
			$check = $this->statics_model->checkPictureLike($pic_id, $user_id);	
		
			if($check == 0)
			{
				$this->statics_model->addLike($pic_id,$user_id);
				$vehicles = $this->car_model->getVehicles($user_id);
				 
				$event_id = $this->statics_model->addNotification(1, $pic_id);
				
			    $this->statics_model->addNotificationVehicle($myvehicle_id,$event_id);
				 
				$pic = $this->statics_model->getPicture($pic_id)->row();
				 
				$vehicles = $this->car_model->getVehicles($pic->created_by);
				 
				foreach($vehicles->result() as $vehicle){
					//ADD GAME POINTS
					$this->addPoints($vehicle->id,1,'likes', 'like');
				 }
				
				$search_data['response']['status'] = 'OK';
			}
			else
			{
				$search_data['response']['status'] = 'ALREADY';
			}				
		}
		else
		{
											  
			$search_data['response']['status'] = 'INVALID';					  	
		}
		
		$json_data = json_encode($search_data, JSON_PRETTY_PRINT);
		echo $json_data;	
	}
		
		
	public function add_event_like()
	{
		
		
		$user_id = $_POST['user_id'];
		$event_id = $_POST['event_id'];
		$myvehicle_id = $_POST['vehicle_id'];
		
		$session_response = "INVALID";

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$search_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$search_data['response']['status'] = array();	
		
		if($session_response == "OK")
		{
			$check = $this->statics_model->checkEventLike($event_id, $user_id);	
		
			if($check == 0)
			{
				$this->statics_model->addEventLike($event_id,$user_id);
				$vehicles = $this->car_model->getVehicles($user_id);
				 
				$created_event_id = $this->statics_model->addNotification(12, $event_id);
				
			    $this->statics_model->addNotificationVehicle($myvehicle_id,$created_event_id);
 
				foreach($vehicles->result() as $vehicle){
					//ADD GAME POINTS
					$this->addPoints($vehicle->id,1,'likes', 'like');
				 }
				
				$search_data['response']['status'] = 'OK';
			}
			else
			{
				$search_data['response']['status'] = 'ALREADY';
			}				
		}
		else
		{
											  
			$search_data['response']['status'] = 'INVALID';					  	
		}
		
		$json_data = json_encode($search_data, JSON_PRETTY_PRINT);
		echo $json_data;	
	}
	
	
	
	public function get_user_pictures()
	{
			
			$user_id = $_POST['user_id'];

			$albums_data = array();
			$session_response = "INVALID";
			
			if(!empty($_POST['session_id'])){
				$session_id = $_POST['session_id'];
				$session_response = $this->checkSession($session_id);
			}
			
			$albums_data['response']['session'] = array("id" => $user_id,
														"status" => $session_response);
				
			if($session_response == "OK")
			{
				$albums = $this->statics_model->getAlbums($user_id);
			
				foreach($albums->result() as $album)
				{
					$pictures = $this->statics_model->getPicturesFromAlbum($album->id)->result();
					
					if(!empty($pictures))
					{
						foreach($pictures as $picture)
						{
							$image = $this->statics_model->getImageByID($picture->id)->row();
							
							$type = "images";
							
							if($image->cover_image == 1)
							{
								$type = "coverpictures";
							}
							else if($image->profile_image == 1)
							{
								$type = "profilepictures";
							}
							
							
							
							$picture_item = array('id' => $picture->id,
											  'title' => $picture->title,
											  'album' => $album->title,
											  'cdate' => $image->created_date,
											  'url' => site_url()."items/uploads/".$type."/".$picture->fname);
						  
						  $albums_data['response']['pictures'][] = 	$picture_item;
						}
					
						
										  
						
					}
					
										  
				}
			}
			else
			{
				$picture_item = array('id' => "00",
										  'title' => "-",
										  'album' => "-",
										  'url' => "-");
										  
				$albums_data['response']['pictures'][] = 	$picture_item;					  	
			}
			
			
			
			
			
			usort($albums_data['response']['pictures'], function($a, $b) {
			    return $b['id'] - $a['id'];
			});
			
			$json_data = json_encode($albums_data, JSON_PRETTY_PRINT);
			echo $json_data;	
			
	}
	
	
	public function nearbyPOIs()
	{
		
		/* TEST DATA */
	/*	$_POST['distance'] = 10; // in miles
		$_POST['latlon'] = 	"48.220463, 16.355292";;
		$_POST['category'] = '';
		$_POST['rating'] = '';
		$_POST['search'] = '';
		
		*/
		
		
		$search_distance = $_POST['distance']; //in miles	
		$from = $_POST['latlon'];	
		$category_search = $_POST['category'];
		$rating_search = $_POST['rating'];
		$search = $_POST['search'];
		
		
		$user_id = $_POST['user_id'];

		$session_response = "INVALID";
		

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$location_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$location_data['response']['pois'] = array();
		if($session_response == "OK")
		{
			
			$poi = $this->statics_model->searchPOI($search)->result();
			foreach($poi as $place)
			{
				

				$to = $place->geo_location;
					
				if($to != NULL && $to != "")
				{
				
					$to_arr = explode(",", $to);
					$from_arr = explode(",", $from);
					
					$from_lat = $from_arr[0];
					$from_lon = trim($from_arr[1]);
					$to_lat = $to_arr[0];
					$to_lon = trim($to_arr[1]);
					
					
					$distance = $this->distance($to_lat, $to_lon, $from_lat, $from_lon);
					$distance = round($distance, 2);
					$nearby_limit = 10;
					if($search_distance != '')
					{
						//$nearby_limit = $search_distance*1609.344; // for meters
						$nearby_limit = $search_distance;
					}
					
					if($distance !== NULL)
					{
					
						
						if($distance <= $nearby_limit)
						{
							$category = $this->statics_model->getPOICategory($place->category_id);
							$ratings = $this->statics_model->getRatings($place->id,$user_id,'poi_ratings','poi_id')->result();
							$rows = count($ratings);
							$ratingSum = 0;
							foreach($ratings as $i=>$row){
								$ratingSum += $row->rating;
							}
							if($rows == 0){
								$rating = "No ratings yet!";
								$rSum = 0;
							}
							else{
								$rSum = $ratingSum/$rows;
								$rSum = round($rSum, 2);
								$number_of_votes = $rows;
								$rating = "Rating: ".$rSum." from ".$number_of_votes." votes";
								
							}
							
							if(!$category)
							{
								$category_name = "N/A";
							}
							else
							{
								$category_name = $category->name;
							}
							
							
							if ($rSum >= $rating_search) {
							
								if ($category_search != '')
								{
								 	if(stripos($category_name,$category_search) !== false) {
										$item = array('id' => $place->id,
														'name' => $place->name,
														'distance' => $distance,
														'geo_location' => $place->geo_location,
														'lat' => $to_lat,
														'lon' => $to_lon,
														'category' => $category_name,
														'category_id' => $place->category_id,
														'address' => $place->address,
														'rating' => $rating,
														'rating_num' => $rSum);
										
										$location_data['response']['pois'][] = $item;
									}
								}
								else
								{
									$item = array('id' => $place->id,
													'name' => $place->name,
													'distance' => $distance,
													'geo_location' => $place->geo_location,
													'lat' => $to_lat,
													'lon' => $to_lon,
													'category' => $category_name,
													'category_id' => $place->category_id,
													'address' => $place->address,
													'rating' => $rating,
													'rating_num' => $rSum);
									
									$location_data['response']['pois'][] = $item;
								}
							}
						}
					}
				}
			}
		}
		else
		{
			$search_item = array('id' => "00",
									'name' => "-",
									'category' => "-",
									'address' => "-",
									'rating' => "-");
									  
			$location_data['response']['pois'][] = $search_item;					  	
		}
		
		$json_data = json_encode($location_data, JSON_PRETTY_PRINT);
		echo $json_data;
		
				
	}
	
		
	
	
	public function getPlace()
	{
		
		
		$place_id = $_POST['place_id'];
		
		$user_id = $_POST['user_id'];

		$session_response = "INVALID";
		

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$location_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$location_data['response']['places'] = array();
		if($session_response == "OK")
		{
			$vet = $this->statics_model->getPlaceByID($place_id)->row();
	
			
			
			$to = $vet->geo_location;
			
			
				
				
				
				$distance = $this->distance($to_lat, $to_lon, $from_lat, $from_lon, "K");
				$distance = intval($distance*1000);
				$time = 0;
				
				
				
					
						$city = $vet->city;				
						if($city == NULL)
						{
							$city = "";
						}
						$geo_location = $vet->geo_location;				
						if($geo_location == NULL)
						{
							$geo_location = "";
						}
						
						$description = nl2br($vet->description);			
						$description = str_replace(array("\r", "\n", "\t"), "", $description);
						
						
						
						$geo_location = strip_tags(nl2br($geo_location));			
						$geo_location = str_replace(array("\r", "\n", "\t"), "", $geo_location);
						
						$type = $this->statics_model->getPlaceTypeByID($vet->id)->row()->name;
						
						$search_item = array('id' => $vet->id,
											'name' => $vet->name,
											'type' => $type,
											'description' => $description,												
											'address' => $vet->address,
											'geo_location' => $geo_location,
											'city' => $city,
											'featured' => $vet->featured,								
											'time' => (string)$time);
											
						$location_data['response']['places'][] = $search_item;	
				
					
					
					
				
						
						
					/*	$to = urlencode($to);
				
						$data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=".$from."&destinations=".$to."&language=en-EN&sensor=false");
						$data = json_decode($data);
						
						$distance = 0;
						$time = 0;
						
						foreach($data->rows[0]->elements as $road) {
						 	$time += $road->duration->value;	   
						    $distance = $road->distance->value;
						}
						
						if($distance === NULL)
						{
							$distance = 999999;
						}
					
						$time = round($time/60);
						
						$distance = (int) $distance;
							
						if($distance <= $nearby_limit)
						{
						
							
								$city = $vet->city;				
								if($city == NULL)
								{
									$city = "";
								}
								$geo_location = $vet->geo_location;				
								if($geo_location == NULL)
								{
									$geo_location = "";
								}
								
								$description = strip_tags(nl2br($vet->description));			
								$description = str_replace(array("\r", "\n", "\t"), "", $description);
								
								$contact = strip_tags(nl2br($vet->contact));			
								$contact = str_replace(array("\r", "\n", "\t"), "", $contact);
								
								$times = strip_tags(nl2br($vet->times));			
								$times = str_replace(array("\r", "\n", "\t"), "", $times);
								
								$geo_location = strip_tags(nl2br($geo_location));			
								$geo_location = str_replace(array("\r", "\n", "\t"), "", $geo_location);
								
								$search_item = array('id' => $vet->id,
													'firstname' => $vet->firstname,
													'lastname' => $vet->lastname,
													'title' => $vet->title,
													'description' => $description,
													'contact' => $contact,
													'address' => $vet->address,
													'times' => $times,
													'profilepicture' => site_url('items/uploads/profilepictures/')."/".$vet->profilepicture,
													'coverpicture' => site_url('items/uploads/coverpictures/')."/".$vet->coverpicture,
													'geo_location' => $geo_location,
													'city' => $city,
													'featured' => $vet->featured,
													'distance' => (string) $distance,
													'time' => (string)$time,
													'avg_rating' =>(string) (string)$ratingSum,
													'num_votes' => $number_of_votes,
													'current_user_rated' => $already_rated);
													
								$location_data['response']['vets'][] = $search_item;	
						
							
							
							
						}*/
					 
				
			
		}
		else
		{
			$search_item = array('id' => "00",
									'name' => "-",
									'type' => "-",
									'description' => "-",
									'address' => "-",
									'geo_location' => "-",
									'city' => "-",
									'featured' => "-",								
									'time' => "0");
									  
			$location_data['response']['places'][] = $search_item;					  	
		}
		
		$json_data = json_encode($location_data, JSON_PRETTY_PRINT);
		echo $json_data;
		
				
	}
	
	
	
	
	
	public function nearbySitters()
	{
		$nearby_limit = 200000; //in meters
		
		$from = $_POST['latlon'];
		
		
		
		
		//$from = urlencode($from);
				
		$user_id = $_POST['user_id'];

		$session_response = "INVALID";
		

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$location_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$location_data['response']['sitters'] = array();
		if($session_response == "OK")
		{
			$vets = $this->statics_model->getSitters()->result();
			
			foreach($vets as $vet)
			{
				
				if($vet->id != $user_id)
				{
					$to = $vet->geo_location;
					
					if($to != NULL && $to != "")
					{
						
						$to_arr = explode(",", $to);
						$from_arr = explode(",", $from);
						
						$from_lat = $from_arr[0];
						$from_lon = trim($from_arr[1]);
						$to_lat = $to_arr[0];
						$to_lon = trim($to_arr[1]);
						
						
						$distance = $this->distance($to_lat, $to_lon, $from_lat, $from_lon, "K");
						$distance = intval($distance*1000);
						$time = 0;
						
						
						if($distance <= $nearby_limit)
							{
							
								
								$city = $vet->city;				
								if($city == NULL)
								{
									$city = "";
								}
								
								$address = $vet->address;				
								if($address == NULL)
								{
									$address = "";
								}
								
								$phone = $vet->phone;				
								if($phone == NULL)
								{
									$phone = "";
								}
								
								$geo_location = $vet->geo_location;				
								if($geo_location == NULL)
								{
									$geo_location = "";
								}
								
								$times = $vet->sitter_availability;				
								if($times == NULL)
								{
									$times = "";
								}
								
								$search_item = array('id' => $vet->id,
													'firstname' => $vet->first_name,
													'lastname' => $vet->last_name,
													'email' => $vet->email,
													'phone' => $phone,
													'times' => $times,
													'featured' => $vet->featured,
													'city' => $city,
													'address' => $address,
													'geo_location' => $geo_location,
													'profilepicture' => site_url('items/uploads/profilepictures/')."/".$vet->profile_picture,
													'distance' => (string)$distance,
													'time' => (string)$time);
														
								$location_data['response']['sitters'][] = $search_item;	
							
								
								
								
							}
						
					/*	$to = urlencode($to);
						
						$data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=".$from."&destinations=".$to."&language=en-EN&sensor=false");
						$data = json_decode($data);
						
						$distance = 0;
						$time = 0;
						
						if($data->rows[0]->elements[0]->status != "ZERO_RESULTS")
						{
							foreach($data->rows[0]->elements as $road) {
							 	$time += $road->duration->value;	   
							    $distance = $road->distance->value;
							}
							
							$time = round($time/60);
							
							
							if($distance <= $nearby_limit)
							{
							
								
								$city = $vet->city;				
								if($city == NULL)
								{
									$city = "";
								}
								
								$address = $vet->address;				
								if($address == NULL)
								{
									$address = "";
								}
								
								$phone = $vet->phone;				
								if($phone == NULL)
								{
									$phone = "";
								}
								
								$geo_location = $vet->geo_location;				
								if($geo_location == NULL)
								{
									$geo_location = "";
								}
								
								$times = $vet->sitter_availability;				
								if($times == NULL)
								{
									$times = "";
								}
								
								$search_item = array('id' => $vet->id,
													'firstname' => $vet->first_name,
													'lastname' => $vet->last_name,
													'email' => $vet->email,
													'phone' => $phone,
													'times' => $times,
													'featured' => $vet->featured,
													'city' => $city,
													'address' => $address,
													'geo_location' => $geo_location,
													'profilepicture' => site_url('items/uploads/profilepictures/')."/".$vet->profile_picture,
													'distance' => (string)$distance,
													'time' => (string)$time);
														
								$location_data['response']['sitters'][] = $search_item;	
							
								
								
								
							}
						
						} */
						
					}
				}
			}
		}
		else
		{
			$search_item = array('id' => "00",
									'firstname' => "-",
									'lastname' => "-",
									'email' => "-",
									'phone' => "-",
									'times' => "-",
									'featured' => "-",
									'city' => "-",
									'address' => "-",
									'geo_location' => "-",
									'profilepicture' => "-",
									'distance' => "0",
									'time' => "0");
									  
			$location_data['response']['sitters'][] = $search_item;					  	
		}
		
		$json_data = json_encode($location_data, JSON_PRETTY_PRINT);
		echo $json_data;
		
				
	}

	public function accept_activity()
	{
		$user_id = $_POST['user_id'];

		$session_response = "INVALID";

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$search_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$search_data['response']['response'] = array();	
		if($session_response == "OK")
		{
			$activity_id = $_POST['activity_id'];
	
			$myDogs = $this->car_model->getVehicles($user_id)->result();	
			foreach($myDogs as $car){
			$myInvites = $this->statics_model->getMyInvites($activity_id, $car->id)->result();
			$numInvites = count($myInvites);
			
			
			
			if($numInvites>0)
			{
				foreach($myInvites as $invite){
					$data = array(
						'id' =>  $invite->id,				
						'pending' => 0,
						'accepted' => 1);
						
					$this->statics_model->editInvites($data);
				}
				$search_item = array('id' => $activity_id,
										'status' => "OK");
			}
			else 
			{
				$search_item = array('id' => "00",
										'status' => "NO INVITES");
			}
				
										
				$search_data['response']['response'][] = $search_item;	
			}	
				
		}
		else
		{
			$search_item = array('id' => '00',
										'status' => "INVALID SESSION");
									  
			$search_data['response']['response'] = $search_item;					  	
		}
		
		$json_data = json_encode($search_data, JSON_PRETTY_PRINT);
		echo $json_data;	
	}

	public function decline_activity()
	{
		$user_id = $_POST['user_id'];
		
		$session_response = "INVALID";

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$search_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$search_data['response']['response'] = array();	
		if($session_response == "OK")
		{
			$activity_id = $_POST['activity_id'];
			
			$myDogs = $this->car_model->getVehicles($user_id)->result();	
			foreach($myDogs as $car){
			$myInvites = $this->statics_model->getMyInvites($activity_id, $car->id)->result();
			
			foreach($myInvites as $invite){
				$data = array(
					'id' =>  $invite->id,				
					'pending' => 0,
					'accepted' => 0);
					
				$this->statics_model->editInvites($data);
			}
			
			
		
				$search_item = array('id' => $activity_id,
										'status' => "OK");
										
				$search_data['response']['response'][] = $search_item;	
			}	
				
		}
		else
		{
			$search_item = array('id' => '00',
										'status' => "INVALID SESSION");
									  
			$search_data['response']['response'] = $search_item;					  	
		}
		
		$json_data = json_encode($search_data, JSON_PRETTY_PRINT);
		echo $json_data;	
	}


	public function nearbyZones()
	{
		$nearby_limit = 200000; //in meters
		
		$from = $_POST['latlon'];

	//	$from = urlencode($from);
				
		$user_id = $_POST['user_id'];

		$session_response = "INVALID";
		

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$location_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$location_data['response']['zones'] = array();
		if($session_response == "OK")
		{
			$vets = $this->statics_model->getZones()->result();
		
			foreach($vets as $vet)
			{
				
				if($vet->id != $user_id)
				{
					$to = $vet->geo_location;
					
					if($to != NULL && $to != "")
					{
					
					$to_arr = explode(",", $to);
					$from_arr = explode(",", $from);
					
					$from_lat = $from_arr[0];
					$from_lon = trim($from_arr[1]);
					$to_lat = $to_arr[0];
					$to_lon = trim($to_arr[1]);
					
					
					$distance = $this->distance($to_lat, $to_lon, $from_lat, $from_lon, "K");
					$distance = intval($distance*1000);
					$time = 0;
					
					if($distance <= $nearby_limit)
					{
						$table = "dogzone_ratings";
						$column = "dogzone_id";
						$ratings = $this->statics_model->getRatings($vet->id,$user_id,$table,$column)->result();
						$rows = count($ratings);
						$ratingSum = 0;
						foreach($ratings as $i=>$row){
							$ratingSum += $row->rating;
						}
						if($rows == 0){
							$ratingSum = "0";
						}
						else{
							$ratingSum = $ratingSum/$rows;	
						}
						
						$ratingSum = round($ratingSum, 2);
						$number_of_votes = $rows;
						$already_rated = $this->statics_model->checkRating($vet->id,$user_id,$table,$column);
						
						$description = nl2br($vet->description);			
						$description = str_replace(array("\r", "\n", "\t"), "", $description);
						$search_item = array('id' => $vet->id,
											'name' => $vet->name,
											'description' => $description,
											'city' => $vet->city,
											'address' => $vet->address,
											'size' => (string)$vet->size,
											'geo_location' => $vet->geo_location,
											'distance' => (string)$distance,
											'time' => (string)$time,
											'avg_rating' => (string)$ratingSum,
											'num_votes' => (string)$number_of_votes,
											'current_user_rated' => $already_rated);
												
						$location_data['response']['zones'][] = $search_item;	
					
						
						
						
					}
					
					
					
					
					/*
						
						$to = urlencode($to);
				
						$data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=".$from."&destinations=".$to."&language=en-EN&sensor=false");
						$data = json_decode($data);
						
						$distance = 0;
						$time = 0;
						
						
						if($data->rows[0]->elements[0]->status != "ZERO_RESULTS")
						{
							
							foreach($data->rows[0]->elements as $road) {
							 	$time += $road->duration->value;	   
							    $distance = $road->distance->value;
							    
							   
							}
							
							$time = round($time/60);
							
							if($distance != 0)
							{
							
								if($distance <= $nearby_limit)
								{
									$table = "dogzone_ratings";
									$column = "dogzone_id";
									$ratings = $this->statics_model->getRatings($vet->id,$user_id,$table,$column)->result();
									$rows = count($ratings);
									$ratingSum = 0;
									foreach($ratings as $i=>$row){
										$ratingSum += $row->rating;
									}
									if($rows == 0){
										$ratingSum = "0";
									}
									else{
										$ratingSum = $ratingSum/$rows;	
									}
									$number_of_votes = $rows;
									$already_rated = $this->statics_model->checkRating($vet->id,$user_id,$table,$column);
									
									$description = strip_tags(nl2br($vet->description));			
									$description = str_replace(array("\r", "\n", "\t"), "", $description);
									$search_item = array('id' => $vet->id,
														'name' => $vet->name,
														'description' => $description,
														'city' => $vet->city,
														'address' => $vet->address,
														'size' => $vet->size,
														'geo_location' => $vet->geo_location,
														'distance' => (string)$distance,
														'time' => (string)$time,
														'avg_rating' => (string)$ratingSum,
														'num_votes' => (string)$number_of_votes,
														'current_user_rated' => $already_rated);
															
									$location_data['response']['zones'][] = $search_item;	
								
									
									
									
								}
							}
						}
						
					*/
					
					}
				}
			}
		}
		else
		{
			$search_item = array('id' => "00",
								'name' => "-",
								'description' => "-",
								'city' => "-",
								'address' => "-",
								'size' => "-",
								'geo_location' => "-",
								'distance' => "-",
								'time' => "-",
								'avg_rating' => "-",
								'num_votes' => "-",
								'current_user_rated' => "-");
									  
			$location_data['response']['zones'][] = $search_item;					  	
		}
		
		 usort($location_data['response']['zones'], function($a, $b) {
		    return $a['distance'] - $b['distance'];
		});
		$json_data = json_encode($location_data, JSON_PRETTY_PRINT);
		echo $json_data;		
	}


	public function searchZoneName()
	{

		$user_id = $_POST['user_id'];
	
		$session_response = "INVALID";

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$search_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$search_data['response']['zones'] = array();	
		if($session_response == "OK")
		{
			$name = $_POST['name'];
			
			$vets = $this->statics_model->ZoneSearchByName($name)->result();
			shuffle($vets);
			
			foreach($vets as $vet)
			{
				$table = "dogzone_ratings";
				$column = "dogzone_id";
				$ratings = $this->statics_model->getRatings($vet->id,$user_id,$table,$column)->result();
				$rows = count($ratings);
				$ratingSum = 0;
				foreach($ratings as $i=>$row){
					$ratingSum += $row->rating;
				}
				if($rows == 0){
					$ratingSum = "0";
				}
				else{
					$ratingSum = $ratingSum/$rows;	
				}
				
				$ratingSum = round($ratingSum, 2);
				$number_of_votes = $rows;
				$already_rated = $this->statics_model->checkRating($vet->id,$user_id,$table,$column);
				
				$description = nl2br($vet->description);			
				$description = str_replace(array("\r", "\n", "\t"), "", $description);
				$search_item = array('id' => $vet->id,
									'name' => $vet->name,
									'description' => $description,
									'city' => $vet->city,
									'address' => $vet->address,
									'size' => (string)$vet->size,
									'geo_location' => $vet->geo_location,
									'avg_rating' => (string)$ratingSum,
									'num_votes' => (string)$number_of_votes,
									'current_user_rated' => $already_rated);
														
				$search_data['response']['zones'][] = $search_item;	
			}	
				
		}
		else
		{
			$search_item = array('id' => "00",
								'name' => "-",
								'description' => "-",
								'city' => "-",
								'address' => "-",
								'size' => "-",
								'geo_location' => "-",
								'avg_rating' => "-",
								'num_votes' => "-",
								'current_user_rated' => "-");

									  
			$search_data['response']['zones'] = $search_item;					  	
		}
		
		$json_data = json_encode($search_data, JSON_PRETTY_PRINT);
		echo $json_data;
	}
	
	
	function suggest_zone_description()
	{
		
		
		$user_id = $_POST['user_id'];
	
		$session_response = "INVALID";

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$search_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$search_data['response']['response'] = array();	
		if($session_response == "OK")
		{
			$zone_id = $_POST['zone_id'];
			$description = $_POST['description'];
			
			$data = array('dogzone_id' => $zone_id,
							'description' => $description,
							'ip_address' => "-");
							
			$this->statics_model->addZoneSuggestion($data);
			
			$search_item = array('id' => $zone_id,
								'status' => "OK");

									  
			$search_data['response']['response'] = $search_item;
				
		}
		else
		{
			$search_item = array('id' => "00",
								'status' => "INVALID SESSION");

									  
			$search_data['response']['response'] = $search_item;					  	
		}
		
		$json_data = json_encode($search_data, JSON_PRETTY_PRINT);
		echo $json_data;
	}
	
	
	public function rate()
	{
		
		
		$user_id = $_POST['user_id'];
	
		$session_response = "INVALID";

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$search_data['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		$search_data['response']['response'] = array();	
		if($session_response == "OK")
		{
			$rating = $_POST['rating'];
			$sitter_id = $_POST['rated_id'];
			$what = $_POST['type'];
			
			switch($what){
				case "sitter": {$table = "sitter_ratings";$column = "sitter_id";$action_id = "3";}
				break;
				case "vet": {$table = "vets_ratings";$column = "vet_id";$action_id = "4";}
				break;
				case "zone": {$table = "dogzone_ratings";$column = "dogzone_id";$action_id = "5";}
				break;
				default: {$table = "";$column = "";}
				break;
			}
			
			
			
			if(!$this->statics_model->checkRating($sitter_id,$user_id,$table,$column)){
				$entity_id = $sitter_id;
				$event_id = $this->statics_model->addEvent($action_id, $entity_id);		
				$cars = $this->car_model->getVehicles($user_id);
				
				foreach($cars->result() as $car){
					$this->statics_model->addEventDog($car->id,$event_id);
				}
				
				
				$this->statics_model->rate($sitter_id,$user_id,$rating,$table,$column);
				
				$search_item = array('id' => $user_id,
								'status' => "OK");

									  
				$search_data['response']['response'] = $search_item;
			}
			else
			{
				$search_item = array('id' => $user_id,
								'status' => "ALREADY RATED");

									  
				$search_data['response']['response'] = $search_item;
			}
			
				
		}
		else
		{
			$search_item = array('id' => "00",
								'status' => "INVALID SESSION");

									  
			$search_data['response']['response'] = $search_item;					  	
		}
		
		$json_data = json_encode($search_data, JSON_PRETTY_PRINT);
		echo $json_data;
		

	}	
	
	
	
	public function upload_user_profilepicture(){
		
		$user_id = $_POST['user_id'];
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
			
			// if file to upload is selected upload
			if($_FILES['image']['size'] != 0 && $_FILES['image']['error'] == 0)	
			{
				$uploadconfig['upload_path'] = 'items/uploads/profilepictures';
				$uploadconfig['allowed_types'] = 'gif|jpg|png';
				$uploadconfig['encrypt_name'] = TRUE;				
				$this->upload->initialize($uploadconfig);			
				$this->upload->do_upload('image');
				$image_data = $this->upload->data();
				
				$fname = $image_data['file_name'];

				$container_item = array("id" => $user_id,
										"url" => site_url('items/uploads/profilepictures')."/".$fname,
															"status" => "OK");
				$container['response']['status'][] = $container_item;
				
				$data = array('id' => $user_id,
								'profile_picture' =>$fname);
				$this->statics_model->updateLocation($data);
			}	
			else{
				$container_item = array("id" => $user_id,
										"url" => "-",
										"status" => "ERROR");
				$container['response']['status'][] = $container_item;
			}	
		}
		else
		{
			$container['response']['status'][] = array("id" => "00",
														"url" => "-",
														"status" => "INVALID SESSION");
		}
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
	}
	
	
	
	
	public function check_in_vet(){
		
		$user_id = $_POST['user_id'];
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
			
			$entity_id = $_POST['vet_id'];
			$mydog_id = $_POST['dog_id'];
				   	 	
	   	 	$event_id = $this->statics_model->addEvent("11", $entity_id);		
			$this->statics_model->addEventDog($mydog_id,$event_id);
				
			
			$container_item = array("id" => $mydog_id,
											"status" => "OK");
			$container['response']['response'][] = $container_item;
				
		}
		else
		{
			$container['response']['response'][] = array("id" => "00",
														"status" => "INVALID SESSION");
		}
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
	}
	
	
	
	public function check_in_zone(){
		
		$user_id = $_POST['user_id'];
		
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
			
			$entity_id = $_POST['zone_id'];
			$mydog_id = $_POST['dog_id'];
				   	 	
	   	 	$event_id = $this->statics_model->addEvent("12", $entity_id);		
			$this->statics_model->addEventDog($mydog_id,$event_id);
				
			
			$container_item = array("id" => $mydog_id,
											"status" => "OK");
			$container['response']['response'][] = $container_item;
				
		}
		else
		{
			$container['response']['response'][] = array("id" => "00",
														"status" => "INVALID SESSION");
		}
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
	}
	
	
	
	
	
	function add_comment()
	{
		
		$user_id = $_POST['user_id'];
		$pic_id = $_POST['picture_id'];
		$comment = $_POST['comment'];
		
	
		
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
			
			
			$comment = strip_tags($comment);
			
			$data = array('image_id' => $pic_id,
							'user_id' => $user_id,
							'comment' => $comment);
							
			$this->statics_model->addPhotoComment($data);
			$cars = $this->car_model->getVehicles($user_id);
		
				 $event_id = $this->statics_model->addEvent(14, $pic_id);
				 foreach($cars->result() as $car){
				 	$mydog_id = $car->id;
					 $this->statics_model->addEventDog($mydog_id,$event_id);
				 }
			
			
			$container_item = array("id" => $pic_id,
											"status" => "OK");
			$container['response']['response'][] = $container_item;
				
		}
		else
		{
			$container['response']['response'][] = array("id" => "00",
														"status" => "INVALID SESSION");
		}
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
		
	}
	
	
	
	function get_photo_comments()
	{
		$pic_id = $_POST['picture_id'];
		$user_id = $_POST['user_id'];		
		
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
			$comments = $this->statics_model->getPhotoComments($pic_id)->result();
			$data = array();
			foreach($comments as $comment)
			{
				
				$firstname = $this->statics_model->getUserData($comment->user_id)->row()->first_name;
				$lastname = $this->statics_model->getUserData($comment->user_id)->row()->last_name;
			
				$username = $firstname." ".$lastname; 				
				$container_item = array('username' => $username,
								'comment' => $comment->comment,
								'created_date' => $comment->created_date);
								
				$container['response']['comments'][] = $container_item;
			}
			
			
			
			
				
		}
		else
		{
			
			$container_item = array('username' => "-",
								'comment' => "-",
								'created_date' => "-");
								
			
			$container['response']['comments'][] = $container_item;
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;

		
	}
	
	
	function get_photo_likes()
	{
		
		
		$pic_id = $_POST['picture_id'];
		$user_id = $_POST['user_id'];	
		
			
		
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
				
			$likes = $this->statics_model->getPicture($pic_id)->row()->likes;
			$container_item = array('id' => $pic_id,
								'likes' => $likes);
								
			$container['response']['image'][] = $container_item;

				
		}
		else
		{
			
			$container_item = array('id' => "00",
								'likes' => "-");
								
			
			$container['response']['image'][] = $container_item;
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
	}
	
	


	public function delete_photo()
	{
		

		$image_id = $_POST['picture_id'];
		$user_id = $_POST['user_id'];	
		
			
		
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
			$pic = $this->statics_model->getPicture($image_id)->row();
		
			$cover = $pic->cover_image;
			$profile = $pic->profile_image;
			
			$event_type = 8; //Regular image;
			
			if($cover == 1)
			{
				$event_type = 7; //Cover image;
			}
			else if($profile == 1)
			{
				$event_type = 6; //Cover image;
			}
				
			$event_id = $this->statics_model->getEventId($image_id, $event_type)->row()->id;	
			$this->statics_model->deleteEventImages($image_id, $event_id, $event_type);
			
			$container_item = array('id' => $image_id,
								'status' => "REMOVED");
								
			$container['response']['image'][] = $container_item;

				
		}
		else
		{
			
			$container_item = array('id' => "00",
								'status' => "INVALID SESSION");
								
			
			$container['response']['image'][] = $container_item;
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
		
	}
	
	
	public function send_invite_mail(){
		
		
		$user_id = $_POST['user_id'];	
	
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
				
			$email = $_POST['email'];
			
			$user = $this->statics_model->getUserData($user_id)->row();
			$mailname = $user->first_name." ".$user->last_name;
			
						
			$this->load->library('My_phpmailer');
			$mail = new PHPMailer(true);
			$subject = 'Snoopstr invite';
			$name = 'Snoopstr';		
			$data['mailname'] = $mailname;
			$body = $this->load->view('mail/invite_mail', $data, true);
			
			try {
				$mail->IsSMTP(); // we are going to use SMTP
		        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server 
		        $mail->SMTPDebug = 1;  // prefix for secure protocol to connect to the server 
		        $mail->Host       = "smtp.easyname.eu";      // setting SMTP server
		        $mail->Port       = 465;                   // SMTP port to connect to 
		        $mail->Username   = "1590mail27";  // user email address
		        $mail->Password   = "Kksnoopstr";            // password 
		        $mail->SMTPAuth   = true; // enabled SMTP authentication
		        $mail->CharSet = 'UTF-8';
		        $mail->SetFrom('noreply@snoopstr.com', 'Snoopstr');  //Who is sending the email
		        $mail->AddReplyTo("noreply@snoopstr.com","Snoopstr");  //email address that receives the response
				$mail->AddAddress($email);		
				$mail->IsMail();	
				$mail->From     = 'info@snoopstr.com';	
				$mail->FromName = 'Snoopstr';	
				$mail->IsHTML(true);	
				$mail->Subject  =  $subject;	
				$mail->Body     =  $body;	
				$mail->Send();
				$container_item = array('id' => $user_id,
								'status' => "SENT",
								'errormsg' => "-");
								
				$container['response']['mail'][] = $container_item;
			
			
			}
			catch (phpmailerException $e) {
				$container_item = array('id' => $user_id,
								'status' => "ERROR",
								'errormsg' => $e->errorMessage());
								
				$container['response']['mail'][] = $container_item;
			}
			

				
		}
		else
		{
			
			$container_item = array('id' => "00",
								'status' => "INVALID SESSION",
								'errormsg' => "-");
								
			
			$container['response']['mail'][] = $container_item;
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
		
		
		
		
		
	}
	
		
	
	function distance($lat1, $lon1, $lat2, $lon2, $unit) {

	  $theta = $lon1 - $lon2;
	
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	
	  $dist = acos($dist);
	
	  $dist = rad2deg($dist);
	
	  $miles = $dist * 60 * 1.1515;
	
	  $unit = strtoupper($unit);
	
	 
	
	  if ($unit == "K") {
	
	    return ($miles * 1.609344);
	
	  } else if ($unit == "N") {
	
	      return ($miles * 0.8684);
	
	    } else {
	
	        return $miles;
	
	      }
	
	}
	
	
	
	public function reportImage()
	{
		$image_id = $_POST['image_id'];
		$user_id = $_POST['user_id'];	
			
		
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
			
			$this->send_mail_report('image', $user_id, $image_id);		
			/*$container_item = array('id' => $this_dog,
								'status' => "REMOVED");
								
			$container['response']['connection'][] = $container_item;*/

				
		}
		else
		{
			
		/*	$container_item = array('id' => "00",
								'status' => "INVALID SESSION");
								
			
			$container['response']['connection'][] = $container_item;*/
		}
		
	/*	$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
		*/

	}
	
	public function reportProfile()
	{
		$profile_id = $_POST['profile_id'];
		$user_id = $_POST['user_id'];	
			
		
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
			
			$this->send_mail_report('profile', $user_id, $profile_id);		
			/*$container_item = array('id' => $this_dog,
								'status' => "REMOVED");
								
			$container['response']['connection'][] = $container_item;*/

				
		}
		else
		{
			
		/*	$container_item = array('id' => "00",
								'status' => "INVALID SESSION");
								
			
			$container['response']['connection'][] = $container_item;*/
		}
		
	/*	$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
		*/
	
		
		
	}

	
	
	public function getStoredCountries()
	{
		$container = array();
		$countrydata = $this->auth_model->getCountries()->result();
		foreach($countrydata as $country)
		{
			$container['response']['countries'][] = array("id" => $country->id,
													"name" => $country->name);
		}
		

		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;

	}
	
	public function getStoredStates()
	{
		$container = array();
		$countrydata = $this->auth_model->getStates()->result();
		foreach($countrydata as $country)
		{
			$container['response']['countries'][] = array("id" => $country->abbreviation,
													"name" => $country->name);
		}
		

		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;

	}
	
	
	
	
	
	public function message_overview()
	{
		$user_id = $_POST['user_id'];	
					
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
			$messages = $this->statics_model->getMessages($user_id);
			$messages_array = array();
			$userlist = array();
			$data['new_message_counter'] = 0;
			
			if($messages != false)
			{
				foreach($messages->result() as $message)
				{
					if($message->has_seen == 0 && $message->sender_id != $user_id)
					{
						$data['new_message_counter']++;
					}
					
					$message_text = $message->message;
					
					
					$sender = $this->statics_model->getUserData($message->sender_id)->row();
					$receiver = $this->statics_model->getUserData($message->receiver_id)->row();
					
					if($sender->id != $user_id)
					{
						$name = $sender->first_name." ".$sender->last_name;
						$profile = $sender->profile_picture;
						$partner_id = $sender->id;
					}
					else 
					{
						$name = $receiver->first_name." ".$receiver->last_name;
						$profile = $receiver->profile_picture;
						$partner_id = $receiver->id;
					}
					
			
					if($profile === NULL || $profile == "")
					{
						$profile = "default.png";
					}
				
					$message_item = array('id' => $partner_id,
											'message' => $message_text,
											'message_id' => $message->id,
											'name' => $name,
											'date' => $message->sent_time,
											'profile' => site_url('items/uploads/profilepictures')."/".$profile);
											
					if(!in_array($sender->id, $userlist) && !in_array($receiver->id, $userlist) )
					{
						$userlist[] = $partner_id;
						$container['response']['partners'][] = $message_item;
						
						
						
						$messages_array[] = $message_item;
					}						
					
				}
									
			}
				
		}
		else
		{
			
			$message_item = array('id' => '00',
									'message' => '-',
									'message_id' => '-',
									'name' => '-',
									'date' => '-',
									'profile' => '-');
								
			
			$container['response']['partners'][] = $container_item;
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;


				
	}
	
	
	
	
	public function show_conversation()
	{

		$partner_id = $_POST['partner_id'];
		$user_id = $_POST['user_id'];
					
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
			$container['response']['messages'] = array();
			$messages = $this->statics_model->getMessagesFromUser($partner_id, $user_id)->result();
		
			foreach($messages as $message)
			{
				
				
				$sender = $this->statics_model->getUserData($message->sender_id)->row();
				$receiver = $this->statics_model->getUserData($message->receiver_id)->row();
				$name = $sender->first_name." ".$sender->last_name;
				$profile = $sender->profile_picture;
				
				
				
				if($sender->id != $user_id)
				{
					
					$own_message = false;
				}
				else 
				{
					
					$own_message = true;
				}
				
			
				
				
				
				$message_item = array('id' => $message->id,
									'own_message' => $own_message,
									'message' => $message->message,
									'name' => $name,
									'date' => $message->sent_time,
									'profile' => site_url('items/uploads/profilepictures')."/".$profile);
										
				$container['response']['messages'][] = $message_item;
			}
			

							
		}
		else
		{
			
			
				$message_item = array('id' => '00',
									'own_message' => '-',
									'message' => '-',
									'name' => '-',
									'date' => '-',
									'profile' => '-');
										
				$container['response']['messages'][] = $message_item;
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;		
	}
	
	
	public function send_message()
	{
	
	
		$receiver_id = $_POST['receiver_id'];
		$sender_id = $_POST['user_id'];
					
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $sender_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
		
			$message = $_POST['message'];
		
			$data = array('sender_id' => $sender_id,
							'receiver_id' => $receiver_id,
							'message' => $message);
			
			
						
			$this->statics_model->sendMessage($data);
			
			$vehicles = $this->car_model->getVehicles($receiver_id);
			
			$action_id = 2; // message
			$event_id = $this->statics_model->addNotification($action_id, $sender_id, $receiver_id);
			foreach($vehicles->result() as $vehicle){
			 	$myvehicle_id = $vehicle->id;
				 $this->statics_model->addNotificationVehicle($myvehicle_id,$event_id);
			 }
	
			$container['response']['status'] = 'OK';				
		}
		else
		{
			
			
				
										
				$container['response']['status'] = 'INVALID';
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
		

	}
	
	
	public function poi_detail()
	{
	
		$poi_id = $_POST['poi_id'];		
		$user_id = $_POST['user_id'];
					
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{	
			$poi = $this->statics_model->getPOI($poi_id);
					
			$geo_location = $poi->geo_location;
			$arr = explode(", ", $geo_location);
			
			$lat = $arr[0];
			$lon = $arr[1];
			
			
			
			$image_array = array();
			
			$images = $this->statics_model->getPoiImages($poi_id);
			
			
			foreach($images as $image)
			{
				$image_array[] = array('id' => (string)$image['id'],
										'url' => site_url('items/uploads/images')."/".$image['fname']);
			}
			
			
			$ratings = $this->statics_model->getRatings($poi_id,$user_id,'poi_ratings','poi_id')->result();
			$rows = count($ratings);
			$ratingSum = 0;
			foreach($ratings as $i=>$row){
				$rating += $row->rating;
			}
			
			if($rows == 0){
				$ratingSum = "";
			}
			else{
				$ratingSum = $rating/$rows;	
			}
			
			
			$ratingSum = round($ratingSum, 0 ,PHP_ROUND_HALF_DOWN);
			
			
			$already_rated = $this->statics_model->checkRating($poi_id,$user_id,'poi_ratings','poi_id');
			
			$already = 1;
			
			if(!$already_rated)
			{
				$already = 0;
			}
									
			$data = array('id' => $poi->id,
							'address' => $poi->address,
							'lat' => (string)$lat,
							'lon' => (string)$lon,
							'description' => $poi->description,
							'votes' => (string)$rows,
							'rating' => (string)$ratingSum,
							'already_voted' => (string)$already,
							'images' => $image_array);
			
			
			
	
			$container['response']['poi'] = $data;				
		}
		else
		{
			$data = array('id' =>'',
							'address' => '',
							'lat' => '',
							'lon' => '',
							'description' => '',
							'votes' => '',
							'rating' => '',
							'already_voted' => '',
							'images' => '');
			
			
			
	
			$container['response']['poi'] = $data;
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;	
							
	}
	
	
	public function event_detail()
	{
			
		/*$_POST['event_id'] = 36;		
		$_POST['user_id'] = 99;
		$_POST['event_type'] = 'user';
		$_POST['vehicle_id'] = 6;	
			*/
			

		$event_id = $_POST['event_id'];		
		$user_id = $_POST['user_id'];
		$event_type = $_POST['event_type'];
		$sent_vehicle_id = $_POST['vehicle_id'];
					
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{	
			
			if($event_type == "user")
			{
				$event = $this->statics_model->getUserEventById($event_id); //user
				$vehicles_joined = $this->statics_model->getVehiclesFromUserEvent($event_id);
				$checkVehicle = $this->statics_model->checkVehicleAtUserEvent($event_id, $sent_vehicle_id);
				
			}
			else
			{
				$event = $this->statics_model->getEventById($event_id); //group
				$vehicles_joined = $this->statics_model->getVehiclesFromEvent($event_id);
				$checkVehicle = $this->statics_model->checkVehicleAtEvent($event_id, $sent_vehicle_id);
			}
			
			$vehicle_id = $event->vehicle_id;
			$geo_location = $event->geo_location;
			$arr = explode(", ", $geo_location);
			
			$lat = $arr[0];
			$lon = $arr[1];
			
					
			
			$im_admin = false;
			
			$joined_event = false;
			
			$vehicles_array = array();
			
			
			$my_vehicle_id = $vehicle_id;
			$my_cars = $this->car_model->getVehicles($user_id)->result();
				
			foreach($my_cars as $car)
			{

					if($car->id == $vehicle_id)
					{
						
						$im_admim = true;
						$my_vehicle_id = $car->id;
					}
					
					
					
				
				
			}
	
			
			
			foreach($vehicles_joined as $vid)
			{
				$vehicle = $this->car_model->getVehicle($vid->vehicle_id)->row();
				
				
				if($vehicle->id != NULL)
				{
					$profile_image = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
					$item = array(	'id' => $vehicle->id,
									'url' => site_url()."vehicle_profile/".$vehicle->pretty_url,
									'nickname' => $vehicle->nickname,
									'profile_image' => site_url('items/uploads/profilepictures')."/".$profile_image);
					
					$vehicles_array[] = $item;
				}
				
			}


			if($checkVehicle->status == 1)
			{
				$joined_event = true;
			}
	
				
			$image = "";
			
			if($event->image != NULL)
			{
				$image = site_url('items/uploads/coverpictures')."/".$event->image;
			}
			
			
			
					
												
			$data = array('id' => $event->id,
							'address' => $event->address,
							'lat' => (string)$lat,
							'lon' => (string)$lon,
							'description' => $event->description,
							'title' => $event->title,
							'start_time' => $event->start_time,
							'image' => $image,
							'admin' => $im_admin,
							'joined_event' => $joined_event,
							'vehicles' => $vehicles_array);
			
			
			
	
			$container['response']['event'] = $data;				
		}
		else
		{
													
			$data = array('id' => '',
							'address' => '',
							'lat' => '',
							'lon' => '',
							'description' => '',
							'title' => '',
							'start_time' => '',
							'image' => '',
							'admin' => '',
							'joined_event' => '',
							'vehicles' => '');
			
			
			
	
			$container['response']['event'] = $data;
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
			
	}
	
	
	
	
	public function get_mod_types()
	{
		
		$vehicle_type = $_POST['vehicle_type'];	
		$engine_type = $_POST['engine_type'];
		$user_id = $_POST['user_id'];
								
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{	
			switch($vehicle_type)
			{
				case "car":
				{
					if($engine_type == "gas") // GAS CAR
					{
						$data = array('value' => 'audio',
									'name' => 'Audio');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'body',
									'name' => 'Body');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'brake',
									'name' => 'Brakes');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'chassis',
									'name' => 'Chassis');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'cooling',
									'name' => 'Cooling System');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'drive_axle',
									'name' => 'Drive Axle');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'engine',
									'name' => 'Engine');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'exhaust',
									'name' => 'Exhaust System');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'fuel',
									'name' => 'Fuel System');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'ignition',
									'name' => 'Ignition System');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'lighting',
									'name' => 'Lighting');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'suspension',
									'name' => 'Suspension');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'transmission',
									'name' => 'Transmission');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'tuned',
									'name' => 'Tuned');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'wheels',
									'name' => 'Wheels & Tires');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'other',
									'name' => 'Other');
			
						$container['response']['mods'][] = $data;
					}
					else //DIESEL CAR
					{	
						$data = array('value' => 'air_intake',
									'name' => 'Air Intake');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'audio',
									'name' => 'Audio');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'body',
									'name' => 'Body');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'brake',
									'name' => 'Brakes');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'chassis',
									'name' => 'Chassis');
			
						$container['response']['mods'][] = $data;
											
						$data = array('value' => 'drive_axle',
									'name' => 'Drive Axle');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'engine',
									'name' => 'Engine');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'exhaust',
									'name' => 'Exhaust System');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'fuellift_pump',
									'name' => 'Fuel Lift Pump');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'injection_pump',
									'name' => 'Injection Pump');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'injector',
									'name' => 'Injector');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'intercooler',
									'name' => 'Intercooler');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'lighting',
									'name' => 'Lighting');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'suspension',
									'name' => 'Suspension');
												
						$data = array('value' => 'transmission',
									'name' => 'Transmission');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'tuned',
									'name' => 'Tuned');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'turbo',
									'name' => 'Turbo');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'wheels',
									'name' => 'Wheels & Tires');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'other',
									'name' => 'Other');
			
						$container['response']['mods'][] = $data;
					}				
				};break;
				
				
				//MOTORCYCLE
				case "motor":
				{
					$data = array('value' => 'air_intake',
									'name' => 'Air Intake');
			
						$container['response']['mods'][] = $data;
								
						
						$data = array('value' => 'body',
									'name' => 'Body');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'brake',
									'name' => 'Brakes');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'engine',
									'name' => 'Engine');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'exhaust',
									'name' => 'Exhaust System');
			
						$container['response']['mods'][] = $data;
																
						$data = array('value' => 'lighting',
									'name' => 'Lighting');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'suspension',
									'name' => 'Suspension');
												
						$data = array('value' => 'transmission',
									'name' => 'Transmission');
			
						$container['response']['mods'][] = $data;

						$data = array('value' => 'wheels',
									'name' => 'Wheels & Tires');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'other',
									'name' => 'Other');
			
						$container['response']['mods'][] = $data;
				};break;
				
				
				
				case "truck":
				{
					if($engine_type == "gas") // GAS TRUCK
					{
						$data = array('value' => 'audio',
									'name' => 'Audio');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'body',
									'name' => 'Body');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'brake',
									'name' => 'Brakes');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'chassis',
									'name' => 'Chassis');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'cooling',
									'name' => 'Cooling System');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'drive_axle',
									'name' => 'Drive Axle');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'engine',
									'name' => 'Engine');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'exhaust',
									'name' => 'Exhaust System');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'fuel',
									'name' => 'Fuel System');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'ignition',
									'name' => 'Ignition System');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'lighting',
									'name' => 'Lighting');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'suspension',
									'name' => 'Suspension');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'transmission',
									'name' => 'Transmission');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'tuned',
									'name' => 'Tuned');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'wheels',
									'name' => 'Wheels & Tires');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'other',
									'name' => 'Other');
			
						$container['response']['mods'][] = $data;
					}
					else //DIESEL TRUCK
					{	
						$data = array('value' => 'air_intake',
									'name' => 'Air Intake');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'audio',
									'name' => 'Audio');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'body',
									'name' => 'Body');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'brake',
									'name' => 'Brakes');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'chassis',
									'name' => 'Chassis');
			
						$container['response']['mods'][] = $data;
											
						$data = array('value' => 'drive_axle',
									'name' => 'Drive Axle');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'engine',
									'name' => 'Engine');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'exhaust',
									'name' => 'Exhaust System');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'fuellift_pump',
									'name' => 'Fuel Lift Pump');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'injection_pump',
									'name' => 'Injection Pump');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'injector',
									'name' => 'Injector');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'intercooler',
									'name' => 'Intercooler');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'lighting',
									'name' => 'Lighting');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'suspension',
									'name' => 'Suspension');
												
						$data = array('value' => 'transmission',
									'name' => 'Transmission');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'tuned',
									'name' => 'Tuned');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'turbo',
									'name' => 'Turbo');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'wheels',
									'name' => 'Wheels & Tires');
			
						$container['response']['mods'][] = $data;
						
						$data = array('value' => 'other',
									'name' => 'Other');
			
						$container['response']['mods'][] = $data;
					}
				};break;
				
			}
										
							
		}
		else
		{
													
			$data = array('name' => '',
							'value' => '');
			
			
			
	
			$container['response']['mods'][] = $data;
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
			
	}
	
	
	
	
	public function get_sub_mod_types()
	{

		$vehicle_type = $_POST['vehicle_type'];	
		$engine_type = $_POST['engine_type'];
		$user_id = $_POST['user_id'];
		$mod_type = $_POST['mod_type'];
						
		$session_response = "OK";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{	
			switch($vehicle_type)
			{
				case "car":
				{
					if($engine_type == "gas") // GAS CAR
					{
						switch($mod_type)
						{
							case 'body': {
								
								$data = array('value' => '2',
									'name' => 'Paint - Factory');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '3',
									'name' => 'Paint - Custom');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Body mods');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							case 'brake': {
								
								$data = array('value' => '3',
									'name' => 'Upgraded - Front - Calipers (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '5',
									'name' => 'Upgraded - Front - Rotors (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '7',
									'name' => 'Upgraded - Rear - Drum (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '9',
									'name' => 'Upgraded - Rear - Disc - Calipers (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '11',
									'name' => 'Upgraded - Rear - Disc - Rotors (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							
							case 'chassis': {
								
								$data = array('value' => '2',
									'name' => 'Aftermarket');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '3',
									'name' => 'Bracing');
			
								$container['response']['mods'][] = $data;
								
								
							};break;
							
							case 'drive_axle': {
								
								$data = array('value' => '2',
									'name' => '2WD (Upgraded internals)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => '4WD (Upgraded internals)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '6',
									'name' => 'AWD (Upgraded internals)');
			
								$container['response']['mods'][] = $data;
								
								
							};break;
							
							case 'engine': {
								
								$data = array('value' => '2',
									'name' => 'Internals');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '3',
									'name' => 'Induction - Naturally aspirated');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Induction - Forced - Turbocharged');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '5',
									'name' => 'Induction - Forced - Supercharged (Positive displacement)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '6',
									'name' => 'Induction - Forced - Supercharged (Centrifugal)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '7',
									'name' => 'Induction - Nitrous - Dry system');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '8',
									'name' => 'Induction - Nitrous - Wet system');
			
								$container['response']['mods'][] = $data;
								
								
								
							};break;

							
							case 'exhaust': {
								
								$data = array('value' => '3',
									'name' => 'Header - Shorty');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Header - Long tube');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '6',
									'name' => 'Converter - High flow');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '7',
									'name' => 'Converter - Offroad pipe');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '9',
									'name' => 'Mid pipe - H-pipe');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '10',
									'name' => 'Mid pipe - X-pipe');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '12',
									'name' => 'Muffler - Aftermarket');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							case 'suspension': {
								
								$data = array('value' => '3',
									'name' => 'Upgraded - Front - Springs (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '5',
									'name' => 'Upgraded - Front - Shocks (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '7',
									'name' => 'Upgraded - Front - Struts (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '9',
									'name' => 'Upgraded - Rear - Springs (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '11',
									'name' => 'Upgraded - Rear - Shocks (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '13',
									'name' => 'Upgraded - Rear - Struts (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								
							};break;
							
							case 'transmission': {
								
								$data = array('value' => '2',
									'name' => 'Manual - Clutch (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Automatic - Converter (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							default: {
								$container['response']['mods'] = "INPUT";
							};break;
						}
						
						
					}
					else //DIESEL CAR
					{	
						switch($mod_type)
						{
							case 'body': {
								
								$data = array('value' => '2',
									'name' => 'Paint - Factory');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '3',
									'name' => 'Paint - Custom');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Body mods');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							case 'brake': {
								
								$data = array('value' => '3',
									'name' => 'Upgraded - Front - Calipers (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '5',
									'name' => 'Upgraded - Front - Rotors (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '7',
									'name' => 'Upgraded - Rear - Drum (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '9',
									'name' => 'Upgraded - Rear - Disc - Calipers (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '11',
									'name' => 'Upgraded - Rear - Disc - Rotors (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							
							case 'chassis': {
								
								$data = array('value' => '2',
									'name' => 'Aftermarket');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '3',
									'name' => 'Bracing');
			
								$container['response']['mods'][] = $data;
								
								
							};break;
							
							case 'drive_axle': {
								
								$data = array('value' => '2',
									'name' => '2WD (Upgraded internals)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => '4WD (Upgraded internals)');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
														
							case 'exhaust': {
								
								$data = array('value' => '2',
									'name' => 'Manifold (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Exhaust system (Aftermarket)');
			
								$container['response']['mods'][] = $data;

							};break;
							
							
							case 'suspension': {
								
								$data = array('value' => '2',
									'name' => 'Lifted');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '3',
									'name' => 'Lowered');
			
								$container['response']['mods'][] = $data;

							};break;
							
							case 'transmission': {
								
								$data = array('value' => '2',
									'name' => 'Manual - Clutch (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Automatic - Converter (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							default: {
								$container['response']['mods'] = "INPUT";
							};break;
						}					
					}				
				};break;
				
				
				//MOTORCYCLE
				case "motor":
				{
					switch($mod_type)
						{
							
							case 'engine': {
								
								$data = array('value' => '2',
									'name' => 'Internals');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '3',
									'name' => 'Forced induction');
			
								$container['response']['mods'][] = $data;

							};break;
							
							
							default: {
								$container['response']['mods'] = "INPUT";
							};break;
						}	
				};break;
				
				
				
				case "truck":
				{
					if($engine_type == "gas") // GAS TRUCK
					{
						switch($mod_type)
						{
							case 'body': {
								
								$data = array('value' => '2',
									'name' => 'Paint - Factory');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '3',
									'name' => 'Paint - Custom');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Body mods');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							case 'brake': {
								
								$data = array('value' => '3',
									'name' => 'Upgraded - Front - Calipers (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '5',
									'name' => 'Upgraded - Front - Rotors (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '7',
									'name' => 'Upgraded - Rear - Drum (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '9',
									'name' => 'Upgraded - Rear - Disc - Calipers (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '11',
									'name' => 'Upgraded - Rear - Disc - Rotors (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							
							case 'chassis': {
								
								$data = array('value' => '2',
									'name' => 'Aftermarket');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '3',
									'name' => 'Bracing');
			
								$container['response']['mods'][] = $data;
								
								
							};break;
							
							case 'drive_axle': {
								
								$data = array('value' => '2',
									'name' => '2WD (Upgraded internals)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => '4WD (Upgraded internals)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '6',
									'name' => 'AWD (Upgraded internals)');
			
								$container['response']['mods'][] = $data;
								
								
							};break;
							
							case 'engine': {
								
								$data = array('value' => '2',
									'name' => 'Internals');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '3',
									'name' => 'Induction - Naturally aspirated');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Induction - Forced - Turbocharged');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '5',
									'name' => 'Induction - Forced - Supercharged (Positive displacement)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '6',
									'name' => 'Induction - Forced - Supercharged (Centrifugal)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '7',
									'name' => 'Induction - Nitrous - Dry system');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '8',
									'name' => 'Induction - Nitrous - Wet system');
			
								$container['response']['mods'][] = $data;
								
								
								
							};break;

							
							case 'exhaust': {
								
								$data = array('value' => '3',
									'name' => 'Header - Shorty');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Header - Long tube');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '6',
									'name' => 'Converter - High flow');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '7',
									'name' => 'Converter - Offroad pipe');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '9',
									'name' => 'Mid pipe - H-pipe');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '10',
									'name' => 'Mid pipe - X-pipe');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '12',
									'name' => 'Muffler - Aftermarket');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							case 'suspension': {
								
								$data = array('value' => '3',
									'name' => 'Upgraded - Front - Springs (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '5',
									'name' => 'Upgraded - Front - Shocks (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '7',
									'name' => 'Upgraded - Front - Struts (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '9',
									'name' => 'Upgraded - Rear - Springs (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '11',
									'name' => 'Upgraded - Rear - Shocks (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '13',
									'name' => 'Upgraded - Rear - Struts (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								
							};break;
							
							case 'transmission': {
								
								$data = array('value' => '2',
									'name' => 'Manual - Clutch (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Automatic - Converter (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							default: {
								$container['response']['mods'] = "INPUT";
							};break;
						}
					}
					else //DIESEL TRUCK
					{	
						switch($mod_type)
						{
							case 'body': {
								
								$data = array('value' => '2',
									'name' => 'Paint - Factory');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '3',
									'name' => 'Paint - Custom');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Body mods');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							case 'brake': {
								
								$data = array('value' => '3',
									'name' => 'Upgraded - Front - Calipers (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '5',
									'name' => 'Upgraded - Front - Rotors (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '7',
									'name' => 'Upgraded - Rear - Drum (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '9',
									'name' => 'Upgraded - Rear - Disc - Calipers (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '11',
									'name' => 'Upgraded - Rear - Disc - Rotors (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							
							case 'chassis': {
								
								$data = array('value' => '2',
									'name' => 'Aftermarket');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '3',
									'name' => 'Bracing');
			
								$container['response']['mods'][] = $data;
								
								
							};break;
							
							case 'drive_axle': {
								
								$data = array('value' => '2',
									'name' => '2WD (Upgraded internals)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => '4WD (Upgraded internals)');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
														
							case 'exhaust': {
								
								$data = array('value' => '2',
									'name' => 'Manifold (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Exhaust system (Aftermarket)');
			
								$container['response']['mods'][] = $data;

							};break;
							
							
							case 'suspension': {
								
								$data = array('value' => '2',
									'name' => 'Lifted');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '3',
									'name' => 'Lowered');
			
								$container['response']['mods'][] = $data;

							};break;
							
							case 'transmission': {
								
								$data = array('value' => '2',
									'name' => 'Manual - Clutch (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
								$data = array('value' => '4',
									'name' => 'Automatic - Converter (Aftermarket)');
			
								$container['response']['mods'][] = $data;
								
							};break;
							
							default: {
								$container['response']['mods'] = "INPUT";
							};break;
						}
					}
				};break;
				
			}
										
							
		}
		else
		{
													
			$data = array('name' => '',
							'value' => '');
			
			
			
	
			$container['response']['mods'][] = $data;
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;
			
	}
	
	
	public function get_vehicle_mods()
	{
		$vehicle_id = $_POST['vehicle_id'];
		$user_id = $_POST['user_id'];
		
						
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		$container['response']['mods'] = array();
		if($session_response == "OK")
		{	
			$vehicle = $this->car_model->getVehicle($vehicle_id)->row();

			$car_id = $vehicle_id;
			
			$mods = array();
		
			switch($vehicle->vehicle_type)
			{
				case 0:{
							$mods = $this->car_model->getVehicleMod($vehicle->engine_type."_mods",$car_id);
							
							foreach($mods as $mod)
							{
								
								switch($mod->category)
								{
									case "audio": { $mod_type = $this->car_model->getMod("audio_types", $mod->type);};break;
									case "air_intake": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type); };break;
									case "body": { $mod_type = $this->car_model->getMod("body_types", $mod->type);};break;
									case "brake": { $mod_type = $this->car_model->getMod("brake_types", $mod->type);};break;
									case "chassis": { $mod_type = $this->car_model->getMod("chassis_types", $mod->type);};break;
									case "cooling": { $mod_type = $this->car_model->getMod("gas_coolingsystem_types", $mod->type);};break;
									case "drive_axle": { $mod_type = $this->car_model->getMod("drive_axle_types", $mod->type);};break;
									case "engine": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type);};break;
									case "exhaust": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type); };break;
									case "fuel": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."system_types", $mod->type); };break;
									case "fuellift_pump": { $mod_type = $this->car_model->getMod("diesel_fuel_lift_types", $mod->type); };break;
									case "injection_pump": { $mod_type = $this->car_model->getMod("diesel_injection_pump_types", $mod->type); };break;
									case "injector": { $mod_type = $this->car_model->getMod("diesel_injection_types", $mod->type); };break;
									case "intercooler": { $mod_type = $this->car_model->getMod("diesel_intercooler_types", $mod->type); };break;
									case "ignition": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."system_types", $mod->type); };break;
									case "lighting": { $mod_type = $this->car_model->getMod("lighting_types", $mod->type);};break;
									case "suspension": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type); };break;
									case "transmission": { $mod_type = $this->car_model->getMod("transmission_types", $mod->type);};break;
									case "tuned": { $mod_type = $this->car_model->getMod("tuned_types", $mod->type);};break;
									case "turbo": { $mod_type = $this->car_model->getMod("diesel_turbo_types", $mod->type);};break;
									case "wheels": { $mod_type = $this->car_model->getMod("wheel_types", $mod->type);};break;
									case "other": { $mod_type = $this->car_model->getMod("wheel_types", $mod->type);};break;
								}
								
								$mod_name = "";
								if($mod_type->name != 'Stock' && $mod_type->name != 'Aftermarket')
								{
									$mod_name = $mod_type->name;
								}
								
								
								$modtitle = str_replace("_", " ", $mod->category);
								$modtitle = ucfirst ($modtitle);
								
								$mod_item = array('category' => $modtitle,
													'text' => $mod_name." ".$mod->text,
													'image' => site_url('items/frontend/img/mods')."/".$mod->category.".png");
	
								$container['response']['mods'][] = $mod_item;
							}
	
					};break;
				case 1:{
						
							$mods = $this->car_model->getVehicleMod("motorcycle_mods",$car_id);
							
							
							foreach($mods as $mod)
							{
								switch($mod->category)
								{
									case "audio": { $mod_type = $this->car_model->getMod("audio_types", $mod->type); };break;
									case "body": { $mod_type = $this->car_model->getMod("motorcycle_body_types", $mod->type);};break;
									case "brake": { $mod_type = $this->car_model->getMod("motorcycle_brake_types", $mod->type);};break;
									case "chassis": { $mod_type = $this->car_model->getMod("chassis_types", $mod->type);};break;
									case "cooling": { $mod_type = $this->car_model->getMod("gas_coolingsystem_types", $mod->type);};break;
									case "drive_axle": { $mod_type = $this->car_model->getMod("drive_axle_types", $mod->type);};break;
									case "engine": { $mod_type = $this->car_model->getMod("motorcycle_engine_types", $mod->type); };break;
									case "exhaust": { $mod_type = $this->car_model->getMod("motorcycle_exhaust_types", $mod->type); };break;
									case "lighting": { $mod_type = $this->car_model->getMod("motorcycle_lighting_types", $mod->type); };break;
									case "suspension": { $mod_type = $this->car_model->getMod("motorcycle_suspension_types", $mod->type); };break;
									case "transmission": { $mod_type = $this->car_model->getMod("motorcycle_transmission_types", $mod->type); };break;
									case "wheel": { $mod_type = $this->car_model->getMod("motorcycle_wheel_types", $mod->type); };break;
									
								}
								
								$mod_name = "";
								if($mod_type->name != 'Stock' && $mod_type->name != 'Aftermarket')
								{
									$mod_name = $mod_type->name;
								}
								
								
								$modtitle = str_replace("_", " ", $mod->category);
								$modtitle = ucfirst ($modtitle);
								
								$mod_item = array('category' => $modtitle,
													'text' => $mod_name." ".$mod->text,
													'image' => site_url('items/frontend/img/mods')."/".$mod->category.".png");
	
								$container['response']['mods'][] = $mod_item;
							}
						};break;
				case 2:{
						 	
							$mods = $this->car_model->getVehicleMod($vehicle->engine_type."_mods",$car_id);
							
							
							foreach($mods as $mod)
							{
								switch($mod->category)
								{
									case "audio": { $mod_type = $this->car_model->getMod("audio_types", $mod->type);};break;
									case "air_intake": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type); };break;
									case "body": { $mod_type = $this->car_model->getMod("body_types", $mod->type);};break;
									case "brake": { $mod_type = $this->car_model->getMod("brake_types", $mod->type);};break;
									case "chassis": { $mod_type = $this->car_model->getMod("chassis_types", $mod->type);};break;
									case "cooling": { $mod_type = $this->car_model->getMod("gas_coolingsystem_types", $mod->type);};break;
									case "drive_axle": { $mod_type = $this->car_model->getMod("drive_axle_types", $mod->type);};break;
									case "engine": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type);};break;
									case "exhaust": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type); };break;
									case "fuel": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."system_types", $mod->type); };break;
									case "fuellift_pump": { $mod_type = $this->car_model->getMod("diesel_fuel_lift_types", $mod->type); };break;
									case "injection_pump": { $mod_type = $this->car_model->getMod("diesel_injection_pump_types", $mod->type); };break;
									case "injector": { $mod_type = $this->car_model->getMod("diesel_injection_types", $mod->type); };break;
									case "intercooler": { $mod_type = $this->car_model->getMod("diesel_intercooler_types", $mod->type); };break;
									case "ignition": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."system_types", $mod->type); };break;
									case "lighting": { $mod_type = $this->car_model->getMod("lighting_types", $mod->type);};break;
									case "suspension": { $mod_type = $this->car_model->getMod($vehicle->engine_type."_".$mod->category."_types", $mod->type); };break;
									case "transmission": { $mod_type = $this->car_model->getMod("transmission_types", $mod->type);};break;
									case "tuned": { $mod_type = $this->car_model->getMod("tuned_types", $mod->type);};break;
									case "turbo": { $mod_type = $this->car_model->getMod("diesel_turbo_types", $mod->type);};break;
									case "wheels": { $mod_type = $this->car_model->getMod("wheel_types", $mod->type);};break;
									case "other": { $mod_type = $this->car_model->getMod("wheel_types", $mod->type);};break;
								}
								
								$mod_name = "";
								if($mod_type->name != 'Stock' && $mod_type->name != 'Aftermarket')
								{
									$mod_name = $mod_type->name;
								}
								
								$modtitle = str_replace("_", " ", $mod->category);
								$modtitle = ucfirst ($modtitle);
								
								$mod_item = array('category' => $modtitle,
													'text' => $mod_name." ".$mod->text,
													'image' => site_url('items/frontend/img/mods')."/".$mod->category.".png");
	
								$container['response']['mods'][] = $mod_item;
							
							}				 
						};break;
			}
	
			
			
			function compareByName($a, $b) {
			  return strcmp($a["category"], $b["category"]);
			}
			usort($container['response']['mods'], 'compareByName');
			
							
							
		}
		else
		{
													
			$data = array('title' => '',
							'text' => '',
							'category' => '',
							'type' => '');

			$container['response']['mods'][] = $data;
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;

			
	}
	
	
	function add_mod()
	{
		
	/*	$_POST['mod_type'] = "engine";
		$_POST['vehicle_id'] = 12;
		$_POST['mod_text'] = "mod through api";	
		$_POST['sub_mod_type'] = 8;
		$_POST['user_id'] = 98;
	*/	
		$user_id = $_POST['user_id'];
		$vehicle_id = $_POST['vehicle_id'];		
		$column_type = $_POST['mod_type'];	
		$mod_text = $_POST['mod_text'];	
		$mod_type = $_POST['sub_mod_type'];
		
		
						
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		
		if($session_response == "OK")
		{	
			$vehicle = $this->car_model->getVehicle($vehicle_id)->row();
			
			$vehicle_type = $vehicle->vehicle_type;
			$engine_type = $vehicle->engine_type;
			
			switch($vehicle_type)
			{
				case 0: { // CAR
					
					$data = array('vehicle_id' => $vehicle_id,
									'category' => $column_type,
									'type' => $mod_type,
									'text' => $mod_text);
					
					
					$table = 'diesel_mods';
					
					if($engine_type == "gas")
					{
						$table = 'gas_mods';
					}
									
									
					
					$this->car_model->addVehicleMod($data, $table);
					
					$data = array('vehicle_id' => $vehicle_id,
									'mod_text' => $column_type,
									'mod' => $mod_text);
									
					$history_id = $this->car_model->addVehicleMod($data, 'car_mod_history');
	
				};break;
				
				case 1: { //MOTORCYCLE
					
						$data = array('vehicle_id' => $vehicle_id,
									'category' => $column_type,
									'type' => $mod_type,
									'text' => $mod_text);
						
						$this->car_model->addVehicleMod($data, 'motorcycle_mods');	
						
						
						$data = array('vehicle_id' => $vehicle_id,
									'mod_text' => $column_type,
									'mod' => $mod_text);
						
						$history_id = $this->car_model->addVehicleMod($data, 'motorcycle_mod_history');
				};break;
				
				case 2: { // TRUCK
					
					$data = array('vehicle_id' => $vehicle_id,
									'category' => $column_type,
									'type' => $mod_type,
									'text' => $mod_text);
					
					
					$table = 'diesel_mods';
					
					if($engine_type == "gas")
					{
						$table = 'gas_mods';
					}
	
					$this->car_model->addVehicleMod($data, $table);
					
					$data = array('vehicle_id' => $vehicle_id,
									'mod_text' => $column_type,
									'mod' => $mod_text);
									
					$history_id = $this->car_model->addVehicleMod($data, 'truck_mod_history');
								
				};break;
			}
			
			
			$entity_id = $vehicle_id;
			$action_id = 4; //new mod
			$event_id = $this->statics_model->addEvent($action_id, $entity_id, $history_id);
			$this->statics_model->addEventVehicle($vehicle_id,$event_id);
			
			//ADD GAME POINTS
			$this->addPoints($vehicle_id,5,'number_of_mods', 'added mod');
			
										
							
		}
		else
		{
			
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;



	}
	
	
	
	public function rate_poi()
	{
		
	/*	$_POST['rating'] = 4;
		$_POST['poi_id'] = 6;
		$_POST['user_id'] = 98;
	*/	
		
		$rating = $_POST['rating'];
		$poi_id = $_POST['poi_id'];
		$user_id = $_POST['user_id'];
		
		$action_id = 12;
		
		
						
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		
		if($session_response == "OK")
		{	
			
			if(!$this->statics_model->checkRating($poi_id,$user_id,'poi_ratings','poi_id')){
				$entity_id = $poi_id;
				$event_id = $this->statics_model->addEvent($action_id, $entity_id);		
				$vehicles = $this->car_model->getVehicles($user_id);
				
				foreach($vehicles->result() as $car){
					$this->statics_model->addEventVehicle($car->id,$event_id);
				}
	
				$this->statics_model->rate($poi_id,$user_id,$rating,'poi_ratings','poi_id');
				
				
				$container['response']['status'] = "OK"; 
			}
			else
			{
				$container['response']['status'] = "ALREADY"; 
			}	
													
		}
		else
		{
			$container['response']['status'] = "INVALID"; 
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;


	}	
	
	
	
	
	public function add_image_poi()
	{
			

		$poi_id = $_POST['poi_id'];
		$user_id = $_POST['user_id'];
		$vehicle_id = $_POST['vehicle_id'];
		
		$action_id = 12;
		
		
						
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		
		if($session_response == "OK")
		{	
			

			if($_FILES['image']['size'] != 0 && $_FILES['image']['error'] == 0)
			{
				$ret = array();
				$error = $_FILES["image"]["error"];				
					
			    	
		       	 	$fileName = $_FILES["image"]["name"];
		       	 	$this->load->library('upload');
		       	
		       	 	$uploadconfig['upload_path'] = 'items/uploads/images/';
					$uploadconfig['max_size'] = '10000000000';
					$uploadconfig['allowed_types'] = 'jpg|png|jpeg';
					$uploadconfig['encrypt_name'] = true;
						
					
					$this->upload->initialize($uploadconfig);
					
					if ( ! $this->upload->do_upload('image'))
					{
						
						 
						// uploading failed. $error will holds the errors.
					}
					
					
					$data = $this->upload->data();
					
					$this->load->library('image_moo');
					$fname = 'items/uploads/images/'.$data['file_name'];
					
					//FIX IMAGE ROTATION 
					
					
					 $exif = exif_read_data($fname);
	
			         $ort = $exif['Orientation'];
						
		            switch($ort)
		            {
		
		                case 3: // 180 rotate left
		                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
		                    break;
		                case 6: // 90 rotate right
		                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
		                    break;			
		                case 8:    // 90 rotate left
		                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
		                    break;
						default:{};
		            }
					
					
					if($data['image_width']>1000 || $data['image_height']>1000)
					{

						$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
						
					}
		       	 		
		       	 			       	 	 	
			       	 	 $ret[$fileName]= $output_dir.$data['file_name'];

						 
			       	 	 $this->statics_model->addPOIPicture($data['file_name'],$poi_id,$vehicle_id);
			
					
				$container['response']['status'] = "OK"; 	
					
			 }
			else
			{
				$container['response']['status'] = "FILE NOT FOUND"; 
			}
													
		}
		else
		{
			$container['response']['status'] = "INVALID"; 
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;	

	
	}
	
	
	
	public function follow_vehicle()
	{
		$user_id = $_POST['user_id'];
		$vehicle_id = $_POST['vehicle_id'];
		$my_vehicle_id = $_POST['my_vehicle_id'];
	
			
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		
		if($session_response == "OK")
		{	
			if(!$this->car_model->checkVehicle($vehicle_id,$my_vehicle_id))
			{
		
				$data = array(
						'vehicle_id' => $vehicle_id,
						'follower_id' => $my_vehicle_id);
			
				
				$this->car_model->followVehicle($data);
				
				
				//ADD EVENT FOR NEWSFEED
				$event_id = $this->statics_model->addEvent("10", $my_vehicle_id, $vehicle_id);
				$this->statics_model->addEventVehicle($vehicle_id,$event_id);
				//$this->statics_model->addEventVehicle($my_vehicle_id,$event_id);
				
				
				//ADD GAME POINTS
				$this->addPoints($vehicle_id,5,'followers');
				
				
				$container['response']['status'] = "OK"; 
			}
			else
			{
				$container['response']['status'] = "ALREADY"; 
			}
							
		}
		else
		{
			$container['response']['status'] = "INVALID"; 
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;		

				
	}
	
	
	public function unfollow_vehicle()
	{
			
	
				
		$user_id = $_POST['user_id'];
		$vehicle_id = $_POST['vehicle_id'];
		$my_vehicle_id = $_POST['my_vehicle_id'];
	
			
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		
		if($session_response == "OK")
		{	
			if($this->car_model->checkVehicle($vehicle_id,$my_vehicle_id))
			{
		
				$data = array(
						'vehicle_id' => $vehicle_id,
						'follower_id' => $my_vehicle_id);
			
				
				$this->car_model->removeFollowVehicle($data);

			}
		
			$container['response']['status'] = "OK";				
		}
		else
		{
			$container['response']['status'] = "INVALID"; 
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;		

				
	}
	
	
	
	public function add_story_piece()
	{
	/*	$_POST['user_id'] = 98;
		$_POST['story'] = "test";
		$_POST['vehicle_id'] = 5;
	*/		
			
		$user_id = $_POST['user_id'];
		$story  = $_POST['story'];
		$vehicle_id = $_POST['vehicle_id'];
	
			
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		
		if($session_response == "OK")
		{	
			$data = array('vehicle_id' => $vehicle_id,
					'story' => $story);
		
		
		
		
			$story_id = $this->car_model->addStoryPiece($data);
			
			
			
			
			if($_FILES['image']['size'] != 0 && $_FILES['image']['error'] == 0)	
			{
				
				$uploadconfig['upload_path'] = 'items/uploads/images';
				$uploadconfig['allowed_types'] = 'gif|jpg|png|jpeg';
				$uploadconfig['encrypt_name'] = true;	
				
				$this->upload->initialize($uploadconfig);
				
				$this->upload->do_upload('image');
				
				$data = $this->upload->data();
				
				$error = array('error' => $this->upload->display_errors());
				
				$this->load->library('image_moo');
				$fname = 'items/uploads/images/'.$data['file_name'];
					
					//FIX IMAGE ROTATION 
					$exif = exif_read_data($fname);
					$ort = $exif['Orientation'];
							
		            switch($ort)
		            {
		
		                case 3: // 180 rotate left
		                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
		                    break;
		                case 6: // 90 rotate right
		                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
		                    break;			
		                case 8:    // 90 rotate left
		                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
		                    break;
						default:{};
		            }
				
				if($data['image_width']<300 || $data['image_height']<300)
				{
					
					$newimage = $this->image_moo->load($fname)->resize(500,500,true)->save($fname,true);
					
					
					
				}
				
				
				
				if($data['image_width']>1000 || $data['image_height']>1000)
				{
					
					$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
					
				}
				
			
			$imgdata = array('fname' => $data['file_name'],
								'story_id' => $story_id);
			
			
			
			$image_id = $this->statics_model->addStoryPicture($imgdata);
			
			
		}		

			$event_id = $this->statics_model->addEvent(2, $vehicle_id, $story_id);		
			$this->statics_model->addEventVehicle($vehicle_id,$event_id);
			
			//echo $story_id;
			$container['response']['status'] = "OK";	
			
		}
		else
		{
			$container['response']['status'] = "INVALID"; 
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;	

	}
	
	
	
	public function delete_story_piece()
	{
	/*	
		$_POST['story_id'] = "7";
	
	*/		
			
		$story_id = $_POST['story_id'];
			
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $story_id,
													"status" => $session_response);
		
		
		if($session_response == "OK")
		{	

			$story_id = $this->car_model->deleteStoryPiece($story_id);

			$container['response']['status'] = "OK";	
			
		}
		else
		{
			$container['response']['status'] = "INVALID"; 
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;	

	}
	

	public function join_event()
	{
		
		$user_id = $_POST['user_id'];
		$event_id = $_POST['event_id'];
		$vehicle_id = $_POST['vehicle_id'];
		$event_type = $_POST['event_type'];
		$status = 1;
			
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		
		if($session_response == "OK")
		{	
			
			if($event_type == "user")
			{
				$checkVehicle = $this->statics_model->checkVehicleAtUserEvent($event_id, $vehicle_id);
		
				if($checkVehicle)
				{
					$data = array('status' => $status);
					$this->statics_model->updateVehicleToUserEvent($event_id,$vehicle_id,$data);
				}
				
				else
				{
					$data = array('user_event_id' => $event_id,
								'vehicle_id' => $vehicle_id,
								'status' => $status);
								
					$this->statics_model->addVehicleToUserEvent($data);
				}
				
				if($status == 1)
				{
					$saved_event_id = $this->statics_model->addNotification(9, $vehicle_id, $event_id);
					
					
					$user_event = $this->statics_model->getUserEventById($event_id);
					
					$this->statics_model->addNotificationVehicle($user_event->vehicle_id,$saved_event_id);	
					
					
					//ADD GAME POINTS
					$this->addPoints($user_event->vehicle_id,5,'event_participants', 'event joined');
					
				}
				
	
				$container['response']['status'] = "OK";
			}
			else if($event_type == "group")
			{
				$checkVehicle = $this->statics_model->checkVehicleAtEvent($event_id, $vehicle_id);
		
				if($checkVehicle)
				{
					$data = array('status_id' => $status);
					$this->statics_model->updateVehicleToEvent($event_id,$vehicle_id,$data);
				}
				
				else
				{
					$data = array('event_id' => $event_id,
								'vehicle_id' => $vehicle_id,
								'status_id' => $status);
								
					$this->statics_model->addVehicleToEvent($data);
				}
				
				if($status == 1)
				{
					$saved_event_id = $this->statics_model->addNotification(10, $vehicle_id, $event_id);
					
					$the_event = $this->statics_model->getGroupEventsByID($event_id);
					
					$the_group = $this->statics_model->getGroupById($the_event->group_id)->row();
					
					$owners_vehicles = $this->car_model->getVehicles($the_group->created_by)->result();
					
					$this->addPoints($the_group->vehicle_id,5,'event_participants', 'event joined');
					
					foreach($owners_vehicles as $vehicle)
					{
						 $this->statics_model->addNotificationVehicle($vehicle->id,$saved_event_id);	
					}		
				}
				
				$container['response']['status'] = "OK";
			}
			else
			{
				$container['response']['status'] = "WRONG EVENT TYPE";
			}
	
				
			
		}
		else
		{
			$container['response']['status'] = "INVALID"; 
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;	

	}




	public function leave_event()
	{

		$user_id = $_POST['user_id'];
		$event_id = $_POST['event_id'];
		$vehicle_id = $_POST['vehicle_id'];
		$event_type = $_POST['event_type'];
		$status = 3;
			
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		
		if($session_response == "OK")
		{	
			
			if($event_type == "user")
			{
				$checkVehicle = $this->statics_model->checkVehicleAtUserEvent($event_id, $vehicle_id);
		
				if($checkVehicle)
				{
					$data = array('status' => $status);
					$this->statics_model->updateVehicleToUserEvent($event_id,$vehicle_id,$data);
				}
				
	
				$container['response']['status'] = "OK";
			}
			else if($event_type == "group")
			{
				$checkVehicle = $this->statics_model->checkVehicleAtEvent($event_id, $vehicle_id);
		
				if($checkVehicle)
				{
					$data = array('status_id' => $status);
					$this->statics_model->updateVehicleToEvent($event_id,$vehicle_id,$data);
				}
						
			}
			else
			{
				$container['response']['status'] = "WRONG EVENT TYPE";
			}
		
		}
		else
		{
			$container['response']['status'] = "INVALID"; 
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;	
			

	}



	public function get_friends()
	{
	//	$_POST['user_id'] = 98;
		
		$user_id = $_POST['user_id'];
		
			
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		
		if($session_response == "OK")
		{	
			$id_holder = array();
			
			
			$my_vehicles = $this->car_model->getVehicles($user_id)->result();
			foreach($my_vehicles as $my_vehicle)
			{
				$my_followers = $this->car_model->getFollowers($my_vehicle->id);
			
				foreach($my_followers as $follower)
				{
					
					$vehicle = $this->car_model->getVehicle($follower['follower_id'])->row();
					
					$profile = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
					
					$to = $vehicle->geo_location;
					
					if($to !== NULL && $to != "")
					{
						
						$to_arr = explode(",", $to);
						$to_lat = $to_arr[0];
						$to_lon = trim($to_arr[1]);
					}
					else
					{
						$to_lat = '';
						$to_lon = '';	
					}
					
						
					$item = array('id' => $vehicle->id,
								'nickname' => $vehicle->nickname,
								'lat' => $to_lat,
								'lon' => $to_lon,
								'profile_picture' => site_url()."items/uploads/profilepictures/".$profile);
					
					if(!in_array($vehicle->id, $id_holder))
					{
						$container['response']['vehicles'][] = $item;
						$id_holder[] = $vehicle->id;
					}
						
	
				}
				
				$follow_ids = array();
				
				$my_follows = $this->car_model->getMyFollows($my_vehicle->id);
				
				foreach($my_follows as $follower)
				{
					
					$vehicle = $this->car_model->getVehicle($follower['vehicle_id'])->row();			
					$profile = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
					
						
					$item = array('id' => $vehicle->id,
								'nickname' => $vehicle->nickname,
								'profile_picture' => site_url()."items/uploads/profilepictures/".$profile);
	
					if(!in_array($vehicle->id, $id_holder))
					{
						$container['response']['vehicles'][] = $item;
						$id_holder[] = $vehicle->id;
					}
		
				}
			}
			

		}
		else
		{
			$container['response']['status'] = "INVALID"; 
		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;	
		
		
	}



	public function get_image_comments()
	{

		$user_id = $_POST['user_id'];
		$image_id = $_POST['image_id'];
		
		
		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$friend_container['response']['session'] = array('id' => $user_id,
														"status" => $session_response);
		$friend_container['response']['comments'] = array();												
		$comments_holder = array();	
		if($session_response == "OK")
		{
			
			$comments = $this->statics_model->getImageComments($image_id);
			
			foreach($comments as $comment)
			{
				$vehicle = $this->car_model->getVehicle($comment->vehicle_id)->row();

				$profile = $this->statics_model->getImageByID($vehicle->profile_image)->row();
				
				$new = array('comment' => $comment->comment,
							 'name' => $vehicle->nickname,
							 'date' => $comment->created_date,	
							 'profile_image' => site_url()."items/uploads/profilepictures/".$profile->fname);
								
							
							
				$friend_container['response']['comments'][] = $new;
			}
		}
		else
		{
			$new = array('comment' => '-',
						 'name' => '-',
						 'date' => '-',	
						 'profile_image' => '-');
							
						
						
			$friend_container['response']['comments'][] = $new;
		}
				
		$json_data = json_encode($friend_container, JSON_PRETTY_PRINT);
		echo $json_data;	
		
	}



	public function invite_to_event()
	{
	
		$vehicles = $_POST['vehicles'];
		$event = $_POST['event_id'];
		$event_type = $_POST['event_type'];	
		$inviting_vehicle = $_POST['vehicle_id'];
		$user_id = $_POST['user_id'];
		
		if($event_type == "user")
		{
			$action_id = 7; //invited to user event
		}
		else
		{
			$action_id = 13; //invited to group event
		}
		
		
		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$friend_container['response']['session'] = array('id' => $user_id,
														"status" => $session_response);													
		if($session_response == "OK")
		{
			
			$event_id = $this->statics_model->addNotification($action_id, $inviting_vehicle, $event);
			
			foreach($vehicles as $vehicle){
				 $this->statics_model->addNotificationVehicle($vehicle,$event_id);
			 }
			 
			 $friend_container['response']['status'] = 'OK';
		}
		else
		{
									
			$friend_container['response']['status'] = 'INVALID';
		}
				
		$json_data = json_encode($friend_container, JSON_PRETTY_PRINT);
		echo $json_data;	
	
		
	}
	
	public function get_poi_categories()
	{

		$user_id = $_POST['user_id'];
		
		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$friend_container['response']['session'] = array('id' => $user_id,
														"status" => $session_response);													
		if($session_response == "OK")
		{
			
			$poi_categories = $this->statics_model->getPOICategories()->result();
			
			foreach($poi_categories as $category)
			{
				$data = array('id' => $category->id,
						'name' => $category->name);
						
			
			 
				$friend_container['response']['categories'][] = $data;
			}
			
			
		}
		else
		{
									
			$friend_container['response']['categories'] = array();
		}
				
		$json_data = json_encode($friend_container, JSON_PRETTY_PRINT);
		echo $json_data;

		
	}
	

	public function suggest_poi()
	{
		
		$name = $_POST['name'];
		$description = $_POST['description'];
		$address = $_POST['address'];
		$category = $_POST['category'];
		$geo_location = $_POST['geo_location'];
		$user_id = $_POST['user_id'];
		
		
		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$friend_container['response']['session'] = array('id' => $user_id,
														"status" => $session_response);													
		if($session_response == "OK")
		{
			
			$data = array('name' => $name,
						'description' => $description,
						'address' => $address,
						'category_id' => $category,
						'geo_location' => $geo_location);
						
			$this->statics_model->addPOI($data);
			 
			 $friend_container['response']['status'] = 'OK';
		}
		else
		{
									
			$friend_container['response']['status'] = 'INVALID';
		}
				
		$json_data = json_encode($friend_container, JSON_PRETTY_PRINT);
		echo $json_data;

		
	}

	public function add_traffic_alert()
	{

		
		$alert = $_POST['text'];
		$vehicle_id = $_POST['vehicle_id']; 
		
		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$friend_container['response']['session'] = array('id' => $vehicle_id,
														"status" => $session_response);													
		if($session_response == "OK")
		{
			$entity_id = $this->statics_model->addAlert($alert,$vehicle_id);
		
			$action_id = 15; // traffic alert
			$event_id = $this->statics_model->addEvent($action_id, $entity_id);
			$this->statics_model->addEventVehicle($vehicle_id,$event_id);
			
			 
			 $friend_container['response']['status'] = 'OK';
		}
		else
		{
									
			$friend_container['response']['status'] = 'INVALID';
		}
				
		$json_data = json_encode($friend_container, JSON_PRETTY_PRINT);
		echo $json_data;
		
		
			
	}
	
	
	public function add_status_update()
	{
		
		$alert = $_POST['text'];
		$vehicle_id = $_POST['vehicle_id']; 
		
		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$friend_container['response']['session'] = array('id' => $vehicle_id,
														"status" => $session_response);													
		if($session_response == "OK")
		{
			$entity_id = $this->statics_model->addStatus($alert,$vehicle_id);
		
			$action_id = 16; // status update
			$event_id = $this->statics_model->addEvent($action_id, $entity_id);
			$this->statics_model->addEventVehicle($vehicle_id,$event_id);	
			
			 
			 $friend_container['response']['status'] = 'OK';
		}
		else
		{
									
			$friend_container['response']['status'] = 'INVALID';
		}
				
		$json_data = json_encode($friend_container, JSON_PRETTY_PRINT);
		echo $json_data;
	
	}
	
	
	public function delete_message()
	{

		$message_id = $_POST['message_id'];

		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$friend_container['response']['session'] = array('id' => $message_id,
														"status" => $session_response);													
		if($session_response == "OK")
		{
			$entity_id = $this->statics_model->deleteMessage($message_id);

			 
			 $friend_container['response']['status'] = 'OK';
		}
		else
		{
									
			$friend_container['response']['status'] = 'INVALID';
		}

				
		$json_data = json_encode($friend_container, JSON_PRETTY_PRINT);
		echo $json_data;
		
		
			
	}
	

	
	function add_event()
	{
		
		$title = $_POST['title'];
		$description = $_POST['description'];
		$address = $_POST['address'];
		$geo_location = $_POST['geo_location'];
		$date = $_POST['start_time'];		
		$datetime = date('Y-m-d H:i', strtotime($date));
		$event_type = $_POST['event_type'];
		$the_entity_id = $_POST['entity_id'];
		$user_id = $_POST['user_id'];
		$public = $_POST['public'];
		
		
		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$friend_container['response']['session'] = array('id' => $user_id,
														"status" => $session_response);													
		if($session_response == "OK")
		{
			
			if($event_type == "user")
			{
				$vehicle_id = $the_entity_id;	
				
				$image = NULL;
				
				if($_FILES['image']['size'] != 0 && $_FILES['image']['error'] == 0)	
				{
					
					$uploadconfig['upload_path'] = 'items/uploads/coverpictures';
					$uploadconfig['allowed_types'] = 'gif|jpg|png';
					$uploadconfig['encrypt_name'] = true;	
					
					$this->upload->initialize($uploadconfig);
					
					$this->upload->do_upload('image');
								
					
					
					
					
					$cover_data = $this->upload->data();
					
									
					$fname = 'items/uploads/coverpictures/'.$cover_data['file_name'];
					$this->load->library('image_moo');
					$this->image_moo->load($fname)->resize(1510,640)->save($fname,true);
					
					$fname_thumb = 'items/uploads/coverpictures/thumb__'.$cover_data['file_name'];
					$this->image_moo->load($fname)->resize(360,360)->save($fname_thumb,true);
					
					$image = $cover_data['file_name'];
				}
				
				
				
				$data = array('vehicle_id' => $vehicle_id,
							'title' => $title,
							'description' => $description,
							'address' => $address,
							'geo_location' => $geo_location,
							'start_time' => $datetime,
							'image' => $image,
							'public' => $public,
							'user_id' => $user_id);
							
				$group_event_id = $this->statics_model->addUserEvent($data);
				
				$event_data = array('user_event_id' => $group_event_id,
									'vehicle_id' => $this->session->userdata('vehicle_id'),
									'status' => 1);
				
				$this->statics_model->addVehicleToUserEvent($event_data);
				
				$entity_id = $group_event_id;
				$action_id = 5; //event created
				
				
				$event_id = $this->statics_model->addEvent($action_id, $entity_id);				
				$this->statics_model->addEventVehicle($vehicle_id,$event_id);
			}
			else
			{
				$group_id = $the_entity_id;
				
				$image = NULL;
				
				if($_FILES['image']['size'] != 0 && $_FILES['image']['error'] == 0)	
				{
					
					$uploadconfig['upload_path'] = 'items/uploads/coverpictures';
					$uploadconfig['allowed_types'] = 'gif|jpg|png';
					$uploadconfig['encrypt_name'] = true;	
					
					$this->upload->initialize($uploadconfig);
					
					$this->upload->do_upload('image');
								
					
					
					
					
					$cover_data = $this->upload->data();
					
									
					$fname = 'items/uploads/coverpictures/'.$cover_data['file_name'];
					$this->load->library('image_moo');
					$this->image_moo->load($fname)->resize(1510,640)->save($fname,true);
					
					$fname_thumb = 'items/uploads/coverpictures/thumb__'.$cover_data['file_name'];
					$this->image_moo->load($fname)->resize(360,360)->save($fname_thumb,true);
					
					$image = $cover_data['file_name'];
				}
				
				$data = array('group_id' => $group_id,
							'title' => $title,
							'description' => $description,
							'address' => $address,
							'geo_location' => $geo_location,
							'image' => $image,
							'public' => $public,
							'start_time' => $datetime);
							
				$group_event_id = $this->statics_model->addGroupEvent($data);
				
				$event_data = array('event_id' => $group_event_id,
									'vehicle_id' => $this->session->userdata('vehicle_id'),
									'status_id' => 1);
				
				$this->statics_model->addVehicleToEvent($event_data);
				
				$action_id = 5; //event created
				
				
				$event_id = $this->statics_model->addNotification($action_id, $group_event_id, $group_id);
				
				$vehicles = $this->statics_model->getGroupMembers($group_id)->result();
				
				foreach($vehicles as $vehicle){
					 $this->statics_model->addNotificationVehicle($vehicle->id,$event_id);
				 }
			}
			 
			 $friend_container['response']['status'] = 'OK';
		}
		else
		{
									
			$friend_container['response']['status'] = 'INVALID';
		}
				
		$json_data = json_encode($friend_container, JSON_PRETTY_PRINT);
		echo $json_data;

		
		
		
	}
	
	
	public function add_car()
	{
		
		
			
		$model_id = $_POST['model_id'];
		$nickname = $_POST['nickname'];
		$engine_type = $_POST['engine'];
		$type = $_POST['vehicle_type'];
		$user_id = $_POST['user_id'];
		
		
		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$friend_container['response']['session'] = array('id' => $session_id,
														"status" => $session_response);													
		if($session_response == "OK")
		{
				
			$vehicle_type = 0; //Car by default
			
			if($type == "motorcycle")
			{
				$vehicle_type = 1; //Motorcycle
			}
			
			if($type == "truck")
			{
				$vehicle_type = 2; //Truck
			}
			
			$data = array(
						'nickname' => $nickname,
						'model_id' => $model_id,
						'vehicle_type' => $vehicle_type,
						'engine_type' => $engine_type,
						'user_id' => $user_id);	
						
			$new_car_id = $this->car_model->createCar($data);
			
		
			$pretty_url = $nickname."".$new_car_id;
			
			$pretty_url = $this->toAscii($pretty_url);
			
			$pretty_data = array('pretty_url' => $pretty_url);
		
			$this->car_model->createPrettyUrl($new_car_id, $pretty_data);
			
			$action_id = 3; //new vehicle profile
			$event_id = $this->statics_model->addEvent($action_id, $new_car_id);
			$this->statics_model->addEventVehicle($new_car_id,$event_id);

			$friend_container['response']['session'] = array('id' => $new_car_id,
														"status" => 'OK');
			 
			 
		}
		else
		{
									
			$friend_container['response']['session'] = array('id' => '-',
														"status" => 'INVALID');
		}

				
		$json_data = json_encode($friend_container, JSON_PRETTY_PRINT);
		echo $json_data;	
			

	}
	
	
	public function getMake()
	{

		
			
		$year = $_POST['year'];
		$type = $_POST['type'];
		
		$session_response = "INVALID";
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$friend_container['response']['session'] = array('id' => $session_id,
														"status" => $session_response);													
		if($session_response == "OK")
		{
				
			$result = $this->statics_model->getVehicleMakeByYear($year, $type);
			$makes = array();
			if($result)
			{
				foreach($result->result() as $vehicle)
				{
				
					$item = array('name' => strtolower($vehicle->make));
					
					$makes[] = $item;
				}
				
				asort($makes);
				$friend_container['response']['status'] = 'OK';
				$friend_container['response']['makes'] = $makes;
				
				
			}
			else
			{
				$friend_container['response']['status'] = 'EMPTY';
				$friend_container['response']['makes'] = '';
			}

			 
			 
		}
		else
		{
									
			$friend_container['response']['status'] = 'INVALID';
			$friend_container['response']['makes'] = '';
		}

				
		$json_data = json_encode($friend_container, JSON_PRETTY_PRINT);
		echo $json_data;	
			
			
			
			
		
	
		
		
		
		
	}
	
	
	public function getModel()
	{
		$year = $_POST['year'];
		$type = $_POST['type'];
		$make = $_POST['make'];
		
		$session_response = "INVALID";

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$friend_container['response']['session'] = array('id' => $session_id,
														"status" => $session_response);													
		if($session_response == "OK")
		{
				
			$result = $this->statics_model->getVehicleModelByYearMake($year, $type, $make);
			
			
			if($result)
			{
				foreach($result->result() as $vehicle)
				{
					
					$item = array('id' => $vehicle->id,
									'model' => $vehicle->model,			
									'desc' => ($vehicle->model_trim)? $vehicle->model_trim : '');
					
					
						$models[] = $item;
	
				}

				$friend_container['response']['session'] = array('id' => $session_id,
														"status" => 'OK');	
				$friend_container['response']['models'] = $models;
	
			}
			else
			{
				$friend_container['response']['session'] = array('id' => $session_id,
														"status" => 'EMPTY');	
				$friend_container['response']['models'] = '';
			}
 
		}
		else
		{
									
			$friend_container['response']['session'] = array('id' => $session_id,
														"status" => 'INVALID');	
			$friend_container['response']['models'] = '';
		}

				
		$json_data = json_encode($friend_container, JSON_PRETTY_PRINT);
		echo $json_data;	
		
	}
	
	public function delete_vehicle()
	{
	
		$user_id = $_POST['user_id'];	
		$vehicle_id = $_POST['vehicle_id'];	
		
		$session_response = "INVALID";
		$container = array();
		
		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$container['response']['session'] = array("id" => $user_id,
													"status" => $session_response);
		
		if($session_response == "OK")
		{
			
			$this->car_model->deleteVehicle($vehicle_id);	

				
		}
		else
		{
			

		}
		
		$json_data = json_encode($container, JSON_PRETTY_PRINT);
		echo $json_data;

	}
	
	
	public function get_all_friends()
	{
		
		$user_id = $_POST['user_id'];
	
		$session_response = "INVALID";

		if(!empty($_POST['session_id'])){
			$session_id = $_POST['session_id'];
			$session_response = $this->checkSession($session_id);
		}
		
		$friend_container['response']['session'] = array('id' => $session_id,
														"status" => $session_response);													
		if($session_response == "OK")
		{
			$users_array = array();
			$users_array[] = $user_id;	
			$my_vehicles = $this->car_model->getVehicles($user_id)->result();
			
			foreach($my_vehicles as $my_vehicle)
			{
				$my_followers = $this->car_model->getFollowers($my_vehicle->id);
			
				foreach($my_followers as $follower)
				{
					
					$friend_user_id = $this->car_model->getVehicle($follower['follower_id'])->row()->user_id;				
					$friend_user = $this->statics_model->getUserData($friend_user_id)->row();
					
					$item = array('id' => $friend_user->id,
								'name' => $friend_user->first_name." ".$friend_user->last_name,
								'profile_picture' => site_url()."items/uploads/profilepictures/".$friend_user->profile_picture);
					
					if(!in_array($friend_user_id, $users_array))
					{
						$friend_container['response']['users'][] = $item;
						$users_array[] = $friend_user_id;
					}
						
	
				}
				
				$follow_ids = array();
				
				$my_follows = $this->car_model->getMyFollows($my_vehicle->id);
				
				foreach($my_follows as $follower)
				{
						
					$friend_user_id = $this->car_model->getVehicle($follower['vehicle_id'])->row()->user_id;				
					$friend_user = $this->statics_model->getUserData($friend_user_id)->row();
					
					$item = array('id' => $friend_user->id,
								'name' => $friend_user->first_name." ".$friend_user->last_name,
								'profile_picture' => site_url()."items/uploads/profilepictures/".$friend_user->profile_picture);
					
					if(!in_array($friend_user_id, $users_array))
					{
						$friend_container['response']['users'][] = $item;
						$users_array[] = $friend_user_id;
					}	
						
				}
			}
 
		}
		else
		{
									
			$friend_container['response']['session'] = array('id' => $session_id,
														"status" => 'INVALID');	
			$friend_container['response']['users'] = array();
		}

				
		$json_data = json_encode($friend_container, JSON_PRETTY_PRINT);
		echo $json_data;	
		
	}
	
}
<script>	
	$('#sidemenu').css({top: 0});
</script>
<div id="content">
	<div class="statics_subheader" style="margin-top:35px;">
		<?= $this->lang->line('sidemenu_member_myfriends');?>
	</div>
	<?php
	if(empty($dogs)){?>
		<?= $this->lang->line('dog_no_dogs');?><br/>
		<?= $this->lang->line('dog_click');?><a href="<?= site_url('new_dog');?>"><?= $this->lang->line('dog_here_link');?></a><?= $this->lang->line('dog_to_dogs');?>
	<?}
	else{
	
		if($count_friends == 0){?>
				<?= $this->lang->line('dog_no_friends');?><br/>
				<?= $this->lang->line('dog_click');?><a href="<?= site_url('profile_search');?>"><?= $this->lang->line('dog_here_link');?></a><?= $this->lang->line('dog_search_friend');?>
		<?php }
			else{ ?>
			<div style="overflow:hidden;margin-bottom:20px;">
				<input type="text" id="friend_search_input" name="friend_search_input" placeholder="<?= $this->lang->line('dog_new_name')?>" defaultText="<?= $this->lang->line('dog_new_name')?>" class="search_input">
				<div id="friend_search_button" style="margin-top:22px;"><?= $this->lang->line('profile_search_submit')?></div> 
			</div>	
			<div style="overflow:hidden;">		
	<?	 $x = 1; foreach($friend_dogs as $dog): ?>
	 
			
			<a href="<?= site_url('profile/'.$dog['pretty_url']);?>">
				<div class="my_friends_item" id="<?= $dog['id']?>" <?if($x%3==0)echo "style='margin-right:0px;'"?> dog_name="<?= $dog['name']?>">
					<div class="div_white_over"></div>
					<img class="friends_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $dog['picture'])?>" border="0">
					<div class="friends_list_item_desc" style="top:35px;"><?= $dog['name']?></div>			
					
					<div class="sansitalic" style="color:#ffffff;position:absolute;text-align:center;width:100%;top:60px;"><?= $this->lang->line('dog_frenemies_friends_of');?></div>
					<div class="friend_thumb_holder">
						<? foreach($dogs as $my_dog): 
						
							
							if (in_array($my_dog['id'], $dog['friends'])){?>
								
									<?= $my_dog['name'];?>
									<br/>							
						<?php }
						endforeach; ?>										
					</div>
				</div>
			</a>
							

		
	<?php $x++; endforeach; ?>
	</div> 
	<?	}
		}
	?>
	
	<div class="statics_subheader" style="margin-top:5px;margin-bottom:15px;">
		<?= $this->lang->line('dog_friend_suggestion');?>
	</div>
	<div>
		<?	
			$x = 1; foreach($random_dogs as $rdog): 
			
			if(is_numeric($rdog['profile_picture']))
			{
				$dog_profile = $this->statics_model->getImageByID($rdog['profile_picture'])->row()->fname;
				
			}
			else
			{
				$dog_profile = $rdog['profile_picture'];
			}
			if($x <=9){?>

				<a href="<?= site_url('profile/'.$rdog['pretty_url']);?>">
					<div class="my_friends_item" id="<?= $rdog['id']?>" style="background:none; <?if($x%3==0)echo 'margin-right:0px';?>" dog_name="<?= $rdog['name']?>">
						<div class="div_white_over"></div>
						<img class="friends_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $dog_profile)?>" border="0" />
						<div class="friends_list_item_desc" style=""><?= $rdog['name'];?></div>			
						
					</div>
				</a>
			<? }?>
		<?php $x++; endforeach; ?>
		<a href="<?= site_url('profile_search');?>">
			<div class="my_friends_item"  style=" <?if($x%3==0)echo 'margin-right:0px';?>" >
				
				<div class="friends_list_item_desc" style=""><?= $this->lang->line('click_here_to_find_more_dogs');?><br/><span style="font-size:36px;">+</span></div>			
				
			</div>
		</a>
	</div>
</div>
<script>	
	$('#sidemenu').css({top: 0});
</script>
<div id="content">
	<div class="statics_subheader" style="margin-top:35px;">
		<?= $this->lang->line('sidemenu_member_myenemies');?>
	</div>
	
	<!-- <a href="<?= site_url('profile_search')?>"><div class="button_skin" id="search"><?= $this->lang->line('my_enemies_search_dog');?></div></a> -->
	<?php
	if(empty($dogs)){?>
		<?= $this->lang->line('dog_no_dogs');?><br/>
		<?= $this->lang->line('dog_click');?><a href="<?= site_url('new_dog');?>"><?= $this->lang->line('dog_here_link');?></a><?= $this->lang->line('dog_to_dogs');?>
	<?}
	else{
	
		if($count_friends == 0){?>
				<?= $this->lang->line('dog_no_friends');?><br/>
				<?= $this->lang->line('dog_click');?><a href="<?= site_url('profile_search');?>"><?= $this->lang->line('dog_here_link');?></a><?= $this->lang->line('dog_search_friend');?>
		<?php }
			else{
					?>			
		 <div style="overflow:hidden;margin-bottom:20px;">
				<input type="text" id="friend_search_input" name="friend_search_input" placeholder="<?= $this->lang->line('dog_new_name')?>" defaultText="<?= $this->lang->line('dog_new_name')?>" class="search_input">
				<div id="friend_search_button" style="margin-top:22px;"><?= $this->lang->line('profile_search_submit')?></div> 
			</div>	
			<div style="overflow:hidden;">		
	<?	 $x = 1; foreach($friend_dogs as $dog): ?>
	 
			
			<a href="<?= site_url('dog/'.$dog['id']);?>">
				<div class="my_friends_item" id="<?= $dog['id']?>" <?if($x%3==0)echo "style='margin-right:0px;'"?> dog_name="<?= $dog['name']?>">
					<div class="div_white_over"></div>
					<img class="friends_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $dog['picture'])?>" border="0">
					<div class="friends_list_item_desc" style="top:35px;"><?= $dog['name']?></div>			
					
					<div class="sansitalic" style="color:#ffffff;position:absolute;text-align:center;width:100%;top:60px;"><?= $this->lang->line('dog_frenemies_enemies_of');?></div>
					<div class="friend_thumb_holder">
						<? foreach($dogs as $my_dog): 
						
							
							if (in_array($my_dog['id'], $dog['friends'])){?>
								
									<?= $my_dog['name'];?>
									<br/>							
						<?php }
						endforeach; ?>										
					</div>
				</div>
			</a>
							

		
	<?php $x++; endforeach; 
		}
		}
	?>
	</div>
	<div class="statics_subheader" style="margin-top:5px;margin-bottom:15px;">
		<?= $this->lang->line('dog_enemy_suggestion');?>
	</div>
	<div>
		<?	 $x = 1; foreach($random_dogs as $dog): ?>

				<a href="<?= site_url('dog/'.$dog->id);?>">
					<div class="my_friends_item" id="<?= $dog->id?>" style="background:none; <?if($x%3==0)echo 'margin-right:0px';?>" dog_name="<?= $dog->name?>">
						<div class="div_white_over"></div>
						<img class="friends_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $dog->profile_picture)?>" border="0" />
						<div class="friends_list_item_desc" style=""><?= $dog->name;?></div>			
						
					</div>
				</a>

		<?php $x++; endforeach; ?>
	</div>
</div>
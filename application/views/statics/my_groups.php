<script>
	$(document).ready(function()
	{	
		initAutocomplete();
	});
</script>



<div id="content" style="width:800px;">
	<img class="" style="position:absolute;left:0px;top:0px;" src="<?= site_url('items/frontend/img/big_chrome.png')?>" border="0">
	<div class="playbold gold page_title" style="text-align:center;font-size:40px;letter-spacing:1px;z-index:10;position:relative;top:10px;">MY CLUBS</div>
	
	
	<div class="error_msg" style="left:auto;right:10px;"></div>
	<div style="position: relative;left:10px;top:25px;width:505px;overflow:hidden;">
		<div class="button" id="new_group" style="margin-top:0px;margin-left:0px;">CREATE NEW CLUB</div>
		<div id="group_new_box">
			<input type="text" name="group_name" id="group_name" class="edit_input" value="" placeholder="Club name" style="float:left;"/>
			<select name="category_select" id="category_select" style="padding-left:10px;width:250px;margin-right:-10px;">
				<option value="0">Please select a category...</option>
				<? foreach($group_categories as $cat):?>
					<option value="<?= $cat['id'];?>"> <?= $cat['name'];?> </option>
				<? endforeach;?>
			</select>
			<input type="text" name="group_address" id="autocomplete" onFocus="geolocate()" class="edit_input" value="" placeholder="Club address" style="float:right;width:240px;margin-right: 0px;margin-top: 5px;"/>
			
			
			
			
			<br/>
			<textarea name="group_description" id="group_description" class="edit_input" placeholder="Club description" style="border:0px;resize:none;height:60px;float:left;padding:3px;"></textarea>
			
			<br/>
			<br/>
			<br/>
			<input type="checkbox" name="terms" id="group_create_terms" value="1">
			<label style="color:#ffffff;" for="terms">I accept the <a target="_blank" href="<?= site_url('group_create_terms')?>">Terms & Conditions</a></label>
			<div id="request_new_grp_btn" class="button" style="float:left;">SUBMIT</div>
		</div>
		
	</div>
	<a style="text-decoration:none;" href="<?= site_url('group_search')?>"><div class="button" style="position: absolute;right: 10px;top: 52px;">FIND CLUBS</div></a>
	<div id="resultHolder" style="margin-top:60px;">
		<div class="dosissemi">OWNED CLUBS</div><br/>
		<div id="result_list" style="overflow:hidden;">
		<? $x = 1; foreach($owned_groups as $group):?>
		<a  style="text-decoration:none;" href="<?= site_url('group/' . $group['id'])?>">	
			<div class="result_list_item" style="<? if($x%2 == 0){ echo "margin-right:0px";}?>">	
					<img class="result_item_image" src="<?= site_url('items/uploads/profilepictures/'.$group['logo'])?>" style="">
					<div class="div_white_over"></div>
					<div class="result_item_text" style="">
							<?= $group['name'];?><br/>
							<?= $group['category'];?><br/>
							<br/>
							<?= $group['vehicle'];?><br/>
							<br/>
							Members: <?= $group['members'];?>
							</div>				
				
			</div>
		</a>
		<? $x++; endforeach;?>
		<? if($x == 1) echo "None yet!";?>
		</div>		
		<br/>
		<div class="dosissemi">MEMBER OF</div><br/>
		<div id="result_list"  style="overflow:hidden;">
		<? $x = 1; foreach($member_of_groups as $group):?>
		<a  style="text-decoration:none;" href="<?= site_url('group/' . $group['id'])?>">	
			<div class="result_list_item" style="<? if($x%2 == 0){ echo "margin-right:0px";}?>">	
					<img class="result_item_image" src="<?= site_url('items/uploads/profilepictures/'.$group['logo'])?>" style="">
					<div class="div_white_over"></div>
					<div class="result_item_text" style="">
							<?= $group['name'];?><br/>
							<?= $group['category'];?><br/>
							<br/>
							<?= $group['vehicle'];?><br/>
							Members: <?= $group['members'];?>
							</div>				
				
			</div>
		</a>
		<? $x++; endforeach;?>
		<? if($x == 1) echo "None yet!";?>
		</div>
	</div>
	
	
</div>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" class="no-js">          
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<script>
			if(!navigator.userAgent.match(/iPad/i))
		    {
		    	document.write('<meta name="viewport" content="width=640, minimum-scale=0.5, maximum-scale=2.5, user-scalable=no">');
			}
			else
			{
				document.write('<meta name="viewport" content="width=1200, minimum-scale=0.2, maximum-scale=4.5, user-scalable=no">');
			}
		</script>
	<!--	<meta name="apple-itunes-app" content="app-id=998164678">
		<meta property="al:ios:url" content="http://snoopstr.com/" />
		<meta property="al:ios:app_store_id" content="998164678" />
		<meta property="al:ios:app_name" content="Performance Nation" /> -->
		<meta property="og:title" content="Performance Nation"/>
		<meta property="og:image" content="<?= site_url('items/frontend/img/logo.png')?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="<?= site_url();?>"/>
		<meta property="og:site_name" content="Performance Nation"/>
		<meta property="og:description" content="Performance Nation"/>

		<title>Performance Nation</title>
		
		<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/fonts.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/desktop.css?version=2');?>" media="all" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/jquery-ui.min.css');?>" media="all" />
		
		
	<!--	<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/frontend.css');?>" media="all" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/dog.css');?>" media="all" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/auth.css');?>" media="all" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/statics.css');?>" media="all" />
		-->
		
		
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/jquery.datepick.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/timepicker-addon.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/lightbox.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/selectize.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/jquery.qtip.min.css');?>" media="screen" />
		
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/upload.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/booking.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/imgareaselect-default.css');?>" media="all" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/iealert/style.css');?>" media="all" />
		<link rel="shortcut icon" type="image/png" href="<?= site_url('items/frontend/img/favicon.ico');?>"/>
			<script src="<?= site_url("items/frontend/js/modernizr.custom.18900.js"); ?>" type="text/javascript"></script>
			<script src="<?= site_url("items/frontend/js/moxie.js"); ?>" type="text/javascript"></script>
	
		<script>
			
			
			mOxie.Env.swf_url = "../items/frontend/js/Moxie.min.swf"; 
			mOxie.Env.xap_url = "../items/frontend/js/Moxie.min.xap";
		</script>		
			
	
		
		
		<script>
			var rootUrl = "<?= site_url();?>";
			var user_id = <?= $user_id ?>;
			var country = "<?= $country_ip ?>";
			var sitter_detail_thank_rate = "<?= $this->lang->line('sitter_detail_thank_rate')?>";
			var select_dog_friend = "<?= $this->lang->line('dog_selection_friend_text')?>";
			var select_dog_enemy = "<?= $this->lang->line('dog_selection_enemy_text')?>";
			var photo_edit_title = "<?= $this->lang->line('photo_edit_title')?>";
			var delete_confirm_text = "<?= $this->lang->line('photo_delete_confirm_text')?>";
			var suggest_zone_description_thanks = "<?= $this->lang->line('suggest_zone_description_thanks')?>";
			var search_searching = "<?= $this->lang->line('search_searching')?>";
			var search_nothing_found = "<?= $this->lang->line('sorry_nothin_found')?>";
			var search_nothing_found_sitter = "<?= $this->lang->line('sorry_nothin_found_sitter')?>";
			var please_type_location = "<?= $this->lang->line('please_type_location')?>";
			var please_wait = "<?= $this->lang->line('please_wait')?>";
			var dragdrop = "<?= $this->lang->line('drag_drop')?>";
			var email_sent_text = "<?= $this->lang->line('email_sent')?>";
			var show_profile_sitter = "<?= $this->lang->line('edit_user_dogsitter_checkbox')?>";
			var hide_profile_sitter = "<?= $this->lang->line('edit_user_dogsitter_checkbox_deactive')?>";
			var loading_text = "<?= $this->lang->line('loading')?>";
			var search_text = "<?= $this->lang->line('profile_search_submit')?>";
			var no_dogs_breed = "<?= $this->lang->line('no_dogs_for_breed')?>";
			var no_dogs_name = "<?= $this->lang->line('no_dogs_for_name')?>";
			var cover_image_text = "<?= $this->lang->line('cover_image')?>";
			var profile_image_text = "<?= $this->lang->line('profile_image')?>";
			var upload_failed_text = "<?= $this->lang->line('upload_failed_text')?>";
		
		</script>
		<script type="text/javascript" src="<?= site_url("items/general/js/jquery-1.11.3.min.js"); ?>" ></script>
		<script src="<?= site_url("items/frontend/js/jquery-ui.min.js"); ?>" type="text/javascript"></script>
		
		<script src="<?= site_url("items/frontend/js/jquery.datepick.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/timepicker-addon.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/lightbox.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/lightbox2.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/jstz.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/jquery.cookie.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/jquery.qtip.min.js"); ?>" type="text/javascript"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBx0OgNneXh128fyjRG43NuMofpM61Edg4&libraries=geometry,places"></script>
		<script src="<?= site_url("items/frontend/js/jquery.eventCalendar.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/jquery.timeago.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/upload.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/jquery.imgareaselect.pack.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/iealert.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/placeholders.min.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/general/js/packery.pkgd.min.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/selectize.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/markerwithlabel.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/responsive.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/desktop.js?ver=2"); ?>" type="text/javascript"></script>
		
		<!-- FB -->
		<script>
		  window.fbAsyncInit = function() {
		    FB.init({
		      appId      : '1680628872212292',
		      xfbml      : true,
		      version    : 'v2.5'
		    });
		  };
		
		  (function(d, s, id){
		     var js, fjs = d.getElementsByTagName(s)[0];
		     if (d.getElementById(id)) {return;}
		     js = d.createElement(s); js.id = id;
		     js.src = "//connect.facebook.net/en_US/sdk.js";
		     fjs.parentNode.insertBefore(js, fjs);
		   }(document, 'script', 'facebook-jssdk'));
		</script>
		 
		 <!-- GA -->
		 
	</head>
	<body>
		<div id="site_bg"></div>
		<div id="loadingBG"></div>
		<img id="loadingGif" src="<?= site_url('items/frontend/img/loading.gif')?>" />	
		<img id="logo" src="<?= site_url('items/frontend/img/logo.png')?>"/>
		
		<? if ($this->session->userdata('vehicle_id') !== FALSE) {?>
			<div id="comment_overlay">
				<div class="overlayBG"></div>
				<div style="position:relative;z-index:100">
					<img class="close_overlay" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />		
					<div class="titboldit" style="font-size:27px;margin-top:30px;text-align:center;">COMMENTS</div>
						<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">
						<div id="user_comments"></div>
						<div style="width:400px;margin:30px auto;">
							<textarea id="send_comment_input" style="width:100%;height:70px;resize:none;"></textarea>	
							<div class="button" id="send_comment" >SAVE COMMENT</div>
						</div>
					</div>
					<div class="regular" id="group_event_error_message"></div>
				</div>
			</div>
			
			
			<div id="menu_holder">
				<div id="menu_left">
					<a style="text-decoration:none;" href="<?=site_url('')?>">
						<div class="menu_item">NEWSFEED</div>
					</a>
					<div class="menu_item sub" grp="profile">GARAGE</div>
					<div class="menu_item sub" grp="explore">EXPLORE</div>
					<div class="menu_item" id="get_notification" style="position:relative;">
						<img style="margin-top:16px;" src="<?= site_url('items/frontend/img/flag.png')?>"/>
						<div class="titit gold" style="position:absolute;bottom:0px;right:3px;font-size:14px;height:20px;line-height:20px;"><? if($notification_counter > 0) echo "(".$notification_counter.")";?></div>
					</div>
					<a style="text-decoration:none;" href="<?= site_url('messages')?>">
						<div class="menu_item" style="border-right:1px dotted #ffffff;">
							<img  style="margin-top:23px;" src="<?= site_url('items/frontend/img/mail_icon.png')?>"/>
						</div>
					</a>
					
					
				</div>
				<div id="menu_right">
					<a style="text-decoration:none;" href="<?= site_url('logout')?>"><div class="button titboldit" style="height:70px;line-height:70px;margin:0px;padding:0px;border:0px;border-left:1px dotted #ffffff;font-size:20px;width:160px;margin-left: 10px;float: right;">LOG OUT</div></a>
					<div style="margin-top:10px;z-index:10">
						<img style="width:38px;border:1px solid #ffffff;float:right; margin-right:0px;margin-top:5px;" src="<?= site_url('items/uploads/profilepictures/'.$vehicle_profile)?>"/>
						<div class="territory_item tititlight" style="float:left;font-size:16px;width: 185px;text-align: right;"><?= $vehicle_nickname;?></div>
						<a style="text-decoration:none;" href="<?= site_url('switch_vehicle')?>">
									<div id="switch_vehicle_button" class="titboldit button" style="cursor:pointer;font-size:16px;position:absolute;left: 88px;top: 12px;width:100px;border:0px;padding:0px;">Switch vehicle</div></a>
					</div>
					
				</div>
		 </div>
		
			<img id="menu_line" src="<?= site_url('items/frontend/img/menu_line.png')?>"/>
			
			
			
			<div id="notifications_holder">
				<img class="" style="position:absolute;left:0px;top:0px;width:100%;" src="<?= site_url('items/frontend/img/notification_chrome.png')?>" border="0">
				<img class="" style="position:absolute;left: 150px;top: -17px;" src="<?= site_url('items/frontend/img/notification_arrow_white.png')?>" border="0">
				
				<div class="playbold" style="font-size:25px;color:#000000;text-align:center;position:relative;z-index:1;line-height:40px;letter-spacing:1px;">NOTIFICATIONS</div>
				<div id="notifications"></div>
				<div class="button" id="mark_read_notifications" style="margin:11px auto;">MARK ALL AS READ</div>
			</div>
			
			
			
			<div class="submenu_holder" grp="profile" style="left:300px;">
				<a style="text-decoration:none;" href="<?= site_url('my_profile')?>"><div class="submenu_item">MY PROFILE</div></a>
				<a style="text-decoration:none;" href="<?= site_url('my_vehicles')?>"><div class="submenu_item">MY RIDES</div></a>
				<a style="text-decoration:none;" href="<?= site_url('my_connections')?>"><div class="submenu_item">MY CREW</div></a>
				<a style="text-decoration:none;" href="<?= site_url('my_groups')?>"><div class="submenu_item">MY CLUBS</div></a>
				<a style="text-decoration:none;" href="<?= site_url('my_events')?>"><div class="submenu_item">MY EVENTS</div></a>
			</div>
			
			
			<div class="submenu_holder" grp="explore" style="left:420px;">
				<a style="text-decoration:none;" href="<?= site_url('vehicle_search')?>"><div class="submenu_item">VEHICLES</div></a>
				<a style="text-decoration:none;" href="<?= site_url('poi_search')?>"><div class="submenu_item">POI</div></a>
				<a style="text-decoration:none;" href="<?= site_url('group_search')?>"><div class="submenu_item">CLUBS</div></a>
				<a style="text-decoration:none;" href="<?= site_url('event_search')?>"><div class="submenu_item">EVENTS</div></a>
			</div>
			
			
		<? }?>
		<div id="container">
			
			
			
		
			
			<div id="wrapper">
			
				<div id="invite_site_overlay">
					<div class="overlayBG"></div>
					<div style="position:relative;z-index:100">
						<img class="close_overlay" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />
						<div class="titboldit" style="font-size:27px;margin-top:30px;text-align:center;">INVITE A FRIEND</div>
						<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">							
								<div class="regular" style=";margin-top:30px;text-align:center;">Enter the email address and<br/> we will send out the invitation!</div>
								
								<input class="edit_input" type="text" id="invite_friend_email" name="invite_friend_email" placeholder="Type in email..." style="margin:20px auto;left:92px;"/>
									
						</div>
						<div class="button" id="invite_site_button">SEND INVITE</div>
						<div class="regular" id="invite_error_message" style="text-align:center;"></div>
					</div>
				</div>
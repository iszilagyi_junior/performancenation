<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Statics_Model extends CI_Model
{
	
	
	function getCarModels()
	{
	
		return $this->db->get('car_models');			
		
	}
	
	function getMotorModels()
	{
	
		return $this->db->get('motorcycle_models');			
		
	}
	
	function getVehicleMakeByYear($year, $type)
	{
		$sql = "SELECT DISTINCT make FROM ".$type."_models WHERE year = ? ORDER BY make ASC";
		$resultset = $this->db->query($sql, array($year));
		if ($resultset->num_rows() > 0) {
				 return $resultset;
			} else {
				return FALSE;
			} 			
		
	}
	
	
	function getVehicleModelByYearMake($year, $type, $make)
	{
		$sql = "SELECT DISTINCT * FROM ".$type."_models WHERE year = ? AND make = ? ORDER BY model ASC";
		$resultset = $this->db->query($sql, array($year, $make));
		if ($resultset->num_rows() > 0) {
				 return $resultset;
			} else {
				return FALSE;
			} 			
		
	}
	
	
	function getUserdataByUsername($username)
	{
		$sql = "SELECT * FROM users WHERE username = ? ";
		$resultset = $this->db->query($sql, array($username));
		if ($resultset->num_rows() > 0) {
				 return $resultset;
			} else {
				return FALSE;
			} 			
		
	}
	
	
	
	function getUserData($id)
	{
		$sql = 'SELECT * FROM users WHERE id = ? ';
		$resultset = $this->db->query($sql, array($id));
		return $resultset;
	}

	function getVehicleData($id)
	{
		$sql = 'SELECT * FROM vehicles WHERE id = ? ';
		$resultset = $this->db->query($sql, array($id));
		return $resultset;
	}
	
	
	
	function getAlbums($vehicle_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$result = $this->db->get('albums');
		
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();
	}
	
	
	function getAlbumImage($album_id)
	{
		$this->db->where('album_id', $album_id);
		$this->db->order_by('created_date', 'desc');
		$result = $this->db->get('albums_images',1);
		
		return ( $result->num_rows() > 0 ) ? $result->row()->image_id : 21;
	}
	
	
	function getAlbumData($album_id)
	{
		$this->db->where('id', $album_id);
		$result = $this->db->get('albums');
		
		return ( $result->num_rows() > 0 ) ? $result->row() : 0;
	}
	
	
	function addAlert($alert,$vehicle_id)
	{
		$sql = "INSERT INTO traffic_alerts (the_alert, vehicle_id) VALUES (?, ?);";
		$resultset = $this->db->query($sql, array($alert,$vehicle_id));
		return $this->db->insert_id();
	}
	
	
	function addStatus($alert,$vehicle_id)
	{
		$sql = "INSERT INTO status_updates (status, vehicle_id) VALUES (?, ?);";
		$resultset = $this->db->query($sql, array($alert,$vehicle_id));
		return $this->db->insert_id();
	}
	
	
	function getAlert($aid)
	{
		$this->db->where('id', $aid);
		$result = $this->db->get('traffic_alerts');
		
		return $result;
	}
	
	
	function getStatus($aid)
	{
		$this->db->where('id', $aid);
		$result = $this->db->get('status_updates');
		
		return $result;
	}
	
	function getVetsPerLetter($letter)
	{
		$sql = 'SELECT * FROM vets WHERE UPPER( LEFT( lastname, 1 ) ) = UPPER(?)';
		$resultset = $this->db->query($sql, array($letter));
		return $resultset->result_array();
	}
	
	function getVet($vet_id)
	{
		$sql = 'SELECT * FROM vets WHERE id = ?';
		$resultset = $this->db->query($sql, array($vet_id));
		return $resultset;
	}	
	
	function getFeaturedVets()
	{
		$sql = 'SELECT * FROM vets WHERE featured = "1"';
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function getVetGallery($vet_id)
	{
		$sql = 'SELECT i.fname FROM vets_images vi, images i WHERE vi.vet_id = ? AND i.id = vi.image_id';
		$resultset = $this->db->query($sql, array($vet_id));
		return $resultset;
	}
	
	function getBreedsPerLetter($letter)
	{
		$sql = 'SELECT * FROM breeds WHERE UPPER( LEFT( name, 1 ) ) = UPPER(?) AND accepted = "1"';
		$resultset = $this->db->query($sql, array($letter));
		return $resultset->result_array();
	}
	
	function getBreedsLetter($letter)
	{
		$sql = 'SELECT * FROM breeds WHERE UPPER( LEFT( name, 1 ) ) = UPPER(?) AND accepted = "1"';
		$resultset = $this->db->query($sql, array($letter));
		return $resultset;
	}
	
	function getBreed($breed_id)
	{
		$sql = 'SELECT * FROM breeds WHERE id = ?';
		$resultset = $this->db->query($sql, array($breed_id));
		return $resultset->row();
	}	
	
	function getBreedGallery($breed_id)
	{
		$sql = 'SELECT i.fname FROM breeds_images bi, images i WHERE bi.breed_id = ? AND i.id = bi.image_id';
		$resultset = $this->db->query($sql, array($breed_id));
		return $resultset;
	}
	
	
	
	function getSittersPerLetter($letter)
	{
		$sql = 'SELECT * FROM users WHERE UPPER( LEFT( last_name, 1 ) ) = UPPER(?) AND dogsitter = 1';
		$resultset = $this->db->query($sql, array($letter));
		return $resultset->result_array();
	}
	
	function getSitter($sitter_id)
	{
		$sql = 'SELECT * FROM users WHERE id = ?';
		$resultset = $this->db->query($sql, array($sitter_id));
		return $resultset;
	}
	
	function getFeaturedSitters()
	{
		$sql = 'SELECT * FROM users WHERE dogsitter = "1" AND featured = "1"';
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function getSitterRatings($sitter_id)
	{
		$sql = 'SELECT * FROM sitter_ratings WHERE sitter_id = ?';
		$resultset = $this->db->query($sql, array($sitter_id));
		return $resultset;
	}		
	
	function getRatings($sitter,$user,$table,$column)
	{
		$sql = "SELECT *  FROM  ".$table."  WHERE  ".$column." = ?";
		$resultset = $this->db->query($sql, array($sitter));
		return $resultset;
	}
	
	function rate($sitter_id,$user_id,$rating,$table,$column)
	{
		$sql = 'INSERT INTO `'.$table.'`(`'.$column.'`, `user_id`, `rating`) VALUES (?,?,?)';
		$resultset = $this->db->query($sql, array($sitter_id,$user_id,$rating));
		return $resultset;
	}

	function checkRating($sitter,$user,$table,$column){
	    $sql = "SELECT *  FROM  ".$table."  WHERE  ".$column." = ? AND user_id = ?";
	    $result = $this->db->query($sql, array($sitter,$user));
		if ($result->num_rows() > 0) {return true;}else {return false;}
    }
	
	function getZonesPerLetter($letter)
	{
		$sql = 'SELECT * FROM dogzones WHERE UPPER( LEFT( name, 1 ) ) = UPPER(?)';
		$resultset = $this->db->query($sql, array($letter));
		return $resultset->result_array();
	}
	
	
	function getZone($vet_id)
	{
		$sql = 'SELECT * FROM dogzones WHERE id = ?';
		$resultset = $this->db->query($sql, array($vet_id));
		return $resultset;
	}	
	
	function getZoneGallery($vet_id)
	{
		$sql = 'SELECT i.fname FROM dogzone_images vi, images i WHERE vi.dogzone_id = ? AND i.id = vi.image_id';
		$resultset = $this->db->query($sql, array($vet_id));
		return $resultset;
	}
	
	
	
	function getSingleAlbum($album_id)
	{
		$sql = 'SELECT * FROM albums WHERE id = ? ';
		$resultset = $this->db->query($sql, array($album_id));
		return $resultset->row();
	}
	
	function addMobileAlbum($title,$about,$vehicle_id)
	{
		$sql = "INSERT INTO albums (title, about, vehicle_id, mobile_upload) VALUES (?, ?, ?, 1);";
		$resultset = $this->db->query($sql, array($title,$about,$user_id));
		return $this->db->insert_id();
	}
	
	function addInstagramAlbum($title,$about,$user_id)
	{
		$sql = "INSERT INTO albums (title, about, created_by, instagram) VALUES (?, ?, ?, 1);";
		$resultset = $this->db->query($sql, array($title,$about,$user_id));
		return $this->db->insert_id();
	}
	
	
	function getMobileAlbum($vehicle_id)
	{
		$sql = 'SELECT * FROM albums WHERE vehicle_id = ? AND mobile_upload = 1 LIMIT 1';
		$resultset = $this->db->query($sql, array($vehicle_id));
		return $resultset->row();		
	}
	
	function editSingleAlbum($album_id)
	{
		$sql = 'SELECT * FROM albums WHERE id = ?';
		$resultset = $this->db->query($sql, array($album_id));
		return $resultset->row();
	}
		
	function addAlbum($title,$about,$vehicle_id)
	{
		$sql = "INSERT INTO albums (title, about, vehicle_id) VALUES (?, ?, ?);";
		$resultset = $this->db->query($sql, array($title,$about,$vehicle_id));
		return $this->db->insert_id();
	}
	
	function addAlbumNewsfeed($title,$about,$vehicle_id)
	{
		$sql = "INSERT INTO albums (title, about, vehicle_id, newsfeed) VALUES (?, ?, ?, 1);";
		$resultset = $this->db->query($sql, array($title,$about,$vehicle_id));
		return $this->db->insert_id();
	}
	
		function editAlbum($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('albums', $data);	

	}
	
	function addPicture($fname,$album_id,$user_id, $cover_image = 0, $profile_image = 0, $vehicle_id = NULL, $title = NULL)
	{
		$sql = "INSERT INTO images (fname,created_by, cover_image, profile_image, vehicle_id, title) VALUES (?,?,?,?,?,?);";
		$resultset = $this->db->query($sql, array($fname,$user_id,$cover_image,$profile_image, $vehicle_id, $title));
		$insert_id = $this->db->insert_id();
		
		$sql = "INSERT INTO albums_images (album_id,image_id,created_by, title) VALUES (?,?,?,?);";
		$resultset = $this->db->query($sql, array($album_id, $insert_id, $user_id, $title));
		return $insert_id;
	}
	
	function addPOIPicture($fname,$poi_id, $vehicle_id)
	{
		$sql = "INSERT INTO poi_images (fname,poi_id,created_by) VALUES (?,?,?);";
		$resultset = $this->db->query($sql, array($fname,$poi_id, $vehicle_id));
		
		
	}
	
	
	function addCoverPicture($fname,$user_id)
	{
		$sql = "INSERT INTO images (fname,created_by) VALUES (?,?);";
		$resultset = $this->db->query($sql, array($fname,$user_id));
		$insert_id = $this->db->insert_id();
		
		
	}

	function addProfilePicture($fname,$user_id)
	{
		$sql = "INSERT INTO images (fname,created_by) VALUES (?,?);";
		$resultset = $this->db->query($sql, array($fname,$user_id));
		$insert_id = $this->db->insert_id();
		
		
	}
	
	function addPictureMobile($fname,$album_id,$user_id, $title, $vehicle_id)
	{
		$sql = "INSERT INTO images (fname, created_by, vehicle_id) VALUES (?,?, ?);";
		$resultset = $this->db->query($sql, array($fname,$user_id, $vehicle_id));
		$insert_id = $this->db->insert_id();
		
		$sql = "INSERT INTO albums_images (album_id,image_id,created_by, title) VALUES (?,?,?,?);";
		$resultset = $this->db->query($sql, array($album_id, $insert_id, $user_id, $title));
		return $insert_id;
	}
	
	function addPictureInstagram($fname,$album_id,$user_id, $title)
	{
		$sql = "INSERT INTO images (fname, created_by) VALUES (?,?);";
		$resultset = $this->db->query($sql, array($fname,$user_id));
		$insert_id = $this->db->insert_id();
		
		$sql = "INSERT INTO albums_images (album_id,image_id,created_by, title) VALUES (?,?,?,?);";
		$resultset = $this->db->query($sql, array($album_id, $insert_id, $user_id, $title));
		return $insert_id;
	}
	
	
	function getPicturesFromAlbum($album_id)
	{
		$sql = 'SELECT i.fname, i.id, ai.title, i.likes, i.cover_image, i.profile_image FROM albums_images ai, images i WHERE ai.album_id = ? AND i.id = ai.image_id ORDER BY i.created_date DESC';
		$resultset = $this->db->query($sql, array($album_id));
		return $resultset;
	}
	
	function getPicture($pic_id)
	{
		$sql = 'SELECT i.id, i.fname, ai.title, i.likes, i.cover_image, i.profile_image, i.created_by FROM  images i, albums_images ai WHERE i.id = ? AND ai.image_id = i.id';
		$resultset = $this->db->query($sql, array($pic_id));
		return $resultset;
	}
	
	function getPicturebyID($pic_id)
	{
		$this->db->where('id', $pic_id);		
		return $this->db->get('images');	
	
	}

	
	function getTaggedPicture($dog_id)
	{
		$sql = 'SELECT * FROM  tagged_dogs WHERE dog_id = ?';
		$resultset = $this->db->query($sql, array($dog_id));
		return $resultset;
	}
	
	
	function getActivities($user_id)
	{
		$sql = 'SELECT * FROM activities WHERE created_by = ?';
		$resultset = $this->db->query($sql, array($user_id));
		return $resultset;
	}
	
	function getActivity($activity_id)
	{
		$sql = 'SELECT * FROM activities WHERE id = ?';
		$resultset = $this->db->query($sql, array($activity_id));
		return $resultset->row();
	}
	
	
	function addActivity($place,$date,$time,$about,$user_id)
	{
		$sql = "INSERT INTO activities (place, date, time, about, created_by) VALUES (?, ?, ?, ?, ?);";
		$resultset = $this->db->query($sql, array($place, $date, $time, $about, $user_id));
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	function getActivityForEdit($activity_id,$user_id)
	{
		$sql = 'SELECT * FROM activities WHERE id = ? AND created_by = ?';
		$resultset = $this->db->query($sql, array($activity_id,$user_id));
		return $resultset->row();
	}
	
	function getGuestsForActivity($activity_id)
	{
		$sql = 'SELECT d.profile_picture, d.name, d.id, ai.pending FROM dogs d, activities_invites ai WHERE ai.activity_id = ? AND d.id = ai.dog_id';
		$resultset = $this->db->query($sql, array($activity_id));
		return $resultset;
	}
	
	
	function getFriendsForActivity($user_id)
	{
		$sql = 'SELECT d.profile_picture, d.name, d.id FROM dogs d, friends f WHERE f.created_by = ?';
		$resultset = $this->db->query($sql, array($user_id));
		return $resultset;
	}
	
	function addActivityGuest($dog_id,$activity_id)
	{
		$sql = "INSERT INTO activities_invites (activity_id, dog_id) VALUES (?, ?);";
		$resultset = $this->db->query($sql, array($activity_id,$dog_id));
		return $resultset;
	}
	
		function editActivity($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('activities', $data);	

	}
	
	function getPendingActivities($dog_id)
	{
		$sql = 'SELECT a.id, a.created_time, a.date, a.time, a.place, a.about, a.created_by FROM activities a, activities_invites ai WHERE ai.dog_id = ? AND ai.activity_id = a.id AND ai.pending = 1 AND ai.accepted = 0';
		$resultset = $this->db->query($sql, array($dog_id));
		return $resultset;
	}
	
		function editInvites($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('activities_invites', $data);	

	}
	
	function getMyInvites($activity_id, $dog_id)
	{
		$sql = 'SELECT * FROM activities_invites WHERE activity_id = ? AND dog_id = ?';
		$resultset = $this->db->query($sql, array($activity_id, $dog_id));
		return $resultset;
	}
	
	function getAcceptedActivities($dog_id)
	{
		$sql = 'SELECT a.id, a.created_time, a.date, a.time, a.place, a.about, a.created_by FROM activities a, activities_invites ai WHERE ai.dog_id = ? AND ai.activity_id = a.id AND ai.pending = 0 AND ai.accepted = 1';
		$resultset = $this->db->query($sql, array($dog_id));
		return $resultset;
	}
	
	function addSuggestion($name,$address,$about,$size, $user_id)
	{
		$sql = "INSERT INTO zone_suggestions (name, description, address, size, created_by) VALUES (?, ?, ?, ?, ?);";
		$resultset = $this->db->query($sql, array($name,$about,$address,$size, $user_id));
		return $resultset;
	}
	
	function getZones()
	{
		$sql = 'SELECT * FROM dogzones ';
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function getSitters()
	{
		$sql = 'SELECT * FROM users WHERE dogsitter = 1 ';
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	
	function getVets()
	{
		$sql = 'SELECT * FROM vets';
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	
	function nearbyUsers()
	{
		$sql = 'SELECT * FROM users WHERE `geo_location` IS NOT NULL AND `geo_location` !=""';
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function addEvent($action_id, $entity_id, $additional_id = null)
	{
		$sql = "INSERT INTO events (action_id, entity_id, additional_id) VALUES (?, ?, ?);";
		$resultset = $this->db->query($sql, array($action_id,$entity_id, $additional_id));
		return $this->db->insert_id();
	}
	
	
	function addEventVehicle($vehicle_id,$event_id)
	{
		$sql = "INSERT INTO event_vehicles (vehicle_id,event_id) VALUES (?, ?);";
		$resultset = $this->db->query($sql, array($vehicle_id,$event_id));
		return $this->db->insert_id();
	}
	
	function getEvents($id)
	{
		$sql = 'SELECT e.action_id, e.entity_id, e.created_date, e.id  FROM events e, event_dogs ed  WHERE ed.dog_id = ? AND ed.event_id = e.id ORDER BY e.created_date DESC ';
		$resultset = $this->db->query($sql, array($id));
		if ($resultset->num_rows() > 0) {
				 return $resultset;
			} else {
				return FALSE;
			} 
	}
	
	function getAllEvents()
	{
		$sql = 'SELECT e.action_id, e.entity_id, e.additional_id, e.created_date, ed.vehicle_id , e.id, e.likes FROM events e, event_vehicles ed  WHERE ed.event_id = e.id ORDER BY e.created_date DESC, RAND()';
		$resultset = $this->db->query($sql);
		return $resultset;
			 
		
	}
	
	
	function addNotification($action_id, $entity_id, $additional_id = null)
	{
		$sql = "INSERT INTO notifications (action_id, entity_id, additional_id) VALUES (?, ?, ?);";
		$resultset = $this->db->query($sql, array($action_id,$entity_id, $additional_id));
		return $this->db->insert_id();
	}
	
	
	function addNotificationVehicle($vehicle_id,$event_id)
	{
		$sql = "INSERT INTO notification_vehicles (vehicle_id,event_id) VALUES (?, ?);";
		$resultset = $this->db->query($sql, array($vehicle_id,$event_id));
		return $this->db->insert_id();
	}
	
	function getNotifications($id)
	{
		$sql = 'SELECT e.action_id, e.entity_id, e.created_date, e.id, e.has_seen  FROM notifications e, notification_vehicles ed  WHERE ed.vehicle_id = ? AND ed.event_id = e.id ORDER BY e.created_date DESC ';
		$resultset = $this->db->query($sql, array($id));
		if ($resultset->num_rows() > 0) {
				 return $resultset;
			} else {
				return FALSE;
			} 
	}
	
	function getAllNotifications()
	{
		$sql = 'SELECT e.action_id, e.entity_id, e.additional_id, e.created_date, ed.vehicle_id , e.id, e.has_seen FROM notifications e, notification_vehicles ed  WHERE ed.event_id = e.id ORDER BY e.created_date DESC, RAND()';
		$resultset = $this->db->query($sql);
		return $resultset;
			 
		
	}
	
	
	function getTimetableSitter($id)
	{
		$sql = 'SELECT * FROM sitter_timeframe WHERE sitter_id = ?';
		$resultset = $this->db->query($sql, array($id));
		return $resultset;		
	}
	
	
	function getBookingsForDate($time_id)
	{
		$sql = 'SELECT * FROM sitter_booking WHERE timeframe_id = ?';
		$resultset = $this->db->query($sql, array($time_id));
		return $resultset->num_rows();		
	}
	
	function updatePictureTitle($data, $pid)
	{
		$this->db->where('image_id', $pid);
		$this->db->update('albums_images', $data);	

	}
	
	function saveHashtagImage($fname, $url, $date, $tags, $username, $created_date) {
	    $sql = "INSERT INTO  `hashtags` ( `fname`, `target_url`, `upload_date`, `tags`, `username`, `created_date` ) VALUES (".$this->db->escape($fname).", ".$this->db->escape($url).", ".$this->db->escape($date).", ".$this->db->escape($tags).", ".$this->db->escape($username).", ".$this->db->escape($created_date).");";
	    $this->db->query($sql); 
	    return $this->db->insert_id();
    }
    
    function getHashtags(){
	    $sql = "SELECT *  FROM  `hashtags` ORDER BY `upload_date` DESC";
	    $result = $this->db->query($sql);
	    
	    return $result;  
  	}
  	
  	function getLastImage(){
	    $sql = "SELECT *  FROM  `hashtags` ORDER BY `upload_date` DESC LIMIT 1";
	    $result = $this->db->query($sql);
	    
	    return $result;  
  	}
  	
  	function checkUserHashtag($user_id)
	{
		$sql = 'SELECT show_hashtags FROM users  WHERE id = ?';
		$resultset = $this->db->query($sql, array($user_id));
		return $resultset;
	}
  	
  	function getAlbumFromPicture($picture)
	{
		$sql = 'SELECT * FROM albums_images WHERE image_id = ? ';
		$resultset = $this->db->query($sql, array($picture));
		return $resultset;
	}

  	function addDeleteRequest($user_id)
	{
		$sql = "INSERT INTO delete_requests (user_id) VALUES (?);";
		$resultset = $this->db->query($sql, array($user_id));
		return $this->db->insert_id();
	}
	
	function getPartner($partner_id)
	{
		$sql = 'SELECT * FROM partners WHERE id = ?';
		$resultset = $this->db->query($sql, array($partner_id));
		return $resultset;
	}
	
	function getPartnerAdvert($partner_id)
	{
		$sql = 'SELECT * FROM partner_advertisements WHERE partner_id = ?';
		$resultset = $this->db->query($sql, array($partner_id));
		if ($resultset->num_rows() > 0) {
				 return $resultset;
			} else {
				return FALSE;
			} 
	}
  	
  	function getConversion()
	{
		$sql = 'SELECT * FROM impression_conversion WHERE id = 1';
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function updateAdvert($data)
	{
		$this->db->where('partner_id', $data['partner_id']);
		$this->db->update('partner_advertisements', $data);	

	}
	
	
	function VetSearchByName($like)
	{
		$sql = 'SELECT * FROM vets WHERE firstname LIKE ? OR lastname LIKE ? OR company_name LIKE ?';
		$resultset = $this->db->query($sql, array('%'.$like.'%', '%'.$like.'%', '%'.$like.'%'));
		return $resultset;
	}
	
	function PlaceSearchByName($like)
	{
		$sql = 'SELECT * FROM misc_places WHERE name LIKE ?';
		$resultset = $this->db->query($sql, array('%'.$like.'%'));
		return $resultset;
	}
	
	function SitterSearchByName($like)
	{
		$sql = 'SELECT * FROM users WHERE dogsitter = 1 AND  (first_name LIKE ? OR last_name LIKE ?)';
		$resultset = $this->db->query($sql, array('%'.$like.'%', '%'.$like.'%'));
		return $resultset;
	}
	
	function ZoneSearchByName($like)
	{
		$sql = 'SELECT * FROM dogzones WHERE name LIKE ? ';
		$resultset = $this->db->query($sql, array('%'.$like.'%'));
		return $resultset;
	}
	
	
	
	function updateUser($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('users', $data);	

	}
	
	function getUserByEmail($email)
	{
		$sql = 'SELECT * FROM users WHERE email = ? ';
		$resultset = $this->db->query($sql, array($email));
		return $resultset;
	}
	
	function removeEventDog($dog_id,$action)
	{
		$sql = "DELETE FROM events WHERE entity_id = ? AND action_id = ?";
		$resultset = $this->db->query($sql, array($dog_id,$action));
		
	}
	
	function deleteAlbum($album_id)
	{
		$sql = "DELETE FROM albums WHERE id = ?";
		$this->db->query($sql, array($album_id));
		
		$sql = "DELETE FROM albums_images WHERE album_id = ?";
		$this->db->query($sql, array($album_id));
		
	}
	
	function deleteEventImages($image_id, $event_id, $event_type)
	{
		$sql = "DELETE FROM albums_images WHERE image_id = ?";
		$this->db->query($sql, array($image_id));
		
		$sql = "DELETE FROM events WHERE action_id = ? AND id = ?";
		$this->db->query($sql, array($event_type, $event_id));
		
		$sql = "DELETE FROM event_vehicles WHERE event_id = ?";
		$this->db->query($sql, array($event_id));
		
		$sql = "DELETE FROM images WHERE id = ?";
		$this->db->query($sql, array($image_id));
	}
	
	function getEventId($entity_id, $event_type)
	{
		$sql = 'SELECT * FROM events WHERE action_id = ? AND entity_id = ? ';
		$resultset = $this->db->query($sql, array($event_type, $entity_id));
		return $resultset;
	}
	
	function getEventIdProfile($image_id, $event_type)
	{
		$sql = 'SELECT * FROM events WHERE action_id = ? AND additional_id = ? ';
		$resultset = $this->db->query($sql, array($event_type, $image_id));
		return $resultset;
	}
	
	function getCities()
	{
		$sql = 'SELECT * FROM cities ';
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function getUsers()
	{
		$sql = 'SELECT * FROM users';
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function getNewsfeedAlbum($vehicle_id)
	{
		$sql = 'SELECT * FROM albums WHERE vehicle_id = ? AND newsfeed = 1';
		$resultset = $this->db->query($sql, array($vehicle_id));
		if ($resultset->num_rows() > 0) {
			 return $resultset;
		} else {
			return FALSE;
		}
	}
	
	function addZoneSuggestion($data)
	{
		$this->db->insert('dogzone_descriptions', $data);		
		
	}
	
	function updateLocation($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('users', $data);	

	}
	
	function updateLocationVehicle($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('vehicles', $data);	

	}
	
	
	
	function addLike($pic_id, $user_id)
	{
		$sql = "UPDATE images SET likes = likes + 1 WHERE id = ?";
		$this->db->query($sql, array($pic_id));
		
		$this->db->insert('image_likes', array('image_id' => $pic_id, 'user_id' => $user_id));
	}
	
	
	function addEventLike($event_id, $user_id)
	{
		$sql = "UPDATE events SET likes = likes + 1 WHERE id = ?";
		$this->db->query($sql, array($event_id));
		
		$this->db->insert('event_likes', array('event_id' => $event_id, 'user_id' => $user_id));
	}
	
	
	function checkPictureLike($pic, $user_id)
	{
		$sql = 'SELECT * FROM image_likes WHERE user_id = ? AND image_id = ?';
		$resultset = $this->db->query($sql, array($user_id, $pic));
		if ($resultset->num_rows() > 0) {
			 return 1;
		} else {
			return 0;
		}
	}
	
	
	function checkEventLike($event_id, $user_id)
	{
		$sql = 'SELECT * FROM event_likes WHERE user_id = ? AND event_id = ?';
		$resultset = $this->db->query($sql, array($user_id, $event_id));
		if ($resultset->num_rows() > 0) {
			 return 1;
		} else {
			return 0;
		}
	}
	
	
	function addPhotoComment($data)
	{
		$this->db->insert('image_comments', $data);		
		
	}
	
	function getNewsPhotoComments($pic_id)
	{
		$this->db->where('image_id', $pic_id);
		return $this->db->get('image_comments');
	}
	
	
	function getPhotoComments($pic_id)
	{
		$sql = 'SELECT i.image_id, i.user_id, i.comment, i.id, i.created_date, i.visible, u.first_name, u.last_name FROM image_comments i, users u  WHERE i.user_id = u.id AND i.image_id = "'.$pic_id.'" ORDER BY i.created_date DESC';
		$resultset = $this->db->query($sql);
		return $resultset;
	/*	$this->db->order_by('created_date', 'DESC');
		//$this->db->limit('5');
		$this->db->where('image_id', $pic_id);
		return $this->db->get('image_comments');*/	

	}
	
	function getUserInstagramAlbum($user_id)
	{		
		$this->db->where('created_by', $user_id);
		$this->db->where('instagram', 1);
		return $this->db->get('albums');	

	}
	
	function getUserInstagramAlbumLastImage($album_id)
	{		
		$this->db->order_by('created_date', 'DESC');
		$this->db->where('album_id', $album_id);
		return $this->db->get('albums_images');	

	}
	
	
	function getImageByID($pic_id)
	{		
		$this->db->where('id', $pic_id);
		return $this->db->get('images');	

	}
	
	function getPlaces()
	{				
		return $this->db->get('misc_places');	
	}
	
	function getPlaceByID($id)
	{				
		$this->db->where('id', $id);
		return $this->db->get('misc_places');	
	}
	
	
	function getPlacesFeatured()
	{		
		$this->db->where('featured', 1);
		return $this->db->get('misc_places');	

	}
	
	function getPlaceTypeByID($id)
	{		
		$this->db->where('id', $id);
		return $this->db->get('misc_place_type');	

	}
	
	function getPrettyUrlPlace($pretty)
	{		
		$this->db->where('pretty_url', $pretty);
		return $this->db->get('misc_places');	

	}
	
	function updateImage($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('images', $data);	

	}
	
	function getComment($comment_id)
	{
		$this->db->where('id', $comment_id);
		return $this->db->get('image_comments');			
		
	}	
	
	function deleteComment($comment_id)
	{
		$this->db->where('id', $comment_id);
		$this->db->delete('image_comments');			
		
	}
	
	function getPartners()
	{
		$this->db->where('impressions_left	>', 0);
		$this->db->order_by('id', 'RANDOM');
		return $this->db->get('partners');			
		
	}
	
	
	function getAdvertisement($partner_id)
	{
		$this->db->where('partner_id', $partner_id);
		return $this->db->get('partner_advertisements');			
		
	}
	
	
	
	function updateImpressions($id)
	{
		$sql = "UPDATE partners SET impressions_left = impressions_left - 1 WHERE id = ?";
		$this->db->query($sql, array($id));
		
	}
	
	function getRandomDogs()
	{
		$sql = "SELECT * FROM dogs ORDER BY RAND() LIMIT 30";
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function addRedirectData($data)
	{
		$this->db->insert('redirect_counter', $data);		
		
	}
	
	
	
	function getUserByUsername($username)
	{
		$this->db->where('username', $username);
		return $this->db->get('users');
	}
	
	
	function sendMessage($data)
	{
		$this->db->insert('messages', $data);		
		
	}
	
	function getMessages($id)
	{
		$this->db->where('sender_id', $id);
     	$this->db->or_where('receiver_id',$id);
		$this->db->order_by('sent_time', 'ASC');
		$result = $this->db->get('messages');	
		if ($result->num_rows() > 0) {
			 return $result;
		} else {
			return false;
		} 		
		
	}

	function getMessagesFromUser($partner, $user)
	{
		$this->db->where('sender_id = '.$user.' AND receiver_id = '.$partner);
     	$this->db->or_where('receiver_id = '.$user.' AND sender_id = '.$partner);
		$this->db->order_by('sent_time', 'DESC');
		return $this->db->get('messages');	

	}
	
	function editMessage($data)
	{
		$this->db->where('sender_id', $data['sender_id']);
		$this->db->update('messages', $data);	

	}
	
	function deleteMessage($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('messages');	

	}
	
	function editNotification($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('notifications', $data);	

	}
	
	function editNotificationVehicle($id,$data)
	{
		$this->db->where('vehicle_id', $id);
		$this->db->update('notification_vehicles', $data);	

	}


	function VehicleSearchByName($like)
	{
		$sql = 'SELECT * FROM vehicles WHERE nickname LIKE ? ';
		$resultset = $this->db->query($sql, array('%'.$like.'%'));
		return $resultset;
	}
	
	
	function VehicleSearchByMake($like, $table, $column)
	{
		$sql = 'SELECT * FROM '.$table.' WHERE '.$column.' LIKE ? ';
		$resultset = $this->db->query($sql, array('%'.$like.'%'));
		return $resultset;
	}
	
	
	function UserSearchByName($like)
	{
		$sql = 'SELECT * FROM users WHERE first_name LIKE ? OR last_name LIKE ? ';
		$resultset = $this->db->query($sql, array('%'.$like.'%', '%'.$like.'%'));
		return $resultset;
	}

	
	function addGroup($data)
	{
		$this->db->insert('managed_groups', $data);		
		return $this->db->insert_id();
	}
	
	
	
	function getNewGroups()
	{
		$this->db->where('accepted', 0);
		$this->db->where('declined', 0);
		$result = $this->db->get('managed_groups');
		
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();
	}

	
	function updateNewGroup($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('managed_groups', $data);	

	}
	
	
	function addGroupUserRole($data)
	{
		$this->db->insert('managed_group_members', $data);		
		
	}
	
	function addGroupRequest($data)
	{
		$this->db->insert('managed_group_requests', $data);		
	}
	
	function addGroupEvent($data)
	{
		$this->db->insert('managed_group_events', $data);	
		return $this->db->insert_id();	
	}
	
	function addUserEvent($data)
	{
		$this->db->insert('user_created_events', $data);	
		return $this->db->insert_id();	
	}
	
	function getGroups()
	{
		$this->db->where('accepted', 1);
		$result = $this->db->get('managed_groups');
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();	

	}
	
	function groupSearch($search)
	{
		$this->db->like('name', $search);
		return $this->db->get('managed_groups');
	}
	
	function getGroupById($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get('managed_groups');
		return $result;	

	}
	
	
	function getGroupRole($grp_id, $user_id)
	{
		$this->db->where('group_id', $grp_id);
		$this->db->where('vehicle_id', $user_id);
		$result = $this->db->get('managed_group_members');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;

	}
	
	
	function getGroupEvents($grp_id)
	{
		$this->db->where('group_id', $grp_id);
		$result = $this->db->get('managed_group_events');
		return ( $result->num_rows() > 0 ) ? $result->result() : false;

	}
	
	function getGroupEventsByID($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get('managed_group_events');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;

	}
	
	function getGroupPosts($grp_id)
	{
		$this->db->order_by('created_date', 'DESC');
		$this->db->where('group_id', $grp_id);
		$result = $this->db->get('managed_group_posts');
		return ( $result->num_rows() > 0 ) ? $result->result() : false;

	}
	
	
	function getGroupMembers($group_id)
	{
		$sql = 'SELECT u.id, u.nickname, u.pretty_url, u.profile_image FROM managed_group_members g, vehicles u  WHERE g.vehicle_id = u.id AND g.group_id = "'.$group_id.'"';
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	
	
	function getGroupApplications($group_id)
	{
		$sql = 'SELECT u.id, u.nickname, u.pretty_url, u.profile_image FROM managed_group_requests g, vehicles u  WHERE g.accepted = 0 AND g.vehicle_id = u.id AND g.group_id = "'.$group_id.'"';
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	
	function editGroup($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('managed_groups', $data);	

	}
	
	function leaveGroup($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('managed_group_members');
	}
	
	function removeGroupRequest($data)
	{
		$this->db->where('group_id', $data['group_id']);
		$this->db->where('vehicle_id', $data['vehicle_id']);
		$this->db->delete('managed_group_requests');
	}
	
	
	function getVehicleGroups($vehicle_id)
	{
		$sql = 'SELECT g.id, g.name, g.logo, g.category_id FROM managed_group_members m, managed_groups g  WHERE g.id = m.group_id AND m.vehicle_id = "'.$vehicle_id.'"';
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	
	function getPOIs()
	{
		$this->db->where('accepted', 1);
		$result = $this->db->get('poi');
		return $result;
	}
	
	function getPOICategories()
	{
	
		$result = $this->db->get('poi_categories');
		return $result;

	}
	
	
	function getPOICategory($id)
	{
		$this->db->where('id', $id);	
		$result = $this->db->get('poi_categories');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;
	}
	
	
	function getPOICategoriesConnection($id)
	{
		$this->db->where('poi_id', $id);	
		$result = $this->db->get('poi_category_connection');
		return ( $result->num_rows() > 0 ) ? $result->result() : array();
	}
	
	
	function getPOI($id)
	{
		$this->db->where('id', $id);
		$this->db->where('accepted', 1);	
		$result = $this->db->get('poi');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;
	}
	
	
	function getEventById($event_id)
	{
		$this->db->where('id', $event_id);
		$result = $this->db->get('managed_group_events');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;

	}
	
	
	function getNewsEventById($event_id)
	{
		$this->db->where('id', $event_id);
		$result = $this->db->get('events');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;

	}
	
	function getNewsEventByPicId($pic_id)
	{
		$this->db->where('additional_id', $pic_id);
		$result = $this->db->get('events');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;

	}
	
	function getUserEventById($event_id)
	{
		$this->db->where('id', $event_id);
		$result = $this->db->get('user_created_events');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;

	}
	
	
	function addVehicleToEvent($data)
	{
		$this->db->insert('managed_group_event_vehicles', $data);		
	}
	
	function addVehicleToUserEvent($data)
	{
		$this->db->insert('user_created_event_vehicles', $data);		
	}
	
	
	
	
	function checkVehicleAtUserEvent($event_id, $vehicle_id)
	{
		$this->db->where('user_event_id', $event_id);
		$this->db->where('vehicle_id', $vehicle_id);
		$result = $this->db->get('user_created_event_vehicles');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;

	}
	
	
	function checkVehicleAtEvent($event_id, $vehicle_id)
	{
		$this->db->where('event_id', $event_id);
		$this->db->where('vehicle_id', $vehicle_id);
		$result = $this->db->get('managed_group_event_vehicles');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;

	}
	
	function updateVehicleToEvent($event_id,$vehicle_id,$data)	
	{
		$this->db->where('event_id', $event_id);
		$this->db->where('vehicle_id', $vehicle_id);
		$this->db->update('managed_group_event_vehicles', $data);
	}
	
	function updateVehicleToUserEvent($event_id,$vehicle_id,$data)	
	{
		$this->db->where('user_event_id', $event_id);
		$this->db->where('vehicle_id', $vehicle_id);
		$this->db->update('user_created_event_vehicles', $data);
	}
	
	function updateUserEvent($event_id,$data)
	{
		$this->db->where('id', $event_id);
		$this->db->update('user_created_events', $data);
	}
	
	
	function updateGroupEvent($event_id,$data)
	{
		$this->db->where('id', $event_id);
		$this->db->update('managed_group_events', $data);
	}
	
	function getVehiclesFromEvent($event_id)
	{
		$this->db->where('event_id', $event_id);
		$this->db->where('status_id', GROUP_EVENT_GOING);
		$result = $this->db->get('managed_group_event_vehicles');
		return ( $result->num_rows() > 0 ) ? $result->result() : array();

	}
	
	
	function getVehiclesFromUserEvent($event_id)
	{
		$this->db->where('user_event_id', $event_id);
		$this->db->where('status', GROUP_EVENT_GOING);
		$result = $this->db->get('user_created_event_vehicles');
		return ( $result->num_rows() > 0 ) ? $result->result() : array();

	}
	
	function getEventComments($event_id)
	{
		$this->db->where('event_id', $event_id);
		$this->db->order_by('comment_date', 'DESC');
		$result = $this->db->get('event_comments');
		return ( $result->num_rows() > 0 ) ? $result->result() : array();

	}
	
	function getImageComments($image_id)
	{
		$this->db->where('image_id', $image_id);
		$this->db->where('visible', 1);
		$this->db->order_by('created_date', 'DESC');
		$result = $this->db->get('image_comments');
		return ( $result->num_rows() > 0 ) ? $result->result() : array();

	}
	
	
	function addNewsComment($data)
	{
		$this->db->insert('event_comments', $data);		
	}
	
	
	function addGroupPost($data)
	{
		
		$this->db->insert('managed_group_posts', $data);		
	}
	
	function getUserEventsCreated($vehicle_id, $user_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$this->db->or_where('user_id', $user_id);
		$this->db->order_by('start_time', 'ASC');
		$result = $this->db->get('user_created_events');
		return ( $result->num_rows() > 0 ) ? $result->result() : array();

	}
	
	
	function getUserEvents()
	{
		$this->db->where('public', 1);
		$this->db->order_by('start_time', 'ASC');
		$result = $this->db->get('user_created_events');
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();

	}
	
	
	function getPublicGroupEvents()
	{
		$this->db->where('public', 1);
		$result = $this->db->get('managed_group_events');
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();

	}
	
	
	
	function eventSearchTitle($search, $table)
	{
		$this->db->where('public', 1);	
		$this->db->like('title', $search, 'both');
		return $this->db->get($table);
	}
	
	function eventSearchTitleStart($search, $table, $start)
	{
		$this->db->where('start_time >=', $start);	
		$this->db->where('public', 1);	
		$this->db->like('title', $search, 'both');
		return $this->db->get($table);
	}
	
	function eventSearchTitleEnd($search, $table, $end)
	{
		$this->db->where('start_time <=', $end);	
		$this->db->where('public', 1);	
		$this->db->like('title', $search, 'both');
		return $this->db->get($table);
	}
	
	function eventSearchTitleBetween($search, $table, $start, $end)
	{
		$this->db->where('start_time >=', $start);
		$this->db->where('start_time <=', $end);	
		$this->db->where('public', 1);	
		$this->db->like('title', $search, 'both');
		return $this->db->get($table);
	}
	
	function getPoiImages($poi_id)
	{
		$this->db->where('poi_id', $poi_id);
		$result = $this->db->get('poi_images');
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();

	}
	
	function getGroupCategories()
	{
		$result = $this->db->get('managed_group_categories');
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();
	}
	
	function getGroupCategory($cat_id)
	{
		$this->db->where('id', $cat_id);
		$result = $this->db->get('managed_group_categories');
		return $result;

	}
	
	
	function addPOISuggestion($data)
	{
		
		$this->db->insert('poi_suggestions', $data);		
	}
	
	function addPOI($data)
	{
		
		$this->db->insert('poi', $data);
		$insert_id = $this->db->insert_id();

   		return  $insert_id;		
	}
	
	function addPOICategories($data)
	{
		
		$this->db->insert('poi_category_connection', $data);
	
	}
	
	function getVehicleEventIDs($vehicle_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$result = $this->db->get('managed_group_event_vehicles');
		return ( $result->num_rows() > 0 ) ? $result->result() : array();

	}
	
	
	function addStoryPicture($data)
	{
		
		$this->db->insert('story_images', $data);		
	}
	
	function getStoryImages($story_id)
	{
		$this->db->where('story_id', $story_id);
		$result = $this->db->get('story_images');
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();

	}
	
	function addReport($data)
	{
		
		$this->db->insert('reports', $data);		
	}
	
	function getPoints($vehicle_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$result = $this->db->get('vehicle_points');
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();

	}
	
	function getNumMods($vehicle_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$result = $this->db->get('game_counter');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;

	}
	
	
	function checkLastPoints($vehicle_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$this->db->where('comment', 'login');
		$this->db->order_by('created_date', 'desc');
		$this->db->limit(1);
		$result = $this->db->get('vehicle_points');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;

	}
	
	function getUserPoints($user_id)
	{
		$this->db->where('user_id', $user_id);
		$result = $this->db->get('user_points');
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();

	}
	
	function addUserPoints($data)
	{
		$this->db->insert('user_points', $data);
			
	}
	
	function checkLastUserPoints($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->order_by('created_date', 'desc');
		$result = $this->db->get('user_points');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;

	}
	
	function searchPOI($search)
	{
		$this->db->like('name', $search);
		return $this->db->get('poi');
	}
	
	
	function removeMod($mod_id, $data, $table)
	{
		$this->db->where('id', $mod_id);
		$this->db->update($table, $data);
	}

	function addUserSuggestion($data)
	{
		$this->db->insert('suggestions', $data);
			
	}
	
	function deleteClub($clubId)
	{
		$sql = "DELETE FROM managed_groups WHERE id = ?";
		$this->db->query($sql, array($clubId));
		
	}
	
	function deleteUserEvent($event_id)
	{
		$sql = "DELETE FROM user_created_events WHERE id = ?";
		$this->db->query($sql, array($event_id));
		
	}
	
	function deleteGroupEvent($event_id)
	{
		$sql = "DELETE FROM managed_group_events WHERE id = ?";
		$this->db->query($sql, array($event_id));
		
	}
	
	
	function updateGroupRole($group_id, $vehicle_id, $data)
	{
		$this->db->where('group_id', $group_id);
		$this->db->where('vehicle_id', $vehicle_id);
		$this->db->update('managed_group_members', $data);
	}
}
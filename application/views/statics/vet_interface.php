<script>	
	$('#sidemenu').css({top: 0});
	
	$(document).ready(function()
	{	
		$("#downloadBG, #socialBG, #welcomeImg").hide();
		
		initMapProfile();
	});
	
	
	
</script>
<div id="image_cropping_overlay">
	<div class="statics_subheader" id="crop_title" style="position:relative;top:0px;"><?= $this->lang->line('profile_image')?></div>
	<div class="sansitalic" style="margin:5px auto;text-align: center;"><?= $this->lang->line('please_select_area')?></div>
	<div id="image_crop_holder">
		
	</div>
	 <input type="hidden" id="selection_x1" name="selection_x1" value="" />
	 <input type="hidden" id="selection_x2" name="selection_x2" value="" />
	 <input type="hidden" id="selection_y1" name="selection_y1" value="" />
	 <input type="hidden" id="selection_y2" name="selection_y2" value="" />
	<div class="dog_new_create_arrow_text" uid="<?= $this->session->userdata('vet_id');?>" id="finish_crop_vet" image_type="" style="width:190px;border:0;left:317px;" dog_id="<?= $dog->id; ?>">Finish</div>
	<div class="dog_new_create_arrow_text" id="close_crop"  style="width:190px;border:0;left:517px;top:-17px;" ><?= $this->lang->line('close')?></div>
</div>	
<div id="content" style="min-height: 970px;">
				<div  class="statics_subheader" style="margin-top:35px;">
					<?= $this->lang->line('vet_login_edit')?>
				</div>
				<div id="edit_error_message">
					<?php if(isset($message)) echo $message;?>
				</div>
				<div id="edit_form">
					

					<?php 
						$data = array('id' => 'edit_user_form');
						echo form_open_multipart("auth/edit_vet", $data);
					?>
					
					<div id="vet_edit_profile_upload" formfield="vet_edit_profile_picture" class="pseudo_image_upload">
							<img style="width:100%;" src="<?= ($vet->profilepicture != "") ? site_url('items/uploads/profilepictures/' . $vet->profilepicture) : site_url('items/frontend/img/profilepicture_upload.png')?>" border="0">
						</div>
						<input type="file" preview="vet_edit_profile_upload" accept="image/*" id="vet_edit_profile_picture" name="dog_edit_profile_picture" class="dog_edit_hidden_upload">
					
					<div style="position: relative;left: 0px;top:10px;font-size: 1.5em;"><?= $this->lang->line('register_firstname')?></div>					
					<input type="text" id="edit_firstname" name="edit_firstname" 
						value="<?= $vet->firstname?>" 
						defaultText="<?= $this->lang->line('register_firstname')?>" 
						class="edit_input">
					<br>
					<br>
					<div style="position: relative;left: 0px;top:10px;font-size: 1.5em;"><?= $this->lang->line('register_lastname')?></div>
					<input type="text" id="edit_lastname" name="edit_lastname" 
						value="<?= $vet->lastname?>" 
						defaultText="<?= $this->lang->line('register_lastname')?>" 
						class="edit_input">
					<br>
					<br>
					<div style="position: relative;left: 0px;top:10px;font-size: 1.5em;"><?= $this->lang->line('vet_title')?></div>
					<input type="text" id="edit_title" name="edit_title" 
						value="<?= $vet->title?>" 
						defaultText="<?= $this->lang->line('vet_title')?>" 
						class="edit_input">
					
					<br>
					<br>
					<div style="position: relative;left: 0px;top:10px;font-size: 1.5em;"><?= $this->lang->line('edit_user_contact')?></div>
					
					<div style="height:130px;">
						<textarea style="text-align:left;resize:none;height:100px;font-size:14px;width:200px;line-height:20px;" type="text" id="edit_contact" name="edit_contact" class="edit_input"><?= $vet->contact == NULL ? "" : strip_tags($vet->contact) ?></textarea>
					</div>
					
					
					<br>
					
					
					<div style="position: relative;left: 0px;top:10px;font-size: 1.5em;"><?= $this->lang->line('vet_detail_times')?>:</div>
					
					<div>
						<textarea style="text-align:left;resize:none;height:100px;font-size:14px;width:200px;line-height:20px;" type="text" id="edit_ordination" name="edit_ordination" class="edit_input"><?= $vet->times == NULL ? "" : strip_tags($vet->times) ?></textarea>
					</div>
					
					
					<br>
					
					
					<div id="edit_dogsitter_data" style="display:block;top:0px;";>
						<div style="position: relative;left: 0px;top:0px;font-size: 1.5em;"><?= $this->lang->line('edit_user_available_text')?></div>					
						<textarea style="text-align:left;resize:none;height:100px;font-size:14px;width:327px;line-height:20px;" type="text" id="edit_about" name="edit_about" class="edit_input"><?= $vet->description == NULL ? "" : strip_tags($vet->description) ?></textarea>
						<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
						<div style="position: relative;left: 0px;top:0px;font-size: 1.5em;height:30px;"><?= $this->lang->line('location')?></div>							
						<input type="hidden" id="edit_state" name="edit_state" 
							value="<?= $vet->state == NULL ? $this->lang->line('register_state') : $vet->state ?>" 
							defaultText="<?= $this->lang->line('register_state')?>" 
							class="edit_input">
					<div style="display:none;">		
						<br>
						<br>
							<div style="position: relative;left: 0px;top:0px;font-size: 12px;"><?= $this->lang->line('register_zip')?></div>
							<input type="text" id="edit_zip" name="edit_zip" style="width:327px;"
								value="<?= $vet->zip == NULL ? $this->lang->line('register_zip') : $vet->zip ?>" 
								defaultText="<?= $this->lang->line('register_zip')?>" 
								class="edit_input">
							<br>
						<br> 
							<div style="position: relative;left: 0px;top:0px;font-size: 12px;"><?= $this->lang->line('register_city')?></div>
							<input type="text" id="edit_city" name="edit_city" style="width:327px;"
								value="<?= $vet->city == NULL ? $this->lang->line('register_city') : $vet->city ?>" 
								defaultText="<?= $this->lang->line('register_city')?>" 
								class="edit_input">
							<br>
						<br>
					</div>
						<div style="position: relative;left: 0px;top:0px;font-size: 12px;"><?= $this->lang->line('register_address')?></div>
						<input type="text" id="edit_address" name="edit_address" style="width:327px;"
							value="<?= $vet->address == NULL ? $this->lang->line('register_address') : $vet->address ?>" 
							defaultText="<?= $this->lang->line('register_address')?>" 
							class="edit_input">
						<br>
						<input type="hidden" id="edit_geolocation" name="edit_geolocation" 
							value="<?= ($vet->geo_location == NULL || $vet->geo_location == "") ? "48.14257669384092, 16.34922897338868" : $vet->geo_location; ?>" class="edit_input">
						<div id="map_canvas_profile" style="width:332px;"></div>
						<br>
						<div style="position: relative;left: 0px;top:0px;font-size: 1.5em;"><?= $this->lang->line('login_email')?>:</div>
						<input type="text" id="edit_login_email" name="edit_login_email" style="width:327px;"
							value="<?= $vet->login_email == NULL ? $this->lang->line('login_email') : $vet->login_email ?>" 
							defaultText="<?= $this->lang->line('login_email')?>" 
							class="edit_input">
						<br>
						<br>
						<div style="position: relative;left: 0px;top:0px;font-size: 1.5em;"><?= $this->lang->line('login_login_password')?>:</div>
						<input type="password" id="edit_login_pw_new" name="edit_login_pw_new" style="width:327px;"
							value="" 
							defaultText="<?= $this->lang->line('new_password')?>" 
							placeholder="<?= $this->lang->line('new_password')?>" 
							class="edit_input">
						<br>
						<br>				
						<input type="password" id="edit_login_pw_confirm" name="edit_login_pw_confirm" style="width:327px;"
							value="" 
							defaultText="<?= $this->lang->line('register_passconf')?>" 
							placeholder="<?= $this->lang->line('register_passconf')?>" 
							class="edit_input">
						<br>

<br>
						<input type="password" id="edit_login_pw_old" name="edit_login_pw_old" style="width:327px;"
							value="" 
							defaultText="<?= $this->lang->line('edit_user_pw_old')?>" 
							placeholder="<?= $this->lang->line('edit_user_pw_old')?>" 
							class="edit_input">
						<br>

						<br/><br/><br/>
						
						
					</div>
					
					<br>
					
					<div style="left:0px;top:20px;" class="edit_arrow_text pseudo_form_submit" action="editprofile"><?= $this->lang->line('edit_user_confirm')?></div>
					
					<?php echo form_close();?>	
				</div>
				
					
			</div>
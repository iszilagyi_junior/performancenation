
<script>
	var all_clubs = <?= $all_clubs;?>;
	$(document).ready(function()
	{	
		initAutocomplete();
		
	});
</script>
<div id="content" style="width:800px;margin-bottom:100px;">
	<img class="" style="position:absolute;left:0px;top:0px;" src="<?= site_url('items/frontend/img/big_chrome.png')?>" border="0">
	<div class="playbold gold page_title" style="text-align:center;font-size:40px;letter-spacing:1px;z-index:10;position:relative;top:10px;margin-bottom:30px;">CLUB SEARCH</div>
	
	<div style="height:110px;text-align: left;margin-left:10px;">
		<input type="text" id="profile_search" name="profile_search" placeholder="Nickname..." defaultText="Nickname..." class="edit_input" style="margin-bottom:5px;">
	
		<br/>
		<div id="group_search_submit" class="button" style="float:left;margin-top:10px;">SEARCH</div>

	</div>
	<div class="error_msg" style="left:auto;right:0px;top: 190px;"></div>
	<div style="position: absolute;right:10px;top:75px;width:505px;overflow:hidden;">
		<div class="button" id="new_group" style="margin-top:0px;margin-right:0px;">CREATE NEW CLUB</div>
		<div id="group_new_box">
			<input type="text" name="group_name" id="group_name" class="edit_input" value="" placeholder="Club name" style="float:left;width:248px;"/>
			<select name="category_select" id="category_select" style="padding-left:10px;width:249px;margin-right:-11px;">
				<option value="0">Please select a category...</option>
				<? foreach($group_categories as $cat):?>
					<option value="<?= $cat['id'];?>"> <?= $cat['name'];?> </option>
				<? endforeach;?>
			</select>
			<input type="text" name="group_address" id="autocomplete" onFocus="geolocate()" class="edit_input" value="" placeholder="Club address" style="float:right;width:240px;margin-right: 0px;margin-top: 5px;"/>
			<br/>
			<textarea name="group_description" id="group_description" class="edit_input" placeholder="Club description" style="border:0px;resize:none;height:60px;float:left;"></textarea>
			<br/>
			<input type="checkbox" name="terms" id="group_create_terms" value="1">
			<label style="color:#ffffff;" for="terms">I accept the <a target="_blank" href="<?= site_url('group_create_terms')?>">Terms & Conditions</a></label>
			<div id="request_new_grp_btn" class="button" style="float:left;">SUBMIT</div>
		</div>
		
	</div>
	<div id="switch_club_list" class="button" style="position:Absolute;left:10px;top:220px;width:120px;display:none;">LIST VIEW</div>
		<div id="switch_club_map" class="button" style="position:Absolute;left:10px;top:220px;width:120px;">MAP VIEW</div>
	<div class="titit" style="text-align:center;margin:10px 0px;color:#ffffff;font-size:18px;margin-top:60px;">Results</div>
	<div id="club_map_holder" ></div>
	<div id="resultHolder" >
		<div id="result_list">
		<? $x = 1; foreach($groups as $group):?>
		<a  style="text-decoration:none;" href="<?= site_url('group/' . $group['id'])?>">	
			<div class="result_list_item" style="<? if($x%2 == 0){ echo "margin-right:0px";}?>">	
					<img class="result_item_image" src="<?= site_url('items/uploads/profilepictures/'.$group['logo'])?>" style="">
					<div class="div_white_over"></div>
					<div class="result_item_text" style=""><?= $group['name'];?><br/><br/><?= $group['category'];?><br/><br/>Members: <?= $group['members'];?></div>				
				
			</div>
		</a>
		<? $x++; endforeach;?>
		
		</div>		
	</div>
</div>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" class="no-js">          
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<script>
			if(!navigator.userAgent.match(/iPad/i))
		    {
		    	document.write('<meta name="viewport" content="width=640, minimum-scale=0.5, maximum-scale=2.5, user-scalable=no">');
			}
			else
			{
				document.write('<meta name="viewport" content="width=1200, minimum-scale=0.2, maximum-scale=4.5, user-scalable=no">');
			}
		</script>
	<!--	<meta name="apple-itunes-app" content="app-id=998164678">
		<meta property="al:ios:url" content="http://snoopstr.com/" />
		<meta property="al:ios:app_store_id" content="998164678" />
		<meta property="al:ios:app_name" content="Performance Nation" /> -->
		<meta property="og:title" content="Performance Nation"/>
		<meta property="og:image" content="<?= site_url('items/frontend/img/logo.png')?>"/>
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="<?= site_url();?>"/>
		<meta property="og:site_name" content="Performance Nation"/>
		<meta property="og:description" content="Performance Nation"/>

		<title>Performance Nation</title>
		
		<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/fonts.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/desktop.css?version=2');?>" media="all" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/jquery-ui.min.css');?>" media="all" />
		
		
	<!--	<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/frontend.css');?>" media="all" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/dog.css');?>" media="all" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/auth.css');?>" media="all" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/statics.css');?>" media="all" />
		-->
		
		
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/jquery.datepick.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/timepicker-addon.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/lightbox.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/selectize.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/jquery.qtip.min.css');?>" media="screen" />
		
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/upload.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/booking.css"); ?>">
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/imgareaselect-default.css');?>" media="all" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/iealert/style.css');?>" media="all" />
		<link rel="shortcut icon" type="image/png" href="<?= site_url('items/frontend/img/favicon.ico');?>"/>
			<script src="<?= site_url("items/frontend/js/modernizr.custom.18900.js"); ?>" type="text/javascript"></script>
			<script src="<?= site_url("items/frontend/js/moxie.js"); ?>" type="text/javascript"></script>
	
		<script>
			
			
			mOxie.Env.swf_url = "../items/frontend/js/Moxie.min.swf"; 
			mOxie.Env.xap_url = "../items/frontend/js/Moxie.min.xap";
		</script>		
			
	
		
		
		<script>
			var rootUrl = "<?= site_url();?>";
			var user_id = <?= $user_id ?>;
			var country = "<?= $country_ip ?>";
			var sitter_detail_thank_rate = "<?= $this->lang->line('sitter_detail_thank_rate')?>";
			var select_dog_friend = "<?= $this->lang->line('dog_selection_friend_text')?>";
			var select_dog_enemy = "<?= $this->lang->line('dog_selection_enemy_text')?>";
			var photo_edit_title = "<?= $this->lang->line('photo_edit_title')?>";
			var delete_confirm_text = "<?= $this->lang->line('photo_delete_confirm_text')?>";
			var suggest_zone_description_thanks = "<?= $this->lang->line('suggest_zone_description_thanks')?>";
			var search_searching = "<?= $this->lang->line('search_searching')?>";
			var search_nothing_found = "<?= $this->lang->line('sorry_nothin_found')?>";
			var search_nothing_found_sitter = "<?= $this->lang->line('sorry_nothin_found_sitter')?>";
			var please_type_location = "<?= $this->lang->line('please_type_location')?>";
			var please_wait = "<?= $this->lang->line('please_wait')?>";
			var dragdrop = "<?= $this->lang->line('drag_drop')?>";
			var email_sent_text = "<?= $this->lang->line('email_sent')?>";
			var show_profile_sitter = "<?= $this->lang->line('edit_user_dogsitter_checkbox')?>";
			var hide_profile_sitter = "<?= $this->lang->line('edit_user_dogsitter_checkbox_deactive')?>";
			var loading_text = "<?= $this->lang->line('loading')?>";
			var search_text = "<?= $this->lang->line('profile_search_submit')?>";
			var no_dogs_breed = "<?= $this->lang->line('no_dogs_for_breed')?>";
			var no_dogs_name = "<?= $this->lang->line('no_dogs_for_name')?>";
			var cover_image_text = "<?= $this->lang->line('cover_image')?>";
			var profile_image_text = "<?= $this->lang->line('profile_image')?>";
			var upload_failed_text = "<?= $this->lang->line('upload_failed_text')?>";
		
		</script>
		<script type="text/javascript" src="<?= site_url("items/general/js/jquery-1.11.3.min.js"); ?>" ></script>
		<script src="<?= site_url("items/frontend/js/jquery-ui.min.js"); ?>" type="text/javascript"></script>
		
		<script src="<?= site_url("items/frontend/js/jquery.datepick.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/timepicker-addon.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/lightbox.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/jstz.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/jquery.qtip.min.js"); ?>" type="text/javascript"></script>
		<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=geometry"></script>
		<script src="<?= site_url("items/frontend/js/jquery.eventCalendar.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/jquery.timeago.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/upload.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/jquery.imgareaselect.pack.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/iealert.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/placeholders.min.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/general/js/packery.pkgd.min.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/selectize.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/markerwithlabel.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/responsive.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/jstz.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/jquery.cookie.js"); ?>" type="text/javascript"></script>
		<script src="<?= site_url("items/frontend/js/desktop.js?ver=2"); ?>" type="text/javascript"></script>
		
		<!-- FB -->
		<script>
		  window.fbAsyncInit = function() {
		    FB.init({
		      appId      : '1680628872212292',
		      xfbml      : true,
		      version    : 'v2.5'
		    });
		  };
		
		  (function(d, s, id){
		     var js, fjs = d.getElementsByTagName(s)[0];
		     if (d.getElementById(id)) {return;}
		     js = d.createElement(s); js.id = id;
		     js.src = "//connect.facebook.net/en_US/sdk.js";
		     fjs.parentNode.insertBefore(js, fjs);
		   }(document, 'script', 'facebook-jssdk'));
		</script>
		 
		 <!-- GA -->
		 
	</head>
	<body>
		<div id="site_bg"></div>
		<div id="header_bg"></div>
		<div id="loadingBG"></div>
		<img id="loadingGif" src="<?= site_url('items/frontend/img/loading.gif')?>" />	 	
		
		<div id="container">
			
			<img id="header_parts" src="<?= site_url('items/frontend/img/header_parts.png')?>" />
			<img id="logo" style="position:absolute;left:404px;top:85px;" src="<?= site_url('items/frontend/img/logo.png')?>"/>
		
			
			<div id="wrapper">
			

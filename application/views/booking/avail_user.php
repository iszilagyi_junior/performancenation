
		
		<div class="avail_overlay calendar_overlay">
			<img class="overlay_arrow" src="<?= site_url('items/frontend/img/popup_arrow.png')?>"/>
			<div class="overlay_close">X</div>
			<div class="popup_title_text">Book Sitter<br/>
				<span style="color:#b4b4b4;font-size:13px;"><?= $weekday ?>, <?= date("F j, Y", strtotime($date));?></span>
			</div>
			<form id="avail_overlay_form">
				<input type="hidden" name="avail_overlay_date" value="<?= $date?>">

				
				<br>
				<div class="dognum_holder">
					<label class="light_popup_text" for="fe3">number of dogs</label>
					<input type="text" name="avail_overlay_dogcount" id="fe3" class="popup_input">
				</div>
				<div class="booking_message_holder">
					<label class="light_popup_text">Message</label>
					<br/>
					<textarea id="booking_message_textarea" name="avail_overlay_message" class="popup_input" style="width:197px;resize:none;height:70px;padding:0px;"></textarea>
				</div>
				<input type="submit" name="avail_overlay_save" value="save" class="popup_button">
			</form>
		</div>
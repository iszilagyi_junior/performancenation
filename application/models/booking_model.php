<?php

class booking_model extends CI_Model  
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		
		$this->load->library('MY_booking');
    }	
	
	public function getSitterTimeslotsRecurring($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('recurring', 1);
		$this->db->where('allday', 1);
		return $this->db->get('sitter_timeslots_view');
	}
	
	public function getSitterTimeslotsNonRecurring($user_id, $startdate, $enddate)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('recurring', 0);
		$this->db->where('date BETWEEN "' . $startdate . '" AND "' . $enddate . '"');
		$this->db->where('allday', 1);
		return $this->db->get('sitter_timeslots_view');
	}	
	
	public function getSitterTimeslotExceptions($user_id, $startdate, $enddate)
	{
		$this->db->where('sitter_id', $user_id);
		$this->db->where('date BETWEEN "' . $startdate . '" AND "' . $enddate . '"');
		return $this->db->get('sitter_timeslot_exceptions_view');
	}
	
	public function getSitterBookings($user_id, $startdate, $enddate, $status)
	{
		$this->db->where('sitter_id', $user_id);
		$this->db->where('date BETWEEN "' . $startdate . '" AND "' . $enddate . '"');
		$this->db->where('status_id', $status);
		$this->db->where('allday', 1);
		return $this->db->get('sitter_bookings_view');
	}

	public function getSitterAvailabilityByID($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('sitter_timeslots');
	}

	public function deleteSitterAvailabilityByID($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('sitter_timeslots');
	}

	public function insertSitterAvailability($data)
	{
		$this->db->insert('sitter_timeslots', $data);
	}
	
	public function insertBookingRequest($data)
	{
		$this->db->insert('sitter_bookings', $data);
	}
	
	
	public function updateSitterAvailability($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('sitter_timeslots', $data);
	}
	
	public function insertSitterException($data)
	{
		$this->db->insert('sitter_timeslot_exceptions', $data);
	}

	public function getSitterException($exception_id)
	{
		$this->db->where('id', $exception_id);
		return $this->db->get('sitter_timeslot_exceptions');
	}
	
	public function updateSitterException($exception_id, $data)
	{
		$this->db->where('id', $exception_id);
		$this->db->update('sitter_timeslot_exceptions', $data);
	}

	public function deleteSitterException($exception_id)
	{
		$this->db->where('id', $exception_id);
		$this->db->delete('sitter_timeslot_exceptions');
	}
	
	public function getAllSitterBookings($id)
	{
		$this->db->where('sitter_id', $id);
		$this->db->where('flag_show', 1);
		return $this->db->get('sitter_bookings_view');
	}
	
	
	public function acceptSitterRequest($id)
	{
		$this->db->where('id', $id);
		$this->db->update('sitter_bookings', array('status_id' => My_Booking::BOOKING_STATUS_ACCEPTED));
	}
	
	public function declineSitterRequest($id)
	{
		$this->db->where('id', $id);
		$this->db->update('sitter_bookings', array('status_id' => My_Booking::BOOKING_STATUS_DECLINED));
	}
	
	public function removeSitterRequest($id)
	{
		$this->db->where('id', $id);
		$this->db->update('sitter_bookings', array('flag_show' => 0));
	}
	
	public function getSitterSearch($date, $dogs, $weekday)
	{
		$sql = "SELECT stv.user_id
				FROM sitter_timeslots_view stv
				WHERE 
				  ((stv.date = ? AND stv.recurring = 0) OR (stv.weekday_id = ? AND stv.recurring = 1))
				  AND 
				    (stv.dog_limit - (select count(*) as booked_dogs from sitter_bookings_view sbv 
				    left join sitter_booking_dogs sbd on sbv.id = sbd.booking_id
				    where sbv.sitter_id = stv.user_id and sbv.date = ?)) >= ?;";
					
		return $this->db->query($sql, array($date, $weekday, $date, $dogs));
	}
	
	public function getSitterSearchSpecific($date, $dogs, $weekday, $sitters)
	{
		$sql = "SELECT stv.user_id
				FROM sitter_timeslots_view stv
				WHERE
				  stv.user_id IN (?)
				  AND
				  ((stv.date = ? AND stv.recurring = 0) OR (stv.weekday_id = ? AND stv.recurring = 1))
				  AND 
				    (stv.dog_limit - (select count(*) as booked_dogs from sitter_bookings_view sbv 
				    left join sitter_booking_dogs sbd on sbv.id = sbd.booking_id
				    where sbv.sitter_id = stv.user_id and sbv.date = ?)) >= ?;";
					
		return $this->db->query($sql, array($sitters, $date, $weekday, $date, $dogs));
	}	
	
	public function getSitter($sitter)
	{
		$this->db->where('id', $sitter);
		return $this->db->get('users');
	}
	
	public function getSitters($sitters)
	{
		$this->db->where_in('id', $sitters);
		return $this->db->get('users');
	}
}
?>









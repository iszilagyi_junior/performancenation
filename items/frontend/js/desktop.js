/* GOOGLE MAPS */
var geocoder = new google.maps.Geocoder();
var poi_map;
var poi_markers = [];
var new_zoom = 10000;
var vehicle_map;
var vehicle_markers = [];
var event_map;
var event_markers = [];
var club_map;
var club_markers = [];
 
var $container;
var is_loading = false;
var content_offset = 11;
$(document).ready(function()
{	
	addListeners();
	addCarListeners();
	addAuthListeners();
	addImageListeners();
	addSearchListeners();
	addGroupListeners();
	addGroupEditListeners();
	addGroupEventListeners();
	addModListeners();
	mapListeners();
	
	window_scroll_event_handler();
	messagingClicks();
	setTimeout(function(){
		initPkry();
	},200);
	checkDates();
	setTimezoneCookie();
});

/** DATE LOCALIZATION **/
function checkDates()
{
	var date = new Date();
	// dconsole.log(date.getTimezoneOffset());
}


function setTimezoneCookie(){
 
    var timezone_cookie = "timezoneoffset";
    var timezone = jstz.determine();
    // if the timezone cookie not exists create one.
    if (!$.cookie(timezone_cookie)) { 
 
        // check if the browser supports cookie
        var test_cookie = 'testcookie';
        $.cookie(test_cookie, true);
 
        // browser supports cookie
        if ($.cookie(test_cookie)) { 
         
            // delete the test cookie
            $.removeCookie(test_cookie);
         
            // create a new cookie 
           
            $.cookie(timezone_cookie, timezone.name());
 
            // re-load the page
            location.reload(); 
        }
    }
    // if the current timezone and the one stored in cookie are different
    // then store the new timezone in the cookie and refresh the page.
    else {         
 
        var storedOffset = $.cookie(timezone_cookie);
        var currentOffset = timezone.name();
 
        // user may have changed the timezone
        if (storedOffset !== currentOffset) { 
            $.cookie(timezone_cookie, timezone.name());
            location.reload();
        }
    }
}






/** GOOGLE AUTOFILL ADDRESS  **/

// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete, addressString;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete(fillForm, elem_id) {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  var elem = elem_id || 'autocomplete';
  var fillF = fillForm || false;
  
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById(elem)),
      {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress(fillF));
}

function fillInAddress(fill) {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();
  
  
  if(fill)
  {
	 // console.log(place);
  }
  
/*  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }
*/
  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
 /* for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      
      addressString += " "+val;
      
     // document.getElementById('autocomplete').value = addressString;
    }
  }*/
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}








function addListeners()
{
	
	$('#newsfeed_img_title_holder').on('keyup', function(){
		var string = $(this).val();
		
		$.post( rootUrl+"statics/saveTitleText",{string:string}, function( data ) {
	   
	   });
	});
	
	
	
	
	/** INVITE MAILS **/
	$("#social_invite").click(function() {	  	
	  	$('#invite_site_overlay').fadeIn();
		scrollTo('invite_site_overlay');
	});
	 
	 
	$("#invite_site_button").click(function() {
	  	$(this).hide();
	  	$('#invite_error_message').text('Sending mail...').show();
	   var email = $('#invite_friend_email').val();

	   $.post( rootUrl+"statics/send_invite_mail",{email:email}, function( data ) {
	   
			$('#invite_error_message').html(data);
			$("#invite_site_button").show();  
			$('#invite_friend_email').val('');
	   });
	   
	}); 
	 
	/** RATINGS **/
	$(".rate").hover(function() {
	    var rating = $(this).attr("rating");
	    for(var i=1; i<=5;i++){
		    $("#empty_circle_"+i).show();
		    $("#full_circle_"+i).hide();		    
	    }
	    $("#empty_circle_"+i).hide();
		$("#full_circle_"+i).show();
	    for(var i=1; i<=rating;i++){
		    $("#empty_circle_"+i).hide();
		    $("#full_circle_"+i).show();		    
	    }
	});
	
	$(".rate").click(function() {
	  
	   var rating = $(this).attr('rating');
	   var poi = $(this).attr('poi');
	   
	   
	   
	   $.post( rootUrl+"statics/rate",{rating:rating, poi_id:poi}, function( data ) {
			location.reload();
	   });
	});
	
	/** RATING END**/
	
	
	
	$('#user_event_image').click(function() {
	   	$('#edit_user_cover_picture').click();
	    
	});

	
	
	$('#send_comment').click(function() {
	   	 $this = $(this);

	        var comment = $.trim($('#send_comment_input').val());
	        var eid = $(this).attr('eid');
	        var pid = $(this).attr('pid');
	       
			console.log(pid);
		       if($.trim(comment) != '')
			   {
			        $.post(rootUrl+'statics/add_photo_comment',{pid:pid, comment:comment}, function(data){
			        	var json = $.parseJSON(data);
		
		        		var elem = '<div class="comment_holder_item">'+
										'<img class="comment_profile_img" src="'+rootUrl+'items/uploads/profilepictures/'+json.profile_image+'">'+
										'<div class="comment_name dosissemi">'+json.nickname+'</div>'+
										'<div class="news_comment_text dosisreg">'+json.comment+'</div>'+
									'</div>';
		        		
		        		$('#user_comments').prepend(elem);
						
						$this.val('');
						$('#send_comment_input').val('');
						$('.close_overlay').click();
			        });
			    } 
	        
	    
	});
	
	
	
	
	$('.menu_item').click(function(){		
		$('.menu_item').removeClass('active');	
		$('.submenu_holder, #notifications_holder').hide();	
		$('#get_notification').removeClass('open');
		$(this).addClass('active');
		
		if($(this).hasClass('sub'))
		{
			var grp = $(this).attr('grp');
			$('.submenu_holder[grp="'+grp+'"]').fadeIn();
		}
	});
	
	$(document).mouseup(function (e)
	{
	    var container = $('.submenu_holder, #notifications_holder');
	
	    if (!container.is(e.target) // if the target of the click isn't the container...
	        && container.has(e.target).length === 0) // ... nor a descendant of the container
	    {
	        $('.menu_item').removeClass('active');
	        container.hide();
	    }
	});
	
	
	$('#report_btn').click(function(){		
		var eid = $(this).attr('eid');
		var type = $(this).attr('rtype');
		
		
		
		$.post( rootUrl+"statics/report_entity",{eid:eid, type:type}, function( data ) {
				window.alert('Report sent!');
		}); 
	});
	
	
	$('#mark_read_notifications').click(function(){		
		
		
		
		
		$.post( rootUrl+"statics/mark_all_notifications_read", function( data ) {
				location.reload();
		}); 
	});
}


function addGroupEventListeners()
{
	
	$('#event_search_submit').click(function(){
		$this = $(this);
		
		$this.text("Searching...");
		
		var search = $('#profile_search').val();
		var name = $('#name_search').val();		
		var location = $('#location_search').val();
		
		var start = $('#start_search').val();
		var end = $('#end_search').val();
		var distance = $('#distance_search').val();
		var show_past = 0;
		
		if($("#show_past_events").is(':checked'))
		{
			show_past = 1;
		}
		
		if(location != "")
		{
			
			geocoder.geocode({'address': location}, function(results, status) {
			      if (status == google.maps.GeocoderStatus.OK) {

					var geo_location = results[0].geometry.location.lat()+", "+ results[0].geometry.location.lng();
					
					$.post( rootUrl+"statics/event_search_result",{search:search, name:name, geo_location:geo_location, start:start, end:end, distance:distance, show_past:show_past}, function( data ) {
						var json = $.parseJSON(data);
						
						$('#resultHolder').empty().append(json.html);
						$this.text("SEARCH");
						initMapAllEvents(json.events, geo_location);
						if(data == 'NO')
						{
							$('#resultHolder').empty().append("No Events found!").show();
						}
						else
						{
							$('#resultHolder').hide();			
							$('#event_map_holder').show();
							$('#event_search_list').show();
						}
				   });
			         
			      } else {		      	
			       // handleNoGeolocation(false);
			       	   var geo_location = "";

				       $.post( rootUrl+"statics/event_search_result",{search:search, name:name, geo_location:geo_location, start:start, end:end, distance:distance, show_past:show_past}, function( data ) {
							var json = $.parseJSON(data);
						
							$('#resultHolder').empty().append(json.html);
							$this.text("SEARCH");
							$('#event_search_list').show();
							
							if(data == 'NO')
							{
								$('#resultHolder').empty().append("No Events found!");
							}
					   });
			      }
			});
		}
		else
		{
			var geo_location = "";
			if (navigator.geolocation) 
			{
       			navigator.geolocation.getCurrentPosition(function(position){
       				geo_location = position.coords.latitude+", "+position.coords.longitude;
       				$.post( rootUrl+"statics/event_search_result",{search:search, name:name, geo_location:geo_location, start:start, end:end, distance:distance, show_past:show_past}, function( data ) {
						var json = $.parseJSON(data);
						
						$('#resultHolder').empty().append(json.html);
						$this.text("SEARCH");
						initMapAllEvents(json.events, geo_location);
						if(data == 'NO')
						{
							$('#resultHolder').empty().append("No Events found!").show();
						}
						else
						{
							$('#resultHolder').hide();
							$('#event_map_holder').show();
							$('#event_search_list').show();
						}
				
				   });
       			});
       		}
			else
			{
				$.post( rootUrl+"statics/event_search_result",{search:search, name:name, geo_location:geo_location, start:start, end:end, distance:distance, show_past:show_past}, function( data ) {
				
					var json = $.parseJSON(data);
				
					$('#resultHolder').empty().append(json.html);
					$this.text("SEARCH");
					$('#event_search_list').show();
					if(data == 'NO')
					{
						$('#resultHolder').empty().append("No Events found!");
					}
		
		   });
			}
			
			
			
		}
	});
	
	
	$('.clone_event').click(function(){
		
		var event_id = $(this).attr('eid');
		
		$(this).unbind('click');
		$.post( rootUrl+"statics/clone_user_event",{event_id:event_id}, function( data ) {
				
				window.location.href = rootUrl+'user_event/'+data;
				
		   });
		
		
		
	});
	
	
	$('.edit_event').click(function(){
		$('#group_event_overlay').fadeIn();
		scrollTo('group_event_overlay');
	});
	
	
	$('#event_search_list').click(function(){
		$(this).hide();
		$('#event_map_holder').hide();
		$('#event_locate_me').hide();
		$('#resultHolder').show();
		$('#event_search_map').show();	
	});
	
	$('#event_search_map').click(function(){
		$(this).hide();
		$('#resultHolder').hide();
		$('#event_locate_me').show();
		$('#event_map_holder').show();		
		$('#event_search_list').show();	
	});
	
	$('#edit_group_event_button').click(function(){
		var event_id = $(this).attr('eid');
		var title = $('#group_event_title').val();
		var description = $('#group_event_description').val();
		var address = $('#group_event_address').val();
		var date = $('#group_event_date').val();
		
		var public = $('input[name="group_public"]:checked').val();
		
		
		if(title != "" && description != "" && date != "" && address != "")
		{
			
			geocoder.geocode({'address': address}, function(results, status) {
			      if (status == google.maps.GeocoderStatus.OK) {

					var geo_location = results[0].geometry.location.lat()+", "+ results[0].geometry.location.lng();
	 
			      } else {		      	
			       // handleNoGeolocation(false);
			       	   var geo_location = '0, 0';

				       
			      }
 
			      $.post(rootUrl+'statics/update_group_event', {title: title,  description: description, date: date, eid:event_id, public:public, address:address, geo_location:geo_location}, function(data){
						location.reload();
					});
			});
			
			
			
		}
		else
		{
			$('#group_event_error_message').empty().text('Please fill out all the fields!');
		}
		
		
		
		
	});
	
	
	$('#edit_event_button').click(function(){
		var event_id = $(this).attr('eid');
		var title = $('#group_event_title').val();
		var description = $('#group_event_description').val();
		var date = $('#group_event_date').val();
		var address = $('#autocomplete').val();
		
		
		if(title != "" && description != "" && date != "" && address != "")
		{
			
			geocoder.geocode({'address': address}, function(results, status) {
			      if (status == google.maps.GeocoderStatus.OK) {

					var geo_location = results[0].geometry.location.lat()+", "+ results[0].geometry.location.lng();
	 
			      } else {		      	
			       // handleNoGeolocation(false);
			       	   var geo_location = '0, 0';

				       
			      }
 
			     $.post(rootUrl+'statics/update_user_event', {title: title,  description: description, date: date, eid:event_id, address:address, geo_location:geo_location}, function(data){
					location.reload();
				});
			});
			
			
			
		}
		else
		{
			$('#group_event_error_message').empty().text('Please fill out all the fields!');
		}
		
		
		
		
	});
	
	$('.event_decision').click(function(){
		var event_id = $(this).attr('cid');
		var vehicle_id = $(this).attr('vid');
		var status = $(this).attr('status');
		
		$.post(rootUrl+'statics/group_event_decision', {event_id: event_id, vehicle_id: vehicle_id, status: status}, function(data){
			location.reload();
		});
	});
	
	$('.user_event_decision').click(function(){
		var event_id = $(this).attr('cid');
		var vehicle_id = $(this).attr('vid');
		var status = $(this).attr('status');
		
		$.post(rootUrl+'statics/user_event_decision', {event_id: event_id, vehicle_id: vehicle_id, status: status}, function(data){
			location.reload();
		});
	});
	
}



function addModListeners()
{
	$('.mod_type_select').on('change', function(){
		var vehicle_type = $(this).attr('tid');
		var mod_type = $(this).val();
		var engine_type = $(this).attr('engine');
		
		$('#mod_default_input').val('').show();
		$('.mod_subselect').hide();
		
		
		if(mod_type != 0)
		{
			if(vehicle_type == 0) // CAR
			{
				if(engine_type == "gas")
				{
					switch(mod_type)
					{
						case 'brake': {$('#mod_brake_types').show();};break;
						case 'body': {$('#mod_body_types').show();};break;	
						case 'chassis': {$('#mod_chassis_types').show();};break;
						case 'drive_axle': {$('#mod_drive_axle_types').show();};break;
						case 'engine': {$('#mod_engine_types').show();};break;
						case 'exhaust': {$('#mod_exhaust_types').show();};break;
						case 'suspension': {$('#mod_suspension_types').show();};break;
						case 'transmission': {$('#mod_transmission_types').show();};break;
						default: { $('#mod_default_types').show(); };
					}
				}
				else
				{
					switch(mod_type)
					{
						case 'brake': {$('#mod_brake_types').show();};break;
						case 'body': {$('#mod_body_types').show();};break;	
						case 'chassis': {$('#mod_chassis_types').show();};break;
						case 'drive_axle': {$('#mod_drive_axle_types').show();};break;
						case 'engine': {$('#mod_engine_types').show();};break;
						case 'exhaust': {$('#mod_exhaust_types').show();};break;
						case 'suspension': {$('#mod_suspension_types').show();};break;
						case 'transmission': {$('#mod_transmission_types').show();};break;
						default: { $('#mod_default_types').show(); };
					}
				}
				
			}
			else if(vehicle_type == 1) // MOTORCYCLE
			{
				if(mod_type == "engine")
				{
					$('#motor_mod_engine_types').show();
				}
				else
				{
					$('#mod_default_types').show();
				}
			}
			else // TRUCK
			{
				if(engine_type == "gas")
				{
					switch(mod_type)
					{
						case 'brake': {$('#mod_brake_types').show();};break;
						case 'body': {$('#mod_body_types').show();};break;	
						case 'chassis': {$('#mod_chassis_types').show();};break;
						case 'drive_axle': {$('#mod_drive_axle_types').show();};break;
						case 'engine': {$('#mod_engine_types').show();};break;
						case 'exhaust': {$('#mod_exhaust_types').show();};break;
						case 'suspension': {$('#mod_suspension_types').show();};break;
						case 'transmission': {$('#mod_transmission_types').show();};break;
						default: { $('#mod_default_types').show(); };
					}
				}
				else
				{
					switch(mod_type)
					{
						case 'brake': {$('#mod_brake_types').show();};break;
						case 'body': {$('#mod_body_types').show();};break;	
						case 'chassis': {$('#mod_chassis_types').show();};break;
						case 'drive_axle': {$('#mod_drive_axle_types').show();};break;
						case 'engine': {$('#mod_engine_types').show();};break;
						case 'exhaust': {$('#mod_exhaust_types').show();};break;
						case 'suspension': {$('#mod_suspension_types').show();};break;
						case 'transmission': {$('#mod_transmission_types').show();};break;
						default: { $('#mod_default_types').show(); };
					}
				}
				
			}
			
			
			$('.mod_subselect').on('change', function(){
				var def_type = $(this).val();
				//$('#mod_default_input').hide();
				
				if(def_type != 1)
				{
					$('#mod_default_input').show();
				}
			});
			
			
			
			
		}
	});
	
	$('#mod_exhaust_types').on('change', function(){
		var selected_val = $(this).val();
		//$('#mod_default_input').hide();
		if(selected_val == 2 || selected_val == 4)
		{
			$('#mod_default_input').show();
		}	
	});
	
	
	$('#engine_type_select').on('change', function(){
		var vehicle_type = $(this).attr('tid');
		
		var selected_val = $(this).val();
		$('.mod_type_select').hide();
		$('.mod_type_select[engine="'+selected_val+'"]').show();
		$('#add_mod_piece').attr('engine', selected_val);		
	});
	
	
	
	$('#add_mod_piece').click(function(){
		var vehicle_id = $(this).attr('cid');
		var vehicle_type = $('.mod_type_select').attr('tid');
		var engine_type = $(this).attr('engine');
		
		$(this).unbind('click');
		if(vehicle_type != 1)
		{
			var column_type = $('.mod_type_select[engine="'+engine_type+'"]').val();
		}
		else
		{
			var column_type = $('.mod_type_select[engine="motor"]').val();
		}
		
		var mod_text = $('#mod_default_input').val();
		var mod_type = $('.mod_subselect:visible').val();
		
		
		
	/*	if($('#mod_default_types').is(":visible"))
		{
			var mod_type = $('#mod_default_types').val();
		}
		else if($('#motor_mod_engine_types').is(":visible"))
		{
			var mod_type = $('#motor_mod_engine_types').val();
		}
		
		*/
		
		
		
		if(column_type != 0)
		{
			$.post(rootUrl+'add_mod', {vehicle_type: vehicle_type, column_type: column_type, engine_type: engine_type, vehicle_id: vehicle_id, mod_text:mod_text, mod_type:mod_type}, function(data){
				$('#mod_error').empty().text('Modification added!');
				location.reload();
			});
		}
		else
		{
			$('#mod_error').empty().text('Please select a modification!');
		}
		
		
		
	});
}

function initMapPOI(pois) {

    var mapCanvas = document.getElementById('poi_map_holder');
    if($("#poi_map_holder").length){

	    var mapOptions = {
	      center: new google.maps.LatLng(48.2165319,16.3608047),
	      zoom: 14,
	      streetViewControl: false,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    }
	    
	    poi_map = new google.maps.Map(mapCanvas, mapOptions);
	  /*  var geo_loc = $("#edit_geolocation").val();
		
	*/
		$.each(pois, function(i,item)
		{
			
			var arr = item.geo_location.split(',');
			var lat = parseFloat(arr[0]);
			var lng = parseFloat(arr[1]);
			var myLatlng = new google.maps.LatLng(lat, lng);
			
			
			
			//var marker = new google.maps.MarkerWithLabel({position: myLatlng, map: map, labelText: });
			var marker = new MarkerWithLabel({
		       position: myLatlng,
		       draggable: false,
		       map: poi_map,
		       id: item.id,
		       url: rootUrl+"poi/"+item.id,
		       category: item.category_ids,
		       rating: item.rating_num,
		       labelContent: item.name,
		       labelClass: "labels", // the CSS class for the label
		       labelAnchor: new google.maps.Point(30, 0),
		       labelVisible: true
		     });
		     
		     poi_markers.push(marker);
			google.maps.event.addListener(marker, 'click', function() {window.location.href = marker.url;});
		});
		
		
		
		
	}  
	
	
	google.maps.event.addListener(poi_map, 'zoom_changed', function() {
   		var zoomLevel = poi_map.getZoom();	   	
	   	var bounds = poi_map.getBounds();
		var center = bounds.getCenter();
		var ne = bounds.getNorthEast();
		
		// r = radius of the earth in statute miles
		var r = 3963.0;  
		
		// Convert lat or lng from decimal degrees into radians (divide by 57.2958)
		var lat1 = center.lat() / 57.2958; 
		var lon1 = center.lng() / 57.2958;
		var lat2 = ne.lat() / 57.2958;
		var lon2 = ne.lng() / 57.2958;
		
		// distance = circle radius from center to Northeast corner of bounds
		var dis = r * Math.acos(Math.sin(lat1) * Math.sin(lat2) + 
		  Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));
		  
		  new_zoom = dis * 1609.344;
		 
		 
		 
		 $('#poi_start_text_search').click();
	});	  
}


function clearMarkers(markers, map)
{
	 for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
    
}


function initMapAllEvents(all_events, geo_loc) {
	
	clearMarkers(event_markers, event_map);
	event_markers = [];
	
	
    var mapCanvas = document.getElementById('event_map_holder');
    if($("#event_map_holder").length){
		
		var arr = geo_loc.split(',');
				var lat = parseFloat(arr[0]);
				var lng = parseFloat(arr[1]);
				var center = new google.maps.LatLng(lat, lng);
		
		
	    var mapOptions = {
	      center: center,
	      zoom: 14,
	      streetViewControl: false,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    }
	    
	    event_map = new google.maps.Map(mapCanvas, mapOptions);
	  /*  var geo_loc = $("#edit_geolocation").val();
		
	*/
		$.each(all_events, function(i,item)
		{
			
			if(item.geo_location !== null)
			{
				var arr = item.geo_location.split(',');
				var lat = parseFloat(arr[0]);
				var lng = parseFloat(arr[1]);
				var myLatlng = new google.maps.LatLng(lat, lng);
				
				var image = rootUrl+"items/uploads/"+item.logo;
				console.log(image.indexOf('cover'));
				if(image.indexOf('cover') > -1)
				{
					var size = new google.maps.Size(68, 30);
				}
				else
				{
					var size = new google.maps.Size(68, 68);  
				}
				
				
				//var marker = new google.maps.MarkerWithLabel({position: myLatlng, map: map, labelText: });
				var marker = new MarkerWithLabel({
			       position: myLatlng,
			       draggable: false,
			       map: event_map,
			       icon: {
	                    url: image,
	                    scaledSize: size,
	                    origin: new google.maps.Point(-4,0), // origin
	                },
			       type: item.type,
			       visible: item.visible,
			       url: rootUrl+"user_event/"+item.id,
			       labelContent: item.title,
			       labelClass: "labels", // the CSS class for the label
			       labelAnchor: new google.maps.Point(30, 0),
			       labelVisible: true
			     });
			     
			     event_markers.push(marker);
			     
			     google.maps.event.addListener(marker, 'click', function() {
			        window.location.href = this.url;
			    });
			}
		});
		
		
		$.each(event_markers, function(i,item){
			if(!item.visible)
			{
				item.setVisible(false);
			}
		});
	}  	  
}

function initMapAllClubs(all_clubs) {
	
	clearMarkers(club_markers, club_map);
	club_markers = [];

    var mapCanvas = document.getElementById('club_map_holder');
    if($("#club_map_holder").length)
    {
		

		if(!navigator.geolocation) 
		{
	   		navigator.geolocation.getCurrentPosition(function(position){
	   				
	   			if(position)
	   			{
	       			var new_center = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
	   			}
	   			else
	   			{
	       			var new_center = new google.maps.LatLng(48.2165319,16.3608047);
	   			}	
			
			
			    var mapOptions = {
			      center: new_center,
			      zoom: 14,
			      streetViewControl: false,
			      mapTypeId: google.maps.MapTypeId.ROADMAP
			    }
			    
			    club_map = new google.maps.Map(mapCanvas, mapOptions);
		
				$.each(all_clubs, function(i,item)
				{
					
					if(item.geo_location !== null)
					{
						var arr = item.geo_location.split(',');
						var lat = parseFloat(arr[0]);
						var lng = parseFloat(arr[1]);
						var myLatlng = new google.maps.LatLng(lat, lng);
						
						var image = rootUrl+"items/uploads/profilepictures/"+item.logo;
						
						var marker = new MarkerWithLabel({
					       position: myLatlng,
					       draggable: false,
					       map: club_map,
					       icon: {
			                    url: image,
			                    scaledSize: new google.maps.Size(68, 68),
			                    origin: new google.maps.Point(-4,0), // origin
			                },
					       type: item.type,
					       url: rootUrl+"group/"+item.id,
					       labelContent: item.name,
					       labelClass: "labels", // the CSS class for the label
					       labelAnchor: new google.maps.Point(30, 0),
					       labelVisible: true
					     });
					     
					     club_markers.push(marker);
					     
					     google.maps.event.addListener(marker, 'click', function() {
					        window.location.href = this.url;
					    });
					}
				});
			});
		}  	 
		else
		{
			
			var new_center = new google.maps.LatLng(48.2165319,16.3608047);
			var mapOptions = {
			      center: new_center,
			      zoom: 14,
			      streetViewControl: false,
			      mapTypeId: google.maps.MapTypeId.ROADMAP
			    }
			    
			    club_map = new google.maps.Map(mapCanvas, mapOptions);
		
				$.each(all_clubs, function(i,item)
				{
					
					if(item.geo_location !== null)
					{
						var arr = item.geo_location.split(',');
						var lat = parseFloat(arr[0]);
						var lng = parseFloat(arr[1]);
						var myLatlng = new google.maps.LatLng(lat, lng);
						
						var image = rootUrl+"items/uploads/profilepictures/"+item.logo;
						
						var marker = new MarkerWithLabel({
					       position: myLatlng,
					       draggable: false,
					       map: club_map,
					       icon: {
			                    url: image,
			                    scaledSize: new google.maps.Size(68, 68),
			                    origin: new google.maps.Point(-4,0), // origin
			                },
					       type: item.type,
					       url: rootUrl+"group/"+item.id,
					       labelContent: item.name,
					       labelClass: "labels", // the CSS class for the label
					       labelAnchor: new google.maps.Point(30, 0),
					       labelVisible: true
					     });
					     
					     club_markers.push(marker);
					     
					     google.maps.event.addListener(marker, 'click', function() {
					        window.location.href = this.url;
					    });
					}
				}); 
		}
	}
}




function initMapVehicles(vehicles) {

    var mapCanvas = document.getElementById('vehicle_map_holder');
    if($("#vehicle_map_holder").length){
		
		
		if (navigator.geolocation) 
		{
       		navigator.geolocation.getCurrentPosition(function(position){
       				
       			if(position)
       			{
	       			var new_center = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
       			}
       			else
       			{
	       			var new_center = new google.maps.LatLng(48.2165319,16.3608047);
       			}	
       			
       			var mapOptions = {
			      center: new_center,
			      zoom: 14,
			      streetViewControl: false,
			      mapTypeId: google.maps.MapTypeId.ROADMAP
			    }
			    
			    vehicle_map = new google.maps.Map(mapCanvas, mapOptions);
			
				$.each(vehicles, function(i,item)
				{
					
					if(item.geo_location !== null)
					{
						
						var arr = item.geo_location.split(',');
						var lat = parseFloat(arr[0]);
						var lng = parseFloat(arr[1]);
						var myLatlng = new google.maps.LatLng(lat, lng);
						
						var image = rootUrl+"items/uploads/profilepictures/"+item.fname;
						
						//var marker = new google.maps.MarkerWithLabel({position: myLatlng, map: map, labelText: });
						var marker = new MarkerWithLabel({
					       position: myLatlng,
					       draggable: false,
					       map: vehicle_map,
					       icon: {
			                    url: image,
			                    scaledSize: new google.maps.Size(68, 68),
			                    origin: new google.maps.Point(-4,0), // origin
			                },
					       type: item.type,
					       url: rootUrl+"vehicle_profile/"+item.pretty_url,
					       labelContent: item.nickname,
					       labelClass: "labels", // the CSS class for the label
					       labelAnchor: new google.maps.Point(30, 0),
					       labelVisible: true
					     });
					   
					     vehicle_markers.push(marker);
					     
					     google.maps.event.addListener(marker, 'click', function() {
					        window.location.href = this.url;
					    });
					}
				});
       			
       			
       			
       			$("#vehicle_nearby_search").hide().text("VEHICLES NEARBY");
	   			$("#vehicle_list_search").show();
       		});
		}
		else
		{
			var new_center = new google.maps.LatLng(48.2165319,16.3608047);
			var mapOptions = {
		      center: new_center,
		      zoom: 14,
		      streetViewControl: false,
		      mapTypeId: google.maps.MapTypeId.ROADMAP
		    }
		    
		    vehicle_map = new google.maps.Map(mapCanvas, mapOptions);
		
			$.each(vehicles, function(i,item)
			{
				
				if(item.geo_location !== null)
				{
					
					var arr = item.geo_location.split(',');
					var lat = parseFloat(arr[0]);
					var lng = parseFloat(arr[1]);
					var myLatlng = new google.maps.LatLng(lat, lng);
					
					var image = rootUrl+"items/uploads/profilepictures/"+item.fname;
					
					//var marker = new google.maps.MarkerWithLabel({position: myLatlng, map: map, labelText: });
					var marker = new MarkerWithLabel({
				       position: myLatlng,
				       draggable: false,
				       map: vehicle_map,
				       icon: {
		                    url: image,
		                    scaledSize: new google.maps.Size(68, 68),
		                    origin: new google.maps.Point(-4,0), // origin
		                },
				       type: item.type,
				       url: rootUrl+"vehicle_profile/"+item.pretty_url,
				       labelContent: item.nickname,
				       labelClass: "labels", // the CSS class for the label
				       labelAnchor: new google.maps.Point(30, 0),
				       labelVisible: true
				     });
				   
				     vehicle_markers.push(marker);
				     
				     google.maps.event.addListener(marker, 'click', function() {
				        window.location.href = this.url;
				    });
				}
			});
			
			$("#vehicle_nearby_search").hide().text("VEHICLES NEARBY");
			$("#vehicle_list_search").show();
		}
		
		
		
		
	    
	
	}  	  
}


function initMapEvent(geo_loc) {

    var mapCanvas = document.getElementById('event_map');
    if($("#event_map").length){

		var arr = geo_loc.split(',');
		var lat = parseFloat(arr[0]);
		var lng = parseFloat(arr[1]);
		var center = new google.maps.LatLng(lat, lng);



	    var mapOptions = {
	      center: center,
	      zoom: 14,
	      streetViewControl: false,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    }
	    
	    var poi_map = new google.maps.Map(mapCanvas, mapOptions);
	    
		var marker = new google.maps.Marker({
		    position: center,
		    map: poi_map
		  });

		
				
	}
	
	  	  
}



function initMapPOIDetail(geo_loc) {

    var mapCanvas = document.getElementById('poi_map');
    if($("#poi_map").length){

		var arr = geo_loc.split(',');
		var lat = parseFloat(arr[0]);
		var lng = parseFloat(arr[1]);
		var center = new google.maps.LatLng(lat, lng);



	    var mapOptions = {
	      center: center,
	      zoom: 14,
	      streetViewControl: false,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    }
	    
	    var poi_map = new google.maps.Map(mapCanvas, mapOptions);
	    
		var marker = new google.maps.Marker({
		    position: center,
		    map: poi_map
		  });

		
				
	}
	
	  	  
}


function mapListeners()
{
	$('#poi_locate_me').click(function(){
	 $('#poi_locate_me').text('Locating...');
		if(navigator.geolocation) {
		    navigator.geolocation.getCurrentPosition(function(position) {
		      var lat = position.coords.latitude
		      var lon = position.coords.longitude;
		      var from = new google.maps.LatLng(lat, lon);
		     
		      poi_map.setCenter(from);
		      $('#poi_locate_me').text('CENTER MAP ON ME');
		    });
	    }
	});
	
	$('#event_locate_me').click(function(){
	 $('#event_locate_me').text('Locating...');
		if(navigator.geolocation) {
		    navigator.geolocation.getCurrentPosition(function(position) {
		      var lat = position.coords.latitude
		      var lon = position.coords.longitude;
		      var from = new google.maps.LatLng(lat, lon);
		     
		      event_map.setCenter(from);
		      $('#event_locate_me').text('CENTER MAP ON ME');
		    });
	    }
	});
	
	
	
	$('#switch_poi_list').click(function(){
		
		$(this).fadeOut();
		$('#poi_locate_me').hide();
		
		$('#poi_map_holder').fadeOut(function(){
			$('#resultHolder').show();
			$('#switch_poi_map').show();
		});

	});
	
	
	$('#switch_poi_map').click(function(){
		
		$(this).fadeOut();
		
		$('#resultHolder').fadeOut(function(){
			$('#poi_map_holder').show();
			$('#switch_poi_list').show();
			$('#poi_locate_me').show();
		});

	});
	
	
	$('#switch_club_list').click(function(){
		
		$(this).fadeOut();
	//	$('#poi_locate_me').hide();
		
		$('#club_map_holder').fadeOut(function(){
			$('#resultHolder').show();
			$('#switch_club_map').show();
		});

	});
	
	
	$('#switch_club_map').click(function(){
		
		$(this).fadeOut();
		
		
		if(document.getElementById('club_map_holder').firstChild === null)
		{
			
			initMapAllClubs(all_clubs);	
		}
		
		$('#resultHolder').fadeOut(function(){
			$('#club_map_holder').show();
			$('#switch_club_list').show();
			//$('#poi_locate_me').show();
		});

	});
	
	
	
	$('#vehicle_nearby_search').click(function(){
		$('#resultHolder').fadeOut();
		$('#vehicle_map_holder').fadeIn();
		initMapVehicles(vehicles);
		
		$(this).text("Loading...");
		
		
	});
	
	$('#vehicle_list_search').click(function(){
		$('#vehicle_map_holder').fadeOut();
		$('#resultHolder').fadeIn();
		
		$(this).hide();
		$("#vehicle_nearby_search").show();
		
	});
	
	
	
	
	$('#poi_categories').on('change', function(){
		
		var selected_category = $(this).val();
		
		if(selected_category != 0)
		{
			$.each(poi_markers, function(i,item){
			
				if ( $.inArray(selected_category, item.category) !== -1 || item.length === 0) {
		            item.setVisible(true);
		            
		            $('.poi_item[pid="'+item.id+'"]').show();
		        }
		        // Categories don't match 
		        else {
		            item.setVisible(false);
		            $('.poi_item[pid="'+item.id+'"]').hide();
		        }
				
				
			});
		}
		else
		{
			$.each(poi_markers, function(i,item){
		            
		    	item.setVisible(true);
		    	$('.poi_item[pid="'+item.id+'"]').show();

			});
		}
		
		
	});
	
	$('#poi_ratings').on('change', function(){
		
		var selected_rating = $(this).val();
		
		if(selected_rating != 0)
		{
			$.each(poi_markers, function(i,item){
					
				if (parseInt(item.rating) >= parseInt(selected_rating) || item.length === 0) {
		            item.setVisible(true);
		            
		            $('.poi_item[pid="'+item.id+'"]').show();
		        }
		        // Categories don't match 
		        else {
		            item.setVisible(false);
		            $('.poi_item[pid="'+item.id+'"]').hide();
		        }
				
				
			});
		}
		else
		{
			$.each(poi_markers, function(i,item){
		            
		    	item.setVisible(true);
		    	$('.poi_item[pid="'+item.id+'"]').show();

			});
		}
		
		
	});
	
	
	
	$('#poi_start_text_search').click(function(){
		
		var text_search = $('#poi_text_search').val();
		var address_search = $('#poi_address_search').val();
		var category_search = $('#poi_categories').val();
		var rating_search = $('#poi_ratings').val();
		
		$(this).text('SEARCHING...');
		
		var nearby_limit = new_zoom; // in meters
		console.log(nearby_limit);
		
		$.each(poi_markers, function(i,item){
				
				if (parseInt(item.rating) >= parseInt(rating_search)) {
			
					if (item.labelContent.toLowerCase().indexOf(text_search.toLowerCase()) > -1) {
						
						
								if(address_search != "")
								{
								 
								   var distance = 0;
								   
								   geocoder.geocode({'address': address_search}, function(results, status) {
									      if (status == google.maps.GeocoderStatus.OK) {
									      
											var pos =  new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
											poi_map.setCenter(pos);
											var distance =  google.maps.geometry.spherical.computeDistanceBetween(item.position, pos);
											
											
											if(parseFloat(distance) <= parseFloat(nearby_limit))
											{
												
												if (category_search != 0) {		
																			
													if (item.category == category_search) {
														 
														item.setVisible(true);
														$('.poi_item[pid="'+item.id+'"]').show();
													}
													else
													{
														item.setVisible(false); 
												 		$('.poi_item[pid="'+item.id+'"]').hide(); 
													}
												}
												else
												{
													item.setVisible(true);
													$('.poi_item[pid="'+item.id+'"]').show();
												}
											}
											else
											{
												 item.setVisible(false);
												 $('.poi_item[pid="'+item.id+'"]').hide();
											}
				
				
				
									      } else {		      	
									       	  item.setVisible(true); 
									       	  $('.poi_item[pid="'+item.id+'"]').show();  
									      }
									});
				
									
								}
								else
								{
									if (category_search != 0) {		
																
										if (item.category == category_search) {
											 
											item.setVisible(true);
											$('.poi_item[pid="'+item.id+'"]').show();
										}
										else
										{
											item.setVisible(false); 
									 		$('.poi_item[pid="'+item.id+'"]').hide(); 
										}
									}
									else
									{
										item.setVisible(true);
										$('.poi_item[pid="'+item.id+'"]').show();
									}
								}
						
						
						
   
			        }
			        else
					{
						 item.setVisible(false);
						 $('.poi_item[pid="'+item.id+'"]').hide();
					}
				}
				else
				{
					item.setVisible(false);
					$('.poi_item[pid="'+item.id+'"]').hide();
				}        

		});
		
		
		$(this).text('SEARCH');
		
	});
}



function addGroupEditListeners()
{
	$('.member_delete').click(function(){
		$(this).hide()
		var $this = $(this).parent();
		
	//	$this.find('.member_item_img').hide();
	//	$this.find('.member_img_fade').hide();
		$this.find('.member_edit_item_title').hide();
		
		
		$this.find('.member_remove_holder').fadeIn();
	});
	
	$('.member_remove_no').click(function(e){
		e.stopPropagation();
		var $this = $(this);
		
		$this.parent().hide();
		
		$this.parent().parent().find('.member_item_img').fadeIn();
		$this.parent().parent().find('.member_img_fade').fadeIn();
		$this.parent().parent().find('.member_edit_item_title').fadeIn();
		$this.parent().parent().find('.member_delete').fadeIn();
		
	});
	
	
	
	$('.member_remove_yes').click(function(e){
		e.stopPropagation();
		var $this = $(this);
		var group_id = $(this).attr('gid');
		var vehicle_id = $(this).attr('mid');
		
		$.post(rootUrl+'statics/leave_group', {group_id: group_id, vehicle_id: vehicle_id}, function(data){
			
			var json = $.parseJSON(data);
			
			if(json.status)
			{
				$this.parent().parent().fadeOut();
			}
			else
			{
				$('#login_error_message').empty().text(json.message);
			}
		});	
		
	});
	
	
	
	$('.member_admin').click(function(e){
		e.stopPropagation();
		var $this = $(this);
		var group_id = $(this).attr('gid');
		var vehicle_id = $(this).attr('mid');
		
		$.post(rootUrl+'statics/toggleGroupRights', {group_id: group_id, vehicle_id: vehicle_id}, function(data){
			
			var json = $.parseJSON(data);
			
			window.alert(json.message);
	
		});	
		
	});
}


function removeGroupEditListeners()
{
	$('.member_edit_item, .member_remove_no, .member_remove_yes').unbind('click');
}

function addGroupListeners()
{
	
	$('#invite_group_button').click(function(){
		var vehicles = [];
		var group_id = $(this).attr('gid');
		$.each($('.selectize-input .item'), function(){
			
			var vid = $(this).attr('data-value');
			
			vehicles.push(vid);
			
		});
		
		
		$.post(rootUrl+'statics/invite_vehicles_to_group',{vehicles:vehicles, group_id:group_id}, function(data){
			$('.close_overlay').click();
			var elem = '<input type="text" autocomplete="off" tabindex="" placeholder="Type in name..." style="width: 76px; opacity: 1; position: relative; left: 0px;">';
			$('.selectize-input').empty().append(elem);
		});
		
	});
	
	
	
	$('#invite_event_button').click(function(){
		var vehicles = [];
		var event_id = $(this).attr('gid');
		$.each($('.selectize-input .item'), function(){
			
			var vid = $(this).attr('data-value');
			
			vehicles.push(vid);
			
		});
		
		
		$.post(rootUrl+'statics/invite_vehicles_to_event',{vehicles:vehicles, event_id:event_id}, function(data){
			$('.close_overlay').click();
		});
		
	});
	
	
	$('#invite_followers_event_button').click(function(){
		
		var event_id = $(this).attr('gid');
		$.post(rootUrl+'statics/invite_all_followers_event',{event_id:event_id}, function(data){
			$('.close_overlay').click();
		});
		
	});
	
	$('#invite_followers_group_button').click(function(){
		
		var group_id = $(this).attr('gid');
		$.post(rootUrl+'statics/invite_all_followers',{group_id:group_id}, function(data){
			$('.close_overlay').click();
		});
		
	});
	
	
	
	
	$('.group_comment_send').click(function() {
	   
	        
	        var post = $('.group_comment_input').val();
	        var gid = $('.group_comment_input').attr('gid');
	         var fname = $(this).attr('fname');
	        
	        $.post(rootUrl+'send_group_post',{gid:gid, post:post, fname:fname}, function(data){
	        	var json = $.parseJSON(data);
				location.reload();
				
        	
	        });
	    
	});
	/*$('.group_comment_input').keypress(function(event) {
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13') {
	        
	        var post = $(this).val();
	        var gid = $(this).attr('gid');
	        
	        $.post(rootUrl+'send_group_post',{gid:gid, post:post}, function(data){
	        	var json = $.parseJSON(data);
				location.reload();
				
        	/*	var elem = '<div class="group_post_item " >'+
        					'<div class="group_post_content">'+
								'<img class="group_post_img" src="'+rootUrl+'items/uploads/profilepictures/'+json.profile_image+'" />'+								
								'<div class="group_post_text_holder" style="height:auto;">'+
										'<div class="group_post_title"style="color:#ffffff;width: 155px;margin-left:100px;">'+json.nickname+'</div>'+
										'<div class="group_post_text">'+json.text+'</div>'+
								'</div>'+	
								'<div class="eventFade"></div>';
																	
					if(json.image != "" && json.image !== null){ 
	
						elem += '<div class="group_post_image_holder" style="background-image: url('+json.image+');"></div>';

					}
							
					elem +=	'</div></div>';
        		
        		$('#post_holder').find('.story_holder').prepend(elem);
        		
        		setTimeout(function(){
	        		initGroupPkry();
        		},100);
				*/
	/*        });
	    }
	});
	*/
	
	
	$('#show_events').click(function(){
		$(this).hide();
		$('#post_holder').hide();
		$('#event_holder, #show_posts').fadeIn();
		initGroupPkry();
	});
	
	
	$('#show_posts').click(function(){
		$(this).hide();
		$('#event_holder').hide();
		$('#post_holder, #show_events').fadeIn();
		initGroupPkry();
	});
	
	
	$('#new_group').click(function(){
		$(this).hide();
		$('#group_new_box').show();
	});
	
	
	$('#new_user_event').click(function(){
	
		$('#user_event_overlay').show();
		scrollTo('new_user_event');
	});
	
	
	$('.create_event').click(function(){
		$('#group_event_overlay').fadeIn();
		scrollTo('group_event_overlay');
	});
	
	
	$('.invite_group').click(function(){
		var top = $(this).position().top;
		$('#invite_overlay').css({top:top-200}).fadeIn();
		scrollTo('invite_overlay');
	});
	
	
	$('.user_invite_group').click(function(){
		var top = 50;
		$('#invite_overlay').css({top:top}).fadeIn();
		scrollTo('invite_overlay');
	});
	
	
	$('.clone_group_event').click(function(){
		var event_id = $(this).attr('eid');
			
 
	      $.post(rootUrl+"statics/clone_group_event", { event_id: event_id}, function(data){
	      
				window.location.href = rootUrl+"group_event/"+data;
			});
			
		
	});
	
	
	$('#create_event_button').click(function(){
		var title = $('#group_event_title').val();
		var description = $('#group_event_description').val();
		var address = $('#group_event_address').val();
		var date = $('#group_event_date').val();
		var group_id = $(this).attr('gid');
		var image = $('#preupload_cover').val();
		
		if(title != "" && description != "" && date != "" && address != "")
		{
			
			geocoder.geocode({'address': address}, function(results, status) {
			      if (status == google.maps.GeocoderStatus.OK) {

					var geo_location = results[0].geometry.location.lat()+", "+ results[0].geometry.location.lng();
	 
			      } else {		      	
			       // handleNoGeolocation(false);
			       	   var geo_location = '0, 0';

				       
			      }
 
			      $.post(rootUrl+"statics/add_group_event", { title: title,  description: description, date: date, group_id: group_id, address:address, geo_location:geo_location, image:image}, function(data){
						$('#group_event_overlay').fadeOut();
						location.reload();
					});
			});
			
			
			
		}
		else
		{
			$('#group_event_error_message').empty().text('Please fill out all the fields!');
		}
		
	});
	
	
	
	$('#create_user_event_button').click(function(){
		var title = $('#group_event_title').val();
		var description = $('#group_event_description').val();
		var date = $('#group_event_date').val();
		var address = $('#autocomplete').val();
		var pb = $('#event_public').val();
		var image = $('#preupload_cover').val();
		
		
		if(title != "" && description != "" && date != "" && address != "")
		{
			
			geocoder.geocode({'address': address}, function(results, status) {
			      if (status == google.maps.GeocoderStatus.OK) {

					var geo_location = results[0].geometry.location.lat()+", "+ results[0].geometry.location.lng();
	 
			      } else {		      	
			       // handleNoGeolocation(false);
			       	   var geo_location = '0, 0';

				       
			      }
 
			      $.post(rootUrl+"statics/add_user_event", {pb:pb, title: title,  description: description, date: date, address:address, geo_location:geo_location, image:image}, function(data){
						$('#user_event_overlay').fadeOut();
						location.reload();
					});
			});
			
			
			
		}
		else
		{
			$('#group_event_error_message').empty().text('Please fill out all the fields!');
		}
		
		
		
		
		
	});
	
	
	
	$('#request_new_grp_btn').click(function(){
		var $this = $(this);
		
		$this.hide();
		
		var name = $('#group_name').val();
		var description = $('#group_description').val();
		var category = $('#category_select').val();
		
		var address = $('#autocomplete').val();
		
		if(name != "" && description != "" && $('#group_create_terms').is(":checked") && category != 0 && address != "")
		{		
			
			
				
				geocoder.geocode({'address': address}, function(results, status) {
				      if (status == google.maps.GeocoderStatus.OK) {
	
						var geo_location = results[0].geometry.location.lat()+", "+ results[0].geometry.location.lng();
						$.post(rootUrl+"statics/register_group", { name: name,  description: description, category:category, address:address, geo_location:geo_location}, function(data){
							
							window.location.href = rootUrl+'group/'+data;
							
						});
						
				         
				      } else {		      	
				       // handleNoGeolocation(false);
				       	   var geo_location = "0, 0";
				       	   $.post(rootUrl+"statics/register_group", { name: name,  description: description, category:category, address:address, geo_location:geo_location}, function(data){
								
								window.location.href = rootUrl+'group/'+data;
								
							});
	
					       
				      }
   
				});
			
			
		}
		else
		{
			$('#request_new_grp_btn').show();
			$('.error_msg').text('Please enter the name and address, select the category and add a description of your club!');
			
		}
		
		
		
		
	});
	
	
	
	$('#join_group').click(function(){
		var group_id = $(this).attr('cid');
		$.post(rootUrl+"statics/join_group", { group_id: group_id}, function(data){
			 
			 var json = $.parseJSON(data);
			 
			 if(json.status)
			 {
				 location.reload();
			 }
			 else
			 {
				 window.alert('Request sent!');
			 }
			 
		});
		
		
	});
	
	$('#leave_group').click(function(){
		var group_id = $(this).attr('cid');
		$.post(rootUrl+"statics/leave_group_vehicle", { group_id: group_id}, function(data){
				 
				 location.reload();		 

		});
		
		
	});
	
	
	
	
	$('.accept_group_application').click(function(){
		var group_id = $(this).attr('gid');
		var vehicle_id = $(this).attr('cid');
		var $this = $(this);
		
		$.post(rootUrl+'statics/accept_group_request', {group_id: group_id, vehicle_id: vehicle_id}, function(data){
			$this.parent().parent().fadeOut();
			
				var json = $.parseJSON(data);
				
				var elem = '<div class="member_edit_item">'+
										'<img class="member_item_img" src="'+rootUrl+'items/uploads/profilepictures/'+json.profile_image+'" />'+
										'<div class="member_img_fade"></div>'+
										'<div class="member_edit_item_title">'+json.nickname+'</div>'+
										'<div class="member_remove_holder">'+
											'<div class="regular" style="font-size:10px;">Are you sure you want to remove this vehicle?</div>'+
											'<div class="member_remove_yes" gid="'+group_id+'" mid="'+vehicle_id+'">YES</div>'+
											'<div class="member_remove_no">NO</div>'+
										'</div>'+
									'</div>';
				
				$('#edit_members_holder').append(elem);
				
				removeGroupEditListeners();
				addGroupEditListeners();
		});
		
	});
	
	
	$('.decline_group_application').click(function(){
		var group_id = $(this).attr('gid');
		var vehicle_id = $(this).attr('cid');
		var $this = $(this);
		$.post(rootUrl+'statics/decline_group_request', {group_id: group_id, vehicle_id: vehicle_id}, function(data){
			
			$this.parent().parent().fadeOut();
		});
	});
	
	
	
	$('#update_group').click(function(){
		var group_id = $(this).attr('gid');
		var name = $('#group_name').val();
		var description = $('#group_description').val();
		var address = $('#autocomplete').val();
		var auto = $('input[name=group_auto_accept]:checked').val();
		
		
		if(name != "" && description != "" && address != "")
		{
			
			geocoder.geocode({'address': address}, function(results, status) {
			      if (status == google.maps.GeocoderStatus.OK) {

					var geo_location = results[0].geometry.location.lat()+", "+ results[0].geometry.location.lng();
	 
			      } else {		      	
			       // handleNoGeolocation(false);
			       	   var geo_location = '0, 0';

				       
			      }
 
			     $.post(rootUrl+'statics/edit_group_validation', {name: name,  description: description, gid:group_id, address:address, geo_location:geo_location, auto:auto}, function(data){
					window.location.href = rootUrl+'group/'+group_id;
				});
			});
			
			
			
		}
		else
		{
			$('#login_error_message').empty().text('Please fill out all the fields!');
		}
		
		
		
		
	});
	
	
	//USER PROFILE
	$('#post_image').change(function()
	{
		if(!$('html').hasClass('no-filereader'))
		{
				
			var type = "images";
			var is_user = false;
			
			 /*LOADING OVERLAY SHOW*/
		    	$('#loadingBG').fadeIn();
		    	$('#loadingGif').fadeIn();
				$("body").css("overflow", "hidden");
			
		
			oFReader = new FileReader();
			var dummy = $(this);
			
			oFReader.onload = function (oFREvent) 
	        {
	            var dataURI = oFREvent.target.result;
	         
	        };
	
	    	oFReader.readAsDataURL(document.getElementById($(this).attr('id')).files[0]);
	    	
	    	
	    	
		    var file_data = $(this).prop('files')[0];   

		    var form_data = new FormData();                  
		    form_data.append('file', file_data);
		          
			
		    
		                             
		    $.ajax({
		                url: rootUrl+"ajax_upload/"+type, // point to server-side PHP script 
		                dataType: 'text',  // what to expect back from the PHP script, if anything
		                cache: false,
		                contentType: false,
		                processData: false,
		                data: form_data,                         
		                type: 'post',
		                success: function(php_script_response){
		                
		                	var json = $.parseJSON(php_script_response);
		                	
		                     // display response from the PHP script, if any
		                    if(php_script_response != "FAIL")
		                    {
		                    	$('.group_comment_send').attr('fname', json.fname);
		                    	  
			                   
		                    }
		                    else
		                    {
			                    window.alert(upload_failed_text);
		                    }
		                    
		                    
		                    /* LOADING OVERLAY HIDE*/
		                    $('#loadingBG').hide();
					    	$('#loadingGif').hide();
							$("body").css("overflow", "visible");
		                }
		     });
			
	    	
    	}
    	
    	
	});
	
	
	
	
}



function initPkry()
{
	
	$container = $('#eventHolder');
	// init
	$container.packery({
	  itemSelector: '.event_item_sortable',
	  gutter: 20
	});
}


function reLayoutPkry()
{
	$container.packery('layout');
}


function initGroupPkry()
{
	$.each($('.story_holder'), function(){
		$container = $(this);
		// init
		$container.packery({
		  itemSelector: '.group_post_item',
		  gutter: 10
		});
	})
	
	
}


function window_scroll_event_handler()
{
	if($('#eventHolder').length > 0)
	{				
		$(window).scroll(function() 
		{
				
			if($(window).scrollTop() + $(window).height() > $(document).height() - 100) 
			{
				
		   		$(window).unbind('scroll');
		   		loadContent();
		   	}
		});	
	}
}

function loadContent()
{
	
	if(!is_loading)
	{
		is_loading = true;
		$.ajax(
		{
			url: rootUrl + 'statics/sample_test',
			data: {offset: content_offset},
			method: 'GET',
			success: function(data)
			{
				
				ret = $.parseJSON(data);
				if(ret.success)
				{
					content_offset = ret.new_offset;
					//console.log(ret.html);
				//	var test = $(ret.html);
					var previousH = $('#eventHolder').height();
					console.log(previousH);
					$('#eventHolder').append(ret.html);
					
					
					$container.packery('destroy');
				//	setTimeout(function(){
						initPkry();
						
						
						 $('html, body').scrollTop(previousH);
   
			//		},200);
					
					
					removeNewsListeners();
					
					$('.news_comment_button').click(function(){
						var $this = $(this);
						var cc = $this.attr('cc');
						var parent = $(this).parent().parent().parent();
						
						parent.toggleClass('open');
						
						if(parent.hasClass('open'))
						{
							$this.text('CLOSE');
							$(this).attr('h', parent.height());
							parent.animate({width:800, height:505},500,function(){
							
								parent.find('.comment_content').fadeIn();
								reLayoutPkry();
								
							});
						}
						else
						{
							var add = '';
							if(cc > 0)
							{
								add = '('+cc+')';
							}
							$this.text('COMMENT '+add);
							parent.find('.comment_content').fadeOut();
							parent.animate({width:390, height:$(this).attr('h')},500,function(){
							
								
								reLayoutPkry();
								
							});
						}
						
					});
					
					$('.comment_input').keypress(function(event) {
					   	 $this = $(this);
					    var keycode = (event.keyCode ? event.keyCode : event.which);
					    if(keycode == '13') {
					        
					        var comment = $.trim($(this).val());
					        var eid = $(this).attr('eid');
					        var parent = $(this).parent();
					        if($.trim(comment) != '')
							{
						        $.post(rootUrl+'send_news_comment',{eid:eid, comment:comment}, function(data){
						        	var json = $.parseJSON(data);
					
					        		var elem = '<div class="comment_holder_item">'+
													'<img class="comment_profile_img" src="'+rootUrl+'items/uploads/profilepictures/'+json.profile_image+'">'+
													'<div class="comment_name dosissemi">'+json.nickname+'</div>'+
													'<div class="news_comment_text dosisreg">'+json.comment+'</div>'+
												'</div>';
					        		
					        		parent.find('.comments').prepend(elem);
									
									$this.val('');
						        });
						    }
					    }
					});
					
					
					$(".like_image").click(function() {
						var pic_id = $(this).attr('pid');
						var $this = $(this);
						var like_count = $this.parent().find(".the_likes").text();
						$.post( rootUrl+"statics/addLike",{pid:pic_id}, function( data ) {		
							var newCount = parseInt(like_count)+1;
							$this.parent().find(".the_likes").text(newCount);
							$this.remove();
						});
					});
					
					$(".image_social_like").click(function() {
						var pic_id = $(this).attr('pid');
						var $this = $(this);
						var like_count = $this.parent().find(".news_likes_holder").text();
						$.post( rootUrl+"statics/addLike",{pid:pic_id}, function( data ) {
							if(data == "OK")
							{
								var newCount = parseInt(like_count)+1;
								$this.parent().find(".news_likes_holder").text(newCount);							
							}	
							else
							{
								$this.effect("shake", {
							        direction: "left",
							        times: 4,
							        distance: 2
								});
							}
						});
					});
					
					$(".event_social_like").click(function() {
						var event_id = $(this).attr('eid');
						var $this = $(this);
						var like_count = $this.parent().find(".news_likes_holder").text();
						$.post( rootUrl+"statics/addEventLike",{eid:event_id}, function( data ) {		
							if(data == "OK")
							{
								var newCount = parseInt(like_count)+1;
								$this.parent().find(".news_likes_holder").text(newCount);							
							}	
							else
							{
								$this.effect("shake", {
							        direction: "left",
							        times: 4,
							        distance: 2
								});
							}
				
						});
					});
					
					
					
					$('.share_image_fb').click(function(){
						var pid = $(this).attr("pid");
						var fname = $(this).attr("fname");
						var title = $(this).attr("title");
						facebookShareImage(fname,title,pid);
					});
					
					$('.share_image_tw').click(function(){
						var pid = $(this).attr("pid");
						var component = "photo/"+pid;
						tweetShare(component);
					});
					
					
					
					
	
					window_scroll_event_handler()
					if(ret.image_count < 10)
						$(window).unbind('scroll');
						
					is_loading = false;
				}
			}
		});			
	}
}



function removeNewsListeners()
{
	$('.news_comment_button').unbind('click');
	$('.comment_input').unbind('keypress');
	$('.like_image').unbind('click');
	$('.image_social_like').unbind('click');
	$('.event_social_like').unbind('click');
	$('.share_image_fb').unbind('click');
	$('.share_image_tw').unbind('click');
}




function addSearchListeners()
{
	$("#profile_search_submit").click(function() {
	   var search = $('#profile_search').val();
	   var my_crew = 0;
	   var for_sale = 0;
	   if($('#only_my_crew').is(":checked"))
	   {
	   		my_crew = 1;
	   }
	   
	   if($('#for_sale').is(":checked"))
	   {
	   		for_sale = 1;
	   }
	   
	   var vehicle_type = $('#vehicle_type').val();
	   var engine_type = $('#engine_type').val();
	   var year_select = $('#vehicle_year_select').val();
	   var make_input = $('#make_search').val();
	   var model_input = $('#model_search').val();
	   var plate_input = $('#plate_search').val();
	   var zerosixty_input = $('#zerosixty_search').val();
	   var hp_ranges = $('#car_hpranges').val();
	   var rank = $('#car_rank').val();
	   var nearby_users = [];
	   var $this = $(this);
	   $this.text('Searching...');

	  
		   var nearby = "false";
		   $.post( rootUrl+"car/profile_search",{rank:rank, hp_ranges:hp_ranges, for_sale:for_sale, search:search, my_crew:my_crew, vehicle_type:vehicle_type, engine_type:engine_type, year_select:year_select, make_input:make_input, model_input:model_input, nearby:nearby, plate_input:plate_input, zerosixty_input:zerosixty_input, users:[]}, function( data ) {
				$('#resultHolder').empty().append(data);
				$this.text('SEARCH');
				$('.follow_circle').unbind('click');
				
				$('.follow_circle').click(function(e){
					e.preventDefault();
					e.stopPropagation();
					var cid = $(this).attr('vid');
					var refresh = $(this).attr('refresh');
					var $this = $(this);
					$.post( rootUrl+"car/followVehicle",{cid:cid}, function( data ) {
						  $this.parent().fadeOut();
						 
						 if(refresh == "yes")
						 {
							location.reload(); 
						 }
						 
					});
				});
				
				
				
				
				
				
				if(data == '')
				{
					
					$('#resultHolder').empty().append('No vehicles found!');
				}
			}); 
	  
	  
	   
	});
	
	
	
	
	
	$("#group_search_submit").click(function() {
	   var search = $('#profile_search').val();
	   var nearby_users = [];
	   var $this = $(this);
	   $this.text('Searching...');
	   
	   var nearby = "false";
	   $.post( rootUrl+"statics/group_search_name",{search:search, nearby:nearby, users:[]}, function( data ) {
			$('#resultHolder').empty().append(data);
			$this.text('SEARCH');
			
			if(data == 'NO')
			{
				
				$('#resultHolder').empty().append('No groups found!');
			}
		}); 
	   
	  
	   
	});
	
	
	$('#vehicle_search_reset').click(function(){
		$('#vehicle_search_form')[0].reset();
		
		 var $select = $('#make_search').selectize();
		 var control = $select[0].selectize;
		 control.clear();
		 
		 var $select = $('#model_search').selectize();
		 var control = $select[0].selectize;
		 control.clear();
		
		
	});
	
	
	
	$('#suggest_poi').click(function(){
		$('#poi_suggestion_overlay').fadeIn();
	});
	
	
	
	$('#create_poi_button').click(function(){
		
		var name = $('#poi_name').val();
		var description = $('#poi_description').val();
		var address = $('#poi_address').val();
		var category = $('#poi_suggest_categories').val();
		
		
		
		
		if(name != "" && description != "" && address != "" && category != 0)
		{
			
			geocoder.geocode({'address': address}, function(results, status) {
		      if (status == google.maps.GeocoderStatus.OK) {
	
				var geo_location = results[0].geometry.location.lat()+", "+ results[0].geometry.location.lng();
	
		      } else {		      	
		       // handleNoGeolocation(false);
		       	   var geo_location = '0, 0';
	
			       
		      }
		   
		   
		   
			    $.post(rootUrl+'statics/add_poi_suggestion', {name: name,  description: description, address:address, category:category, geo_location:geo_location}, function(data){
							
					$('#poi_error_message').empty().text('Suggestion saved!');
					$('.close_overlay').click();
					
					$('#poi_name').val('');
					$('#poi_description').val('');
					$('#poi_address').val('');
					$('#poi_suggest_categories').val(0);
					$('#poi_error_message').empty().text('');
					
				});
			
			});
			
		}
		else
		{
			$('#poi_error_message').empty().text('Please fill out all the fields!');
		}
		
		
		
		
	});
	
}




function addImageListeners()
{
	
		$('.news_comment_button').click(function(){
			var $this = $(this);
			var cc = $this.attr('cc');
			var parent = $(this).parent().parent().parent();
			
			parent.toggleClass('open');
			
			if(parent.hasClass('open'))
			{
				$this.text('CLOSE');
				$(this).attr('h', parent.height());
				parent.animate({width:800, height:505},500,function(){
				
					parent.find('.comment_content').fadeIn();
					reLayoutPkry();
					
				});
			}
			else
			{
				var add = '';
				if(cc > 0)
				{
					add = '('+cc+')';
				}
				$this.text('COMMENT '+add);
				parent.find('.comment_content').fadeOut();
				parent.animate({width:390, height:$(this).attr('h')},500,function(){
				
					
					reLayoutPkry();
					
				});
			}
			
		});
	
	
	$('.comment_input').keypress(function(event) {
	   	$this = $(this);
	    var keycode = (event.keyCode ? event.keyCode : event.which);
	    if(keycode == '13') {
	        
	        var comment = $.trim($(this).val());
			var eid = $(this).attr('eid');
	        var parent = $(this).parent();
	        if($.trim(comment) != '')
	        {
		        $.post(rootUrl+'send_news_comment',{eid:eid, comment:comment}, function(data){
		        	var json = $.parseJSON(data);
	
	        		var elem = '<div class="comment_holder_item">'+
									'<img class="comment_profile_img" src="'+rootUrl+'items/uploads/profilepictures/'+json.profile_image+'">'+
									'<div class="comment_name dosissemi">'+json.nickname+'</div>'+
									'<div class="news_comment_text dosisreg">'+json.comment+'</div>'+
								'</div>';
	        		
	        		parent.find('.comments').prepend(elem);
					$this.val('');
		        });
		    }
	    }
	});
	
	
	
	
	$('.share_image_fb').click(function(){
		var pid = $(this).attr("pid");
		var fname = $(this).attr("fname");
		var title = $(this).attr("title");
		
		
		
		facebookShareImage(fname,title,pid);
	});
	
	$('.share_image_tw').click(function(){
		var pid = $(this).attr("pid");
		var component = "photo/"+pid;
		tweetShare(component);
	});

	$('#user_edit_profile_upload').click(function(e){
		e.preventDefault();
		if(!$('html').hasClass('no-filereader'))
		{
			$('#user_edit_profile_picture').click();
		}
		else
		{
			$('#add_profileimage_ie').click();
		}
		
	});
	
	//USER PROFILE
	$('#user_edit_profile_picture').change(function()
	{
		if(!$('html').hasClass('no-filereader'))
		{
			var preview = $(this).attr("preview");	
			var type = "coverpictures";
			var is_user = false;
			
			 /*LOADING OVERLAY SHOW*/
		    	$('#loadingBG').fadeIn();
		    	$('#loadingGif').fadeIn();
				$("body").css("overflow", "hidden");
			
			
			
			type = "profilepictures";
			is_user = true;
			
		
			oFReader = new FileReader();
			var dummy = $(this);
			
			oFReader.onload = function (oFREvent) 
	        {
	            var dataURI = oFREvent.target.result;
	         
	        };
	
	    	oFReader.readAsDataURL(document.getElementById($(this).attr('id')).files[0]);
	    	
	    	
	    	
		    var file_data = $(this).prop('files')[0];   

		    var form_data = new FormData();                  
		    form_data.append('file', file_data);
		          
			
		    
		                             
		    $.ajax({
		                url: rootUrl+"ajax_upload/"+type, // point to server-side PHP script 
		                dataType: 'text',  // what to expect back from the PHP script, if anything
		                cache: false,
		                contentType: false,
		                processData: false,
		                data: form_data,                         
		                type: 'post',
		                success: function(php_script_response){
		                
		                	
		                	
		                     // display response from the PHP script, if any
		                    if(php_script_response != "FAIL")
		                    {
		                    	var json = $.parseJSON(php_script_response);
		                    	var source = rootUrl+"items/uploads/"+type+"/"+json.fname;
		                    	var elem = "<img id='img_to_crop' src="+source+">";
			                    $("#image_crop_holder").append(elem);
			                    $('#finish_crop').attr("image_type", type);
			                    $('#finish_crop').attr("user", is_user);
			                    $('#finish_crop').attr("image_name", "items/uploads/"+type+"/"+json.fname);
			                    $('#finish_crop').attr("file_name", json.fname);
			               
			                    
			                     $('#finish_crop_vet').attr("image_type", type);
			                    $('#finish_crop_vet').attr("user", is_user);
			                    $('#finish_crop_vet').attr("image_name", "items/uploads/"+type+"/"+json.fname);
			                    $('#finish_crop_vet').attr("file_name", json.fname);
			        
			                    $('#image_cropping_overlay').fadeIn();
			                    $('#crop_title').text("Profile picture");
			                   
				                   setTimeout(function(){	
				                   		$('input[name="selection_x1"]').val(0);
							            $('input[name="selection_y1"]').val(0);
							            $('input[name="selection_x2"]').val(300);
							            $('input[name="selection_y2"]').val(300);
										cropping_overlay_instance = $('#image_crop_holder').imgAreaSelect({ 
					                    	aspectRatio: '300:300',
					                    	minHeight:300, 
					                    	minWidth:300,
					                    	maxWidth:1024, 
					                    	handles: true,
					                    	instance: true,
						                    onSelectEnd: function (img, selection) {
									            $('input[name="selection_x1"]').val(selection.x1);
									            $('input[name="selection_y1"]').val(selection.y1);
									            $('input[name="selection_x2"]').val(selection.x2);
									            $('input[name="selection_y2"]').val(selection.y2);            
									        }, 
									        
									        
									        
									    });
				                   
				                    
					                cropping_overlay_instance.setOptions({ show: true, x1: 0, y1: 0, x2: 180, y2: 180 }); 
					                    var ht =  $("#img_to_crop").height();
						                 	
						                   $("#image_crop_holder").css({height: ht});  
				                    },1500);
			                   
			                   
			                   
			                   		
			                   				                   
			                   
			                   
		                    }
		                    else
		                    {
			                    window.alert('Image upload Failed, please choose a smaller file');
			                    $('#loadingBG').hide();
								$('#loadingGif').hide();
								$("body").css("overflow", "visible");
		                    }
		                    
		                    
		                    /* LOADING OVERLAY HIDE*/
		                    $('#loadingBG').hide();
					    	$('#loadingGif').hide();
							$("body").css("overflow", "visible");
		                }
		     });
			
	    	
    	}
    	
    	
	});
	
	// IMAGE UPLOAD
	/*$('#upload_picture').change(function()
	{
		if(!$('html').hasClass('no-filereader'))
		{
			
			var type = "images";
			var is_user = false;
			var cid = $(this).attr('cid');
			
		    	$('#loadingBG').fadeIn();
		    	$('#loadingGif').fadeIn();
				$("body").css("overflow", "hidden");

			oFReader = new FileReader();
			var dummy = $(this);
			
			oFReader.onload = function (oFREvent) 
	        {
	            var dataURI = oFREvent.target.result;
	         
	        };
	
	    	oFReader.readAsDataURL(document.getElementById($(this).attr('id')).files[0]);
	    	
	    	
	    	
		    var file_data = $(this).prop('files')[0];   
console.log(file_data);
		    var form_data = new FormData();                  
		    form_data.append('file', file_data);
		    form_data.append('cid', cid);
		          
			
		    console.log(form_data);
		                             
		    $.ajax({
		                url: rootUrl+"statics/add_photo", // point to server-side PHP script 
		                dataType: 'text',  // what to expect back from the PHP script, if anything
		                cache: false,
		                contentType: false,
		                processData: false,
		                data: form_data,                         
		                type: 'post',
		                success: function(php_script_response){
		                
		                	var json = $.parseJSON(php_script_response);
		                	
		                     // display response from the PHP script, if any
		                    if(php_script_response != "FAIL")
		                    {
		                    	
			                    location.reload();
			                   
			                   
		                    }
		                    else
		                    {
			                    window.alert(upload_failed_text);
		                    }
		                    
		                    
		                   
		                    $('#loadingBG').hide();
					    	$('#loadingGif').hide();
							$("body").css("overflow", "visible");
		                }
		     });
			
	    	
    	}
    	
    	
	});
	
	*/
	
	// VEHICLE PROFILE
	$('#edit_profile_picture').change(function()
	{
		if(!$('html').hasClass('no-filereader'))
		{
			var preview = $(this).attr("preview");	
			var type = "profilepictures";
			var is_user = false;
			
			 /*LOADING OVERLAY SHOW*/
		    	$('#loadingBG').fadeIn();
		    	$('#loadingGif').fadeIn();
				$("body").css("overflow", "hidden");
			
		
			oFReader = new FileReader();
			var dummy = $(this);
			
			oFReader.onload = function (oFREvent) 
	        {
	            var dataURI = oFREvent.target.result;
	          
	        };
	
	    	oFReader.readAsDataURL(document.getElementById($(this).attr('id')).files[0]);
	    	
	    	
	    	
		    var file_data = $(this).prop('files')[0];   

		    var form_data = new FormData();                  
		    form_data.append('file', file_data);
		          
			
		    
		                             
		    $.ajax({
		                url: rootUrl+"ajax_upload/"+type, // point to server-side PHP script 
		                dataType: 'text',  // what to expect back from the PHP script, if anything
		                cache: false,
		                contentType: false,
		                processData: false,
		                data: form_data,                         
		                type: 'post',
		                success: function(php_script_response){
		                
		                	
		                	
		                     // display response from the PHP script, if any
		                    if(php_script_response != "FAIL")
		                    {
		                    	var json = $.parseJSON(php_script_response);
		                    	var source = rootUrl+"items/uploads/"+type+"/"+json.fname;
		                    	var elem = "<img id='img_to_crop' src="+source+" />";
			                    $("#image_crop_holder").append(elem);
			                    $('#finish_crop').attr("image_type", type);
			                    $('#finish_crop').attr("user", is_user);
			                    $('#finish_crop').attr("image_name", "items/uploads/"+type+"/"+json.fname);
			                    $('#finish_crop').attr("file_name", json.fname);
			               
			                    
			                     $('#finish_crop_vet').attr("image_type", type);
			                    $('#finish_crop_vet').attr("user", is_user);
			                    $('#finish_crop_vet').attr("image_name", "items/uploads/"+type+"/"+json.fname);
			                    $('#finish_crop_vet').attr("file_name", json.fname);
								
								
								 
			                    $('#image_cropping_overlay').fadeIn();
			                  
			                   		$('#crop_title').text("Profile picture");
			                   		
			                   		
			                   	setTimeout(function(){	
			                   		$('input[name="selection_x1"]').val(0);
						            $('input[name="selection_y1"]').val(0);
						            $('input[name="selection_x2"]').val(180);
						            $('input[name="selection_y2"]').val(180);
									cropping_overlay_instance = $('#image_crop_holder').imgAreaSelect({ 
				                    	aspectRatio: '180:180',
				                    	minHeight:180, 
				                    	minWidth:180,
				                    	maxWidth:1500, 
				                    	handles: true,
				                    	instance: true,
					                    onSelectEnd: function (img, selection) {
								            $('input[name="selection_x1"]').val(selection.x1);
								            $('input[name="selection_y1"]').val(selection.y1);
								            $('input[name="selection_x2"]').val(selection.x2);
								            $('input[name="selection_y2"]').val(selection.y2);            
								        }, 
								        
								        
								        
								    });
			                   
			                    
				                cropping_overlay_instance.setOptions({ show: true, x1: 0, y1: 0, x2: 180, y2: 180 }); 
				                    var ht =  $("#img_to_crop").height();
					                 	
					                   $("#image_crop_holder").css({height: ht});  
			                    },1500);
			                   
		                    }
		                    else
		                    {
			                     window.alert('Image upload Failed, please choose a smaller file');
				                    $('#loadingBG').hide();
									$('#loadingGif').hide();
									$("body").css("overflow", "visible");
		                    }
		                    
		                    
		                    /* LOADING OVERLAY HIDE*/
		                    $('#loadingBG').hide();
					    	$('#loadingGif').hide();
							$("body").css("overflow", "visible");
		                }
		     });
			
	    	
    	}
    	
    	
	});
	
	
	// VEHICLE COVER
	$('#edit_cover_picture').change(function()
	{
		if(!$('html').hasClass('no-filereader'))
		{
			var preview = $(this).attr("preview");	
			var type = "coverpictures";
			var is_user = false;
			
			 /*LOADING OVERLAY SHOW*/
		    	$('#loadingBG').fadeIn();
		    	$('#loadingGif').fadeIn();
				$("body").css("overflow", "hidden");
			
		
			oFReader = new FileReader();
			var dummy = $(this);
			
			oFReader.onload = function (oFREvent) 
	        {
	            var dataURI = oFREvent.target.result;
	           
	        };
	
	    	oFReader.readAsDataURL(document.getElementById($(this).attr('id')).files[0]);
	    	
	    	
	    	
		    var file_data = $(this).prop('files')[0];   

		    var form_data = new FormData();                  
		    form_data.append('file', file_data);
		          
			
		    
		                             
		    $.ajax({
		                url: rootUrl+"ajax_upload/"+type, // point to server-side PHP script 
		                dataType: 'text',  // what to expect back from the PHP script, if anything
		                cache: false,
		                contentType: false,
		                processData: false,
		                data: form_data,                         
		                type: 'post',
		                success: function(php_script_response){
		                
		                	
		                	
		                     // display response from the PHP script, if any
		                    if(php_script_response != "FAIL")
		                    {
		                    	var json = $.parseJSON(php_script_response);
		                    	var source = rootUrl+"items/uploads/"+type+"/"+json.fname;
		                    	var elem = "<img id='img_to_crop' src="+source+">";
			                    $("#image_crop_holder").append(elem);
			                    $('#finish_crop').attr("image_type", type);
			                    $('#finish_crop').attr("user", is_user);
			                    $('#finish_crop').attr("image_name", "items/uploads/"+type+"/"+json.fname);
			                    $('#finish_crop').attr("file_name", json.fname);
			                //    $('#finish_crop').attr("image_id", json.image_id);
			                    
			                     $('#finish_crop_vet').attr("image_type", type);
			                    $('#finish_crop_vet').attr("user", is_user);
			                    $('#finish_crop_vet').attr("image_name", "items/uploads/"+type+"/"+json.fname);
			                    $('#finish_crop_vet').attr("file_name", json.fname);
			                  //  $('#finish_crop_vet').attr("image_id", json.image_id);
			                    $('#image_cropping_overlay').fadeIn();
			                  
			                   
				                   $('#crop_title').text("Cover picture");
				                   
				                   
				                  setTimeout(function(){	
			                   		$('input[name="selection_x1"]').val(0);
						            $('input[name="selection_y1"]').val(0);
						            $('input[name="selection_x2"]').val(755);
						            $('input[name="selection_y2"]').val(320); 
									cropping_overlay_instance = $('#image_crop_holder').imgAreaSelect({
				                    	x1: 0, y1: 0, x2: 755, y2: 320, 
				                    	aspectRatio: '755:320',
				                    	minHeight:320, 
				                    	minWidth:755, 
				                    	maxWidth:1024,
				                    	handles: true,
					                    onSelectEnd: function (img, selection) {
								            $('input[name="selection_x1"]').val(selection.x1);
								            $('input[name="selection_y1"]').val(selection.y1);
								            $('input[name="selection_x2"]').val(selection.x2);
								            $('input[name="selection_y2"]').val(selection.y2);            
								        }, 
								        
								        
								        
								    });
			                   
			                    
				                cropping_overlay_instance.setOptions({ show: true, x1: 0, y1: 0, x2: 180, y2: 180 }); 
				                    var ht =  $("#img_to_crop").height();
					                 	
					                   $("#image_crop_holder").css({height: ht});  
			                    },1500);
				                   
				                   
				                   
				                   
				                   
				                   
				                   
				                   
				                   $('input[name="selection_x1"]').val(0);
						            $('input[name="selection_y1"]').val(0);
						            $('input[name="selection_x2"]').val(755);
						            $('input[name="selection_y2"]').val(320); 
				               cropping_overlay_instance = $('#image_crop_holder').imgAreaSelect({
				                    	x1: 0, y1: 0, x2: 755, y2: 320, 
				                    	aspectRatio: '755:320',
				                    	minHeight:320, 
				                    	minWidth:755, 
				                    	maxWidth:1500,
				                    	handles: true,
					                    onSelectEnd: function (img, selection) {
								            $('input[name="selection_x1"]').val(selection.x1);
								            $('input[name="selection_y1"]').val(selection.y1);
								            $('input[name="selection_x2"]').val(selection.x2);
								            $('input[name="selection_y2"]').val(selection.y2);            
								        }, 
								    }); 
			                   
			                   
			                   
			                   
		                    }
		                    else
		                    {
			                    window.alert('Image upload Failed, please choose a smaller file');
			                    $('#loadingBG').hide();
								$('#loadingGif').hide();
								$("body").css("overflow", "visible");
		                    }
		                    
		                    
		                    /* LOADING OVERLAY HIDE*/
		                    $('#loadingBG').hide();
					    	$('#loadingGif').hide();
							$("body").css("overflow", "visible");
		                }
		     });
			
	    	
    	}
    	
    	
	});

	
	
	
	
	
	
	// GROUP LOGO
	$('#edit_group_profile_picture').change(function()
	{
		if(!$('html').hasClass('no-filereader'))
		{
			var preview = $(this).attr("preview");	
			var type = "profilepictures";
			var is_user = false;
			
			 /*LOADING OVERLAY SHOW*/
		    	$('#loadingBG').fadeIn();
		    	$('#loadingGif').fadeIn();
				$("body").css("overflow", "hidden");
			
		
			oFReader = new FileReader();
			var dummy = $(this);
			
			oFReader.onload = function (oFREvent) 
	        {
	            var dataURI = oFREvent.target.result;
	          
	        };
	
	    	oFReader.readAsDataURL(document.getElementById($(this).attr('id')).files[0]);
	    	
	    	
	    	
		    var file_data = $(this).prop('files')[0];   

		    var form_data = new FormData();                  
		    form_data.append('file', file_data);
		          
			
		    
		                             
		    $.ajax({
		                url: rootUrl+"ajax_upload/"+type, // point to server-side PHP script 
		                dataType: 'text',  // what to expect back from the PHP script, if anything
		                cache: false,
		                contentType: false,
		                processData: false,
		                data: form_data,                         
		                type: 'post',
		                success: function(php_script_response){
		                
		                	var json = $.parseJSON(php_script_response);
		                	
		                     // display response from the PHP script, if any
		                    if(php_script_response != "FAIL")
		                    {
		                    	var source = rootUrl+"items/uploads/"+type+"/"+json.fname;
		                    	var elem = "<img src="+source+">";
			                    $("#image_crop_holder").append(elem);
			                    $('#finish_crop').attr("image_type", type);
			                    $('#finish_crop').attr("user", is_user);
			                    $('#finish_crop').attr("image_name", "items/uploads/"+type+"/"+json.fname);
			                    $('#finish_crop').attr("file_name", json.fname);
			               
			                    
			                     $('#finish_crop_vet').attr("image_type", type);
			                    $('#finish_crop_vet').attr("user", is_user);
			                    $('#finish_crop_vet').attr("image_name", "items/uploads/"+type+"/"+json.fname);
			                    $('#finish_crop_vet').attr("file_name", json.fname);
			                 
			                    $('#image_cropping_overlay').fadeIn();
			                  
			                   
			                   		$('#crop_title').text("Profile picture");
			                   		$('input[name="selection_x1"]').val(0);
						            $('input[name="selection_y1"]').val(0);
						            $('input[name="selection_x2"]').val(180);
						            $('input[name="selection_y2"]').val(180);
				               cropping_overlay_instance = $('#image_crop_holder').imgAreaSelect({
			                    	x1: 0, y1: 0, x2: 180, y2: 180, 
				                    	aspectRatio: '180:180',
				                    	minHeight:180, 
				                    	minWidth:180,
				                    	maxWidth:1500, 
				                    	handles: true,
					                    onSelectEnd: function (img, selection) {
								            $('input[name="selection_x1"]').val(selection.x1);
								            $('input[name="selection_y1"]').val(selection.y1);
								            $('input[name="selection_x2"]').val(selection.x2);
								            $('input[name="selection_y2"]').val(selection.y2);            
								        }, 
								    });
			                   
			                   
			                   
		                    }
		                    else
		                    {
			                    window.alert(upload_failed_text);
		                    }
		                    
		                    
		                    /* LOADING OVERLAY HIDE*/
		                    $('#loadingBG').hide();
					    	$('#loadingGif').hide();
							$("body").css("overflow", "visible");
		                }
		     });
			
	    	
    	}
    	
    	
	});
	
	
	// GROUP COVER
	$('#edit_group_cover_picture').change(function()
	{
		if(!$('html').hasClass('no-filereader'))
		{
			var preview = $(this).attr("preview");	
			var type = "coverpictures";
			var is_user = false;
			
			 /*LOADING OVERLAY SHOW*/
		    	$('#loadingBG').fadeIn();
		    	$('#loadingGif').fadeIn();
				$("body").css("overflow", "hidden");
			
		
			oFReader = new FileReader();
			var dummy = $(this);
			
			oFReader.onload = function (oFREvent) 
	        {
	            var dataURI = oFREvent.target.result;
	           
	        };
	
	    	oFReader.readAsDataURL(document.getElementById($(this).attr('id')).files[0]);
	    	
	    	
	    	
		    var file_data = $(this).prop('files')[0];   

		    var form_data = new FormData();                  
		    form_data.append('file', file_data);
		          
			
		    
		                             
		    $.ajax({
		                url: rootUrl+"ajax_upload/"+type, // point to server-side PHP script 
		                dataType: 'text',  // what to expect back from the PHP script, if anything
		                cache: false,
		                contentType: false,
		                processData: false,
		                data: form_data,                         
		                type: 'post',
		                success: function(php_script_response){
		                
		                	var json = $.parseJSON(php_script_response);
		                	
		                     // display response from the PHP script, if any
		                    if(php_script_response != "FAIL")
		                    {
		                    	var source = rootUrl+"items/uploads/"+type+"/"+json.fname;
		                    	var elem = "<img src="+source+">";
			                    $("#image_crop_holder").append(elem);
			                    $('#finish_crop').attr("image_type", type);
			                    $('#finish_crop').attr("user", is_user);
			                    $('#finish_crop').attr("image_name", "items/uploads/"+type+"/"+json.fname);
			                    $('#finish_crop').attr("file_name", json.fname);
			                //    $('#finish_crop').attr("image_id", json.image_id);
			                    
			                     $('#finish_crop_vet').attr("image_type", type);
			                    $('#finish_crop_vet').attr("user", is_user);
			                    $('#finish_crop_vet').attr("image_name", "items/uploads/"+type+"/"+json.fname);
			                    $('#finish_crop_vet').attr("file_name", json.fname);
			                  //  $('#finish_crop_vet').attr("image_id", json.image_id);
			                    $('#image_cropping_overlay').fadeIn();
			                  
			                   
				                   $('#crop_title').text("Cover picture");
				                   $('input[name="selection_x1"]').val(0);
						            $('input[name="selection_y1"]').val(0);
						            $('input[name="selection_x2"]').val(755);
						            $('input[name="selection_y2"]').val(320); 
				               cropping_overlay_instance = $('#image_crop_holder').imgAreaSelect({
				                    	x1: 0, y1: 0, x2: 755, y2: 320, 
				                    	aspectRatio: '755:320',
				                    	minHeight:320, 
				                    	minWidth:755, 
				                    	maxWidth:1500,
				                    	handles: true,
					                    onSelectEnd: function (img, selection) {
								            $('input[name="selection_x1"]').val(selection.x1);
								            $('input[name="selection_y1"]').val(selection.y1);
								            $('input[name="selection_x2"]').val(selection.x2);
								            $('input[name="selection_y2"]').val(selection.y2);            
								        }, 
								    }); 
			                   
			                   
			                   
			                   
		                    }
		                    else
		                    {
			                    window.alert(upload_failed_text);
		                    }
		                    
		                    
		                    /* LOADING OVERLAY HIDE*/
		                    $('#loadingBG').hide();
					    	$('#loadingGif').hide();
							$("body").css("overflow", "visible");
		                }
		     });
			
	    	
    	}
    	
    	
	});

	
	
	
	
	// USER EVENT COVER
	$('#edit_user_cover_picture').change(function()
	{
		if(!$('html').hasClass('no-filereader'))
		{
			var preview = $(this).attr("preview");	
			var type = "coverpictures";
			var is_user = false;
			
			 /*LOADING OVERLAY SHOW*/
		    	$('#loadingBG').fadeIn();
		    	$('#loadingGif').fadeIn();
				$("body").css("overflow", "hidden");
			
		
			oFReader = new FileReader();
			var dummy = $(this);
			
			oFReader.onload = function (oFREvent) 
	        {
	            var dataURI = oFREvent.target.result;
	           
	        };
	
	    	oFReader.readAsDataURL(document.getElementById($(this).attr('id')).files[0]);
	    	
	    	
	    	
		    var file_data = $(this).prop('files')[0];   

		    var form_data = new FormData();                  
		    form_data.append('file', file_data);
		          
			
		    
		                             
		    $.ajax({
		                url: rootUrl+"ajax_upload/"+type, // point to server-side PHP script 
		                dataType: 'text',  // what to expect back from the PHP script, if anything
		                cache: false,
		                contentType: false,
		                processData: false,
		                data: form_data,                         
		                type: 'post',
		                success: function(php_script_response){
		                
		                	var json = $.parseJSON(php_script_response);
		                	
		                     // display response from the PHP script, if any
		                    if(php_script_response != "FAIL")
		                    {
		                    	var source = rootUrl+"items/uploads/"+type+"/"+json.fname;
		                    	var elem = "<img src="+source+">";
			                    $("#image_crop_holder").append(elem);
			                    
			                    $('#finish_crop_event').attr("file_name", json.fname);
								$('#finish_crop_event').attr("image_name", "items/uploads/"+type+"/"+json.fname);
			                    $('#image_cropping_overlay').fadeIn();
			                  
			                   
				                   $('#crop_title').text("Cover picture");
				                   $('input[name="selection_x1"]').val(0);
						            $('input[name="selection_y1"]').val(0);
						            $('input[name="selection_x2"]').val(755);
						            $('input[name="selection_y2"]').val(320); 
				               cropping_overlay_instance = $('#image_crop_holder').imgAreaSelect({
				                    	x1: 0, y1: 0, x2: 755, y2: 320, 
				                    	aspectRatio: '755:320',
				                    	minHeight:320, 
				                    	minWidth:755, 
				                    	maxWidth:1500,
				                    	handles: true,
					                    onSelectEnd: function (img, selection) {
								            $('input[name="selection_x1"]').val(selection.x1);
								            $('input[name="selection_y1"]').val(selection.y1);
								            $('input[name="selection_x2"]').val(selection.x2);
								            $('input[name="selection_y2"]').val(selection.y2);            
								        }, 
								    }); 
			                   
			                   
			                   
			                   
		                    }
		                    else
		                    {
			                    window.alert(upload_failed_text);
		                    }
		                    
		                    
		                    /* LOADING OVERLAY HIDE*/
		                    $('#loadingBG').hide();
					    	$('#loadingGif').hide();
							$("body").css("overflow", "visible");
		                }
		     });
			
	    	
    	}
    	
    	
	});
	
	
	$('#finish_crop_event').click(function(){
		
		
		var fname = $(this).attr("file_name");
		var eid = $(this).attr("eid");
		var type = $(this).attr("type");
		var $this = $(this);
		
		
		var w = 755;
		var h = 320;
		
		
		var x1 = $("#selection_x1").val();
		var x2 = $("#selection_x2").val();
		var y1 = $("#selection_y1").val();
		var y2 = $("#selection_y2").val();
		
		
		
		var file = $(this).attr("image_name");
		
		$.post(rootUrl+"statics/crop_image", { x1: x1, x2 : x2, y1 : y1, y2 : y2, w : w, h : h, fname : file}, function(data){
		    
		    
		    if(eid != 0)
		    {
			    if(type == "user")
			    {
				    $.post(rootUrl+"statics/save_coverpicture_user_event", {eid: eid, fname: fname }, function(data){
						    window.location.href = rootUrl+"user_event/"+eid;
					});
			    }
			    else
			    {
				    $.post(rootUrl+"statics/save_coverpicture_group_event", {eid: eid, fname: fname }, function(data){
						    window.location.href = rootUrl+"group_event/"+eid;
					});
			    } 
		    }
		    else
		    {
		    	
			    	$('#preupload_cover').val(fname);
					$('#preupload_cover_holder').attr('src', rootUrl+file).show();
		    
					$('#close_crop').click();
			    
		    }
		    
		    
		    
		   
		    
  
		});
	
	});
	
	
	
	
	$('#close_crop').click(function(){
		var elem = $('.edit_hidden_upload');
		resetFormElement(elem);
		 $("#image_crop_holder").empty();
		$('.imgareaselect-selection').parent().hide();
		$('.imgareaselect-outer').hide();
		$(this).parent().fadeOut();
		
	});
	
	$('#finish_crop').click(function(){
	
	
		 /*LOADING OVERLAY SHOW*/
    	$('#loadingBG').fadeIn();
    	$('#loadingGif').fadeIn();
		$("body").css("overflow", "hidden");
	
	
		
		var type = $(this).attr("image_type");
		var the_user = $(this).attr("user");
		var fname = $(this).attr("file_name");
		var car_id = $(this).attr("car_id");
		var image_id = $(this).attr("image_id");
		var is_group = $(this).attr("is_group");
		var $this = $(this);
		
		if(type == "coverpictures")
		{
			var w = 755;
			var h = 320;
		}
		else
		{
			var w = 180;
			var h = 180;
		}
		
		var x1 = $("#selection_x1").val();
		var x2 = $("#selection_x2").val();
		var y1 = $("#selection_y1").val();
		var y2 = $("#selection_y2").val();
		var w = x2 - x1;
		var h = y2 - y1;
		
		
		var file = $(this).attr("image_name");
		
		$.post(rootUrl+"statics/crop_image", { x1: x1, x2 : x2, y1 : y1, y2 : y2, w : w, h : h, fname : file}, function(data){
		     if(is_group == "true")
		     {	
		     	 var group_id = $this.attr('group_id');
		     	 
		     	 if(type == "coverpictures")
			     {
			     	
				     
				    $.post(rootUrl+"statics/save_coverpicture_group", {group_id: group_id, fname: fname }, function(data){
						    window.location.href = rootUrl+"edit_group/"+group_id;
					});
			     } 
			     else
			     {
				     $.post(rootUrl+"statics/save_profilepicture_group", {group_id: group_id, fname: fname}, function(data){
						   window.location.href = rootUrl+"edit_group/"+group_id;
					});
			     } 
		     
		     	
			    
		     }
		     else if(the_user == "true")
		     {
		     	var uid = $(this).attr('uid');
			    $.post(rootUrl+"statics/save_profilepicture_user", {user_id: uid, fname: fname }, function(data){
					    window.location.href = rootUrl+"editprofile";
				}); 
		     }
		     else
		     {
		     	
			     if(type == "coverpictures")
			     {
			     	
				     $.post(rootUrl+"statics/save_coverpicture", {car_id: car_id, fname: fname, image_id:image_id}, function(data){
						    window.location.href = rootUrl+"edit_vehicle/"+car_id;
					});
			     } 
			     else
			     {
				     $.post(rootUrl+"statics/save_profilepicture", {car_id: car_id, fname: fname, image_id:image_id}, function(data){
						    window.location.href = rootUrl+"edit_vehicle/"+car_id;
					});
			     } 
		     }
		     
		     
		});
	
	});
	
	
	
	$('#edit_cover_upload, #cover_hover_holder').mouseenter(function(){
		$('#cover_hover_holder').show();
	});
	
	$('#edit_cover_upload, #cover_hover_holder').mouseleave(function(){
		$('#cover_hover_holder').hide();
	});
	
	
	
	
	
	$('#edit_profile_upload, #profile_hover_holder').mouseenter(function(){
		$('#profile_hover_holder').show();
	});
	
	$('#edit_profile_upload').mouseleave(function(){
		$('#profile_hover_holder').hide();
	});
	
	$('#cover_hover_holder').click(function(){
		
		if(!$('html').hasClass('no-filereader'))
		{
			
			$('#edit_cover_picture').click();
		}
		else
		{
			$('#add_coverimage_ie').click();
		}
		
	});
	
	
	$('#edit_profile_upload').click(function(){
		
		if(!$('html').hasClass('no-filereader'))
		{
			$('#edit_profile_picture').click();
		}
		else
		{
			$('#add_dogprofileimage_ie').click();
		}
		
	});
	
	
	
	
	
	
	
	
	/* GROUP COVER IMAGE*/
	$('#edit_group_cover_upload, #group_cover_hover_holder').mouseenter(function(){
		$('#group_cover_hover_holder').show();
	});
	
	$('#edit_group_cover_upload').mouseleave(function(){
		$('#group_cover_hover_holder').hide();
	});
	
	
	$('#group_cover_hover_holder').click(function(){
		
		if(!$('html').hasClass('no-filereader'))
		{
			
			$('#edit_group_cover_picture').click();
		}
		else
		{
			$('#add_group_coverimage_ie').click();
		}
		
	});
	
	
	
	
	/* GROUP LOGO */
	
	$('#edit_group_profile_upload, #group_profile_hover_holder').mouseenter(function(){
		$('#group_profile_hover_holder').show();
	});
	
	$('#edit_group_profile_upload').mouseleave(function(){
		$('#group_profile_hover_holder').hide();
	});
	
	$('#edit_group_profile_upload').click(function(){
		
		if(!$('html').hasClass('no-filereader'))
		{
			$('#edit_group_profile_picture').click();
		}
		else
		{
			$('#add_group_profileimage_ie').click();
		}
		
	});
	
/*	$('#upload_photo').click(function(){
		
		$('#upload_picture').click();
			
	});*/
	
	
	// ALBUMS
	$('#add_album').click(function(){
		$(this).unbind('click');
		
		var title = $('#new_album_title').val();
		var cid = $(this).attr('cid');
		
		if(title == "")
		{
			title = "Untitled";
		}
		
		$.post(rootUrl+'statics/add_album', {cid:cid, title: title}, function(data){
			
			
				$('#new_album_title').val('');
				
				
				var json = $.parseJSON(data);
				
				
				var elem = '<a href="'+rootUrl+'album/'+json.id+'">'+
								'<div class="album_item">'+
									'<div class="album_item_image" style="background-image: url('+rootUrl+'items/uploads/images/'+json.fname+'"></div>'+
									'<div class="album_item_title">'+json.title+'</div>'+
								'</div>'+
							'</a>';
				
				$('#edit_albums_holder').append(elem);
			
				
				
				location.reload();
			
		});
		
		
		
	});
	
	
	$('#remove_album').click(function(){
		$(this).hide();
		$('#delete_album_confirm_holder').fadeIn();
	});
	
	$('#album_delete_yes').click(function(){
		var album_id = $(this).attr('album_id');
		$.post(rootUrl+'statics/delete_album', {album_id:album_id}, function(data) 
		{	
			
			window.location.href = rootUrl+"albums";	
		});
	});	
	
	$('#album_delete_no').click(function(){
		$('#delete_album_confirm_holder').hide();
		$('#remove_album').fadeIn();
	});

}


function addCarListeners()
{
	
	$('.remove_album_image').click(function(){				
		$(this).parent().find('.remove_album_image_holder').show();
	});
	
	$('.delete_image_no').click(function(){	
		console.log(1);;	
		$(this).parent().hide();			
	});
	
	$('.delete_image_yes').click(function(){
		var pid = $(this).attr('pid');
		var $this = $(this);
		
		$.post( rootUrl+"statics/delete_image",{pid:pid}, function( data ) {
			  
			  	$this.parent().parent().remove();	 
		});
	});
	
	
	
	$('.story_upload_btn').click(function(){
		
		var sid = $(this).attr('sid');
		
		$('#story_image_file').attr('sid', sid);
		
		$('#story_image_file').click();
		
		
	});
	
	
	$('#delete_album_btn').click(function(){		
		$(this).hide();		
		$('#delete_album_holder').show();
	});
	
	$('#delete_album_no').click(function(){		
		$(this).parent().hide();		
		$('#delete_album_btn').show();
	});
	
	$('#delete_album_yes').click(function(){
		var aid = $(this).attr('aid');
		var pretty = $(this).attr('pretty');
		var $this = $(this);
		
		$.post( rootUrl+"statics/delete_album",{album_id:aid}, function( data ) {
			  
			  window.location.href = rootUrl+"vehicle_profile/"+pretty;		 
		});
	});
	
	
	$('#delete_mod_yes').click(function(){
		var cid = $(this).attr('cid');
		var mid = $(this).attr('mid');
		var $this = $(this);
		$.post( rootUrl+"statics/delete_mod",{car_id:cid, mod_id:mid}, function( data ) {
			  $('.delete_mod[mid="'+mid+'"]').parent().remove();
			  $('#delete_mod_overlay').fadeOut();		 
		});
	});	
		
	
	
	
	$('#upload_blog_button').click(function(){		
		$('#story_image_file_up').click();
	});
	
	
	// CAR EDIT FOR SALE
	$('#for_sale').change(function()
	{
		var $this = $(this);
		var check = $this.prop('checked');
		
		if(check)
		{
			$('#show_price').show();
		}
		else
		{
			$('#show_price').hide();
		}
	});
	
	
	
	// BLOG IMAGE BEFORE SAVE
	$('#story_image_file_up').change(function()
	{
		if(!$('html').hasClass('no-filereader'))
		{

			var num_photo = $(this).get(0).files.length;
			
			for (var i = 0; i < num_photo; ++i) {
				oFReader = new FileReader();
				var dummy = $(this);
				
				oFReader.onload = function (oFREvent) 
		        {
		            var dataURI = oFREvent.target.result;
		            
		            var elem = '<img class="story_thumb_img" src="'+dataURI+'" />';
		            
		            
		            $('#story_thumb_holder').append(elem);
		           
		        };
		    	oFReader.readAsDataURL(document.getElementById($(this).attr('id')).files[i]);
   
			  	
	
			 /*   var form_data = new FormData();                  
			    form_data.append('file', file_data);
			    form_data.append('sid', sid);
			          
				
			    
			                             
			    $.ajax({
			                url: rootUrl+"statics/story_image_upload/", // point to server-side PHP script 
			                dataType: 'text',  // what to expect back from the PHP script, if anything
			                cache: false,
			                contentType: false,
			                processData: false,
			                data: form_data,                         
			                type: 'post',
			                success: function(php_script_response){
			                
			                	var json = $.parseJSON(php_script_response);
			                	
			                     // display response from the PHP script, if any
			                    if(php_script_response != "FAIL")
			                    {
			                   		
			                   		
			                   		if(i == num_photo)
			                   		{
			                   			location.reload();		
			                   		}	                   
				                   
				                   
			                    }
			                    else
			                    {
				                    window.alert(upload_failed_text);
			                    }
			                    
			                    
			                   
			                    $('#loadingBG').hide();
						    	$('#loadingGif').hide();
								$("body").css("overflow", "visible");
			                }
			     });
			     
			     
			     
			     */
			}
	    	
    	}
    	
    	
	});

	
	
	
	
	
	
	// BLOG IMAGE
	$('#story_image_file').change(function()
	{
		if(!$('html').hasClass('no-filereader'))
		{
			var sid = $(this).attr("sid");	
			var type = "coverpictures";
			
			
			 /*LOADING OVERLAY SHOW*/
		    	$('#loadingBG').fadeIn();
		    	$('#loadingGif').fadeIn();
				$("body").css("overflow", "hidden");
			
		
			
			
			
			var num_photo = $(this).get(0).files.length;
			
			for (var i = 0; i < num_photo; ++i) {
				oFReader = new FileReader();
				var dummy = $(this);
				
				oFReader.onload = function (oFREvent) 
		        {
		            var dataURI = oFREvent.target.result;
		           
		        };
		    	oFReader.readAsDataURL(document.getElementById($(this).attr('id')).files[i]);
		    	
		    	
		    	
			    var file_data = $(this).prop('files')[i];   
	
			    var form_data = new FormData();                  
			    form_data.append('file', file_data);
			    form_data.append('sid', sid);
			          
				
			    
			                             
			    $.ajax({
			                url: rootUrl+"statics/story_image_upload/", // point to server-side PHP script 
			                dataType: 'text',  // what to expect back from the PHP script, if anything
			                cache: false,
			                contentType: false,
			                processData: false,
			                data: form_data,                         
			                type: 'post',
			                success: function(php_script_response){
			                
			                	var json = $.parseJSON(php_script_response);
			                	
			                     // display response from the PHP script, if any
			                    if(php_script_response != "FAIL")
			                    {
			                   		
			                   		
			                   		if(i == num_photo)
			                   		{
			                   			location.reload();		
			                   		}	                   
				                   
				                   
			                    }
			                    else
			                    {
				                    window.alert(upload_failed_text);
			                    }
			                    
			                    
			                    /* LOADING OVERLAY HIDE*/
			                    $('#loadingBG').hide();
						    	$('#loadingGif').hide();
								$("body").css("overflow", "visible");
			                }
			     });
			}
	    	
    	}
    	
    	
	});

	
	
	
	
	$('#post_traffic_alert').click(function(){
		$('#traffic_alert_holder').fadeIn();
	});
	
	$('#update_status').click(function(){
		$('#update_status_holder').fadeIn();
	});
	
	
	$('#message_owner').click(function(){
		$('#message_overlay').css({top: $(this).position().top}).show();
		scrollTo('message_overlay');
	});
	
	
	$('#send_message_to_owner').click(function(){
		var message = $("#send_message_input").val();
		var car_id = $(this).attr('cid');
		$.post(rootUrl+'statics/sendMessageOwner',{car_id:car_id, message: message},function(data){			
			$('.close_overlay').click();
		});
		
	});
	
	$('#newsfeed_post_send').click(function(){
		var text = $('#post_input').val();
		var type = $('input[name=input_radio]:checked').val();
		
		$(this).hide();
		
		 /*LOADING OVERLAY SHOW*/
    	$('#loadingBG').fadeIn();
    	$('#loadingGif').fadeIn();
		$("body").css("overflow", "hidden");
		
		
		if(type == "traffic")
		{
			var route = "add_traffic_alert";
		}
		else
		{
			var route = "add_status_update";
		}
		
		$.post(rootUrl+'statics/'+route, {text:text}, function(data){			
			if(data != "FALSE")
			{
				location.reload();
			}
		});
		
	});
	
	
	$('#newsfeed_suggestion_send').click(function(){
		var text = $('#suggestion_input').val();

		$(this).hide();
		
		 /*LOADING OVERLAY SHOW*/
    	$('#loadingBG').fadeIn();
    	$('#loadingGif').fadeIn();
		$("body").css("overflow", "hidden");
		
		
		
		
		$.post(rootUrl+'statics/sendSuggestion', {text:text}, function(data){			
			if(data != "FALSE")
			{
				$('#loadingBG').fadeOut();
		    	$('#loadingGif').fadeOut();
				$("body").css("overflow", "scroll");
				$('#newsfeed_suggestion_send').show();
				$('#suggestion_input').val(''); 
				window.alert('Thank you for your input!');
			}
		});
		
	});
	
/*	$('#add_alert_btn').click(function(){
		var text = $('#alert_input').val();
		
		$.post(rootUrl+'statics/add_traffic_alert', {text:text}, function(data){			
			if(data != "FALSE")
			{
				$('#traffic_alert_holder').hide();
				location.reload();
			}
		});
	});
	
	
	
	$('#add_status_btn').click(function(){
		var text = $('#status_input').val();
		
		$.post(rootUrl+'statics/add_status_update', {text:text}, function(data){			
			if(data != "FALSE")
			{
				$('#update_status_holder').hide();
				location.reload();
			}
		});
	});
	*/
	
	
	$('#vehicle_type_select').on('change', function(){
		$('#vehicle_make_select, #vehicle_model_select').hide();
		$('#vehicle_year_select').val(0);
		
		if($(this).val() == "motorcycle")
		{
			$('#vehicle_engine_select').val('gas').hide();
		}
		else
		{
			$('#vehicle_engine_select').val(0).show();
		}
	});
	
	
	$('#vehicle_year_select').on('change', function(){
	$('#vehicle_make_select, #vehicle_model_select').hide();
		var which = $('#vehicle_type_select').val();
		var year = $(this).val();
		
		$.post(rootUrl+'car/getMakeByYear', {year:year, type: which}, function(data){
			
			if(data != "FALSE")
			{
				$('#vehicle_make_select').empty().append(data).show();
			}
		});
	});
	
	
	$('#vehicle_make_select').on('change', function(){
		$('#vehicle_model_select').hide();
		var which = $('#vehicle_type_select').val();
		var year = $('#vehicle_year_select').val();
		var make = $(this).val();
		
		if(make != 'not')
		{
			$.post(rootUrl+'car/getModel', {year:year, type: which, make:make}, function(data){
				
				if(data != "FALSE")
				{
					$('#vehicle_model_select').empty().append(data).show();
				}
				
				
				
			});
		}
		
	});
	
	
	
	$('#add_new_car').click(function(){
		
		if($('#create_car_name').val() != "" && $('#vehicle_year_select').val() != 0 && $('#vehicle_make_select').val() != 0 && $('#vehicle_model_select').val() != 0 && $('#vehicle_engine_select').val() != 0)
		{
			$('#car_new_form').submit();
		}
		else
		{
			create_error_msg("Please fill out all the fields!");
		}
	});
	
	$('#follow_vehicle').click(function(){
		var cid = $(this).attr('cid');
		var $this = $(this);
		$.post( rootUrl+"car/followVehicle",{cid:cid}, function( data ) {
			  $this.fadeOut(function(){
				  $('#unfollow_vehicle').show();
			  });
			 
		});
	});
	
	
	$('.follow_circle').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		var cid = $(this).attr('vid');
		var refresh = $(this).attr('refresh');
		var $this = $(this);
		$.post( rootUrl+"car/followVehicle",{cid:cid}, function( data ) {
			  $this.parent().fadeOut();
			 
			 if(refresh == "yes")
			 {
				location.reload(); 
			 }
			 
		});
	});

	
	
	
	$('#unfollow_vehicle').click(function(){
		var cid = $(this).attr('cid');
		var $this = $(this);
		$.post( rootUrl+"car/unFollowVehicle",{cid:cid}, function( data ) {
			  $this.fadeOut(function(){
				  $('#follow_vehicle').show();
			  });
			 
		});
	});
	
	
	$('#add_comment_vehicle').click(function(){
		var cid = $(this).attr('cid');
		var comment = $('#vehicle_comment').val();
		var $this = $(this);
		$.post( rootUrl+"car/addComment",{cid:cid, comment:comment}, function( data ) {
			  location.reload();		 
		});
	});
	
	
	
	$( "#car_new_form" ).on( "submit", function( event ) {
	  event.preventDefault();

		$.ajax({
	        url: rootUrl+"car/create", // point to server-side PHP script 
	        data: $(this).serialize(),                         
	        type: 'post',
	        success: function(response){
			     
			       var resp = $.parseJSON(response);
			       
			       if(resp.success)
			       {
			       	
			       	 window.location.href = rootUrl+"edit_vehicle/"+resp.car_id;
			       }
			        
			       
			       create_error_msg(resp.message);       
			                	
			 }
	 	});

	});
	
	$('#add_story').click(function(){
		
		$('#add_story_overlay').css({'margin-top': '-150px'}).fadeIn();
		scrollTo('story_holder');
	});
	
	$('#add_story_edit').click(function(){
		
		$('#add_story_overlay').css({'margin-top': '-150px'}).fadeIn();
		
	});
	
	$('#add_modification').click(function(){
		$('#add_modification_overlay').css({top: $(this).offset().top}).fadeIn();
		scrollTo('add_modification');
	});
	
	
	$('.delete_mod').click(function(){
		$('#delete_mod_overlay').css({top: $(this).offset().top}).fadeIn();
		var mid = $(this).attr('mid');
		$('#delete_mod_yes').attr('mid', mid);
		scrollTo('delete_mod_overlay');
	});
	
	
	
	$('#cancel_story_piece, #cancel_mod_piece').click(function(){
		$(this).parent().parent().parent().fadeOut();
		$('#story_piece_text').val('');
	});
	
	$('#delete_mod_no').click(function(){
		$('#delete_mod_overlay').fadeOut();
		
	});
	
	$('.close_overlay').click(function(){
		$(this).parent().parent().fadeOut();
		$('#story_piece_text').val('');
	});
	
	$( "#add_story_piece" ).click( function( ) {
	 	
	 	$(this).text('Uploading...');
	 	var story = $('#story_piece_text').val();
		var cid = $(this).attr('cid');
		$(this).unbind('click');
		$.ajax({
	        url: rootUrl+"car/add_story_piece", // point to server-side PHP script 
	        data: {cid: cid, story: story},                         
	        type: 'post',
	        success: function(response){
	        
	        	  var sid = response;
	        	  var num_photo = $('#story_image_file_up').get(0).files.length;
				  
				  if(num_photo > 0)
				  {
						for (var i = 0; i < num_photo; ++i) {
							oFReader = new FileReader();
							var dummy = $(this);
							
							oFReader.onload = function (oFREvent) 
					        {
					            var dataURI = oFREvent.target.result;
					           
					        };
					    	oFReader.readAsDataURL(document.getElementById($('#story_image_file_up').attr('id')).files[i]);
					    	
					    	
					    	
						    var file_data = $('#story_image_file_up').prop('files')[i];   
				
						    var form_data = new FormData();                  
						    form_data.append('file', file_data);
						    form_data.append('sid', sid);
						          
							
						    
						                             
						    $.ajax({
						                url: rootUrl+"statics/story_image_upload/", // point to server-side PHP script 
						                dataType: 'text',  // what to expect back from the PHP script, if anything
						                cache: false,
						                contentType: false,
						                processData: false,
						                data: form_data,                         
						                type: 'post',
						                success: function(php_script_response){
						                
						                	var json = $.parseJSON(php_script_response);
						                	
						                     // display response from the PHP script, if any
						                    if(php_script_response != "FAIL")
						                    {
						                   		 
											      setTimeout(function(){
												      $('#login_error_message').fadeOut().text('');
											      }, 5000);
						                   		
						                   		if(i == num_photo)
						                   		{
						                   			location.reload();
						                   			$('.close_overlay').click();
											       $('#login_error_message').text('Story piece added');		
						                   		}	                   
							                   
							                   
						                    }
						                    else
						                    {
							                    window.alert(upload_failed_text);
						                    }
						                    
						                    
						                    /* LOADING OVERLAY HIDE*/
						                    $('#loadingBG').hide();
									    	$('#loadingGif').hide();
											$("body").css("overflow", "visible");
						                }
						     });
						}
	        
					}
					else
					{
						location.reload();
               			$('.close_overlay').click();
				        $('#login_error_message').text('Story piece added');
					}
	        
	        		
			     
			                	
			 }
	 	});

	});
	
}


function addAuthListeners()
{
	$('#fb_login').click(function(){
		loginUser("fb");
	});
	
	$('#fb_register').click(function(){
		loginUser("register");
	});
	
	/** LOGIN/REGISTRATION **/
	$('#register_teaser_button').click(function(){
	
		$('#login_holder').fadeOut(function(){
			$('#register').show();
		});
		
		
	});
	
	
	$('#register_back_btn').click(function(){
	
		$('#register').fadeOut(function(){
			$('#login_holder').show();
		});
		
		
	});
	
	$('#forgot_back_btn').click(function(){
	
		$('#forgot').fadeOut(function(){
			
		});
		
		
	});
	
	$('#register_country').on('change', function(){
		var val = $(this).val();
		
		if(val == "US")
		{
			$('#register_state').show();
			
		}
		else
		{
			$('#register_state').hide();
		}
	});
	
	
	$('#forgot_pw_text').click(function(){
	
		$('#register_teaser').hide();
		$('#register').hide();
		$('#forgot').fadeIn();
		
	});
	
	
	$("#profile_delete").click(function() {
		$('#profile_delete_confirm').fadeIn();
	});
	
	$("#profile_delete_no").click(function() {
		$(this).parent().fadeOut();
	});
	
	$("#profile_delete_yes").click(function() {
		var $this = $(this);
		$.post( rootUrl+"statics/delete_profile_request", function( data ) {		
			$this.parent().fadeOut();
			$this.parent().parent().fadeOut();
		});
	});
	
	
	$("#vehicle_delete").click(function() {
		$('#vehicle_delete_confirm').fadeIn();
	});
	
	$("#vehicle_delete_no").click(function() {
		$(this).parent().fadeOut();
	});
	
	$("#vehicle_delete_yes").click(function() {
		var cid = $(this).attr('cid');
	
		
		$.post( rootUrl+"statics/delete_vehicle", {cid: cid}, function( data ) {		
			
			window.location.href = rootUrl+"switch_vehicle";
		
		});
	});
	
	$("#club_delete_yes").click(function() {
		var iid = $(this).attr('iid');
	
		$.post( rootUrl+"statics/delete_club", {iid: iid}, function( data ) {		
			
			window.location.href = rootUrl;
		
		});
	});
	
	
	$("#event_delete_yes").click(function() {
		var eid = $(this).attr('eid');
	
		
		$.post( rootUrl+"statics/delete_user_event", {eid: eid}, function( data ) {		
			
			window.location.href = rootUrl+"my_events";
		
		});
	});
	
	$("#groupevent_delete_yes").click(function() {
		var eid = $(this).attr('eid');
	
		
		$.post( rootUrl+"statics/delete_group_event", {eid: eid}, function( data ) {		
			
			window.location.href = rootUrl+"my_events";
		
		});
	});
	
	$('.remove_image').click(function(){
		
		var $this = $(this);
		$this.fadeOut();							
		$this.parent().parent().find('.delete_editor').fadeIn();	
		
	});
	
	$('.photo_delete_no').click(function(){
		
		var $this = $(this);					
		$this.parent().parent().fadeOut();	
		$('.remove_image').fadeIn();

	});
	
	
	$('.photo_delete_yes').click(function(){
		var pic_id = $(this).attr('pid');
		var $this = $(this);
		$.post(rootUrl+'statics/delete_image', {pid:pic_id}, function(data) 
		{	
						
			$this.parent().parent().parent().fadeOut();	
		});
	});
	
	$('.title_hover').click(function(){
		$(this).hide();
		
		$(this).parent().parent().find('.title_editor').fadeIn();
	});
	
	$(".photo_title_save").click(function() {
		var photo_id = $(this).parent().attr('pid');
		var title = $(this).parent().find('.photo_title_input').val();
		var $this = $(this);
		$.post( rootUrl+"statics/update_photo_title",{ pid: photo_id, title: title}, function( data ) {		
			$this.parent().fadeOut();
			$this.parent().parent().bind('mouseenter', function(){
				$(this).find('.title_hover').show();
			});
		});
	});
	
	$(".like_image").click(function() {
		var pic_id = $(this).attr('pid');
		var $this = $(this);
		var like_count = $this.parent().find(".the_likes").text();
		$.post( rootUrl+"statics/addLike",{pid:pic_id}, function( data ) {		
			var newCount = parseInt(like_count)+1;
			$this.parent().find(".the_likes").text(newCount);
			$this.remove();
		});
	});
	
	$(".image_social_like").click(function() {
		var pic_id = $(this).attr('pid');
		var $this = $(this);
		var like_count = $this.parent().find(".news_likes_holder").text();
		$.post( rootUrl+"statics/addLike",{pid:pic_id}, function( data ) {		
			if(data == "OK")
			{
				var newCount = parseInt(like_count)+1;
				$this.parent().find(".news_likes_holder").text(newCount);							
			}	
			else
			{
				$this.effect("shake", {
			        direction: "left",
			        times: 4,
			        distance: 2
				});
			}

		});
	});
	
	
	$(".event_social_like").click(function() {
		var event_id = $(this).attr('eid');
		var $this = $(this);
		var like_count = $this.parent().find(".news_likes_holder").text();
		$.post( rootUrl+"statics/addEventLike",{eid:event_id}, function( data ) {		
			if(data == "OK")
			{
				var newCount = parseInt(like_count)+1;
				$this.parent().find(".news_likes_holder").text(newCount);							
			}	
			else
			{
				$this.effect("shake", {
			        direction: "left",
			        times: 4,
			        distance: 2
				});
			}

		});
	});
	
	
	
	$('.comment_image, .image_social_comment').click(function(){
			
		var pic_id = $(this).attr('pid');
		
		$('#send_comment').attr('pid', pic_id);
		
		$.post( rootUrl+"statics/get_photo_comments",{pid: pic_id}, function( data ) {
				
			$('#user_comments').empty();
			var json = $.parseJSON(data);
			
			$.each(json, function(i,item)
			{
				
				var deletebtn = "";
				if(user_id == item.uid)
				{
					deletebtn = '<div class="delete_comment_btn" cid="'+item.cid+'">X</div>'
				}
				 var elem = 	'<div class="comment_holder_item">'+
										'<img class="comment_profile_img" src="'+rootUrl+'items/uploads/profilepictures/'+item.profile_image+'">'+
										'<div class="comment_name dosissemi">'+item.nickname+'</div>'+
										'<div class="news_comment_text dosisreg">'+item.comment+'</div>'+
									'</div>';
							
				$('#user_comments').append(elem);
				addCommentDelete();
				$('#comment_overlay').show();
			});
			
		});
		

		
		
		
	/*	$('#comment_holder').attr('pic', pic_id);
		$('#comment_holder').fadeIn();
	*/	
	});
	
	
	$('.tag_overlay_close').click(function(){
		$(this).parent().fadeOut();
	});
	
	$('#add_comment_btn').click(function(){
			
		var pic_id = $('#comment_holder').attr('pic');
		var comment = $('#comment_photo_input').val();
		$.post( rootUrl+"statics/add_photo_comment",{pid: pic_id, comment: comment}, function( data ) {
				
			$('#user_comments').empty();
			var json = $.parseJSON(data);
			$.each(json, function(i,item)
			{
				
				var deletebtn = "";			
				if(user_id == item.user_id)
				{
					deletebtn = '<div class="delete_comment_btn" cid="'+item.id+'">X</div>'
				}
				var elem = '<div class="comment_photo_item" >'+
								'<div style="overflow:hidden;width:100%;height:20px;">'+
									'<div class="comment_user">'+item.first_name+' '+item.last_name+'</div>'+						
									'<div class="comment_date">'+item.created_date+'</div>'+
									deletebtn+
								'</div>'+
								'<div class="comment_text">'+item.comment+'</div>'+
							'</div>'+
							'<hr class="red_divider" style="border:0px;height:1px;background:#ffffff;width:50%;margin:10px 25%;">';
							
				$('#user_comments').append(elem);
			});
			
			$('#comment_photo_input').val("");
			addCommentDelete();
		});
		
		
		
	});
}



function loginUser(whichLogin) 
{    
	FB.login(function(response) 
	{
		//console.log(response);
		
	    if (response.status === 'connected' || response.status === 'not_authorized') 
		    checkUser(whichLogin, response);	    	

	}, {scope: 'email'});	     
}
	    
  
function checkUser(how, response) 
{
	
	FB.api('/me?fields=email,first_name,last_name', function(resp) 
	{
		
		golbalResponseObject = resp;
		user_fb_id = golbalResponseObject.id;
		fb_firstname = golbalResponseObject.first_name;
		fb_lastname = golbalResponseObject.last_name;
		fb_email = golbalResponseObject.email;
	   	
	   	
	   	if(how == "register")
			insertData(); 	
	    else
			fbLogin(response.authResponse.accessToken);			
	});
};




function insertData(){
	$("#register_email").val(fb_email);
	$("#register_firstname").val(fb_firstname);
	$("#register_lastname").val(fb_lastname);
	$("#fbIdHolder").val(user_fb_id);
	
	$('#fb_register').fadeOut();
}

function fbLogin(token)
{
	
	var remember = $('#login_rememberme').val();
	$.post(rootUrl+'Auth/loginUserFB', {fb:user_fb_id, email:fb_email, token: token, remember: remember}, function(data) 
	{
		var json = $.parseJSON(data);
		
		
		
		if(json.success)
		{
			window.location.href = rootUrl;
		}
		else
		{
			
			if(json.status != 'failed')
			{
				window.location.href = rootUrl+json.status;
			}
			else
			{
				$('#error').html(json.message);
			}	
		}
		
	});
}


function gallery_file_upload(){
	
	var album_id = $('#upload_picture').attr('album_id');
	var cid = $('#upload_picture').attr('cid');
	var addData = [
				    {
				        name: 'aid',
				        value: album_id
				    },
				    {
				        name: 'cid',
				        value: cid
				    }
				]
	
	
	var settings = {
	    url: rootUrl+"statics/add_photo",
	    method: "POST",
	    allowedTypes:"jpg,png,jpeg",
	    fileName: "myfile",
	    multiple: false,
	    formData: addData,
	    onSubmit:function(files)
		{
			$(".ajax-file-upload").hide();
			$('.ajax-upload-dragdrop').find('#dragdrop').text("Please wait...");
		},
	    onSuccess:function(files,data,xhr)
	    {
	    	
	        window.location.href = rootUrl+"edit_album/"+album_id;
	        $(".ajax-file-upload").show();
			$('.ajax-upload-dragdrop').find('#dragdrop').text("Drag & Drop files here");			

	    },
	    onError: function(files,status,errMsg)
	    {     
	    	
	        $(".ajax-file-upload").show();
	        $('.ajax-upload-dragdrop').find('#dragdrop').text("Drag & Drop files here");
	    }
	}
	
	$("#upload_photo").uploadFile(settings);
}


function newsfeed_file_upload(){
	
	var settings = {
	    url: rootUrl+"statics/add_photo_newsfeed",
	    dynamicFormData: function()
		{
		    var data ={"title":$('#newsfeed_img_title_holder').val()};
		    return data;        
		},
	    method: "POST",
	    allowedTypes:"jpg,png,jpeg",
	    fileName: "myfile",
	    multiple: false,
	    onSubmit:function(files)
		{
			$(".ajax-file-upload").hide();
			$('.ajax-upload-dragdrop').find('#dragdrop').text("Please wait...");
		},
	    onSuccess:function(files,data,xhr)
	    {
	    	
	        window.location.href = rootUrl;
	        $(".ajax-file-upload").show();
			$('.ajax-upload-dragdrop').find('#dragdrop').text("Drag & Drop files here");			

	    },
	    onError: function(files,status,errMsg)
	    {     
	    	
	        $(".ajax-file-upload").show();
	        $('.ajax-upload-dragdrop').find('#dragdrop').text("Drag & Drop files here");
	    }
	}
	
	$("#add_photo_btn_newsfeed").uploadFile(settings);
}


function album_file_upload(album_id){ 
	var settings = {
	    url: rootUrl+"statics/add_photo_album/"+album_id,
	    method: "POST",
	    allowedTypes:"jpg,png,jpeg",
	    fileName: "myfile",
	    multiple: false,
	    button: $(".button"), 
	    onSubmit:function(files)
		{
			$(".ajax-file-upload").hide();
			$('.ajax-upload-dragdrop').find('#dragdrop').text("Please wait...");
		},
	    onSuccess:function(files,data,xhr)
	    {
	    	
	       location.reload();
	        $(".ajax-file-upload").show();
			$('.ajax-upload-dragdrop').find('#dragdrop').text("Drag & Drop files here");			

	    },
	    onError: function(files,status,errMsg)
	    {     
	    	
	        $(".ajax-file-upload").show();
	        $('.ajax-upload-dragdrop').find('#dragdrop').text("Drag & Drop files here");
	    }
	}
	
	$("#add_photo_album_btn").uploadFile(settings);
}



function poi_file_upload(poi_id){
	var settings = {
	    url: rootUrl+"statics/add_photo_poi/"+poi_id,
	    method: "POST",
	    allowedTypes:"jpg,png,jpeg",
	    fileName: "myfile",
	    multiple: false,
	    onSubmit:function(files)
		{
			/*$(".ajax-file-upload").hide();
			$('.ajax-upload-dragdrop').find('#dragdrop').text("Please wait...");*/
		},
	    onSuccess:function(files,data,xhr)
	    {
	    	
	       // window.location.href = rootUrl+"poi/"+poi_id;
	     /*   $(".ajax-file-upload").show();
			$('.ajax-upload-dragdrop').find('#dragdrop').text("Drag & Drop files here");	*/		

	    },
	    onError: function(files,status,errMsg)
	    {     
	    	
	       /* $(".ajax-file-upload").show();
	        $('.ajax-upload-dragdrop').find('#dragdrop').text("Drag & Drop files here");*/
	    }
	}
	
	$("#add_photo_btn_poi").uploadFile(settings);
}



 function create_error_msg(message)
 {
 	$('#car_create_error_message').empty().append(message);
 }


function resetFormElement(e) {
  e.wrap('<form>').closest('form').get(0).reset();
  e.unwrap();
}


function tweetShare(component) {
	var thisCorrectUrl = rootUrl;
	var text = "";
	if(component == "TEXT")
	{
		var thisCorrectUrl = rootUrl;
		text = " I love Performance Nation! Register now to find vehicles in your neighbourhood!";
	}
	else if(component!="")
	{
		var thisCorrectUrl = rootUrl+component;
	}
	window.open(
	  'https://twitter.com/share?url='+encodeURIComponent(thisCorrectUrl)+'&text='+text,
      'facebook-share-dialog', 
      'width=626,height=436'); 
    return false;
}

function facebookShareImage(fname,title,pid) {
  
   if(title == " " || title == "")
   {
   	title = "Image from Performance Nation";
   }
   
   FB.ui({
	  method: 'feed',
	  link: rootUrl+"photo/"+pid,  // Go here if user click the picture
	  picture: fname,
	  caption: 'Performance Nation',
	  description: title,
	}, function(response){});
   
   
   
  	/*	var share=
		{
			method: 'stream.publish',  
			message: "",
			display: 'iframe',
			caption: rootUrl,
			name: title,			
			picture: rootUrl+"items/uploads/images/"+fname,    
			link: rootUrl+"photo/"+pid,  // Go here if user click the picture
			description: "See this image and vehicle on performancenation.com",
			actions: [{ name: 'performancenation.com', link: rootUrl+"photo/"+pid }],			
		}	
		FB.ui(share, onPostToWallCompleted);
	*/
}

function  onPostToWallCompleted(response)
{
	if (response && !response.error_code) {
     
    } else {
    	console.log(response);
    }
}


function selectize_poi_category()
{
	$('#poi_suggest_categories').selectize({
	    delimiter: ',',
	    
	});
}


function selectize_invite_search()
{
		
	$('#invite_vehicle_select').selectize({
	  maxItems: 100, //Max items selectable in the textbox
      maxOptions: 15, //Max options to render at once in the dropdown
      sortField: 'text',
      onType: function(value) {  
		if(value != "")
		{		
		//	$('#invite_vehicle_select').empty();
			var itemList = [];
			$.post(rootUrl+'statics/VehicleSearchByName', {search: value}, function(data)
			{
				
				var json = $.parseJSON(data);
				var selectize_tags = $("#invite_vehicle_select")[0].selectize;
				
				
				
				$.each(json, function(i,item)
				{
					var the_item = { text: item.name, value: item.id};
				    itemList.push(the_item);
			
				});
				
				//selectize_tags.clear();
				//selectize_tags.clearOptions();
				selectize_tags.load(function(callback) {
				    callback(itemList);
				});
			});
			
			
			
		}
      },
      onChange: function(value) {
       }
	});
}



function selectize_vehicle_search_make()
{
		
	$('#make_search').selectize({
	  maxItems: 1, //Max items selectable in the textbox
      maxOptions: 10, //Max options to render at once in the dropdown
      sortField: 'make',
      onType: function(value) {  
		if(value != "")
		{		
		//	$('#invite_vehicle_select').empty();
			var itemList = [];
			$.post(rootUrl+'statics/VehicleSearchByMake', {search: value}, function(data)
			{
				
				var json = $.parseJSON(data);
				
				var selectize_tags = $("#make_search")[0].selectize;
				
				
				
				$.each(json, function(i,item)
				{
					var the_item = { text: item, value: item};
				    itemList.push(the_item);
			
				});
				
				//selectize_tags.clear();
				//selectize_tags.clearOptions();
				selectize_tags.load(function(callback) {
				    callback(itemList);
				  
				});
			});
			
			
			
		}
      },
      onChange: function(value) {
       }
	});
}


function selectize_vehicle_search_model()
{
		
	$('#model_search').selectize({
	  maxItems: 1, //Max items selectable in the textbox
      maxOptions: 10, //Max options to render at once in the dropdown
      sortField: 'make',
      onType: function(value) {  
		if(value != "")
		{		
		//	$('#invite_vehicle_select').empty();
			var itemList = [];
			$.post(rootUrl+'statics/VehicleSearchByModel', {search: value}, function(data)
			{
				
				var json = $.parseJSON(data);
				
				var selectize_tags = $("#model_search")[0].selectize;
				
				
				
				$.each(json, function(i,item)
				{
					var the_item = { text: item, value: item};
				    itemList.push(the_item);
			
				});
				
				//selectize_tags.clear();
				//selectize_tags.clearOptions();
				selectize_tags.load(function(callback) {
				    callback(itemList);
				    
				});
			});
			
			
			
		}
      },
      onChange: function(value) {
       }
	});
}



/** MESSAGING **/

function selectize_user_search()
{
		
	$('#conversation_name_search').selectize({
	  maxItems: 1, //Max items selectable in the textbox
      maxOptions: 30, //Max options to render at once in the dropdown
      sortField: 'text',
      onType: function(value) {  
		if(value != "")
		{		
			$('#conversation_name_search').empty();
			var itemList = [];
			$.post(rootUrl+'statics/UserSearchByName', {search: value}, function(data)
			{
				
				var json = $.parseJSON(data);
				var selectize_tags = $("#conversation_name_search")[0].selectize;
				
				
				
				$.each(json, function(i,item)
				{
					var the_item = { text: item.name, value: item.id};
				    itemList.push(the_item);
			
				});
				
				selectize_tags.clear();
				selectize_tags.clearOptions();
				selectize_tags.load(function(callback) {
				    callback(itemList);
				});
			});
			
			
			
		}
      },
      onChange: function(value) {
        if (!value.length) return;
        	$('#send_new_message').attr("rec_id", value);
        	
        	if($('.conversation_item[pid="'+value+'"]').length)
        	{
	        	$('.conversation_item[pid="'+value+'"]').click();
        	}
        	
		}
	});
}






function send_message(rec_id,message)
{
	
	$.post(rootUrl+'statics/sendMessage',{rec_id:rec_id, message: message},function(data){			
		
	});	
}

function getMessagesFromUser(user_id)
{
	$('#messages_holder').empty();
//	$('#messages_loading').show();
	$.post(rootUrl+'statics/getMessagesFromUser',{pid:user_id},function(data){			
		$('#messages_loading').hide();
		var json = $.parseJSON(data);
		$.each(json, function(i, item)
		{
			if(item.own_message == true)
			{
				var elem = '<div class="messages_holder_item" mid="'+item.id+'">'+
							'<div class="message_image_holder">'+
								'<img style="width:100%" src="'+rootUrl+'items/uploads/profilepictures/'+item.profile+'"/>'+
							'</div>'+
							'<div class="message_date">'+item.date+'</div>'+
							'<div class="message_name">'+item.name+'</div>'+
							'<div class="message_text_sample">'+item.message+'</div>'+
				'</div>';
			}
			else
			{
				var elem = '<div class="messages_holder_item partner"  mid="'+item.id+'">'+
							'<div class="partner_message_image_holder">'+
								'<img style="width:100%" src="'+rootUrl+'items/uploads/profilepictures/'+item.profile+'"/>'+
							'</div>'+
							'<div class="partner_message_date">'+item.date+'</div>'+
							'<div class="partner_message_name">'+item.name+'</div>'+
							'<div class="partner_message_text_sample">'+item.message+'</div>'+
				'</div>';
			}
			
			$('#messages_holder').append(elem);
		});
	});
}

function updateSeenFlag(sid)
{
	$.post(rootUrl+'statics/updateMessageSeen', {sid: sid}, function(){
		
	});
}

function updateSeenFlagNotification(sid)
{
	$.post(rootUrl+'statics/updateNotificationSeen', {sid: sid}, function(){
		
	});
}

function messagingClicks()
{
	$('#send_new_message').click(function(){
		var rec_id = $(this).attr("rec_id");
		var message = $('#new_message_text').val();
		
		
		if(message != "" && message!= " ")
		{
			
			send_message(rec_id,message);
			$('.conversation_item[pid="'+rec_id+'"]').click();
			
			$('#new_message_text').val('');
		}
		
	});	
	
	$('.conversation_item').click(function(){
		$('.conversation_item').removeClass('active');
		$('.active_arrow').hide();
		
		var pid = $(this).attr("pid");
		$(this).addClass('active');
		$(this).find('.active_arrow').show();
		
		
		$('#send_new_message').attr('rec_id', pid);
		getMessagesFromUser(pid);
		updateSeenFlag(pid);
	});	
	
	
	$('#get_notification').click(function(){
		$(this).toggleClass('open');
		
		if($(this).hasClass('open'))
		{
			$.post(rootUrl+'statics/notifications', function(data){
				$('#notifications').empty().append(data);
				$('#notifications_holder').show();
				
				$('.notification_item').unbind('click');
				$('.notification_item').click(function(){
					
					var pid = $(this).attr("pid");				
					$(this).css({'background': '#ffffff'});
					
					updateSeenFlagNotification(pid);
				});
				
				
				
			});
		}
		else
		{
			$('#notifications_holder').hide();
		}
		
			
		
	});
	
	
	
}


function scrollTo(id){   
      // Scroll
    $('html,body').animate({
        scrollTop: $("#"+id).offset().top-250},
        'slow');
}


function selectize_make_search()
{
		
	$('#conversation_name_search').selectize({
	  maxItems: 1, //Max items selectable in the textbox
      maxOptions: 30, //Max options to render at once in the dropdown
      sortField: 'text',
      onType: function(value) {  
		if(value != "")
		{		
			$('#conversation_name_search').empty();
			var itemList = [];
			$.post(rootUrl+'statics/UserSearchByName', {search: value}, function(data)
			{
				
				var json = $.parseJSON(data);
				var selectize_tags = $("#conversation_name_search")[0].selectize;
				
				
				
				$.each(json, function(i,item)
				{
					var the_item = { text: item.name, value: item.id};
				    itemList.push(the_item);
			
				});
				
				selectize_tags.clear();
				selectize_tags.clearOptions();
				selectize_tags.load(function(callback) {
				    callback(itemList);
				});
			});
			
			
			
		}
      },
      onChange: function(value) {
        if (!value.length) return;
        	$('#send_new_message').attr("rec_id", value);
        	
        	if($('.conversation_item[pid="'+value+'"]').length)
        	{
	        	$('.conversation_item[pid="'+value+'"]').click();
        	}
        	
		}
	});
}
<script>	
	$('#sidemenu').css({top: 0});
</script>
			<div id="content">
				<div class="statics_subheader" style="margin-top:35px;">
					<?= $this->lang->line('dog_frenemies_friends_of') . ' ' . $dog->name.' ('.$friends_total.')';?>
				</div>
				
				<div id="dog_frenemies_friends" style="overflow:hidden;">
					<?php $x = 1; foreach ($friends->result() as $friend): 
						
						if(is_numeric($friend->profile_picture))
						{
							$dogprofile = $this->statics_model->getImageByID($friend->profile_picture)->row()->fname;
						}
						else
						{
							$dogprofile = $friend->profile_picture;
						}
						
					?>						
						<div class="my_friends_item" id="<?= $friend->id?>" <?if($x%3==0)echo "style='margin-right:0px;'"?> dog_name="<?= $friend->name?>">
							<div class="div_white_over"></div>
							<img class="friends_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $dogprofile)?>" border="0">						
							<a href="<?= site_url('dog/'.$friend->id);?>">
								<div class="friends_list_item_desc" style="top:35px;"><?= $friend->name?></div>	
							</a>									
							<div class="sansitalic" style="color:#ffffff;position:absolute;text-align:center;width:100%;top:60px;"><?= $this->lang->line('dog_frenemies_mutual_friends')." ".$mutualfriends[$friend->id];?></div>
							<div class="cancel_confirm_holder">
								<div class="confirm_text"><?= $this->lang->line('are_you_sure') ?></div>
								<div class="confirm_remove" dog_id="<?= $dog->id;?>" friend_id="<?= $friend->id?>"><?= $this->lang->line('yes') ?></div>
								<div class="cancel_remove"><?= $this->lang->line('no') ?></div>
							</div>
							<div class="cancel_friend" style="font-size:10px;"><?= $this->lang->line('dog_remove_friend') ?></div>
														
						</div>

					<?php $x++; endforeach; ?>
					
					
				</div>
				
<!--
				<div class="statics_subheader" style="margin-top:15px;">
					<?= $this->lang->line('dog_frenemies_enemies_of') . ' ' . $dog->name.' ('.$enemies_total.')';?>
				</div>
				
				<div id="dog_frenemies_enemies" style="overflow:hidden;">
					<?php $x = 1; foreach ($enemies->result() as $friend): ?>								
						
							<div class="my_friends_item" id="<?= $friend->id?>" <?if($x%3==0)echo "style='margin-right:0px;'"?> dog_name="<?= $friend->name?>">
								<div class="div_white_over"></div>
								<img class="friends_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $friend->profile_picture)?>" border="0">							
								<a href="<?= site_url('dog/'.$friend->id);?>">
									<div class="friends_list_item_desc" style="top:35px;"><?= $friend->name?></div>	
								</a>										
								<div class="sansitalic" style="color:#ffffff;position:absolute;text-align:center;width:100%;top:60px;"><?= $this->lang->line('dog_frenemies_mutual_enemies')." ".$mutualenemies[$friend->id];?></div>
								<div class="cancel_confirm_holder">
									<div class="confirm_text"><?= $this->lang->line('are_you_sure') ?></div>
									<div class="confirm_enemy_remove"  dog_id="<?= $dog->id;?>" enemy_id="<?= $friend->id?>"><?= $this->lang->line('yes') ?></div>
									<div class="cancel_remove"><?= $this->lang->line('no') ?></div>
								</div>
								<div class="cancel_enemy" ><?= $this->lang->line('dog_remove_enemy') ?></div>
								
							</div>
							
						
					
					<?php $x++; endforeach; ?>
				</div>
-->
			</div>	
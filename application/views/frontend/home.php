<div id="content" style="width:100%;left:0px;top:260px;background:#363636;margin-bottom:100px;">
	
	<div style="position:relative;z-index:100;padding-bottom:25px;overflow:hidden;">
		
		
		<div id="register" style="<? if(isset($message) && $message != "") echo 'display:block;'?>">
		<div class="button" id="register_back_btn" style="border:0;">BACK</div>
		<div style="font-size:17px;letter-spacing: 4px;" class="titil" style="letter-spacing: 4px;">////////////////////////////////////////////////// &nbsp;&nbsp;<span style="font-size:25px;" class="dosisbold">REGISTRATION</span>&nbsp;&nbsp; //////////////////////////////////////////////////</div>
		<div style="overflow:hidden;width:610px;margin:30px auto;">
			<div id="login_left">
				<form action="<?= site_url('Auth/register_user')?>" method="post" accept-charset="utf-8" id="">
						<input class="login_input" value="<?php if(isset($firstname_repop) && $firstname_repop != "") echo $firstname_repop;?>" placeholder="FIRST NAME*" type="text" name="register_firstname" id="register_firstname" /><br/>					
						<input class="login_input" value="<?php if(isset($lastname_repop) && $lastname_repop != "") echo $lastname_repop;?>" placeholder="LAST NAME*" type="text" name="register_lastname" id="register_lastname" /><br/>	
						<input class="login_input" value="<?php if(isset($email_repop) && $email_repop != "") echo $email_repop;?>" placeholder="E-MAIL*" type="text" name="register_email" id="register_email" /><br/>					
						<input class="login_input" value="<?php if(isset($nickname_repop) && $nickname_repop != "") echo $nickname_repop;?>" placeholder="NICKNAME*" type="text" name="register_nickname" id="register_nickname" /><br/>	
						<input class="login_input" placeholder="PASSWORD*" type="password" name="register_pw" id="reg_pw" /><br/>					
						<input class="login_input" placeholder="PASSWORD CONFIRMATION*" type="password" name="register_pw_conf" id="reg_pw_conf" /><br/>
						<!--<input class="login_input" placeholder="REGISTRATION CODE" type="text" name="register_code_conf" id="reg_code_conf" /><br/> --></br/>
						
					<?= $message;?>
			</div>
			<div id="login_right">			
				<div id="termsHolder">
					<select class="login_input"  placeholder="COUNTRY" name="register_country" id="register_country" style="width:255px;">
					
					<option value="US">United States</option>
					<? foreach($countrydata as $country):?>
					<option value="<?= $country->id;?>" <? if($country->id == $country_repop) echo 'selected="selected"';?>><?= $country->name;?></option>
					<? endforeach;?>
					</select>
					<br/>	
					<select style="width:255px;<? if(isset($country_repop) && $country_repop == 'US'){ echo 'display:block;';}else{echo 'display:none;';} ?>" class="login_input" placeholder="STATE" type="text" name="register_state" id="register_state" >
					<option value="0">Please select a state...</option>
					<? foreach($statedata as $state):?>
					<option value="<?= $state->abbreviation;?>" <? if($state->abbreviation == $state_repop) echo 'selected="selected"';?> ><?= $state->name;?></option>
					<? endforeach;?>
					</select>
					<br/>	
					<input class="login_input" value="<?php if(isset($zip_repop) && $zip_repop != "") echo $zip_repop;?>" placeholder="ZIP CODE*" type="text" name="register_zip" id="register_zip" /><br/>	
					<input class="login_input" value="<?php if(isset($city_repop) && $city_repop != "") echo $city_repop;?>" placeholder="CITY" type="text" name="register_city" id="register_city" /><br/>	
					<input class="login_input" value="<?php if(isset($address_repop) && $address_repop != "") echo $address_repop;?>" placeholder="ADDRESS" type="text" name="register_address" id="register_address" /><br/>	
					<input type="checkbox" id="terms" name="register_terms" value="1" style="margin-right:10px;"><a target="_blank" style="" href="<?= site_url('terms')?>">I accept the Terms & Conditions</a></input>
					<input type="hidden" id="fbIdHolder" name="fbIdHolder" value="0" />
				</div>	
				<div id="fb_register">REGISTER WITH <span class="dosisbold">FACEBOOK</span></div>
				
				<input type="submit" value="REGISTER" class="button regular" style="margin-top:0px;line-height:25px;" />	
				
			</div>
			</form>
		</div>
	
	
		<div style="font-size:17px;letter-spacing: 4px;" class="titil" style="letter-spacing: 4px;">//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////</div>
	
	</div>
		
		
		
		
		<div id="start_left">
			<div id="start">START YOUR</div>
			<div id="engines" class="playbold">ENGINES!</div>
			<br/><br/>
			<div id="description">	
	<span class="bold" style="font-size:18px;text-transform:uppercase;letter-spacing:1px;">Welcome to the underground world of – Performancenation.com</span>
	<br/><br/><br/>
	PerformanceNation is an invitation only social network enabling its members to find like-minded individuals and their custom builds in a specific geographical area. We give you all the tools you need to develop your own local club, find members, and create your events.<br/> Work your way from a Rookie all the way up to a Legend and become an Ambassador in your area.
			</div>
		</div>
		<div id="start_right">
			<div id="ready" class="playbold">READY, SET, GO!</div>
			<div id="description">Apply for a membership or register with your invitation code.</div>
			
			<div id="register_teaser_button" class="button">REGISTER NOW</div>
			
			
			<div id="description" class="titit" style="margin-bottom:-20px;padding:0px;letter-spacing:4px;">///////////// LOG IN //////////////</div>
			<div id="login_holder" style="<? if(isset($message) && $message != "") echo 'display:none;'?>">
				<div style="overflow:hidden;width:100%;margin:0px auto;">
					
						<div id="form" style="width:255px;margin:0px auto;">
							<form action="<?= site_url('Auth/login')?>" method="post" accept-charset="utf-8" id="">
								<div class="" style="font-size:16px;color:#ffffff;"></div>
								<input class="login_input" placeholder="E-MAIL" type="text" name="login_email" id="login_email" /><br/>					
								<input class="login_input" placeholder="PASSWORD" type="password" name="login_pw" id="login_pw" />								
								<div id="forgot_pw_text">Forgot your password?</div>
								<input type="submit" value="LOG IN" class="button regular" style="margin-top:10px;line-height:25px;" />
								<div id="error"><?= $message_login;?></div>
								<div style="width:35px;margin:0px auto;text-align:center;">- or -</div>
								<div id="fb_login">LOG IN WITH <span class="dosisbold">FACEBOOK</span></div>
								<div style="float:right;"><label for="save_login">Keep me logged in</label><input type="checkbox" value="1" name="save_login" id="save_login"></div>
							</form>
						</div>
						<br/>
						
					
					
							
						<div id="forgot" style="<?if(isset($message_pw) && $message_pw != "") echo 'display:block;'?>">
								
							<form action="<?= site_url('Auth/forgot_password')?>" method="post" accept-charset="utf-8" id="" style="width:255px;margin:40px auto;">
							      <div style="margin-left:0px;text-align:left;">
							      		<?if(isset($message_pw) && $message_pw != "")
										{
											echo $message_pw;
										}
										else
										{?>
										<div class="dosisbold" style="font-size:20px">PASSWORD RECOVERY</div>
							      		<div for="email" class="regular" style="margin:5px 0px">Enter your E-mail address to receive details on how to reset your password:</div>
										<? }?>
							      		 
							      		<input type="text" name="email" placeholder="E-MAIL" value="" id="email" class="login_input">	      
								  		<input type="submit" class="button regular" action="login" source="login" value="SUBMIT" style="line-height:25px;">
							     </div>					
							</form>
							
							<div class="button" id="forgot_back_btn" style="border:0;">BACK</div>
						</div>
						
								
						
					
				</div>

			</div>
			
			
			
			
			
			
			
			
			
		</div>
		
		
		<img id="skitz" src="<?= site_url('items/frontend/img/car_skitz.png')?>"/>
		
		
		
		
		
		
		
	</div>
</div>

	




	
	
	
	


     
		
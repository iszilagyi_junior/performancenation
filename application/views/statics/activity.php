<script>	
	$('#sidemenu').css({top: 35});
</script>
<div id="content">
	
	<? if($user_id == $created_by){?>
		<div id="friends_holder">
			<div id="close_overlay">X</div>
			<div class="statics_subheader" style="height:30px;line-height:30px;background:none;color:#d4003d;">
				<?= $this->lang->line('dog_friends')?>		
			</div>
			<?
			if($friends){
				foreach($friends as $friend){?>								
					<div class="friends_item_activity" activity_id="<?= $activity_id;?>" dogId="<?= $friend['id'];?>">											
						<div class="div_white_over"></div>
						<img class="friends_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $friend['picture'])?>" border="0">						
						<div class="friends_list_item_desc">
							<?= $friend['name'];?>
						</div>
					</div>
					
					
			<? }
			}?>
		</div> 
	<? }?>
	
	<a href="<?= site_url('activities');?>">
		<div id="edit_activity_button" style="right:700px;width:80px;top:35px;"><?= $this->lang->line('user_album_back_button')?></div>
	</a>
	<? if($user_id == $created_by){?>
		<a href="<?= site_url('edit_activity/'.$activity_id);?>">
			<div id="edit_activity_button" style="top:35px;"><?= $this->lang->line('edit_button')?></div>
		</a>
	<? }?>
	<div class="statics_subheader" style="margin-top:35px;">
		<?= $place;?>
	</div>
	<br/>
	<div id="activity_info_holder">
		<div class="activityAbout" ><?= $this->lang->line('about')?></div>
		<div class="activityDate"><?= $this->lang->line('new_activity_form_date')?></div>
		<br style="clear:both;">
			<div class="activity_date_holder"><?= $date;?> <br/> <?= $time;?></div>
			<div class="activity_description_holder"><?= $about;?></div>
	</div>
	<div style="position:relative">
		<div class="statics_subheader" style="margin-top:35px;">
			<?= $this->lang->line('activity_guests')?>		
		</div>
		<? if($user_id == $created_by){?>
			<div id="add_guests_btn" style="position: absolute;top:21px;">
				<div id="show_friends" style="top:-5px;" class="album_arrow_text"><?= $this->lang->line('add_activity_guest')?></div>
			</div> 
		<? }?>
	</div>
	
	<div id="guests_holder">
	<?
		if($accepted_count == 0)
		{?>
			<?= $this->lang->line('no_accepted_invites')?>
		<?}
		else
		{
			if($guests){ $x = 1;
				foreach($guests as $guest){
					if($guest['pending'] == 0)
					{
					?>
						<a href="<?= site_url('dog/'.$guest['id']);?>">
							<div class="nearby_list_item" <? if($x%3 == 0){ echo "style='margin-right:0px'";}?> >
								<img class="vets_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $guest['picture'])?>" border="0">
								<div class="div_white_over"></div>
								<div class="zones_list_item_desc" style="top:90px;">
									<?= $guest['name'];?>
								</div>
							</div>			
					 </a>
				 <? }
			$x++; }
			}
		}?>
	</div>	
	<br>
	<div class="statics_subheader" style="margin-top:5px;">
		<?= $this->lang->line('activity_pending')?>		
	</div>
	<div id="pending_holder">
	<?
		if($pending_count == 0)
		{?>
			<?= $this->lang->line('no_pending_invites')?>
		<?}
		else
		{
			if($guests){ $x = 1;
				foreach($guests as $guest){
							if($guest['pending'] == 1)
							{
						?>
							<a href="<?= site_url('dog/'.$guest['id']);?>">
								<div class="nearby_list_item" <? if($x%3 == 0){ echo "style='margin-right:0px'";}?> >
									<img class="vets_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $guest['picture'])?>" border="0">
									<div class="div_white_over"></div>
									<div class="zones_list_item_desc" style="top:90px;">
										<?= $guest['name'];?>
									</div>
								</div>			
						 </a>
					<? }
			$x++; }
			}
		}?>
	</div>	
</div>
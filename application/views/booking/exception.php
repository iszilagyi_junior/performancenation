

		<div class="exception_overlay calendar_overlay">
			<img class="overlay_arrow" src="<?= site_url('items/frontend/img/popup_arrow.png')?>"/>
			<div class="overlay_close">X</div>
			<div class="popup_title_text">edit exception<br/>
				<span style="color:#b4b4b4;font-size:13px;"><?= $weekday ?>, <?= date("F j, Y", strtotime($the_date));?></span>
			</div>
			<form id="exception_overlay_form">
				<input type="hidden" name="exception_overlay_date" id="exception_overlay_date" value="<?= $date?>">
				<input type="hidden" name="exception_overlay_exception_id" id="exception_overlay_exception_id" value="<?= $exception_id?>">
				
				<div class="dognum_holder" style="text-align:left;">
					<label for="fe1" class="popup_text" style="margin-left:60px;">Exception reason:</label>
					<input type="text" name="exception_overlay_reason" value="<?= $reason?>" id="fe1" class="popup_input" style="width:199px;margin-left:60px;">
					<br>
					<input type="submit" name="exception_overlay_save" value="Edit exception" class="popup_button" style="top:60px;left:60px;width:203px;">
				</div>
				<div class="popup_delete_holder" style="top:190px;">
					<hr class="popup_line"><span class="popup_text" style="float:left;margin:0px 20px;">or</span><hr class="popup_line">
					<input type="button" id="exception_overlay_clear" value="delete exception" class="popup_button" style="top:30px;left:-16px;width:203px;">
				</div>
			</form>
		</div>
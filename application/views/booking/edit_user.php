
		
		<div class="availedit_overlay calendar_overlay" style="height:380px;">
			<img class="overlay_arrow" src="<?= site_url('items/frontend/img/popup_arrow.png')?>"/>
			<div class="overlay_close">X</div>
			<div class="popup_title_text">edit availability<br/>
				<span style="color:#b4b4b4;font-size:13px;"><?= $weekday ?>, <?= date("F j, Y", strtotime($the_date));?></span>
			</div>
			<form id="availedit_overlay_form">
				<input type="hidden" name="availedit_overlay_date" value="<?= $date?>">
				<input type="hidden" name="availedit_overlay_avail_id" id="availedit_overlay_avail_id" value="<?= $avail_id?>">
				<div class="dognum_holder">
					<label class="light_popup_text" for="fe1">number of dogs</label>
					<input type="text" name="availedit_overlay_dogcount" value="<?= $dog_maxcount?>" id="fe1" class="popup_input">
					<input type="submit" name="availedit_overlay_save" value="Edit" class="popup_button" style="top:40px;left:60px;width:203px;">
				</div>

				<div class="popup_exception_holder">
					<label class="popup_text" for="fe2" style="margin-left:83px;">Add exception for this day:</label>
					<input type="text" placeholder="Exception reason" name="availedit_overlay_exceptionreason" value="" id="fe2" class="popup_input" style="width:199px;margin-left:60px;">
					<br>
					<input type="button" id="availedit_overlay_exception" value="add exception" class="popup_button" style="top:60px;left:60px;width:203px;">
				</div>
				
				<div class="popup_delete_holder">
					<hr class="popup_line"><span class="popup_text" style="float:left;margin:0px 20px;">or</span><hr class="popup_line">
					<input type="button" id="availedit_overlay_clear" value="delete availability" class="popup_button" style="top:30px;left:-16px;width:203px;">
				</div>
			</form>
		</div>
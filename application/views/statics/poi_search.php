<script>
	var pois = <?= $pois_json;?>;
	$(document).ready(function()
	{	
		initMapPOI(pois);
		initAutocomplete(false, 'poi_address_search');
		
		initAutocomplete(false, 'poi_address');
		
		selectize_poi_category();
	});
	
</script>


<div id="content" style="width:800px;margin-bottom:100px;">

	<div id="poi_suggestion_overlay">
		<div class="overlayBG"></div>
		<div style="position:relative;z-index:100">
			<img class="close_overlay" style="top:0px" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />
			
			<div class="titboldit" style="font-size:27px;margin-top:30px;">SUGGEST POI</div>
			<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">
				<table style="width:400px;margin:30px auto;">
					<tr>
						<td style="regular">
							Name
						</td>
						<td>
							<input type="text" placeholder="Name" class="edit_input" name="poi_name" id="poi_name"></input>
						</td>
					</tr> 
					
					<tr>
						<td style="regular">
							Address
						</td>
						<td>
							<input type="text" onFocus="geolocate()" placeholder="Address" class="edit_input" name="poi_address" id="poi_address"></input>
						</td>
					</tr> 
					
					<tr>
						<td style="regular">
							Description
						</td>
						<td>
							<textarea  placeholder="Description" class="edit_input" name="poi_description" id="poi_description" style="resize:none;height:60px;"></textarea>
						</td>
					</tr>
					
					<tr>
						<td style="regular">
							Category
						</td>
						<td>
							<select multiple id="poi_suggest_categories" class="edit_input" style="width:256px;padding-left:10px;margin-left:27px;">
								<option value="0"></option>
								<? foreach($categories as $category):?>
									<option value="<?= $category->id;?>"><?= $category->name;?></option>
								<? endforeach;?>
							</select>
						</td>
					</tr>
					
					
					
					
					
				</table>
			</div>
			<div class="button" id="create_poi_button" >SEND</div>
		
			<div class="regular" id="poi_error_message"></div>
		</div>
	</div>
	
	<img class="" style="position:absolute;left:0px;top:0px;" src="<?= site_url('items/frontend/img/big_chrome.png')?>" border="0">
	<div class="playbold gold page_title" style="text-align:center;font-size:40px;letter-spacing:1px;z-index:10;position:relative;top:10px;">PLACES OF INTEREST</div>

	<div id="poi_search_options">
		<div class="regular"></div>
		
		<div style="overflow:visible;margin-top:5px;">
			<select id="poi_categories" class="edit_input" style="width:250px;padding-left:10px;margin-bottom: 0px;">
				<option value="0">Show all categories</option>
				<? foreach($categories as $category):?>
					<option value="<?= $category->id;?>"><?= $category->name;?></option>
				<? endforeach;?>
				
			</select>
			
			<input style="padding-left:10px;width:240px;" type="text" placeholder="Search Name" class="edit_input" id="poi_text_search" name="poi_text_search"></input>
			<input style="padding-left:10px;width:240px;" type="text" placeholder="Search Address" onFocus="geolocate()" class="edit_input" id="poi_address_search" name="poi_address_search"></input>
			<br/>
			<select id="poi_ratings" class="edit_input" style="width:250px;padding-left:10px;margin-left:10px;float:left;margin-top:10px;">
				<option value="0">Show all ratings</option>
					<option value="2">2+ Stars</option>
					<option value="3">3+ Stars</option>	
					<option value="4">4+ Stars</option>								
			</select>
			<div class="button" id="poi_start_text_search" style="float:left;margin-left:12px;margin-top:10px;">SEARCH</div>
			
			<div id="suggest_poi" class="button" style="position:absolute;left:10px;top:100px;width:120px;">SUGGEST POI</div>
			<div id="switch_poi_list" class="button" style="position:Absolute;right:10px;top:100px;width:120px;">LIST VIEW</div>
			<div id="switch_poi_map" class="button" style="position:Absolute;right:10px;top:100px;width:120px;display:none;">MAP VIEW</div>
		</div>
		<div class="button" id="poi_locate_me">CENTER MAP ON ME</div>
	</div>
	
	<div id="poi_map_holder"></div>
	<div id="resultHolder" style="display:none;margin-top:220px;">
		<div class="titit" style="text-align:center;margin:20px 0px;color:#ffffff;font-size:18px;margin-top:0px;">Results</div>
		
		<? foreach($pois as $poi):?>
			<a style="text-decoration:none;" href="<?= site_url('poi/'.$poi['id'])?>">
				<div class="poi_item regular" pid="<?= $poi['id']?>">
					<span class="playbold" style="font-size:16px;margin-top:20px;"><?= $poi['name']?></span><br/>
					<?= $poi['category']?><br/>
					<?= $poi['address']?><br/>
					<?= $poi['rating']?>
				</div>
			</a>
			
			
		<? endforeach;?>
	</div>
	
	
	
</div>
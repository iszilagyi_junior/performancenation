<script>	
	
	
	$(document).ready(function()
	{	
		newsfeed_file_upload();
	});
	
	
</script>

<div id="content" style="background:none;">


			<div class="statics_subheader" style="">
				<div class="dosisextralight" style="font-size:30px;">Follow others to see their stories and pictures.</div>
				<div class="dosissemi" style="font-size:21px;">HERE ARE SOME SUGGESTIONS:</div>
			</div>
			
			
			
			<div style="width: 100%;height:278px;margin-top:20px;overflow:hidden;">
				<?	 $x = 1; foreach($random_vehicles_news as $vehicle): 
					
					if($x <= 4){?>
		
						<a href="<?= site_url('vehicle_profile/'.$vehicle['pretty_url']);?>">
							<div class="news_random_friend_item" id="<?= $vehicle['id']?>" style="<?if($x == 4) echo 'margin-right:0px;'?>" dog_name="<?= $vehicle['nickname']?>">
								<div class="div_white_over"></div>
								<img class="news_random_friend_image" src="<?= site_url('items/uploads/profilepictures/' . $vehicle['profile_picture'])?>" border="0" />
								<div class="news_random_friend_desc dosisbold" style=""><?= $vehicle['nickname'];?></div>	
								
								<div class="news_random_friend_add regular" style="">VIEW</div>		
								
							</div>
						</a>
					<? }?>
				<?php $x++; endforeach; ?>
				
			</div>
			
			<div id="newsfeed_actions_holder">
				<div class="titil" style="font-size:17px;letter-spacing:4px;"> ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////</div>
				<div id="notIE9" style="position:absolute;left:410px;top:47px;">
					<input type="text" name="newsfeed_img_title_holder" id="newsfeed_img_title_holder" placeholder="Image title" style=""></input><br/>
					<div id="add_photo_btn_newsfeed" class="newsfeed_action_button" style="position: absolute;left: 0px;top: 4px;width: 146px;height: 0px;line-height: 0px;">UPLOAD A PICTURE</div>
					<input type="file" preview="add_photo_btn_newsfeed" accept="image/*" id="add_photo_upload_btn" name="add_photo_upload_btn" style="display:none;">
				</div>
				<div id="IE9" style="display:none;">
					<div class="newsfeed_action_button">UPLOAD A PICTURE</div>
					<?php 
					$data = array('id' => 'add_photo_ie');
					echo form_open_multipart("statics/add_photo_newsfeed_ie", $data);?>
						<input type="file" preview="add_photo_btn_newsfeed" accept="image/*" id="add_photo_upload_btn_ie" name="myfile" style="">
					<? echo form_close();?>
				</div>
				<div id="newsfeed_post_holder">
					<div class="dosissemi" style="width:100%;text-align:center;margin-bottom:5px;font-size:17px;">POST</div>
					<textarea id="post_input" name="post_input" placeholder="Write here..."></textarea><br/>
					<div class="button" id="newsfeed_post_send" style="width: 80px;margin: 0px;float: right;">SEND</div>
					
					<div style="margin-top:10px;">
						<label><input type="radio" name="input_radio" value="traffic" checked="checked">Traffic alert</input></label>
						<label><input type="radio" name="input_radio" value="status" >Status update</input></label>
					</div>
					
				</div>

				<a href="<?= site_url('vehicle_search')?>">
					<div class="newsfeed_action_button" style="position:absolute;right:0px;top:68px;width:145px;">SEARCH</div>
				</a>
				
				<div id="newsfeed_suggestion_holder">
					<div class="dosissemi" style="width:100%;text-align:left;margin-bottom:5px;font-size:17px;">Send us your suggestion:</div>
					<textarea id="suggestion_input" name="suggestion_input" placeholder="Write here..."></textarea><br/>
					<div class="button" id="newsfeed_suggestion_send" style="width: 80px;margin-top: -46px;float: right;">SEND</div>
				</div>
				<div class="titil" style="font-size:17px;letter-spacing:4px;"> ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////</div>
			</div>
			
			
			
			<div id="advertisement_holder">
					<?  if($advertisement!= false){?>
					 <div class="advertisement_item">
					 	
						 <a target="_blank" href="<?= $advertisement_link;?>">
							 <img class="ad_img" src="<?= site_url('items/uploads/partnerimages/'.$advertisement->image_fname)?>" />
						 </a>
						 
					 </div> 
				<? }?>		
			</div>
			
			
			<div id="whatsnew_text"><?= $this->lang->line('home_most_recent')?></div>				
			
			<? 
				
				  if(empty($whatsnew)){?>
					 <div style="margin-left:20px;">Your newsfeed is empty! Start following others to see their activities!</div> 
				<? }?>
			<div id="eventHolder">
			
				
				
				
				<? if(!empty($whatsnew)){?>
				<?
				$x = -4;
				$k = 0;
				$j = 0;
				
				$small_counter = 1;
					 foreach($whatsnew as $i=>$new):
						if($user_hashtags == 1)
						{
							
							if($x%5 == 0 ){
								
								?>
								
								<div class="instagram_item event_item_sortable">
									
									<a style="text-decoration:none;" target="_blank"  href="<?= $hashtags[$k]['link']; ?>">
										<div class="eventFade" style="left:0px;top:0px;"></div>		
										<div class="eventText"  style="left:0px;top:0px;"><?= $this->lang->line('news_photo_instagram');?></div>
										<div class="taggedHolder">
											<img class="taggedPicture" src="<?= $hashtags[$k]['img_url']; ?>" >
										</div>
										<div class="instagram_item_details">
											<div class="instagram_user"><?= $hashtags[$k]['username']; ?></div>
											<div class="instagram_upload_date"><?= $hashtags[$k]['upload_date']; ?></div>
											<div class="instagram_text"><?= $hashtags[$k]['tags']; ?></div>
										</div>
									</a>
									
									
								</div>
								
							<? $k++; 
							}
						}
					?>
						<? if($new['event_type'] == "story_piece" || $new['event_type'] == "traffic_alert" || $new['event_type'] == "status_update")
							{  ?>
							
							<!-- FULL WIDTH NO IMAGE -->
							<div class="event_item_small event_item_sortable" style="">
							
								
								<div class="event_content">
									<img class="event_img" src="<?= $new['profile'];?>">
									<div class="event_text_holder" style="bottom:25px;">
										<a style="text-decoration:none;" href="<?= site_url('vehicle_profile/'.$new['pretty_url']);?>"><span class="event_name white"><?= $new['vehiclename'];?></span><br/> <?= $new['text']?> 
										
												<br/>
												 <div class="event_time white"><?= $new['created_date'];?></div> 
											</div>
											<div class="event_desc"><?= $new['content'];?></div>
										</a>
									<? if($new['event_type'] == "traffic_alert" || $new['event_type'] == "status_update" || $new['event_type'] == "story_piece"){?>		
											<div class="image_share_holder" style="">
												<img style="width:30px;position:absolute;top:0px;right:165px;" class="image_social_icon event_social_like" eid="<?= $new['event_id']; ?>" pid="<?= $new['image_id']; ?>" src="<?= site_url('items/frontend/img/like_sq.png');?>"/>	
											    <div class="news_likes_holder" ><?= $new['likes'];?></div>
												<div class="button news_comment_button" cc="<?= count($new['comments']);?>">COMMENT <? if(count($new['comments']) > 0) echo "(".count($new['comments']).")";?></div>
											</div>
									<? 	}	?>	
							</div>
							
							<!-- COMMENT CONTENT -->
								<div class="comment_content">
									<div class="dosissemi" style="width:340px;margin-left:25px;margin-top:25px;">COMMENTS</div>
									<div class="comments">
										<? foreach($new['comments'] as $comment):?>
											<div class="comment_holder_item">
												<img class="comment_profile_img" src="<?= site_url('items/uploads/profilepictures/'.$comment['profile_image'])?>">
												<div class="comment_name dosissemi"><?= $comment['nickname']?></div>
												<div class="news_comment_text dosisreg"><?= $comment['comment']?></div>
											</div>
										<? endforeach;?>
									</div>
									<textarea class="comment_input" eid="<?= $new['event_id'] ?>" placeholder="Write your message here..." name="comment_input"></textarea>
									<div class="comment_send">Press ENTER to send the message!</div>
								</div>
							
							
							
							<a style="text-decoration:none;" href="<?= site_url('vehicle_profile/'.$new['pretty_url']);?>">
								<div class="newsfeed_profile_img_holder" style="background-image: url('<?= $new['profile']?>');background-size: cover;">
								
								</div>
								
							</a>
						</div>
									
						<?	}
							else
							{?>
							
							<!-- SMALL NEWSFEED ITEM -->

							<div class="event_item_small event_item_sortable" >
							
								<? $small_counter++;?>
								
								
								<!-- EVENT CONTENT -->
								<div class="event_content">
									<a style="text-decoration:none;" href="<?= site_url('vehicle_profile/'.$new['pretty_url']);?>">
										<img class="event_img" src="<?= $new['profile'];?>">
										<div class="event_text_holder">
												<span class="event_name " style="color:#ffffff"><?= $new['vehiclename'];?></span></a><br/> <?= $new['text']?>
											<a style="text-decoration:none;" href="<?= site_url($new['entity_link']);?>"><span class="event_name" style="color:#ffffff;font-size:15px;"><?= $new['entity_name'];?></span></a>
											 <div class="event_time"><?= $new['created_date'];?></div> 
										</div>	
										<div class="eventFade"></div>
										<? if($new['event_type'] == "new_modification"): ?>
											<div class="event_desc"><?= $new['content'];?></div>
										<? endif;?>							
										<? if($new['event_image'] != ""){ ?>
											
											<? if($new['event_type'] != "new_modification"): ?>		
												<a href="<?= $new['event_image'];?>" eid="<?= $new['event_id'];?>" pic_id="<?= $new['image_id'];?>" data-lightbox="image_<?= $new['event_image'];?>">
											<? endif;?>			
														<div class="eventText" style="top:30px;"><?= $new['event_text']?></div>
														<div class="event_image_holder" style="background-image: url('<?= $new['event_image']?>');"></div>
											<? if($new['event_type'] != "new_modification"): ?>		
												</a>
											<? endif;?>	
												
										<? if($new['event_type'] == "new_photo" || $new['event_type'] == "new_cover" || $new['event_type'] == "new_profile"){?>		
											<div class="image_share_holder" style="">
												<img style="float:left;margin-right:15px;" class="image_share_icon share_image_tw" pid="<?= $new['image_id']; ?>" src="<?= site_url('items/frontend/img/twitter_sq.png');?>"/>
												<img style="float:left;" class="image_share_icon share_image_fb" pid="<?= $new['image_id']; ?>"  fname="<?= $new['event_image']; ?>" title="" src="<?= site_url('items/frontend/img/fb_sq.png');?>"/>
												<img style="width:30px;position:absolute;top:0px;right:165px;" class="image_social_icon event_social_like" eid="<?= $new['event_id']; ?>" pid="<?= $new['image_id']; ?>" src="<?= site_url('items/frontend/img/like_sq.png');?>"/>	
											    <div class="news_likes_holder" ><?= $new['likes'];?></div>
												<div class="button news_comment_button" cc="<?= count($new['comments']);?>">COMMENT <? if(count($new['comments']) > 0) echo "(".count($new['comments']).")";?></div>						
											</div>
											
											
									<? 	}
									else 
										{ ?>
											<div class="event_desc"></div>
											<div class="image_share_holder" style="">
												<img style="width:30px;position:absolute;top:0px;right:165px;" class="image_social_icon event_social_like" eid="<?= $new['event_id']; ?>" pid="<?= $new['image_id']; ?>" src="<?= site_url('items/frontend/img/like_sq.png');?>"/>	
												 <div class="news_likes_holder" ><?= $new['likes'];?></div>
												<div class="button news_comment_button" cc="<?= count($new['comments']);?>">COMMENT <? if(count($new['comments']) > 0) echo "(".count($new['comments']).")";?></div>						
											</div>
									<?	}
									}
									else{ ?>	
											<div class="event_desc"></div>
											<div class="image_share_holder" style="">
												<img style="width:30px;position:absolute;top:0px;right:165px;" class="image_social_icon event_social_like" eid="<?= $new['event_id']; ?>" pid="<?= $new['image_id']; ?>" src="<?= site_url('items/frontend/img/like_sq.png');?>"/>	
												 <div class="news_likes_holder" ><?= $new['likes'];?></div>
												<div class="button news_comment_button" cc="<?= count($new['comments']);?>">COMMENT <? if(count($new['comments']) > 0) echo "(".count($new['comments']).")";?></div>						
											</div>
									<?	}
									?>
								</div>
								
								
								<!-- COMMENT CONTENT -->
								<div class="comment_content">
									<div class="dosissemi" style="width:340px;margin-left:25px;margin-top:25px;">COMMENTS</div>
									<div class="comments">
										<? foreach($new['comments'] as $comment):?>
											<div class="comment_holder_item">
												<img class="comment_profile_img" src="<?= site_url('items/uploads/profilepictures/'.$comment['profile_image'])?>">
												<div class="comment_name dosissemi"><?= $comment['nickname']?></div>
												<div class="news_comment_text dosisreg"><?= $comment['comment']?></div>
											</div>
										<? endforeach;?>
									</div>
									<textarea class="comment_input" eid="<?= $new['event_id'] ?>" placeholder="Write your message here..." name="comment_input"></textarea>
									<div class="comment_send">Press ENTER to send the message!</div>
								</div>
								
							</div>
						
						<? }?>
					<?php $x++;
						$j++;
					 endforeach;
				 }?>			
			</div>

</div>
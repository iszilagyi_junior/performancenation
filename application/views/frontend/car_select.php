	<div id="login_car_selector" style="text-align:center;">
		<? if(count($my_cars)>0){ ?>
				<div class="dosisextralight" style="font-size:30px;text-align:center;">Choose your ride</div>
				<div id="login_car_holder">
					<div style="width: 100%;margin-top:0px;overflow:hidden;text-align:center;">
					 <?
						$x = 1; foreach($my_cars as $car): 
					
						if ($x <= 10){
						?>
							<a href="<?= site_url('car/select_vehicle/'.$car['id']);?>">
								<div class="news_random_friend_item " id="<?= $car['id']?>" style="height:230px;<?if($x%5 == 0) echo 'margin-right:0px;'; else echo 'margin-right:18px;';?>" dog_name="<?= $car['nickname']?>">
									<div class="div_white_over"></div>
									<img class="news_random_friend_image" src="<?= site_url('items/uploads/profilepictures/' . $car['profile_image'])?>" border="0" />
									<div class="news_random_friend_desc dosisbold" style=""><?= $car['nickname'];?></div>	
								</div>
							</a>
						
					<?php } $x++; endforeach; 
					}else{ ?>
						<div class="dosisextralight" style="font-size:30px;text-align:center;">Ooh, it appears you dont have a vehicle yet!</div>
						<div class="regular">Add one below to get started</div>
						<br><br>
						
						<div id="login_form" style="height:300px;">
							<?php 
								$data = array('id' => 'car_new_form');
								echo form_open("car/create", $data);
							?>
							<input type="text" id="create_car_name" name="create_car_name" value="" placeholder="Vehicle nickname" class="login_input" style="width: 226px;height: 26px;line-height: 26px;">
							<br>					
							<select id="vehicle_type_select" name="vehicle_type_select" class="login_input" style="">									
								<option value="car">Car</option>
								<option value="motorcycle">Motorcycle</option>
								<option value="truck">Truck</option>
							</select>
							<br>
							<select id="vehicle_engine_select" name="vehicle_engine_select" class="login_input" style="">									
								<option value="0">Engine type...</option>
								<option value="gas">Gas</option>
								<option value="diesel">Diesel</option>
							</select>
							<br/>
							<select id="vehicle_year_select" name="vehicle_year_select" class="login_input" style="text-transform:capitalize;width:240px;">									
								<option value="0">Please select a year...</option>
								<? foreach($car_years as $year){?>
									<option value="<?= $year;?>"><?= $year?></option>
								<? }?>
							</select>
							<br>							
							<select id="vehicle_make_select" name="vehicle_make_select" class="login_input" style="">									
								<option value="0">Make...</option>
								<option value="not">Not in the list</option>
							</select>
							<br>
							<select id="vehicle_model_select" name="vehicle_model_select" class="login_input" style="">									
								<option value="0">Model...</option>
								<option value="not">Not in the list</option>
							</select>
						<!--	
							<br/ style="clear:both;">
							<div id="dog_register_breed" style="">
								<div class="sansitalic" style="margin:5px auto;text-align: center;"><?= $this->lang->line('dog_create_breed')?></div>
								<input type="text" id="dog_breed" name="dog_breed" 
									value="<?php if(isset($dog_breed_repop) && $dog_breed_repop != "") echo $dog_breed_repop; else echo $this->lang->line('dog_new_breed')?>" 
									defaultText="<?= $this->lang->line('dog_new_breed')?>" 
									class="suggest_input <?php if(!isset($dog_breed_repop) || $dog_breed_repop == "") echo "input_default";?>">
							</div>
							<br style="clear:both;">
						-->
							<div class="button" id="add_new_car" style="width:240px;">ADD</div>
							<?php echo form_close();?>	
							<div id="car_create_error_message">
								
							</div>
						</div>
						<?}
					?>
				
			</div>
				</div>
	</div>
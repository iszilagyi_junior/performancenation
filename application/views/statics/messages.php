<script>

$(document).ready(function()
{
	selectize_user_search();
});
</script>
<div id="content" style="width:800px;top:263px;margin-bottom:100px;">
	<img class="" style="position:absolute;left:0px;top:0px;" src="<?= site_url('items/frontend/img/big_chrome.png')?>" border="0">
	<div class="playbold gold page_title" style="text-align:center;font-size:40px;letter-spacing:1px;z-index:10;position:relative;top:10px;">MESSAGES</div>

	<div id="messages_main_body">
	<!--	<img id="messages_loading" src="<?= site_url('items/frontend/img/loading_hd.gif')?>"> -->
				<div id="messages_header">
			<div class="dosissemi" style="padding-left:15px;float:left;">CONVERSATIONS</div>
			<div class="dosisextralight" style="margin-left:55px;float:left;<? if($new_message_counter > 0) echo "color:#f9a33b"?>"><span id="messages_counter"><?= $new_message_counter;?></span> NEW</div>
			<div id="conversations_name_search_holder">
				<label style="float: left;margin-left: 30px;" class="dosissemi" for="conversation_name_search">To:</label>
				<div style="width: 300px;float: left;margin-left: 10px;margin-top: 8px;">
					<select id="conversation_name_search" name="conversation_name_search"></select>
				</div>
				
			</div>
			<!-- <div class="regular" id="write_message_button">WRITE MESSAGE</div> -->
		</div>
		
		<div id="conversations_holder">
			<? foreach($messages as $message):?>
				<div class="conversation_item" pid="<?= $message['partner_id']?>">
					<img class="active_arrow" src="<?= site_url('items/frontend/img/message_active_arrow.png')?>"/>
					<div class="conversation_image_holder">
					
						<img style="width:100%" src="<?= site_url('items/uploads/profilepictures/'.$message['profile'])?>"/>
					</div>
					<div class="conversation_date"><?= $message['date'];?></div>
					<div class="conversation_name"><?= $message['name'];?></div>
					<div class="conversation_text_sample"><?= $message['message'];?></div>
				</div>
			<? endforeach;?>
		
		</div>
		<div id="messages_holder">
			
		</div>

		<div id="message_input_holder">
			<textarea id="new_message_text" name="new_message_text" placeholder="Write message..."></textarea>
			<div class="button regular" id="send_new_message" rec_id="101">SEND</div>
		</div>
	</div>
	
</div>
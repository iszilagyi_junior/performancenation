<div id="content" style="background:#000000; width:800px;left:200px;top: 260px;margin-bottom: 280px;color:#ffffff;min-height:600px;">
	
	<div class="statics_subheader" style="top:0px;">Albums</div>
	<? if($my_vehicle):?>
			<div>
				<div class="regular">New Album:</div>
				<input type="text" class="edit_input" name="new_album_title" id="new_album_title" placeholder="Title..." value="" />
				<div id="add_album" cid="<?= $my_vehicle_id;?>" class="button" style="margin-top:0px;">ADD ALBUM</div>
			</div>
		<? endif;?>
	
	<a href="javascript:history.back()">	
		<div  style="width:90px;top:0px;position:absolute;left:20px;z-index:10;"  class="button">BACK</div>			
	</a>
	
	<div id="album_holder" style="top:30px">
		<? if($count_albums == 0){?>
			<div class="no_photo_text sansitalic">User has no albums yet!</div>
		<? }?>
		<?if(isset($albums)){
			$x = 1;
			foreach($albums as $album){?>
				<div class="album_item_outer" <?if($x%3==0){ echo 'style="margin-right:0px;"';}?>>
					<a href="<?= site_url('album/'.$album['id']);?>">
						<div class="album_item" >
							<?if($album['pictures']){ 				
								foreach($album['pictures'] as $i=>$pic){
									if($i==0){
										if($pic['fname']!=""){
							?>
											
											<div class="album_item_image_holder" style="background-image: url('<?= site_url('items/uploads/'.$pic['folder'].'/'.$pic['fname']);?>');"></div>
											
							<? 				
										}
									}
								}
							}else{?>
								<div class="album_item_image_holder">
									<img class="album_item_image" src="<?= site_url('items/uploads/profilepictures/default.png')?>"/>
								</div>
							<? }?>
							
							<div class="album_item_title"><?= $album['title'];?></div>
							<div class="album_item_about"><?= $album['about'];?></div>
						</div>
					</a>
				</div>
		<? 
		$x++;
		}
			}?>
	</div>	
	<br>
	
</div>
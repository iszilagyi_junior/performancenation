<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['gender_male'] = 'Male';
$lang['gender_female'] = 'Female';
$lang['years'] = 'Year';
$lang['months'] = 'Month';
$lang['favorite'] = 'favorite';
$lang['favorite'] = 'unfavorite';

$lang['about_snoopstr'] = 'About snoopstr';
/*********************************************************************************
 * FRONTEND
 ***********************************************************************************/
$lang['sidemenu_login_header'] = 'login';
$lang['sidemenu_login_username'] = 'Email';
$lang['sidemenu_login_password'] = 'Password';
$lang['sidemenu_login_confirm'] = 'sign in';
$lang['sidemenu_facebook_header'] = 'On Facebook?';
$lang['sidemenu_facebook_confirm'] = 'Facebook-LOGIN';
$lang['sidemenu_register_header'] = 'new here?';
$lang['sidemenu_register_confirm'] = 'register now';
$lang['sidemenu_static_breed'] = 'breeds';
$lang['sidemenu_static_dogzone'] = 'dogzones';
$lang['sidemenu_static_vet'] = 'veterinaries';
$lang['sidemenu_static_tips'] = 'tips';
$lang['sidemenu_static_sitters'] = 'dogsitters';
$lang['sidemenu_static_misc'] = 'places';

$lang['sidemenu_footer_contact'] = 'contact';
$lang['sidemenu_footer_impressum'] = 'imprint';
$lang['sidemenu_member_greeting'] = 'Logged in as';
$lang['sidemenu_member_area'] = 'Territory';
$lang['sidemenu_member_myprofile'] = 'profile';
$lang['sidemenu_member_logout'] = 'logout';
$lang['sidemenu_member_mydogs'] = ' your dogs';
$lang['sidemenu_member_myphotos'] = 'photos';
$lang['sidemenu_member_myfriends'] = 'dog friends';
$lang['sidemenu_member_myenemies'] = 'dog enemies';
$lang['sidemenu_member_search'] = 'dog search';
$lang['sidemenu_member_invite'] = 'invite';
$lang['sidemenu_member_activities'] = 'activities';
$lang['sidemenu_member_news'] = 'news feed';

$lang['login_login_username'] = 'Email';
$lang['login_login_password'] = 'Password';
$lang['login_login_confirm'] = 'login';
$lang['login_header'] = 'login';

$lang['register_top_header'] = 'Register';
$lang['register_email'] = 'Email';
$lang['register_password'] = 'Password';
$lang['register_passconf'] = 'Repeat password';
$lang['register_firstname'] = 'Firstname';
$lang['register_lastname'] = 'Lastname';
$lang['register_pw_length'] = 'Minimum 8 characters';
$lang['register_birthday'] = 'Birthday (MM.DD.YYYY)';
$lang['register_birthday_day'] = 'DD';
$lang['register_birthday_month'] = 'MM';
$lang['register_birthday_year'] = 'YYYY';
$lang['register_terms'] = 'Accept Terms & Conditions';
$lang['register_newsletter'] = 'Please send me newsletters';
$lang['register_confirm'] = 'Register';
$lang['register_header'] = 'Become part of <br/>the gang';
$lang['register_success'] = 'Registration successfull';
$lang['register_success_text'] = 'You can log in now with your email address and password';
$lang['register_country'] = 'Country';
$lang['register_state'] = 'State';
$lang['register_zip'] = 'Zip';
$lang['register_city'] = 'City';
$lang['register_address'] = 'Address';
$lang['register_availability'] = 'Availabilty';
$lang['register_phone'] = 'Phone';
$lang['register_fb'] = 'Facebook';
$lang['register_tw'] = 'Twitter';
$lang['register_insta'] = 'Instagram';
$lang['instagram_hashtag'] = 'Your hashtag';
$lang['register_or'] = 'or';
$lang['register_teaser'] = 'Create profiles for your dogs, make <br/> friends with others and plan activities!';

$lang['edit_user_pw_confirm'] = "Change password";
$lang['edit_user_confirm'] = "Apply changes";
$lang['edit_user_pw_old'] = "Old Password";
$lang['edit_user_header'] = "Edit Profile";
$lang['edit_user_pw_change_success'] = 'Password successfully changed';
$lang['edit_user_profile_change'] = 'Profile updated';
$lang['edit_user_dogsitter_checkbox'] = "Show profile as a dogsitter";
$lang['edit_user_dogsitter_checkbox_deactive'] = "Remove my profile from dogsitters";
$lang['edit_user_hashtag_checkbox'] = "Show hashtagged images";
$lang['edit_user_import_instagram'] = "Enable auto import from hashtagged images from Instagram";
$lang['edit_user_import_instagram_description'] = "To import Instagram photos into your snoopstr album simply use the hashtag below with your Instagram posts. The images will show up in your album called Instagram photos:";
$lang['edit_user_show_email'] = "Show email address";
$lang['edit_user_select_country'] = "Please select a country...";
$lang['edit_user_select_state'] = "Please select a state...";
$lang['edit_user_available_text'] = "About:";
$lang['edit_user_contact'] = "Contact:";
$lang['edit_user_contact_text'] = "Provide us with your contact information to let other dog owners contact you.";

$lang['home_most_recent'] = 'Most recent';
$lang['home_teaser'] = 'Everybody loves dogs!<br/><br/>Welcome to the community that brings together dogs and their owners,<br/>locating your nearest friends, as well as dog-zones, vets, sitters!<br/>Sign in or sign up to use snoopstr and enjoy befriending other dogs, following their shared photos, stories and lives.';
$lang['about'] = "About";
 /*********************************************************************************
 * DOGS
 ***********************************************************************************/
$lang['dog_profile'] = "Profile";
$lang['dog_overview_new_dog'] = 'New dogprofile';
$lang['dog_new_name'] = "Name";
$lang['dog_owner'] = "Owner";
$lang['dog_new_nickname'] = "Nickname";
$lang['dog_new_breed'] = "Breed";
$lang['dog_new_create'] = "Create dogprofile";
$lang['dog_new_header'] = "New dogprofile";

$lang['dog_edit_header'] = " profile edit";
$lang['dog_edit_birthday'] = "Birthday (MM.DD.YYYY)";
$lang['dog_edit_color'] = "Color";
$lang['dog_edit_coat'] = "Coat";
$lang['dog_edit_type'] = "Type";
$lang['dog_edit_size'] = "Size";
$lang['dog_edit_characteristics'] = "Characteristics";
$lang['dog_edit_about'] = "Story";
$lang['dog_edit_chip'] = "Chip-Number";
$lang['dog_edit_success'] = "Dogprofile successfully updated";
$lang['dog_edit_manage_friends'] = "edit Friends";
$lang['dog_edit_manage_enemies'] = "edit Enemies";
$lang['dog_edit_gender'] = " Gender";
$lang['dog_update_profile'] = "SAVE";
$lang['dog_delete_profile'] = "Delete Dog Profile";
$lang['dog_delete_confirm_text'] = "Are you sure you wish to delete this dog profile?";
$lang['dog_birthday'] = "Birthday";
$lang['dog_about'] = "About";
$lang['dog_doctor'] = "Veterinary";
$lang['dog_vaccine'] = "Vaccination";
$lang['dog_edit_cover'] = "Edit Cover Picture (600px x 250px)";
$lang['dog_edit_profile'] = "Edit Profile Picture (190px x 220px)";

$lang['dog_frenemies_header'] = "Connections of ";
$lang['dog_frenemies_friends_of'] = "Friend of ";
$lang['dog_frenemies_enemies_of'] = "Enemies of ";
$lang['dog_frenemies_friends'] = "Friends";
$lang['dog_frenemies_enemies'] = "Enemies";
$lang['dog_frenemies_mutual_friends'] = "mutual Friends";
$lang['dog_frenemies_mutual_enemies'] = "mutual Enemies";
$lang['dog_remove_friend'] = "Remove Friend";
$lang['dog_remove_enemy'] = "Remove Enemy";

$lang['dog_delete_friend'] = "Remove connection";
$lang['dog_add_friend'] = "Add as Friend";
$lang['dog_added_friend'] = "Is now your Friend";
$lang['dog_add_enemy'] = "Add as Enemy";
$lang['dog_added_enemy'] = "Is now your Enemy";
$lang['dog_friends'] = "Friends";
$lang['dog_enemies'] = "Enemies";
$lang['dog_no_dogs'] = "You have no dogs yet!";
$lang['dog_selection_add'] = "Add";
$lang['dog_selection_cancel'] = "Cancel";
$lang['dog_selection_friend_text'] = "Select your dog as friend:";
$lang['dog_selection_enemy_text'] = "Select your dog as enemy:";
$lang['dog_click'] = "Click ";
$lang['dog_here_link'] = " here";
$lang['dog_to_dogs'] = " to add a dog to your profile!";
$lang['dog_search_friend'] = " to search for dog friends!";
$lang['dog_no_friends'] = "You have no friends!";
$lang['dog_create_breed'] = "Add your own!";
$lang['dog_search_all_breeds'] = "All breeds";
$lang['dog_overview_facts'] = "facts";
$lang['dog_overview_medical'] = "medical records";
$lang['dog_overview_about'] = "Story";
$lang['dog_overview_see_more'] = "see more";
$lang['iframe_show_dog_text'] = "See on Snoopstr";

$lang['dog_lost'] = "Lost";
$lang['dog_lost_info'] = "Information:";
$lang['dog_lost_contact'] = "Contact:";


/*********************************************************************************
 * STATICS
 ***********************************************************************************/
$lang['vets_header'] = "Veterinaries";
$lang['vet_detail_desc'] = "About";
$lang['vet_detail_contact'] = "Contact";
$lang['vet_detail_times'] = "Ordination";
$lang['vet_detail_gallery'] = "Gallery";
$lang['vet_detail_already_rated'] = "You have aleady rated this vet!";
$lang['closest_vets'] = "Locate me";
$lang['vets_nearby'] = "vet search";
$lang['vet_name'] = "Vet name";
$lang['vets_featured_text'] = "FEATURED VET";
$lang['vets_nearby_desc'] = "Enter your current location, or the name of the vet <br/> below to find vets in your area!";
$lang['vets_search_name'] = "Search by name";
$lang['vets_search_text'] = "Type the name in the field below!";


$lang['places_nearby'] = "Place search";
$lang['place_name'] = "Place name";
$lang['places_featured_text'] = "FEATURED PLACE";
$lang['places_nearby_desc'] = "Enter your current location, or the name of the place <br/> below to find places in your area!";
$lang['places_search_name'] = "Search by name";
$lang['places_search_text'] = "Type the name in the field below!";

$lang['breed_details_characteristics'] = "Characteristics";

$lang['sitter_detail_contact'] = "Contact";
$lang['sitter_detail_desc'] = "Description";
$lang['sitter_detail_rating'] = "Rating:";
$lang['sitter_detail_rate'] = "Rate";
$lang['sitter_detail_already_rated'] = "You have aleady rated this sitter!";
$lang['sitter_detail_thank_rate'] = "Thank you for the rating!";
$lang['closest_sitters'] = "locate me";
$lang['sitters_featured_text'] = "FEATURED DOGSITTER";
$lang['sitter_name'] = "Sitter name";
$lang['sitters_nearby'] = "Dog sitter search";
$lang['sitters_nearby_desc'] = "Enter your current location, or the name of the sitter <br/> below to find vets in your area!";

$lang['zone_detail_address'] = "Address";
$lang['zone_detail_desc'] = "Description";
$lang['zone_detail_friend'] = " friends are here";
$lang['zone_detail_size'] = "Size: ";
$lang['zone_detail_rating'] = "Rating:";
$lang['zone_detail_rate'] = "Rate";
$lang['zone_detail_already_rated'] = "You have aleady rated this zone!";
$lang['zone_detail_thank_rate'] = "Thank you for the rating!";
$lang['suggest_zone'] = "Suggest zone";
$lang['closest_zones'] = "locate me";
$lang['suggest_zone_name'] = "Name";
$lang['suggest_zone_about'] = "Description";
$lang['suggest_zone_address'] = "Address";
$lang['suggest_zone_area'] = "Area (m&#178;)";
$lang['send_suggestion'] = "Send suggestion";
$lang['suggestion_sent'] = "Suggestion sent!";
$lang['zone_search'] = "Enter your current location, or let youself locate to find zones nearby.";
$lang['cant_find'] = "CAN’T FIND WHAT YOU ARE LOOKING FOR?";
$lang['suggest_new'] = "Suggest a new zone:";
$lang['zone_name'] = "Zone name";

$lang['profile_search'] = "Dog name";
$lang['profile_search_submit'] = "Search";

$lang['albums_add_photo'] = "add photo";
$lang['album_new_title'] = "Albumtitle";
$lang['album_about'] = "About this Album...";
$lang['add_new_album'] = "add album";
$lang['save_new_album'] = "save album";
$lang['new_album'] = "New Album";
$lang['my_albums'] = "My Albums";
$lang['add_album'] = "Album added!";
$lang['edit_album'] = "Edit Album";
$lang['remove_album'] = "Remove Album";
$lang['albums'] = "Albums";
$lang['no_photo_yet'] = "No photos in album";
$lang['no_tagged_photo_yet'] = "No tagged photo";
$lang['no_album_yet'] = "User has no albums";
$lang['photo_edit_title'] = "Edit title";

$lang['user_albums'] = "Photo Albums";
$lang['user_album_back_button'] = "Back";

$lang['edit_activity'] = "Edit Activity";
$lang['new_activity'] = "New Activity";
$lang['new_activity_date'] = "Date (MM-DD-YYYY)";
$lang['new_activity_time'] = "Time";
$lang['new_activity_form_date'] = "Date";
$lang['new_activity_place'] = "Location";
$lang['new_activity_about'] = "Description";
$lang['pending_activities'] = "Pending activities";
$lang['joined_activities'] = "Joined activities";
$lang['add_new_activity_button'] = "add activity";
$lang['edit_activity_button'] = "edit activity";
$lang['activity_guests'] = "Participating dogs";
$lang['add_activity_guest'] = "Invite a friend";
$lang['activity'] = "Activity";
$lang['accept_activity'] = "Accept";
$lang['decline_activity'] = "Decline";
$lang['activity_pending'] = "Pending requests";


$lang['sidemenu_newhere_text'] = 'Register to join the world’s first social network for animals';

$lang['invite_spread'] = 'spread the word';
$lang['invite_page_miss'] = 'Invite your dog owner friends to come register on Snoopstr';
$lang['invite_page_email_enter'] = 'Share our site and spread the word on social media!';
$lang['invite_page_share'] = 'or share this site on facebook or via mail to invite more dogs and dog owners!';
$lang['invite_send_mail_button'] = 'send invite';
$lang['home_no_news'] = 'No news';
$lang['activity_plan'] = 'You can plan activities with other dogs!';
$lang['activity_invite_friend'] = 'Enter the activity details below and invite your dog pals.';
$lang['closest_dogs'] = "Show dogs nearby";
$lang['vets_description'] = "Here you can find a list of all Vets who are registered.<br/> Search through them alphabetically, by entering their name, or let yourself localized to show nearby Vets";
$lang['sitters_description'] = "Here you can find a list of all Dogsitters who are registered.<br/> Search through the alphabetically, by entering their name, or let yourself localized to show nearby Dogsitters";
$lang['my_friends_search_dog'] = 'Find more dog friends';
$lang['my_enemies_search_dog'] = 'Find more Rivals';

$lang['news_photo_instagram'] = 'From instagram ';
$lang['request_delete'] = 'Request profile delete';
$lang['request_delete_confirm'] = 'Are you sure you wish to delete your profile?';
$lang['delete_underway'] = 'You requested a profile delete on: ';

$lang['partner_login_header'] = 'Partner login';
$lang['partner_login_username'] = 'Partner email';
$lang['partner_advertisements'] = 'Advertisements';
$lang['partner_available_amount'] = 'Available amount: ';
$lang['amount_mark'] = '$';
$lang['advert_image'] = 'Image:';
$lang['advert_link'] = 'Link to:';
$lang['advert_impressions'] = 'Impressions: ';
$lang['advert_clicks'] = 'Clicks: ';

$lang['partner_logout'] = 'Logout';
$lang['location'] = 'Location';
$lang['results'] = "Results";
$lang['please_log_in'] = "Please log in to see further details!";
$lang['search_dogs'] = "Search for dogs";
$lang['dog_search_description'] = "You can search based on name or breed. Leave name empty to show all dogs.";
$lang['dog_friend_suggestion'] = "Here are more friend suggestions";
$lang['dog_friend_suggestion_newsfeed'] = "Add some friends to see stories<br/> here are some suggestions";
$lang['dog_enemy_suggestion'] = "Here are more enemy suggestions";
$lang['dog_enemies_of'] = "Enemies of ";
$lang['dog_friends_of'] = "Friends of ";
$lang['dog_no_friends'] = "No friends yet!";
$lang['dog_no_enemies'] = "No enemies yet!";
$lang['booking_sitter_btn'] = "to the calendar";
$lang['booking_hover'] = "Bookings";
$lang['album_delete_confirm_text'] = "Are you sure you wish to delete this album?";
$lang['photo_delete_confirm_text'] = "Are you sure you wish to delete this picture?";
$lang['places'] = 'places';

$lang['dog_is_missing'] = ' is MISSING!';
$lang['embed_profile'] = 'Embed Profile';
$lang['edit_button'] = 'Edit';
$lang['owners_photos'] = 'Owners Photos';
$lang['tagged_images'] = 'Tagged Images';
$lang['are_you_sure'] = 'Are you sure?';
$lang['yes'] = 'YES';
$lang['no'] = 'NO';

$lang['cover_image'] = 'Cover Image';
$lang['please_select_area'] = 'Please select the area then click finish!';
$lang['finish'] = 'Finish';
$lang['close'] = 'Close';
$lang['not_in_list'] = 'Not in the list';
$lang['preview'] = 'Preview';
$lang['dog_not_lost'] = 'This dog is not marked as lost!';
$lang['print'] = 'Print';
$lang['please_select_breed'] = 'Please select breed...';
$lang['no_pending_activity'] = 'No pending activities';
$lang['invited_you'] = 'invited you to';
$lang['at'] = 'at';
$lang['no_upcoming_activity'] = 'No upcoming activities';
$lang['you_created_invitation'] = 'You created an invitation';
$lang['no_accepted_invites'] = 'No accepted invites';
$lang['no_pending_invites'] = 'No pending invites';
$lang['comments'] = 'COMMENTS';
$lang['add_comment'] = 'Add comment';
$lang['submit'] = 'SUBMIT';
$lang['available'] = 'Available';
$lang['unavailable'] = 'Unavailable';
$lang['suggest_zone_description'] = 'Suggest a description:';
$lang['tags'] = 'TAGS';
$lang['already_tagged'] = 'Already tagged';
$lang['search'] = 'SEARCH';
$lang['type_in_name'] = 'TYPE IN NAME';
$lang['save'] = 'SAVE';
$lang['likes'] = 'Likes: ';
$lang['register_to_like_comment'] = 'REGISTER TO LIKE OR COMMENT ON THIS PHOTO';
$lang['click_here_to_find_more_dogs'] = 'Click here to find even more dogs';
$lang['points_from'] = 'Points from';
$lang['users'] = 'users';
$lang['log_in_further_info'] = 'Please log in to see further information!';
$lang['book_this_sitter'] = 'Book this sitter';
$lang['sitter_available_dates'] = 'Here you can see this sitter available dates. Click on a date you would like to book!';
$lang['tagged_photos_of'] = 'Tagged photos of';

$lang['please_wait'] = 'Please wait...';
$lang['profile_image'] = 'Profile Image';
$lang['loading'] = 'LOADING...';
$lang['email_sent'] = 'Email sent!';
$lang['thank_you_suggestion'] = 'Thank you! Your suggestion has been sent to review!';
$lang['drag_drop'] = 'Drag & Drop Files Here';


/* NEW LINES ADDED AFTER 07.23 */
$lang['vet_login_header'] = 'Veterinary login';
$lang['vet_login_username'] = 'Email address';
$lang['vet_login_edit'] = 'Veterinary edit';
$lang['vet_title'] = 'Title:';
$lang['login_email'] = 'Login email';
$lang['promo_code_desc'] = 'If you have received a promotional code from one of our partners, please enter it here';
$lang['promo_code'] = 'Promo Code';
$lang['new_password'] = 'New Password';
$lang['password_mismatch'] = 'Password and Confirm password doesnt match!';
$lang['invalid_old_pw'] = 'Invalid old password!';
$lang['welcome'] = 'Welcome';
$lang['add_dog_to_friend'] = 'Add dogs to become friends';



$lang['remember_me'] = 'Remember Me';
$lang['community_access'] = 'Our community can also be accessed via our app';
$lang['download_now'] = 'Download it now!';
$lang['find_nearby_everything'] = 'Find nearby dogs, vets, and dog-friendly places. Get news feeds with photos, updates, and so much more!';
$lang['also_follow'] = 'You can also follow us now on';
$lang['spread_word'] = 'Help us spread the word!';
$lang['send_invite'] = 'SEND INVITES';
$lang['footer_press'] = 'PRESS';
$lang['suggest_zone_description_thanks'] = 'Thank you! Your suggestion has been sent to review!';
$lang['search_searching'] = 'Searching...';
$lang['sorry_nothin_found'] = 'Sorry, nothing found!';
$lang['sorry_nothin_found_sitter'] = 'Sorry, nothing found. Add yourself as a dogsitter and you will be listed here!';
$lang['login_forgot_password'] = 'Forgot your password?';
$lang['login_click_here'] = 'Click here!';
$lang['please_type_location'] = 'Please type in a location!';
$lang['dog_lost'] = 'Dog lost';
$lang['more'] = 'MORE';
$lang['forgot_pw_header'] = 'FORGOT PASSWORD';
$lang['forgot_sub_header'] = 'Please enter your Email so we can send you an email to reset your password.';
$lang['forgot_email'] = 'Email:';
$lang['forgot_pw_submit'] = 'SUBMIT';
$lang['no_dogs_for_breed'] = 'No dogs found for this breed';
$lang['no_dogs_for_name'] = 'No dogs found with this name';
$lang['upload_failed_text'] = 'Upload failed please try a smaller image';


/* EVENTS */
$lang['event_added_friend'] = ' added friend ';
$lang['event_new_friend'] = 'new Friend';
$lang['event_new_profile_image'] = 'new Profile Picture';
$lang['event_changed_profile_image'] = ' changed profile picture';
$lang['event_added_photo_album'] = ' added photo to album: ';
$lang['event_new_photo'] = 'new Picture';
$lang['event_rate_sitter'] = ' rated dogsitter ';
$lang['event_rate_vet'] = ' rated vet ';
$lang['event_rate_zone'] = ' rated dogzone ';
$lang['event_changed_cover'] = ' changed cover picture ';
$lang['event_new_cover'] = 'new Cover picture';
$lang['event_tagged_on_photo'] = ' was tagged on a photo: ';
$lang['event_tagged'] = 'Tagged';
$lang['event_dog_is_missing'] = ' is missing';
$lang['event_missing'] = 'missing';
$lang['event_dog_lost'] = 'LOST';
$lang['event_checked_in_vet'] = ' checked in at vet ';
$lang['event_checked_in'] = 'checked in';
$lang['event_checked_in_zone'] = ' checked in at dogzone ';
$lang['event_liked_a'] = ' liked a ';
$lang['event_commented_on_a'] = ' commented on a ';
$lang['event_commented_picture'] = 'Commented on a picture';
$lang['event_changed_story'] = ' changed story ';
$lang['event_story_change'] = 'Story change';
$lang['event_liked_picture'] = 'Liked a picture';

/* ERRORS */
$lang['forgot_password_email_not_found'] = 'If the given email address is found we will send a recovery mail.';


?>




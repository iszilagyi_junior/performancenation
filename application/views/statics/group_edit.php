<script>	
	var type= "<?= $type?>";
	
	$(document).ready(function()
	{	
		initAutocomplete();
	});
	
	
</script>
<? if(!$is_ie){?>
	<div id="image_cropping_overlay">
		<div class="" id="crop_title" style="position:relative;top:0px;text-align: center;font-size:22px;">Profile Picture</div>
		<div class="sansitalic" style="margin:5px auto;text-align: center;">Please select an area</div>
		<div id="image_crop_holder">
			
		</div>
		 <input type="hidden" id="selection_x1" name="selection_x1" value="" />
		 <input type="hidden" id="selection_x2" name="selection_x2" value="" />
		 <input type="hidden" id="selection_y1" name="selection_y1" value="" />
		 <input type="hidden" id="selection_y2" name="selection_y2" value="" />
		<div class="button" uid="<?= $this->session->userdata('user_id');?>" id="finish_crop" image_type="" is_group="true" style="left:255px;position: absolute;" group_id="<?= $group->id; ?>">FINISH</div>
		<div class="button" id="close_crop"  style="left:545px;position: absolute;" >CLOSE</div>
	</div>
<? }
	else
	{
?>
	
	<script>	

	$(document).ready(function()
	{	
		
		if(type == "coverpictures")
       {
           $('#crop_title').text(cover_image_text);
           $('input[name="selection_x1"]').val(0);
            $('input[name="selection_y1"]').val(0);
            $('input[name="selection_x2"]').val(755);
            $('input[name="selection_y2"]').val(320); 
       $('#image_crop_holder').imgAreaSelect({
            	x1: 0, y1: 0, x2: 755, y2: 320, 
            	aspectRatio: '600:250',
            	minHeight:320, 
            	minWidth:755, 
            	maxWidth:1500,
            	handles: true,
                onSelectEnd: function (img, selection) {
              
		            $('input[name="selection_x1"]').val(selection.x1);
		            $('input[name="selection_y1"]').val(selection.y1);
		            $('input[name="selection_x2"]').val(selection.x2);
		            $('input[name="selection_y2"]').val(selection.y2);            
		        }, 
		    }); 
       }
       else
       {
       		$('#crop_title').text(profile_image_text);
       		$('input[name="selection_x1"]').val(0);
            $('input[name="selection_y1"]').val(0);
            $('input[name="selection_x2"]').val(180);
            $('input[name="selection_y2"]').val(180);
       $('#image_crop_holder').imgAreaSelect({
        	x1: 0, y1: 0, x2: 180, y2: 180, 
            	aspectRatio: '180:180',
            	minHeight:180, 
            	minWidth:180,
            	maxWidth:1500, 
            	handles: true,
                onSelectEnd: function (img, selection) {
		            $('input[name="selection_x1"]').val(selection.x1);
		            $('input[name="selection_y1"]').val(selection.y1);
		            $('input[name="selection_x2"]').val(selection.x2);
		            $('input[name="selection_y2"]').val(selection.y2);            
		        }, 
		    });
       }
	});
	
	
	
</script>

	
	<div id="image_cropping_overlay" style="display:block;">
		<div class="statics_subheader" id="crop_title" style="position:relative;top:0px;">Profile Picture</div>
		<div class="sansitalic" style="margin:5px auto;text-align: center;">Please select an area</div>
		<div id="image_crop_holder">
			<img src="<?= site_url("items/uploads/".$type."/".$fname);?>">
		</div>
		 <input type="hidden" id="selection_x1" name="selection_x1" value="" />
		 <input type="hidden" id="selection_x2" name="selection_x2" value="" />
		 <input type="hidden" id="selection_y1" name="selection_y1" value="" />
		 <input type="hidden" id="selection_y2" name="selection_y2" value="" />
		<div class="dog_new_create_arrow_text" uid="<?= $this->session->userdata('user_id');?>" user="false" image_type="<?= $type;?>" image_name="<?= "items/uploads/".$type."/".$fname;?>" file_name="<?= $fname;?>" id="finish_crop" image_type="" style="width:190px;border:0;left:317px;" group_id="<?= $group->id; ?>">FINISH</div>
		<div class="dog_new_create_arrow_text" id="close_crop"  style="width:190px;border:0;left:517px;top:-17px;" >CLOSE</div>
	</div>
<? }?>
<div id="content" style="background:#000000; width:800px;top: 260px;margin-bottom: 280px;color:#ffffff;">


				<div id="BkgEr" style=""></div>
				<div id="group_cover_hover_holder">
					<div id="cover_hover_background"></div>
					<div id="cover_hover_text">
						Upload new cover picture (755x320px)
					</div>
				</div>
				
		
		<div id="IE9" style="display:none;margin-top:10px;height:30px;">
			<?php 
			$data = array('id' => 'add_cover_ie');
			echo form_open_multipart("statics/add_cover_ie", $data);?>
				<input type="file" preview="add_photo_btn_newsfeed" accept="image/*" id="add_group_coverimage_ie" name="cover_image" style="display:none;">
				<input type="hidden" name="dog_id" value="<?= $group->id?>" />
			<? echo form_close();
			
			
			$data = array('id' => 'add_dogprofile_ie');
			echo form_open_multipart("statics/add_dog_profile_ie", $data);?>
				<input type="file" preview="add_photo_btn_newsfeed" accept="image/*" id="add_group_profileimage_ie" name="profile_image" style="display:none;">
				<input type="hidden" name="dog_id" value="<?= $group->id?>" />
			<? echo form_close();?>
		</div>
				
				
				<div class="statics_subheader" style="top:0px;">
					<?= 'Edit group - '.$group->name;?>
				</div>
				<div id="edit_group_cover_upload" formfield="edit_group_cover_picture" class="">
					<img src="<?= site_url('items/uploads/coverpictures/' . $cover)?>" width="100%" border="0">
				</div>
				<div id="edit_group_profile_upload" formfield="edit_group_profile_picture" class="">
					<div id="group_profile_hover_holder">
						<div id="profile_hover_background"></div>
						<div id="profile_hover_text" style="font-size:12px;">
							Upload new logo (180x180px)
						</div>
					</div>
					<img style="width:100%;" src="<?=  site_url('items/uploads/profilepictures/' . $profile)?>" border="0">
				</div>
				
				
				<div id="edit_form" style="height:400px">
					<?php 
						$data = array('id' => 'group_edit_form');
						echo form_open_multipart("statics/edit_group_validation", $data);
					?>
					<div id="edit_left" class="edit_half" style="width:400px;float:left;">
						<input type="hidden" id="group_id" name="group_id" value="<?= $group->id?>">
						<input type="file" preview="edit_cover_upload" accept="image/*" id="edit_group_cover_picture" name="edit_group_cover_picture" class="edit_hidden_upload">
						<input type="file" preview="edit_profile_upload" accept="image/*" id="edit_group_profile_picture" name="edit_group_profile_picture" class="edit_hidden_upload">
						
						
						<div class="relative_parent">
							<div class="label_edit" style="line-height:60px;">Name</div>
							<input type="text" id="group_name" name="group_name" style="margin-top:15px" class="edit_input right" value="<?= $group->name;?>"/>				
						</div>
												
						<div class="relative_parent">
							<div class="label_edit" style="line-height:60px;">Description</div>
							<textarea  id="group_description" name="group_description" style="resize:none;height:65px;" class="edit_input right"><?= $group->description;?></textarea>				
						</div>
						
						
						<div class="relative_parent">
							<div class="label_edit" style="line-height:60px;">Address</div>
							<input  id="autocomplete" name="group_address" style="margin-top:15px" class="edit_input right" placeholder="Address..." value="<?= $group->address;?>"/>			
						</div>
						
						
					<!--	<div class="relative_parent">
							<div class="label_edit" style="line-height:60px;">Geo Location</div>
							<input  id="group_geo_location" name="group_geo_location" style="margin-top:15px" class="edit_input right" value="<?= $group->geo_location;?>"/>			
						</div>
					-->	
					
						<input type="hidden" id="group_geo_location" name="group_geo_location" style="margin-top:15px" class="edit_input right" value="<?= $group->geo_location;?>"/>
						
						
						<div class="relative_parent">
							<div class="label_edit" style="line-height:60px;">Auto accept applications? </div>
							<input type="radio" name="group_auto_accept" style="margin-top:25px;" class="" value="1" <? if($group->auto_accept == 1) echo 'checked="checked"'?>>YES &nbsp;&nbsp;
							<input type="radio" name="group_auto_accept" style="" class="" value="0" <? if($group->auto_accept == 0) echo 'checked="checked"'?>>NO 		
						</div>
						
					<!--	<input type="submit" value="UPDATE" class="button regular" style="line-height:25px;float:right;" action="group_edit" /> -->
												
						<div  class="button regular" style="float:right;" id="update_group" gid="<?= $group->id;?>">UPDATE</div>
						
						<div id="vehicle_delete_holder">					
							<div id="vehicle_delete" class="button" style="margin-top:70px; margin-left: 18px;">DELETE CLUB</div>
							<div id="vehicle_delete_confirm" style="margin-right:20px;">
								Are you sure you wish to delete this Club?<br/>
								<div id="club_delete_yes" iid="<?= $group->id;?>" class="button" style="width:80px;float: left;margin-left: 20px;">YES</div>
								<div id="vehicle_delete_no" class="button" style="width:80px;float:right;margin-right:30px;">NO</div>
							</div>
						</div>
						
					</div>

					<div id="edit_right" class="edit_half" style="position:relative;left:0px;top:0px;min-height:30px;margin-top:100px;float:right;">
					
						<div class="regular" style="text-align:center;">APPLICATIONS</div>
						<div id="edit_applications_holder">
							<? foreach($applications as $application):?>
								
									<div class="group_application_item">
										<div class="album_item_image" style="float:left;width:60px;height:60px;background-image: url('<?= site_url('items/uploads/profilepictures/'.$application['profile_image']);?>')"></div>						
										<div class="application_accept_holder">
											<a style="text-decoration:none;" href="<?= site_url('vehicle_profile/'.$application['pretty_url']);?>">
												<div class="group_application_item_title"><?= $application['nickname'];?></div>
											</a>
											<div class="accept_group_application" gid="<?= $group->id; ?>" cid="<?= $application['id'];?>">ACCEPT</div>
											<div class="decline_group_application" gid="<?= $group->id; ?>" cid="<?= $application['id'];?>">DECLINE</div>
										</div>
									</div>
									
								
							<? endforeach;?>
						</div>
						
					</div>
					
					
					
					<div id="edit_left" class="edit_half" style="width:100%;float:left;overflow:hidden;">
					
						<div class="regular" style="text-align:center;">MEMBERS</div>
						<div id="edit_members_holder">
							<? foreach($members as $member):?>
								<!--<a href="<?= site_url('vehicle_profile/'.$member['pretty_url']);?>">-->
									<div class="member_edit_item">
										<img class="member_item_img" style="width:100%;height:100%;" src="<?= site_url('items/uploads/profilepictures/'.$member['profile_image']);?>" />
										<div class="member_delete">X</div>
										
										<img class="member_admin" gid="<?= $group->id;?>" mid="<?= $member['id'];?>" src="<?= site_url('items/frontend/img/circle_plus.png')?>">
										<div class="member_img_fade"></div>
										<div class="member_edit_item_title"><?= $member['nickname'];?></div> 
										<div class="member_remove_holder">
											<div class="regular" style="font-size:16px;">Are you sure you want to remove this vehicle?</div>
											<div class="member_remove_yes" gid="<?= $group->id;?>" mid="<?= $member['id'];?>">YES</div>
											<div class="member_remove_no">NO</div>
										</div>
									</div>
									
									
							<!--	</a> -->
							<? endforeach;?>
						</div>
						
					</div>
					
						
						<?php echo form_close();?>
						
					<div id="login_error_message" style="position:absolute;left: 0px;top: 60px;height:20px;width:100%;">
						<?php if(isset($message)) echo $message;?>
					</div>
				</div>
				
				
				
</div>
<script>
	$('#sidemenu').css({top:35});
</script>
<div id="content">
<? if(count($featured)>0):?>
	<script>
		$('#sidemenu').css({top: 15});
	</script>
	<div id="featured_vets_holder">
		<? $x = 1; foreach($featured as $doc){?>
			<a href="<?= site_url('place/'.$doc['pretty'])?>">
			<div class="nearby_list_item" pid="<?= $doc['id'];?>" style="background:#d4003d;cursor:pointer;">																						
				<div class="zones_list_item_desc" style="top:50px;"><?= $doc['name'];?></div>
				<div class="zones_list_item_desc" style="top:90px;font-size:14px;"><?= $doc['type'];?></div>
				
				<div class="zones_list_item_desc" style="top:120px;font-size:14px;"><?= $this->lang->line('places_featured_text')?></div>
			</div>
		</a>
		<? $x++; }?>
	</div>
<? endif;?>
	<br/>
	<div id="vets_nearby_holder" style="margin-top:18px;">
		<div class="statics_subheader"><?= $this->lang->line('places_nearby')?></div>
		<div class="sansitalic" style="text-align:center;margin:20px 0px;"><?= $this->lang->line('places_nearby_desc')?></div>
		<hr class="hline">
		<div id="closest_places_btn_text"><?= $this->lang->line('closest_vets')?></div>
		<div id="closest_places_btn_search" style="margin-right:15px;"><?= $this->lang->line('profile_search_submit')?></div>
		<input type="text" id="place_location_input" name="place_location_input" placeholder="<?= $this->lang->line('location')?>" defaultText="<?= $this->lang->line('location')?>" >
		
		<div class="separator" style="width:172px;height:26px;clear:both;">
			<hr class="hline" style="float:left;margin-top:27px;width:60px">
			<span class="sansitalic" style="float:left;margin:10px 20px;margin-top:20px;">or</span>
			<hr class="hline" style="float:left;margin-top:27px;width:60px">
		</div>
		<div style="width:100%;height:50px;">
			<input type="text" id="place_search" name="place_search" placeholder="<?= $this->lang->line('place_name')?>" defaultText="<?= $this->lang->line('vet_name')?>" class="search_input">
			<div id="closest_places_name_search" style="margin-right:15px;"><?= $this->lang->line('profile_search_submit')?></div> 
		</div>
		<div class="sansitalic" style="text-align:center;margin:30px 0px;"><?= $this->lang->line('results')?></div>
		
		<div id="vets_list"></div>
					
	</div>
	<div id="places_holder">
		
	</div>
	
</div>
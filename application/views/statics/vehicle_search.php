<script>
	var vehicles = <?= $all_vehicles;?>;
	
	$(document).ready(function()
	{	
		
		setTimeout(function(){		
			selectize_vehicle_search_make();
			selectize_vehicle_search_model();
		},200);
	});
	
</script>

<div id="content" style="width:800px;top:260px;margin-bottom:100px;">
	<img class="" style="position:absolute;left:0px;top:0px;" src="<?= site_url('items/frontend/img/big_chrome.png')?>" border="0">
	<div class="playbold gold page_title" style="text-align:center;font-size:40px;letter-spacing:1px;z-index:10;position:relative;top:10px;margin-bottom:30px;">VEHICLE SEARCH</div>
	<form id="vehicle_search_form">
	<div style="text-align: left;margin-left:10px;">
		
		<div style="margin:0px auto;width: 600px;">
			<input type="text" id="profile_search" name="profile_search" placeholder="Nickname..." defaultText="Nickname..." class="edit_input" style="margin-bottom:5px;">
			<select id="vehicle_type" style="width:100px" name="vehicle_type">
				<option value="all">Vehicle type</option>
				<option value="0">Car</option>
				<option value="1">Motorcycle</option>
				<option value="2">Truck</option>
			</select>
			<select id="engine_type" style="width:109px" name="engine_type">
				<option value="all">Engine type</option>
				<option value="gas">Gas</option>
				<option value="diesel">Diesel</option>
			</select>
			
			<input type="number" placeholder="Year" id="vehicle_year_select" name="vehicle_year_select" class="" style="width:100px;height:34px;position:relative;top:-1px;" min="1936"/>									
<!--				<option value="0">Year...</option>
				<? foreach($car_years as $year){?>
					<option value="<?= $year;?>"><?= $year?></option>
				<? }?>
			</select> -->
			<br/>
			<input type="text" id="make_search" name="make_search" placeholder="Make" defaultText="Make" class="edit_input" style="width:205px;padding-left:5px;">
			<input type="text" id="model_search" name="model_search" placeholder="Model" defaultText="Model" class="edit_input" style="width:205px;padding-left:5px;">
			<input type="text" id="plate_search" name="plate_search" placeholder="Plate" defaultText="Plate" class="edit_input" style="width:155px;padding-left:5px;">
			<input type="text" id="zerosixty_search" name="zerosixty_search" placeholder="0-60 in seconds" defaultText="0 - 60" class="edit_input" style="width:195px;padding-left:5px;">
			<select id="car_rank" name="car_rank" style="width:205px;">
				<option value="all">All Ranks</option>
				<option value="rookie">Rookie</option> 
				<option value="racer">Racer</option>
				<option value="champion">Champion</option>
				<option value="elite">Elite</option>
				<option value="legend">Legend</option>

			</select>
			<select id="car_hpranges" name="car_hpranges" style="width:165px;">
				<option value="all">HP Range</option>
				<option value="50">50-150 HP</option> 
				<option value="151">151-250 HP</option>
				<option value="251">251-400 HP</option>
				<option value="401">401-700 HP</option>
				<option value="701">701-1000 HP</option>
				<option value="1001">>1000 HP</option> 
			</select>
			<div style="width:350px;margin-left:80px;overflow:hidden">
				<div style="float:left;text-align:center;color:#ffffff;">Show only my crew: <input style="position:relative;top:0px;" type="checkbox" name="only_my_crew" id="only_my_crew" value="1"/></div>
				<div style="float:right;text-align:center;color:#ffffff;">Show only for sale: <input style="position:relative;top:0px;" type="checkbox" name="for_sale" id="for_sale" value="1"/></div>
			</div>
			<div id="vehicle_search_reset" class="button" style="">RESET</div>
		</div>
		</form>
		
		<div id="profile_search_submit" class="button" style="margin:10px auto;">SEARCH</div>
		
		<div id="vehicle_nearby_search" class="button" style="margin:0px auto;">VEHICLES NEARBY</div>
		<div id="vehicle_list_search" class="button" style="margin:0px auto;display:none">LIST VIEW</div>
	</div>
		<br/>	<br/>
	<div class="titit" style="text-align:center;margin:10px 0px;color:#ffffff;font-size:18px;">Results</div>
	<div id="vehicle_map_holder"></div>
	<div id="resultHolder" style="margin-top:15px;">
		<div id="result_list">
		<? $x = 1; foreach($vehicles as $vehicle):?>
		<a  style="text-decoration:none;" href="<?= site_url('vehicle_profile/' . $vehicle['pretty_url'])?>">	
			<div class="result_list_item" style="">	
					<img class="result_item_image" src="<?= site_url('items/uploads/profilepictures/'.$vehicle['fname'])?>" style="">
					
					<img class="follow_circle" refresh="no" title="Follow" vid="<?= $vehicle['id'];?>" src="<?= site_url('items/frontend/img/circle_plus.png')?>">
					<div class="div_white_over"></div>
					<div class="result_item_text" style=""><?= $vehicle['nickname'];?><br/>
						<br/>
						<?= $vehicle['model']->year;?> 
						<span style="text-transform: capitalize;"><?= $vehicle['model']->make;?></span><br/>
						<span style="text-transform: capitalize;"><?= $vehicle['model']->model;?></span><br/>
						0-60: <? if($vehicle['zerosixty'] === NULL || $vehicle['zerosixty'] == "" || $vehicle['zerosixty'] == "0"){echo "N/A";} else { echo $vehicle['zerosixty']." s";} ?><br/>
						Followers: <?= $vehicle['followers'];?>
					</div>				
				
			</div>
		</a>
		<? $x++; endforeach;?>
		
		</div>		
	</div>
</div>
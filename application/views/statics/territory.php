<div id="content">
	<div id="vets_header" class="statics_header">
		<?= $this->lang->line('sidemenu_member_area')?>
	</div>
	
	<div id="territory_icon_holder">
		<a href="<?= site_url('dog_overview')?>"><div class="territory_item"><?= $this->lang->line('sidemenu_member_mydogs');?></div></a>
		<a href="<?= site_url('albums')?>"><div class="territory_item"><?= $this->lang->line('sidemenu_member_myphotos');?></div></a>
		<a href="<?= site_url('my_friends')?>"><div class="territory_item"><?= $this->lang->line('sidemenu_member_myfriends');?></div></a>
		<a href="<?= site_url('activities')?>"><div class="territory_item"><? echo $this->lang->line('sidemenu_member_activities'); if($pending_count>0) echo " (".$pending_count.")";?> </div></a>
		<a href="<?= site_url('profile_search')?>"><div class="territory_item"><?= $this->lang->line('sidemenu_member_search');?></div></a>
		<a href="<?= site_url('share')?>"><div class="territory_item"><?= $this->lang->line('sidemenu_member_invite');?></div></a>
	</div>
</div>

<div id="sidemenu_memberarea" class="sidemenu_submenu">				
								
				</div>
<style>
	select{width:100%;}
</style>
<script>	
	var type= "<?= $type?>";
	
	$(document).ready(function()
	{	
		gallery_file_upload();
	});
	
	
</script>
<? if(!$is_ie){?>
	<div id="image_cropping_overlay">
		<div class="" id="crop_title" style="position:relative;top:0px;text-align: center;font-size:22px;">Profile Picture</div>
		<div class="sansitalic" style="margin:5px auto;text-align: center;">Please select an area</div>
		<div id="image_crop_holder">
			
		</div>
		 <input type="hidden" id="selection_x1" name="selection_x1" value="" />
		 <input type="hidden" id="selection_x2" name="selection_x2" value="" />
		 <input type="hidden" id="selection_y1" name="selection_y1" value="" />
		 <input type="hidden" id="selection_y2" name="selection_y2" value="" />
		<div class="button" uid="<?= $this->session->userdata('user_id');?>" id="finish_crop" image_type="" style="left:255px;position: absolute;" car_id="<?= $car->id; ?>">FINISH</div>
		<div class="button" id="close_crop"  style="left:545px;position: absolute;" >CLOSE</div>
	</div>
<? }
	else
	{
?>
	
	<script>	

	$(document).ready(function()
	{	
		
		if(type == "coverpictures")
       {
           $('#crop_title').text(cover_image_text);
           $('input[name="selection_x1"]').val(0);
            $('input[name="selection_y1"]').val(0);
            $('input[name="selection_x2"]').val(755);
            $('input[name="selection_y2"]').val(320); 
       $('#image_crop_holder').imgAreaSelect({
            	x1: 0, y1: 0, x2: 755, y2: 320, 
            	aspectRatio: '600:250',
            	minHeight:320, 
            	minWidth:755, 
            	maxWidth:1000,
            	handles: true,
                onSelectEnd: function (img, selection) {
              
		            $('input[name="selection_x1"]').val(selection.x1);
		            $('input[name="selection_y1"]').val(selection.y1);
		            $('input[name="selection_x2"]').val(selection.x2);
		            $('input[name="selection_y2"]').val(selection.y2);            
		        }, 
		    }); 
       }
       else
       {
       		$('#crop_title').text(profile_image_text);
       		$('input[name="selection_x1"]').val(0);
            $('input[name="selection_y1"]').val(0);
            $('input[name="selection_x2"]').val(180);
            $('input[name="selection_y2"]').val(180);
       $('#image_crop_holder').imgAreaSelect({
        	x1: 0, y1: 0, x2: 180, y2: 180, 
            	aspectRatio: '180:180',
            	minHeight:180, 
            	minWidth:180,
            	maxWidth:500, 
            	handles: true,
                onSelectEnd: function (img, selection) {
		            $('input[name="selection_x1"]').val(selection.x1);
		            $('input[name="selection_y1"]').val(selection.y1);
		            $('input[name="selection_x2"]').val(selection.x2);
		            $('input[name="selection_y2"]').val(selection.y2);            
		        }, 
		    });
       }
	});
	
	
	
</script>

	
	<div id="image_cropping_overlay" style="display:block;">
		<div class="statics_subheader" id="crop_title" style="position:relative;top:0px;">Profile Picture</div>
		<div class="sansitalic" style="margin:5px auto;text-align: center;">Please select an area</div>
		<div id="image_crop_holder">
			<img src="<?= site_url("items/uploads/".$type."/".$fname);?>">
		</div>
		 <input type="hidden" id="selection_x1" name="selection_x1" value="" />
		 <input type="hidden" id="selection_x2" name="selection_x2" value="" />
		 <input type="hidden" id="selection_y1" name="selection_y1" value="" />
		 <input type="hidden" id="selection_y2" name="selection_y2" value="" />
		<div class="dog_new_create_arrow_text" uid="<?= $this->session->userdata('user_id');?>" user="true" image_type="<?= $type;?>" image_name="<?= "items/uploads/".$type."/".$fname;?>" file_name="<?= $fname;?>" id="finish_crop" image_type="" style="width:190px;border:0;left:317px;" car_id="<?= $car->id; ?>">FINISH</div>
		<div class="dog_new_create_arrow_text" id="close_crop"  style="width:190px;border:0;left:517px;top:-17px;" >CLOSE</div>
	</div>
<? }?>
<div id="content" style="width:800px;margin-bottom: 100px;color:#ffffff;">


				<div id="BkgEr" style=""></div>
				<div id="cover_hover_holder">
					<div id="cover_hover_background"></div>
					<div id="cover_hover_text">
						Upload new cover picture (755x320px)
					</div>
				</div>
				
		
		<div id="IE9" style="display:none;margin-top:10px;height:30px;">
			<?php 
			$data = array('id' => 'add_cover_ie');
			echo form_open_multipart("statics/add_cover_ie", $data);?>
				<input type="file" preview="add_photo_btn_newsfeed" accept="image/*" id="add_coverimage_ie" name="cover_image" style="display:none;">
				<input type="hidden" name="dog_id" value="<?= $car->id?>" />
			<? echo form_close();
			
			
			$data = array('id' => 'add_dogprofile_ie');
			echo form_open_multipart("statics/add_dog_profile_ie", $data);?>
				<input type="file" preview="add_photo_btn_newsfeed" accept="image/*" id="add_dogprofileimage_ie" name="profile_image" style="display:none;">
				<input type="hidden" name="dog_id" value="<?= $car->id?>" />
			<? echo form_close();?>
		</div>
				
				
				<div class="statics_subheader" style="top:0px;">
					<?= 'Edit '.$car->nickname.' - '.$model->year." ".$model->make." ".$model->model.'';?>
				</div>
				<div id="edit_cover_upload" formfield="edit_cover_picture" class="">
					<img src="<?= site_url('items/uploads/coverpictures/' . $cover)?>" width="100%" border="0">
				</div>
				<div id="edit_profile_upload" formfield="edit_profile_picture" class="">
					<div id="profile_hover_holder">
						<div id="profile_hover_background"></div>
						<div id="profile_hover_text" style="font-size:12px;">
							Upload new profile picture (180x180px)
						</div>
					</div>
					<img style="width:100%;" src="<?=  site_url('items/uploads/profilepictures/' . $profile)?>" border="0">
				</div>
				
				
				<div id="add_story_overlay">
							<div class="overlayBG"></div>
							<div style="position:relative;z-index:100">
								<img class="close_overlay" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />		
								<div class="titboldit" style="font-size:27px;margin-top:30px;">BLOG UPDATE</div>
									<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">
										<textarea placeholder="Type the story here..." id="story_piece_text"></textarea>
										<div class="button" id="upload_blog_button" style="margin-top:0px;">ADD IMAGES</div>
										<input type="file" name="upload_blog" id="upload_blog" multiple style="display:none;">
										<div id="story_thumb_holder">
											<!--<div class="story_thumb_img" style="background-image: url('<?= site_url('items/uploads/images/0f907df4c0a262d9d67224a4475550a3.jpg')?>');"></div>
											-->
										</div>
									</div>
									<div style="overflow:hidden;">
										<div id="add_story_piece" cid="<?= $car->id?>" class="button" style="float:left;margin-top:0px;margin-left:120px;width:120px;">SAVE</div>				
										<div id="cancel_story_piece" class=" button" style="float:right;margin-top: 0px;margin-right: 120px;width:120px;">CANCEL</div>
									</div>
								</div>
						</div>
				
				
				
				<div id="add_modification_overlay" style="">
		<div class="overlayBG"></div>
		<div style="position:relative;z-index:100">
			<img class="close_overlay" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />		
			<div class="titboldit" style="font-size:27px;margin-top:30px;">ADD MODIFICATION</div>
			<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">					
						<div style="width:400px;margin:30px auto;    text-align: center;">	
							<? if($car->vehicle_type == 0) // CAR
								{
							?>
								<input type="hidden" id="engine_type_select" tid="0" name="mod_vehicle_select" value="<?= $engine_type?>">
							
								<? if($engine_type == "gas"){?>
								<!-- GAS -->
									<select class="mod_type_select" engine="gas" tid="0" name="mod_type_select" >
										<option value="0">Please Select a mod type...</option>
										<option value="audio">Audio</option>
										<option value="body">Body</option>
										<option value="brake">Brakes</option>
										<option value="chassis">Chassis</option>
										<option value="cooling">Cooling system</option>
										<option value="drive_axle">Drive axle</option>
										<option value="engine">Engine</option>
										<option value="exhaust">Exhaust system</option>
										<option value="fuel">Fuel system</option>
										<option value="ignition">Ignition system</option>
										<option value="lighting">Lighting</option>
										<option value="suspension">Suspension</option>
										<option value="transmission">Transmission</option>
										<option value="tuned">Tuned</option>
										<option value="wheels">Wheels & Tires</option>
										<option value="other">Other</option>
									</select>
									<br/>
									
									<select id="mod_default_types" class="mod_subselect" name="mod_default_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Aftermarket</option>
									</select>
									
							
									
									<select id="mod_transmission_types" class="mod_subselect" name="mod_transmission_types" style="display:none;">
							<!--		<option value="1">Manual - Clutch (Stock)</option> -->
										<option value="2">Manual - Clutch (Aftermarket)</option>
							<!--		<option value="3">Automatic - Converter (Stock)</option> -->
										<option value="4">Automatic - Converter (Aftermarket)</option>
				
									</select>
								
									
									<select id="mod_body_types" class="mod_subselect" name="mod_body_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Paint - Factory</option>
										<option value="3">Paint - Custom</option>
										<option value="4">Body mods</option>
									</select>
									
									
									<select id="mod_chassis_types" class="mod_subselect" name="mod_chassis_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Aftermarket</option>
										<option value="3">Bracing</option>
									</select>
								
									
									<select id="mod_drive_axle_types" class="mod_subselect" name="mod_drive_axle_types" style="display:none;">
							<!--		<option value="1">2WD (Stock)</option> -->
										<option value="2">2WD (Upgraded internals)</option>
							<!--		<option value="3">4WD (Stock)</option> -->
										<option value="4">4WD (Upgraded internals)</option>
							<!--		<option value="5">AWD (Stock)</option> -->
										<option value="6">AWD (Upgraded internals)</option>
									</select>
									
									
									<select id="mod_brake_types" class="mod_subselect" name="mod_brake_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
							<!--		<option value="2">Upgraded - Front - Calipers (Stock)</option> -->
										<option value="3">Upgraded - Front - Calipers (Aftermarket)</option>
							<!--		<option value="4">Upgraded - Front - Rotors (Stock)</option> -->
										<option value="5">Upgraded - Front - Rotors (Aftermarket)</option>
							<!--		<option value="6">Upgraded - Rear - Drum (Stock)</option> -->
										<option value="7">Upgraded - Rear - Drum (Aftermarket)</option>
							<!--		<option value="8">Upgraded - Rear - Disc - Calipers (Stock)</option> -->
										<option value="9">Upgraded - Rear - Disc - Calipers (Aftermarket)</option>
							<!--		<option value="10">Upgraded - Rear - Disc - Rotors (Stock)</option> -->
										<option value="11">Upgraded - Rear - Disc - Rotors (Aftermarket)</option>
									</select>
									
									
									<select id="mod_exhaust_types" class="mod_subselect" name="mod_exhaust_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
							<!--		<option value="2">Header - Stock</option> -->
										<option value="3">Header - Shorty</option>
										<option value="4">Header - Long tube</option>
							<!--		<option value="5">Converter - Stock</option> -->
										<option value="6">Converter - High flow</option>
										<option value="7">Converter - Offroad pipe</option>
							<!--		<option value="8">Mid pipe - Stock</option> -->
										<option value="9">Mid pipe - H-pipe</option>
										<option value="10">Mid pipe - X-pipe</option>
							<!--		<option value="11">Muffler - Stock</option> -->
										<option value="12">Muffler - Aftermarket</option>
									</select>
								
									
									<select id="mod_engine_types" class="mod_subselect" name="mod_engine_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Internals</option>
										<option value="3">Induction - Naturally aspirated</option>
										<option value="4">Induction - Forced - Turbocharged</option>
										<option value="5">Induction - Forced - Supercharged (Positive displacement)</option>
										<option value="6">Induction - Forced - Supercharged (Centrifugal)</option>
										<option value="7">Induction - Nitrous - Dry system</option>
										<option value="8">Induction - Nitrous - Wet system</option>
										
									</select>
								
									
									
									<select id="mod_suspension_types" class="mod_subselect" name="mod_suspension_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
							<!--		<option value="2">Upgraded - Front - Springs (Stock)</option> -->
										<option value="3">Upgraded - Front - Springs (Aftermarket)</option>
							<!--		<option value="4">Upgraded - Front - Shocks (Stock)</option> -->
										<option value="5">Upgraded - Front - Shocks (Aftermarket)</option>
							<!--		<option value="6">Upgraded - Front - Struts (Stock)</option> -->
										<option value="7">Upgraded - Front - Struts (Aftermarket)</option>
							<!--		<option value="8">Upgraded - Rear - Springs (Stock)</option> -->
										<option value="9">Upgraded - Rear - Springs (Aftermarket)</option>
							<!--		<option value="10">Upgraded - Rear - Shocks (Stock)</option> -->
										<option value="11">Upgraded - Rear - Shocks (Aftermarket)</option>
							<!--		<option value="12">Upgraded - Rear - Struts (Stock)</option> -->
										<option value="13">Upgraded - Rear - Struts (Aftermarket)</option>
									</select>
									<br/>
									
								<? }
								else
								{?>	
									<!-- DIESEL -->
									<select class="mod_type_select" engine="diesel" tid="0" name="mod_type_select" >
										<option value="0">Please Select a mod type...</option>
										<option value="air_intake">Air intake</option>
										<option value="audio">Audio</option>
										<option value="body">Body</option>
										<option value="brake">Brakes</option>
										<option value="chassis">Chassis</option>
										<option value="drive_axle">Drive axle</option>
										<option value="engine">Engine</option>
										<option value="exhaust">Exhaust</option>
										<option value="fuellift_pump">Fuel lift pump</option>
										<option value="injection_pump">Injection pump</option>
										<option value="injector">Injectors</option>
										<option value="intercooler">Intercooler</option>
										<option value="lighting">Lighting</option>
										<option value="suspension">Suspension</option>
										<option value="transmission">Transmission</option>
										<option value="tuned">Tuned</option>
										<option value="turbo">Turbo</option>
										<option value="wheels">Wheels & Tires</option>
										<option value="other">Other</option>
									</select>
									
									<br/>
									<select id="mod_default_types" class="mod_subselect" name="mod_default_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Aftermarket</option>
									</select>
									
									
									
									<select id="mod_engine_types" class="mod_subselect" name="mod_engine_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Internals</option>	
									</select>
								
									
									<select id="mod_exhaust_types" class="mod_subselect" name="mod_exhaust_types" style="display:none;">
							<!--		<option value="1">Manifold (Stock)</option> -->
										<option value="2">Manifold (Aftermarket)</option>
							<!--		<option value="3">Exhaust system (Stock)</option> -->
										<option value="4">Exhaust system (Aftermarket)</option>
									</select>
								
									
									<select id="mod_transmission_types" class="mod_subselect" name="mod_transmission_types" style="display:none;">
							<!--		<option value="1">Manual - Clutch (Stock)</option> -->
										<option value="2">Manual - Clutch (Aftermarket)</option>
							<!--		<option value="3">Automatic - Converter (Stock)</option> -->
										<option value="4">Automatic - Converter (Aftermarket)</option>
				
									</select>
									
									
									<select id="mod_body_types" class="mod_subselect" name="mod_body_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Paint - Factory</option>
										<option value="3">Paint - Custom</option>
										<option value="4">Body mods</option>
									</select>
									
									
									<select id="mod_chassis_types" class="mod_subselect" name="mod_chassis_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Aftermarket</option>
										<option value="3">Bracing</option>
									</select>
									
									
									<select id="mod_drive_axle_types" class="mod_subselect" name="mod_drive_axle_types" style="display:none;">
							<!--		<option value="1">2WD (Stock)</option> -->
										<option value="2">2WD (Upgraded internals)</option>
							<!--		<option value="3">4WD (Stock)</option> -->
										<option value="4">4WD (Upgraded internals)</option>
									</select>
									
									
									
									<select id="mod_brake_types" class="mod_subselect" name="mod_chassis_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
							<!--		<option value="2">Upgraded - Front - Calipers (Stock)</option> -->
										<option value="3">Upgraded - Front - Calipers (Aftermarket)</option>
							<!--		<option value="4">Upgraded - Front - Rotors (Stock)</option> -->
										<option value="5">Upgraded - Front - Rotors (Aftermarket)</option>
							<!--		<option value="6">Upgraded - Rear - Drum (Stock)</option> -->
										<option value="7">Upgraded - Rear - Drum (Aftermarket)</option>
							<!--		<option value="8">Upgraded - Rear - Disc - Calipers (Stock)</option> -->
										<option value="9">Upgraded - Rear - Disc - Calipers (Aftermarket)</option>
							<!--		<option value="10">Upgraded - Rear - Disc - Rotors (Stock)</option> -->
										<option value="11">Upgraded - Rear - Disc - Rotors (Aftermarket)</option>
									</select>
									
									
									<select id="mod_suspension_types" class="mod_subselect" name="mod_suspension_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Lifted</option>
										<option value="3">Lowered</option>
									</select>
									<br/>
									
									
								<? }?>
								<br/>
								<input type="text" name="mod_default_input" id="mod_default_input" class="edit_input" placeholder="Enter text here..." style="display:none;"/>
								
							<?
								}
								else if($car->vehicle_type == 1) // MOTORCYCLE
								{
							?>
								
								
								<select class="mod_type_select" tid="1" engine="motor" name="mod_vehicle_select">
									<option value="0">Please Select a mod type...</option>
									<option value="engine">Engine</option>
									<option value="air_intake">Air intake</option>
									<option value="exhaust">Exhaust</option>
									<option value="transmission">Transmission</option>
									<option value="brake">Brakes</option>
									<option value="wheel">Wheels & Tires</option>
									<option value="lighting">Lighting</option>
									<option value="body">Body mods</option>
									<option value="suspension">Suspension</option>
									<option value="other">Other</option>
								</select>
								<br/>
								<select id="motor_mod_engine_types" class="mod_subselect" name="motor_mod_engine_types" style="display:none;">
							<!--	<option value="1">Stock</option> -->
									<option value="2">Internals</option>
									<option value="3">Forced induction</option>
								</select>
								
								
								<select id="mod_default_types" class="mod_subselect" name="mod_default_types" style="display:none;">
							<!--	<option value="1">Stock</option> -->
									<option value="2">Aftermarket</option>
								</select>
								<br/>
								<br/>
								<input type="text" name="mod_default_input" id="mod_default_input" class="edit_input" placeholder="Enter text here..." style="display:none;"/>
								
							<? }
								else // TRUCK
								{ ?>
									<input type="hidden" id="engine_type_select" tid="0" name="mod_vehicle_select" value="<?= $engine_type?>">
									
									<? if($engine_type == "gas"){?>	
										<!-- GAS -->
										<select class="mod_type_select" engine="gas" tid="2" name="mod_type_select" >
											<option value="0">Please Select a mod type...</option>
											<option value="audio">Audio</option>
											<option value="body">Body</option>
											<option value="brake">Brakes</option>
											<option value="chassis">Chassis</option>
											<option value="cooling">Cooling system</option>
											<option value="drive_axle">Drive axle</option>
											<option value="engine">Engine</option>
											<option value="exhaust">Exhaust system</option>
											<option value="fuel">Fuel system</option>
											<option value="ignition">Ignition system</option>
											<option value="lighting">Lighting</option>
											<option value="suspension">Suspension</option>
											<option value="transmission">Transmission</option>
											<option value="tuned">Tuned</option>
											<option value="wheels">Wheels & Tires</option>
											<option value="other">Other</option>
										</select>
										
										<select id="mod_default_types" class="mod_subselect" name="mod_default_types" style="display:none;">
								<!--		<option value="1">Stock</option> -->
											<option value="2">Aftermarket</option>
										</select>
										<br/>
								
										
										<select id="mod_transmission_types" class="mod_subselect" name="mod_transmission_types" style="display:none;">
								<!--		<option value="1">Manual - Clutch (Stock)</option> -->
											<option value="2">Manual - Clutch (Aftermarket)</option>
								<!--		<option value="3">Automatic - Converter (Stock)</option> -->
											<option value="4">Automatic - Converter (Aftermarket)</option>
					
										</select>
									
										
										<select id="mod_body_types" class="mod_subselect" name="mod_body_types" style="display:none;">
								<!--		<option value="1">Stock</option> -->
											<option value="2">Paint - Factory</option>
											<option value="3">Paint - Custom</option>
											<option value="4">Body mods</option>
										</select>
									
										
										<select id="mod_chassis_types" class="mod_subselect" name="mod_chassis_types" style="display:none;">
								<!--		<option value="1">Stock</option> -->
											<option value="2">Aftermarket</option>
											<option value="3">Bracing</option>
										</select>
									
										
										<select id="mod_drive_axle_types" class="mod_subselect" name="mod_drive_axle_types" style="display:none;">
								<!--		<option value="1">2WD (Stock)</option> -->
											<option value="2">2WD (Upgraded internals)</option>
								<!--		<option value="3">4WD (Stock)</option> -->
											<option value="4">4WD (Upgraded internals)</option>
								<!--		<option value="5">AWD (Stock)</option> -->
											<option value="6">AWD (Upgraded internals)</option>
										</select>
									
										
										<select id="mod_brake_types" class="mod_subselect" name="mod_brake_types" style="display:none;">
								<!--		<option value="1">Stock</option> -->
								<!--		<option value="2">Upgraded - Front - Calipers (Stock)</option> -->
											<option value="3">Upgraded - Front - Calipers (Aftermarket)</option>
								<!--		<option value="4">Upgraded - Front - Rotors (Stock)</option> -->
											<option value="5">Upgraded - Front - Rotors (Aftermarket)</option>
								<!--		<option value="6">Upgraded - Rear - Drum (Stock)</option> -->
											<option value="7">Upgraded - Rear - Drum (Aftermarket)</option>
								<!--		<option value="8">Upgraded - Rear - Disc - Calipers (Stock)</option> -->
											<option value="9">Upgraded - Rear - Disc - Calipers (Aftermarket)</option>
								<!--		<option value="10">Upgraded - Rear - Disc - Rotors (Stock)</option> -->
											<option value="11">Upgraded - Rear - Disc - Rotors (Aftermarket)</option>
										</select>
									
										
										<select id="mod_exhaust_types" class="mod_subselect" name="mod_exhaust_types" style="display:none;">
								<!--		<option value="1">Stock</option>
											<option value="2">Header - Stock</option> -->
											<option value="3">Header - Shorty</option>
											<option value="4">Header - Long tube</option>
								<!--		<option value="5">Converter - Stock</option> -->
											<option value="6">Converter - High flow</option>
											<option value="7">Converter - Offroad pipe</option>
								<!--		<option value="8">Mid pipe - Stock</option> -->
											<option value="9">Mid pipe - H-pipe</option>
											<option value="10">Mid pipe - X-pipe</option>
								<!--		<option value="11">Muffler - Stock</option> -->
											<option value="12">Muffler - Aftermarket</option>
										</select>
									
										
										<select id="mod_engine_types" class="mod_subselect" name="mod_engine_types" style="display:none;">
								<!--		<option value="1">Stock</option> -->
											<option value="2">Internals</option>
											<option value="3">Induction - Naturally aspirated</option>
											<option value="4">Induction - Forced - Turbocharged</option>
											<option value="5">Induction - Forced - Supercharged (Positive displacement)</option>
											<option value="6">Induction - Forced - Supercharged (Centrifugal)</option>
											<option value="7">Induction - Nitrous - Dry system</option>
											<option value="8">Induction - Nitrous - Wet system</option>
											
										</select>
									
										
										<select id="mod_suspension_types" class="mod_subselect" name="mod_suspension_types" style="display:none;">
											<option value="1">Stock</option> -->
								<!--		<option value="2">Upgraded - Front - Springs (Stock)</option> -->
											<option value="3">Upgraded - Front - Springs (Aftermarket)</option>
								<!--		<option value="4">Upgraded - Front - Shocks (Stock)</option> -->
											<option value="5">Upgraded - Front - Shocks (Aftermarket)</option>
								<!--		<option value="6">Upgraded - Front - Struts (Stock)</option> -->
											<option value="7">Upgraded - Front - Struts (Aftermarket)</option>
								<!--		<option value="8">Upgraded - Rear - Springs (Stock)</option> -->
											<option value="9">Upgraded - Rear - Springs (Aftermarket)</option>
								<!--		<option value="10">Upgraded - Rear - Shocks (Stock)</option> -->
											<option value="11">Upgraded - Rear - Shocks (Aftermarket)</option>
								<!--		<option value="12">Upgraded - Rear - Struts (Stock)</option> -->
											<option value="13">Upgraded - Rear - Struts (Aftermarket)</option>
										</select>
										<br/>
											
										
									<? }
									else
									{?>	
										<!-- DIESEL -->
										<select class="mod_type_select" engine="diesel" tid="2" name="mod_type_select" >
											<option value="0">Please Select a mod type...</option>
											<option value="air_intake">Air intake</option>
											<option value="audio">Audio</option>
											<option value="body">Body</option>
											<option value="brake">Brakes</option>
											<option value="chassis">Chassis</option>
											<option value="drive_axle">Drive axle</option>
											<option value="engine">Engine</option>
											<option value="exhaust">Exhaust</option>
											<option value="fuellift_pump">Fuel lift pump</option>
											<option value="injection_pump">Injection pump</option>
											<option value="injector">Injectors</option>
											<option value="intercooler">Intercooler</option>
											<option value="lighting">Lighting</option>
											<option value="suspension">Suspension</option>
											<option value="transmission">Transmission</option>
											<option value="tuned">Tuned</option>
											<option value="turbo">Turbo</option>
											<option value="wheels">Wheels & Tires</option>
											<option value="other">Other</option>
										</select>
									<? }?>
									<br/>
									<select id="mod_default_types" class="mod_subselect" name="mod_default_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Aftermarket</option>
									</select>
								
									
									
									<select id="mod_engine_types" class="mod_subselect" name="mod_engine_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Internals</option>	
									</select>
							
									
									<select id="mod_exhaust_types" class="mod_subselect" name="mod_exhaust_types" style="display:none;">
							<!--		<option value="1">Manifold (Stock)</option> -->
										<option value="2">Manifold (Aftermarket)</option>
							<!--		<option value="3">Exhaust system (Stock)</option> -->
										<option value="4">Exhaust system (Aftermarket)</option>
									</select>
								
									
									<select id="mod_transmission_types" class="mod_subselect" name="mod_transmission_types" style="display:none;">
							<!--		<option value="1">Manual - Clutch (Stock)</option> -->
										<option value="2">Manual - Clutch (Aftermarket)</option>
							<!--		<option value="3">Automatic - Converter (Stock)</option> -->
										<option value="4">Automatic - Converter (Aftermarket)</option>
				
									</select>
							
									
									<select id="mod_body_types" class="mod_subselect" name="mod_body_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Paint - Factory</option>
										<option value="3">Paint - Custom</option>
										<option value="4">Body mods</option>
									</select>
									
									
									<select id="mod_chassis_types" class="mod_subselect" name="mod_chassis_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Aftermarket</option>
										<option value="3">Bracing</option>
									</select>
								
									<select id="mod_drive_axle_types" class="mod_subselect" name="mod_drive_axle_types" style="display:none;">
							<!--		<option value="1">2WD (Stock)</option> -->
										<option value="2">2WD (Upgraded internals)</option>
							<!--		<option value="3">4WD (Stock)</option> -->
										<option value="4">4WD (Upgraded internals)</option>
									</select>
								
									
									
									<select id="mod_brake_types" class="mod_subselect" name="mod_chassis_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
							<!--		<option value="2">Upgraded - Front - Calipers (Stock)</option> -->
										<option value="3">Upgraded - Front - Calipers (Aftermarket)</option>
							<!--		<option value="4">Upgraded - Front - Rotors (Stock)</option> -->
										<option value="5">Upgraded - Front - Rotors (Aftermarket)</option>
							<!--		<option value="6">Upgraded - Rear - Drum (Stock)</option> -->
										<option value="7">Upgraded - Rear - Drum (Aftermarket)</option>
							<!--		<option value="8">Upgraded - Rear - Disc - Calipers (Stock)</option> -->
										<option value="9">Upgraded - Rear - Disc - Calipers (Aftermarket)</option>
							<!--		<option value="10">Upgraded - Rear - Disc - Rotors (Stock)</option> -->
										<option value="11">Upgraded - Rear - Disc - Rotors (Aftermarket)</option>
									</select>
							
									
									<select id="mod_suspension_types" class="mod_subselect" name="mod_suspension_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Lifted</option>
										<option value="3">Lowered</option>
									</select>
									<br/>
								<br/>
								<input type="text" name="mod_default_input" id="mod_default_input" class="edit_input" placeholder="Enter text here..." style="display:none;"/>
						
							<?	}
							?>
					
						</div>
					</div>	
					<div style="overflow:hidden">
						<div id="add_mod_piece" engine="<?= $engine_type?>" cid="<?= $car->id?>" class="button" style="width:120px;float:left;margin-top:0px;margin-left:160px;">ADD</div>				
						<div id="cancel_mod_piece" class="button" style="width:120px;margin-top:0px;margin-right: 160px;float:right;">CANCEL</div>
					</div>
					<div id="mod_error" class="regular" style="margin-top:30px;text-align:center;"></div>
				</div>
				
				</div>
				
				
				<div id="edit_form" style="">
					<?php 
						$data = array('id' => 'car_edit_form');
						echo form_open_multipart("car/edit", $data);
					?>
					<div id="edit_left" class="edit_half" style="width:400px">
						<input type="hidden" id="car_id" name="car_id" value="<?= $car->id?>">
						<input type="file" preview="edit_cover_upload" accept="image/*" id="edit_cover_picture" name="edit_cover_picture" class="edit_hidden_upload">
						<input type="file" preview="edit_profile_upload" accept="image/*" id="edit_profile_picture" name="edit_profile_picture" class="edit_hidden_upload">
						
						<div class="relative_parent">
							<div class="label_edit">Nickname</div>
							<input maxlength="100" type="text" id="car_nickname" name="car_nickname" value="<?= $car->nickname;?>" class="edit_input right" >				
						</div>
						
						<div class="relative_parent">
							<div class="label_edit">Additional label</div>
							<input type="text" id="car_label" name="car_label" value="<?= $car->label;?>" class="edit_input right">				
						</div>
						
						<div class="relative_parent">
							<div class="label_edit">Plate</div>
							<input type="text" id="car_plate" name="car_plate" value="<?= $car->plate;?>" class="edit_input right">				
						</div>
						
						<div class="relative_parent">
							<div class="label_edit">0-60 Entry</div>
							<input type="text" id="car_zerosixty" name="car_zerosixty" value="<?= $car->zerosixty;?>" class="edit_input right">				
						</div>
						
						<div class="relative_parent">
							<div class="label_edit" style="">HP /Ranges/</div>
							<select id="car_hpranges" name="car_hpranges" class="edit_input right" style="width:256px;">
								<option value="50" <? if($car->hp_ranges == 50) echo 'selected="selected"';?>>50-150 HP</option> 
								<option value="151" <? if($car->hp_ranges == 151) echo 'selected="selected"';?>>151-250 HP</option>
								<option value="251" <? if($car->hp_ranges == 251) echo 'selected="selected"';?>>251-400 HP</option>
								<option value="401" <? if($car->hp_ranges == 401) echo 'selected="selected"';?>>401-700 HP</option>
								<option value="701" <? if($car->hp_ranges == 701) echo 'selected="selected"';?>>701-1000 HP</option>
								<option value="1001" <? if($car->hp_ranges == 1001) echo 'selected="selected"';?>>>1000 HP</option> 
							</select>
							<!-- <textarea type="text"  style="resize:none;height:65px;" class="edit_input right"><?= $car->hp_ranges;?></textarea>	-->			
						</div>
						
						<div class="relative_parent">
							<div class="label_edit" style="">Actual HP</div>
							<input type="text" id="car_actual_hp" name="car_actual_hp" value="<?= $car->actual_hp;?>" class="edit_input right">		
						</div>
						
						<div class="relative_parent">
							<div class="label_edit">Quarter Mile</div>
							<input type="text" id="car_quarter_mile" name="car_quarter_mile" value="<?= $car->quarter_mile;?>" class="edit_input right">				
						</div>
						
						<div class="relative_parent">
							<div class="label_edit">Torque</div>
							<input type="text" id="car_torque" name="car_torque" value="<?= $car->torque;?>" class="edit_input right">				
						</div>
						
						
											
						<div class="relative_parent">
							<div class="label_edit" style="line-height:60px;">Description</div>
							<textarea  id="car_description" name="car_description" style="resize:none;height:65px;" class="edit_input right"><?= $car->description;?></textarea>				
						</div>
						
						<div class="relative_parent">
							<div class="label_edit" style="line-height:60px;">Set for sale flag</div>
							<input type="checkbox" name="for_sale" id="for_sale" value="1" style="float:left;margin-left:30px;margin-top:25px;" <?if($car->for_sale == 1) echo 'checked="checked"';?>/>					
						</div>
						
						
						<div class="relative_parent" id="show_price" style="<? if($car->for_sale == 0) echo "display:none";?>">
							<div class="label_edit">Price</div>
							<input type="text" id="price" name="price" value="<?= $car->price;?>" class="edit_input right">				
						</div>
						
						<input type="submit" value="UPDATE" class="button regular" style="line-height:25px;float:right;" action="car_edit" />
												
						
						
						
						
					</div>

					<div id="edit_right" class="edit_half">
						
						<div id="add_story_edit"  class="button" style="margin-top:0px;">ADD BLOG PIECE</div>
						<div id="add_modification"  class="button" style="margin-top:0px;">ADD MOD</div>
						
						<div class="regular">New Album:</div>
						<input type="text" class="edit_input" name="new_album_title" id="new_album_title" placeholder="Title..." value="" />
						<div id="add_album" cid="<?= $car->id;?>" class="button" style="margin-top:0px;">ADD ALBUM</div>
						<br/>
						<div class="regular" style="text-align:center;">ALBUMS</div>
						<div id="edit_albums_holder">
							<? foreach($albums as $album):?>
								<a href="<?= site_url('edit_album/'.$album['id']);?>">
									<div class="album_teaser_item">
										<div class="album_item_image" style="background-image: url('<?= site_url('items/uploads/images/'.$album['fname']);?>')"></div>
										<div class="album_item_title"><?= $album['title'];?></div>
									</div>
									
								</a>
							<? endforeach;?>
						</div>
						
					</div>
					<input type="file" preview="" accept="image/*" id="story_image_file" name="story_image_file" style="display:none;" multiple>
					<input type="file" preview="" accept="image/*" id="story_image_file_up" name="story_image_file_up" style="display:none;" multiple>	
						
						<?php echo form_close();?>
					
					<div id="vehicle_delete_holder">					
						<div id="vehicle_delete" class="button" style="margin:5px 20px;">DELETE VEHICLE</div>
						<div id="vehicle_delete_confirm">
							Are you sure you wish to delete your vehicle?<br/>
							<div id="vehicle_delete_yes" cid="<?= $car->id;?>" class="button" style="width:80px;float: left;margin-left: 20px;">YES</div>
							<div id="vehicle_delete_no" class="button" style="width:80px;float:right;margin-right:20px;">NO</div>
						</div>
					</div>	
					
					<div id="login_error_message" style="position: relative;right: 0px;top: 30px;">
						<?php if(isset($message)) echo $message;?>
					</div>
				</div>
				
				
				
</div>
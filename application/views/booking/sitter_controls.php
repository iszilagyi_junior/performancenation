<script>
	var addSitterControl = true;
	addSitterControlListeners();		
</script>


	<div class="sitter_controls" style="width:600px;">
	
	
	
	<?php foreach($requests->result() as $request):?>
		
		
		<div class="request_item <? if($request->status_id == 3)echo "request_declined_item"?>" >
			<div class="request_date">
				<div class="sitter_controll_th">Date</div>
				<?= $request->date?>
			</div>
			<br/>
			<div class="request_status" style="top:62px;">
				<div class="sitter_controll_th" >Status</div>
				<?= $request->status_name?>
			</div>
			<div class="request_user">
				<div class="sitter_controll_th">User</div>
				<?= $request->user_username?>
			</div>
			
			<div class="request_dogs" style="top:62px;">
				<div class="sitter_controll_th">Dogs</div>
				<?= $request->dogcount?>
			</div>
			<div class="request_message" style="">
				<div class="sitter_controll_th" style="text-align:left;">Message</div>
				<span class="request_the_message"><?= $request->message?></span>
			</div>
			<div class="request_actions" style="">
				<div class="sitter_controll_th" style="margin-bottom:2px;">Actions</div>
				<?= $actions[$request->id]?>
			</div>
		</div>
	<?php endforeach; ?>
	<div id='booking_message_overlay'>
		<div class='message_overlay_header'>Message</div>
		<div class="message_overlay_close">X</div>
		<div class="message_overlay_text"></div>
	</div>

	</div>
	
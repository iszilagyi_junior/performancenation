<script>	
	$('#sidemenu').css({top: 0});
</script>
<div id="content">
	<div class="statics_subheader" style="margin-top:35px"><?= $this->lang->line('tagged_photos_of')?> <?= $dog->name;?></div>
		
	<div id="pictures_holder" style="top:0px;">
		
		<?if(isset($pictures) && count($pictures) != 0){
			foreach($pictures as $pic){?>
					<div class="album_item">
						<a title="<?= $pic['title']; ?>" href="<?= site_url('items/uploads/images/'.$pic['fname']);?>" data-lightbox="pictures">
							<div class="album_item_image_holder">
								<img class="album_item_image" src="<?= site_url('items/uploads/images/'.$pic['fname']);?>"/>
							</div>
						</a>					
					</div>
		<? 
		}
			}
			else{?>
				<div class="no_photo_text sansitalic"><?= $this->lang->line('no_tagged_photo_yet')?></div>
			<? }?>
	</div>
</div>
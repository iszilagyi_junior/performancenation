<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Car extends MY_Controller {

    function __construct()
    {
        parent::__construct();
		//$this->checkLogin();

		$this->config->load('dog');
		$this->load->library('form_validation');
		$this->load->library('upload');
		
		$this->load->model('car_model');
		$this->load->model('statics_model');
		
		$this->checkLogin();
    }  	 
	 
	public function index()
	{
		$this->checkLogin();
		$this->loadDefaultView('frontend/home');
	}
	
	public function getDogInputValue($fields)
	{
		
		return $return;
	}
	
	
	public function my_vehicles()
	{
		$this->checkLogin();
		$my_cars = $this->car_model->getVehicles($this->session->userdata('user_id'))->result();
			
		$data['my_cars'] = array();
		
		foreach($my_cars as $car)
		{
			$profile = $this->statics_model->getImageByID($car->profile_image)->row()->fname;
			$cover = $this->statics_model->getImageByID($car->cover_image)->row()->fname;
			
			
			$item = array('id' => $car->id,
							'nickname' => $car->nickname,
							'pretty_url' => $car->pretty_url,
							'profile_image' =>$profile,
							'cover_image' => $cover);
							
			$data['my_cars'][] = $item;
		}
		$this->loadDefaultView('car/my_vehicles', $data);
	}
	
	public function create_view()
	{
		$this->checkLogin();
		$data['message'] = $this->session->flashdata('message');
		$data['dog_name_repop'] = $this->session->flashdata('dog_name_repop');
		$data['dog_nickname_repop'] = $this->session->flashdata('dog_nickname_repop');
		$data['dog_breed_repop'] = $this->session->flashdata('dog_breed_repop');
		
		$data['breeds'] = $this->car_model->getBreeds()->result();
				
		$this->loadDefaultView('car/vehicle_new', $data);
	}	
	
	public function create()
	{
		//validate form input
		$this->form_validation->set_rules('create_car_name', "Nickname", 'required|xss_clean|max_length[128]');
		
		
		if ($this->form_validation->run() == true)
		{
			
			$model_id =  $this->input->post('vehicle_model_select');
			$nickname = $this->input->post('create_car_name');
			
			
			
			if($this->input->post('create_car_name') == "Vehicle nickname")
			{
				$nickname = "";
			}
			else
			{
				$nickname = $this->input->post('create_car_name');
			}
			
			$engine_type = $this->input->post('vehicle_engine_select');
			$type = $this->input->post('vehicle_type_select');
			
			$vehicle_type = 0; //Car by default
			
			if($type == "motorcycle")
			{
				$vehicle_type = 1; //Motorcycle
			}
			
			if($type == "truck")
			{
				$vehicle_type = 2; //Truck
			}
			
			$data = array(
						'nickname' => $nickname,
						'model_id' => $model_id,
						'vehicle_type' => $vehicle_type,
						'engine_type' => $engine_type,
						'user_id' => $this->session->userdata('user_id'));	
						
			$new_car_id = $this->car_model->createCar($data);
			
		
			$pretty_url = $nickname."".$new_car_id;
			
			$pretty_url = $this->toAscii($pretty_url);
			
			$pretty_data = array('pretty_url' => $pretty_url);
		
			$this->car_model->createPrettyUrl($new_car_id, $pretty_data);
			
			$action_id = 3; //new vehicle profile
			$event_id = $this->statics_model->addEvent($action_id, $new_car_id);
			$this->statics_model->addEventVehicle($new_car_id,$event_id);
			
			$this->session->set_userdata('vehicle_id', $new_car_id);
			
			$response = array('success' => true,
								'car_id' => $new_car_id,
								'message' => "Vehicle successfully added!");
			
			
		}
		else
		{
			//display the create user form
			//set the flash data error message if there is one
			$message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));

			$this->session->set_flashdata('message', $this->data['message']);
			$this->session->set_flashdata('dog_name_repop', $this->input->post('dog_name'));
			$this->session->set_flashdata('dog_nicknam_repop', $this->input->post('dog_nickname'));
			$this->session->set_flashdata('dog_breed_repop', $this->input->post('dog_breed'));
			
			$response = array('success' => false,
								'message' => $message);
		}	
		
		echo json_encode($response);	
	}
	
	public function toAscii($str, $replace=array(), $delimiter='_') {
		 if( !empty($replace) ) {
		  $str = str_replace((array)$replace, ' ', $str);
		 }
		
		 $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		 $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		 $clean = strtolower(trim($clean, '-'));
		 $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
		
		 return $clean;
	}
	
	
	public function edit_view($car_id)
	{
		$this->checkLogin();
		

		$data['message'] = $this->session->flashdata('message');
		$data['car'] = $this->car_model->getVehicle($car_id)->row();
		
		if($data['car']->user_id != $this->session->userdata('user_id'))
		{
			redirect('vehicle_profile/'.$data['car']->pretty_url);
		}
		

		switch($data['car']->vehicle_type)
		{
			case 0:{
						 $data['model'] = $this->car_model->getCarModelById($data['car']->model_id)->row();
						$data['mods'] = $this->car_model->checkVehicleMod($data['car']->engine_type."_mods",$car_id);
	
					};break;
			case 1:{
					 $data['model'] = $this->car_model->getMotorModelById($data['car']->model_id)->row();
						$data['mods'] = $this->car_model->checkVehicleMod("motorcycle_mods",$car_id);
					};break;
			case 2:{
					 $data['model'] = $this->car_model->getTruckModelById($data['car']->model_id)->row();
					 $data['mods'] = $this->car_model->checkVehicleMod($data['car']->engine_type."_mods",$car_id);
					};break;
		}
		
		
		$data['is_ie'] = false;	
		
		$profile = $this->statics_model->getImageByID($data['car']->profile_image)->row()->fname;
		
				
		$cover = $this->statics_model->getImageByID($data['car']->cover_image)->row()->fname;
		
		
		$data['engine_type'] = $data['car']->engine_type;
		$data['profile'] = $profile;
		$data['cover'] = $cover;
		
		
		
		
		
		
		
		$albums =  $this->statics_model->getAlbums($car_id);
	
		$data['albums'] = array();
		
		foreach($albums as $album)
		{
			$image_id = $this->statics_model->getAlbumImage($album['id']);
			
			$fname = $this->statics_model->getImageByID($image_id)->row()->fname;
			$item = array('id' => $album['id'],
						  'title' => $album['title'],
						  'fname' => $fname);
						  
			$data['albums'][] = $item;			  
		}
		
		
		$this->loadDefaultView('car/vehicle_edit', $data);
	}
	
	
	public function edit()
	{
		//validate form input
		
		$this->form_validation->set_rules('car_nickname', 'Nickname', 'xss_clean|max_length[100]');
		$this->form_validation->set_rules('car_label', 'Label', 'xss_clean');
		$this->form_validation->set_rules('car_plate', 'Plate', 'xss_clean');
		$this->form_validation->set_rules('car_zerosixty', '0-60', 'xss_clean');
		$this->form_validation->set_rules('car_hpranges', 'HP /Ranges/', 'xss_clean');
		$this->form_validation->set_rules('car_torque', 'Torque', 'xss_clean');
		$this->form_validation->set_rules('car_quarter_mile', 'Quarter Mile', 'xss_clean');
		$this->form_validation->set_rules('car_description', 'Description', 'xss_clean');
		

		$cover_data = NULL;
		$profile_data = NULL;
		$entity_id = $this->input->post('car_id');
		
		if ($this->form_validation->run() == true)
		{
			
			$saved = $this->car_model->getVehicle($entity_id)->row();
			
			if($saved->nickname != $this->input->post('car_nickname'))
			{
				
				$event_id = $this->statics_model->addEvent(1, $entity_id);		
				$this->statics_model->addEventVehicle($entity_id,$event_id);
			}
			
			
			// prepare array for db insert
			$data = array(
						'id' => $entity_id,
						'nickname' => $this->input->post('car_nickname'),
						'label' => $this->input->post('car_label'),
						'plate' => $this->input->post('car_plate'),
						'description' => $this->input->post('car_description'),
						'hp_ranges' => $this->input->post('car_hpranges'),
						'actual_hp' => $this->input->post('car_actual_hp'),
						'zerosixty' => $this->input->post('car_zerosixty'),
						'quarter_mile' => $this->input->post('car_quarter_mile'),
						'torque' => $this->input->post('car_torque'),
						'for_sale' => $this->input->post('for_sale'),
						'price' => $this->input->post('price'));
						
		
			$this->car_model->editVehicle($data);
		
			$this->session->set_flashdata('message', 'Updated successfully!');
			redirect('vehicle_profile/' . $saved->pretty_url,'location');
			
		}
		else
		{
		
			$this->session->set_flashdata('message', (validation_errors() ? validation_errors() : $this->session->flashdata('message')));
			redirect('edit_vehicle/' . $entity_id,'location');
		}
		
			
	}
	
	
	
	
	
	public function edit_cover_ie($dog_id, $fname)
	{
		$this->checkLogin();
		$data['message'] = $this->session->flashdata('message');
		$data['dog'] = $this->car_model->getDog($dog_id);
		
		$data['is_ie'] = true;
		$data['type'] = 'coverpictures';
		$data['fname'] = $fname;
		
		$data['breeds'] = $this->car_model->getBreeds()->result();
		$data['year'] = NULL;
		$data['month'] = NULL;
		$data['day'] = NULL;
		$birthday = $data['dog']->birthday;
		
		if($birthday!=NULL){
			$arr = explode(".",$birthday);	
			$data['year'] = $arr[2];
			$data['month'] = $arr[1];
			$data['day'] = $arr[0];
		}	
		
		
		if(is_numeric($data['dog']->profile_picture))
		{
			$dogprofile = $this->statics_model->getImageByID($data['dog']->profile_picture)->row()->fname;
		}
		else
		{
			$dogprofile = $data['dog']->profile_picture;
		}
		
		if(is_numeric($data['dog']->cover_picture))
		{
			$dogcover = $this->statics_model->getImageByID($data['dog']->cover_picture)->row()->fname;
		}
		else
		{
			$dogcover = $data['dog']->cover_picture;
		}
		
		
		$data['dog_profile'] = $dogprofile;
		$data['dog_cover'] = $dogcover;
		
		
		$data['friends'] = $this->car_model->getFriends($dog_id, 5);
		$data['enemies'] = $this->car_model->getEnemies($dog_id, 5);
		$data['friends_total'] = $this->car_model->getFriendsTotal($dog_id);
		$data['enemies_total'] = $this->car_model->getEnemiesTotal($dog_id);
		$this->loadDefaultView('dog/dog_edit', $data);
	}
	
	
	
	public function edit_dogprofile_ie($dog_id, $fname)
	{
		$this->checkLogin();
		$data['message'] = $this->session->flashdata('message');
		$data['dog'] = $this->car_model->getDog($dog_id);
		
		$data['is_ie'] = true;
		$data['type'] = 'profilepictures';
		$data['fname'] = $fname;
		
		$data['breeds'] = $this->car_model->getBreeds()->result();
		$data['year'] = NULL;
		$data['month'] = NULL;
		$data['day'] = NULL;
		$birthday = $data['dog']->birthday;
		
		if($birthday!=NULL){
			$arr = explode(".",$birthday);	
			$data['year'] = $arr[2];
			$data['month'] = $arr[1];
			$data['day'] = $arr[0];
		}	
		
		
		if(is_numeric($data['dog']->profile_picture))
		{
			$dogprofile = $this->statics_model->getImageByID($data['dog']->profile_picture)->row()->fname;
		}
		else
		{
			$dogprofile = $data['dog']->profile_picture;
		}
		
		if(is_numeric($data['dog']->cover_picture))
		{
			$dogcover = $this->statics_model->getImageByID($data['dog']->cover_picture)->row()->fname;
		}
		else
		{
			$dogcover = $data['dog']->cover_picture;
		}
		
		
		$data['dog_profile'] = $dogprofile;
		$data['dog_cover'] = $dogcover;
		
		
		$data['friends'] = $this->car_model->getFriends($dog_id, 5);
		$data['enemies'] = $this->car_model->getEnemies($dog_id, 5);
		$data['friends_total'] = $this->car_model->getFriendsTotal($dog_id);
		$data['enemies_total'] = $this->car_model->getEnemiesTotal($dog_id);
		$this->loadDefaultView('dog/dog_edit', $data);
	}
	
	public function lost_dog($dog_id)
	{
		$this->checkLogin();
		$data['dog'] = $this->car_model->getDog($dog_id);
		
		if(is_numeric($data['dog']->profile_picture))
		{
			$dogprofile = $this->statics_model->getImageByID($data['dog']->profile_picture)->row()->fname;
		}
		else
		{
			$dogprofile = $data['dog']->profile_picture;
		}
		
		$data['dogprofile'] = $dogprofile;
		$data['owner'] = $this->car_model->getOwners($dog_id)->row();
		
		$this->loadDefaultView('dog/dog_lost', $data);
	}
	
	
	

	public function frenemies($dog_id)
	{
		$this->checkLogin();
		$data['dog'] = $this->car_model->getDog($dog_id);
		$data['friends'] = $this->car_model->getFriends($dog_id);
		foreach($data['friends']->result() as $friend)
		{
			$data['mutualfriends'][$friend->id] = $this->car_model->getMutualFriends($dog_id, $friend->id);
		}
		$data['enemies'] = $this->car_model->getEnemies($dog_id);
		foreach($data['enemies']->result() as $enemy)
		{
			$data['mutualenemies'][$enemy->id] = $this->car_model->getMutualFriends($dog_id, $enemy->id);
		}
		$data['friends_total'] = $this->car_model->getFriendsTotal($dog_id);
		$data['enemies_total'] = $this->car_model->getEnemiesTotal($dog_id);	
		$this->loadDefaultView('dog/frenemies', $data);
	}
	
	public function removeFriend()
	{
		$dog_id = $_POST['dog_id'];
		$friend_id = $_POST['friend_id'];
		$this->car_model->deleteFriend($dog_id, $friend_id);
	}
	
	public function removeEnemy()
	{
		$dog_id = $_POST['dog_id'];
		$enemy_id = $_POST['enemy_id'];
		$this->car_model->deleteEnemy($dog_id, $enemy_id);
	}
	
	
	public function getShortinfo()
	{
		$this->load->helper('MY_dog_helper');
		$shortinfo = $this->car_model->getShortinfo($_POST['params'][0]);
        $data['breed'] = $shortinfo->breed;
        $data['name'] = $shortinfo->name;
        $data['profile_picture'] = $shortinfo->profile_picture;
		$data['age'] = getDogAge($shortinfo->birthday, $this->lang->line('years'), $this->lang->line('months'));
		$owners = $this->car_model->getOwners($_POST['params'][0]);
		$data['owners'] = "";
		foreach($owners->result() as $owner)
		{
			if($data['owners'] != "")
				$data['owners'] .= ', ';
			$data['owners'] = $owner->first_name . ' ' . $owner->last_name;
		}
        
        echo json_encode($data);
	}
	

	
	public function vehicle_detail_pretty($pretty_url)
	{
		$vehicle = $this->car_model->getPrettyUrl($pretty_url)->row();

		$data['car'] = $vehicle;
		$car_id = $data['car']->id;
		
		$data['mods'] = array();
	
		switch($data['car']->vehicle_type)
		{
			case 0:{
						$data['model'] = $this->car_model->getCarModelById($data['car']->model_id)->row();
						$mods = $this->car_model->getVehicleMod($data['car']->engine_type."_mods",$car_id);
						
						
						foreach($mods as $mod)
						{
							
							switch($mod->category)
							{
								case "audio": { $mod_type = $this->car_model->getMod("audio_types", $mod->type);};break;
								case "air_intake": { $mod_type = $this->car_model->getMod($data['car']->engine_type."_".$mod->category."_types", $mod->type); };break;
								case "body": { $mod_type = $this->car_model->getMod("body_types", $mod->type);};break;
								case "brake": { $mod_type = $this->car_model->getMod("brake_types", $mod->type);};break;
								case "chassis": { $mod_type = $this->car_model->getMod("chassis_types", $mod->type);};break;
								case "cooling": { $mod_type = $this->car_model->getMod("gas_coolingsystem_types", $mod->type);};break;
								case "drive_axle": { $mod_type = $this->car_model->getMod("drive_axle_types", $mod->type);};break;
								case "engine": { $mod_type = $this->car_model->getMod($data['car']->engine_type."_".$mod->category."_types", $mod->type);};break;
								case "exhaust": { $mod_type = $this->car_model->getMod($data['car']->engine_type."_".$mod->category."_types", $mod->type); };break;
								case "fuel": { $mod_type = $this->car_model->getMod($data['car']->engine_type."_".$mod->category."system_types", $mod->type); };break;
								case "fuellift_pump": { $mod_type = $this->car_model->getMod("diesel_fuel_lift_types", $mod->type); };break;
								case "injection_pump": { $mod_type = $this->car_model->getMod("diesel_injection_pump_types", $mod->type); };break;
								case "injector": { $mod_type = $this->car_model->getMod("diesel_injection_types", $mod->type); };break;
								case "intercooler": { $mod_type = $this->car_model->getMod("diesel_intercooler_types", $mod->type); };break;
								case "ignition": { $mod_type = $this->car_model->getMod($data['car']->engine_type."_".$mod->category."system_types", $mod->type); };break;
								case "lighting": { $mod_type = $this->car_model->getMod("lighting_types", $mod->type);};break;
								case "suspension": { $mod_type = $this->car_model->getMod($data['car']->engine_type."_".$mod->category."_types", $mod->type); };break;
								case "transmission": { $mod_type = $this->car_model->getMod("transmission_types", $mod->type);};break;
								case "tuned": { $mod_type = $this->car_model->getMod("tuned_types", $mod->type);};break;
								case "turbo": { $mod_type = $this->car_model->getMod("diesel_turbo_types", $mod->type);};break;
								case "wheels": { $mod_type = $this->car_model->getMod("wheel_types", $mod->type);};break;
								case "other": { $mod_type = $this->car_model->getMod("wheel_types", $mod->type);};break;
							}
							
							$mod_name = "";
							if($mod_type->name != 'Stock' && $mod_type->name != 'Aftermarket')
							{
								$mod_name = $mod_type->name;
							}
							
							
							$modtitle = str_replace("_", " ", $mod->category);
							$modtitle = ucfirst ($modtitle);
							
							$mod_item = array('id' => $mod->id,
												'title' => $modtitle,
												'text' => $mod->text,
												'category' => $mod->category,
												'type' => $mod_name);
							
							$the_mods[] = $mod_item;
						}

				};break;
			case 1:{
					$data['model'] = $this->car_model->getMotorModelById($data['car']->model_id)->row();
						$mods = $this->car_model->getVehicleMod("motorcycle_mods",$car_id);
						
						
						foreach($mods as $mod)
						{
							switch($mod->category)
							{
								case "audio": { $mod_type = $this->car_model->getMod("audio_types", $mod->type); };break;
								case "body": { $mod_type = $this->car_model->getMod("motorcycle_body_types", $mod->type);};break;
								case "brake": { $mod_type = $this->car_model->getMod("motorcycle_brake_types", $mod->type);};break;
								case "chassis": { $mod_type = $this->car_model->getMod("chassis_types", $mod->type);};break;
								case "cooling": { $mod_type = $this->car_model->getMod("gas_coolingsystem_types", $mod->type);};break;
								case "drive_axle": { $mod_type = $this->car_model->getMod("drive_axle_types", $mod->type);};break;
								case "engine": { $mod_type = $this->car_model->getMod("motorcycle_engine_types", $mod->type); };break;
								case "exhaust": { $mod_type = $this->car_model->getMod("motorcycle_exhaust_types", $mod->type); };break;
								case "lighting": { $mod_type = $this->car_model->getMod("motorcycle_lighting_types", $mod->type); };break;
								case "suspension": { $mod_type = $this->car_model->getMod("motorcycle_suspension_types", $mod->type); };break;
								case "transmission": { $mod_type = $this->car_model->getMod("motorcycle_transmission_types", $mod->type); };break;
								case "wheel": { $mod_type = $this->car_model->getMod("motorcycle_wheel_types", $mod->type); };break;
								
							}
							
							$mod_name = "";
							if($mod_type->name != 'Stock' && $mod_type->name != 'Aftermarket')
							{
								$mod_name = $mod_type->name;
							}
							
							
							$modtitle = str_replace("_", " ", $mod->category);
							$modtitle = ucfirst ($modtitle);
							
							$mod_item = array('id' => $mod->id,
												'title' => $modtitle,
												'text' => $mod->text,
												'category' => $mod->category,
												'type' => $mod_name);
							
							$the_mods[] = $mod_item;
						}
					};break;
			case 2:{
					 	$data['model'] = $this->car_model->getTruckModelById($data['car']->model_id)->row();
						$mods = $this->car_model->getVehicleMod($data['car']->engine_type."_mods",$car_id);
						
						
						foreach($mods as $mod)
						{
							switch($mod->category)
							{
								case "audio": { $mod_type = $this->car_model->getMod("audio_types", $mod->type);};break;
								case "air_intake": { $mod_type = $this->car_model->getMod($data['car']->engine_type."_".$mod->category."_types", $mod->type); };break;
								case "body": { $mod_type = $this->car_model->getMod("body_types", $mod->type);};break;
								case "brake": { $mod_type = $this->car_model->getMod("brake_types", $mod->type);};break;
								case "chassis": { $mod_type = $this->car_model->getMod("chassis_types", $mod->type);};break;
								case "cooling": { $mod_type = $this->car_model->getMod("gas_coolingsystem_types", $mod->type);};break;
								case "drive_axle": { $mod_type = $this->car_model->getMod("drive_axle_types", $mod->type);};break;
								case "engine": { $mod_type = $this->car_model->getMod($data['car']->engine_type."_".$mod->category."_types", $mod->type);};break;
								case "exhaust": { $mod_type = $this->car_model->getMod($data['car']->engine_type."_".$mod->category."_types", $mod->type); };break;
								case "fuel": { $mod_type = $this->car_model->getMod($data['car']->engine_type."_".$mod->category."system_types", $mod->type); };break;
								case "fuellift_pump": { $mod_type = $this->car_model->getMod("diesel_fuel_lift_types", $mod->type); };break;
								case "injection_pump": { $mod_type = $this->car_model->getMod("diesel_injection_pump_types", $mod->type); };break;
								case "injector": { $mod_type = $this->car_model->getMod("diesel_injection_types", $mod->type); };break;
								case "intercooler": { $mod_type = $this->car_model->getMod("diesel_intercooler_types", $mod->type); };break;
								case "ignition": { $mod_type = $this->car_model->getMod($data['car']->engine_type."_".$mod->category."system_types", $mod->type); };break;
								case "lighting": { $mod_type = $this->car_model->getMod("lighting_types", $mod->type);};break;
								case "suspension": { $mod_type = $this->car_model->getMod($data['car']->engine_type."_".$mod->category."_types", $mod->type); };break;
								case "transmission": { $mod_type = $this->car_model->getMod("transmission_types", $mod->type);};break;
								case "tuned": { $mod_type = $this->car_model->getMod("tuned_types", $mod->type);};break;
								case "turbo": { $mod_type = $this->car_model->getMod("diesel_turbo_types", $mod->type);};break;
								case "wheels": { $mod_type = $this->car_model->getMod("wheel_types", $mod->type);};break;
								case "other": { $mod_type = $this->car_model->getMod("wheel_types", $mod->type);};break;
							}
							
							$mod_name = "";
							if($mod_type->name != 'Stock' && $mod_type->name != 'Aftermarket')
							{
								$mod_name = $mod_type->name;
							}
							
							$modtitle = str_replace("_", " ", $mod->category);
							$modtitle = ucfirst ($modtitle);
							
							$mod_item = array('id' => $mod->id,
												'title' => $modtitle,
												'text' => $mod->text,
												'category' => $mod->category,
												'type' => $mod_name);
							
							$the_mods[] = $mod_item;
						}				 
					};break;
		}

		
		
		function compareByName($a, $b) {
		  return strcmp($a["title"], $b["title"]);
		}
		usort($the_mods, 'compareByName');
		

		$data['mods'] = $the_mods;
		
	/*	switch($data['car']->vehicle_type)
		{
			case 0:{
					$data['model'] = $this->car_model->getCarModelById($data['car']->model_id)->row();
					$mods = $this->car_model->checkVehicleMod($data['car']->engine_type."_mods",$car_id);
						
					if($data['car']->engine_type == "diesel")
					{
					 
					 
					/********* DIESEL MODS **********/ 
				/*	 	$mod_type = $this->car_model->getMod($data['car']->engine_type."_engine_types", $mods->engine_type);
						
						$mod_item = array('title' => "Engine",
											'text' => $mods->engine_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_air_intake_types", $mods->air_intake_type);
						
						$mod_item = array('title' => "Air Intake",
											'text' => $mods->air_intake_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_turbo_types", $mods->turbo_type);
						
						$mod_item = array('title' => "Turbo",
											'text' => $mods->turbo_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_intercooler_types", $mods->intercooler_type);
						
						$mod_item = array('title' => "Intercooler",
											'text' => $mods->intercooler_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_injection_pump_types", $mods->injection_pump_type);
						
						$mod_item = array('title' => "Injection Pump",
											'text' => $mods->injection_pump_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_fuel_lift_types", $mods->fuellift_pump_type);
						
						$mod_item = array('title' => "Fuellift Pump",
											'text' => $mods->fuellift_pump_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_injection_types", $mods->injector_type);
						
						$mod_item = array('title' => "Injector",
											'text' => $mods->injector_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_exhaust_types", $mods->exhaust_type);
						
						$mod_item = array('title' => "Exhaust",
							
											'text' => $mods->exhaust_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;

						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_suspension_types", $mods->suspension_type);
						
						$mod_item = array('title' => "Suspension",
											'text' => $mods->suspension_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						

					 }
					 else 
					 {
			*/		 
			 /********* GAS MODS *********/
			/*		 	$mod_type = $this->car_model->getMod($data['car']->engine_type."_engine_types", $mods->engine_type);
						
						$mod_item = array('title' => "Engine",
											'text' => $mods->engine_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						

						$mod_type = $this->car_model->getMod($data['car']->engine_type."_coolingsystem_types", $mods->cooling_type);
						
						$mod_item = array('title' => "Cooling System",
											'text' => $mods->cooling_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_fuelsystem_types", $mods->fuelsystem_type);
						
						$mod_item = array('title' => "Fuel System",
											'text' => $mods->fuelsystem_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_ignitionsystem_types", $mods->ignition_type);
						
						$mod_item = array('title' => "Ignition",
											'text' => $mods->ignition_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_exhaust_types", $mods->exhaust_type);
						
						$mod_item = array('title' => "Exhaust",
							
											'text' => $mods->exhaust_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						

						$mod_type = $this->car_model->getMod($data['car']->engine_type."_suspension_types", $mods->suspension_type);
						
						$mod_item = array('title' => "Suspension",
											'text' => $mods->suspension_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						
						
	

					 }
					 
					 	$mod_type = $this->car_model->getMod("tuned_types", $mods->tuned_type);
						
						$mod_item = array('title' => "Tuned",
											'text' => $mods->tuned_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
					 
						$mod_type = $this->car_model->getMod("lighting_types", $mods->lighting_type);
						
						$mod_item = array('title' => "Lighting",
											'text' => $mods->lighting_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						

						$mod_type = $this->car_model->getMod("transmission_types", $mods->transmission_type);
						
						$mod_item = array('title' => "Transmission",
											'text' => $mods->transmission_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("drive_axle_types", $mods->drive_axle_type);
						
						$mod_item = array('title' => "Drive Axle",
											'text' => $mods->drive_axle_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("brake_types", $mods->brake_type);
						
						$mod_item = array('title' => "Brakes",
											'text' => $mods->brake_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("chassis_types", $mods->chassis_type);
						
						$mod_item = array('title' => "Chassis",
											'text' => $mods->chassis_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("body_types", $mods->body_type);
						
						$mod_item = array('title' => "Body",
											'text' => $mods->body_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("audio_types", $mods->audio_type);
						
						$mod_item = array('title' => "Audio",
											'text' => $mods->audio_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("wheel_types", $mods->wheels_type);
						
						$mod_item = array('title' => "Wheels",
											'text' => $mods->wheels_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_item = array('title' => "Other",
											'text' => $mods->other_mods,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						
	
					};break;
			case 1:{
					 $data['model'] = $this->car_model->getMotorModelById($data['car']->model_id)->row();
						$mods = $this->car_model->checkVehicleMod("motorcycle_mods",$car_id);
						
						
						$mod_type = $this->car_model->getMod("motorcycle_engine_types", $mods->engine_type);
						
						$mod_item = array('title' => "Engine",
											'text' => $mods->engine_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("motorcycle_air_intake_types", $mods->air_intake_type);
						
						$mod_item = array('title' => "Air Intake",
											'text' => $mods->air_intake_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("motorcycle_exhaust_types", $mods->exhaust_type);
						
						$mod_item = array('title' => "Exhaust",
							
											'text' => $mods->exhaust_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("motorcycle_transmission_types", $mods->transmission_type);
						
						$mod_item = array('title' => "Transmission",
											'text' => $mods->transmission_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("motorcycle_brake_types", $mods->brake_type);
						
						$mod_item = array('title' => "Brakes",
											'text' => $mods->brake_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("motorcycle_wheel_types", $mods->wheel_type);
						
						$mod_item = array('title' => "Wheels",
											'text' => $mods->wheel_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("motorcycle_lighting_types", $mods->lighting_type);
						
						$mod_item = array('title' => "Lighting",
											'text' => $mods->lighting_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("motorcycle_body_types", $mods->body_type);
						
						$mod_item = array('title' => "Body",
											'text' => $mods->body_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("motorcycle_suspension_types", $mods->suspension_type);
						
						$mod_item = array('title' => "Suspension",
											'text' => $mods->suspension_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						$mod_item = array('title' => "Other",
											'text' => $mods->other_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
					};break;
			case 2:{
					 $data['model'] = $this->car_model->getTruckModelById($data['car']->model_id)->row();
				 
					 $mods = $this->car_model->checkVehicleMod($data['car']->engine_type."_mods",$car_id);
					 
				
					 
					 if($data['car']->engine_type == "diesel")
					 {
					 
			*/		 
					/********* DIESEL MODS **********/ 
			/*		 	$mod_type = $this->car_model->getMod($data['car']->engine_type."_engine_types", $mods->engine_type);
						
						$mod_item = array('title' => "Engine",
											'text' => $mods->engine_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_air_intake_types", $mods->air_intake_type);
						
						$mod_item = array('title' => "Air Intake",
											'text' => $mods->air_intake_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_turbo_types", $mods->turbo_type);
						
						$mod_item = array('title' => "Turbo",
											'text' => $mods->turbo_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_intercooler_types", $mods->intercooler_type);
						
						$mod_item = array('title' => "Intercooler",
											'text' => $mods->intercooler_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_injection_pump_types", $mods->injection_pump_type);
						
						$mod_item = array('title' => "Injection Pump",
											'text' => $mods->injection_pump_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_fuel_lift_types", $mods->fuellift_pump_type);
						
						$mod_item = array('title' => "Fuellift Pump",
											'text' => $mods->fuellift_pump_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_injection_types", $mods->injector_type);
						
						$mod_item = array('title' => "Injector",
											'text' => $mods->injector_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_exhaust_types", $mods->exhaust_type);
						
						$mod_item = array('title' => "Exhaust",
							
											'text' => $mods->exhaust_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;

						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_suspension_types", $mods->suspension_type);
						
						$mod_item = array('title' => "Suspension",
											'text' => $mods->suspension_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						

					 }
					 else 
					 {
		*/			 
			 /********* GAS MODS *********/
		/*			 	$mod_type = $this->car_model->getMod($data['car']->engine_type."_engine_types", $mods->engine_type);
						
						$mod_item = array('title' => "Engine",
											'text' => $mods->engine_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						

						$mod_type = $this->car_model->getMod($data['car']->engine_type."_coolingsystem_types", $mods->cooling_type);
						
						$mod_item = array('title' => "Cooling System",
											'text' => $mods->cooling_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_fuelsystem_types", $mods->fuelsystem_type);
						
						$mod_item = array('title' => "Fuel System",
											'text' => $mods->fuelsystem_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_ignitionsystem_types", $mods->ignition_type);
						
						$mod_item = array('title' => "Ignition",
											'text' => $mods->ignition_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						
						$mod_type = $this->car_model->getMod($data['car']->engine_type."_exhaust_types", $mods->exhaust_type);
						
						$mod_item = array('title' => "Exhaust",
							
											'text' => $mods->exhaust_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						

						$mod_type = $this->car_model->getMod($data['car']->engine_type."_suspension_types", $mods->suspension_type);
						
						$mod_item = array('title' => "Suspension",
											'text' => $mods->suspension_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						
						
	

					 }
					 
					 	$mod_type = $this->car_model->getMod("tuned_types", $mods->tuned_type);
						
						$mod_item = array('title' => "Tuned",
											'text' => $mods->tuned_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
					 
						$mod_type = $this->car_model->getMod("lighting_types", $mods->lighting_type);
						
						$mod_item = array('title' => "Lighting",
											'text' => $mods->lighting_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						

						$mod_type = $this->car_model->getMod("transmission_types", $mods->transmission_type);
						
						$mod_item = array('title' => "Transmission",
											'text' => $mods->transmission_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("drive_axle_types", $mods->drive_axle_type);
						
						$mod_item = array('title' => "Drive Axle",
											'text' => $mods->drive_axle_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("brake_types", $mods->brake_type);
						
						$mod_item = array('title' => "Brakes",
											'text' => $mods->brake_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("chassis_types", $mods->chassis_type);
						
						$mod_item = array('title' => "Chassis",
											'text' => $mods->chassis_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("body_types", $mods->body_type);
						
						$mod_item = array('title' => "Body",
											'text' => $mods->body_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("audio_types", $mods->audio_type);
						
						$mod_item = array('title' => "Audio",
											'text' => $mods->audio_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_type = $this->car_model->getMod("wheel_types", $mods->wheels_type);
						
						$mod_item = array('title' => "Wheels",
											'text' => $mods->wheels_text,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
						
						
						$mod_item = array('title' => "Other",
											'text' => $mods->other_mods,
											'type' => $mod_type->name);
						
						$data['mods'][] = $mod_item;
					 
					};break;
		}
		*/
		$data['engine_type'] = $data['car']->engine_type;
		
		$data['owner'] = $this->car_model->getOwners($vehicle->user_id)->row();
		
		$data['is_my_car'] = false;
		if($data['owner']->id == $this->session->userdata('user_id')){
			$data['is_my_car'] = true;
		}
		
		$data['profile'] = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
		$data['cover'] = $this->statics_model->getImageByID($vehicle->cover_image)->row()->fname;
		
		$data['gallery'] = $this->car_model->getGalleryImages($vehicle->id);
		
		
		$stories = $this->car_model->getStoryPieces($vehicle->id);
		
		foreach($stories as $story)
		{
			$gallery_images = $this->statics_model->getStoryImages($story['id']);
			
			
			
			
			$story_item = array('id' => $story['id'],
								'created_date' => $this->set_user_timezone($story['created_date']),
								'story' => $story['story'],
								'gallery' => $gallery_images);
			$data['stories'][] = $story_item;
		}
		
		//$data['stories'] = $this->car_model->getStoryPieces($vehicle->id);
		
		$comments = $this->car_model->getVehicleComments($vehicle->id);
		$data['comments'] = array();
		
		
		foreach($comments as $comment)
		{			
			$username = $this->statics_model->getUserData($comment['user_id'])->row()->username;
			
			$item = array('id' => $comment['id'],
						'created_date' => $comment['created_date'],
						'comment' => $comment['comment'],
						'user' => $username);
		
			$data['comments'][] = $item;			
		}
		
		
		$albums =  $this->statics_model->getAlbums($vehicle->id);
	
		$data['albums'] = array();
		
		foreach($albums as $album)
		{
			$image_id = $this->statics_model->getAlbumImage($album['id']);
			$theimage = $this->statics_model->getImageByID($image_id)->row();
			$fname = $theimage->fname;
			$folder = 'images';
			
			if($theimage->cover_image == 1)
			{
				$folder = 'coverpictures';
			}
			
			
			if($theimage->profile_image == 1)
			{
				$folder = 'profilepictures';
			}
			
			
			$item = array('id' => $album['id'],
						  'folder' => $folder,
						  'title' => $album['title'],
						  'fname' => $fname);
						  
			$data['albums'][] = $item;			  
		}
		
		
		$data['follower_count'] = count($this->car_model->getFollowers($vehicle->id));
		
		$data['following'] = false;
		
		if($this->car_model->checkVehicle($vehicle->id,$this->session->userdata('vehicle_id')))
		{
			$data['following'] = true;
		}
		
		
		$num_mods = $this->statics_model->getNumMods($vehicle->id);
		
		$badge = false;
		
		if($num_mods->number_of_mods == 1)
		{
			$badge = '1mod_badge.png';
		}
		
		if($num_mods->number_of_mods == 2)
		{
			$badge = '2mod_badge.png';
		}
		
		if($num_mods->number_of_mods == 3)
		{
			$badge = '3mod_badge.png';
		}
		
		if($num_mods->number_of_mods == 4)
		{
			$badge = '4mod_badge.png';
		}
		
		if($num_mods->number_of_mods == 5)
		{
			$badge = '5mod_badge.png';
		}
		
		if($num_mods->number_of_mods > 5)
		{
			$badge = '5plusmod_badge.png';
		}
		
		
		$data['badge'] = $badge;
		
		
		
		
		$this->loadDefaultView('car/vehicle_detail', $data);	
	}
	
	
	public function dog_iframe($dog_id)
	{
		$this->CheckLanguage();
		$dogs = $this->car_model->getVehicles($this->session->userdata['user_id']);		
		$dogs_data = array();
		foreach($dogs->result() as $dog)
		{
			$name = $dog->name;
			$profile_picture = $dog->profile_picture;
			if(!$this->car_model->checkDog($dog->id,$dog_id)){	
				$dogs_data[] = array('id' => $dog->id,
									  'name' => $name,
									  'profile_picture' => $profile_picture
									  );
			}
		}
		$data['dogs'] = $dogs_data;
		$data['selected_dog'] = $dog_id;
		$data['dog'] = $this->car_model->getDog($dog_id);
		
		if(is_numeric($data['dog']->profile_picture))
		{
			$dogprofile = $this->statics_model->getImageByID($data['dog']->profile_picture)->row()->fname;
		}
		else
		{
			$dogprofile = $data['dog']->profile_picture;
		}
		
		$data['dog_profile'] = $dogprofile;
		
		
		$data['friends'] = $this->car_model->getFriends($dog_id)->result();
		$data['owner'] = $this->car_model->getOwners($dog_id)->row();
		
		foreach($data['friends'] as $friend)
		{
			$data['mutualfriends'][$friend->id] = $this->car_model->getMutualFriends($dog_id, $friend->id);
		}
		$data['enemies'] = $this->car_model->getEnemies($dog_id)->result();
		$data['friends_total'] = $this->car_model->getFriendsTotal($dog_id);
		$data['enemies_total'] = $this->car_model->getEnemiesTotal($dog_id);
		
		$this->load->view('dog/dog_iframe', $data);	
	}
	
	
	public function profile_search()
	{
		$search = $_POST['search'];
		$search = trim($search);
		
		$my_crew = $_POST['my_crew'];
		$for_sale = $_POST['for_sale'];
		
		$vehicle_type = $_POST['vehicle_type'];
		$engine_type = $_POST['engine_type'];
		
		$hp_ranges = $_POST['hp_ranges'];
		
		$car_rank = $_POST['rank'];
		
		$year_select = $_POST['year_select'];
		
		$make_input = $_POST['make_input'];
		$make_input = trim($make_input);
		
		$model_input = $_POST['model_input'];
		$model_input = trim($model_input);
		
		$plate_input = $_POST['plate_input'];
		$plate_input = trim($plate_input);
		
		$zerosixty_input = $_POST['zerosixty_input'];
		$zerosixty_input = trim($zerosixty_input);
		
		$nearby = $_POST['nearby'];
		
		$users = (isset($_POST['users']))? $_POST['users'] : array();
		$vehicle_array = array();
		
		$where_container = array();
		$like_container = array();
		
		
		$my_followers = $this->car_model->getFollowers($this->session->userdata('vehicle_id'));
		$my_follows = $this->car_model->getMyFollows($this->session->userdata('vehicle_id'));
		
		$vehicle_ids = array();
		$vehicle_container = $this->car_model->vehicleSearchProfile($search)->result();
	
		foreach($vehicle_container as $vehicle){
			
			
			switch($vehicle->vehicle_type)
			{
				case 0:{
							$model = $this->car_model->getCarModelById($vehicle->model_id)->row();
							$make = $model->model." ".$model->model_trim;
				};break;
				
				case 1:{
							$model = $this->car_model->getMotorModelById($vehicle->model_id)->row();
							$make = $model->model;
				};break;
				
				case 2:{
							$model = $this->car_model->getTruckModelById($vehicle->model_id)->row();
							$make = $model->model." ".$model->model_trim;
				};break;
			}	
		
		
		
		
			$followers = $this->car_model->getFollowers($vehicle->id);
			
			$vehicle_rank = "rookie";
			
			$sumpoints = 0;
			$user_points = $this->statics_model->getUserPoints($vehicle->user_id);
		
			foreach($user_points as $point)
			{
				$sumpoints += $point['points'];
			}
			
			if($sumpoints > 999)
			{
				$vehicle_rank = "racer";
			}
			
			if($sumpoints > 4999)
			{
				$vehicle_rank = "champion";
			}
			
			if($sumpoints > 9999)
			{
				$vehicle_rank = "elite";
			}
			
			if($sumpoints > 24999)
			{
				$vehicle_rank = "legend";
			}

			
			
			
			$vehicleprofile = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
			
			
			if($vehicle_type == "all" || $vehicle_type == $vehicle->vehicle_type)
			{
					
				if($engine_type == "all" || $engine_type == $vehicle->engine_type)
				{
					
					
					if($car_rank == "all" || $car_rank == $vehicle_rank)
					{		
					   if($hp_ranges == "all" || $hp_ranges == $vehicle->hp_ranges)
					   {
					
					if($year_select == 0 || $year_select == $model->year)
					{
						if($make_input == '' ||  strpos(strtolower($model->make), strtolower($make_input)) !== false )
						{
							
							if($model_input == '' ||  strpos(strtolower($make), strtolower($model_input)) !== false )
							{
									
								if($plate_input == '' ||  strpos(strtolower($vehicle->plate), strtolower($plate_input)) !== false )
								{
									
									$mystring = $zerosixty_input;
									$first = strtok($mystring, '.');
									
									$mystring = $vehicle->zerosixty;
									$second = strtok($mystring, '.');
									
									
									if($zerosixty_input == '' ||  strpos(strtolower($second), strtolower($first)) !== false )
									{
											
										if($my_crew == 0)
										{
											
											if($for_sale == 0)
											{
												$vehicle_array[] = array('id' => $vehicle->id,	
														'nickname' => $vehicle->nickname,
														'pretty_url' => $vehicle->pretty_url,
														'zerosixty' => $vehicle->zerosixty,
														'followers' => count($followers),
														'model' => $model,
														'profile_picture' => $vehicleprofile);
												
												$vehicle_ids[] = $vehicle->id;
											}
											else
											{
												if($vehicle->for_sale == 1)
												{
													$vehicle_array[] = array('id' => $vehicle->id,	
													'nickname' => $vehicle->nickname,
													'pretty_url' => $vehicle->pretty_url,
													'zerosixty' => $vehicle->zerosixty,
													'followers' => count($followers),
													'model' => $model,
													'profile_picture' => $vehicleprofile);
											
													$vehicle_ids[] = $vehicle->id;
												}
											}
										}
										else
										{
											foreach($my_followers as $follower)
											{
												
												if($vehicle->id == $follower['follower_id'])
												{
														
													if(!in_array($vehicle->id, $vehicle_ids))
													{
														if($for_sale == 0)
														{
														
															$vehicle_array[] = array('id' => $vehicle->id,	
															'nickname' => $vehicle->nickname,
															'pretty_url' => $vehicle->pretty_url,
															'zerosixty' => $vehicle->zerosixty,
															'followers' => count($followers),
															'model' => $model,
															'profile_picture' => $vehicleprofile);
															
															$vehicle_ids[] = $vehicle->id;
														}
														else
														{
															if($vehicle->for_sale == 1)
															{
																$vehicle_array[] = array('id' => $vehicle->id,	
																'nickname' => $vehicle->nickname,
																'pretty_url' => $vehicle->pretty_url,
																'zerosixty' => $vehicle->zerosixty,
																'followers' => count($followers),
																'model' => $model,
																'profile_picture' => $vehicleprofile);
														
																$vehicle_ids[] = $vehicle->id;
															}
														}
														
													}	
													
												}

											}
											
											
											foreach($my_follows as $follower)
											{
												
												if($vehicle->id == $follower['vehicle_id'])
												{
													if(!in_array($vehicle->id, $vehicle_ids))
													{
														
														if($for_sale == 0)
														{
															$vehicle_array[] = array('id' => $vehicle->id,	
															'nickname' => $vehicle->nickname,
															'pretty_url' => $vehicle->pretty_url,
															'zerosixty' => $vehicle->zerosixty,
															'followers' => count($followers),
															'model' => $model,
															'profile_picture' => $vehicleprofile);
															
															$vehicle_ids[] = $vehicle->id;
														
														}
														else
														{
															if($vehicle->for_sale == 1)
															{
																$vehicle_array[] = array('id' => $vehicle->id,	
																'nickname' => $vehicle->nickname,
																'pretty_url' => $vehicle->pretty_url,
																'zerosixty' => $vehicle->zerosixty,
																'followers' => count($followers),
																'model' => $model,
																'profile_picture' => $vehicleprofile);
														
																$vehicle_ids[] = $vehicle->id;
															}
														}
													}
												}
												
											}
										}	
										
									}
								}
							}
						}
					
					}
				   }
				   }
				}
			}
									
			
		}
							
			
		
		

		$data['vehicles'] = $vehicle_array;
		$data['vehicles_count'] = count($data['vehicles']);	
		
		if($data['vehicles_count'] > 0)
		{
			return $this->load->view('statics/profile_search_result', $data);
		}
		else 
		{
			return "NO";
		}		
	}
	
	public function getDogInfo()
	{
		$dog_id = $_POST['dog'];
		
		$dog = $this->car_model->getDog($dog_id);
		
		echo json_encode($dog);
	}
	
	
	public function my_friends()
	{
		$this->checkLogin();
		$dogs = $this->car_model->getVehicles($this->session->userdata['user_id']);
		$data['random_dogs'] = array();
		$random_dogs = $this->car_model->getRandomDogs()->result();
		
		
		
		
		$dogs_data = array();
		$friends_ids_added = array();
		$friends_data = array();
		foreach($dogs->result() as $dog)
		{
			$friends_ids_added[] = 	$dog->id;
			$name = $dog->name;
			$profile_picture = $dog->profile_picture;
			$friends = $this->car_model->getFriends($dog->id);
			$enemies = $this->car_model->getEnemies($dog->id);
			$friends_total = $this->car_model->getFriendsTotal($dog->id);
			$enemies_total = $this->car_model->getEnemiesTotal($dog->id);
			
			$enemies_data = array();
			foreach($friends->result() as $friend)
			{
				$friendName = $friend->name;
				$friends_ids = array();
				$friend_friends = $this->car_model->getFriends($friend->id)->result();
				foreach($friend_friends as $ff)
				{
					
					$friends_ids[] = $ff->id;
				}
				
				
				$friendBreed = $friend->breed;	
				$friendBirthday = $friend->birthday;
				$friendPicture = $friend->profile_picture;
				if (!in_array($friend->id, $friends_ids_added))
				{
					if(is_numeric($friendPicture))
					{
						$dogprofile = $this->statics_model->getImageByID($friendPicture)->row()->fname;
					}
					else
					{
						$dogprofile = $friendPicture;
					}
					$friends_data[] = array('id' => $friend->id,
									  'name' => $friendName,
									  'pretty_url' => $friend->pretty_url,
									  'breed' => $friendBreed,
									  'birthday' => $friendBirthday,
									  'picture' => $dogprofile,
									  'friends' => $friends_ids);
									  
						$friends_ids_added[] = 	$friend->id;		  
				}					  
			}	
			
			foreach($enemies->result() as $enemy)
			{
				$enemyName = $enemy->name;
				$enemyBreed = $enemy->breed;	
				$enemyBirthday = $enemy->birthday;
				$enemyPicture = $enemy->profile_picture;
				if(is_numeric($enemyPicture))
				{
					$dogprofile = $this->statics_model->getImageByID($enemyPicture)->row()->fname;
				}
				else
				{
					$dogprofile = $enemyPicture;
				}
				
				
				
				$enemies_data[] = array('id' => $enemy->id,
								  'name' => $enemyName,
								  'pretty_url' => $enemy->pretty_url,
								  'breed' => $enemyBreed,
								  'birthday' => $enemyBirthday,
								  'picture' => $dogprofile);
			}
			
			if(is_numeric($profile_picture))
			{
				$dogprofile = $this->statics_model->getImageByID($profile_picture)->row()->fname;
			}
			else
			{
				$dogprofile = $profile_picture;
			}
			
			$dogs_data[] = array('id' => $dog->id,
								  'name' => $name,
								  'pretty_url' => $dog->pretty_url,
								  'profile_picture' => $dogprofile,
								  'friends_total' => $friends_total,
								  'enemies_total' => $enemies_total,
								  'friends' => $friends_data,
								  'enemies' => $enemies_data);
		}

		foreach($random_dogs as $random_dog)
		{
			if (!in_array($random_dog->id, $friends_ids_added))
			{
				if(is_numeric($random_dog->profile_picture))
				{
					$dogprofile = $this->statics_model->getImageByID($random_dog->profile_picture)->row()->fname;
				}
				else
				{
					$dogprofile = $random_dog->profile_picture;
				}
				$dog_data = array('id' => $random_dog->id,
								'name' => $random_dog->name,
								'pretty_url' => $random_dog->pretty_url,
								'profile_picture' => $dogprofile);
				$data['random_dogs'][] = $dog_data;
			}
			
		}

		

		$data['dogs'] = $dogs_data;
		$data['friend_dogs'] = $friends_data;
		$data['count_friends'] = count($friends_ids_added);
		$this->loadDefaultView('statics/my_friends', $data);		
	}
	
	public function my_enemies()
	{
		$this->checkLogin();
		$dogs = $this->car_model->getVehicles($this->session->userdata['user_id']);
		$data['random_dogs'] = $this->car_model->getRandomDogs()->result();
		$dogs_data = array();
		$friends_ids_added = array();
		$friends_data = array();
		foreach($dogs->result() as $dog)
		{
			$name = $dog->name;
			$profile_picture = $dog->profile_picture;
			$friends = $this->car_model->getFriends($dog->id);
			$enemies = $this->car_model->getEnemies($dog->id);
			$friends_total = $this->car_model->getFriendsTotal($dog->id);
			$enemies_total = $this->car_model->getEnemiesTotal($dog->id);
			
			$enemies_data = array();
						
			foreach($enemies->result() as $friend)
			{
				$friendName = $friend->name;
				$friends_ids = array();
				$friend_friends = $this->car_model->getFriends($friend->id)->result();
				foreach($friend_friends as $ff)
				{
					
					$friends_ids[] = $ff->id;
				}
				
				
				$friendBreed = $friend->breed;	
				$friendBirthday = $friend->birthday;
				$friendPicture = $friend->profile_picture;
				if (!in_array($friend->id, $friends_ids_added))
				{
					if(is_numeric($friendPicture))
					{
						$dogprofile = $this->statics_model->getImageByID($friendPicture)->row()->fname;
					}
					else
					{
						$dogprofile = $friendPicture;
					}
					$friends_data[] = array('id' => $friend->id,
									  'name' => $friendName,
									  'breed' => $friendBreed,
									  'birthday' => $friendBirthday,
									  'picture' => $dogprofile,
									  'friends' => $friends_ids);
									  
						$friends_ids_added[] = 	$friend->id;		  
				}
			}
			if(is_numeric($profile_picture))
			{
				$dogprofile = $this->statics_model->getImageByID($profile_picture)->row()->fname;
			}
			else
			{
				$dogprofile = $profile_picture;
			}
			$dogs_data[] = array('id' => $dog->id,
								  'name' => $name,
								  'profile_picture' => $dogprofile,
								  'friends_total' => $friends_total,
								  'enemies_total' => $enemies_total,
								  'friends' => $friends_data,
								  'enemies' => $enemies_data);
		}
		$data['dogs'] = $dogs_data;
		$data['friend_dogs'] = $friends_data;
		$data['count_friends'] = count($friends_ids_added);
		$this->loadDefaultView('statics/my_enemies', $data);		
	}
	
	public function owner_enemies($dog_id)
	{
		$this->checkLogin();
		$dogs = $this->car_model->getVehicles($this->session->userdata['user_id']);	
		$data['random_dogs'] = $this->car_model->getRandomDogs()->result();	
		$dogs_data = array();
		foreach($dogs->result() as $dog)
		{
			$name = $dog->name;
			$profile_picture = $dog->profile_picture;
			if(!$this->car_model->checkDog($dog->id,$dog_id)){	
			
				$dogs_data[] = array('id' => $dog->id,
									  'name' => $name,
									  'profile_picture' => $profile_picture
									  );
			}
		}
		$data['dogs'] = $dogs_data;
		$data['selected_dog'] = $dog_id;
		$data['dog'] = $this->car_model->getDog($dog_id);
		$data['friends'] = $this->car_model->getFriends($dog_id)->result();
		
		$data['owner'] = $this->car_model->getOwners($dog_id)->row();
		
		$data['mydog'] = false;
		if($data['owner']->id == $this->session->userdata['user_id']){
			$data['mydog'] = true;
		}
		$data['enemies'] = $this->car_model->getEnemies($dog_id)->result();
		$data['friends_total'] = $this->car_model->getFriendsTotal($dog_id);
		$data['enemies_total'] = $this->car_model->getEnemiesTotal($dog_id);
	
		$this->loadDefaultView('statics/owner_enemies', $data);		
	}
	
	
	
	public function owner_friends($dog_id)
	{
		$this->checkLogin();
		$dogs = $this->car_model->getVehicles($this->session->userdata['user_id']);	
		$data['random_dogs'] = $this->car_model->getRandomDogs()->result();	
		$dogs_data = array();
		foreach($dogs->result() as $dog)
		{
			$name = $dog->name;
			$profile_picture = $dog->profile_picture;
			if(!$this->car_model->checkDog($dog->id,$dog_id)){
					
				$dogs_data[] = array('id' => $dog->id,
									  'name' => $name,
									  'profile_picture' => $profile_picture
									  );
			}
		}
		$data['dogs'] = $dogs_data;
		$data['selected_dog'] = $dog_id;
		$data['dog'] = $this->car_model->getDog($dog_id);
		$data['friends'] = $this->car_model->getFriends($dog_id)->result();
		
		$data['owner'] = $this->car_model->getOwners($dog_id)->row();
		
		$data['mydog'] = false;
		if($data['owner']->id == $this->session->userdata['user_id']){
			$data['mydog'] = true;
		}
		$data['enemies'] = $this->car_model->getEnemies($dog_id)->result();
		$data['friends_total'] = $this->car_model->getFriendsTotal($dog_id);
		$data['enemies_total'] = $this->car_model->getEnemiesTotal($dog_id);
		$this->loadDefaultView('statics/owner_friends', $data);		
	}
	
	public function removeConnection()
	{
		$dogs = $this->car_model->getVehicles($this->session->userdata['user_id']);	
		$selected_dog_id = $_POST['selected_dog'];
		foreach($dogs->result() as $dog)
		{
			$mydog_id = $dog->id;			
			$this->car_model->deleteFriend($mydog_id, $selected_dog_id);
			$this->car_model->deleteEnemy($mydog_id, $selected_dog_id);
		}
	}
	
	
	
	
	public function addDogEnemy()
	{
		$mydog_id = $_POST['mydog'];
		$selected_dog_id = $_POST['selected_dog'];
		if(!$this->car_model->checkDog($mydog_id,$selected_dog_id)){
			$this->car_model->addDogEnemy($mydog_id,$selected_dog_id);
			$this->car_model->addDogEnemy($selected_dog_id, $mydog_id);
			$event_id = $this->statics_model->addEvent("2", $selected_dog_id);
			$this->statics_model->addEventDog($mydog_id,$event_id);
		}
				
	}
	
	
	public function deleteDog()
	{
		$dog_id = $_POST['dog_id'];
		$dogs = $this->car_model->getVehicles($this->session->userdata['user_id']);
		
		$dogs_data = array();		
		foreach($dogs->result() as $dog)
		{
			$dogs_data[] = $dog->id;
		}
		
		if(in_array($dog_id, $dogs_data)){
			$this->car_model->deleteDogProfile($dog_id);
			echo "REDIRECT";
		}
		else
		{
			echo "This is not your dog!";
		}
		
	}
	
	
	
	/* PNATION */
	
	public function getMakeByYear()
	{
		
		$year = $_POST['year'];
		$type = $_POST['type'];
		
		$data['makes'] = array();
		
		$result = $this->statics_model->getVehicleMakeByYear($year, $type);
		
		
		
		if($result)
		{
			foreach($result->result() as $vehicle)
			{
				$item = $vehicle->make;
				$data['makes'][] = $item;
			}
			
			asort($data['makes']);
			$theView = $this->load->view('statics/make_selector', $data, true);
			
			echo $theView;
			
		}
		else
		{
			echo "FAIL";
		}
		
		
	}
	
	
	public function getModel()
	{
		
		$year = $_POST['year'];
		$type = $_POST['type'];
		$make = $_POST['make'];
		
		$data['models'] = array();
		
	
		
		$result = $this->statics_model->getVehicleModelByYearMake($year, $type, $make);
		
		
		if($result)
		{
			foreach($result->result() as $vehicle)
			{
				$item = array('id' => $vehicle->id,
								'model' => $vehicle->model,			
								'desc' => $vehicle->model_trim);
				
				
					$data['models'][] = $item;
						
				
			}
			
			asort($data['models']['model']);
			$theView = $this->load->view('statics/model_selector', $data, true);
			
			echo $theView;
			
		}
		else
		{
			echo "FAIL";
		}
			
	}
	
	
	public function select_vehicle($vehicle_id)
	{
		
		$this->session->set_userdata('vehicle_id', $vehicle_id);
		
		redirect('news');
		
	}
	
	public function add_story_piece()
	{
		$story  = $_POST['story'];
		$vehicle_id = $_POST['cid'];
		
		$data = array('vehicle_id' => $vehicle_id,
					'story' => $story);
		
		
		
		
		$story_id = $this->car_model->addStoryPiece($data);
		
		$event_id = $this->statics_model->addEvent(2, $vehicle_id, $story_id);		
		$this->statics_model->addEventVehicle($vehicle_id,$event_id);
		
		echo $story_id;
	}
	
	
	public function followVehicle()
	{
		$this->checkLogin();
		$vehicle_id = $_POST['cid'];
		$my_vehicle_id = $this->session->userdata('vehicle_id');
		if(!$this->car_model->checkVehicle($vehicle_id,$my_vehicle_id)){
		
			$data = array(
					'vehicle_id' => $vehicle_id,
					'follower_id' => $my_vehicle_id);
		
			
			$this->car_model->followVehicle($data);
			
			
			//ADD EVENT FOR NEWSFEED
			$event_id = $this->statics_model->addEvent("10", $my_vehicle_id, $vehicle_id);
			$this->statics_model->addEventVehicle($vehicle_id,$event_id);
			//$this->statics_model->addEventVehicle($my_vehicle_id,$event_id);
			
			
			//ADD GAME POINTS
			$this->addPoints($vehicle_id,5,'followers');
			
			
			
		}
				
	}
	
	
	public function unFollowVehicle()
	{
		$this->checkLogin();
		$vehicle_id = $_POST['cid'];
		$my_vehicle_id = $this->session->userdata('vehicle_id');
		if($this->car_model->checkVehicle($vehicle_id,$my_vehicle_id)){
		
			$data = array(
					'vehicle_id' => $vehicle_id,
					'follower_id' => $my_vehicle_id);
		
			
			$this->car_model->removeFollowVehicle($data);
			
			
		}
				
	}
	
	
	public function addComment()
	{
		$this->checkLogin();
		$comment  = $_POST['comment'];
		$vehicle_id = $_POST['cid'];
		
		$data = array('vehicle_id' => $vehicle_id,
					'user_id' => $this->session->userdata('user_id'),
					'comment' => $comment);
		
		
		
		
		$this->car_model->addComment($data);
		
		$event_id = $this->statics_model->addNotification(4, $vehicle_id, $this->session->userdata('user_id'));		
		$this->statics_model->addNotificationVehicle($vehicle_id,$event_id); 
	}

	
	
	
	
	
}



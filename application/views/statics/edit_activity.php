<script>	
	$('#sidemenu').css({top: 0});
</script>
<div id="content">
	<a href="<?= site_url('activity/'.$activity_id);?>">
		<div id="edit_activity_button" style="right:700px;width:80px;"><?= $this->lang->line('user_album_back_button')?></div>
	</a>
	<div class="statics_subheader" style="margin-top:35px;">
		<?= $this->lang->line('edit_activity')?>
	</div>

	
	
	<div style="overflow:hidden;">
		<?php 
			$data = array('id' => 'edit_activity_form');
			echo form_open("statics/update_activity/".$activity_id, $data);?>
		<div id="album_hl"><?= $this->lang->line('new_activity')?></div><br/>
		
		<input style="margin-top:0px;height:38px;width:55px;" type="text" id="register_birthday_month" name="register_birthday_month" 
			value="<?= $month == NULL ? $this->lang->line('register_birthday_month') : $month ?>"  
			defaultText="<?= $this->lang->line('register_birthday_month')?>" maxlength="2" class="date_short <?php if(!isset($birthday_month_repop) || $lastname_repop == "") echo "input_default";?>">		
		<input style="margin-top:0px;height:38px;width:55px;" type="text" id="register_birthday_day" name="register_birthday_day" 
			value="<?= $day == NULL ? $this->lang->line('register_birthday_day') : $day ?>"
			defaultText="<?= $this->lang->line('register_birthday_day')?>" maxlength="2" class="date_short <?php if(!isset($birthday_day_repop) || $lastname_repop == "") echo "input_default";?>">	
		<input style="margin-top:0px;height:38px;width:89px;" type="text" id="register_birthday_year" name="register_birthday_year" 
			value="<?= $year == NULL ? $this->lang->line('register_birthday_year') : $year ?>" 
			defaultText="<?= $this->lang->line('register_birthday_year')?>" maxlength="4" class="date_long <?php if(!isset($birthday_year_repop) || $lastname_repop == "") echo "input_default";?>">					
						
		<!--<input type="text" id="activity_date" name="activity_date" placeholder="<?= $this->lang->line('new_activity_date')?>" class="edit_input" style="margin-top:0px;font-size:1.2em;"> -->
		<input type="text" id="activity_time" name="activity_time" placeholder="<?= $this->lang->line('new_activity_time')?>" class="suggest_input" style="" value="<?= $time;?>">
		<input type="text" id="activity_place" name="activity_place" placeholder="<?= $this->lang->line('new_activity_place')?>" class="suggest_input" style="" value="<?= $place;?>">
		<textarea type="text" id="activity_about" name="activity_about" placeholder="<?= $this->lang->line('new_activity_about')?>" class="suggest_input" style="height:81px;resize:none;"><?= $about;?></textarea>
		<div id="add_album_btn" style="margin-top:44px;">
			<div id="edit_activity" style="top: 0px;width: 220px;" class="album_arrow_text"><?= $this->lang->line('edit_activity_button')?></div>
		</div> 
		<?php echo form_close();?>
	</div>
	<div id="edit_error_message" style="position: relative;">
		<?php if(isset($message)) echo $message;?>
	</div>
	
	<div style="position:relative;">
		<div class="statics_subheader" style="margin-top:5px;">
			<?= $this->lang->line('activity_guests')?>		
		</div>
		<div id="add_guests_btn" style="position: absolute;top: 21px;">
			<div id="show_friends" style="top:-5px;" class="album_arrow_text"><?= $this->lang->line('add_activity_guest')?></div>
		</div> 
	</div>
	
	<div id="friends_holder">
		<div id="close_overlay">X</div>
		<div class="statics_subheader" style="height:30px;line-height:30px;background:none;color:#d4003d;">
			<?= $this->lang->line('dog_friends')?>		
		</div>
		<?
		if($friends){
			foreach($friends as $friend){?>								
				<div class="friends_item" activity_id="<?= $activity_id;?>" dogId="<?= $friend['id'];?>">											
					<div class="div_white_over"></div>
					<img class="friends_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $friend['picture'])?>" border="0">						
					<div class="friends_list_item_desc">
						<?= $friend['name'];?>
					</div>
				</div>
				
				
		<? }
		}?>
	</div>
	
	<div id="guests_holder">
	<?
		if($guests){ $x = 1;
			foreach($guests as $guest){?>
						<a href="<?= site_url('dog/'.$guest->id);?>">
							<div class="nearby_list_item" <? if($x%3 == 0){ echo "style='margin-right:0px'";}?> >
								<img class="vets_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $guest->profile_picture)?>" border="0">
								<div class="div_white_over"></div>
								<div class="zones_list_item_desc" style="top:90px;">
									<?= $guest->name;?> <? if($guest->pending == 1) echo " (Pending)";?>
								</div>
							</div>			
					 </a>
				<? 
		$x++; }
		}?>
	</div>	
	<br>
	
</div>
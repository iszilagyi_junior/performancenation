<script>	
	
	$("meta[property='og\\:image']").attr("content", "<?= site_url('items/uploads/profilepictures/' . $group->logo);?>");
	
	$(document).ready(function()
	{	
		
		setTimeout(function(){
			initGroupPkry();
			var event = "<?= $event_geo;?>";
	
			initMapEvent(event);
		},200);
	});

	
</script>
<? if(!$is_ie){?>
	<div id="image_cropping_overlay">
		<div class="" id="crop_title" style="position:relative;top:0px;text-align: center;font-size:22px;">Cover Picture</div>
		<div class="sansitalic" style="margin:5px auto;text-align: center;">Please select an area</div>
		<div id="image_crop_holder">
			
		</div>
		 <input type="hidden" id="selection_x1" name="selection_x1" value="" />
		 <input type="hidden" id="selection_x2" name="selection_x2" value="" />
		 <input type="hidden" id="selection_y1" name="selection_y1" value="" />
		 <input type="hidden" id="selection_y2" name="selection_y2" value="" />
		<div class="button" type="group" eid="<?= $event->id;?>" id="finish_crop_event" image_type="" style="left:255px;position: absolute;">FINISH</div>
		<div class="button" id="close_crop"  style="left:545px;position: absolute;" >CLOSE</div>
	</div>
<? } ?>
<div id="content" style="width:800px;margin-bottom: 100px;color:#ffffff;">

	<div id="BkgEr" style=""></div>
			<? if($event->image != NULL && $event->image != ''){?>
				<img class="car_cover_photo" style="z-index:10000;" src="<?= site_url('items/uploads/coverpictures/'.$event->image);?>" border="0">				
			<? }
				else
				{
			?>
				<div id="event_map"></div>
			<? }?>
		<!--	<div id="show_comments" cid="<?= $car->id;?>" class="button" style="">COMMENTS</div>
			<div id="show_story" cid="<?= $car->id;?>" class="button" style="">STORY</div> -->
			<?  if($im_admin){?>
				
				<script>	
	
					$(document).ready(function()
					{	
						$('#group_event_date').datetimepicker();
					});
					
				</script>
				
				
				
				
				
				<div id="group_event_overlay">
					<div class="overlayBG"></div>
					<div style="position:relative;z-index:100">
						<img class="close_overlay" style="top:0px" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />
						<div class="titboldit" style="font-size:27px;margin-top:30px;">CREATE EVENT</div>
						<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">
								<table style="width:400px;margin:30px auto;">
									<tr>
										<td style="regular">
											Title
										</td>
										<td>
											<input type="text" placeholder="Title" class="edit_input" name="group_event_title" id="group_event_title" value="<?= $event->title; ?>"></input>
										</td>
									</tr> 
									
									<tr>
										<td style="regular">
											Address
										</td>
										<td>
											<input type="text" placeholder="Address" class="edit_input" name="group_event_address" id="group_event_address" value="<?= $event->address; ?>"></input>
										</td>
									</tr> 
									
									<tr>
										<td style="regular">
											Description
										</td>
										<td>
											<textarea  placeholder="Description" class="edit_input" name="group_event_description" id="group_event_description" style="resize:none;height:60px;"><?= $event->description;?></textarea>
										</td>
									</tr>
									
									<tr>
										<td style="regular">
											Date
										</td>
										<td>
											<input type="text" placeholder="Date" class="edit_input" name="group_event_date" id="group_event_date" value="<?= $event->start_time;?>"></input>
										</td>
									</tr>
								</table>
							<div class="relative_parent" style="width:400px;margin:0px auto;">
								<div class="label_edit" style="line-height:60px;">Public event?</div>
								<input type="radio" name="group_public" style="margin-top:25px;" class="" value="1" <? if($event->public == 1) echo 'checked="checked"'?>>YES &nbsp;&nbsp;
								<input type="radio" name="group_public" style="" class="" value="0" <? if($event->public == 0) echo 'checked="checked"'?>>NO 		
							</div>
							<input type="file" preview="edit_group_cover_upload" accept="image/*" id="edit_user_cover_picture" name="edit_user_cover_picture" class="edit_hidden_upload">
							<div class="button" id="user_event_image" eid="<?= $event->id;?>">UPLOAD COVER</div>
						</div>
						<div class="button" id="edit_group_event_button" eid="<?= $event->id;?>">SAVE</div>
					</div>
					<div class="regular" id="group_event_error_message"></div>
					
					<div id="vehicle_delete_holder" style="margin:5px auto;left:2px;position:relative">					
						<div id="vehicle_delete" class="button" style="margin:5px 20px;">DELETE EVENT</div>
						<div id="vehicle_delete_confirm">
							Are you sure you wish to delete this event?<br/>
							<div id="groupevent_delete_yes" eid="<?= $event->id;?>" class="button" style="width:80px;float: left;margin-left: 20px;">YES</div>
							<div id="vehicle_delete_no" class="button" style="width:80px;float:right;margin-right:20px;">NO</div>
						</div>
					</div>
				</div>
				
			
				
				<div class="button clone_group_event" eid="<?= $event->id;?>" style="">CLONE</div>
				<div class="button edit_event" style="">EDIT</div>
				
				
				
			
			<? }
				else{
				?>
					<div id="going_event" vid="<?= $my_vehicle_id;?>" cid="<?= $event->id;?>" class="button event_decision" status="<?= GROUP_EVENT_GOING;?>" style="<? if($joined_event){ echo 'display:none;';}?>">Join Event</div>
				<!--	<div id="maybe_event" vid="<?= $my_vehicle_id;?>" cid="<?= $event->id;?>" class="button event_decision" status="<?= GROUP_EVENT_MAYBE;?>" style="<? if($decided){ echo '';}?>">Maybe</div> -->
					<div id="not_going_event" vid="<?= $my_vehicle_id;?>" cid="<?= $event->id;?>" class="button event_decision" status="<?= GROUP_EVENT_NOT;?>" style="display:none;<? if($joined_event){ echo 'display:block';}?>">Leave Event</div>
			<? }?>	
				
				
				<br/><br/>
				
				<div class="car_detail_nickname playbold" style=""><?= $event->title;?></div>
				<div class="car_detail_make regular"><?= $event->address;?></div>
				<div class="car_detail_owner dosissemi"><?= date('F j, Y, \a\t g:i a',strtotime($event->start_time));?></div>		
				
				<div style="overflow:hidden;margin-top:30px;min-height:400px;">
					<div class="detail_left">
						<div class="user_section" style="width:100%;">
							<div class="dosisextralight"><?= nl2br($event->description);?></div>
						</div>
					</div>
					
					<div class="detail_right">
						<div class="dosissemi">PARTICIPANTS</div>
						<div id="gallery_images">
							<? foreach($vehicles_joined as $member):?>
								<a href="<?= site_url('vehicle_profile/'.$member['pretty_url']);?>">
									<div class="member_item">
										<img class="member_item_img" src="<?= site_url('items/uploads/profilepictures/'.$member['profile_image']);?>" />
									<!--	<div class="member_item_title"><?= $member->username;?></div> -->
									</div>
									
								</a>
							<? endforeach;?>
						</div>
						
						<? if($event->image != NULL && $event->image != ''){?>
							<div id="event_map" style="width:360px;height:280px;margin-top:20px;"></div>
						<? }?>
					</div>
					<br/>
				</div>	
					
				
</div>
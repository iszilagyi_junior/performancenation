<script>
	$('#sidemenu').css({top:35});
</script>
			<div id="content" class="printable">
			<div id="BkgEr" style=""></div>	
			<? if($dog->lost == 0){?>
				<div><?= $this->lang->line('dog_not_lost');?></div>
			<? }
				else{
			?>			
				<div id="lost_detail_header_overlay" style="position:relative;top:35px;">
					<?= $this->lang->line('dog_lost');?>
				</div>
				<div id="lost_detail_header">
					<div id="lost_detail_header_cover">
						<?if($dogprofile!=""){?>
							<img class="dog_cover_photo" src="<?= site_url('items/uploads/profilepictures/' . $dogprofile)?>" border="0">
						<? }?>
						
						
					</div>
				</div>			
				<div id="vets_details" style="min-height:400px;">
					<div id="edit_left" style="width:310px;">
						<div class="overview_left" class="statics_half">
							<div class="overview_header" style="background:#d4003d;"><?= $this->lang->line('dog_lost_info');?></div>
							<div id="dog_details_lost">							
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_new_name');?></div>
									<div class="detail_line"><?= $dog->name;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_new_nickname');?></div>
									<div class="detail_line"><?= $dog->nickname;?></div>
								</div>						
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_new_breed');?></div>
									<div class="detail_line"><?= $dog->breed_name;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_gender');?></div>
									<div class="detail_line"><? if($dog->gender == 0){ echo "Male";}else{ echo "Female";}?></div>
								</div>														
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_color');?></div>
									<div class="detail_line"><?= $dog->color;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_coat');?></div>
									<div class="detail_line"><?= $dog->coat;?></div>
								</div>	
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_type');?></div>
									<div class="detail_line"><?= $dog->type;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_size');?></div>
									<div class="detail_line"><?= $dog->size;?></div>
								</div>										
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_characteristics');?></div>
									<div class="detail_line"><?= $dog->characteristics;?></div>
								</div>											
							</div>
						</div>						
					</div>	
					<div id="edit_right" style="width:300px;left:310px;">
						<div class="overview_right statics_half" style="width:310px">
							<div class="overview_header" style="background:#d4003d;"><?= $this->lang->line('dog_lost_contact');?></div>
							<div id="dog_details_lost">
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_owner');?></div>
									<div class="detail_line"><?= $owner->first_name." ".$owner->last_name;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('register_phone');?></div>
									<div class="detail_line"><?= $owner->phone;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('register_email');?></div>
									<div class="detail_line"><?= $owner->email;?></div>
								</div>																	
							</div>
						</div>
					</div>						
				
				<br/><br/>
				</div>
				
			</div>
			<div id="print_button"><?= $this->lang->line('print');?></div>
			
			<? }?>
			
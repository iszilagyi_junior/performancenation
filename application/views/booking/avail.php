
		
		<div class="avail_overlay calendar_overlay">
			<img class="overlay_arrow" src="<?= site_url('items/frontend/img/popup_arrow.png')?>"/>
			<div class="overlay_close">X</div>
			<div class="popup_title_text">create availability<br/>
				<span style="color:#b4b4b4;font-size:13px;"><?= $weekday ?>, <?= date("F j, Y", strtotime($date));?></span>
			</div>
			<form id="avail_overlay_form">
				<input type="hidden" name="avail_overlay_date" value="<?= $date?>">

				
				<br>
				<div class="dognum_holder">
					<label class="light_popup_text" for="fe3">number of dogs</label>
					<input type="text" name="avail_overlay_dogcount" id="fe3" class="popup_input">
				</div>
				
				<div class="popup_repeat_holder">
					<div>
						<img class="popup_select_full" src="<?= site_url('items/frontend/img/popup_select_full.png')?>"/>
						<img class="popup_select_empty" fe="1" style="display:none;" src="<?= site_url('items/frontend/img/popup_select_empty.png')?>"/>
						<input type="radio" name="avail_overlay_recurring" checked value="recurring" id="fe1" style="display:none;">
						<label class="popup_text" for="fe1">Repeat every <?= $weekday ?></label>
					</div>
					
					<br>
					<div>
						<img class="popup_select_full" style="display:none;" src="<?= site_url('items/frontend/img/popup_select_full.png')?>"/>
						<img class="popup_select_empty" fe="2" src="<?= site_url('items/frontend/img/popup_select_empty.png')?>"/>
						<input type="radio" name="avail_overlay_recurring" value="date_specific" id="fe2" style="display:none;">
						<label class="popup_text" for="fe1">Only this day</label>
					</div>
					
				</div>
				
				<input type="submit" name="avail_overlay_save" value="save" class="popup_button">
			</form>
		</div>
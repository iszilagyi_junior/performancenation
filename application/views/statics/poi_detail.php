
<script>	

	$(document).ready(function()
	{	
		var geo_loc = "<?= $geo_location;?>";
		setTimeout(function(){
			initMapPOIDetail(geo_loc);			
		},200);
		
		var poi_id = "<?= $poi_id;?>";
		poi_file_upload(poi_id);
	});
	
</script>
<div id="content" style="margin-bottom: 280px;color:#ffffff;min-height:600px;">

	<div id="BkgEr" style=""></div>
				<img class="" style="position:absolute;left:0px;top:0px;" src="<?= site_url('items/frontend/img/big_chrome.png')?>" border="0">
				<div class="playbold gold page_title" style="text-align:center;font-size:40px;letter-spacing:1px;z-index:10;position:relative;top:10px;"><?= $poi->name;?></div>
				<!--
				<div class="map_navi">
					<input type="text" class="edit_input" id="navi_start" name="navi_start" placeholder="Start" />
					<input type="text" class="edit_input" id="navi_end" name="navi_end" placeholder="End" />
				</div>
				-->
				<a target="_blank" href="http://maps.google.com/?q=<?= $poi->address;?>">
					<div id="poi_map"></div>
				</a>
				
				
				<div style="overflow:hidden;margin-top:30px;">
					
					<div class="detail_left" style="min-height:300px;">
						
						<div class="user_section" style="width:100%;">
							<div class="dosissemi" style="font-size:18px;">Address</div>
							<div class="dosisextralight"><?= $poi->address;?></div>
						</div>
						<br/>

						
						
						<div class="user_section" style="width:100%;">
							<div class="dosissemi" style="font-size:18px;">Description</div>
							<div class="dosisextralight"><?= $poi->description;?></div>
						</div>
						<br/>

					</div>
					
					
					<div class="detail_right" style="width:225px;">
						<div id="notIE9" style="">
							<div id="add_photo_btn_poi" class="button">UPLOAD A PICTURE</div>
							<input type="file" preview="add_photo_btn_poi" accept="image/*" id="add_photo_upload_btn" name="add_photo_upload_btn" style="display:none;">
						</div>
						<div id="IE9" style="display:none;">
							<div class="button">UPLOAD A PICTURE</div>
							<?php 
							$data = array('id' => 'add_photo_ie');
							echo form_open_multipart("statics/add_photo_newsfeed_ie", $data);?>
								<input type="file" preview="add_photo_btn_newsfeed" accept="image/*" id="add_photo_upload_btn_ie" name="myfile" style="">
							<? echo form_close();?>
						</div>
					</div>
					
					
					<div class="detail_right" style="width:275px;">
						
						
						<div class="dosissemi" style="font-size:18px;text-align:center;">RATING</div>
						<div class="regular" style="font-size:12px;">
							<? for($i = 1; $i<= $ratingSum; $i++){?>
								<img class="circle_half"  src="<?= site_url('items/frontend/img/star_full.png')?>" border="0">							
							<? }
								
								if((int) $ratingSum != $ratingSum)
								{
									
							?>
								<img class="circle_half"  src="<?= site_url('items/frontend/img/star_half.png')?>" border="0">
							<? }?>
							
							
						</div>
						<div id="ratingHolder">
							
							<? if(!$already_rated){?>
								<div class="rate"  rating="1" poi="<?= $poi->id;?>">
									<img class="circle_empty" id="empty_circle_1" src="<?= site_url('items/frontend/img/star_empty.png')?>" border="0">
									<img class="circle_full" id="full_circle_1" src="<?= site_url('items/frontend/img/star_full.png')?>" border="0">
								</div>
								<div class="rate" rating="2" poi="<?= $poi->id;?>">
									<img class="circle_empty" id="empty_circle_2" src="<?= site_url('items/frontend/img/star_empty.png')?>" border="0">
									<img class="circle_full" id="full_circle_2" src="<?= site_url('items/frontend/img/star_full.png')?>" border="0">
								</div>
								<div class="rate"  rating="3" poi="<?= $poi->id;?>">
									<img class="circle_empty" id="empty_circle_3" src="<?= site_url('items/frontend/img/star_empty.png')?>" border="0">
									<img class="circle_full" id="full_circle_3" src="<?= site_url('items/frontend/img/star_full.png')?>" border="0">
								</div>
								<div class="rate" rating="4" poi="<?= $poi->id;?>">
									<img class="circle_empty" id="empty_circle_4" src="<?= site_url('items/frontend/img/star_empty.png')?>" border="0">
									<img class="circle_full" id="full_circle_4" src="<?= site_url('items/frontend/img/star_full.png')?>" border="0">
								</div>
								<div class="rate" rating="5" poi="<?= $poi->id;?>">
									<img class="circle_empty" id="empty_circle_5" src="<?= site_url('items/frontend/img/star_empty.png')?>" border="0">
									<img class="circle_full" id="full_circle_5" src="<?= site_url('items/frontend/img/star_full.png')?>" border="0">
								</div>
							<? }
							else
							{ ?>
								<div class="regular" style="font-size:12px;">You already gave a rating!</div>
							<? } ?>
						</div>
					</div>
					<br/><br/><br/>
					<div class="detail_right" style="width:275px;">
						
						
						<div class="dosissemi" style="font-size:18px;text-align:center;">IMAGES</div>
						
						<div id="gallery_images" style="margin-left:0px;">
							<? foreach($images as $album):?>
								
								<a href="<?= site_url('items/uploads/images/'.$album['fname']);?>" data-lightbox="gallery">
									<div class="album_teaser_item">
										<div class="album_item_image" style="background-image: url('<?= site_url('items/uploads/images/'.$album['fname']);?>')"></div>
										
									</div>
								</a>
								
								
							<? endforeach;?>
						</div>
					</div>
					
					
					
					
					
					
					
					
					
					
					
				</div>
				
</div>
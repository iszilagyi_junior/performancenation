<script>
	var zone_geo_location = "<?= $zone->geo_location?>";
	$('#sidemenu').css({top: 35});
</script>
<style>
	.gm-style-cc { display:none; }
</style>
<script src="<?= site_url("items/frontend/js/detail_map.js"); ?>" type="text/javascript"></script>
			<div id="content">
				<div id="vet_detail_header">
					<div id="zone_map_canvas">
					
					</div>
					<div class="statics_subheader">
						<?= $zone->name?>
					</div>				
				</div>
				<div id="vets_details">
					<div id="statics_left" class="statics_half">
						<div id="vet_details_desc">
							<p class="vets_details_subheader"><?= $this->lang->line('zone_detail_desc')?></p>
							<? if($zone->description == "")
								{?>
								<div id="description_suggestion_holder">
									<div class="sansregular" id="zone_desc_suggestion_text"><?= $this->lang->line('suggest_zone_description')?></div>
									<textarea name="zone_description_suggestion" id="zone_description_suggestion" style="resize:none; width: 390px;margin:0px;padding:0px;height: 200px;"></textarea>
									<div id="submit_zone_description" zone_id="<?= $zone->id; ?>"><?= $this->lang->line('submit')?></div>
								</div>
							<? }
								else
								{
								echo $zone->description;
								}
							?>
							
						</div>
					</div>
					<div id="statics_right" class="statics_half">
						<div id="vet_details_contact">
							<p class="vets_details_subheader"><?= $this->lang->line('zone_detail_address')?></p>
							<?= $zone->address?><br/><br/>
							<?= $this->lang->line('zone_detail_size')?> <?= $zone->size?> 
						</div>					
					</div>
					
					<? if($this->ion_auth->logged_in()){?>
					
					
				<!--	<div id="statics_right" class="statics_half">					
						<div id="vet_details_times">
							<p class="vets_details_subheader"><?= $this->lang->line('zone_detail_friend')?></p>
							
							
						</div>
					</div>-->
					
					<div id="statics_right" class="statics_half">
						<div id="sitter_details_rating">
							<p class="vets_details_subheader">
								<?= $this->lang->line('sitter_detail_rating')?> 
							</p>
							<div class="sansItalic"><?= $ratingSum;?> <?= $this->lang->line('points_from')?>  <?= $number_of_votes;?> <?= $this->lang->line('users')?> </div> 
							<div id="rating_holder">
								
								<? if(!$already_rated){?>
								<p class="vets_details_subheader"><?= $this->lang->line('sitter_detail_rate')?></p>
									<div class="rate" who="dogzone" rating="1" sitter="<?= $zone->id;?>">
										<img class="circle_empty" id="empty_circle_1" src="<?= site_url('items/frontend/img/circle_empty.png')?>" border="0">
										<img class="circle_full" id="full_circle_1" src="<?= site_url('items/frontend/img/circle_full.png')?>" border="0">
									</div>
									<div class="rate" who="dogzone" rating="2" sitter="<?= $zone->id;?>">
										<img class="circle_empty" id="empty_circle_2" src="<?= site_url('items/frontend/img/circle_empty.png')?>" border="0">
										<img class="circle_full" id="full_circle_2" src="<?= site_url('items/frontend/img/circle_full.png')?>" border="0">
									</div>
									<div class="rate" who="dogzone" rating="3" sitter="<?= $zone->id;?>">
										<img class="circle_empty" id="empty_circle_3" src="<?= site_url('items/frontend/img/circle_empty.png')?>" border="0">
										<img class="circle_full" id="full_circle_3" src="<?= site_url('items/frontend/img/circle_full.png')?>" border="0">
									</div>
									<div class="rate" who="dogzone" rating="4" sitter="<?= $zone->id;?>">
										<img class="circle_empty" id="empty_circle_4" src="<?= site_url('items/frontend/img/circle_empty.png')?>" border="0">
										<img class="circle_full" id="full_circle_4" src="<?= site_url('items/frontend/img/circle_full.png')?>" border="0">
									</div>
									<div class="rate" who="dogzone" rating="5" sitter="<?= $zone->id;?>">
										<img class="circle_empty" id="empty_circle_5" src="<?= site_url('items/frontend/img/circle_empty.png')?>" border="0">
										<img class="circle_full" id="full_circle_5" src="<?= site_url('items/frontend/img/circle_full.png')?>" border="0">
									</div>
								<? }
								else{?>
									<br/>
									<div><?= $this->lang->line('zone_detail_already_rated')?> </div>
									<br/>
									<?
										for($i=1;$i <= 5; $i++)
										{
											
											if(intval($i) <= intval($ratingSum))
											{ 
											?>
												<img style="display:inline;" class="circle_full" src="<?= site_url('items/frontend/img/circle_full.png')?>" border="0">
										<?	}
											else
											{ ?>
												<img class="circle_empty"  src="<?= site_url('items/frontend/img/circle_empty.png')?>" border="0">
										<?	}
										}
										
									?>
									
								<? }?>
							</div>
						</div>
					</div>
					<? }?>
				<!--	<div id="statics_right" class="statics_half">
						<div id="vet_details_gallery">
							<p class="vets_details_subheader"><?= $this->lang->line('vet_detail_gallery')?></p>
							
							<?php $i = 0; ?>
													
						    <?php foreach ($gallery->result() as $image): ?>
						    <div class="vet_details_gallery_item <?php if($i < 3) echo "gallery_item_active"?>" gallery_id="<?=$i++?>" >
						        <a href="<?=site_url("items/uploads/images/" . $image->fname); ?>" data-lightbox="image-1" >
						        	<img src="<?=site_url("items/uploads/images/" . $image->fname); ?>" width="100%" border="0">
						    	</a>
						    </div>
						    
						    <?php endforeach; ?>
				    		<script type="text/javascript">
								var gallery_item_count = <?= $i ?>;
							</script>
							<div class="gallery_rotate_left gallery_rotator">
						    	<
						    </div>
							<div class="gallery_rotate_right gallery_rotator">
						    	>
						    </div>						    							
						</div>
					</div>					
				</div>-->
			</div>
		</div>
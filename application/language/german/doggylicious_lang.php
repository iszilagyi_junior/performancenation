<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['gender_male'] = 'Männlich';
$lang['gender_female'] = 'Weiblich';
$lang['years'] = 'Jahr';
$lang['months'] = 'Monat';
$lang['favorite'] = 'favorite';
$lang['favorite'] = 'unfavorite';


$lang['about_snoopstr'] = 'Über snoopstr';

/*********************************************************************************
 * FRONTEND
 ***********************************************************************************/
$lang['sidemenu_login_header'] = 'Einloggen';
$lang['sidemenu_login_username'] = 'Email';
$lang['sidemenu_login_password'] = 'Passwort';
$lang['sidemenu_login_confirm'] = 'Anmelden';
$lang['sidemenu_facebook_header'] = 'auf Facebook?';
$lang['sidemenu_facebook_confirm'] = 'Facebook-LOGIN';
$lang['sidemenu_register_header'] = 'neu hier?';
$lang['sidemenu_register_confirm'] = 'JETZT REGISTRIEREN';
$lang['sidemenu_static_breed'] = 'RASSEN';
$lang['sidemenu_static_dogzone'] = 'hundezonen';
$lang['sidemenu_static_vet'] = 'TIERÄRZTE';
$lang['sidemenu_static_tips'] = 'tipps';
$lang['sidemenu_static_sitters'] = 'HUNDESITTER';
$lang['sidemenu_static_misc'] = 'PLÄTZE';

$lang['sidemenu_footer_contact'] = 'Kontakt';
$lang['sidemenu_footer_impressum'] = 'Impressum';
$lang['sidemenu_member_greeting'] = 'Eingeloggt als';
$lang['sidemenu_member_area'] = 'Territorium';
$lang['sidemenu_member_myprofile'] = 'Profil';
$lang['sidemenu_member_logout'] = 'ausloggen';
$lang['sidemenu_member_mydogs'] = ' deine Hunde';
$lang['sidemenu_member_myphotos'] = 'fotos';
$lang['sidemenu_member_myfriends'] = 'hundefreunde';
$lang['sidemenu_member_myenemies'] = 'hundefeinde';
$lang['sidemenu_member_search'] = 'hundesuche';
$lang['sidemenu_member_invite'] = 'einladen';
$lang['sidemenu_member_activities'] = 'Aktivitäten';
$lang['sidemenu_member_news'] = 'Nachrichtenübersicht';

$lang['login_login_username'] = 'Email';
$lang['login_login_password'] = 'Passwort';
$lang['login_login_confirm'] = 'einloggen';
$lang['login_header'] = 'einloggen';

$lang['register_top_header'] = 'Registrierung';
$lang['register_email'] = 'Email';
$lang['register_password'] = 'Passwort';
$lang['register_passconf'] = 'Passwort wiederholen';
$lang['register_firstname'] = 'Vorname';
$lang['register_lastname'] = 'Nachname';
$lang['register_pw_length'] = 'Mindestens 8 Zeichen';
$lang['register_birthday'] = 'Geburtstag (TT.MM.JJJJ)';
$lang['register_birthday_day'] = 'TT';
$lang['register_birthday_month'] = 'MM';
$lang['register_birthday_year'] = 'JJJJ';
$lang['register_terms'] = 'Geschäftsbedingungen akzeptieren';
$lang['register_newsletter'] = 'Bitte sende mir den Newsletter zu.';
$lang['register_confirm'] = 'Register';
$lang['register_header'] = 'Werde ein Teil <br/>der Gang ';
$lang['register_success'] = 'Registrierung erfolgreich';
$lang['register_success_text'] = 'Du kannst dich nun mit deiner E-Mail-Adresse und deinem Passwort einloggen.';
$lang['register_state'] = 'Land';
$lang['register_zip'] = 'PLZ';
$lang['register_city'] = 'Stadt';
$lang['register_address'] = 'Adresse';
$lang['register_availability'] = 'Verfügbarkeit';
$lang['register_phone'] = 'Telefon';
$lang['register_fb'] = 'Facebook';
$lang['register_tw'] = 'Twitter';
$lang['register_insta'] = 'Instagram';
$lang['instagram_hashtag'] = 'Dein Hashtag';
$lang['register_or'] = 'oder';
$lang['register_teaser'] = 'Richte Profile für deine Hunde ein,<br/> finde Freunde und plane Aktivitäten.';

$lang['edit_user_pw_confirm'] = "Passwort ändern";
$lang['edit_user_confirm'] = "Änderungen speichern";
$lang['edit_user_pw_old'] = "Altes Passwort";
$lang['edit_user_header'] = "Profil bearbeiten";
$lang['edit_user_pw_change_success'] = 'Passwort erfolgreich geändert';
$lang['edit_user_profile_change'] = 'Profil geändert';
$lang['edit_user_dogsitter_checkbox'] = "Profil als Hundesitter anzeigen";
$lang['edit_user_dogsitter_checkbox_deactive'] = "Profil als Hundesitter deaktivieren";
$lang['edit_user_hashtag_checkbox'] = "Zeige Bilder mit Hashtags";
$lang['edit_user_show_email'] = "Zeige meine E-mail adresse an";
$lang['edit_user_import_instagram'] = "Erlaube die automatische Übertragung von Bildern mit Hashtags aus Instagram";
$lang['edit_user_import_instagram_description'] = "Um Instagram Fotos in dein snoopstr Album zu übertragen, verwende den unten angegebenen Hashtag in deinen Instagram Postings. Die Bilder werden in deinem Album “Instagram Fotos” angezeigt:";
$lang['edit_user_select_country'] = "Bitte wähle ein Land aus";
$lang['edit_user_available_text'] = "Über:";
$lang['edit_user_contact'] = "Kontakt:";
$lang['edit_user_contact_text'] = "Gib deine Kontaktdaten an, damit dich andere Hundebesitzer kontaktieren können.";

$lang['home_most_recent'] = 'Neueste Meldungen';
$lang['home_teaser'] = 'Alle lieben Hunde!<br/><br/>Willkommen in der Community, die Hunde und ihre Besitzer zusammenbringt und deine Freunde, sowie Hundezonen, Tierärzte und Hundesitter in der Nähe lokalisiert.<br/>
Melde dich an oder registriere dich um snoopstr zu verwenden, befreunde dich mit anderen Hunden und folge ihren geteilten Fotos und Geschichten.
';
$lang['about'] = "Über";
 /*********************************************************************************
 * DOGS
 ***********************************************************************************/
$lang['dog_profile'] = "Profil";
$lang['dog_overview_new_dog'] = 'Hundeprofil erstellen';
$lang['dog_new_name'] = "Name";
$lang['dog_owner'] = "Besitzer";
$lang['dog_new_nickname'] = "Spitzname";
$lang['dog_new_breed'] = "Rasse";
$lang['dog_new_create'] = "Hundeprofil erstellen";
$lang['dog_new_header'] = "Neues Hundeprofil";

$lang['dog_edit_header'] = " Profil bearbeiten";
$lang['dog_edit_birthday'] = "Geburtstag (TT.MM.JJJJ)";
$lang['dog_edit_color'] = "Farbe";
$lang['dog_edit_coat'] = "Fell";
$lang['dog_edit_type'] = "Typ";
$lang['dog_edit_size'] = "Größe";
$lang['dog_edit_characteristics'] = "Eigenschaften";
$lang['dog_edit_about'] = "Geschichte";
$lang['dog_edit_chip'] = "Chip Nummer";
$lang['dog_edit_success'] = "Hundeprofil erfolgreich geändert";
$lang['dog_edit_manage_friends'] = "Freunde bearbeiten";
$lang['dog_edit_manage_enemies'] = "Rivalen bearbeiten";
$lang['dog_edit_gender'] = " Geschlecht";
$lang['dog_update_profile'] = "SPEICHERN";
$lang['dog_delete_profile'] = "Hundeprofil löschen";
$lang['dog_delete_confirm_text'] = "Bist du sicher, dass du dieses Hundeprofil löschen möchtest?";
$lang['dog_birthday'] = "Geburtstag";
$lang['dog_about'] = "Über";
$lang['dog_doctor'] = "Tierarzt";
$lang['dog_vaccine'] = "Impfung";
$lang['dog_edit_cover'] = "Titelbild bearbeiten (600px x 250px)";
$lang['dog_edit_profile'] = "Profilbild bearbeiten (190px x 220px)";

$lang['dog_frenemies_header'] = "Verbindungen von ";
$lang['dog_frenemies_friends_of'] = "Freund von ";
$lang['dog_frenemies_enemies_of'] = "Rivalen von ";
$lang['dog_frenemies_friends'] = "Freunde";
$lang['dog_frenemies_enemies'] = "Rivalen";
$lang['dog_frenemies_mutual_friends'] = "gemeinsame Freunde";
$lang['dog_frenemies_mutual_enemies'] = "gemeinsame Rivalen";
$lang['dog_remove_friend'] = "Freund entfernen";
$lang['dog_remove_enemy'] = "Rivale entfernen";

$lang['dog_delete_friend'] = "Verbindung entfernen";
$lang['dog_add_friend'] = "Als Freund hinzufügen";
$lang['dog_added_friend'] = "ist jetzt dein Freund";
$lang['dog_add_enemy'] = "Als Rivale hinzufügen";
$lang['dog_added_enemy'] = "ist jetzt dein Rivale";
$lang['dog_friends'] = "Freunde";
$lang['dog_enemies'] = "Rivalen";
$lang['dog_no_dogs'] = "Du hast noch keine Hunde!";
$lang['dog_selection_add'] = "Hinzufügen";
$lang['dog_selection_cancel'] = "Abbrechen";
$lang['dog_selection_friend_text'] = "Deinen Hund als Freund auswählen";
$lang['dog_selection_enemy_text'] = "Deinen Hund als Rivale auswählen";
$lang['dog_click'] = "Klick ";
$lang['dog_here_link'] = " Hier";
$lang['dog_to_dogs'] = " einen Hund zum Profil hinzufügen";
$lang['dog_search_friend'] = " nach Hundefreunden suchen";
$lang['dog_no_friends'] = "Du hast keine Freunde!";
$lang['dog_create_breed'] = "Füge deinen eigenen hinzu!";
$lang['dog_search_all_breeds'] = "Alle Rassen";
$lang['dog_overview_facts'] = "Fakten";
$lang['dog_overview_medical'] = "Krankengeschichte";
$lang['dog_overview_about'] = "Geschichte";
$lang['dog_overview_see_more'] = "Mehr sehen";
$lang['iframe_show_dog_text'] = "Folge diesem Hund auf snoopstr!";

$lang['dog_lost'] = "Verloren";
$lang['dog_lost_info'] = "Information:";
$lang['dog_lost_contact'] = "Kontakt:";


/*********************************************************************************
 * STATICS
 ***********************************************************************************/
$lang['vets_header'] = "Tierärzte";
$lang['vet_detail_desc'] = "Über";
$lang['vet_detail_contact'] = "Kontakt";
$lang['vet_detail_times'] = "Ordination";
$lang['vet_detail_gallery'] = "Galerie";
$lang['vet_detail_already_rated'] = "Du hast diesen Tierarzt bereits bewertet!";
$lang['closest_vets'] = "Lokalisiere mich";
$lang['vets_nearby'] = "Tierarzt Suche";
$lang['vet_name'] = "Tierarzt Name";
$lang['vets_featured_text'] = "Empfohlene Ärzte";
$lang['vets_nearby_desc'] = "Gib deinen Aufenthaltsort oder den Anfang vom Namen eines Tierarztes ein,<br/> um Tierärzte in deiner Gegend zu finden.";
$lang['vets_search_name'] = "Suche nach Namen";
$lang['vets_search_text'] = "Trage deinen Namen in das untere Feld ein!";


$lang['places_nearby'] = "Lokalisierte Suche";
$lang['place_name'] = "Ortsname ";
$lang['places_featured_text'] = "Empfohlene Hundefreundliche Orte";
$lang['places_nearby_desc'] = "Gib deinen Aufenthaltsort oder den Anfang vom Namen eines Platzes ein<br/> um Hundefreundliche Orte in deiner Nähe zu finden.";
$lang['places_search_name'] = "Suche nach Namen";
$lang['places_search_text'] = "Gib den Namen im unteren Feld ein!";

$lang['breed_details_characteristics'] = "Eigenschaften";

$lang['sitter_detail_contact'] = "Kontakt";
$lang['sitter_detail_desc'] = "Beschreibung";
$lang['sitter_detail_rating'] = "Bewertung:";
$lang['sitter_detail_rate'] = "bewerten";
$lang['sitter_detail_already_rated'] = "Du hast diesen Hundesitter schon bewertet!";
$lang['sitter_detail_thank_rate'] = "Danke für die Bewertung!";
$lang['closest_sitters'] = "Lokalisiere mich";
$lang['sitters_featured_text'] = "Empfohlene Hundesitter";
$lang['sitter_name'] = "Hundesitter Name";
$lang['sitters_nearby'] = "Hundesitter Suche";
$lang['sitters_nearby_desc'] = "Gib deinen Aufenthaltsort oder den Anfang vom Namen einem Hundesitter ein<br/> um Hundesitter in deiner Nähe zu finden.";

$lang['zone_detail_address'] = "Adresse";
$lang['zone_detail_desc'] = "Beschreibung";
$lang['zone_detail_friend'] = " Freunde sind hier";
$lang['zone_detail_size'] = "Größe: ";
$lang['zone_detail_rating'] = "Bewertung:";
$lang['zone_detail_rate'] = "bewerten";
$lang['zone_detail_already_rated'] = "Du hast diese Zone bereits bewertet!";
$lang['zone_detail_thank_rate'] = "Danke für die Bewertung!";
$lang['suggest_zone'] = "Zone vorschlagen";
$lang['closest_zones'] = "Lokalisiere mich";
$lang['suggest_zone_name'] = "Name";
$lang['suggest_zone_about'] = "Beschreibung";
$lang['suggest_zone_address'] = "Adresse";
$lang['suggest_zone_area'] = "Grösse (m&#178;)";
$lang['send_suggestion'] = "Vorschlag senden";
$lang['suggestion_sent'] = "Vorschlag gesendet!";
$lang['zone_search'] = "Gib deinen Aufenthaltsort ein oder lass dich lokalisieren um Zonen in der Nähe zu finden.";
$lang['cant_find'] = "DU FINDEST NICHT WONACH DU SUCHST?";
$lang['suggest_new'] = "Neue Zone vorschlagen:";
$lang['zone_name'] = "Zonenname";

$lang['profile_search'] = "Hundename";
$lang['profile_search_submit'] = "Suchen";

$lang['albums_add_photo'] = "Foto hinzufügen";
$lang['album_new_title'] = "Albumtitel";
$lang['album_about'] = "Über dieses Album...";
$lang['add_new_album'] = "Album hinzufügen";
$lang['save_new_album'] = "Album speichern";
$lang['new_album'] = "Neues Album";
$lang['my_albums'] = "Meine Alben";
$lang['add_album'] = "Album hinzugefügt!";
$lang['edit_album'] = "Album bearbeiten";
$lang['remove_album'] = "Album entfernen";
$lang['albums'] = "ALben";
$lang['no_photo_yet'] = "Keine Fotos im Album";
$lang['no_tagged_photo_yet'] = "Keine markierten Fotos";
$lang['no_album_yet'] = "Benutzer hat keine Alben";
$lang['photo_edit_title'] = "Titel bearbeiten";

$lang['user_albums'] = "Fotoalben";
$lang['user_album_back_button'] = "Zurück";

$lang['edit_activity'] = "Aktivität bearbeiten";
$lang['new_activity'] = "Neue Aktivität";
$lang['new_activity_date'] = "Datum (MM-TT-JJJJ)";
$lang['new_activity_time'] = "Zeit";
$lang['new_activity_form_date'] = "Datum";
$lang['new_activity_place'] = "Ort";
$lang['new_activity_about'] = "Beschreibung";
$lang['pending_activities'] = "Unbeantwortete Aktivitäten";
$lang['joined_activities'] = "Ausgemachte Aktivitäten";
$lang['add_new_activity_button'] = "Aktivität hinzufügen";
$lang['edit_activity_button'] = "Aktivität bearbeiten";
$lang['activity_guests'] = "Teilnehmende Hunde";
$lang['add_activity_guest'] = "Lade einen Freund ein";
$lang['activity'] = "Aktivität";
$lang['accept_activity'] = "Akzeptieren";
$lang['decline_activity'] = "Ablehnen";
$lang['activity_pending'] = "Unbeantwortete Anfragen";


$lang['sidemenu_newhere_text'] = 'Registriere dich, um dich dem ersten sozialen Netzwerk für Tiere anzuschließen.';

$lang['invite_spread'] = 'Bitte weitersagen';
$lang['invite_page_miss'] = 'Lade deine Freunde, die Hunde besitzen ein, sich bei Snoopstr zu registrieren.';
$lang['invite_page_email_enter'] = 'Teile unsere Seite auf sozialen Netzwerken.';
$lang['invite_page_share'] = 'oder teile diese Seite auf Facebook oder per E-Mail um weitere Hunde und Hundebesitzer einzuladen.';
$lang['invite_send_mail_button'] = 'Einladung senden';
$lang['home_no_news'] = 'Keine Neuigkeiten';
$lang['activity_plan'] = 'Du kannst Aktivitäten mit anderen Hunden gemeinsam planen.';
$lang['activity_invite_friend'] = 'Gib die Details zu einer Aktivität unten ein und lade deine Hundefreunde ein.';
$lang['closest_dogs'] = "Zeige Hunde in der Nähe";
$lang['vets_description'] = "Hier findest du eine Liste aller registrierten Tierärzte.<br/> Durchsuche sie alphabetisch, indem du ihren Namen eingibst oder lass dich lokalisieren um Tierärzte in der Nähe anzuzeigen";
$lang['sitters_description'] = "Hier findest du eine Liste aller registrierten Hundesitter.<br/> Durchsuche sie alphabetisch, indem du ihren Namen eingibst oder lass dich lokalisieren um Hundesitter in der Nähe anzuzeigen";
$lang['my_friends_search_dog'] = 'Finde weitere Hundefreunde';
$lang['my_enemies_search_dog'] = 'Finde weitere Rivalen';

$lang['news_photo_instagram'] = 'Von Instagram ';
$lang['request_delete'] = 'Löschung des Profils anfordern';
$lang['request_delete_confirm'] = 'Bist du sicher, dass du dein Profil löschen möchtest?';
$lang['delete_underway'] = 'Du hast die Löschung des Profils angefragt am: ';

$lang['partner_login_header'] = 'Partner login';
$lang['partner_login_username'] = 'Partner email';
$lang['partner_advertisements'] = 'Werbung';
$lang['partner_available_amount'] = 'Verfügbare Summe: ';
$lang['amount_mark'] = '$';
$lang['advert_image'] = 'Bild:';
$lang['advert_link'] = 'Link zu:';
$lang['advert_impressions'] = 'Sichtkontakte: ';
$lang['advert_clicks'] = 'Klicks: ';

$lang['partner_logout'] = 'Ausloggen';
$lang['location'] = 'Ort';
$lang['results'] = "Ergebnisse";
$lang['please_log_in'] = "Bitte melde dich an um weitere Details zu sehen!";
$lang['search_dogs'] = "Nach Hunden suchen";
$lang['dog_search_description'] = "Du kannst nach Namen oder Rasse suchen. Lass das Namensfeld leer um alle Hunde anzuzeigen.";
$lang['dog_friend_suggestion'] = "Hier sind weitere Freundschaftsvorschläge";
$lang['dog_friend_suggestion_newsfeed'] = "Füge Freunde hinzu um Geschichten zu sehen <br/>hier sind einige Vorschläge";
$lang['dog_enemy_suggestion'] = "Hier sind weitere Vorschläge für Rivalen";
$lang['dog_enemies_of'] = "Rivale von ";
$lang['dog_friends_of'] = "Freund von ";
$lang['dog_no_friends'] = "Noch keine Freunde!";
$lang['dog_no_enemies'] = "Noch keine Rivalen!";
$lang['booking_sitter_btn'] = "zum Kalender";
$lang['booking_hover'] = "Buchungen";
$lang['album_delete_confirm_text'] = "Bist du sicher, dass du dieses Album löschen möchtest?";
$lang['photo_delete_confirm_text'] = "Bist du sicher, dass du dieses Foto löschen möchtest?";
$lang['places'] = 'Plätze';

$lang['dog_is_missing'] = ' wird VERMISST!';
$lang['embed_profile'] = 'Profil einbetten';
$lang['edit_button'] = 'Bearbeiten';
$lang['owners_photos'] = 'Fotos vom Besitzer';
$lang['tagged_images'] = 'Markierte Fotos';
$lang['are_you_sure'] = 'Bist du sicher?';
$lang['yes'] = 'JA';
$lang['no'] = 'NEIN';

$lang['cover_image'] = 'Titelbild';
$lang['please_select_area'] = 'Bitte wähle die Gegend aus und klicke „Speichern“!';
$lang['finish'] = 'Speichern';
$lang['close'] = 'Abbrechen';
$lang['not_in_list'] = 'Nicht in der Liste';
$lang['preview'] = 'Vorschau';
$lang['dog_not_lost'] = 'Dieser Hund ist nicht als verschwunden markiert.';
$lang['print'] = 'Drucken';
$lang['please_select_breed'] = 'Bitte wähle eine Rasse aus...';
$lang['no_pending_activity'] = 'Keine unbeantwortete Aktivitäten';
$lang['invited_you'] = 'hat dich eingeladen zu';
$lang['at'] = 'UM';
$lang['no_upcoming_activity'] = 'Keine anstehenden Aktivitäten';
$lang['you_created_invitation'] = 'Du hast eine Einladung erstellt';
$lang['no_accepted_invites'] = 'Keine akzeptierten Einladungen';
$lang['no_pending_invites'] = 'Keine ausstehenden Einladungen';
$lang['comments'] = 'KOMMENTARE';
$lang['add_comment'] = 'Kommentar hinzufügen';
$lang['submit'] = 'ABSCHICKEN';
$lang['available'] = 'Verfügbar';
$lang['unavailable'] = 'Nicht verfügbar';
$lang['suggest_zone_description'] = 'Eine Beschreibung vorschlagen:';
$lang['tags'] = 'Markierungen';
$lang['already_tagged'] = 'Bereits markiert';
$lang['search'] = 'SUCHE';
$lang['type_in_name'] = 'NAME EINGEBEN';
$lang['save'] = 'SPEICHERN';
$lang['likes'] = 'Likes: ';
$lang['register_to_like_comment'] = 'REGISTRIERE DICH UM DIESES FOTO ZU LIKEN ODER ZU KOMMENTIEREN';
$lang['click_here_to_find_more_dogs'] = 'Klicke hier um noch mehr Hunde zu finden';
$lang['points_from'] = 'Punkte von';
$lang['users'] = 'User';
$lang['log_in_further_info'] = 'Bitte logge dich ein um mehr Information zu sehen!';
$lang['book_this_sitter'] = 'Diesen Hundesitter buchen';
$lang['sitter_available_dates'] = 'Hier siehst du die Verfügbarkeit des Hundesitters. Click auf ein Datum um zu buchen!';
$lang['tagged_photos_of'] = 'Markierte Fotos von ';

$lang['please_wait'] = 'Bitte warten...';
$lang['profile_image'] = 'Profilbild';
$lang['loading'] = 'LADET...';
$lang['email_sent'] = 'E-mail wurde versendet!';
$lang['thank_you_suggestion'] = 'Danke! Dein Vorschlag wurde zur Überprüfung versendet.';
$lang['drag_drop'] = 'Fotodateien hierher ziehen';


/* NEW LINES ADDED AFTER 07.23 */
$lang['vet_login_header'] = 'Veterinary login';
$lang['vet_login_username'] = 'Email address';
$lang['vet_login_edit'] = 'Veterinary edit';
$lang['vet_title'] = 'Title:';
$lang['login_email'] = 'Login email';
$lang['promo_code_desc'] = 'Wenn du einen Promo Code bekommen hast, gebe ihn bitte hier an:';
$lang['promo_code'] = 'Promo Code';
$lang['new_password'] = 'New Password';
$lang['password_mismatch'] = 'Password and Confirm password doesnt match!';
$lang['invalid_old_pw'] = 'Invalid old password!';
$lang['welcome'] = 'Willkommen';
$lang['add_dog_to_friend'] = 'Add dogs to become friends';

$lang['remember_me'] = 'Angemeldet bleiben';
$lang['community_access'] = '<br/>Zugriff auf unsere Community gibt es auch per App';
$lang['download_now'] = 'Jetzt herunterladen!';
$lang['find_nearby_everything'] = 'Finde Hunde, Tierärzte und hundefreundliche Plätze in der Nähe.<br/> Hol’ dir alle Neuigkeiten mit Fotos, Updates und einiges mehr!';
$lang['also_follow'] = 'Du kannst uns jetzt auch folgen auf: ';
$lang['spread_word'] = 'Hilf uns noch bekannter zu werden!';
$lang['send_invite'] = 'EINLADUNGEN SENDEN';
$lang['footer_press'] = 'PRESSE';

$lang['suggest_zone_description_thanks'] = 'Danke! Dein Vorschlag wurde zur Überprüfung an uns gesendet!';
$lang['search_searching'] = 'Suchen...';
$lang['sorry_nothin_found'] = 'Tut uns leid, wir haben nichts gefunden!';
$lang['sorry_nothin_found_sitter'] = 'Leider keine Ergebnisse gefunden. Markiere dich als Hundesitter damit du hier gelistet wird.';
$lang['login_forgot_password'] = 'Passwort vergessen?';
$lang['login_click_here'] = 'Hier klicken!';
$lang['please_type_location'] = 'Bitte gib einen Ort an!';
$lang['dog_lost'] = 'Hund als vermisst markieren';
$lang['more'] = 'MEHR';
$lang['forgot_pw_header'] = 'PASSWORT VERGESSEN';
$lang['forgot_sub_header'] = 'Gib deine E-mail Adresse hier ein und wir schicken dir ein neues Passwort zu:';
$lang['forgot_email'] = 'E-mail Adresse: ';
$lang['forgot_pw_submit'] = 'ABSCHICKEN';
$lang['no_dogs_for_breed'] = 'Keine Hunde dieser Rasse gefunden!';
$lang['no_dogs_for_name'] = 'Keine Hunde mit diesem Namen gefunden!';
$lang['upload_failed_text'] = 'Upload failed please try a smaller image';

/* EVENTS */
$lang['event_added_friend'] = ' hat ein Freund hinzugefügt: ';
$lang['event_new_friend'] = 'new Friend';
$lang['event_new_profile_image'] = 'Neues Profilbild';
$lang['event_changed_profile_image'] = ' changed profile picture';
$lang['event_added_photo_album'] = ' hat ein Foto hinzugefügt in: ';
$lang['event_new_photo'] = 'Neues Bild';
$lang['event_rate_sitter'] = ' bewertete Hundesitter ';
$lang['event_rate_vet'] = ' bewertete Hundearzt ';
$lang['event_rate_zone'] = ' bewertete Hundezone ';
$lang['event_changed_cover'] = ' hat sein Coverbild geändert. ';
$lang['event_new_cover'] = 'neues Coverbild';
$lang['event_tagged_on_photo'] = ' wurde auf einem Photo getagged ';
$lang['event_tagged'] = 'Tagged';
$lang['event_dog_is_missing'] = ' wird vermisst';
$lang['event_missing'] = 'vermisst';
$lang['event_dog_lost'] = 'WIRD VERMISST';
$lang['event_checked_in_vet'] = ' hat eingecheckt bei: ';
$lang['event_checked_in'] = 'eingecheckt';
$lang['event_checked_in_zone'] = ' hat eingecheckt in ';
$lang['event_liked_a'] = ' hat ein Foto geliket ';
$lang['event_commented_on_a'] = ' hat ein Foto kommentiert ';
$lang['event_commented_picture'] = 'hat ein Foto kommentiert';
$lang['event_changed_story'] = ' änderte die Geschichte ';
$lang['event_story_change'] = 'Änderung der Geschichte';
$lang['event_liked_picture'] = 'hat ein Foto geliket';


/* ERRORS */
$lang['forgot_password_email_not_found'] = 'Wenn der angegeben E-mail Adresse gefunden wurde, versenden wir dir ein neues Passwort.';


?>

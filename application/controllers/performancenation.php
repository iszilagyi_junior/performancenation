<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Performancenation extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('statics_model');
		$this->load->model('auth_model');
		$this->load->model('car_model');
		
		
		
	} 
	
	public function index()
	{
		
		$this->session->set_userdata('language', 'en');
				 
		if (!$this->ion_auth->logged_in()){
			$data = null;
			
			
			$data['countrydata'] = $this->auth_model->getCountries()->result();
			$data['statedata'] = $this->auth_model->getStates()->result();
			
			$data['message'] = $this->session->flashdata('message');
			$data['email_repop'] = $this->session->flashdata('email_repop');
			$data['firstname_repop'] = $this->session->flashdata('firstname_repop');
			$data['lastname_repop'] = $this->session->flashdata('lastname_repop');
			$data['nickname_repop'] = $this->session->flashdata('nickname_repop');
			
			$data['country_repop'] = $this->session->flashdata('country_repop');
			$data['state_repop'] = $this->session->flashdata('state_repop');
			$data['city_repop'] = $this->session->flashdata('city_repop');
			$data['zip_repop'] = $this->session->flashdata('zip_repop');
			$data['address_repop'] = $this->session->flashdata('address_repop');
			
			$data['message_pw'] = $this->session->flashdata('message_pw');
			$data['message_login'] = $this->session->flashdata('message_login');
			$this->loadDefaultView('frontend/home', $data);
			
			
		}
		else{ 
			
			$data = null;
			
			$my_cars = $this->car_model->getVehicles($this->session->userdata('user_id'))->result();
			
			$data['my_cars'] = array();
			
			foreach($my_cars as $car)
			{
				$item = array('id' => $car->id,
								'nickname' => $car->nickname,
								'pretty_url' => $car->pretty_url,
								'profile_image' => $this->statics_model->getImageByID($car->profile_image)->row()->fname,
								'cover_image' => $this->statics_model->getImageByID($car->cover_image)->row()->fname);
								
				$data['my_cars'][] = $item;
				
				
			}
			
			if(count($my_cars) == 1)
			{
				redirect('car/select_vehicle/'.$data['my_cars'][0]['id']);
			}
			else
			{
				if ($this->session->userdata('vehicle_id') !== FALSE) {
					
				  	redirect('news');
				}
				else
				{
					$this->loadDefaultView('frontend/car_select', $data);
				}
					
			}
			
			
		}
		
	}
	
	public function invitation($id)
	{
	
		$data = null;
		$data['invite_id'] = $id;
		$data['message'] = $this->session->flashdata('message');
		$this->loadDefaultView('frontend/invitation', $data);
	}
	
	
	public function terms()
	{
		$this->session->set_userdata('language', 'en');
		
		$data = null;
		
		$this->loadDefaultView('frontend/terms', $data);
			
	}
	
	
	public function switch_vehicle()
	{
		$this->checkLogin();
		$data = null;
			
			$my_cars = $this->car_model->getVehicles($this->session->userdata('user_id'))->result();
			
			$data['my_cars'] = array();
			
			foreach($my_cars as $car)
			{
				$item = array('id' => $car->id,
								'nickname' => $car->nickname,
								'pretty_url' => $car->pretty_url,
								'profile_image' => $this->statics_model->getImageByID($car->profile_image)->row()->fname,
								'cover_image' => $this->statics_model->getImageByID($car->cover_image)->row()->fname);
								
				$data['my_cars'][] = $item;
				
				
			}

		$this->loadDefaultView('frontend/car_select', $data);
	}
	
	
	public function help()
	{
	
		$data = null;
			
			

		$this->loadDefaultView('frontend/help', $data);
	}
	
	public function group_create_terms()
	{
	
		$data = null;
			
			

		$this->loadDefaultView('frontend/group_terms', $data);
	}
	
	
	public function lang_de()
	{
		$this->session->set_userdata('language', 'de');
		redirect('');
	}
	
	public function lang_en()
	{
		$this->session->set_userdata('language', 'en');
		redirect('');
	}
	
	public function News()
	{
		$data = null;
		$this->checkLogin();
		$this->loadDefaultView('frontend/news', $data);
	}
	
	
	public function press()
	{
		$lang = "en";
		
		if ($this->session->userdata('language') !== FALSE) {
			$lang = $this->session->userdata('language');
		}
		
		$this->loadDefaultView('frontend/press_'.$lang);
	}
	
	public function contact()
	{
		$this->loadDefaultView('frontend/contact');
	}
	
	public function impressum()
	{
		$lang = $this->session->userdata('language');
   		
   		switch($lang)
   		{
	   		case 'de': { $this->loadDefaultView('frontend/impressum_de');};break;
	   		
	   		case 'en': { $this->loadDefaultView('frontend/impressum');};break;
	   		
	   		default: { $this->loadDefaultView('frontend/impressum');}

   		}
	}
	
	public function mobile_terms()
	{
	    	
		$lang = $this->session->userdata('language');
   		
   		switch($lang)
   		{
	   		case 'de': { $this->loadDefaultView('frontend/mobile_terms_de');};break;
	   		
	   		case 'en': { $this->loadDefaultView('frontend/mobile_terms');};break;
	   		
	   		default: { $this->loadDefaultView('frontend/mobile_terms');}

   		}	
			
	   
	}
	
	public function imprint()
	{
			
		$lang = $this->session->userdata('language');
   		
   		switch($lang)
   		{
	   		case 'de': { $this->loadDefaultView('frontend/impressum_de');};break;
	   		
	   		case 'en': { $this->loadDefaultView('frontend/impressum');};break;
	   		
	   		default: { $this->loadDefaultView('frontend/impressum');}

   		}	
			
	
	}
	
	public function about()
	{
		
		
		$lang = $this->session->userdata('language');
   		
   		switch($lang)
   		{
	   		case 'de': { $this->loadDefaultView('frontend/about_de');};break;
	   		
	   		case 'en': { $this->loadDefaultView('frontend/about');};break;
	   		
	   		default: { $this->loadDefaultView('frontend/about');}

   		}
	}
	
	public function nopreview()
	{
		$this->load->view('nopreview');
	}
	
	public function favorite_popup()
	{
		$type = $_POST['type'];
		$type_id = $_POST['type_id'];
		$user_id = $_POST['user_id'];
		
		$this->load->model('car_model');
		$data['dogs'] = $this->car_model->getDogs($user_id);
		foreach($data['dogs']->result() as $dog)
		{
			$data['favorites'][$dog->id] = $this->car_model->isFavorite($type, $dog->id, $type_id);
		}
		$data['type'] = $type;
		$data['type_id'] = $type_id;
		
		echo $this->load->view('frontend/favorite_popup', $data, true);
	}
	
	
	public function app_redirecter()
	{
		
		$ip = $this->input->ip_address();
		
		$data = array('ip_address' => $ip);
		$this->statics_model->addRedirectData($data);
		
		 header("Location: https://appsto.re/us/gJSF7.i");
	}
	
	
	public function my_profile()
	{
		$this->checkLogin();
		$userdata = $this->statics_model->getUserData($this->session->userdata('user_id'))->row();
		
		$data['userdata'] = $userdata;
		
		$username = $userdata->username;
		
		$data['my_profile'] = false;
		
		if($userdata->id == $this->session->userdata('user_id'))
		{
			$data['my_profile'] = true;
		}		
		
		$my_cars = $this->car_model->getVehicles($userdata->id)->result();
			
		$data['vehicles'] = array();
		
		
		$sumpoints = 0;
		
		foreach($my_cars as $car)
		{
			$points = $this->statics_model->getPoints($car->id);
			
			foreach($points as $point)
			{
				$sumpoints += $point['points'];
			}
			
			
			
			$profile = $this->statics_model->getImageByID($car->profile_image)->row()->fname;
			$cover = $this->statics_model->getImageByID($car->cover_image)->row()->fname;
			
			$item = array('id' => $car->id,
							'nickname' => $car->nickname,
							'pretty_url' => $car->pretty_url,
							'profile_image' => $profile,
							'cover_image' => $cover);
							
			$data['vehicles'][] = $item;
			
			
		}
		
		$user_points = $this->statics_model->getUserPoints($userdata->id);
		
		foreach($user_points as $point)
		{
			$sumpoints += $point['points'];
		}
		
		$original_badge = false;
		if($userdata->id < 1001)
		{
			$original_badge = true;
		}
		
		
		
		$badge = 'rookie_badge.png';
		
		if($sumpoints > 999)
		{
			$badge = 'racer_badge.png';
		}
		
		if($sumpoints > 4999)
		{
			$badge = 'champion_badge.png';
		}
		
		if($sumpoints > 9999)
		{
			$badge = 'elite_badge.png';
		}
		
		if($sumpoints > 24999)
		{
			$badge = 'legend_badge.png';
		}
		
		$data['points'] = $sumpoints;
		$data['badge'] = $badge;
		$data['original_badge'] = $original_badge;
		
		$this->loadDefaultView('statics/user_detail', $data);	
	}
	
	
	public function user_detail($username)
	{
		$this->checkLogin();
		$userdata = $this->statics_model->getUserdataByUsername($username)->row();
		
		$data['userdata'] = $userdata;
		
		$username = $userdata->username;
		
		
		$data['my_profile'] = false;
		
		if($userdata->id == $this->session->userdata('user_id'))
		{
			$data['my_profile'] = true;
		}
				
		
		$my_cars = $this->car_model->getVehicles($userdata->id)->result();
			
		$data['vehicles'] = array();
		$sumpoints = 0;
		
		foreach($my_cars as $car)
		{
			
			$points = $this->statics_model->getPoints($car->id);
			
			foreach($points as $point)
			{
				$sumpoints += $point['points'];
			}
			
			
			$profile = $this->statics_model->getImageByID($car->profile_image)->row()->fname;
			$cover = $this->statics_model->getImageByID($car->cover_image)->row()->fname;
			
			$item = array('id' => $car->id,
							'nickname' => $car->nickname,
							'pretty_url' => $car->pretty_url,
							'profile_image' => $profile,
							'cover_image' => $cover);
							
			$data['vehicles'][] = $item;
			
			
		}
		
		$user_points = $this->statics_model->getUserPoints($userdata->id);
		
		foreach($user_points as $point)
		{
			$sumpoints += $point['points'];
		}
		
		
		$original_badge = false;
		if($userdata->id < 1001)
		{
			$original_badge = true;
		}
		
		
		
		$badge = 'rookie_badge.png';
		
		if($sumpoints > 999)
		{
			$badge = 'racer_badge.png';
		}
		
		if($sumpoints > 4999)
		{
			$badge = 'champion_badge.png';
		}
		
		if($sumpoints > 9999)
		{
			$badge = 'elite_badge.png';
		}
		
		if($sumpoints > 24999)
		{
			$badge = 'legend_badge.png';
		}
		
		$data['points'] = $sumpoints;
		$data['badge'] = $badge;
		$data['original_badge'] = $original_badge;
		
		
		$this->loadDefaultView('statics/user_detail', $data);	
	}
	
	
	public function my_groups()
	{
		$this->checkLogin();
		
		$user_id = $this->session->userdata('user_id');
			
		$data['owned_groups'] = array();
		$data['member_of_groups'] = array();
		$group_id_holder = array();
		$my_vehicles = $this->car_model->getVehicles($user_id)->result();
		
		foreach($my_vehicles as $vehicle)
		{
			
			$groups = $this->statics_model->getVehicleGroups($vehicle->id)->result();
			
			
			
			foreach($groups as $group)
			{
				$members = $this->statics_model->getGroupMembers($group->id)->result();
				
				$category = $this->statics_model->getGroupCategory($group->category_id)->row()->name;
				
				$item = array('id' => $group->id,
							'name' => $group->name,
							'logo' => $group->logo,
							'members' => count($members),
							'category' => $category,
							'vehicle' => $vehicle->nickname);
			
			
				$grp_role = $this->statics_model->getGroupRole($group->id, $vehicle->id);
				
				
				
				if($grp_role->role_id == 1)
				{
					if(!in_array($group->id, $group_id_holder))
					{
					
						$data['owned_groups'][] = $item;
						$group_id_holder[] = $group->id;
						
					}
				}
				else
				{
					if(!in_array($group->id, $group_id_holder))
					{
					
						$data['member_of_groups'][] = $item;
						$group_id_holder[] = $group->id;
						
					}
				}
				
				
			}
			
		}
		
		$data['group_categories'] = $this->statics_model->getGroupCategories();
		
		
		$data['group_count'] = count($groups);	
		$this->loadDefaultView('statics/my_groups', $data);
	}
	
	
	public function my_connections()
	{
		$this->checkLogin();
		
		$user_id = $this->session->userdata('user_id');
			
		$data['followers'] = array();
		$data['follows'] = array();
		$group_id_holder = array();
		$my_followers = $this->car_model->getFollowers($this->session->userdata('vehicle_id'));
		
		foreach($my_followers as $follower)
		{
			
			$vehicle = $this->car_model->getVehicle($follower['follower_id'])->row();
			
			$profile = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
			
				
				$item = array('pretty' => $vehicle->pretty_url,
							'name' => $vehicle->nickname,
							'logo' => $profile);
			
			

				
				
				$data['followers'][] = $item;
					
					
				

			
		}
		
		$follow_ids = array();
		
		$my_follows = $this->car_model->getMyFollows($this->session->userdata('vehicle_id'));
		
		foreach($my_follows as $follower)
		{
			
			$vehicle = $this->car_model->getVehicle($follower['vehicle_id'])->row();
			
			$profile = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
			
				
				$item = array('pretty' => $vehicle->pretty_url,
							'name' => $vehicle->nickname,
							'logo' => $profile);
			
			

			
					$data['follows'][] = $item;
					$follow_ids[] = $vehicle->id;
					
					
				

			
		}
		
		$data['group_count'] = count($data['followers']);	
		
		
		
		$vehicles = $this->car_model->getRandomVehicles()->result();
		$data['vehicles'] = array();
		foreach($vehicles as $vehicle)
		{
			$fname = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
			
		switch($vehicle->vehicle_type)
		{
			case 0:{
						 $model = $this->car_model->getCarModelById($vehicle->model_id)->row();
			};break;
			
			case 1:{
						 $model = $this->car_model->getMotorModelById($vehicle->model_id)->row();
			};break;
			
			case 2:{
						 $model = $this->car_model->getTruckModelById($vehicle->model_id)->row();
			};break;
		}	
			
			
			$followers = $this->car_model->getFollowers($vehicle->id);
			
			if(!in_array($vehicle->id, $follow_ids))
			{
				$item = array('id' => $vehicle->id,
						'nickname' => $vehicle->nickname,
						'type' => $vehicle->vehicle_type,
						'pretty_url' => $vehicle->pretty_url,
						'geo_location' => $vehicle->geo_location,
						'zerosixty' => $vehicle->zerosixty,
						'followers' => count($followers),
						'model' => $model,
						'fname' => $fname);
			
				$data['vehicles'][] = $item;
			}
			
		}
		$data['vehicles_count'] = count($vehicles);	
		
		
		$this->loadDefaultView('statics/my_connections', $data);
	}
	
	
	
	public function my_events()
	{
		$this->checkLogin();
		
		$user_id = $this->session->userdata('user_id');
			
		$data['my_events'] = array();
		$data['joined_events'] = array();
		$event_id_holder = array();
		$my_vehicles = $this->car_model->getVehicles($user_id)->result();
		
		foreach($my_vehicles as $vehicle)
		{
			$grp_event_ids = $this->statics_model->getVehicleEventIDs($vehicle->id);
			
			foreach($grp_event_ids as $grp_event_id)
			{
				$grp_event = $this->statics_model->getEventById($grp_event_id->event_id);
				
				$grp_role = $this->statics_model->getGroupRole($grp_event->group_id, $vehicle->id)->role_id;
				
				$group = $this->statics_model->getGroupById($grp_event->group_id)->row();
			
				$image = $group->logo;
				$image = "profilepictures/".$image;
					
				if($grp_event->image != NULL && $grp_event->image != '')
				{
					$image = "coverpictures/".$grp_event->image;
				}
				
				
				
				if($grp_role == 1)
				{
					$item = array('id' => $grp_event->id,
							'title' => $grp_event->title,
							'address' => $grp_event->address,
							'start' => $grp_event->start_time,
							'logo' => $image,
							'type' => 'group');
	
					if(!in_array($grp_event->id, $event_id_holder))
					{
						$data['my_events'][] = $item;
						$event_id_holder[] = $event->id;
						
					}
				}
				
				else 
				{
					$item = array('id' => $grp_event->id,
							'title' => $grp_event->title,
							'address' => $grp_event->address,
							'start' => $grp_event->start_time,
							'logo' => $image,
							'type' => 'group');
	
					if(!in_array($grp_event->id, $event_id_holder))
					{				
						$data['joined_events'][] = $item;
						$event_id_holder[] = $event->id;
						
					}
				}
				
			}
			
			
			$events = $this->statics_model->getUserEventsCreated($vehicle->id, $user_id);
			
			
			
			foreach($events as $event)
			{
				$vehicle = $this->car_model->getVehicle($event->vehicle_id)->row();
			
				$image = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;	
				if(!$image)
				{
					$image = "default_logo.png";
				}
				
				
				$image = "profilepictures/".$image;	
				
				
				
				
				if($event->image != NULL && $event->image != '')
				{
					$image = "coverpictures/".$event->image;
				}
				
				
				$item = array('id' => $event->id,
							'title' => $event->title,
							'address' => $event->address,
							'start' => $event->start_time,
							'logo' => $image,
							'type' => 'user');
			
			
				if(!in_array($event->id, $event_id_holder))
				{
				
					$data['my_events'][] = $item;
					$event_id_holder[] = $event->id;
					
				}
				
				
				
			}
			
		}
		
		function date_compare($a, $b)
		{
		    $t1 = strtotime($a['start']);
		    $t2 = strtotime($b['start']);
		    return $t2 - $t1;
		}    
		usort($data['my_events'], 'date_compare');	
		usort($data['joined_events'], 'date_compare');
		
		
			
		$this->loadDefaultView('statics/my_events', $data);
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
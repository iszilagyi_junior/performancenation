<script>
	var zone_geo_location = "<?= $place->geo_location?>";
	$('#sidemenu').css({top: 35});
</script>
<style>
	.gm-style-cc { display:none; }
</style>
<script src="<?= site_url("items/frontend/js/detail_map.js"); ?>" type="text/javascript"></script>
			<div id="content">
				<div id="vet_detail_header">
					<div id="zone_map_canvas">
					
					</div>
					<div class="statics_subheader">
						<?= $place->name?>
					</div>				
				</div>
				<div id="vets_details">
					<div id="statics_left" class="statics_half">
						<div id="vet_details_desc">
							<p class="vets_details_subheader"><?= $this->lang->line('zone_detail_desc')?></p>
							
							<?= $place->description; ?>
							
							
						</div>
					</div>
					<div id="statics_right" class="statics_half">
						<div id="vet_details_contact">
							<p class="vets_details_subheader"><?= $this->lang->line('zone_detail_address')?></p>
							<?= $place->address?><br/><br/>		
						</div>					
					</div>
					
					
				
			</div>
		</div>
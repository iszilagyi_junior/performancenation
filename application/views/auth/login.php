<script>
	var text = "<?= $this->lang->line('login_header')?>";
	$('.page_header').text(text);
	
    $('#sidemenu').css({top: -5});

</script>
			<div id="content">
				<div id="register_header" class="content_header">
					<?= $this->lang->line('login_header')?>
				</div>				
				<div id="login_error_message">
					<?php echo $message;?>
				</div>
				<div id="login_form">
					<?php 
						$data = array('id' => 'login_login_form');
						echo form_open("login/login", $data);
					?>
					<input type="text" id="login_username" name="login_username" value="<?= $this->lang->line('login_login_username')?>" defaultText="<?= $this->lang->line('login_login_username')?>" class="login_login_input input_default">
					<br>
					<input type="password" id="login_password" name="login_password" class="login_login_input password_real" placeholder="<?= $this->lang->line('login_login_password')?>">
					<!-- <input type="text" id="login_password_fake" name="login_password_fake" value="<?= $this->lang->line('login_login_password')?>" defaultText="<?= $this->lang->line('login_login_password')?>" class="login_login_input input_default password_fake"> -->
					<br>
					<br>
					<div style="width:200px;text-align:center;">
						<input type="checkbox" name="login_rememberme" id="login_rememberme" value="1" />
						<label class="sansItalic" for="login_rememberme" style="color:#5a5a5a;width:129px;margin-left:5px;cursor:pointer;text-align:center;"><?= $this->lang->line('remember_me')?></label>
					</div>
					
					<div class="login_arrow_text pseudo_form_submit" action="login" source="login"><?= $this->lang->line('login_login_confirm')?></div>
					<?php echo form_close();?>
					
				</div>
				<div style="width:200px;margin:20px auto;margin-left:195px;">
					<img id="sidemenu_facebook_login" src="<?= site_url('items/frontend/img/snoopstr_fb_signin.png');?>"/>
				</div>
				
				<a style="text-decoration:none;color:#5a5a5a;" href="<?= site_url('auth/forgot_password')?>">
					<div id="forgot_password" style="width:129px;margin:0px auto;cursor:pointer;text-align:center;" class="sansItalic"><?= $this->lang->line('login_forgot_password')?><br/><?= $this->lang->line('login_click_here')?></div>
				</a>
				
			</div>
			
			
	
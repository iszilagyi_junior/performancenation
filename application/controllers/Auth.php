<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');

		// Load MongoDB library instead of native db driver if required
		$this->config->item('use_mongodb', 'ion_auth') ?
		$this->load->library('mongo_db') :

		$this->load->database();

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->load->helper('language');
		
	}

	//redirect if needed, otherwise display the user list
	function index()
	{	
		
		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			//redirect('auth/login', 'refresh');
			$this->loadDefaultView('auth/login');
		}
		elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->_render_page('auth/index', $this->data);
		}
	}

	// LOGGING
	function login()
	{
		if($this->ion_auth->logged_in())
		{
			redirect('','location');
		}

		//validate form input
		$this->form_validation->set_rules('login_email', $this->lang->line('sidemenu_login_username'), 'required');
		$this->form_validation->set_rules('login_pw', $this->lang->line('sidemenu_login_password'), 'required');

		if ($this->form_validation->run() == true)
		{
			$inputPW = $this->input->post('login_pw');
			$inputEmail = $this->input->post('login_email');
			
			$remember = $this->input->post('save_login');
			
			$pwCheck = $this->check_hash($inputPW, $this->auth_model->getPW($inputEmail)->row()->password);
			
			if ($pwCheck)
			{
				//if the login is successful
				//redirect them back to the home page
				$data = null;
				$result = $this->auth_model->checkLoginUserFB($inputEmail);
				
				
				if($result->active == 1)
				{
					if($remember == 1)
					{				
						$this->ion_auth_model->set_session($result, true);
					}
					else
					{					
						$this->ion_auth_model->set_session($result);
					}
					$this->addUserPoints($result->id,1);
					//$this->loadDefaultView('frontend/home', $data);
					redirect('');
				}
				else
				{
					redirect('invitation/'.$result->id);
				}
				
			}
			else
			{
				//if the login was un-successful
				//redirect them back to the login page
				//$data['message'] = $this->ion_auth->errors();
				$this->session->set_flashdata('message_login' ,"User or password incorrect!");
				redirect('#login', 'redirect');
			}
		}
		else
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$this->session->set_flashdata('message_login' ,"User or password incorrect!");
			redirect('#login', 'redirect');
		}
	}
	
	public function loginUserFB(){
		
		$fbId = $_POST['fb'];
		$email = $_POST['email'];
		$remember = $_POST['remember'];
		$token = $_POST['token'];			
		$token_check = file_get_contents('https://graph.facebook.com/me?access_token=' . $token);
		$fb_id = json_decode($token_check)->id;
		$token_check = file_get_contents('https://graph.facebook.com/app?access_token=' . $token);
		$app_id = json_decode($token_check)->id;
		
		
		
		if($fb_id == $fbId && $app_id == "1680628872212292")
		{	
			
			
				$result = $this->auth_model->checkLoginUserFBID($fb_id);
				
				
				if(!$result)
				{
					
					$result = $this->auth_model->checkLoginUserFB($email);
					
					
				}
				
				
				if(!$result)
				{
					
					$status = array('success' => false, 'status'=> 'failed', 'message' => 'Unknown user or password!');
					
					
				}
				else
				{
					
					
					
					
					if($result->active == 1)
					{
						
						if($remember == 1)
						{				
							
							$this->ion_auth_model->set_session($result, true);
							
							
						}
						else
						{					
							
							$this->ion_auth_model->set_session($result);
							
							
						}
						
						if($result->fb_id == 0){
							$additional_data = array(
								'fb_id'  => $fbId
							);
							
							$this->auth_model->updateProfilePic($email, $additional_data);
						}
						
						if($result->profile_picture == 'default.png' ){
						
							$content = file_get_contents('http://graph.facebook.com/'.$fbId.'/picture?redirect=0');
							$data = json_decode($content, true);
							
							$picture = $data["data"]["url"];
							
							$filename = substr($picture, strrpos($picture, '/') + 1);
							
							$arr = explode("?", $filename, 2);
							$filename = $arr[0];
							
							$img = $_SERVER['DOCUMENT_ROOT'].'/performancenation/items/uploads/profilepictures/'.$filename;
							
							$url = 'https://graph.facebook.com/'.$fbId.'/picture?width=300&height=300';
						
						    $file = file_get_contents($url);
						    $fp = fopen($img,"wb");
						   
						    if (!$fp) exit;
						  
						    fwrite($fp, $file);
						    
						    fclose($fp);
							
							$additional_data = array(
								'profile_picture'  => $filename
							);
							
							$this->session->set_userdata('profile_picture', $filename);
							
							$this->auth_model->updateProfilePic($email, $additional_data);
							
						}
						
						$this->addUserPoints($result->id,1);
						$status = array('success' => true, 'status'=> 'News');
					}
					else
					{
						
						$status = array('success' => true, 'status'=> 'invitation/'.$result->id);
					}
	
					
				}
			
		}
		else{
			$status = array('success' => false,'status'=> 'INVALID');
		}
		
		
		echo json_encode($status);
	}
	
	function logout()
	{
		if(!$this->ion_auth->logged_in())
		{
			redirect('','location');
		}
		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them to the home page
		redirect("", 'refresh');
	}

	//change password
	function change_password()
	{
		$this->form_validation->set_rules('edit_password_old', $this->lang->line('change_password_validation_old_password_label'), 'trim|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']');
		$this->form_validation->set_rules('edit_password', $this->lang->line('change_password_validation_new_password_label'), 'trim|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']');
		$this->form_validation->set_rules('edit_passconf', $this->lang->line('change_password_validation_new_password_confirm_label'), 'matches[edit_password]');

		if(!$this->ion_auth->logged_in())
		{
			redirect('','location');
		}

		$user = $this->ion_auth->user()->row();

		if($this->form_validation->run() == false)
		{
			//display the form
			//set the flash data error message if there is one
			$this->session->set_flashdata('message' ,(validation_errors()) ? validation_errors() : $this->session->flashdata('message'));

			redirect('editprofile','location');
			
		}
		else
		{
			$identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));
			$oldPW = $this->input->post('edit_password_old');
			$newPW = $this->input->post('edit_password');

			$change = $this->ion_auth->change_password($identity, $oldPW, $newPW);
			
			if($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->lang->line('edit_user_pw_change_success'));
				redirect('editprofile', 'location');
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/change_password', 'refresh');
			}
		}
	}

	//forgot password
	function forgot_password()
	{
		$this->CheckLanguage();
		$this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required');
		if ($this->form_validation->run() == false)
		{
			//setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
			);

			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$this->data['identity_label'] = $this->lang->line('forgot_password_username_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			//set any errors and display the form
			$this->data['message_pw'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			
			$this->session->set_flashdata('message_pw', $this->data['message_pw']);
			redirect("#forgot", 'refresh');
			
			
		}
		else
		{
			// get identity for that email
            $identity = $this->ion_auth->where('email', strtolower($this->input->post('email')))->users()->row();
         
            if(empty($identity)) {
            	
                $this->ion_auth->set_message('forgot_password_email_not_found');
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("#forgot", 'refresh');
            }
        
			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				//if there were no errors
				
				$this->session->set_flashdata('message_pw', $this->ion_auth->messages());
				redirect("#forgot", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message_pw', $this->ion_auth->errors());
				redirect("#forgot", 'refresh');
			}
		}
	}

	//reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
		
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
		
		
			//if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				//display the form

				//set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
				'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);

				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id'   => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				
				
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				
				
				
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;
				$this->data['uid'] = $user->id;
				//render
				
				$this->loadDefaultView('auth/reset_password', $this->data);
			
			}
			else
			{
			
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{
					
					//something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						//if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						//$this->logout();
						redirect('');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('Auth/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			//if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("Auth/forgot_password", 'refresh');
		}
	}


	//activate the user
	function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			//redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth", 'refresh');
		}
		else
		{
			//redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}
	//deactivate the user
	function deactivate($id = NULL)
	{
		$id = $this->config->item('use_mongodb', 'ion_auth') ? (string) $id : (int) $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
		$this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

		if ($this->form_validation->run() == FALSE)
		{
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user($id)->row();

			$this->_render_page('auth/deactivate_user', $this->data);
		}
		else
		{
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes')
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				{
					show_error($this->lang->line('error_csrf'));
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			//redirect them back to the auth page
			redirect('auth', 'refresh');
		}
	}

	// REGISTER
	function register()
	{	
		if ($this->ion_auth->logged_in())
		{
			redirect('','location');
		}
		$this->CheckLanguage();
		
		$data['user_country'] = $this->checkGeoIP();
		
		
		$data['countrydata'] = $this->auth_model->getCountries()->result();
		$data['statedata'] = $this->auth_model->getStates()->result();
		$data['message'] = $this->session->flashdata('message');
		$data['email_repop'] = $this->session->flashdata('email_repop');
		$data['firstname_repop'] = $this->session->flashdata('firstname_repop');
		$data['lastname_repop'] = $this->session->flashdata('lastname_repop');
		$data['birthday_repop'] = $this->session->flashdata('birthday_repop');
		$data['country_repop'] = $this->session->flashdata('country_repop');
		$data['state_repop'] = $this->session->flashdata('state_repop');
		$data['city_repop'] = $this->session->flashdata('city_repop');
		$data['zip_repop'] = $this->session->flashdata('zip_repop');
		$data['address_repop'] = $this->session->flashdata('address_repop');
		$data['promo_repop'] = $this->session->flashdata('promo_repop');
		$data['selected_language'] = $this->session->userdata('language');
		$this->loadDefaultView('auth/register', $data);
	}
	function register_success()
	{
		
		$this->loadDefaultView('auth/register_success');
	}
	function register_user()
	{
		
		if ($this->ion_auth->logged_in())
		{
			redirect('','location');
		}
 		$this->CheckLanguage();
		
	//validate form input
		$this->form_validation->set_rules('register_firstname', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
		$this->form_validation->set_rules('register_lastname', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
		$this->form_validation->set_rules('register_email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('register_nickname', 'Nickname', 'required|xss_clean|is_unique[users.username]');
		$this->form_validation->set_rules('register_pw', $this->lang->line('create_user_validation_password_label'), 'trim|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']');
		$this->form_validation->set_rules('register_pw_conf', $this->lang->line('create_user_validation_password_confirm_label'), 'matches[register_pw]');
		$this->form_validation->set_rules('register_terms', 'Terms & Conditions', 'required');
		$this->form_validation->set_rules('register_zip', 'ZIP Code', 'required');
	//	$this->form_validation->set_rules('register_code_conf', 'Registration confirmation code', 'required');
		
		
		$this->form_validation->set_message('is_unique', 'The %s is already taken');
		
		
	//	$registration_code = $this->input->post('register_code_conf');	
	//	$valid_code = $this->auth_model->checkRegistrationCode($registration_code);
		
		
		
//		if($valid_code)
//		{
				
				if ($this->form_validation->run() == true)
				{

					$username = $this->input->post('register_nickname');
					$username = $this->toAscii($username);
					$email    = strtolower($this->input->post('register_email'));
					$password = $this->input->post('register_pw');			
					$fbId	  = $this->input->post('fbIdHolder');
					
					$country = $this->input->post('register_country');
					$state = $this->input->post('register_state');
					$zip = $this->input->post('register_zip');
					$address = $this->input->post('register_address');
					$city = $this->input->post('register_city');
					
					
					
					$newsletter	  = 0;//$this->input->post('register_newsletter');
					$mailname = $this->input->post('register_firstname') . ' ' . $this->input->post('register_lastname');
					$pw = $this->my_hash($password);
					
					$hashtag = "performancenation-".$username;
					
					if($fbId!=0)
					{
						
						$content = file_get_contents('http://graph.facebook.com/'.$fbId.'/picture?redirect=0');
						$data = json_decode($content, true);
						
						$picture = $data["data"]["url"];
						
						$filename = substr($picture, strrpos($picture, '/') + 1);
						
						$arr = explode("?", $filename, 2);
						$filename = $arr[0];
						
						$img = $_SERVER['DOCUMENT_ROOT'].'/items/uploads/profilepictures/'.$filename;
						
						copy($picture,$img);
					
						$additional_data = array(
							'first_name' => $this->input->post('register_firstname'),
							'last_name'  => $this->input->post('register_lastname'),					
							'profile_picture'  => $filename,
							'default_language'  => 'en',
							'country'  => $country,
							'state'  => $state,
							'zip'  => $zip,
							'city'  => $city,
							'address'  => $address,
							'user_hashtag'  => $hashtag,
							
						);
						
					}
					else{
						$additional_data = array(
							'first_name' => $this->input->post('register_firstname'),
							'last_name'  => $this->input->post('register_lastname'),
							'country'  => $country,
							'state'  => $state,
							'zip'  => $zip,
							'city'  => $city,
							'address'  => $address,
							'default_language'  => 'en',
							
							'user_hashtag'  => $hashtag,
						);
					}
					
					if($newsletter == 1)
					{
						$additional_data['newsletter'] = 1;
					}
					
					
				}
				
				
				
				if ($this->form_validation->run() == true && $this->ion_auth->register($username, $pw, $email, $fbId, $additional_data))
				{
					//check to see if we are creating the user
					//redirect them back to the admin page
					$this->session->set_flashdata('message', $this->ion_auth->messages());
					
					
					//$this->send_mail('registration', $email, $username, $pw, $promo = NULL, $mailname);
					
					$userdata = $this->auth_model->checkLoginUserFB($email);
					
					
					
					if($userdata != false)
					{
						
					/*	$user_id = $userdata->row()->id;
						$promo_code_rate = $this->auth_model->getPromoRate()->row()->bonus_impressions;
						$userWithPromo = $this->auth_model->checkPromoCode($promo_code);
						
						if($userWithPromo != false)
						{
							
							$promo_data = array('user_id' => $user_id,
												'partner_id' => $userWithPromo->row()->id);
												
							$this->auth_model->savePromoRegistration($promo_data);
							
							$newImpressions = $userWithPromo->row()->impressions_left+$promo_code_rate;
							
							
							$impression_data = array('impressions_left' => $newImpressions);
							$this->auth_model->updateImpressions($userWithPromo->row()->id, $impression_data);
						}
						*/
						
						
					} 
					
					
					$result = $this->auth_model->checkLoginUserFB($email);
				
					
					//$this->auth_model->updateRegistrationCode($registration_code);				
					//$this->ion_auth_model->set_session($result);
					
					//$this->loadDefaultView('frontend/home', $data);
					redirect('invitation/'.$result->id);
					
				}
				else
				{
					
					//display the create user form
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		
					
					
					
					$this->session->set_flashdata('message', $this->data['message']);
					$this->session->set_flashdata('email_repop', $this->input->post('register_email'));
					$this->session->set_flashdata('firstname_repop', $this->input->post('register_firstname'));
					$this->session->set_flashdata('lastname_repop', $this->input->post('register_lastname'));
					$this->session->set_flashdata('nickname_repop', $this->input->post('register_nickname'));
					
					$this->session->set_flashdata('country_repop', $this->input->post('register_country'));
					$this->session->set_flashdata('state_repop', $this->input->post('register_state'));
					$this->session->set_flashdata('city_repop', $this->input->post('register_city'));
					$this->session->set_flashdata('zip_repop', $this->input->post('register_zip'));
					$this->session->set_flashdata('address_repop', $this->input->post('register_address'));
					
					redirect('#register','refresh');
				}
/*		}
		else
		{
			$this->data['message'] = "Invalid Registration code!";
			$this->session->set_flashdata('message', $this->data['message']);
			$this->session->set_flashdata('email_repop', $this->input->post('register_email'));
			$this->session->set_flashdata('firstname_repop', $this->input->post('register_firstname'));
			$this->session->set_flashdata('lastname_repop', $this->input->post('register_lastname'));
			$this->session->set_flashdata('nickname_repop', $this->input->post('register_nickname'));
			
			$this->session->set_flashdata('country_repop', $this->input->post('register_country'));
			$this->session->set_flashdata('state_repop', $this->input->post('register_state'));
			$this->session->set_flashdata('city_repop', $this->input->post('register_city'));
			$this->session->set_flashdata('zip_repop', $this->input->post('register_zip'));
			$this->session->set_flashdata('address_repop', $this->input->post('register_address'));
			
			redirect('#register','refresh');
		}*/
		
	}



	function activate_user()
	{
		$this->form_validation->set_rules('register_code_conf', 'Invitation code', 'required');
		
		$user_id = $this->input->post('register_id');
		
		if ($this->form_validation->run() == true )
		{
				
				$invite_code = $this->input->post('register_code_conf');
					
					
					$valid_code = $this->auth_model->checkRegistrationCode($invite_code);
					
					if(!$valid_code)
					{
						$this->data['message'] = "Invalid invitation code!";

						$this->session->set_flashdata('message', $this->data['message']);
					//	$this->session->set_flashdata('email_repop', $this->input->post('register_email'));
						
						
						redirect('invitation/'.$user_id,'refresh');
						
						
						
					}
					else
					{
						
						
						$user = $this->auth_model->getProfile($user_id);
					
						$email = $user->email;
						$username = $user->username;
						$pw = $user->password;
						
						$mailname = $user->first_name . ' ' . $user->last_name;
						
						$result = $this->auth_model->checkLoginUserFB($email);
					
						$promo = $valid_code->id;
										
						$this->auth_model->updateRegistrationCode($invite_code);
						
						$this->auth_model->activateUserProfile($user_id, $promo);
						
						$this->send_mail('registration', $email, $username, $pw, $promo = NULL, $mailname);
							
						$this->ion_auth_model->set_session($result);
						
						
						redirect('');
					}
					
		}
		else
		{
			
			//display the create user form
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			
			
			
			$this->session->set_flashdata('message', $this->data['message']);
		//	$this->session->set_flashdata('email_repop', $this->input->post('register_email'));
			
			
			redirect('invitation/'.$user_id,'refresh');
		}
	}

	
	function no_code()
	{
		
		
		$user_id = $this->input->post('register_id');
		$user = $this->auth_model->getProfile($user_id);
		$email = $user->email;
		$username = $user->username;
		$pw = $user->password;	
		$mailname = $user->first_name . ' ' . $user->last_name;
		$this->send_mail('no_code', $email, $username, $pw, $promo = NULL, $mailname);
		
		redirect('');
		
	}
	



	// EDIT PROFILE
	function edit()
	{
		if(!$this->ion_auth->logged_in())
		{
			redirect('','location');
		}		
		
		$id = $this->session->userdata('user_id');
		
		$data['message'] = $this->session->flashdata('message');	
		$data['profiledata'] = $this->auth_model->getProfile($id);
		$birthday = $data['profiledata']->birthday;
		$arr = explode(".",$birthday);
		$data['year'] = $arr[2];
		$data['month'] = $arr[1];
		$data['day'] = $arr[0];
		
		$data['countrydata'] = $this->auth_model->getCountries()->result();
		$data['under_delete'] = $this->auth_model->checkUnderDelete($this->session->userdata('user_id'));
		//echo print_r($data);
		
		
		$data['is_ie'] = false;
		$data['type'] = 'profilepictures';
		$data['fname'] = '';
		
		
		$this->loadDefaultView('auth/edit_user', $data);
	}
	
	
	function edit_ie($fname)
	{
		if(!$this->ion_auth->logged_in())
		{
			redirect('','location');
		}		
		
		$id = $this->session->userdata('user_id');
		
		$data['message'] = $this->session->flashdata('message');	
		$data['profiledata'] = $this->auth_model->getProfile($id);
		$birthday = $data['profiledata']->birthday;
		$arr = explode(".",$birthday);
		$data['year'] = $arr[2];
		$data['month'] = $arr[1];
		$data['day'] = $arr[0];
		
		$data['countrydata'] = $this->auth_model->getCountries()->result();
		$data['under_delete'] = $this->auth_model->checkUnderDelete($this->session->userdata('user_id'));
		//echo print_r($data);
		
		
		$data['is_ie'] = true;
		$data['type'] = 'profilepictures';
		$data['fname'] = $fname;
		
		
		$this->loadDefaultView('auth/edit_user', $data);
	}
	
	
	
	
	function edit_user()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('','location');
		}

		$id = $this->session->userdata('user_id');

		$user = $this->ion_auth->user($id)->row();
		
		//validate form input
		$this->form_validation->set_rules('edit_firstname', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
		$this->form_validation->set_rules('edit_lastname', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
		
		
		
		
		$cover_data = NULL;
		$profile_data = NULL;

		if ($this->form_validation->run() === TRUE)
		{

			$user_hashtag = $this->input->post('import_instagram_hashtag');

			$data = array(
			'first_name' => $this->input->post('edit_firstname'),
			'last_name'  => $this->input->post('edit_lastname'),
			'show_hashtags'  => $this->input->post('edit_hashtag'),
			'country'  => $this->input->post('edit_country'),
			'state'  => $this->input->post('edit_state'),
			'zip'  => $this->input->post('edit_zip'),
			'city'  => $this->input->post('edit_city'),
			'address'  => $this->input->post('edit_address'),
			'geo_location'  => $this->input->post('edit_geolocation'),
			'phone'  => $this->input->post('edit_phone'),
			'skype'  => $this->input->post('edit_skype'),
			'fb_link'  => $this->input->post('edit_fb'),
			'tw_link'  => $this->input->post('edit_tw'),
			'insta_link'  => $this->input->post('edit_insta'),	
			'import_instagram'  => $this->input->post('import_instagram')
			
			);
			
			// set array for new cover and profile picture
		if($cover_data != NULL)
			$data['cover_picture'] = $cover_data['file_name'];
		if($profile_data != NULL){
			$data['profile_picture'] = $profile_data['file_name'];
			$this->session->set_userdata("profile_picture", $profile_data['file_name']);
		}
			$this->ion_auth->update($user->id, $data);

			//check to see if we are creating the user
			//redirect them back to the admin page
			$this->session->set_flashdata('message', $this->lang->line('edit_user_profile_change'));
			redirect("my_profile", 'refresh');
		}
		else
		{
		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		$this->session->set_flashdata('message', $this->data['message']);
		
		redirect('editprofile','location');			
		}
	}
	
	function edit_vet()
	{
		
		if (!$this->session->userdata('vet_id'))
		{
			redirect('vet_login','location');
		}

		$vet_id = $this->session->userdata('vet_id'); 

		$user = $this->ion_auth->user($id)->row();
		
		//validate form input
		$this->form_validation->set_rules('edit_firstname', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
		$this->form_validation->set_rules('edit_lastname', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
		
	
		
		
		$cover_data = NULL;
		$profile_data = NULL;

		if ($this->form_validation->run() === TRUE)
		{
	

			$address = $this->input->post('edit_address')." ".$this->input->post('edit_zip')." ".$this->input->post('edit_city');
			
			$new_pw = $this->input->post('edit_login_pw_new');
			$confirm_pw = $this->input->post('edit_login_pw_confirm');
			$old_pw = $this->input->post('edit_login_pw_old');
			
			$stored_hash = $this->auth_model->getVetbyID($vet_id)->row()->login_pw;
			if($new_pw != "" && $confirm_pw != "" && $old_pw != "")
			{
				$pw_check = $this->check_hash($old_pw, $stored_hash);
				
				
				if($new_pw != $confirm_pw)
				{
					$this->session->set_flashdata('message', $this->lang->line('password_mismatch'));
				
					redirect('vet_login','location');			
	
				}
				else if(!$pw_check)
				{
				
					$this->session->set_flashdata('message', $this->lang->line('invalid_old_pw'));
			
					redirect('vet_login','location');			
	
				}
				else
				{
					
				
					$data = array(
					'firstname' => $this->input->post('edit_firstname'),
					'lastname'  => $this->input->post('edit_lastname'),
					'title'  => $this->input->post('edit_title'),
					'description'  => $this->input->post('edit_about'),
					'contact'  => $this->input->post('edit_contact'),
					'times'  => $this->input->post('edit_ordination'),		
					'address'  => $address,
					'city'  => $this->input->post('edit_city'),
					'geo_location'  => $this->input->post('edit_geolocation'),
					'login_email'  => $this->input->post('edit_login_email'),
					'login_pw'  => $this->my_hash($new_pw)
					);
					
					// set array for new cover and profile picture
				
					$this->auth_model->updateVet($vet_id, $data);
		
		
					//check to see if we are creating the user
					//redirect them back to the admin page
					$this->session->set_flashdata('message', $this->lang->line('edit_user_profile_change'));
					redirect("vet_login", 'refresh');
				}
			}
			else
			{
				
					$data = array(
					'firstname' => $this->input->post('edit_firstname'),
					'lastname'  => $this->input->post('edit_lastname'),
					'title'  => $this->input->post('edit_title'),
					'description'  => $this->input->post('edit_about'),
					'contact'  => $this->input->post('edit_contact'),
					'times'  => $this->input->post('edit_ordination'),		
					'address'  => $address,
					'city'  => $this->input->post('edit_city'),
					'geo_location'  => $this->input->post('edit_geolocation'),
					'login_email'  => $this->input->post('edit_login_email')
					);
					
					// set array for new cover and profile picture
				
					$this->auth_model->updateVet($vet_id, $data);
		
		
					//check to see if we are creating the user
					//redirect them back to the admin page
					$this->session->set_flashdata('message', $this->lang->line('edit_user_profile_change'));
					redirect("vet_login", 'refresh');
				
			}
		}
		else
		{
		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		$this->session->set_flashdata('message', $this->data['message']);
		
		redirect('vet_login','location');			
		}
	}
	
	
	
	
	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function _render_page($view, $data=null, $render=false)
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $render);

		if (!$render) return $view_html;
	}

}

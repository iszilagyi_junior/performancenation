<script>
	$(document).ready(function()
	{	
		gallery_file_upload();
		$('.ajax-file-upload').css({'position': 'absolute', left:0});
	});
</script>

<div id="tag_holder" pic="">	
	<div class="tag_overlay_close">x</div>
	<div class="tag_overlay_head" style=""><?= $this->lang->line('tags')?></div>
	<div id="already_tagged_holder">
		<div class="" style="color:#d4003d;font-family:'open_sanssemibold';margin:10px 0px;font-size:14px;"><?= $this->lang->line('already_tagged')?></div>
		<div id="already_tagged_friends">
			
		</div>
	</div>	
	<div id="tag_holder_top" style="clear:left;">
		<div style="margin-bottom:10px;"><?= $this->lang->line('search')?></div>
		<input type="text" name="tag_search_input" id="tag_search_input" placeholder="<?= $this->lang->line('type_in_name')?>"/>
	</div>
	<div id="tag_holder_bottom">
		<div id="tag_search_results">
			<? foreach($home_friends as $friend){?>
				<div class="tag_friend_item" dog="<?= $friend['id'];?>">
					<div class="tag_friend_img_holder">
						<img class="tag_item_image" src="<?= site_url('items/uploads/profilepictures/'.$friend['fname']);?>">						
					</div>
					<div class="tag_friend_name"><?= $friend['name'];?></div>
				</div>
			<? }?>
		</div>			
	</div>
</div>

<div id="comment_holder" pic="">	
	<div class="tag_overlay_close">x</div>
	<div class="tag_overlay_head" style=""><?= $this->lang->line('comments')?></div>
	<div id="add_comment_holder">
		<div class="regular" style="color:#ffffff;margin:10px 0px;font-size:14px;">Add Comment</div>
		<textarea id="comment_photo_input" name="comment_photo_input" ></textarea>
		<div id="add_comment_btn" class="button" style="float:none;top:5px;width:100%;">ADD</div>
	</div>
	<hr style="border:0px;height:1px;background:#fff;width:50%;margin:15px 25% 5px 25%;"/>	
	<div id="user_comments">
					
	</div>
</div>

<div id="content" style="background:none; width:800px;left:200px;">
	<? if($album->newsfeed != 1 && $album->instagram != 1){ ?>
		<div class="statics_subheader" style="font-size:24px;">
			Edit Album
		</div>
		<a href="<?= site_url('edit_vehicle/'.$album->vehicle_id);?>">
			<div class="button" style="width: 75px;position: absolute;top: 70px;left: 20px;z-index:300;">BACK</div>				
		</a>
		<?php 
			
			$data = array('id' => 'edit_album_form');
			echo form_open("statics/edit_album_info", $data);?>
		<input type="text" id="album_title" name="album_title" placeholder="Album title..." class="edit_input" style="margin-top:60px;" value="<?= $album->title;?>">
		<br/>
		<textarea type="text" id="album_about" name="album_about" placeholder="About..." class="edit_input" style="height:81px;resize:none;"><?= $album->about;?></textarea>
		<br/>
		<input type="hidden" name="album_id" value="<?= $album->id;?>"></input>
		<input type="submit" id="edit_album" style="width:256px;left:0px;line-height:25px;margin:0px;" class="button regular" value="UPDATE" />

		
		
	
	<? }?>
	<?php echo form_close();?>
	<? if($album->newsfeed != 1 && $album->instagram != 1){ ?>
	
	
	<div id="upload_photo" class="button" style="margin-top:0px;position:absolute;">UPLOAD</div>
	<input type="file" preview="" accept="image/*" id="upload_picture" name="upload_picture" album_id="<?= $album->id;?>"  cid="<?= $album->vehicle_id;?>" class="edit_hidden_upload">

	<div id="remove_album_holder" style="">
		<div id="remove_album" album_id="<?= $album->id;?>" style="width:210px;top:21px;margin-right:0px;" class="button">DELETE ALBUM</div>
		<div id="delete_album_confirm_holder">
			<div id="delete_confirm_text" class="sansitalic">Are you sure you wish to delete this album and all of its images?</div>
			<div class="button " style="float:left;width:85px;top:14px;" album_id="<?= $album->id;?>" id="album_delete_yes">YES</div>
			<div class="button " style="float:right;width:85px;top:14px;" id="album_delete_no">NO</div>
		</div>
	</div>
	
	<!--
		<div class="statics_subheader" style="margin-top:0px;font-size:20px;">Images</div>
	-->
	<? }
	else{?>
		<div class="statics_subheader" style="">
		NEWSFEED UPLOADS
		<a href="<?= site_url('albums');?>">
			<div class="button_skin" style="width: 75px;position: absolute;top: 45px;left: 220px;z-index:300;"><?= $this->lang->line('user_album_back_button')?></div>				
		</a>	
	</div>
	<?}?>	
	<?php if($this->session->flashdata('error')){echo $this->session->flashdata('error');}?>
	
	
	<? if($album->newsfeed != 1 && $album->instagram != 1){ ?>
	
		<div id="notIE9">
			<?php 
			$data = array('id' => 'add_photo_form');
			// echo form_open_multipart("statics/add_photo", $data);?>
			<div id="add_photo_btn">
				<div id="add_photo_btn_text"><?= $this->lang->line('albums_add_photo')?></div>
				<input type="hidden" name="album_id" value="<?= $album->id;?>"></input>
			</div>
			<input type="file" preview="add_photo_btn" accept="image/*" id="add_photo_upload_btn" name="add_photo_upload_btn" style="display:none;">
			
		</div>
		<div id="IE9" style="display:none;margin-top:10px;height:30px;">
			<div id="add_photo_btn_text" style="margin-right:20px;"><?= $this->lang->line('albums_add_photo')?></div>
			<?php 
			$data = array('id' => 'add_photo_ie');
			echo form_open_multipart("statics/add_photo_to_album_ie", $data);?>
				<input type="file" preview="add_photo_btn_newsfeed" accept="image/*" id="add_photo_upload_btn_ie" name="myfile" style="">
				<input type="hidden" name="album_id" value="<?= $album->id?>" />
			<? echo form_close();?>
		</div>
	
	
	
	
	
	
	<?php // echo form_close();
	}?>	
	<br>
	<div id="pictures_holder">
		<? if($count_pictures == 0){?>
			<div class="no_photo_text">You haven`t uploaded anything into this album.</div>
		<? }?>
		<?if(isset($pictures)){
			$x = 1;
			foreach($pictures as $pic){?>
					<div class="album_item_outer" <? if($x%4 == 0) echo 'style="margin-right:0px;"'?>>					
						<div class="album_item" >
						
						<a class="image_click" pic="<?= $pic['id']; ?>" pic_id="<?= $pic['id']; ?>" title="<?= $pic['title']; ?>" href="<?= site_url('items/uploads/'.$pic['folder'].'/'.$pic['fname']);?>" data-lightbox="pictures">
							<div class="album_item_image_holder" style="background-image: url('<?= site_url('items/uploads/'.$pic['folder'].'/'.$pic['fname']);?>');"></div>
						</a>							
						
						
						<div class="title_editor" pid="<?= $pic['id']; ?>">
							<input type="text" class="photo_title_input" maxlength="255" placeholder="Title..." value="<?= $pic['title'];?>"/>
							<div class="photo_title_save button" style="width: 190px;margin-top: 0px;">SAVE</div>
						</div>
						<div class="delete_editor" >	
							<div>Are you sure?</div>
							<div style="overflow:hidden">
								<div class="photo_delete_yes button_skin" pid="<?= $pic['id']; ?>" style="width: 80px;margin-top: 3px;float:left;">YES</div>
								<div class="photo_delete_no button_skin" style="width: 80px;margin-top: 3px;float:right;">NO</div>
							</div>					
							
						</div>
						
						
						
						<div class="album_image_text_holder">
							<img class="title_hover" src="<?= site_url('items/frontend/img/photo_icons_edit_white-01.png')?>" />
							<img class="remove_image" pid="<?= $pic['id']; ?>" style="" src="<?= site_url('items/frontend/img/photo_icons_delete_white-01.png')?>" />
							<? if($pic['already'] == 0){?>
							<img class="like_image" pid="<?= $pic['id']; ?>" style="" src="<?= site_url('items/frontend/img/photo_icons_love_white-01.png')?>" />
						<? }?>
						
						
							<img class="comment_image" pid="<?= $pic['id']; ?>" style="" src="<?= site_url('items/frontend/img/photo_icons_comment_white-01.png')?>" />
							<div class="image_like_holder" style="position:absolute;top:40px;left:0px;width:100%;text-align:center;" pid="<?= $pic['id']; ?>" style="">LIKES: <span class="the_likes"><?= $pic['likes']; ?></span></div>
						</div>
					</div>
					</div>
					
		<? 
		$x++;
		}
			} ?>
		
	</div>
	
	
	
	
</div>
<script>
	$('#sidemenu').css({top: 35});
</script>
			<div id="content">
			
			<? if(count($featured)>0):?>
			<script>
				$('#sidemenu').css({top: 15});
			</script>			
				<div id="featured_vets_holder">
					<? $x = 1; foreach($featured as $doc){?>
						<div class="featured_vets_list_item" <? if($x%3 == 0){ echo "style='margin-right:0px'";}?>>
							<img class="vets_list_item_image" src="<?= site_url('items/uploads/profilepictures/'.$doc->profile_picture) ?>" border="0">
								<a href="<?= site_url('sitters/' . $doc->id)?>">							
										
									<div class="div_white_over"></div>
									<div class="featured_text"><?= $this->lang->line('sitters_featured_text')?></div>
									<div class="vets_list_item_desc">
										<?= $doc->first_name . ' ' . $doc->last_name ?>
									</div>
								</a>
							</div>
							
				<? $x++; }?>
				
				</div>
				
			<? endif;?>
			<br/>
				<div id="vets_nearby_holder" style="margin-top:18px;">
					<div class="statics_subheader"><?= $this->lang->line('sitters_nearby')?></div>
					<div class="sansitalic" style="text-align:center;margin:20px 0px;"><?= $this->lang->line('sitters_nearby_desc')?></div>
					<hr class="hline">
					<div id="closest_sitters_btn"><?= $this->lang->line('closest_sitters')?></div>
					<div id="closest_sitters_btn_search" style="margin-right:15px;"><?= $this->lang->line('profile_search_submit')?></div>
					<input type="text" id="sitter_location_input" name="sitter_location_input" placeholder="<?= $this->lang->line('location')?>" defaultText="<?= $this->lang->line('location')?>" >
					
					<div class="separator" style="width:172px;height:26px;clear:both;">
						<hr class="hline" style="float:left;margin-top:27px;width:50px">
						<span class="sansitalic" style="float:left;margin:10px 20px;margin-top:20px;"><?= $this->lang->line('register_or')?></span>
						<hr class="hline" style="float:left;margin-top:27px;width:50px">
					</div>
					<div style="width:100%;height:50px;overflow:hidden">
						<input type="text" id="sitter_search" name="sitter_search" placeholder="<?= $this->lang->line('sitter_name')?>" defaultText="<?= $this->lang->line('vet_name')?>" class="search_input">
						<div id="closest_sitters_name_search" style="margin-right:15px;"><?= $this->lang->line('profile_search_submit')?></div> 
					</div>
					<div class="sansitalic" style="text-align:center;margin:30px 0px;"><?= $this->lang->line('results')?></div>
					
					<div id="vets_list"></div>
				</div>
				
				

				
				
			
			</div>
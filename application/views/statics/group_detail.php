<script>	
	
	$("meta[property='og\\:image']").attr("content", "<?= site_url('items/uploads/profilepictures/' . $group->logo);?>");
	
	$(document).ready(function()
	{	
		
		setTimeout(function(){
			initGroupPkry();
			selectize_invite_search();
			
			var event = "<?= $group_geo;?>";
			
			<? if($group_geo !== NULL && $group_geo != "" && $group_geo != "0, 0"):?>
				initMapEvent(event);
			<? endif;?>
			
		},200);
	});

	
</script>
<? if(!$is_ie){?>
	<div id="image_cropping_overlay">
		<div class="" id="crop_title" style="position:relative;top:0px;text-align: center;font-size:22px;">Cover Picture</div>
		<div class="sansitalic" style="margin:5px auto;text-align: center;">Please select an area</div>
		<div id="image_crop_holder">
			
		</div>
		 <input type="hidden" id="selection_x1" name="selection_x1" value="" />
		 <input type="hidden" id="selection_x2" name="selection_x2" value="" />
		 <input type="hidden" id="selection_y1" name="selection_y1" value="" />
		 <input type="hidden" id="selection_y2" name="selection_y2" value="" />
		<div class="button" type="user" eid="0" id="finish_crop_event" image_type="" style="left:255px;position: absolute;">FINISH</div>
		<div class="button" id="close_crop"  style="left:545px;position: absolute;" >CLOSE</div>
	</div>
<? } ?>
<div id="content" style="width:800px;margin-bottom: 280px;color:#ffffff;min-height:600px;">

	<div id="BkgEr" style=""></div>
		<? if($member_of_grp || $user_admin): ?>	
			<div id="invite_overlay">
				<div class="overlayBG"></div>
				<div style="position:relative;z-index:100">
					<img class="close_overlay" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />
					<div class="titboldit" style="font-size:27px;margin-top:30px;">INVITE TO GROUP</div>
					<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">
							
							<div style="overflow:hidden;">
								<div class="button" style="float:left;width:180px;" id="invite_followers_group_button" gid="<?= $group->id;?>">INVITE ALL FOLLOWERS</div>						
								<div class="regular" style="float:left;margin-left:30px;margin-top:30px;">or add vehicles:</div>
							</div>
							
							<div style="position:relative;left: 0px;top: 0px;margin-bottom:20px;">
								<select id="invite_vehicle_select" name="invite_vehicle_select" placeholder="Type in name..."></select>
							</div>
							
					</div>
					<div class="button" id="invite_group_button" gid="<?= $group->id;?>">SEND</div>
					<div class="regular" id="group_event_error_message"></div>
				</div>
			</div>
		<? endif; ?>	
			
			
			
			<?  if($user_admin){?>
				
				<script>	
	
					$(document).ready(function()
					{	
						$('#group_event_date').datetimepicker();
						initAutocomplete(false, 'group_event_address');
					});
					
				</script>
				
				<div id="group_event_overlay">
					<div class="overlayBG"></div>
					<div style="position:relative;z-index:100">
						<img class="close_overlay" style="top:0px" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />
						
						<div class="titboldit" style="font-size:27px;margin-top:30px;">CREATE EVENT</div>
						<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">
							<table style="width:400px;margin:30px auto;">
								<tr>
									<td style="regular">
										Title
									</td>
									<td>
										<input type="text" placeholder="Title" class="edit_input" name="group_event_title" id="group_event_title"></input>
									</td>
								</tr> 
								
								<tr>
									<td style="regular">
										Address
									</td>
									<td>
										<input type="text" onFocus="geolocate()" placeholder="Title" class="edit_input" name="group_event_address" id="group_event_address"></input>
									</td>
								</tr> 
								
								<tr>
									<td style="regular">
										Description
									</td>
									<td>
										<textarea  placeholder="Description" class="edit_input" name="group_event_description" id="group_event_description" style="resize:none;height:60px;"></textarea>
									</td>
								</tr>
								
								<tr>
									<td style="regular">
										Date
									</td>
									<td>
										<input type="text" placeholder="Date" class="edit_input" name="group_event_date" id="group_event_date"></input>
									</td>
								</tr>
							<input type="hidden"  class="edit_input" name="preupload_cover" id="preupload_cover"></input>
							<img style="display:none;width:20%;margin:5px auto;" id="preupload_cover_holder" />
							<div class="button" id="user_event_image" eid="">UPLOAD COVER</div>
						</table>
						<input type="file" preview="edit_user_cover_upload" accept="image/*" id="edit_user_cover_picture" name="edit_user_cover_picture" class="edit_hidden_upload">
						</div>
						<div class="button" id="create_event_button" gid="<?= $group->id;?>">CREATE</div>
					
						<div class="regular" id="group_event_error_message"></div>
					</div>
				</div>
				
			
				
				<a style="text-decoration:none;" href="<?= site_url('edit_group/'.$group->id);?>">
					<div class="button" style="position:absolute;left: 24px;top: 410px; width:120px;">EDIT</div>
				</a>
				
				
				<div class="button create_event" style="position:absolute;left: 170px;top: 410px; width:120px;">CREATE EVENT</div>
				
				
				
				
			<? }
				else{
			?>
					<div id="join_group" cid="<?= $group->id;?>" class="button" style="<? if($member_of_grp){ echo 'display:none';}?>">JOIN GROUP</div>
					<div id="leave_group" cid="<?= $group->id;?>" class="button" style="<? if($member_of_grp){ echo 'display:block';}?>">LEAVE GROUP</div>
					
					
					
					
			<? }?>
				<? if($member_of_grp || $user_admin): ?>	
					<div class="button invite_group" style="position:absolute;left: 316px;top: 410px; width:120px;">INVITE</div>					
				<? endif; ?>
				<img class="car_cover_photo" src="<?= site_url('items/uploads/coverpictures/'. $group->cover_image )?>" border="0">
				<img class="" style="position:absolute;left:0px;top:342px;" src="<?= site_url('items/frontend/img/big_chrome.png')?>" border="0">
				<div class="playbold gold page_title" style="width:800px;text-align:center;font-size:40px;letter-spacing:1px;z-index:10;position:absolute;top:350px;"><?= $group->name;?></div>

				
				
			<!--	<img class="car_profile_photo" src="<?= site_url('items/uploads/profilepictures/'. $group->logo )?>" border="0"> -->
				<div class="car_detail_make regular"></div>
				<div class="car_detail_owner dosissemi"></div>		
				
				<div style="overflow:hidden;margin-top:30px;margin-bottom:30px;">
					<div class="detail_left" style="margin-top:50px;width:500px;">
						
						<div class="user_section" style="width:485px;">
							<div class="dosisbold">ADDRESS</div>
							<div class="dosisextralight"><?= nl2br($group->address);?></div>
						</div>
						
						<div class="user_section" style="width:485px;">
							<div class="dosisbold">DESCRIPTION</div>
							<div class="dosisextralight"><?= nl2br($group->description);?></div>
						</div>
					</div>
					
					<div class="detail_right" style="width:246px;margin-right:19px;">
						<div class="dosissemi" style="overflow:hidden;"><div style="float:left;">MEMBERS</div> <div style="float:right;font-size:12px;"><?= count($members)?> Members</div></div>
						<div id="gallery_images" style="width:100%;margin-left:0px;">
							<? foreach($members as $member):?>
								<a href="<?= site_url('vehicle_profile/'.$member['pretty_url']);?>">
									<div class="member_item">
										<img class="member_item_img" src="<?= site_url('items/uploads/profilepictures/'.$member['profile_image']);?>" />
									<!--	<div class="member_item_title"><?= $member->username;?></div> -->
									</div>
									
								</a>
								
							<? endforeach;?>
						</div>
						
						<? if($group_geo !== NULL && $group_geo != "" && $group_geo != "0, 0"):?>
							<div id="event_map" style="margin-top:20px;width:245px;height:200px;"></div>
						<? endif;?>	
					</div>
					<br/>
				<? if($member_of_grp || $user_admin): ?>
					
					
					
			
					</div>
					
					<img class="" style="position:relative;left:0px;top:0px;width:800px" src="<?= site_url('items/frontend/img/small_chrome.png')?>" border="0">
					<? if($member_of_grp || $user_admin):?>
						<div style="position:relative">
							<div id="show_events" cid="<?= $group->id;?>" class="button" style="position:absolute;right:20px;width:180px;top:-110px;">SEE EVENTS</div>
							<div id="show_posts" cid="<?= $group->id;?>" class="button" style="display:none; position:absolute;right:20px;width:180px;top:-110px;">SEE POSTS</div>
						</div>
					<? endif;?>
					
					<div id="post_holder">
						<div class="playbold" style="color:#000000;position:relative;top:-40px;letter-spacing:4px;font-size:30px;text-shadow: 1px 1px 1px rgba(150, 150, 150, 1);">POSTS</div>
						<div class="textarea_bg" >
							<textarea class="group_comment_input" gid="<?= $group->id ?>" placeholder="Write your message here..." name="group_input"></textarea>
							
							<input type="file" preview="" accept="image/*" id="post_image" name="cover_image" style="position:absolute;right:28px;top:60px;width:180px;">
							<div class="button group_comment_send" fname="">SEND</div>
						</div>
						
						
						<div class="story_holder" page="posts"  style="margin-top:20px;">
							<?  foreach($posts as $post):?>
	
										<div class="group_post_item " >
								
											<div class="group_post_content">
													<img class="group_post_img" src="<?= site_url('items/uploads/profilepictures/'.$post['profile_image']);?>">								
													<div class="group_post_text_holder" style="height:auto;">
															<div class="group_post_title"style="color:#ffffff;width: 155px;margin-left:100px;"><?= $post['nickname'];?></div>
															
															<div class="group_post_text"><?= $post['text']?></div>
															
														
													</div>	
													 <div class="group_post_time" style="position:absolute;right:20px;top: 0px;color: #ffffff;"><?= $post['created_date'];?></div> 
												
												<div class="eventFade"></div>
																				
												<? if($post['image'] != "" && $post['image'] !== NULL){ ?>	
								
													<img class="group_post_image_holder" src="<?= site_url('items/uploads/images/'.$post['image']);?>" />
													
												<? 	
												}
												?>
											</div>
										</div>
	
							<? endforeach;?>
						</div>
					</div>
					
					
					
					<div id="event_holder">
						
						<div class="playbold" style="color:#000000;position:relative;top:-40px;letter-spacing:4px;font-size:30px;text-shadow: 1px 1px 1px rgba(150, 150, 150, 1);">EVENTS</div>
					
						<div class="story_holder" page="events"  style="margin-top:20px;">
							
							<?  foreach($events as $post):?>
	
										<div class="group_post_item " >
								
											<div class="group_post_content">
												<a style="text-decoration:none;" href="<?= site_url('group_event/'.$post['id']);?>"											
													<div class="group_post_text_holder">
															<span class="group_post_title " style="color:#ffffff"><?= $post['title'];?></span><br/>
															
															<br/> 
															<?= $post['description']?>
															<br/>
															<br/>
														 <div class="group_post_time" style="text-align:center;">At: <?= $post['address'];?></div>
														 <div class="group_post_time" style="text-align:center;">Starting: <?= $post['start_time'];?></div> 
													</div>
													<div class="" style="text-align:center;font-family: 'dosisbold';margin-top:10px;font-size:14px;">CLICK HERE TO JOIN</div>	
												</a>
												
												<div class="eventFade"></div>
																				
												<? if($post['image'] != "" && $post['image'] !== NULL){ ?>	
								
													<div class="group_post_image_holder" style="background-image: url('<?= $post['image']?>');"></div>
													
												<? 	
												}
												?>
											</div>
				
							<? endforeach;?>
						</div>
					</div>
				<? endif;?>
</div>
//Google maps
var zoneMarker;


$(document).ready(function()
{	
	initMap();
});

function initMap() {
    var mapCanvas = document.getElementById('zone_map_canvas');
    var arr = zone_geo_location.split(',');
	var lat = parseFloat(arr[0]);
	var lng = parseFloat(arr[1]);
    var mapOptions = {
     center: new google.maps.LatLng(lat, lng),
     zoom: 12,
     mapTypeId: google.maps.MapTypeId.ROADMAP,
     mapTypeControl: false,
	 zoomControl: true,
	 zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL,
        position: google.maps.ControlPosition.LEFT_CENTER
	 },
	    scaleControl: true,
	    streetViewControl: false,
	    streetViewControlOptions: {
	        position: google.maps.ControlPosition.LEFT_BOTTOM
	    }
    }
    var map = new google.maps.Map(mapCanvas, mapOptions);
	zoneMarker = new google.maps.Marker({position: new google.maps.LatLng(lat, lng), map: map});

}
			<div id="content">
				<div id="dog_overview">
					<?php $x++; foreach ($dogs->result() as $dog): ?>
						<div class="nearby_list_item" <? if($x%3 == 0){ echo "style='margin-right:0px'";}?> >
							<img class="vets_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $dog->profile_picture)?>" border="0">
							<div class="div_white_over"></div>
							<div class="zones_list_item_desc" style="top:90px;">
								<?= $dog->name;?>
							</div>
						</div>
					<?php endforeach; ?>
					<div class="dog_overview_dog">
						<a href="<?= site_url('new_dog')?>">
							<div class="dog_overview_dog_overlay"><p><?= $this->lang->line('dog_overview_new_dog') ?></p></div>
							<img src="<?= site_url('items/frontend/img/sidemenu_logo.png')?>" width="100%" border="0">
						</a>
					</div>
				</div>
			</div>

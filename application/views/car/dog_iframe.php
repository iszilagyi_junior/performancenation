	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, user-scalable=no">
		<meta property="og:title" content="Snoopstr"/>
		<meta property="og:image" content="<?= site_url('items/frontend/img/sidemenu_logo.png')?>"/>
		<meta property="og:site_name" content="<?= site_url('')?>"/>
		<meta property="og:description" content="Soopstr is the first social network for animals. "/>

		<title>Snoosptr</title>
		
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/frontend.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/dog.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/auth.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/statics.css');?>" media="screen" />
		
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/jquery.datepick.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/jquery.timepicker.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/lightbox.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/jquery.qtip.min.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/eventCalendar.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/eventCalendar_theme_responsive.css');?>" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?= site_url('items/frontend/css/upload.css');?>" media="screen" />
		
		
		<link rel="shortcut icon" type="image/png" href="<?= site_url('items/frontend/img/favicon.ico');?>"/>
		<script>
			var rootUrl = "<?= site_url();?>";
		</script>
		<script src="<?= site_url("items/general/js/jquery-2.0.3.min.js"); ?>" type="text/javascript"></script>
		
		<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=geometry"></script>
	
		
		<script>
		   window.fbAsyncInit = function() {
		     // init the FB JS SDK
		     FB.init({
		       appId      : '310875745734656',                    // App ID from the app dashboard
		       status     : true,                                 // Check Facebook Login status
		       cookie  	  : true, 								  // enable cookies to allow the server to access the session
		       xfbml      : true                                  // Look for social plugins on the page
		     });		    	
		     // Additional initialization code such as adding Event Listeners goes here
		   };
		 
		   // Load the SDK asynchronously
		   (function(d, s, id){
		      var js, fjs = d.getElementsByTagName(s)[0];
		      if (d.getElementById(id)) {return;}
		      js = d.createElement(s); js.id = id;
		      js.src = "//connect.facebook.net/en_US/all.js";
		      fjs.parentNode.insertBefore(js, fjs);
		    }(document, 'script', 'facebook-jssdk'));
		    
		   //
		 </script>

	</head>
	<body>						
			<div id="iframe_content">			
				<div id="iframe_header_overlay">
					<img style="position: absolute;left: 20px;top: 10px;" src="<?= site_url('items/frontend/img/snoopstr_logo.png')?>" />					
				</div>
				<div id="vets_details">
					<div id="edit_left">
						<div class="overview_left" class="statics_half" style="overflow:hidden;">
							<div id="iframe_dog_details_desc">					
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_new_name');?></div>
									<div class="detail_line"><?= $dog->name;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_new_nickname');?></div>
									<div class="detail_line"><?= $dog->nickname;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_new_breed');?></div>
									<div class="detail_line"><?= $dog->breed_name;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_gender');?></div>
									<div class="detail_line"><? if($dog->gender == 0) echo $this->lang->line('gender_male'); else echo $this->lang->line('gender_female');?></div>
								</div>																
							</div>
							
							<!--
							<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_friends');?>:</div>								
							</div>
							<div class="iframe_profile_thumb_holder">
								<? for($i = 0;$i<$friends_total;$i++){ 
									if($i<5){
									?>
									<a title="<?= $friends[$i]->name;?>" href="<?= site_url('dog/'.$friends[$i]->id);?>"><div class="iframe_friend_thumb">
										<img class="iframe_overview_profile_thumb" src="<?= site_url('items/uploads/profilepictures/'.$friends[$i]->profile_picture);?>" /></div>	
									</a>							
								<?  }
								   }
								?>
									
							</div> -->
						</div>
						
						
						
						
						
					</div>						
														
				</div>
				<a target="_blank" href="<?= site_url('profile/'.$dog->pretty_url);?>">
					<div id="iframe_picture_holder">
						<img id="iframe_detail_profile_picture" src="<?= site_url('items/uploads/profilepictures/' . $dog_profile)?>" border="0">				
					</div>
					<div id="iframe_link_page">
						<div id="iframe_button_skin" style="width: 270px;margin-bottom: 13px;"><?= $this->lang->line('iframe_show_dog_text');?></div>				
					</div>
				</a>
			</div>
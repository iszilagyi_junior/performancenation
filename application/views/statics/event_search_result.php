<? if($vehicles_count > 0){?>
	<div id="result_list">
	<? $x = 1; foreach($events as $event):?>
	<a  style="text-decoration:none;" href="<?= site_url($event['type'].'_event/' . $event['id'])?>">	
		<div class="result_list_item" style="<? if($x%2 == 0){ echo "margin-right:0px";}?>">	
				<img class="result_item_image" src="<?= site_url('items/uploads/'.$event['logo'])?>" style="">
				<div class="div_white_over"></div>
				<div class="result_item_text" style=""><?= $event['title'];?><br/><?= $event['address'];?><br/><?= $event['start'];?><br/><br/>Members: <?= $event['participants']?></div>				
			
		</div>
	</a>
	<? $x++; endforeach;?>
	
	</div>		
<? }
else
{?>
	<div class="regular" style="text-align:center">No events found near you!<br/>Increase the search distance or add a different location!</div>
<? }?>
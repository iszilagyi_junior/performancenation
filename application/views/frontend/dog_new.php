			<div id="content">
				<div id="dog_new_header" class="content_header">
					<?= $this->lang->line('dog_new_header')?>
				</div>
				<div id="login_error_message">
					<?php if(isset($message)) echo $message;?>
				</div>
				<div id="login_form">
					<?php 
						$data = array('id' => 'register_form');
						echo form_open("auth/register_user", $data);
					?>
					<input type="text" id="register_email" name="register_email" 
						value="<?php if(isset($email_repop) && $email_repop != "") echo $email_repop; else echo $this->lang->line('register_email')?>" 
						defaultText="<?= $this->lang->line('register_email')?>" 
						class="register_input <?php if(!isset($email_repop) || $email_repop == "") echo "input_default";?>">
					<br>
					<input type="password" id="register_password" name="register_password" class="register_input input_hidden password_real">
					<input type="text" id="register_password_fake" name="register_password_fake" value="<?= $this->lang->line('register_password')?>" defaultText="<?= $this->lang->line('register_password')?>" class="register_input input_default password_fake">
					<br>
					<input type="password" id="register_passconf" name="register_passconf" class="register_input input_hidden password_real">
					<input type="text" id="register_passconf_fake" name="register_passconf_fake" value="<?= $this->lang->line('register_passconf')?>" defaultText="<?= $this->lang->line('register_passconf')?>" class="register_input input_default password_fake">
					<br>
					<br>
					<br>
					<input type="text" id="register_firstname" name="register_firstname" 
						value="<?php if(isset($firstname_repop) && $firstname_repop != "") echo $firstname_repop; else echo $this->lang->line('register_firstname');?>" 
						defaultText="<?= $this->lang->line('register_firstname')?>" 
						class="register_input <?php if(!isset($firstname_repop) || $firstname_repop == "") echo "input_default";?>">
					<br>
					<input type="text" id="register_lastname" name="register_lastname" 
						value="<?php if(isset($lastname_repop) && $lastname_repop != "") echo $lastname_repop; else echo $this->lang->line('register_lastname')?>" 
						defaultText="<?= $this->lang->line('register_lastname')?>" 
						class="register_input <?php if(!isset($lastname_repop) || $lastname_repop == "") echo "input_default";?>">
					<br>
					<input type="text" id="register_birthday" name="register_birthday" readonly
						value="<?php if(isset($birthday_repop) && $birthday_repop != "") echo $birthday_repop; else echo $this->lang->line('register_birthday')?>" 
						defaultText="<?= $this->lang->line('register_birthday')?>" 
						class="register_input <?php if(!isset($birthday_repop) || $birthday_repop == "") echo "input_default";?>">
					<br>
					<br>
					<img class="register_arrow pseudo_form_submit" src="<?= site_url('items/frontend/img/register_arrow.png');?>">
					<div class="register_arrow_text pseudo_form_submit" action="register"><?= $this->lang->line('register_confirm')?></div>
					<?php echo form_close();?>	
				</div>
			</div>
<script>	
	$('#sidemenu').css({top: 35});
	$("meta[property='og\\:image']").attr("content", "<?= site_url('items/uploads/profilepictures/' . $dog_profile);?>");
</script>
<div id="content">
	<div id="BkgEr" style=""></div>			
				<?if($dog->cover_picture!=""){?>
					<script>	
						$('#sidemenu').css({top: 35});
					</script>
					<div id="vet_detail_header">
						<div id="vet_detail_header_cover">
							
								<img class="dog_cover_photo" src="<?= site_url('items/uploads/coverpictures/' . $dog_cover)?>" border="0">
							
							
							
						</div>
					</div>
				<? }
					else
					{ ?>
						<script>	
							$('#sidemenu').css({top: 0});
						</script>
					<? }
				?>
				<div class="statics_subheader" style="<?if($dog->cover_picture=="") echo "margin-top:35px;";?>">
					<?= $dog->name; 
						if($dog->lost==1){ 
							echo  $this->lang->line('dog_is_missing');;
						}
					 ?>
				</div>		
				<div id="vets_details">
					<div id="edit_left">
						<div class="overview_left" class="statics_half">
							<div class="activityAbout" style="width:100%;"><?= $this->lang->line('dog_overview_facts');?></div>
							<div id="dog_details_desc">
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_owner');?></div>
									<div class="detail_line"><?= $owner->first_name." ".$owner->last_name;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_new_name');?></div>
									<div class="detail_line"><?= $dog->name;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_new_nickname');?></div>
									<div class="detail_line"><?= $dog->nickname;?></div>
								</div>
								<hr class="overview_line">
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_new_breed');?></div>
									<div class="detail_line"><?= $dog->breed_name;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_gender');?></div>
									<div class="detail_line"><? if($dog->gender == 0){ echo $this->lang->line('gender_male');}else{ echo $this->lang->line('gender_female');}?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_birthday');?></div>
									<div class="detail_line"><?= $dog->birthday;?></div>
								</div>
								<hr class="overview_line">
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_color');?></div>
									<div class="detail_line"><?= $dog->color;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_coat');?></div>
									<div class="detail_line"><?= $dog->coat;?></div>
								</div>	
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_type');?></div>
									<div class="detail_line"><?= $dog->type;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_size');?></div>
									<div class="detail_line"><?= $dog->size;?></div>
								</div>			
								<hr class="overview_line">
								<div class="relative_parent" style="height:50px;">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_characteristics');?></div>
									<div class="detail_line"><?= $dog->characteristics;?></div>
								</div>											
							</div>
						</div>
						<div class="overview_left" class="statics_half">
							<div class="activityAbout" style="width:100%;"><?= $this->lang->line('dog_overview_medical');?></div>
							<div id="dog_details_times" style="padding-bottom:15px;overflow:hidden;">
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_edit_chip');?>	</div>
									<div class="detail_line"><?= $dog->chip_nr;?></div>	
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_doctor');?>	</div>
									<div class="detail_line"><?= $dog->doctor;?></div>
								</div>
								<div class="relative_parent">
									<div class="dog_details_subheader"><?= $this->lang->line('dog_vaccine');?>	</div>
									<div class="detail_line"><?= $dog->vaccines;?></div>
								</div>											
							</div>								
						</div>
						<div class="overview_left" class="statics_half">
							<div class="activityAbout" style="width:100%;"><?= $this->lang->line('dog_overview_about');?></div>
							<div id="dog_details_desc" style="padding-top:10px;">													
								<div class="detail_line" style="position:relative;padding-bottom:80px;left:0px;top:0px;font-family: 'open_sansregular';margin-left:20px;width:260px;"><?= $dog->about;?></div>					
							</div>
						</div>	
					</div>	
					<div id="edit_right">
						<div class="overview_right" class="statics_half">
							<img id="detail_profile_picture" src="<?= site_url('items/uploads/profilepictures/' . $dog_profile)?>" border="0">				
						</div>
						<div class="overview_right" class="statics_half" style="position:relative;">
					
						
							<div class="button_skin" id="embed_dog_overview" style="width: 190px;margin-bottom:10px;"><?= $this->lang->line('embed_profile');?></div>				
						</div>
						<div class="overview_right" class="statics_half" style="position:relative;">
							<div id="embed_holder">				
								<input type="text" id="embed_input" value='<iframe width="480" height="260" src="<?= site_url('dog_iframe/'.$dog->id);?>" frameborder="0" allowfullscreen></iframe>'/>
							</div>
						</div>
						<div class="overview_right" class="statics_half" style="position:relative;margin-bottom:10px;overflow:hidden;width:100%;">
							<img class="" id="dog_detail_tw" dog="<?= $dog->id;?>" src="<?= site_url('items/frontend/img/snoopstr_tw.png')?>" style="float:left;margin-left:18px;cursor:pointer;" border="0">	
							<img class="" id="dog_detail_fb" dog="<?= $dog->id;?>" src="<?= site_url('items/frontend/img/snoopstr_fb.png')?>" style="float:right;margin-right:20px;cursor:pointer;" border="0">	
						</div>
						<div class="overview_right" class="statics_half">
							<div id="dog_overview_friend_holder">
								
									<div class="activityAbout" style="overflow:hidden;width:190px;margin-bottom:10px;"><?= $this->lang->line('dog_friends');?>
										<span style="float:right;">(<?= $friends_total;?>)</span>
		
									</div>
									<br/>								
								<div class="profile_thumb_holder">
								<?  for($i = 0;$i<$friends_total;$i++){ 
									if($i<4){
									
										if(is_numeric($friends[$i]->profile_picture))
										{
											$dogprofile = $this->statics_model->getImageByID($friends[$i]->profile_picture)->row()->fname;
										}
										else
										{
											$dogprofile = $friends[$i]->profile_picture;
										}
									?>
									<a title="<?= $friends[$i]->name;?>" href="<?= site_url('profile/'.$friends[$i]->pretty_url);?>">
										<div class="friend_profile_thumb_holder">
											<img class="overview_profile_thumb" src="<?= site_url('items/uploads/profilepictures/'.$dogprofile);?>" />
										</div>
											
									</a>	
															
								<?  }
								   }
								?>
									
								</div>
								
							<!--	<a href="<?= site_url('enemies/'.$dog->id);?>">
									<div class="activityAbout" style="overflow:hidden;width:190px;margin-bottom:10px;"><?= $this->lang->line('dog_enemies');?>	
										<span style="float:right;">(<?= $enemies_total;?>)</span>	
									</div>
								</a>
								<div class="profile_thumb_holder">
									<? for($i = 0;$i<$enemies_total;$i++){ 
									if($i<4){
									?>
										<a title="<?= $enemies[$i]->name;?>" href="<?= site_url('profile/'.$enemies[$i]->pretty_url);?>">
											<div class="friend_profile_thumb_holder">
												<img class="overview_profile_thumb" src="<?= site_url('items/uploads/profilepictures/'.$enemies[$i]->profile_picture);?>" />	
											</div>	
										</a>							
								<?  }
								   }
								?>
									
								</div>	-->											
							</div>
						</div>	
					</div>						
				</div>
			</div>
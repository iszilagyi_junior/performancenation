<script>	
	
	
	var type= "<?= $type?>";
	$(document).ready(function()
	{	
	
		var savedCountry = "<?= $profiledata->country?>";
	//	switchCountryselect(savedCountry);

	});

</script>

<? if(!$is_ie){?>
	<div id="image_cropping_overlay">
		<div class="" id="crop_title" style="position:relative;top:0px;text-align: center;font-size:22px;">Profile Picture</div>
		<div class="sansitalic" style="margin:5px auto;text-align: center;">Please select an area</div>
		<div id="image_crop_holder">
			
		</div>
		 <input type="hidden" id="selection_x1" name="selection_x1" value="" />
		 <input type="hidden" id="selection_x2" name="selection_x2" value="" />
		 <input type="hidden" id="selection_y1" name="selection_y1" value="" />
		 <input type="hidden" id="selection_y2" name="selection_y2" value="" />
		<div class="button" uid="<?= $this->session->userdata('user_id');?>" id="finish_crop" image_type="" style="left:255px;position: absolute;" dog_id="<?= $dog->id; ?>">FINISH</div>
		<div class="button" id="close_crop"  style="left:545px;position: absolute;" >CLOSE</div>
	</div>
<? }
	else
	{
?>
	
	<script>	

	$(document).ready(function()
	{	
		
		if(type == "coverpictures")
       {
           $('#crop_title').text(cover_image_text);
           $('input[name="selection_x1"]').val(0);
            $('input[name="selection_y1"]').val(0);
            $('input[name="selection_x2"]').val(600);
            $('input[name="selection_y2"]').val(250); 
       $('#image_crop_holder').imgAreaSelect({
            	x1: 0, y1: 0, x2: 600, y2: 250, 
            	aspectRatio: '600:250',
            	minHeight:250, 
            	minWidth:600, 
            	maxWidth:1000,
            	handles: true,
                onSelectEnd: function (img, selection) {
              
		            $('input[name="selection_x1"]').val(selection.x1);
		            $('input[name="selection_y1"]').val(selection.y1);
		            $('input[name="selection_x2"]').val(selection.x2);
		            $('input[name="selection_y2"]').val(selection.y2);            
		        }, 
		    }); 
       }
       else
       {
       		$('#crop_title').text(profile_image_text);
       		$('input[name="selection_x1"]').val(0);
            $('input[name="selection_y1"]').val(0);
            $('input[name="selection_x2"]').val(190);
            $('input[name="selection_y2"]').val(220);
       $('#image_crop_holder').imgAreaSelect({
        	x1: 0, y1: 0, x2: 190, y2: 220, 
            	aspectRatio: '190:220',
            	minHeight:220, 
            	minWidth:190,
            	maxWidth:500, 
            	handles: true,
                onSelectEnd: function (img, selection) {
		            $('input[name="selection_x1"]').val(selection.x1);
		            $('input[name="selection_y1"]').val(selection.y1);
		            $('input[name="selection_x2"]').val(selection.x2);
		            $('input[name="selection_y2"]').val(selection.y2);            
		        }, 
		    });
       }
	});
	
	
	
</script>

	
	<div id="image_cropping_overlay" style="display:block;">
		<div class="" id="crop_title" style="position:relative;top:0px;">Profile Picture</div>
		<div class="sansitalic" style="margin:5px auto;text-align: center;">Please select an area</div>
		<div id="image_crop_holder">
			<img src="<?= site_url("items/uploads/".$type."/".$fname);?>">
		</div>
		 <input type="hidden" id="selection_x1" name="selection_x1" value="" />
		 <input type="hidden" id="selection_x2" name="selection_x2" value="" />
		 <input type="hidden" id="selection_y1" name="selection_y1" value="" />
		 <input type="hidden" id="selection_y2" name="selection_y2" value="" />
		<div class="dog_new_create_arrow_text" uid="<?= $this->session->userdata('user_id');?>" user="true" image_type="<?= $type;?>" image_name="<?= "items/uploads/".$type."/".$fname;?>" file_name="<?= $fname;?>" id="finish_crop" image_type="" style="width:190px;border:0;left:317px;" dog_id="<?= $dog->id; ?>">FINISH</div>
		<div class="dog_new_create_arrow_text" id="close_crop"  style="width:190px;border:0;left:517px;top:-17px;" >CLOSE</div>
	</div>
<? }?>

	
<div id="content" style="width:800px;margin-bottom: 100px;color:#ffffff">
				
				<div id="edit_error_message">
					<?php if(isset($message)) echo $message;?>
				</div>
				<div id="edit_form">
					<?php 
						$data = array('id' => 'change_password_form');
						echo form_open("Auth/change_password", $data);
					?>
					<div style="width:300px;position:relative;left: 360px;top:100px;height: 280px;">
						<input type="password" placeholder="Old password" id="edit_password_old" name="edit_password_old" 
							class="edit_input password_real">
						
						<br>
						<input type="password" placeholder="New password"  id="edit_password" name="edit_password" 
							class="edit_input  password_real">
						
						<br>
						<input type="password" placeholder="Confirm new password"  id="edit_passconf" name="edit_passconf" 
							class="edit_input  password_real">
						
						<br>
						<br>
						
						<input type="submit" class="button regular" value="Update password"  style="top: -30px;position: relative;left: 0px;text-transform:uppercase;line-height: 25px;
}"></input>
					</div>
					<?php echo form_close();?>
					<br style="clear:both;">
					<br/>
					
					<div id="" style="display:none;margin-top:10px;height:30px;">
						<?php 
						$data = array('id' => 'add_profile_ie');
						echo form_open_multipart("statics/add_profile_ie", $data);?>
							<input type="file" preview="add_photo_btn_newsfeed" accept="image/*" id="add_profileimage_ie" name="profile_image" style="display:none;">						
						<? echo form_close(); ?>
					</div>
					
					<br/><br/>
					<br/><br/>
					<?php 
						$data = array('id' => 'edit_user_form');
						echo form_open_multipart("Auth/edit_user", $data);
					?>
					
					<div style="width:300px;float:left;margin:20px 20px;">	
						<div id="user_edit_profile_upload" formfield="dog_edit_profile_picture" class="">
								<img style="width:100%;" src="<?= ($profiledata->profile_picture != "") ? site_url('items/uploads/profilepictures/' . $profiledata->profile_picture) : site_url('items/frontend/img/profilepicture_upload.png')?>" border="0">
							</div>
							<input type="file" preview="user_edit_profile_upload" accept="image/*" id="user_edit_profile_picture" name="user_edit_profile_picture" class="edit_hidden_upload">
						
											
						<input type="text" id="edit_firstname" name="edit_firstname" 
							value="<?= $profiledata->first_name?>" placeholder="Firstname" class="edit_input">
						<br>
						<input type="text" id="edit_lastname" name="edit_lastname" 
							value="<?= $profiledata->last_name?>" placeholder="Lastname" class="edit_input">
						<br>
					<!--	<div><?= $this->lang->line('register_birthday')?></div>
						<input  type="text" id="edit_birthday_month" name="edit_birthday_month" 
							value="<?= $month == NULL ? "MM" : $month ?>" 
							defaultText="<?= $this->lang->line('register_birthday_month')?>" maxlength="2" class="date_short">	
						
						<input type="text" id="edit_birthday_day" name="edit_birthday_day" 
							value="<?= $day == NULL ? "DD" : $day ?>" 
							defaultText="<?= $this->lang->line('register_birthday_day')?>" maxlength="2" class="date_short">
						
						<input type="text" id="edit_birthday_year" name="edit_birthday_year" style="width:69px;"
							value="<?= $year == NULL ? "YYYY" : $year ?>"
							defaultText="<?= $this->lang->line('register_birthday_year')?>" maxlength="4" class="date_long">															
						<br>
						-->
					
						<br>
						
						<input type="text" id="edit_phone" name="edit_phone" 
							value="<?= $profiledata->phone == NULL ? "" : $profiledata->phone ?>" placeholder="Phone" class="edit_input <?= $profiledata->phone == NULL ? 'input_default' : "" ?>">
						<br>
						
						<input type="text" id="edit_skype" name="edit_skype" 
							value="<?= $profiledata->skype == NULL ? "" : $profiledata->skype ?>" placeholder="Skype username" class="edit_input <?= $profiledata->skype == NULL ? 'input_default' : "" ?>">
						<br>
						
						<input type="text" id="edit_fb" name="edit_fb" 
							value="<?= $profiledata->phone == fb_link ? "" : $profiledata->fb_link ?>" placeholder="Facebook username" class="edit_input <?= $profiledata->fb_link == NULL ? 'input_default' : "" ?>">
						<br>
						<input type="text" id="edit_tw" name="edit_tw" 
							value="<?= $profiledata->tw_link == NULL ? "" : $profiledata->tw_link ?>" placeholder="Twitter username" class="edit_input <?= $profiledata->tw_link == NULL ? 'input_default' : "" ?>">
						<br>
						<input type="text" id="edit_insta" name="edit_insta" 
							value="<?= $profiledata->insta_link == NULL ? "" : $profiledata->insta_link ?>" placeholder="Instagram username" class="edit_input <?= $profiledata->insta_link == NULL ? 'input_default' : "" ?>">
						<br />
						<br/>
						<div id="profile_delete_holder">
							<? if(!$under_delete){ ?>
								<div id="profile_delete" class="button" style="margin:5px 20px;">DELETE PROFILE</div>
								<div id="profile_delete_confirm">
									Are you sure you wish to delete your profile?<br/>
									<div id="profile_delete_yes" class="button" style="width:80px;float: left;margin-left: 20px;">YES</div>
									<div id="profile_delete_no" class="button" style="width:80px;float:right;margin-right:20px;">NO</div>
								</div>
							<? }
							else{ ?>	
								<div>Profile delete request received at: <? echo $under_delete->row()->request_date;?></div>
							<? } ?>
						</div>
					</div>	
						
					<div style="width:300px;float:left;margin:20px 20px;">

						<div id="edit_dogsitter_data">
							<select  id="edit_country" name="edit_country" class="edit_input" style="width: 256px;padding-left: 5px;">
								<option disabled><?= $this->lang->line('edit_user_select_country')?></option>
								<? foreach($countrydata as $country){?>
									<option <?if($profiledata->country==$country->name) echo "selected='selected'";?> value="<?= $country->name;?>"><?= $country->name;?></option>
								<?}?>					
							</select>
							<br>						
							<input type="text" id="edit_state" name="edit_state" 
								value="<?= $profiledata->state == NULL ? $this->lang->line('register_state') : $profiledata->state ?>" 
								defaultText="<?= $this->lang->line('register_state')?>" placeholder="State"
								class="edit_input">
							<br>
							<input type="text" id="edit_zip" name="edit_zip" 
								value="<?= $profiledata->zip == NULL ? $this->lang->line('register_zip') : $profiledata->zip ?>" 
								defaultText="<?= $this->lang->line('register_zip')?>" placeholder="ZIP"
								class="edit_input">
							<br>
							<input type="text" id="edit_city" name="edit_city" 
								value="<?= $profiledata->city == NULL ? $this->lang->line('register_city') : $profiledata->city ?>" 
								defaultText="<?= $this->lang->line('register_city')?>" placeholder="City"
								class="edit_input">
							<br>
							<input type="text" id="edit_address" name="edit_address" 
								value="<?= $profiledata->address == NULL ? $this->lang->line('register_address') : $profiledata->address ?>" 
								defaultText="<?= $this->lang->line('register_address')?>" placeholder="Address" 
								class="edit_input">
							<br>
						
							<div style="position:relative;left:20px;">
								<input <?if($profiledata->show_hashtags==1) echo "checked";?> type="checkbox" id="edit_hashtag" name="edit_hashtag" value="1" class="" style="margin:0px;margin-top:10px;float:left;">
								<div style="position:relative;left:10px;top:8px;font-size: 12px;float:left;">Show hashtagged posts</div>
							</div>
							<br/>
							<div style="position:relative;top:10px;left:7px;">
								<input <?if($profiledata->import_instagram==1) echo "checked";?> type="checkbox" id="import_instagram" name="import_instagram" value="1" class="" style="margin:0px;margin-top:10px;float:left;">
								<div style="position:relative;left:2px;top:6px;font-size: 12px;float:left;width:210px;">Import instagram photos with the hashtag:</div>
							</div>				
							<br style="clear:both;">
							<div id="import_instagram_hashtag" style="margin-top:25px;font-size:16px;">#<?= $profiledata->user_hashtag?></div>
							
						</div>
						
						<br>
						
						<input type="submit" style="left:0px;line-height: 25px;" class="button regular" action="editprofile" value="UPDATE"></input>
						
					</div>
					<?php echo form_close();?>	
				</div>
				
					
			</div>
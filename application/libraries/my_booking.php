<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once('MY_Calendar.php'); 

class My_booking extends MY_Calendar
{
	
	const BOOKING_STATUS_ACCEPTED = 1;
	const BOOKING_STATUS_CANCELED = 2;
	const BOOKING_STATUS_DECLINED = 3;
	const BOOKING_STATUS_COMPLETED = 4;
	const BOOKING_STATUS_PENDING = 5;
	
	protected $booking_model = null;
	protected $user_id = null;
	protected $show_recurring = null;
	protected $show_date_specific = null;
	protected $show_exceptions = null;
	protected $show_bookings = null;
	protected $edit_recurring = null;
	protected $edit_date_specific = null;
	protected $edit_exceptions = null;
	protected $edit_bookings = null;
	protected $edit_unoccupied = null;
	
    public function __construct()
    {
    	parent::__construct(true);
		
		$this->ci->load->model('booking_model');
		
		$this->booking_model = new booking_model();		
    }

	public function setUserID($user_id)
	{
		$this->user_id = $user_id;
	}
	
	public function setBookingElements($elements)
	{
		foreach($elements as $element)
		{
			switch($element)
			{
				case 'recurring':
					$this->show_recurring = true;
					break;
				case 'date_specific':
					$this->show_date_specific = true;
					break;
				case 'exceptions':
					$this->show_exceptions = true;
					break;
				case 'bookings':
					$this->show_bookings = true;
					break;
			}
		}
	}
	
	public function setEditableBookingElements($elements)
	{
		foreach($elements as $element)
		{
			switch($element)
			{
				case 'recurring':
					$this->edit_recurring = true;
					break;
				case 'date_specific':
					$this->edit_date_specific = true;
					break;
				case 'exceptions':
					$this->edit_exceptions = true;
					break;
				case 'bookings':
					$this->edit_bookings = true;
					break;
				case 'unoccupied':
					$this->edit_unoccupied = true;
					break;
			}
		}		
	}
	
	public function renderBookingCalendar()
	{
		$this->setTimeslots($this->getTimeslots());

		return $this->render();
	}
	
	public function renderSitterControls($user_id)
	{
		$data['user_id'] = $user_id;
		$data['requests'] = $this->booking_model->getAllSitterBookings($data['user_id']);
		$actions = array();
		foreach($data['requests']->result() as $request)
		{
			$actions[$request->id] = $this->getSitterRequestActions($request->id, $request->status_id, $request->user_id);
		}
		$data['actions'] = $actions;
		return $this->ci->load->view('booking/sitter_controls', $data, true);
	}
	
	private function getSitterRequestActions($id, $status_id, $user_id)
	{
		$action = "";
		
		$user_email = $this->booking_model->getSitter($user_id)->row()->email;
		switch($status_id)
		{
			case MY_Booking::BOOKING_STATUS_ACCEPTED:
				$action = '<a style="text-decoration:none;" href="mailto:'.$user_email.'" ><div class="sitter_controls_button">contact</div></a>  <div class="sitter_control_action decline_request sitter_controls_button" request_id="' . $id . '">decline</div><div class="sitter_control_action remove_request sitter_controls_button" request_id="' . $id . '">remove</div>';
				break;
			case MY_Booking::BOOKING_STATUS_CANCELED:
				$action = '<a style="text-decoration:none;" href="" target="_blank"><div class="sitter_controls_button">userprofile</div></a>';
				break;
			case MY_Booking::BOOKING_STATUS_COMPLETED:
				$action = '<a style="text-decoration:none;" href="" target="_blank"><div class="sitter_controls_button">userprofile</div></a>';
				break;
			case MY_Booking::BOOKING_STATUS_DECLINED:
				$action = '<a style="text-decoration:none;" href="mailto:'.$user_email.'" ><div class="sitter_controls_button">contact</div></a><div class="sitter_control_action remove_request sitter_controls_button" request_id="' . $id . '">remove</div>';
				break;
			case MY_Booking::BOOKING_STATUS_PENDING:
				$action = '<a style="text-decoration:none;" href="mailto:'.$user_email.'" ><div class="sitter_controls_button">contact</div></a>  <div class="sitter_control_action accept_request sitter_controls_button" request_id="' . $id . '">accept</div> <div class="sitter_control_action decline_request sitter_controls_button" request_id="' . $id . '">decline</div>';
				break;
		}
		
		return $action;
	}

	protected function getTimeslots()
	{
		$data = array();
		$enddate = $this->lastDayofMonth($this->calendar_startdate);
		switch($this->calendar_style)
		{
			case My_Calendar::CALENDAR_STYLE_WEEK:
			
				// RECURRING
				if($this->show_recurring)
				{
					$recurring = $this->booking_model->getSitterTimeslotsRecurring($this->user_id);
		
					foreach($recurring->result() as $slot)
					{
						if($slot->allday != 1)
						{
							$startslot = $this->getTimeslot($slot->starttime);
							$endslot = $this->getTimeslot($slot->endtime);	
						}
						else
						{
							$startslot = 0;
							$endslot = 48;	
						}
						
						
						$this->saveTimeslotInfo($data, $slot->weekday_id, $startslot, $endslot, array('class' => 'slot_available recurring' . ($this->edit_recurring ? ' edit_recurring' : ''),
																									  'additional' => array('dog_limit' => $slot->dog_limit,
																									  						'avail_id' => $slot->id)));
					}					
				}

				
				// DATE SPECIFIC
				if($this->show_date_specific)
				{
					$specifics = $this->booking_model->getSitterTimeslotsNonRecurring($this->user_id, $this->calendar_startdate, $this->calendar_enddate);
		
					foreach($specifics->result() as $specific)
					{
						if($specific->allday != 1)
						{
							$startslot = $this->getTimeslot($specific->starttime);
							$endslot = $this->getTimeslot($specific->endtime);	
						}
						else
						{
							$startslot = 0;
							$endslot = 48;	
						}
						
						
						$this->saveTimeslotInfo($data, $specific->weekday_id, $startslot, $endslot, array('class' => 'slot_available date_specific'  . ($this->edit_date_specific ? ' edit_date_specific' : ''),
																										  'additional' => array('dog_limit' => $slot->dog_limit,
																										  						'avail_id' => $slot->id)));
					}
				}

				
				// EXCEPTIONS
				if($this->show_exceptions)
				{
					$exceptions = $this->booking_model->getSitterTimeslotExceptions($this->user_id, $this->calendar_startdate, $this->calendar_enddate);
					
					foreach($exceptions->result() as $exception)
					{
						if($exception->allday != 1)
						{
							$startslot = $this->getTimeslot($exception->starttime);
							$endslot = $this->getTimeslot($exception->endtime);	
						}
						else
						{
							$startslot = 0;
							$endslot = 48;	
						}		
									
						$weekday = $this->getWeekdayID($exception->date);
						
						$this->saveTimeslotInfo($data, $weekday, $startslot, $endslot, array('class' => 'slot_unavailable exception' . ($this->edit_exceptions ? ' edit_exceptions' : ''),
																							 'additional' => array('description' => $exception->description,
																												   'exception_id' => $exception->id)));
					}					
				}

				
				
				// BOOKINGS
				if($this->show_bookings)
				{
					$bookings = $this->booking_model->getSitterBookings($this->user_id, $this->calendar_startdate, $this->calendar_enddate, MY_Booking::BOOKING_STATUS_ACCEPTED);
		
					foreach($bookings->result() as $booking)
					{
						if($booking->allday != 1)
						{
							$startslot = $this->getTimeslot($booking->starttime);
							$endslot = $this->getTimeslot($booking->endtime);	
						}
						else
						{
							$startslot = 0;
							$endslot = 48;	
						}	
											
						$weekday = $this->getWeekdayID($booking->date);
								
						for($i = $startslot ; $i < $endslot ; $i++)
						{
							if(isset($data[$weekday][$i]))
							{
								$data[$weekday][$i]['class'] .= 'slot_booked booking' . ($this->edit_bookings ? ' edit_bookings' : '');
								$data[$weekday][$i]['additional']['booking_id'][] = $booking->id;
							}
							else
							{
								$data[$weekday][$i] = array('class' => 'slot_booked booking' . ($this->edit_bookings ? ' edit_bookings' : ''),
															'additional' => array('booking_id' => array($booking->id)));
							}
						}									
					}					
				}
				
				break;

			case My_Calendar::CALENDAR_STYLE_MONTH:
				
				$data = array();

				// RECURRING
				if($this->show_recurring)
				{
					$recurring = $this->booking_model->getSitterTimeslotsRecurring($this->user_id);
					foreach($recurring->result() as $slot)
					{
						$days = $this->getWeekdaysFromRange(date('Y-m-d 00:00:00', strtotime($this->calendar_startdate)), date('Y-m-d 23:59:59', strtotime($enddate)), $slot->weekday_id);
						foreach($days as $day)
						{
							if(isset($data[$day]))
							{
								$data[$day]['class'] .= 'slot_available recurring' . ($this->edit_recurring ? ' edit_recurring' : '');
								$data[$day]['additional']['dog_limit'] = $slot->dog_limit;
								$data[$day]['additional']['avail_id'] = $slot->id;								
							}	
							else
							{
								$data[$day] = array('class' => 'slot_available recurring' . ($this->edit_recurring ? ' edit_recurring' : ''),
													'additional' => array(	'dog_limit' => $slot->dog_limit,
																			'avail_id' => $slot->id));
							}

						}
					}					
				}			
				
				// DATE SPECIFIC
				if($this->show_date_specific)
				{
					$specifics = $this->booking_model->getSitterTimeslotsNonRecurring($this->user_id, $this->calendar_startdate, $enddate);
		
					foreach($specifics->result() as $specific)
					{
							
						$day = date('j', strtotime($specific->date));
						
						if(isset($data[$day]))
						{
							if(!strpos($data[$day]['class'], 'slot_available'))
							{
								$data[$day]['class'] .= 'slot_available date_specific' . ($this->edit_date_specific ? ' edit_date_specific' : '');
								$data[$day]['additional']['dog_limit'] = $specific->dog_limit;
								$data[$day]['additional']['avail_id'] = $specific->id;								
							}
						}			
						else 
						{
							$data[$day] = array('class' => 'slot_available date_specific' . ($this->edit_date_specific ? ' edit_date_specific' : ''),
												'additional' => array(	'dog_limit' => $specific->dog_limit,
																		'avail_id' => $specific->id));
						}			
					}
				}				
				
				// EXCEPTIONS
		/*		if($this->show_exceptions)
				{
					$exceptions = $this->booking_model->getSitterTimeslotExceptions($this->user_id, $this->calendar_startdate, $enddate);
					
					foreach($exceptions->result() as $exception)
					{
						$day = date('j', strtotime($exception->date));
						
						$data[$day] = $data[$day] = array(	'class' => 'slot_exception' . ($this->edit_exceptions ? ' edit_exception' : ''),
															'additional' => array(	'excep_id' => $exception->id,
																					'description' => $exception->description));
					}					
				}					
				*/
				// BOOKINGS
				if($this->show_bookings)
				{
					$bookings = $this->booking_model->getSitterBookings($this->user_id, $this->calendar_startdate, $enddate, MY_Booking::BOOKING_STATUS_ACCEPTED);
					
					foreach($bookings->result() as $booking)
					{
						$day = date('j', strtotime($booking->date));
						
						if(isset($data[$day]))
						{
							$data[$day]['class'] .= 'slot_booked booking' . ($this->edit_bookings ? ' edit_bookings' : '');
							$data[$day]['additional']['bookings'][] = $booking->id;
						}
						else
						{
							$data[$day] = array('class' => 'slot_booked booking' . ($this->edit_bookings ? ' edit_bookings' : ''),
												'additional' => array('bookings' => array($booking->id)));
						}
					}
				}
				break;
		}
		return $data;
	}

	public function getSitterCalendar($user_id, $startdate = null, $reload = false)
	{
		$this->ci->load->helper('url');
		$segment_id =  $this->ci->uri->segment(2);
		
		if($segment_id == FALSE || $segment_id == "loadSitterCalendar")
		{
			$segment_id = $this->ci->uri->segment(3);
			
			if($segment_id == FALSE || $segment_id == "loadSitterCalendar")
			{
				$segment_id = $this->ci->session->userdata('user_id');
			}
		}
		$this->setUserID($segment_id);
		$this->setCalendarStartdate($startdate == null ? date('Y-m-01') : $startdate);
		$this->setCalendarStyle(My_Calendar::CALENDAR_STYLE_MONTH);
		$this->setBookingElements(array('recurring', 'date_specific', 'exceptions', 'bookings'));
		$this->setEditableBookingElements(array('recurring', 'date_specific', 'exceptions'));
		$this->setReload($reload);
		
		
		
		
		$this->setReloadUrl(site_url('booking/loadSitterCalendar/'.$segment_id));
		$this->setFormData(array('edit_unoccupied' => true));
		$this->setCustomJSFunction('addSitterCalendarListeners');
		
		return $this->renderBookingCalendar();		
	}	

	public function getUserCalendar($user_id, $startdate = null, $reload = false)
	{
		$this->setUserID($user_id);
		$this->setCalendarStartdate($startdate == null ? date('Y-m-01') : $startdate);
		$this->setCalendarStyle(My_Calendar::CALENDAR_STYLE_MONTH);
		$this->setBookingElements(array('bookings'));
		$this->setEditableBookingElements(array('bookings'));
		$this->setReload($reload);
		$this->setReloadUrl(site_url('booking/loadUserCalendar'));
		
		return $this->renderBookingCalendar();		
	}
	
	public function sitterSearch($startdate, $enddate, $dogs)
	{
		$sitters = array();
		while($startdate <= $enddate)
		{
			if($sitters == array())	
			{
				$result = $this->booking_model->getSitterSearch(date('Y-m-d', $startdate), $this->getWeekday(date('Y-m-d', $startdate)), $dogs);
				foreach($result->result() as $sitter)
				{
					$sitters[] = $sitter->user_id;
				}				
			}	
			else
			{
				$result = $this->booking_model->getSitterSearchSpecific(date('Y-m-d', $startdate), $this->getWeekday(date('Y-m-d', $startdate)), $dogs,  implode(',', $sitters));
				foreach($result->result() as $sitter)
				{
					if(!in_array($sitter->user_id ,$sitters))
						unset($sitters[$sitter->user_id]);	
				}					
			} 
			
			$startdate = strtotime('+1 day', $startdate);
		}		
		
		return $sitters;
	}
}
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['dog_name_max_length'] = 128;
$config['dog_nickname_max_length'] = 128;
$config['dog_breed_max_length'] = 128;
$config['dog_color_max_length'] = 128;
$config['dog_coat_max_length'] = 128;
$config['dog_type_max_length'] = 128;
$config['dog_size_max_length'] = 128;
$config['dog_characteristics_max_length'] = 128;
$config['dog_about_max_length'] = 200;
$config['dog_chip_max_length'] = 128;

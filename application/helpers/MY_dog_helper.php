<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function getDogAge($birthday, $years_text = 'years', $months_text = 'month')
{
	if($birthday != NULL)		
	{
		$date1 = new DateTime($birthday);
		$date2 = new DateTime(date("Y-m-d H:i:s"));
	
		$age = $date1->diff($date2);
		
		$text = "";
		
		if($age->y > 0)
			$text .= $age->y . ' ' . ($age->y == 1 ? substr($years_text, 0, strlen($years_text)-1) : $years_text);
		if($age->m > 0)
			$text .=  ' ' . $age->m . ' ' . ($age->m == 1 ? substr($months_text, 0, strlen($months_text)-1) : $months_text);
		
		if($text == "")
			$text .= "< 1 " . substr($months_text, 0, strlen($months_text));
	}
	else
		$text = "";	
	return $text;
}

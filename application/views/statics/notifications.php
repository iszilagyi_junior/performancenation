
			<?  foreach($notifications as $notification):?>
				<div class="notification_item" pid="<?= $notification['event_id']?>" style="<? if($notification['event_seen'] == 0) echo 'background:#555555;'?>">
					<a href="<?= site_url($notification['sender_link'])?>">
						<div class="conversation_image_holder">
							<img style="width:100%" src="<?= site_url('items/uploads/profilepictures/'.$notification['profile'])?>"/>
						</div>	
						<div class="conversation_name"><?= $notification['entity_name'];?></div>
					</a>
					
					<div class="conversation_date"><?= $notification['created_date'];?></div>
					
					<a href="<?= site_url($notification['link'])?>">
						<div class="conversation_text_sample"><?= $notification['text'];?></div>
					</a>
				</div>
			<? endforeach;?>
		

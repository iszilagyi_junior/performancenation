<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Car_Model extends CI_Model
{
	function getVehicles($user_id)
	{
		$this->db->where('user_id', $user_id);
		$return = $this->db->get('vehicles');
		return $return;
	}
	
	
	function getVehiclesForEvent($event_id)
	{
		$this->db->where('event_id', $event_id);
		$return = $this->db->get('event_vehicles');
		return $return;
	}
	
	
	function getCarModelById($model_id)
	{
		$this->db->where('id', $model_id);
		$return = $this->db->get('car_models');
		return $return;
	}
	
	function getMotorModelById($model_id)
	{
		$this->db->where('id', $model_id);
		$return = $this->db->get('motorcycle_models');
		return $return;
	}
	
	function getTruckModelById($model_id)
	{
		$this->db->where('id', $model_id);
		$return = $this->db->get('truck_models');
		return $return;
	}
	
	function getVehicle($vehicle_id)
	{
		$this->db->where('id', $vehicle_id);
		$return = $this->db->get('vehicles');
		return $return;
	}
	
	function addStoryPiece($data)
	{
		$this->db->insert('story_pieces', $data);
		return $this->db->insert_id();	
	}
	
	
	function deleteStoryPiece($id)
	{
		$this->db->where('id', $id);	
		$this->db->delete('story_pieces');
		
		
		$this->db->where('story_id', $id);	
		$this->db->delete('story_images');

	}
	
	function getGalleryImages($vehicle_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$result = $this->db->get('images');
		
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();
	}
	
	
	function getFollowers($vehicle_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$result = $this->db->get('followers');
		
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();
	}
	
	
	
	
	function getMyFollows($vehicle_id)
	{
		$this->db->where('follower_id', $vehicle_id);
		$result = $this->db->get('followers');
		
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();
	}
	
	
	
	function getRandomVehicles()
	{
		$sql = "SELECT * FROM vehicles ORDER BY RAND() LIMIT 30";
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function getAllVehicles()
	{
		$sql = "SELECT * FROM vehicles";
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	
	function checkVehicle($vehicle_id,$my_vehicle_id)
	{
		$sql = "SELECT * FROM followers WHERE vehicle_id = ? AND follower_id = ?";
		$result = $this->db->query($sql, array($vehicle_id,$my_vehicle_id));
		if ($result->num_rows() > 0) {return true;}else {return false;}
	}
	
	
	
	function followVehicle($data)
	{
		
		$this->db->insert('followers', $data);
		
	}
	
	
	function removeFollowVehicle($data)
	{
		$this->db->delete('followers', $data);
		
	}
	
	function getStoryPieceById($story_id)
	{
		$this->db->where('id', $story_id);
		
		return $result = $this->db->get('story_pieces');
		
	}
	
	
	function getStoryPieces($vehicle_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$this->db->order_by('created_date', 'DESC');
		$result = $this->db->get('story_pieces');
		
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();
	}
	
	
	function addComment($data)
	{
		
		$this->db->insert('vehicle_comments', $data);
		
	}
	
	function getVehicleComments($vehicle_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$this->db->order_by('created_date', 'DESC');
		$result = $this->db->get('vehicle_comments');
		
		return ( $result->num_rows() > 0 ) ? $result->result_array() : array();
	}
	
	
	function vehicleSearchProfile($search)
	{
		$this->db->like('nickname', $search);
		return $this->db->get('vehicles');
	}
	
	
	
	
	function createDog($data)
	{
		$this->db->trans_start();	
			
		$sql = "INSERT INTO dogs (name, nickname, pretty_url, breed, created_by, modified_by) VALUES (?, ?, ?, ?, ?, ?);";
		$resultset = $this->db->query($sql, array($data['name'], $data['nickname'], $data['pretty_url'], $data['breed'], $data['created_by'], $data['created_by']));
		$insert_id = $this->db->insert_id();
		$sql = "INSERT INTO users_dogs (dog_id, user_id, modified_by, created_by) VALUES (?, ?, ?, ?);";
		$resultset = $this->db->query($sql, array($this->db->insert_id(), $data['created_by'], $data['created_by'], $data['created_by']));
		
		$this->db->trans_complete();
		
		return $insert_id;
	}
	
	
	function createCar($data)
	{
		$this->db->insert('vehicles', $data);	
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	
	function createPrettyUrl($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('vehicles', $data);
		
	}
	
	function getPrettyUrl($pretty_url)
	{
		$this->db->where('pretty_url', $pretty_url);
		$return = $this->db->get('vehicles');
		return $return;
	}
	
	function getDog($dog_id)
	{
		$sql = "SELECT d.id, d.name, d.nickname, d.pretty_url, DATE_FORMAT(d.birthday, '%d.%m.%Y') as birthday, d.profile_picture, d.cover_picture, d.gender, d.breed, d.color, d.coat, d.type, d.size, d.characteristics, d.about, d.chip_nr, d.doctor, d.vaccines, d.modified_date, d.modified_by, d.created_date, d.created_by, b.name as breed_name, d.lost FROM dogs d, breeds b WHERE d.id = ? AND b.id = d.breed";
		$resultset = $this->db->query($sql, array($dog_id));
		return $resultset->row();
	}
	
	function getDogByName($name, $pic_id)
	{
		$sql = "SELECT * FROM dogs  WHERE name LIKE ? AND id NOT IN(SELECT dog_id FROM tagged_dogs WHERE image_id = ?)LIMIT 10";
		$resultset = $this->db->query($sql, array('%'.$name.'%', $pic_id));
		return $resultset;
	}
	
	
	function getDogsTagged($pic_id)
	{
		$sql = "SELECT * FROM dogs d, tagged_dogs td  WHERE  td.image_id = ? AND d.id = td.dog_id";
		$resultset = $this->db->query($sql, array( $pic_id));
		return $resultset;
	}
	
	
	function editVehicle($data)
	{	
		$this->db->trans_start();
		$this->db->where('id', $data['id']);
		$this->db->update('vehicles', $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
		    return "FAIL"; // generate an error... or use the log_message() function to log your error
		}
		else{
			return "OK";
		}
						
	}
	
	function registerDogMobile($data)
	{
		$this->db->trans_start();	
			
		$sql = "INSERT INTO dogs (name, nickname, breed, created_by, modified_by) VALUES (?, ?, ?, ?, ?);";
		$resultset = $this->db->query($sql, array($data['name'], $data['nickname'], $data['breed'], $data['created_by'], $data['created_by']));
		$insert_id = $this->db->insert_id();
		$sql = "INSERT INTO users_dogs (dog_id, user_id, modified_by, created_by) VALUES (?, ?, ?, ?);";
		$resultset = $this->db->query($sql, array($this->db->insert_id(), $data['created_by'], $data['created_by'], $data['created_by']));
		
		$this->db->trans_complete();
		
		return $insert_id;
	}
	
	
	function getFriends($vehicle_id, $limit = NULL)
	{
		$sql = "SELECT f.id, d.id, d.nickname, d.pretty_url, d.profile_image FROM followers f, vehicles d WHERE f.follower_id = ? AND d.id = f.vehicle_id ";
		if($limit != NULL)
			$sql .= " LIMIT " . $limit;
		$resultset = $this->db->query($sql, array($vehicle_id));
		return $resultset; 
	}
	
	function getEnemies($dog_id, $limit = NULL)
	{
		$sql = "SELECT f.id, d.id, d.name, d.pretty_url, d.profile_picture, d.breed, d.birthday FROM enemies f, dogs d WHERE f.dog_id = ? AND d.id = f.enemy_dog_id ";
		if($limit != NULL)
			$sql .= " LIMIT " . $limit;
		$resultset = $this->db->query($sql, array($dog_id));
		return $resultset; 
	}
	
	function getFriendsTotal($car_id)
	{
		$this->db->from('followers');	
		$this->db->where('vehicle_id', $car_id);
		return $this->db->count_all_results();
	}	
	
	function getEnemiesTotal($dog_id)
	{
		$this->db->from('enemies');	
		$this->db->where('dog_id', $dog_id);
		return $this->db->count_all_results();
	}		
	
	function getMutualFriends($dog_id, $friend_dog_id)
	{
		$sql = "SELECT count(a.friendID) as mutualfriends
				FROM
				      ( SELECT CASE WHEN dog_id = ?
				                      THEN friend_dog_id 
				                      ELSE dog_id 
				               END AS friendID 
				        FROM friends 
				        WHERE dog_id = ?
				      ) a
				  JOIN
				      ( SELECT CASE WHEN dog_id = ?
				                      THEN friend_dog_id 
				                      ELSE dog_id 
				               END AS friendID 
				        FROM friends 
				        WHERE dog_id = ?
				      ) b
				    ON b.friendID = a.friendID ";
		$resultset = $this->db->query($sql, array($dog_id, $dog_id, $friend_dog_id, $friend_dog_id));	
		return $resultset->row()->mutualfriends;
	}
	
	function getMutualEnemies($dog_id, $enemy_dog_id)
	{
		$sql = "SELECT count(a.friendID) as mutualfriends
				FROM
				      ( SELECT CASE WHEN dog_id = ?
				                      THEN enemy_dog_id 
				                      ELSE dog_id 
				               END AS friendID 
				        FROM enemies 
				        WHERE dog_id = ?
				      ) a
				  JOIN
				      ( SELECT CASE WHEN dog_id = ?
				                      THEN enemy_dog_id 
				                      ELSE dog_id 
				               END AS friendID 
				        FROM enemies 
				        WHERE dog_id = ?
				      ) b
				    ON b.friendID = a.friendID ";
		$resultset = $this->db->query($sql, array($dog_id, $dog_id, $enemy_dog_id, $enemy_dog_id));	
		return $resultset->row();
	}	

	function getShortinfo($dog_id)
	{
		$sql = "SELECT breed, birthday, name, profile_picture FROM dogs WHERE id = ?";
		$resultset = $this->db->query($sql, array($dog_id));
		return $resultset->row();
	}	
	
	function getOwners($user_id)
	{
		$sql = "SELECT * FROM users  WHERE id = ?";
		$resultset = $this->db->query($sql, array($user_id));
		return $resultset;
	}
	

	
	function favoriteVet($dog_id, $vet_id)
	{
		$data = array(
					'dog_id' => $dog_id,
					'vet_id' => $vet_id,
					'created_by' => $this->session->userdata('user_id'));
		$this->db->insert('dogs_vets', $data);
	}
	
	function unfavoriteVet($dog_id, $vet_id)
	{
		$this->db->delete('dogs_vets', array('dog_id' => $dog_id, 'vet_id' => $vet_id));
	}	
	
/*	function dogSearchProfile($search)
	{
		$sql = "SELECT d.id, d.name, d.pretty_url, d.nickname, DATE_FORMAT(d.birthday, '%d.%m.%Y') as birthday, d.profile_picture, d.cover_picture, d.gender, d.breed, d.color, d.coat, d.type, d.size, d.characteristics, d.about, d.chip_nr, d.modified_date, d.modified_by, d.created_date, d.created_by from dogs d WHERE (d.name LIKE '%".$search."%') OR (d.nickname LIKE '%".$search."%')";
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
*/	
	
	
	
	function addDogEnemy($mydog_id,$selected_dog_id)
	{
		$data = array(
					'enemy_dog_id' => $mydog_id,
					'dog_id' => $selected_dog_id,
					'created_by' => $this->session->userdata('user_id'));
		$this->db->insert('enemies', $data);
	}
	
	function checkActivity($activity_id)
	{
		$sql = "SELECT * FROM activities_invites ai WHERE ai.activity_id = ?";
		$result = $this->db->query($sql, array($activity_id));
		return $result;
	}
	
	
	function getBreeds()
	{
		$sql = "SELECT * FROM breeds WHERE accepted = '1' ORDER BY name ASC";
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function getBreedName($id)
	{
		$sql = "SELECT * FROM breeds WHERE id = ? LIMIT 1";
		$resultset = $this->db->query($sql, array($id));
		return $resultset->row();
	}
	
	function addBreedFromUser($name)
	{
		$data = array(
					'name' => $name,
					);
		$this->db->insert('breeds', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
		
	}
	
	function addTag($picture_id, $dog_id)
	{
		$data = array(
					'image_id' => $picture_id,
					'dog_id' => $dog_id,
					);
		$this->db->insert('tagged_dogs', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;		
	}
	
	function checkNearbyDog($dog_id, $user_id)
	{
		$sql = "SELECT * FROM users_dogs WHERE dog_id = ? AND user_id = ?";
		$result = $this->db->query($sql, array($dog_id,$user_id));
		if ($result->num_rows() > 0) {return true;}else {return false;}
	}
	
	function dogSearchProfileBreed($search,$breed)
	{
		$sql = "SELECT id, name, nickname, pretty_url, DATE_FORMAT(birthday, '%d.%m.%Y') as birthday, profile_picture, cover_picture, gender, breed, color, coat, type, size, characteristics, about, chip_nr, modified_date, modified_by, created_date, created_by from dogs WHERE name LIKE '%".$search."%' AND breed = '".$breed."'";
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	
	
	function deleteVehicle($vehicle_id)
	{
		$sql = "DELETE FROM vehicles WHERE id = ?";
		$this->db->query($sql, array($vehicle_id));
		
		$sql = "DELETE FROM vehicle_comments WHERE vehicle_id = ?";
		$this->db->query($sql, array($vehicle_id));
		
		$sql = "DELETE FROM user_created_event_vehicles WHERE vehicle_id = ?";
		$this->db->query($sql, array($vehicle_id));
		
			
	}
	
	
	function getMod($table,$id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get($table);
		return $result->row();

	
	}
	
	
	function getVehicleMod($table,$vehicle_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$this->db->where('visible', 1);
		$result = $this->db->get($table);
		return ( $result->num_rows() > 0 ) ? $result->result() : array();
	}
	
	
	function checkVehicleMod($table,$vehicle_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$result = $this->db->get($table);
		return ( $result->num_rows() > 0 ) ? $result->row() : false;
	}
	
	function addVehicleMod($data, $table)
	{
		
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
		
	}
	
	function updateVehicleMod($data,$table)	
	{		
		$this->db->where('vehicle_id', $data['vehicle_id']);
		$this->db->update($table, $data);	
	}
	
	function vehicleAdvancedsearch($search)
	{
		$sql = "SELECT  FROM vehicles d, breeds b WHERE d.id = ? AND b.id = d.breed";
		$resultset = $this->db->query($sql, array($dog_id));
		return $resultset->row();
	}
	
	
	
	
	function addVehiclePoints($data)
	{
		$this->db->insert('vehicle_points', $data);
			
	}
	
	function checkVehiclePoints($vehicle_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$result = $this->db->get('vehicle_points');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;
	}
	
	function updateVehiclePoints($data)	
	{		
		$this->db->where('vehicle_id', $data['vehicle_id']);
		$this->db->update('vehicle_points', $data);	
	}
	
	
	
	function addGameCounter($data)
	{
		$this->db->insert('game_counter', $data);
			
	}
	
	function checkGameCounter($vehicle_id)
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$result = $this->db->get('game_counter');
		return ( $result->num_rows() > 0 ) ? $result->row() : false;
	}
	
	function updateGameCounter($data)	
	{		
		$this->db->where('vehicle_id', $data['vehicle_id']);
		$this->db->update('game_counter', $data);	
	}
	
	
}
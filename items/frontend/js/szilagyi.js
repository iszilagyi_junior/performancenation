//Google maps
var marker;
var markerPlaced = false;
var topiclist_toggle = false;
var profile_toggle = false;
var geocoder = new google.maps.Geocoder();
var nearby_distance_limit = 650; // in km
var is_loading = false;
var content_offset = 10;
var profile_map = null;
var ajax_call_running = false;

var cropping_overlay_instance;
var $container;
$(document).ready(function()
{	
	resize();
	addListeners();
	albumImageListeners();
	dateTimePicker();
	qtip_test();
	setTimeout(function(){
		initPkry();
	},200);
	
	showHideIE9Elements();
	
	initMap();
	window_scroll_event_handler();
	outdatedBrowser();
});

$(window).resize(function(){
	resize();
})

function outdatedBrowser()
{
	$("body").iealert({
	  support: "ie8"
	});
}


function initPkry()
{
	
	$container = $('#eventHolder');
	// init
	$container.packery({
	  itemSelector: '.event_item_sortable',
	  gutter: 15
	});
}


function registerListeners()
{
	$('#register_country').on('change', function(){
		var country = $(this).val();
		
		if(country == "US")
		{
			$('#state_holder').fadeIn();
			
		}
		else
		{
			$('#state_holder').hide();
		}
	});
}


function window_scroll_event_handler()
{
	$(window).scroll(function() 
	{
			
		if($(window).scrollTop() + $(window).height() > $(document).height() - 100) 
		{
			
	   		$(window).unbind('scroll');
	   	//	loadContent();
	   	}
	});	
}

function loadContent()
{
	
	if(!is_loading)
	{
		is_loading = true;
		$.ajax(
		{
			url: rootUrl + 'statics/sample_test',
			data: {offset: content_offset},
			method: 'GET',
			success: function(data)
			{
				ret = $.parseJSON(data);
				if(ret.success)
				{
					content_offset = ret.new_offset;
					//console.log(ret.html);
				//	var test = $(ret.html);
					var previousH = $('#eventHolder').height();
					$('#eventHolder').append(ret.html);
					
					
					$container.packery('destroy');
					setTimeout(function(){
						initPkry();
						$(window).scrollTop(previousH);
					},200);
					
	
					window_scroll_event_handler()
					if(ret.image_count < 10)
						$(window).unbind('scroll');
						
					is_loading = false;
				}
			}
		});			
	}
}


function resize(){
	
	var windowHeight = $(window).height();
	var windowWidth = $(window).width();
	
	var leftPos = (windowWidth-$('#lost_preview_holder').width())/2;
	$('#lost_preview_holder').css({left: leftPos});
	$('#container, #wrapper').css({width: "1000px"});
	
	
	if(windowWidth<=1000)
	{
		leftPos = 0
		$('#container').css({left: leftPos});
		
		
		if(windowWidth<=640)
		{
			$('#headerMenu').css({left: leftPos, width: 640});	
		$('#profile_hover_box').css({left: windowWidth-$('#profile_hover_box').width()});
		$("#downloadBG, #socialBG").hide();
		}
		else
		{
			$('#headerMenu').css({left: leftPos, width: windowWidth});	
			$('#profile_hover_box').css({left: windowWidth-$('#profile_hover_box').width()});
		}
		
		
		
		
	}
	else
	{
		leftPos = (windowWidth-$('#container').width())/2;
		$('#container').css({left: leftPos});
		
		leftPos = (windowWidth-$('#headerMenu').width())/2;
		$('#headerMenu').css({left: leftPos, width:1024});
		
		$('#profile_hover_box').css({left: leftPos+1044-$('#profile_hover_box').width()});
	}
	
/*	$('#loadingBG').css({height: windowHeight});
	$('#loadingGif').css({left: (windowWidth-200)/2, top:(windowHeight-200)/2});
	*/
}

function newsfeed_file_upload(){
	var settings = {
	    url: rootUrl+"statics/add_photo_newsfeed",
	    method: "POST",
	    allowedTypes:"jpg,png,jpeg",
	    fileName: "myfile",
	    multiple: false,
	    onSubmit:function(files)
		{
			$(".ajax-file-upload").hide();
			$('.ajax-upload-dragdrop').find('#dragdrop').text(please_wait);
		},
	    onSuccess:function(files,data,xhr)
	    {
	    	
	        window.location.href = rootUrl;
	        $(".ajax-file-upload").show();
			$('.ajax-upload-dragdrop').find('#dragdrop').text(dragdrop);			

	    },
	    onError: function(files,status,errMsg)
	    {     
	    	
	        $(".ajax-file-upload").show();
	        $('.ajax-upload-dragdrop').find('#dragdrop').text(dragdrop);
	    }
	}
	
	$("#add_photo_btn_newsfeed").uploadFile(settings);
}

function album_file_upload(){
	var settings = {
	    url: rootUrl+"statics/add_photo_newsfeed",
	    method: "POST",
	    allowedTypes:"jpg,png,jpeg",
	    fileName: "myfile",
	    multiple: false,
	    onSubmit:function(files)
		{
			$(".ajax-file-upload").hide();
			$('.ajax-upload-dragdrop').find('#dragdrop').text(please_wait);
		},
	    onSuccess:function(files,data,xhr)
	    {
	    	
	        window.location.href = rootUrl+"albums";
	         $(".ajax-file-upload").show();
			$('.ajax-upload-dragdrop').find('#dragdrop').text(dragdrop);
						

	    },
	    onError: function(files,status,errMsg)
	    {       
	        $(".ajax-file-upload").show();
	        $('.ajax-upload-dragdrop').find('#dragdrop').text(dragdrop);
	    }
	}
 
	$("#add_photo_btn_albums").uploadFile(settings);
}


function file_upload(){
	var settings = {
	    url: rootUrl+"statics/add_photo",
	    method: "POST",
	    allowedTypes:"jpg,png,jpeg",
	    fileName: "myfile",
	    multiple: false,
	    onSubmit:function(files)
		{
			$(".ajax-file-upload").hide();
			$('.ajax-upload-dragdrop').find('#dragdrop').text('Please wait...');
		},
	    onSuccess:function(files,data,xhr)
	    {
	    	var json = $.parseJSON(data);
	    	
	    	$.each(json, function(i,item){
	    	
		    	var elem = '<div class="album_item">'+
							'<a class="image_click" pic="'+item.id+'" pic_id="'+item.id+'" title="" href="'+rootUrl+'items/uploads/images/'+item.fname+'" data-lightbox="pictures">'+
								'<div class="album_item_image_holder">'+
									'<img title class="album_item_image" src="'+rootUrl+'items/uploads/images/'+item.fname+'"/>'+
								'</div>'+
							'</a>'+
							'<div class="title_editor" pid="'+item.id+'">'+
								'<input type="text" class="photo_title_input" maxlength="255" placeholder="title" value=""/>'+
								'<div class="photo_title_save button_skin" style="width: 142px;margin-top: 3px;">Save</div>'+
							'</div>'+
							
							'<div class="delete_editor" >'+	
								'<div>'+delete_confirm_text+'</div>'+
								'<div style="overflow:hidden">'+
									'<div class="photo_delete_yes button_skin" pid="'+item.id+'" style="width: 80px;margin-top: 3px;float:left;">YES</div>'+
									'<div class="photo_delete_no button_skin" style="width: 80px;margin-top: 3px;float:right;">NO</div>'+
								'</div>'+			
								
							'</div>'+
							
							'<div class="album_image_text_holder">'+
								'<img class="title_hover" src="'+rootUrl+'items/frontend/img/photo_icons_edit_white-01.png" />'+
								'<img class="remove_image" pid="'+item.id+'" style="" src="'+rootUrl+'items/frontend/img/photo_icons_delete_white-01.png" />'+
								'<img class="like_image" pid="'+item.id+'" style="" src="'+rootUrl+'items/frontend/img/photo_icons_love_white-01.png" />'+
								'<img class="comment_image" pid="'+item.id+'" style="" src="'+rootUrl+'items/frontend/img/photo_icons_comment_white-01.png" />'+
								'<div class="image_like_holder" style="position:absolute;top:40px;left:0px;width:100%;text-align:center;" pid="'+item.id+'" style="">LIKES: <span class="the_likes">0</span></div>'+
							'</div>'+
						'</div>';
						
						
						
	       
						$('#pictures_holder').append(elem); 
	    	});
	         
	        
	       removeAlbumImageListeners();
	       albumImageListeners();     
	      
	         $(".ajax-file-upload").show();
			$('.ajax-upload-dragdrop').find('#dragdrop').text(dragdrop);
	    },
	    onError: function(files,status,errMsg)
	    {       
	        $(".ajax-file-upload").show();
	        $('.ajax-upload-dragdrop').find('#dragdrop').text(dragdrop);
	    }
	}
 
	$("#add_photo_btn").uploadFile(settings);
}

function qtip_test()
{
	var type;
	var letter;
	$('.alphabet_tooltip').each(function()
	{
		type = $(this).attr('type');
		$(this).qtip(
		{
		    
		    hide:
		    { //moved hide to here,
		        delay:500, //give a small delay to allow the user to mouse over it.
		        fixed:true
		    },
		    content: 
		    {
		        text: function(event, api) 
		        {
		            $.ajax(
	            	{
		                url: rootUrl+'statics/' + type + '_getList',
		                data: {letter: $(this).attr('letter')},
		                type:"POST"
		            })
		            .then(function(content) {
		                // Set the tooltip content upon successful retrieval
		                api.set('content.text', content);
		            }, function(xhr, status, error) {
		                // Upon failure... set the tooltip content to the status and error value
		                api.set('content.text', status + ': ' + error);
		            });
		
		            return 'Loading...'; // Set some initial text
		        }
    		},
    		style:
		    {
		    	classes: 'custom_tooltip'
		    },
		});
	});
	

}

function tweetShare(component) {
	var thisCorrectUrl = rootUrl;
	var text = "";
	if(component == "TEXT")
	{
		var thisCorrectUrl = rootUrl;
		text = " I love snoopstr! Register now to find new dog friends in your neighbourhood!";
	}
	else if(component!="")
	{
		var thisCorrectUrl = rootUrl+component;
	}
	window.open(
	  'https://twitter.com/share?url='+encodeURIComponent(thisCorrectUrl)+'&text='+text,
      'facebook-share-dialog', 
      'width=626,height=436'); 
    return false;
}


function  onPostToWallCompleted(response)
{
	if (response && !response.error_code) {
     
    } else {
    
    }
}

function facebookShare(dog_id, pretty) {
    $.post( rootUrl+"dog/getDogInfo",{ dog: dog_id}, function( data ) {		
		var	ret = $.parseJSON(data);
			var dog=
		{
			method: 'stream.publish',  
			message: "",
			display: 'iframe',
			caption: rootUrl,
			name: ret.name,			
			picture: rootUrl+"items/uploads/profilepictures/"+ret.profile_picture,    
			link: rootUrl+"profile/"+pretty,  // Go here if user click the picture
			description: "More info on snoopstr.com",
			actions: [{ name: 'Snoopstr.com', link: rootUrl+"dog/"+dog_id }],			
		}	
		FB.ui(dog, onPostToWallCompleted);
	});
}

function facebookShareInvite() {
    
    var dog=
		{
			method: 'stream.publish',  
			message: "",
			display: 'iframe',
			caption: rootUrl,
			name: "Snoopstr",			
			picture: rootUrl+"items/frontend/img/10001000.png",    
			link: rootUrl,  // Go here if user click the picture
			description: "I love snoopstr! Register now to find new dog friends in your neighbourhood!",
			actions: [{ name: 'Snoopstr.com', link: rootUrl }],			
		}
		FB.ui(dog, onPostToWallCompleted);
	
}

function facebookShareImage(fname,title,pid) {
   if(title == " ")
   {
   	title = "Image from snoopstr";
   }
  		var share=
		{
			method: 'stream.publish',  
			message: "",
			display: 'iframe',
			caption: rootUrl,
			name: title,			
			picture: rootUrl+"items/uploads/images/"+fname,    
			link: rootUrl+"photo/"+pid,  // Go here if user click the picture
			description: "See this image and dog on snoopstr.com",
			actions: [{ name: 'snoopstr.com', link: rootUrl+"photo/"+pid }],			
		}	
		FB.ui(share, onPostToWallCompleted);
	
}


function dateTimePicker()
{
	$('#register_birthday, #edit_birthday, #dog_birthday, #activity_date')
										.datepick({ 	width: "300px",
														yearRange: '1930:2014',
														defaultDate: '20.05.2014',
														dateFormat: 'yyyy-mm-dd',
														onSelect: function(dates) { $(this).removeClass('input_default'); }});
														
	$('#activity_time').timepicker({ 'timeFormat': 'H:i' });													
}


function removeAlbumImageListeners()
{
	$(".like_image, .comment_image, .remove_image, .photo_delete_no, .photo_delete_yes, .title_hover, .photo_title_save").unbind('click');
}


function addCommentDelete()
{
	
	$('.delete_comment_btn').unbind('click');
	$('.delete_comment_btn').click(function(){
		
		var comment_id = $(this).attr('cid');
		var $this = $(this);
		$this.parent().parent().fadeOut();	
		$this.parent().parent().next().fadeOut();	
		$.post(rootUrl+'backend/deleteComment', {cid:comment_id}, function(data) 
		{	
		
		});
	});
}


function albumImageListeners()
{
	
	$('#dog_detail_delete_friend').click(function(){
		$(this).hide();
		$('#delete_connection_holder').fadeIn();
	});
	
	$('#dog_detail_connection_delete_no').click(function(){
		$('#delete_connection_holder').hide();
		$('#dog_detail_delete_friend').fadeIn();
	});
	
	$('#dog_detail_connection_delete_yes').click(function(){
		var this_dog = $(this).attr("did");
		$('#delete_connection_holder').fadeOut();
		$.post( rootUrl+"dog/removeConnection",{selected_dog: this_dog}, function( data ) {		
			 location.reload();
		});
	});
	
	$('.comment_image').click(function(){
		var posX = (windowWidth-420)/2;
		var posY = $(this).parent().offset().top-95;	
		var pic_id = $(this).attr('pid');
		
	
		$.post( rootUrl+"statics/get_photo_comments",{pid: pic_id}, function( data ) {
				
			$('#user_comments').empty();
			var json = $.parseJSON(data);
			$.each(json, function(i,item)
			{
				var deletebtn = "";
				if(user_id == item.uid)
				{
					deletebtn = '<div class="delete_comment_btn" cid="'+item.cid+'">X</div>'
				}
				var elem = '<div class="comment_photo_item" >'+
								'<div style="overflow:hidden;width:100%">'+
									'<div class="comment_user">'+item.user_id+'</div>'+						
									'<div class="comment_date">'+item.created_date+'</div>'+
									deletebtn+
								'</div>'+
								'<div class="comment_text">'+item.comment+'</div>'+
							'</div>'+
							'<hr class="red_divider" style="border:0px;height:1px;background:#d3004d;width:50%;margin:10px 25%;">';
							
				$('#user_comments').append(elem);
				addCommentDelete();
			});
			
		});
		

		
		
		
		$('#comment_holder').css({left:posX}).attr('pic', pic_id);
		$('#comment_holder').fadeIn();
		
	});
	
	$('.remove_image').click(function(){
		
		var $this = $(this);
		$this.fadeOut();							
		$this.parent().parent().find('.delete_editor').fadeIn();	
		
	});
	
	$('.photo_delete_no').click(function(){
		
		var $this = $(this);					
		$this.parent().parent().fadeOut();	
		$('.remove_image').fadeIn();

	});
	
	
	$('.photo_delete_yes').click(function(){
		var pic_id = $(this).attr('pid');
		var $this = $(this);
		$.post(rootUrl+'statics/delete_image', {pid:pic_id}, function(data) 
		{	
			
			$this.parent().parent().parent().fadeOut();	
		});
	});
	
	
	$('.title_hover').click(function(){
		$(this).hide();
		
		$(this).parent().parent().find('.title_editor').fadeIn();
	});
	
	$(".photo_title_save").click(function() {
		var photo_id = $(this).parent().attr('pid');
		var title = $(this).parent().find('.photo_title_input').val();
		var $this = $(this);
		$.post( rootUrl+"statics/update_photo_title",{ pid: photo_id, title: title}, function( data ) {		
			$this.parent().fadeOut();
			$this.parent().parent().bind('mouseenter', function(){
				$(this).find('.title_hover').show();
			});
		});
	});
}


function sendZoneSuggestion(zone_id)
{
	var description = $("#zone_description_suggestion").val();
	
	if(description != "")
	{
		$.post( rootUrl+"statics/suggest_zone_description",{zone_id:zone_id, description: description}, function( data ) {
			
			$('#zone_description_suggestion, #submit_zone_description').fadeOut();
			$('#zone_desc_suggestion_text').text(suggest_zone_description_thanks);
			
		});
	}
}



function addListeners()
{	
		
	
	
	
	
	
	
	
	
	$('#edit_country').on('change',function(){
	
		moveMapCenter();
	});
	
	
	$('.share_image_fb').click(function(){
		var pid = $(this).attr("pid");
		var fname = $(this).attr("fname");
		var title = $(this).attr("title");
		facebookShareImage(fname,title,pid);
	});
	
	$('.share_image_tw').click(function(){
		var pid = $(this).attr("pid");
		var component = "photo/"+pid;
		tweetShare(component);
	});
	
	$('#submit_zone_description').click(function(){
		var zone_id = $(this).attr("zone_id");
		sendZoneSuggestion(zone_id);
	});
	
	
	
	$('#snoopstr_logo').click(function(){
		var scroll = $(window).scrollTop();
		if(scroll>0)
		{
			$('html,body').animate({ scrollTop: 0 }, 'slow', function () {
	          
	        });		
		}
		else
		{
			window.location.href = rootUrl;
		}
		
	});
	
	$('#tag_search_input').on('input',function(e){
    	var searchFor = $(this).val();  
    	var pic_id = $('#tag_holder').attr('pic');	
    	$.post( rootUrl+"statics/searchName",{name:searchFor, pic_id: pic_id}, function( data ) {		
			$('#tag_search_results').empty();
			var json = $.parseJSON(data);
			$.each(json, function(i,item)
			{
				
				var elem = '<div class="tag_friend_item" dog="'+item.id+'">'+
								'<div class="tag_friend_img_holder">'+
									'<img class="album_item_image" src="'+rootUrl+'items/uploads/profilepictures/'+item.profile_picture+'">'+					
								'</div>'+
								'<div class="tag_friend_name">'+item.name+'</div>'+
							'</div>';
							
				$('#tag_search_results').append(elem);
			});
			$('.tag_friend_item').click(function(){
				$this = $(this);
				var pic_id = $('#tag_holder').attr('pic');
				var dog_id = $(this).attr('dog');
				$.post( rootUrl+"statics/tagDog",{pic_id:pic_id, dog_id:dog_id}, function( data ) {		
					$this.remove();
				});
			});
		});
    	
    });
    
    $("#closest_places_name_search").click(function(){
    	var searchFor =  $('#place_search').val();
    	if(searchFor == "" || searchFor == " ")
    	{
    		$('#vets_list').empty();
    	}
    	else
    	{
    		$.post( rootUrl+"statics/searchPlaceName",{name:searchFor}, function( data ) {		
				$('#vets_list').empty();
				var json = $.parseJSON(data);
				var x = 1;
				$.each(json, function(i,item)
				{
					var nomargin = 'style="margin-top:15px;"';
					if(x%3==0)
					{
						nomargin = 'style="margin-right:0px;margin-top:15px;"';
					}
					
					
					
					
					var elem = '<a style="text-decoration:none;" href="'+rootUrl+'place/'+item.pretty_url+'"><div style="background:#d4003d;" class="nearby_list_item" '+nomargin+'>'+		   													   							
		   							'<div class="zones_list_item_desc" >'+item.name+'</div>'+
		   						'</div></a>';;
								
					$('#vets_list').append(elem);

					x++;
				});
				
				
			});
    	}
    });
    
    
    $("#closest_vets_name_search").click(function(){
    	var searchFor =  $('#vet_search').val();
    	if(searchFor == "" || searchFor == " ")
    	{
    		$('#vets_list').empty();
    	}
    	else
    	{
    		$.post( rootUrl+"statics/searchVetName",{name:searchFor}, function( data ) {		
				$('#vets_list').empty();
				var json = $.parseJSON(data);
				var x = 1;
				$.each(json, function(i,item)
				{
					var nomargin = 'style="margin-top:15px;"';
					if(x%3==0)
					{
						nomargin = 'style="margin-right:0px;margin-top:15px;"';
					}
					
					
					
					
					var elem = '<div class="featured_vets_list_item" '+nomargin+'>'+
									//'<img class="vets_list_item_image" src="'+rootUrl+'items/uploads/profilepictures/'+item.profilepicture+'" border="0">'+
									'<a href="'+rootUrl+'vets/'+item.id+'" style="text-decoration:none;">' +															
										'<div class="div_white_over"></div>'+
										'<div class="featured_text"></div>'+
										'<div class="vets_list_item_desc">'+item.title+' '+item.firstname+' '+item.lastname+'</div>' +				
									'</a>' +		
								'</div>';
								
					$('#vets_list').append(elem);

					x++;
				});
				
				
			});
    	}
    });
    
	
	$('#closest_sitters_name_search').click(function(e){
    	var searchFor = $("#sitter_search").val(); 
    	if(searchFor == "" || searchFor == " ")
    	{
	    	$('#vet_list').empty();
    	}
    	else
    	{
	    	$.post( rootUrl+"statics/searchSitterName",{name:searchFor}, function( data ) {		
				$('#vet_list').empty();
				
				var json = $.parseJSON(data);
				var x = 1;
				$.each(json, function(i,item)
				{
					
					var nomargin = 'style="margin-top:15px;"';
					if(x%3==0)
					{
						nomargin = 'style="margin-right:0px;margin-top:15px;"';
					}
					
					
					
					
					var elem = '<div class="featured_vets_list_item" '+nomargin+'>'+
									'<img class="vets_list_item_image" src="'+rootUrl+'items/uploads/profilepictures/'+item.profile_picture+'" border="0">'+
									'<a href="'+rootUrl+'sitters/'+item.id+'" style="text-decoration:none;">' +															
										'<div class="div_white_over"></div>'+
										'<div class="featured_text">DOG SITTER</div>'+
										'<div class="vets_list_item_desc">'+item.first_name+' '+item.last_name+'</div>' +				
									'</a>' +		
								'</div>';
								
					$('#vets_list').append(elem);

					x++;

				});
				
			});
		}
    	
    });
	
	
	$('#closest_zone_name_search').click(function(e){
    	var searchFor = $("#zone_search").val(); 
    	$('#vet_list').empty();
    	if(searchFor == "" || searchFor == " ")
    	{
	    	$('#vet_list').empty();
    	}
    	else
    	{
	    	$.post( rootUrl+"statics/searchZoneName",{name:searchFor}, function( data ) {		
				$('#vet_list').empty();
				
				var json = $.parseJSON(data);
				var x = 1;
				$.each(json, function(i,item)
				{
				
					   		var text = item.name;
					   		var cl = "zone_search_item";
					   		var pic = item.profilepicture;
							   		
							   
							if(x%3==0)
					   		{
					   			margin = "style='margin-right:0px'";
					   			
					   		}
					   		else
					   		{
						   		margin = "style='margin-right:15px'";
					   		}
					   		
							if(pic!=""){
   								var image =	'<img class="vets_list_item_image" src="'+rootUrl+'items/uploads/profilepictures/'+pic+'" border="0">'+
   											'<div class="div_white_over"></div>';
   							}
   							else
   							{
	   							var image = '';
   							}
							
							
							
							
						   		var new_item = '<a style="text-decoration:none;" href="'+rootUrl+'zones/'+item.id+'"><div class="'+cl+'" '+margin+'>'+
							   						image+	   									   							
						   							'<div class="zones_list_item_desc">'+text+'</div>'+
						   						'</div></a>';
						   		
							   $('#vets_list').append(new_item); 

					x++;

				});
				
			});
		}
    	
    });
	
	
	$('#profile_hover').mouseenter(function()
	{
		profile_toggle = true;
		if(!$('#profile_hover_box').hasClass('open'))
			$('#profile_hover_box').animate({top:60}, 300);
	});
	
	$('#profile_hover').mouseleave(function()
	{
		profile_toggle = false;
		setTimeout(function()
		{
			if(!profile_toggle)
			{
				$('#profile_hover_box').animate({top:0}, 1000);
				profile_toggle = false;
			}	
		}, 800);
	});
	
	$('#profile_hover_box').mouseenter(function()
	{
		profile_toggle = true;
	});
	
	$('#profile_hover_box').mouseleave(function()
	{
		profile_toggle = false;
		setTimeout(function()
		{
			if(!profile_toggle)
			{
				$('#profile_hover_box').animate({top:0}, 1000);
				profile_toggle = false;
			}	
		}, 800);
	});
	
	
	
	$('#dog_breed_select').on("change",function()
	{
		var val = $(this).val();
		if(val == "not"){
			$('#dog_register_breed').fadeIn();
		}
		else{
			$('#dog_register_breed').fadeOut();
		}
	});
	
	$('#dog_edit_breed_select').on("change",function()
	{
		var val = $(this).val();
		if(val == "not"){
			$('#dog_breed_holder').fadeIn();
		}
		else{
			$('#dog_breed_holder').fadeOut();
		}
	});
	
	$('#sidemenu_memberarea').mouseleave(function()
	{
		topiclist_toggle = false;
		if($('#sidemenu_memberarea').hasClass('open'))
			animateMemberArea('close');
	});
	
	$('#dog_edit_cover_upload, #cover_hover_holder').mouseenter(function(){
		$('#cover_hover_holder').show();
	});
	
	$('#dog_edit_cover_upload, #cover_hover_holder').mouseleave(function(){
		$('#cover_hover_holder').hide();
	});
	
	
	$('#dog_edit_profile_upload, #profile_hover_holder').mouseenter(function(){
		$('#profile_hover_holder').show();
	});
	
	$('#dog_edit_profile_upload').mouseleave(function(){
		$('#profile_hover_holder').hide();
	});
	
	
	
	$('.image_click').click(function(){
		$('#tag_holder').hide();		
	});
	
	$("#add_album").click(function() {
		$('#add_album_form').submit();
	});
	
	$("#profile_delete").click(function() {
		$('#profile_delete_confirm').fadeIn();
	});
	
	$("#profile_delete_no").click(function() {
		$(this).parent().fadeOut();
	});
	
	$("#profile_delete_yes").click(function() {
		var $this = $(this);
		$.post( rootUrl+"statics/delete_profile_request", function( data ) {		
			$this.parent().fadeOut();
			$this.parent().parent().fadeOut();
		});
	});
	
	
	$("#closest_zone_btn_text").click(function() {	
		getMyLocation("zones");			
	});
	
	
	$("#closest_zone_btn_search").click(function() {
		var input_val = $('#zone_location_input').val();
		if(input_val == "" || input_val == " ")
		{
			 $('#vets_list').empty();
     		 $('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>"+please_type_location+"</div>");
		}
		else
		{
			getAddressLocation("zones", input_val);
		}
		
	});
	
	
	$("#zone_location_input").on('keypress',function(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		
		if(keycode == '13') {
		
			var input_val = $('#zone_location_input').val();
			if(input_val == "" || input_val == " ")
			{
				 $('#vets_list').empty();
	     		 $('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>"+please_type_location+"</div>");
			}
			else
			{
				
			       getAddressLocation("zones", input_val); 
			    
				
			}
		}
	});
	
	
	 
   
	
	
	
	$("#closest_sitters_btn").click(function() {	
		getMyLocation("sitters");			
	});
	
	
	$("#closest_sitters_btn_search").click(function() {
		var input_val = $('#sitter_location_input').val();
		if(input_val == "" || input_val == " ")
		{
			 $('#vets_list').empty();
     		 $('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>"+please_type_location+"</div>");
		}
		else
		{
			getAddressLocation("sitters", input_val);
		}
		
	});
	
	$("#sitter_location_input").on('keypress',function(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13') {
		
			var input_val = $('#sitter_location_input').val();
			if(input_val == "" || input_val == " ")
			{
				 $('#vets_list').empty();
	     		 $('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>"+please_type_location+"</div>");
			}
			else
			{
				
			       getAddressLocation("sitters", input_val); 
			    
				
			}
		}
	});
	
	$("#closest_places_btn_text").click(function() {
		getMyLocation("places");			
	});
	
	$("#closest_places_btn_search").click(function() {
		var input_val = $('#place_location_input').val();	
		
		if(input_val == "" || input_val == " ")
		{
			 $('#vets_list').empty();
     		 $('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>"+please_type_location+"</div>");
		}
		else
		{
			getAddressLocation("places", input_val);
		}
				
	});
	
	
	
	$("#closest_vets_btn_text").click(function() {
		getMyLocation("vets");			
	});
	
	$("#closest_vets_btn_search").click(function() {
		var input_val = $('#vet_location_input').val();	
		
		if(input_val == "" || input_val == " ")
		{
			 $('#vets_list').empty();
     		 $('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>"+please_type_location+"</div>");
		}
		else
		{
			getAddressLocation("vets", input_val);
		}
				
	});
	
	
	$("#vet_location_input").on('keypress',function(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13') {
		
			var input_val = $('#vet_location_input').val();
			if(input_val == "" || input_val == " ")
			{
				 $('#vets_list').empty();
	     		 $('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>"+please_type_location+"</div>");
			}
			else
			{
				
			       getAddressLocation("vets", input_val); 
			    
				
			}
		}
	});
	
	
	
	
	$("#dogs_nearby").click(function() {
		//getMyLocation("sitters");
	});

	
	$("#add_activity").click(function() {
		$('#add_activity_form').submit();
	});
	
	$("#edit_activity").click(function() {
		$('#edit_activity_form').submit();
	});
	
	$("#edit_album").click(function() {
		$('#edit_album_form').submit();
	});
	
	$('#vet_edit_profile_upload').click(function(){
		$('#vet_edit_profile_picture').click();
	});
	
	
	$('#cover_hover_holder').click(function(){
		
		if(!$('html').hasClass('no-filereader'))
		{
			$('#dog_edit_cover_upload').click();
		}
		else
		{
			$('#add_coverimage_ie').click();
		}
		
	});
	
	
	$('#dog_edit_profile_upload').click(function(){
		
		if(!$('html').hasClass('no-filereader'))
		{
			$('#dog_edit_profile_picture').click();
		}
		else
		{
			$('#add_dogprofileimage_ie').click();
		}
		
	});
	
	
	$('#user_edit_profile_upload').click(function(){
		
		if(!$('html').hasClass('no-filereader'))
		{
			$('#dog_edit_profile_picture').click();
		}
		else
		{
			$('#add_profileimage_ie').click();
		}
		
	});

	
	
	$('#print_button').click(function(){
		printDiv('content');
	});
	
	$('.cancel_friend').click(function(){
		$(this).hide();
		$(this).parent().find('.cancel_confirm_holder').fadeIn();
	});
	
	$('.cancel_enemy').click(function(){
		$(this).hide();
		$(this).parent().find('.cancel_confirm_holder').fadeIn();
	});
	
	$('.cancel_remove').click(function(){
		$(this).parent().hide();
		$(this).parent().parent().find('.cancel_friend').fadeIn();
	});
	
	
	
	$('.confirm_remove').click(function(){
		var $this = $(this);
		var friend_id = $(this).attr('friend_id');
		var dog_id = $(this).attr('dog_id');
		$.post( rootUrl+"dog/removeFriend",{dog_id: dog_id, friend_id: friend_id}, function( data ) {		
			$this.parent().parent().hide();
		});
	});
	
	$('.confirm_enemy_remove').click(function(){
		var $this = $(this);
		var enemy_id = $(this).attr('enemy_id');
		var dog_id = $(this).attr('dog_id');
		$.post( rootUrl+"dog/removeEnemy",{dog_id: dog_id, enemy_id: enemy_id}, function( data ) {		
			$this.parent().parent().hide();
		});
	});
	
	$('#dog_lost').click(function(){
	/*	if ($(this).prop("checked"))
		{
			$('#lost_preview').fadeIn();
		}
		else
		{
			$('#lost_preview').fadeOut();
		}*/
	});
	
	
	
	$("#suggest_zone_btn").click(function() {
		$('#suggest_zone_holder').fadeIn();
	});
	
	$("#send_suggestion").click(function() {
		$('#suggest_zone_form').submit();
	});
	
	$("#invite_send_mail_button").click(function() {
		var $this = $(this);
		if(!$('#invite_send_mail').val()==""){
			var email = $('#invite_send_mail').val();
			$this.fadeOut();
			$('#invite_mail_error').text(please_wait);
			$.post( rootUrl+"statics/send_invite_mail",{email:email}, function( data ) {		
				$this.fadeIn();
				$('#invite_send_mail').val("");
				$('#invite_mail_error').text(email_sent_text);
			});
		}
	});
	
	
	 $("#sidemenu_password, #sidemenu_username").keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                $("#sidemenu_login_form").submit();
            }
        });
        
     $("#login_password").keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                $("#login_login_form").submit();
            }
        }); 
       
       
       
	$('.showTag').click(function(){
		var posX = $(this).parent().offset().left-260;
		var posY = $(this).parent().offset().top-95;	
		var pic_id = $(this).attr('pid');
		
		$.post( rootUrl+"statics/getFriendsNotTagged",{pic_id: pic_id}, function( data ) {		
			$('#tag_search_results').empty();
			var json = $.parseJSON(data);
			$.each(json, function(i,item)
			{
				
				var elem = '<div class="tag_friend_item" dog="'+item.id+'">'+
								'<div class="tag_friend_img_holder">'+
									'<img class="tag_item_image" src="'+rootUrl+'items/uploads/profilepictures/'+item.fname+'">'+					
								'</div>'+
								'<div class="tag_friend_name">'+item.name+'</div>'+
							'</div>';
							
				$('#tag_search_results').append(elem);
			});
			
		});
		
		
		
		$.post( rootUrl+"statics/searchTagged",{pic_id: pic_id}, function( data ) {		
			$('#already_tagged_friends').empty();
			var json = $.parseJSON(data);
			$.each(json, function(i,item)
			{
				
				var elem = '<a class="already_tagged_item"  href="'+rootUrl+'dog/'+item.id+'">'+							
								'<img class="already_tagged_dot" src="'+rootUrl+'items/frontend/img/tagged_dot.png" />'+
								'<div class="tag_item_name">'+item.name+'</div>'+
							'</a>';
							
				$('#already_tagged_friends').append(elem);
			});
			
		});

		
		
		
		$('#tag_holder').css({top:posY, left:posX}).attr('pic', pic_id);
		$('#tag_holder').fadeIn();
	});
	
	$('.comment_image, .image_social_comment').click(function(){
		/*var posX = $(this).parent().offset().left-260;
		var posY = $(this).parent().offset().top-95;	*/
		
		var windowHeight = $(window).height();
		var windowWidth = $(window).width();
	
		var posX = (windowWidth-420)/2;
		var posY = 200;
		
		var pic_id = $(this).attr('pid');
		
	
		$.post( rootUrl+"statics/get_photo_comments",{pid: pic_id}, function( data ) {
				
			$('#user_comments').empty();
			var json = $.parseJSON(data);
			$.each(json, function(i,item)
			{
				var deletebtn = "";
				if(user_id == item.uid)
				{
					deletebtn = '<div class="delete_comment_btn" cid="'+item.cid+'">X</div>'
				}
				var elem = '<div class="comment_photo_item" >'+
								'<div style="overflow:hidden;width:100%">'+
									'<div class="comment_user">'+item.user_id+'</div>'+						
									'<div class="comment_date">'+item.created_date+'</div>'+
									deletebtn+
								'</div>'+
								'<div class="comment_text">'+item.comment+'</div>'+
							'</div>'+
							'<hr class="red_divider" style="border:0px;height:1px;background:#d3004d;width:50%;margin:10px 25%;">';
							
				$('#user_comments').append(elem);
			});
			
		});
		

		
		
		
		$('#comment_holder').css({left:posX}).attr('pic', pic_id);
		$('#comment_holder').fadeIn();
		addCommentDelete();
	});
	
	$('#add_comment_btn').unbind('click');
	$('#add_comment_btn').click(function(){
			
		var pic_id = $('#comment_holder').attr('pic');
		var comment = $('#comment_photo_input').val();
		$.post( rootUrl+"statics/add_photo_comment",{pid: pic_id, comment: comment}, function( data ) {
				
			$('#user_comments').empty();
			var json = $.parseJSON(data);
			$.each(json, function(i,item)
			{
				
				var deletebtn = "";			
				if(user_id == item.user_id)
				{
					deletebtn = '<div class="delete_comment_btn" cid="'+item.id+'">X</div>'
				}
				var elem = '<div class="comment_photo_item" >'+
								'<div style="overflow:hidden;width:100%">'+
									'<div class="comment_user">'+item.first_name+' '+item.last_name+'</div>'+						
									'<div class="comment_date">'+item.created_date+'</div>'+
									deletebtn+
								'</div>'+
								'<div class="comment_text">'+item.comment+'</div>'+
							'</div>'+
							'<hr class="red_divider" style="border:0px;height:1px;background:#d3004d;width:50%;margin:10px 25%;">';
							
				$('#user_comments').append(elem);
			});
			
			$('#comment_photo_input').val("");
			addCommentDelete();
		});
		
		
		
	});
	
	$('.tag_friend_item').click(function(){
		$this = $(this);
		var pic_id = $('#tag_holder').attr('pic');
		var dog_id = $(this).attr('dog');
		$.post( rootUrl+"statics/tagDog",{pic_id:pic_id, dog_id:dog_id}, function( data ) {		
			$this.remove();
		});
	});
	
	
	$('.tag_overlay_close').click(function(){
		$(this).parent().fadeOut();
	});
	
	$(".accept_activity").click(function() {
		var activity_id = $(this).attr('activity');
		var elem = $(this).parent();
		var $this = $(this);
		$.post( rootUrl+"statics/accept_activity",{activity_id:activity_id}, function( data ) {		
			$this.hide();
			elem.find('.decline_activity').hide();
			elem.hide().remove();
			$('#activity_holder').append(elem);
			elem.fadeIn();
		});
	});
	
	$(".decline_activity").click(function() {
		var activity_id = $(this).attr('activity');
		var elem = $(this).parent();
		$.post( rootUrl+"statics/decline_activity",{activity_id:activity_id}, function( data ) {		
			elem.fadeOut().remove();
		});
	});
	
	$(".like_image").unbind('click');
	$(".like_image").click(function() {
		var pic_id = $(this).attr('pid');
		var $this = $(this);
		var like_count = $this.parent().find(".the_likes").text();
		$.post( rootUrl+"statics/addLike",{pid:pic_id}, function( data ) {		
			var newCount = parseInt(like_count)+1;
			$this.parent().find(".the_likes").text(newCount);
			$this.remove();
		});
	});
	
	$(".image_social_like").unbind('click');
	$(".image_social_like").click(function() {
		var pic_id = $(this).attr('pid');
		var $this = $(this);
		var like_count = $this.parent().parent().find(".the_likes").text();
		$.post( rootUrl+"statics/addLike",{pid:pic_id}, function( data ) {		
			var newCount = parseInt(like_count)+1;
			$this.parent().parent().find(".the_likes").text(newCount);
			$this.remove();

		});
	});
	
	
	$(".friends_item").click(function() {
		var dog_id = $(this).attr('dogId');
		var activity_id = $(this).attr('activity_id');
		var elem = $(this);
		elem.remove();
		$.post( rootUrl+"statics/addActivityGuest",{dog_id:dog_id,activity_id:activity_id}, function( data ) {		
			
			elem.attr("class", "nearby_list_item");
			var img = elem.find(".friends_list_item_image");
			
			img.attr("class", "vets_list_item_image");
			$('#guests_holder').append(elem);
		});
	});
	
	
	$(".friends_item_activity").click(function() {
		var dog_id = $(this).attr('dogId');
		var activity_id = $(this).attr('activity_id');
		var elem = $(this);
		elem.remove();
		$.post( rootUrl+"statics/addActivityGuest",{dog_id:dog_id,activity_id:activity_id}, function( data ) {		
			
			elem.attr("class", "nearby_list_item");
			var img = elem.find(".friends_list_item_image");
			
			img.attr("class", "vets_list_item_image");
			var text = $('#pending_holder').text();
			
			if(text.indexOf("No pending invites") >= 0 )
			{
				$('#pending_holder').empty().append(elem);
			}
			else
			{
				$('#pending_holder').append(elem);
			}
		});
	});
	
	$("#add_photo_btn").click(function() {
		$('#add_photo_upload_btn').click();
		
	});
	
	$("#show_friends").click(function() {
		$('#friends_holder').animate({top:0},500);		
	});
	
	$("#close_overlay").click(function() {
		$('#friends_holder').animate({top:-700},500);		
	});
	
	


	if($('#add_photo_upload_btn').length){
		document.getElementById("add_photo_upload_btn").onchange = function() {
		    document.getElementById("add_photo_form").submit();
		};
	}
	
	
	if($('#add_photo_upload_btn_ie').length){
		document.getElementById("add_photo_upload_btn_ie").onchange = function() {
		    document.getElementById("add_photo_ie").submit();
		};
	}
	
	
	if($('#add_coverimage_ie').length){
		document.getElementById("add_coverimage_ie").onchange = function() {
		    document.getElementById("add_cover_ie").submit();
		};
	}
	
	if($('#add_dogprofileimage_ie').length){
		document.getElementById("add_dogprofileimage_ie").onchange = function() {
		    document.getElementById("add_dogprofile_ie").submit();
		};
	}
	
	if($('#add_profileimage_ie').length){
		document.getElementById("add_profileimage_ie").onchange = function() {
		    document.getElementById("add_profile_ie").submit();
		};
	}
	
	
	$("#edit_user_dogsitter_btn").click(function() {
		var chkbx = $('#edit_dogsitter');
		
		
	    if(chkbx.is(':checked')) {
	       $('#edit_dogsitter').prop('checked', false);
	      // $('#edit_dogsitter_data').fadeOut();
	       $("#edit_user_dogsitter_btn").text(show_profile_sitter);
	    }
	    else{
		    $('#edit_dogsitter').prop('checked', true);  
		  //  $('#edit_dogsitter_data').fadeIn();
		    $("#edit_user_dogsitter_btn").text(hide_profile_sitter);
	    }
	});
	
	
	$(".rate").hover(function() {
	    var rating = $(this).attr("rating");
	    for(var i=1; i<=5;i++){
		    $("#empty_circle_"+i).show();
		    $("#full_circle_"+i).hide();		    
	    }
	    $("#empty_circle_"+i).hide();
		$("#full_circle_"+i).show();
	    for(var i=1; i<=rating;i++){
		    $("#empty_circle_"+i).hide();
		    $("#full_circle_"+i).show();		    
	    }
	});
	
	
	$("#dog_detail_add_friend").click(function() {
		var length = $('.dog_list_item').length;
		action = "Friend"; 
		if(length==1){
			$('.dog_list_item').addClass("selected");
			$("#save_dog_selection").click();
			$('#dog_detail_add_friend').hide();
			$('#dog_detail_add_enemy').hide();
			$('#saved_relation_friend').fadeIn();
			
		}
		else{		 
			$('#dog_holder_text').text(select_dog_friend);
			$('#my_dog_holder').animate({top:320},500);	
		}
	   
	});
	
	$("#dog_detail_add_enemy").click(function() {
	   var length = $('.dog_list_item').length;
		action = "Enemy";  
		if(length==1){
			$('.dog_list_item').addClass("selected");
			$("#save_dog_selection").click();
			$('#dog_detail_add_friend').hide();
			$('#dog_detail_add_enemy').hide();
			$('#saved_relation_enemy').fadeIn();
		}
		else{		 
			 $('#dog_holder_text').text(select_dog_enemy);
			 $('#my_dog_holder').animate({top:320},500);
		}
	   
	   
	   
	  
	});
	
	
	$("#embed_dog_overview").click(function() {
		$("#embed_holder").fadeIn('fast');
		$("#embed_input").select();
	});
	
	$("#add_new_album").click(function() {
		$(this).hide();
		$("#add_album_form").fadeIn('fast');
	});
	
	$("#cancel_dog_selection").click(function() {	   
	   $('#my_dog_holder').animate({top:-1500},500);
	});
	
	$("#share_fb").click(function() {
	   FB.ui({
		  method: 'share',
		  href: rootUrl
		}, function(response){}); 
	});
	
	
	$("#dog_detail_fb").click(function() {
		var dog_id = $(this).attr('dog');
		var pretty = $(this).attr('pretty');
		//var component = "dog/"+dog_id;
		facebookShare(dog_id,pretty);
	});
	
	$("#dog_detail_tw").click(function() {
		var pretty = $(this).attr('pretty');
		var component = "profile/"+pretty;
		tweetShare(component);
	});
	
	$("#invite_tw").click(function() {	
		var component = "TEXT";
		tweetShare(component);
	});
	

	$("#invite_fb").click(function() {
		facebookShareInvite();
	});
	
	$("#profile_search_submit").click(function() {
	   var search = $('#profile_search').val();
	   var breed = $('#dog_breed_select').val();
	   var nearby_users = [];
	   var $this = $(this);
	   $this.text(loading_text);
	   if( $('#nearby_dogs:checked').length > 0){
	  
	   		var nearby = "true";
	   		if(navigator.geolocation) {
			    navigator.geolocation.getCurrentPosition(function(position) {
			      var lat = position.coords.latitude
			      var lon = position.coords.longitude;
			      var from = new google.maps.LatLng(lat, lon);
			      
			           
			      
			     
			      $.post(rootUrl+'statics/nearby_users',function(data)
			      {
			      	
					var json = $.parseJSON(data);
					
					$.each(json, function(i,item)
					{
						var arr = item.geo_location.split(',');
						var lat = parseFloat(arr[0]);
						var lng = parseFloat(arr[1]);
					   var to = new google.maps.LatLng(lat,lon);
					   var distance = google.maps.geometry.spherical.computeDistanceBetween(from, to);
					   distance = distance/1000;
					  
					   if(distance<nearby_distance_limit)
					   {
					   		nearby_users.push(item); 
					   }
					   else
					   {
						  
					   }
					});
					
					$.post( rootUrl+"dog/profile_search",{search:search, breed:breed, nearby:nearby, users:nearby_users}, function( data ) {
						$('#resultHolder').empty().append(data);
						$this.text(search_text);
						
						
						
						if(data == 'NO')
						{
							
							$('#resultHolder').empty().append(no_dogs_name);
						}
						
						
						
				   });
				});
			     
			    }, function() {
			      handleNoGeolocation(true);
			    });
			  } else {
			    // Browser doesn't support Geolocation
			    handleNoGeolocation(false);
			  }
		   
	   }
	   else{
	   	
		   var nearby = "false";
		   $.post( rootUrl+"dog/profile_search",{search:search, breed:breed, nearby:nearby, users:[]}, function( data ) {
				$('#resultHolder').empty().append(data);
				$this.text(search_text);
				
				if(data == '')
				{
					
					$('#resultHolder').empty().append(no_dogs_name);
				}
			}); 
	   }
	  
	   
	});
	
	$(".rate").click(function() {
	   var what = $(this).attr('who');
	   var rating = $(this).attr('rating');
	   var sitter = $(this).attr('sitter');
	   
	   
	   
	   $.post( rootUrl+"statics/rate",{rating:rating, sitter_id:sitter, what: what}, function( data ) {
			$('#rating_holder').empty().append("<br/>"+sitter_detail_thank_rate);
	   });
	});
	
	$(".dog_list_item").click(function() {
	  // $(this).find(".dog_select_box").prop("checked", !$(this).find(".dog_select_box").prop("checked"));
	  $(this).toggleClass("selected");
	});
	
	
	$("#save_dog_selection").click(function() {
	   $(".dog_list_item").each(function(){
	   		var $this = $(this);
		   var mydog = $(this).attr("mydog");
		   var selected_dog = $(this).attr("selected_dog");
		  if($(this).hasClass("selected")){
		   //if($(this).find(".dog_select_box").prop("checked")){
			  $.post( rootUrl+"dog/addDog"+action,{mydog:mydog, selected_dog:selected_dog}, function( data ) {
				  $('#my_dog_holder').animate({top:-1500},1000);
				  $this.fadeOut('slow');
			 });
		   }
		   $(this).removeClass("selected");        
	   });

	});
	
	// manages the default text in the input textfields
	$('.sidemenu_login_input, .login_login_input, .register_input, .dog_edit_input, .date_short, .date_long').focus(function()
	{
		
		// handles pw field behaviour
		if($(this).hasClass('password_fake'))
		{
			/*var dummy = $(this).attr('id');
			
			$(this).addClass('input_hidden');
			
			$('#' + dummy.substring(0,dummy.length-5)).removeClass('input_hidden');
			$('#' + dummy.substring(0,dummy.length-5)).focus();*/
		}
		// non-pw field behaviour
		else
		{
			if($(this).val() == $(this).attr('defaultText'))
			{
				$(this).val("");
				$(this).removeClass('input_default');
			}				
		}
	});
	$('.sidemenu_login_input, .login_login_input, .register_input, .dog_edit_input, .date_short, .date_long').blur(function()
	{
		// handles pw field behaviour
		if($(this).hasClass('password_real'))
		{
			/*if($(this).val() == "")
			{
				$(this).addClass('input_hidden');	
				$('#' + $(this).attr('id') + '_fake').removeClass('input_hidden');
			}*/
		}
		// non-pw field behaviour
		else
		{
			if($(this).val() == "")
			{
				$(this).val($(this).attr('defaultText'));
				$(this).addClass('input_default');
			}
		}	
	});
	
	// those pseudo "buttons consisting of an img and a div"
	$('.pseudo_form_submit').click(function()
	{
		if($(this).parent().children('div').length)
		{
			$(this).parent().children('div').each(function()
			{
				$(this).children('').each(function()
				{
					if($(this).val() == $(this).attr('defaulttext'))
						$(this).val("");
				});	
			});			
		}
		else
		{
			$(this).parent().children('input').each(function()
			{
				if($(this).val() == $(this).attr('defaulttext'))
					$(this).val("");
			});				
		}

		$(this).parent().submit();
	});
	
	$('#sidemenu_static .sidemenu_item').click(function()
	{
		switch($(this).attr('id'))
		{
			case 'memberarea':
				animateMemberArea(close);
				break;
		}
	});
	
	
	$('.sidemenu_item').hover(function(e){
		var $this = $(this);
		$('.sidemenu_item').each(function(){
			
		//	$(this).children().first().css({color:"#4b4b4b"});
			$(this).children().first().find(".menu_icon").show();
			$(this).children().first().find(".white_icon").hide();
		});
		
	//	$this.children().first().css({color: "#d4003d"});
		$this.children().first().find(".menu_icon").hide();
		$this.children().first().find(".white_icon").show();
	});
	
	$('.sidemenu_submenu_item').hover(function(e){
		var $this = $(this);
		$('.sidemenu_submenu_item').each(function(){
			$(this).css({background:"none"});
			$(this).children().first().css({color:"#005279"});
			
		});
		
		$this.css({background: "#005279"});
		$this.children().first().css({color: "white"});
	});
	
	
	// pseudo image upload handling	
	$('.pseudo_image_upload').click(function()
	{
		if(!$('html').hasClass('no-filereader'))
		{
			$('#' + $(this).attr('formfield')).click();
		}
	});
	
	function resetFormElement(e) {
	  e.wrap('<form>').closest('form').get(0).reset();
	  e.unwrap();
	}
	
	
	$('.dog_edit_hidden_upload').change(function()
	{
		if(!$('html').hasClass('no-filereader'))
		{
			var preview = $(this).attr("preview");	
			var type = "coverpictures";
			var is_user = false;
			
			 /*LOADING OVERLAY SHOW*/
		    	$('#loadingBG').fadeIn();
		    	$('#loadingGif').fadeIn();
				$("body").css("overflow", "hidden");
			
			
			if(preview == "dog_edit_profile_upload")
			{
				type = "profilepictures";
			}
			else if(preview == "user_edit_profile_upload")
			{
				type = "profilepictures";
				is_user = true;
			}
			else if(preview == "vet_edit_profile_upload")
			{
				type = "profilepictures";
				is_user = false;
			}
			else
			{
				type = "coverpictures";
			}
		
		
		
		
		
			oFReader = new FileReader();
			var dummy = $(this);
			
			oFReader.onload = function (oFREvent) 
	        {
	            var dataURI = oFREvent.target.result;
	           // $('#' + dummy.attr('preview')).children('img').attr('src', dataURI);
	        };
	
	    	oFReader.readAsDataURL(document.getElementById($(this).attr('id')).files[0]);
	    	
	    	
	    	
		    var file_data = $(this).prop('files')[0];   
		    var form_data = new FormData();                  
		    form_data.append('file', file_data);
		          
		   
		    
		                             
		    $.ajax({
		                url: rootUrl+"statics/ajax_upload/"+type, // point to server-side PHP script 
		                dataType: 'text',  // what to expect back from the PHP script, if anything
		                cache: false,
		                contentType: false,
		                processData: false,
		                data: form_data,                         
		                type: 'post',
		                success: function(php_script_response){
		                
		                	var json = $.parseJSON(php_script_response);
		                	
		                     // display response from the PHP script, if any
		                    if(php_script_response != "FAIL")
		                    {
		                    	var source = rootUrl+"items/uploads/"+type+"/"+json.fname;
		                    	var elem = "<img src="+source+">";
			                    $("#image_crop_holder").append(elem);
			                    $('#finish_crop').attr("image_type", type);
			                    $('#finish_crop').attr("user", is_user);
			                    $('#finish_crop').attr("image_name", "items/uploads/"+type+"/"+json.fname);
			                    $('#finish_crop').attr("file_name", json.fname);
			                //    $('#finish_crop').attr("image_id", json.image_id);
			                    
			                     $('#finish_crop_vet').attr("image_type", type);
			                    $('#finish_crop_vet').attr("user", is_user);
			                    $('#finish_crop_vet').attr("image_name", "items/uploads/"+type+"/"+json.fname);
			                    $('#finish_crop_vet').attr("file_name", json.fname);
			                  //  $('#finish_crop_vet').attr("image_id", json.image_id);
			                    $('#image_cropping_overlay').fadeIn();
			                  
			                   if(type == "coverpictures")
			                   {
				                   $('#crop_title').text(cover_image_text);
				                   $('input[name="selection_x1"]').val(0);
						            $('input[name="selection_y1"]').val(0);
						            $('input[name="selection_x2"]').val(600);
						            $('input[name="selection_y2"]').val(250); 
				               cropping_overlay_instance = $('#image_crop_holder').imgAreaSelect({
				                    	x1: 0, y1: 0, x2: 600, y2: 250, 
				                    	aspectRatio: '600:250',
				                    	minHeight:250, 
				                    	minWidth:600, 
				                    	maxWidth:1000,
				                    	handles: true,
					                    onSelectEnd: function (img, selection) {
								            $('input[name="selection_x1"]').val(selection.x1);
								            $('input[name="selection_y1"]').val(selection.y1);
								            $('input[name="selection_x2"]').val(selection.x2);
								            $('input[name="selection_y2"]').val(selection.y2);            
								        }, 
								    }); 
			                   }
			                   else
			                   {
			                   		$('#crop_title').text(profile_image_text);
			                   		$('input[name="selection_x1"]').val(0);
						            $('input[name="selection_y1"]').val(0);
						            $('input[name="selection_x2"]').val(190);
						            $('input[name="selection_y2"]').val(220);
				               cropping_overlay_instance = $('#image_crop_holder').imgAreaSelect({
			                    	x1: 0, y1: 0, x2: 190, y2: 220, 
				                    	aspectRatio: '190:220',
				                    	minHeight:220, 
				                    	minWidth:190,
				                    	maxWidth:500, 
				                    	handles: true,
					                    onSelectEnd: function (img, selection) {
								            $('input[name="selection_x1"]').val(selection.x1);
								            $('input[name="selection_y1"]').val(selection.y1);
								            $('input[name="selection_x2"]').val(selection.x2);
								            $('input[name="selection_y2"]').val(selection.y2);            
								        }, 
								    });
			                   }
			                   
			                   
		                    }
		                    else
		                    {
			                    window.alert(upload_failed_text);
		                    }
		                    
		                    
		                    /* LOADING OVERLAY HIDE*/
		                    $('#loadingBG').hide();
					    	$('#loadingGif').hide();
							$("body").css("overflow", "visible");
		                }
		     });
			
	    	
    	}
    	
    	
	});
	$('#close_crop').click(function(){
		var elem = $('.dog_edit_hidden_upload');
		resetFormElement(elem);
		 $("#image_crop_holder").empty();
		$('.imgareaselect-selection').parent().hide();
		$('.imgareaselect-outer').hide();
		$(this).parent().fadeOut();
		
	});
	
	$('#finish_crop').click(function(){
		var type = $(this).attr("image_type");
		var the_user = $(this).attr("user");
		var fname = $(this).attr("file_name");
		var dog_id = $(this).attr("dog_id");
		var image_id = $(this).attr("image_id");
		
		if(type == "coverpictures")
		{
			var w = 600;
			var h = 250;
		}
		else
		{
			var w = 190;
			var h = 220;
		}
		
		var x1 = $("#selection_x1").val();
		var x2 = $("#selection_x2").val();
		var y1 = $("#selection_y1").val();
		var y2 = $("#selection_y2").val();
		
		
		//var file = $(this).parent().find('#image_crop_holder').find('img').attr("src");
		var file = $(this).attr("image_name");
		
		$.post(rootUrl+"statics/crop_image", { x1: x1, x2 : x2, y1 : y1, y2 : y2, w : w, h : h, fname : file}, function(data){
		     if(the_user == "true")
		     {
		     	var uid = $(this).attr('uid');
			     $.post(rootUrl+"statics/save_profilepicture_user", {user_id: uid, fname: fname }, function(data){
					    window.location.href = rootUrl+"editprofile";
				}); 
		     }
		     else
		     {
		     	
			     if(type == "coverpictures")
			     {
			     	
				     $.post(rootUrl+"statics/save_coverpicture", {dog_id: dog_id, fname: fname, image_id:image_id}, function(data){
						    window.location.href = rootUrl+"edit_dog/"+dog_id;
					});
			     } 
			     else
			     {
				     $.post(rootUrl+"statics/save_profilepicture", {dog_id: dog_id, fname: fname, image_id:image_id}, function(data){
						    window.location.href = rootUrl+"edit_dog/"+dog_id;
					});
			     } 
		     }
		     
		     
		});
	
	});
	
	
	$('#finish_crop_vet').click(function(){
		var type = $(this).attr("image_type");
		var the_user = $(this).attr("user");
		var fname = $(this).attr("file_name");
		var uid = $(this).attr("uid");
		var image_id = $(this).attr("image_id");
		
		if(type == "coverpictures")
		{
			var w = 600;
			var h = 250;
		}
		else
		{
			var w = 190;
			var h = 220;
		}
		
		var x1 = $("#selection_x1").val();
		var x2 = $("#selection_x2").val();
		var y1 = $("#selection_y1").val();
		var y2 = $("#selection_y2").val();
		
		//var file = $(this).parent().find('#image_crop_holder').find('img').attr("src");
		var file = $(this).attr("image_name");
		
		$.post(rootUrl+"statics/crop_image", { x1: x1, x2 : x2, y1 : y1, y2 : y2, w : w, h : h, fname : file}, function(data){
		    
			     	
		     $.post(rootUrl+"statics/save_profile_vet", {uid: uid, fname: fname, image_id:image_id}, function(data){
				    window.location.reload();
			});
			
		     
		     
		});
	
	});
	
	
	
	
	
	$('.frenemies_list_big .friend_item, .my_friends_item').mouseenter(function()
	{
	/*	var hover = $('#dog_frenemies_hover');
	    var param = [];
	    
	    param.push($(this).attr('id'));
	    		
	    $.ajax({
	        url: rootUrl+'dog/getShortinfo',
	        data: {params: param},
	        type:"POST",
	        success: function(data) 
	        {
	            var json=$.parseJSON(data);
			    $('#dog_frenemies_hover_desc_name').text(json.name);
			    $('#dog_frenemies_hover_desc_breed').text(json.breed);
			    $('#dog_frenemies_hover_desc_age').text(json.age);
			    $('#dog_frenemies_hover_desc_owner').text(json.owners); 
			    $('#dog_frenemies_hover_profilepicture').attr("src", rootUrl+"items/uploads/profilepictures/"+json.profile_picture); 
	        }
	    });		
		hover.css({ 'top': $(this).offset().top-135, 'left': $(this).offset().left-$('#content').offset().left });
		hover.fadeIn(150);
		*/
	});
	$('.frenemies_list_big .friend_item, .my_friends_item').mouseleave(function()
	{
		$('#dog_frenemies_hover').hide();
	});	

	$('.gallery_rotate_right').click(function()
	{
		var last = parseInt(getLastActiveGalleryItem()) + 1;
		var first = parseInt(getFirstActiveGalleryItem()); 
		var last_obj = $(".vet_details_gallery_item[gallery_id='" + (last) + "']");
		if(last < gallery_item_count)
		{
			$(".vet_details_gallery_item[gallery_id='" + (first) + "']").removeClass('gallery_item_active');
			$(".vet_details_gallery_item[gallery_id='" + (last) + "']").addClass('gallery_item_active');
		}
	});
	$('.gallery_rotate_left').click(function()
	{
		var last = parseInt(getLastActiveGalleryItem());
		var first = parseInt(getFirstActiveGalleryItem()) - 1; 
		var last_obj = $(".vet_details_gallery_item[gallery_id='" + (last) + "']");
		if(first >= 0)
		{
			$(".vet_details_gallery_item[gallery_id='" + (first) + "']").addClass('gallery_item_active');
			$(".vet_details_gallery_item[gallery_id='" + (last) + "']").removeClass('gallery_item_active');
		}
	});	

	$('#vet_detail_favorite').click(function()
	{
	    $.ajax({
	        url: rootUrl+'statics/vet_favorite',
	        data: {user_id: user_id, vet_id: $(this).attr('vet_id')},
	        type:"POST",
	        success: function(data) 
	        {
	            var json=$.parseJSON(data);
				
	        }
	    });     			
	});
	
	$('#friend_search_button').click(function(){
		var name = $('#friend_search_input').val();
		searchFriendsName(name);
	});
	
	
	$('#fb_register').click(function(){
		loginUser("register");
	});
	
	
	
	$('#dog_delete').click(function(){
		$('#delete_confirm_holder').fadeIn();
	});
	
	$('#dog_delete_yes').click(function(){
		var dog_id = $(this).attr('did');
		$.post(rootUrl+'dog/deleteDog', {dog_id:dog_id}, function(data) 
		{
			if(data=="REDIRECT"){
				window.location.href = rootUrl+"dog_overview";
			}
		});
	});
	
	$('#dog_delete_no').click(function(){
		$('#delete_confirm_holder').fadeOut();
	});
	
	
	$('#remove_album').click(function(){
		$(this).hide();
		$('#delete_album_confirm_holder').fadeIn();
	});
	
	$('#album_delete_yes').click(function(){
		var album_id = $(this).attr('album_id');
		$.post(rootUrl+'statics/delete_album', {album_id:album_id}, function(data) 
		{	
			
			window.location.href = rootUrl+"albums";	
		});
	});	
	
	$('#album_delete_no').click(function(){
		$('#delete_album_confirm_holder').hide();
		$('#remove_album').fadeIn();
	});
	
	
	$('.remove_image').click(function(){
		
		var $this = $(this);
		$this.fadeOut();							
		$this.parent().parent().find('.delete_editor').fadeIn();	
		
	});
	
	$('.photo_delete_no').click(function(){
		
		var $this = $(this);					
		$this.parent().parent().fadeOut();	
		$('.remove_image').fadeIn();

	});
	
	
	$('.photo_delete_yes').click(function(){
		var pic_id = $(this).attr('pid');
		var $this = $(this);
		$.post(rootUrl+'statics/delete_image', {pid:pic_id}, function(data) 
		{	
						
			$this.parent().parent().parent().fadeOut();	
		});
	});
}

function getFirstActiveGalleryItem()
{
	var first = 9999;
	$('.gallery_item_active').each(function()
	{
		if($(this).attr('gallery_id') < first)
			first = $(this).attr('gallery_id');
	});
	return first;
}

function getLastActiveGalleryItem()
{
	var last = 0;
	$('.gallery_item_active').each(function()
	{
		if($(this).attr('gallery_id') > last)
			last = $(this).attr('gallery_id');
	});
	return last;
}



function animateMemberArea(action)
{
	var obj = $('.sidemenu_submenu');
	
	if(action == "close")
	{
		if(obj.hasClass('open'))
		{
			obj.animate({left:0}, 500, function()
			{
				//$('#topicListHolder').remove();
			});
			obj.removeClass('open');
		}
	}
	else
	{		
		obj.animate({left:185}, 400, function()
		{
			obj.addClass('open');
		});
	}	
}

$.fn.exists = function () {
    return this.length !== 0;
}




function getAddressLocation(which, address, nearby_multiplier){
	  nearby_multiplier = typeof nearby_multiplier !== 'undefined' ? nearby_multiplier : 1;
	  $('#vets_list').empty();
      $('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>"+search_searching+"</div>");
    
      
      geocoder.geocode({'address': address}, function(results, status) {
			      if (status == google.maps.GeocoderStatus.OK) {
					latlng = results[0].geometry.location;	
					
					var from = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());	
					
					
			         $.post(rootUrl+'statics/get_'+which,function(data)
					 {
					 	
					 	if(data == "")
					 	{
						 	$('#vets_list').empty();
									
									
									if(which == "sitters")
									{
										$('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>"+search_nothing_found_sitter+"</div>");
									}
									else
									{
									
										$('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>"+search_nothing_found+"</div>");
									}

					 	}
					 	else
					 	{
					      	$('#vets_list').empty();
							var json = $.parseJSON(data);
							var x = 1;
							var margin = "";
							
							$.each(json, function(i,item)
							{
								
								var arr = item.geo_location.split(',');
								var lat = parseFloat(arr[0]);
								var lng = parseFloat(arr[1]);
							   var to = new google.maps.LatLng(lat,lng);
							   var distance = google.maps.geometry.spherical.computeDistanceBetween(from, to);
							   distance = distance/1000;
							  		
							  
							   if(parseFloat(distance)<parseFloat(nearby_distance_limit*nearby_multiplier))
							   {
							   
							   		switch(which){
								   		case "zones":{
									   		var text = item.name;
									   		var cl = "zone_search_item";
									   		var pic = item.profilepicture;
								   		};break;
								   		case "sitters":{
									   		var text = item.first_name+" "+item.last_name;
									   		var pic = item.profile_picture;
									   		var cl = "nearby_list_item";
								   		};break;
								   		case "vets":{
									   		var text = item.title+" "+item.firstname+" "+item.lastname;
									   		var pic = "";
									   		var cl = "nearby_list_item";
								   		};break;
								   		case "places":{
									   		var text = item.name+"<br/>"+item.type;
									   		var pic = "";
									   		var cl = "nearby_list_item";
								   		};break;
								   
								   }
								   
								if(x%3==0)
						   		{
						   			margin = "style='margin-right:0px'";
						   			
						   		}
						   		else
						   		{
							   		margin = "style='margin-right:15px'";
						   		}
						   		
								if(pic!=""){
	   								var image =	'<img class="vets_list_item_image" src="'+rootUrl+'items/uploads/profilepictures/'+pic+'" border="0">'+
	   											'<div class="div_white_over"></div>';
	   							}
	   							else
	   							{
		   							var image = '';
	   							}
								
								
								
								
							   		if(which != 'places')
									{
								   		var new_item = '<a distance="'+(distance*1000)+'" style="text-decoration:none;" href="'+rootUrl+which+'/'+item.id+'"><div  class="'+cl+'" '+margin+'>'+
									   						image+	   									   							
								   							'<div class="zones_list_item_desc">'+text+'</div>'+
								   						'</div></a>';
								   }
								   else
								   {
								   	var new_item = '<a distance="'+(distance*1000)+'" style="text-decoration:none;" href="'+rootUrl+'place/'+item.pretty_url+'"><div style="background:#d4003d;"  class="'+cl+'" '+margin+'>'+		   													   							
								   							'<div class="zones_list_item_desc" >'+text+'</div>'+
								   						'</div></a>';
								   }
							   		
								   $('#vets_list').append(new_item); 							   
								   x++;
							   }
							   else
							   {
							   	
							
							   }
							});
							
							if(x == 1)
							{
								if(nearby_multiplier<11)
								{
									getAddressLocation(which, address, nearby_multiplier+1);
								}
								else
								{
									$('#vets_list').empty();
									
									
									if(which == "sitters")
									{
										$('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>"+search_nothing_found_sitter+"</div>");
									}
									else
									{
									
										$('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>"+search_nothing_found+"</div>");
									}
								}
							}
							
							sortByDistance(which);
						}
					});
			      } else {		      	
			        handleNoGeolocation(false);
			      }
		});

}

function sortByDistance(which)
{
	if(which == "zones")
	{
		var zoneList = $('#vets_list').find('a');
		zoneList.sort(function (a, b) {
	
	      var contentA =parseInt( $(a).attr('distance'));
	      var contentB =parseInt( $(b).attr('distance'));
	           
	      
	      return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
	   });
	   
	   
	   
	   $('#vets_list').html(zoneList);
	   
	   var items = $('#vets_list').find('.zone_search_item');
	   $.each(items, function(i,item){
	   	var counter = i+1;
		   if(counter%3==0)
		   {
			   $(this).css({'margin-right': '0px'});
		   }
		   else
		   {
			    $(this).css({'margin-right': '15px'});
		   }
	   });
	   
	}
	else
	{
		 var nearbylist = $('#vets_list').find('a');
	   nearbylist.sort(function (a, b) {
	
	      var contentA =parseInt( $(a).attr('distance'));
	      var contentB =parseInt( $(b).attr('distance'));
	      return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
	   });
	   
	   $('#vets_list').html(nearbylist);
	   
	   var items = $('#vets_list').find('.nearby_list_item');
	   $.each(items, function(i,item){
	   	var counter = i+1;
		   if(counter%3==0)
		   {
			   $(this).css({'margin-right': '0px'});
		   }
		   else
		   {
			    $(this).css({'margin-right': '15px'});
		   }
	   });
	}
	
   
   
  
}


function getMyLocation(which){
	 $('#vets_list').empty();
      $('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>"+search_searching+"</div>");
	if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var lat = position.coords.latitude
      var lon = position.coords.longitude;
      var from = new google.maps.LatLng(lat, lon);
      
           
      
     
      $.post(rootUrl+'statics/get_'+which,function(data)
      {
      	$('#vets_list').empty();
		var json = $.parseJSON(data);
		var x = 1;
		
		$.each(json, function(i,item)
		{
			
			var arr = item.geo_location.split(',');
			var lat = parseFloat(arr[0]);
			var lng = parseFloat(arr[1]);
		   var to = new google.maps.LatLng(lat,lng);
		   var distance = google.maps.geometry.spherical.computeDistanceBetween(from, to);
		   distance = distance/1000;
		   var margin = "";
		   if(distance<nearby_distance_limit)
		   {
		   		switch(which){
			   		case "zones":{
				   		var text = item.name;
				   		var pic = item.profilepicture;
				   		var cl = "zone_search_item";
			   		};break;
			   		case "sitters":{
				   		var text = item.first_name+" "+item.last_name;
				   		var pic = item.profile_picture;
				   		var cl = "nearby_list_item";
			   		};break;
			   		case "vets":{
				   		var text = item.title+" "+item.firstname+" "+item.lastname;
				   		var pic = "";
				   		var cl = "nearby_list_item";
			   		};break;
			   		case "places":{
				   		var text = item.name;
				   		var pic = "";
				   		var cl = "nearby_list_item";
			   		};break;
		   		}
		   		
		   		if(x%3==0)
		   		{
		   			margin = "style='margin-right:0px'";
		   		}
		   		
		   		if(pic!=""){
   								var image =	'<img class="vets_list_item_image" src="'+rootUrl+'items/uploads/profilepictures/'+pic+'" border="0">'+
   											'<div class="div_white_over"></div>';
   							}
   							else
   							{
	   							var image = '';
   							}
			if(which != 'places')
			{
		   		var new_item = '<a style="text-decoration:none;" href="'+rootUrl+which+'/'+item.id+'"><div class="'+cl+'" '+margin+'>'+
			   						image+	   									   							
		   							'<div class="zones_list_item_desc">'+text+'</div>'+
		   						'</div></a>';
		   }
		   else
		   {
		   	var new_item = '<a style="text-decoration:none;" href="'+rootUrl+'place/'+item.pretty_url+'"><div style="background:#d4003d;" class="'+cl+'" '+margin+'>'+		   													   							
		   							'<div class="zones_list_item_desc" >'+text+'</div>'+
		   						'</div></a>';
		   }		
			   $('#vets_list').append(new_item); 
			  
			   
			   x++;
		   }
		   else
		   {
			  
		   }
		   
		   if(x==1)
		   {
		   	 $('#vets_list').empty();
     		 $('#vets_list').append("<div style='text-align:center;width:100%;color:#d4003d;'>Nothing found nearby...</div>");
		   }
		});
	});
     
    }, function() {
      handleNoGeolocation(true);
    });
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }
}

function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }
}



function initMap() {
    var mapCanvas = document.getElementById('map_canvas');
    if($("#map_canvas").length){
	    var mapOptions = {
	      center: new google.maps.LatLng(48.14257669384092, 16.34922897338868),
	      zoom: 8,
	      streetViewControl: false,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    }
	    var map = new google.maps.Map(mapCanvas, mapOptions);
	    var geo_loc = $("#edit_geolocation").val();
		var arr = geo_loc.split(',');
		var lat = parseFloat(arr[0]);
		var lng = parseFloat(arr[1]);
	
	   
	    if(geo_loc!="NULL" && geo_loc!=""){
	    	var myLatlng = new google.maps.LatLng(lat, lng);
			marker = new google.maps.Marker({position: myLatlng, map: map}); 
			markerPlaced = true;
	    }
		 google.maps.event.addListener(map, 'click', function(event) {  
		 	if(!markerPlaced){
			 	marker = new google.maps.Marker({position: event.latLng, map: map});
			 	markerPlaced = true;
		 	} 
		   	else{
			   	marker.setPosition(event.latLng);
		   	}	   
		   //Geo location
	       $("#edit_geolocation").val(event.latLng.lat()+", "+event.latLng.lng());
		   var geocoder = new google.maps.Geocoder();
	           geocoder.geocode({
	              "latLng":event.latLng
	             }, function (results, status) {
	                 if (status == google.maps.GeocoderStatus.OK) {
						 var streetAddress = "";
				       for (var i=0; i<results[0].address_components.length; i++) {
				          for (var b=0;b<results[0].address_components[i].types.length;b++) {
							  	
							  	//Country
							  	if (results[0].address_components[i].types[b] == "country") {		                    
				                    $("#edit_country").val(results[0].address_components[i].short_name);			                   
				                }
							  	
							  	//State
							  	 if (results[0].address_components[i].types[b] == "administrative_area_level_1") {		                  
				                    $("#edit_state").val(results[0].address_components[i].long_name);			                   
				                }
							  	
							  	//City
				                if (results[0].address_components[i].types[b] == "sublocality" || results[0].address_components[i].types[b] == "locality") {			                    
				                    $("#edit_city").val(results[0].address_components[i].long_name);			                   
				                }
				                
				                //Zip
				                if (results[0].address_components[i].types[b] == "postal_code") {			                    
				                    $("#edit_zip").val(results[0].address_components[i].long_name);			                   
				                }
				                
				               //Address number
				                if (results[0].address_components[i].types[b] == "street_number") {			                    
				                    streetAddress += results[0].address_components[i].long_name+" ";			                   
				                }
				                
				               //Address route
				                if (results[0].address_components[i].types[b] == "route") {			                    
				                    streetAddress += results[0].address_components[i].long_name;			                   
				                }
				            }
				        }
	                     //Address
	                     $("#edit_address").val(streetAddress);
	                     
	                     
	                 }
	           });
		       
		 }); 
	}  	  
}

function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}

function switchCountryselect(savedCountry)
{
	
	if(savedCountry == ""){
	  $('#edit_country').val(country);
	};
	
	initMapProfile();
}


function moveMapCenter()
{
	var country_select = $("#edit_country option:selected").text();
	geocoder.geocode({'address': country_select}, function(results, status) {    	
		      if (status == google.maps.GeocoderStatus.OK) {
				 latlng = results[0].geometry.location;	
				 profile_map.setZoom(5);
				 profile_map.setCenter(new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()));	
			  }
		  });
	
	
}


function initMapProfile() {
    var mapCanvas = document.getElementById('map_canvas_profile');
    
    
	
    var center = new google.maps.LatLng(48.14257669384092, 16.34922897338868);
     
    var default_city = $('#edit_city').val();
    var default_country = $("#edit_country option:selected").text();
    var default_street = $('#edit_address').val();
	
	
	var default_address = default_country+" "+default_city+" "+default_street;
	

	geocoder.geocode({'address': default_address}, function(results, status) {    	
	      if (status == google.maps.GeocoderStatus.OK) {
	      		
			 latlng = results[0].geometry.location;		
			 		 
			 center = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
			 if($("#map_canvas_profile").length){
			 
			 	
			 		
				    var mapOptions = {
				      center: center,
				      zoom: 8,
				      streetViewControl: false,
				      mapTypeId: google.maps.MapTypeId.ROADMAP
				    }
				    profile_map = new google.maps.Map(mapCanvas, mapOptions);
				    
				    var geo_loc = $("#edit_geolocation").val();
					var arr = geo_loc.split(',');
					var lat = parseFloat(arr[0]);
					var lng = parseFloat(arr[1]);
			
				   
					
				   
				    if(geo_loc!="NULL" && geo_loc!=""){
				    
				    	
				    	var myLatlng = new google.maps.LatLng(lat, lng);
						marker = new google.maps.Marker({position: myLatlng, map: profile_map}); 
						markerPlaced = true;
				    }
				    
				  
					 google.maps.event.addListener(profile_map, 'click', function(event) {  
					 	if(!markerPlaced){
						 	marker = new google.maps.Marker({position: event.latLng, map: profile_map});
						 	markerPlaced = true;
					 	} 
					   	else{
						   	marker.setPosition(event.latLng);
					   	}	   
					   //Geo location
				       $("#edit_geolocation").val(event.latLng.lat()+", "+event.latLng.lng());
					   var geocoder = new google.maps.Geocoder();
				           geocoder.geocode({
				              "latLng":event.latLng
				             }, function (results, status) {
				                 if (status == google.maps.GeocoderStatus.OK) {
									 var streetAddress = "";
							       for (var i=0; i<results[0].address_components.length; i++) {
							          for (var b=0;b<results[0].address_components[i].types.length;b++) {
										  	
										  	//Country
										  	if (results[0].address_components[i].types[b] == "country") {		                    
							                    $("#edit_country").val(results[0].address_components[i].short_name);			                   
							                }
										  	
										  	//State
										  	 if (results[0].address_components[i].types[b] == "administrative_area_level_1") {		                  
							                    $("#edit_state").val(results[0].address_components[i].long_name);			                   
							                }
										  	
										  	//City
							                if (results[0].address_components[i].types[b] == "sublocality" || results[0].address_components[i].types[b] == "locality") {			                    
							                    $("#edit_city").val(results[0].address_components[i].long_name);			                   
							                }
							                
							                //Zip
							                if (results[0].address_components[i].types[b] == "postal_code") {			                    
							                    $("#edit_zip").val(results[0].address_components[i].long_name);			                   
							                }
							                
							               //Address number
							                if (results[0].address_components[i].types[b] == "street_number") {			                    
							                    streetAddress += results[0].address_components[i].long_name+" ";			                   
							                }
							                
							               //Address route
							                if (results[0].address_components[i].types[b] == "route") {			                    
							                    streetAddress += results[0].address_components[i].long_name;			                   
							                }
							            }
							        }
				                     //Address
				                     $("#edit_address").val(streetAddress);
				                     
				                     
				                 }
				           });
					       
					 }); 
					 
					 
					 
				}
		  }
	      else
	      {
		       
		      geocoder.geocode({'address': default_country}, function(results, status) {    	
			      if (status == google.maps.GeocoderStatus.OK) {
					 latlng = results[0].geometry.location;	
					 var mapOptions = {
				      center: new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()),
				      zoom: 8,
				      streetViewControl: false,
				      mapTypeId: google.maps.MapTypeId.ROADMAP
				    }
				    profile_map = new google.maps.Map(mapCanvas, mapOptions);
					var geo_loc = $("#edit_geolocation").val();
					var arr = geo_loc.split(',');
					var lat = parseFloat(arr[0]);
					var lng = parseFloat(arr[1]);
				
				   
				    if(geo_loc!="NULL" && geo_loc!=""){
				    	var myLatlng = new google.maps.LatLng(lat, lng);
						marker = new google.maps.Marker({position: myLatlng, map: profile_map}); 
						markerPlaced = true;
				    }
					 google.maps.event.addListener(profile_map, 'click', function(event) {  
					 	if(!markerPlaced){
						 	marker = new google.maps.Marker({position: event.latLng, map: profile_map});
						 	markerPlaced = true;
					 	} 
					   	else{
						   	marker.setPosition(event.latLng);
					   	}	   
					  
					   //Geo location
				       $("#edit_geolocation").val(event.latLng.lat()+", "+event.latLng.lng());
					   var geocoder = new google.maps.Geocoder();
				           geocoder.geocode({
				              "latLng":event.latLng
				             }, function (results, status) {
				                 if (status == google.maps.GeocoderStatus.OK) {
									 var streetAddress = "";
							       for (var i=0; i<results[0].address_components.length; i++) {
							          for (var b=0;b<results[0].address_components[i].types.length;b++) {
										  	
										  	//Country
										  	if (results[0].address_components[i].types[b] == "country") {		                    
							                    $("#edit_country").val(results[0].address_components[i].short_name);			                   
							                }
										  	
										  	//State
										  	 if (results[0].address_components[i].types[b] == "administrative_area_level_1") {		                  
							                    $("#edit_state").val(results[0].address_components[i].long_name);			                   
							                }
										  	
										  	//City
							                if (results[0].address_components[i].types[b] == "sublocality" || results[0].address_components[i].types[b] == "locality") {			                    
							                    $("#edit_city").val(results[0].address_components[i].long_name);			                   
							                }
							                
							                //Zip
							                if (results[0].address_components[i].types[b] == "postal_code") {			                    
							                    $("#edit_zip").val(results[0].address_components[i].long_name);			                   
							                }
							                
							               //Address number
							                if (results[0].address_components[i].types[b] == "street_number") {			                    
							                    streetAddress += results[0].address_components[i].long_name+" ";			                   
							                }
							                
							               //Address route
							                if (results[0].address_components[i].types[b] == "route") {			                    
							                    streetAddress += results[0].address_components[i].long_name;			                   
							                }
							            }
							        }
				                     //Address
				                     $("#edit_address").val(streetAddress);
				                     
				                     
				                 }
				           });
					       
					 });
				  }
			  });
		      
		      
	      }
	  });
   
	
   /* if($("#map_canvas_profile").length){
	    var mapOptions = {
	      center: center,
	      zoom: 8,
	      streetViewControl: false,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    }
	    profile_map = new google.maps.Map(mapCanvas, mapOptions);
	    var geo_loc = $("#edit_geolocation").val();
		var arr = geo_loc.split(',');
		var lat = parseFloat(arr[0]);
		var lng = parseFloat(arr[1]);
	
	   
	    if(geo_loc!="NULL" && geo_loc!=""){
	    	var myLatlng = new google.maps.LatLng(lat, lng);
			marker = new google.maps.Marker({position: myLatlng, map: profile_map}); 
			markerPlaced = true;
	    }
		 google.maps.event.addListener(profile_map, 'click', function(event) {  
		 	if(!markerPlaced){
			 	marker = new google.maps.Marker({position: event.latLng, map: profile_map});
			 	markerPlaced = true;
		 	} 
		   	else{
			   	marker.setPosition(event.latLng);
		   	}	   
		   //Geo location
	       $("#edit_geolocation").val(event.latLng.k+", "+event.latLng.A);
		   var geocoder = new google.maps.Geocoder();
	           geocoder.geocode({
	              "latLng":event.latLng
	             }, function (results, status) {
	                 if (status == google.maps.GeocoderStatus.OK) {
						 var streetAddress = "";
				       for (var i=0; i<results[0].address_components.length; i++) {
				          for (var b=0;b<results[0].address_components[i].types.length;b++) {
							  	
							  	//Country
							  	if (results[0].address_components[i].types[b] == "country") {		                    
				                    $("#edit_country").val(results[0].address_components[i].short_name);			                   
				                }
							  	
							  	//State
							  	 if (results[0].address_components[i].types[b] == "administrative_area_level_1") {		                  
				                    $("#edit_state").val(results[0].address_components[i].long_name);			                   
				                }
							  	
							  	//City
				                if (results[0].address_components[i].types[b] == "sublocality" || results[0].address_components[i].types[b] == "locality") {			                    
				                    $("#edit_city").val(results[0].address_components[i].long_name);			                   
				                }
				                
				                //Zip
				                if (results[0].address_components[i].types[b] == "postal_code") {			                    
				                    $("#edit_zip").val(results[0].address_components[i].long_name);			                   
				                }
				                
				               //Address number
				                if (results[0].address_components[i].types[b] == "street_number") {			                    
				                    streetAddress += results[0].address_components[i].long_name+" ";			                   
				                }
				                
				               //Address route
				                if (results[0].address_components[i].types[b] == "route") {			                    
				                    streetAddress += results[0].address_components[i].long_name;			                   
				                }
				            }
				        }
	                     //Address
	                     $("#edit_address").val(streetAddress);
	                     
	                     
	                 }
	           });
		       
		 }); 
		 
		 
		 
	}  */	  
}


function import_instagram(uid)
{
	if(!ajax_call_running)
	{
		ajax_call_running = true;
		$.post(rootUrl+'statics/my_instagram',{user_id:uid},function(data){			
			ajax_call_running = false;
		});
	}
}

function showHideIE9Elements()
{
	if(!$('html').hasClass('no-filereader'))
	{
		file_upload();
		newsfeed_file_upload();
		album_file_upload();
		$('#notIE9').show();
		$('#IE9').hide();
	}
	else
	{
		$('#notIE9').hide();
		$('#IE9').show();
	}
}
	








<style>
	select{width:100%;}
</style>

<script>	
	
	$("meta[property='og\\:image']").attr("content", "<?= site_url('items/uploads/profilepictures/' . $car->profile_picture);?>");
</script>
<div id="content" style="margin-bottom: 280px;color:#ffffff;min-height:600px;">
	
	
	
	<?  if($is_my_car){?>
	
		<div id="delete_mod_overlay">
			<div class="overlayBG"></div>
			<div style="position:relative;z-index:100">
				<img class="close_overlay" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />		
				<div class="titboldit" style="font-size:27px;margin-top:30px;">DELETE MOD</div>
					<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">
					<div style="width:400px;margin:30px auto;">
						<div class="regular">Are you sure you wish to remove this MOD?</div>
						<div style="width:100%;overflow:hidden">
							<div id="delete_mod_yes" class="button" cid="<?= $car->id;?>" style="float:left;width:100px;">YES</div>
							<div id="delete_mod_no" class="button" style="float:right;width:100px;">NO</div>
						</div>	
						
					</div>
				</div>	
			</div>
		</div>	
		
	
	
	
	
	
	
	
	
	<div id="add_modification_overlay" style="">
		<div class="overlayBG"></div>
		<div style="position:relative;z-index:100">
			<img class="close_overlay" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />		
			<div class="titboldit" style="font-size:27px;margin-top:30px;">ADD MODIFICATION</div>
			<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">					
						<div style="width:400px;margin:30px auto;    text-align: center;">	
							<? if($car->vehicle_type == 0) // CAR
								{
							?>
								<input type="hidden" id="engine_type_select" tid="0" name="mod_vehicle_select" value="<?= $engine_type?>">
							 
								<? if($engine_type == "gas"){?>
								<!-- GAS -->
									<select class="mod_type_select" engine="gas" tid="0" name="mod_type_select" >
										<option value="0">Please Select a mod type...</option>
										<option value="audio">Audio</option>
										<option value="body">Body</option>
										<option value="brake">Brakes</option>
										<option value="chassis">Chassis</option>
										<option value="cooling">Cooling system</option>
										<option value="drive_axle">Drive axle</option>
										<option value="engine">Engine</option>
										<option value="exhaust">Exhaust system</option>
										<option value="fuel">Fuel system</option>
										<option value="ignition">Ignition system</option>
										<option value="lighting">Lighting</option>
										<option value="suspension">Suspension</option>
										<option value="transmission">Transmission</option>
										<option value="tuned">Tuned</option>
										<option value="wheels">Wheels & Tires</option>
										<option value="other">Other</option>
									</select>
									<br/>
									
									<select id="mod_default_types" class="mod_subselect" name="mod_default_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Aftermarket</option>
									</select>
									
							
									
									<select id="mod_transmission_types" class="mod_subselect" name="mod_transmission_types" style="display:none;">
							<!--		<option value="1">Manual - Clutch (Stock)</option> -->
										<option value="2">Manual - Clutch (Aftermarket)</option>
							<!--		<option value="3">Automatic - Converter (Stock)</option> -->
										<option value="4">Automatic - Converter (Aftermarket)</option>
				
									</select>
								
									
									<select id="mod_body_types" class="mod_subselect" name="mod_body_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Paint - Factory</option>
										<option value="3">Paint - Custom</option>
										<option value="4">Body mods</option>
									</select>
									
									
									<select id="mod_chassis_types" class="mod_subselect" name="mod_chassis_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Aftermarket</option>
										<option value="3">Bracing</option>
									</select>
								
									
									<select id="mod_drive_axle_types" class="mod_subselect" name="mod_drive_axle_types" style="display:none;">
							<!--		<option value="1">2WD (Stock)</option> -->
										<option value="2">2WD (Upgraded internals)</option>
							<!--		<option value="3">4WD (Stock)</option> -->
										<option value="4">4WD (Upgraded internals)</option>
							<!--		<option value="5">AWD (Stock)</option> -->
										<option value="6">AWD (Upgraded internals)</option>
									</select>
									
									
									<select id="mod_brake_types" class="mod_subselect" name="mod_brake_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
							<!--		<option value="2">Upgraded - Front - Calipers (Stock)</option> -->
										<option value="3">Upgraded - Front - Calipers (Aftermarket)</option>
							<!--		<option value="4">Upgraded - Front - Rotors (Stock)</option> -->
										<option value="5">Upgraded - Front - Rotors (Aftermarket)</option>
							<!--		<option value="6">Upgraded - Rear - Drum (Stock)</option> -->
										<option value="7">Upgraded - Rear - Drum (Aftermarket)</option>
							<!--		<option value="8">Upgraded - Rear - Disc - Calipers (Stock)</option> -->
										<option value="9">Upgraded - Rear - Disc - Calipers (Aftermarket)</option>
							<!--		<option value="10">Upgraded - Rear - Disc - Rotors (Stock)</option> -->
										<option value="11">Upgraded - Rear - Disc - Rotors (Aftermarket)</option>
									</select>
									
									
									<select id="mod_exhaust_types" class="mod_subselect" name="mod_exhaust_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
							<!--		<option value="2">Header - Stock</option> -->
										<option value="3">Header - Shorty</option>
										<option value="4">Header - Long tube</option>
							<!--		<option value="5">Converter - Stock</option> -->
										<option value="6">Converter - High flow</option>
										<option value="7">Converter - Offroad pipe</option>
							<!--		<option value="8">Mid pipe - Stock</option> -->
										<option value="9">Mid pipe - H-pipe</option>
										<option value="10">Mid pipe - X-pipe</option>
							<!--		<option value="11">Muffler - Stock</option> -->
										<option value="12">Muffler - Aftermarket</option>
									</select>
								
									
									<select id="mod_engine_types" class="mod_subselect" name="mod_engine_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Internals</option>
										<option value="3">Induction - Naturally aspirated</option>
										<option value="4">Induction - Forced - Turbocharged</option>
										<option value="5">Induction - Forced - Supercharged (Positive displacement)</option>
										<option value="6">Induction - Forced - Supercharged (Centrifugal)</option>
										<option value="7">Induction - Nitrous - Dry system</option>
										<option value="8">Induction - Nitrous - Wet system</option>	
									</select>
								
									
									
									<select id="mod_suspension_types" class="mod_subselect" name="mod_suspension_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
							<!--		<option value="2">Upgraded - Front - Springs (Stock)</option> -->
										<option value="3">Upgraded - Front - Springs (Aftermarket)</option>
							<!--		<option value="4">Upgraded - Front - Shocks (Stock)</option> -->
										<option value="5">Upgraded - Front - Shocks (Aftermarket)</option>
							<!--		<option value="6">Upgraded - Front - Struts (Stock)</option> -->
										<option value="7">Upgraded - Front - Struts (Aftermarket)</option>
							<!--		<option value="8">Upgraded - Rear - Springs (Stock)</option> -->
										<option value="9">Upgraded - Rear - Springs (Aftermarket)</option>
							<!--		<option value="10">Upgraded - Rear - Shocks (Stock)</option> -->
										<option value="11">Upgraded - Rear - Shocks (Aftermarket)</option>
							<!--		<option value="12">Upgraded - Rear - Struts (Stock)</option> -->
										<option value="13">Upgraded - Rear - Struts (Aftermarket)</option>
									</select>
									<br/>
									
								<? }
								else
								{?>	
									<!-- DIESEL -->
									<select class="mod_type_select" engine="diesel" tid="0" name="mod_type_select" >
										<option value="0">Please Select a mod type...</option>
										<option value="air_intake">Air intake</option>
										<option value="audio">Audio</option>
										<option value="body">Body</option>
										<option value="brake">Brakes</option>
										<option value="chassis">Chassis</option>
										<option value="drive_axle">Drive axle</option>
										<option value="engine">Engine</option>
										<option value="exhaust">Exhaust System</option>
										<option value="fuellift_pump">Fuel lift pump</option>
										<option value="injection_pump">Injection pump</option>
										<option value="injector">Injectors</option>
										<option value="intercooler">Intercooler</option>
										<option value="lighting">Lighting</option>
										<option value="suspension">Suspension</option>
										<option value="transmission">Transmission</option>
										<option value="tuned">Tuned</option>
										<option value="turbo">Turbo</option>
										<option value="wheels">Wheels & Tires</option>
										<option value="other">Other</option>
									</select>
									
									<br/>
									<select id="mod_default_types" class="mod_subselect" name="mod_default_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Aftermarket</option>
									</select>
									
									
									
									<select id="mod_engine_types" class="mod_subselect" name="mod_engine_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Internals</option>	
									</select>
								
									
									<select id="mod_exhaust_types" class="mod_subselect" name="mod_exhaust_types" style="display:none;">
							<!--		<option value="1">Manifold (Stock)</option> -->
										<option value="2">Manifold (Aftermarket)</option>
							<!--		<option value="3">Exhaust system (Stock)</option> -->
										<option value="4">Exhaust system (Aftermarket)</option>
									</select>
								
									
									<select id="mod_transmission_types" class="mod_subselect" name="mod_transmission_types" style="display:none;">
							<!--		<option value="1">Manual - Clutch (Stock)</option> -->
										<option value="2">Manual - Clutch (Aftermarket)</option>
							<!--		<option value="3">Automatic - Converter (Stock)</option> -->
										<option value="4">Automatic - Converter (Aftermarket)</option>
				
									</select>
									
									
									<select id="mod_body_types" class="mod_subselect" name="mod_body_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Paint - Factory</option>
										<option value="3">Paint - Custom</option>
										<option value="4">Body mods</option>
									</select>
									
									
									<select id="mod_chassis_types" class="mod_subselect" name="mod_chassis_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Aftermarket</option>
										<option value="3">Bracing</option>
									</select>
									
									
									<select id="mod_drive_axle_types" class="mod_subselect" name="mod_drive_axle_types" style="display:none;">
							<!--		<option value="1">2WD (Stock)</option> -->
										<option value="2">2WD (Upgraded internals)</option>
							<!--		<option value="3">4WD (Stock)</option> -->
										<option value="4">4WD (Upgraded internals)</option>
									</select>
									
									
									
									<select id="mod_brake_types" class="mod_subselect" name="mod_chassis_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
							<!--		<option value="2">Upgraded - Front - Calipers (Stock)</option> -->
										<option value="3">Upgraded - Front - Calipers (Aftermarket)</option>
							<!--		<option value="4">Upgraded - Front - Rotors (Stock)</option> -->
										<option value="5">Upgraded - Front - Rotors (Aftermarket)</option>
							<!--		<option value="6">Upgraded - Rear - Drum (Stock)</option> -->
										<option value="7">Upgraded - Rear - Drum (Aftermarket)</option>
							<!--		<option value="8">Upgraded - Rear - Disc - Calipers (Stock)</option> -->
										<option value="9">Upgraded - Rear - Disc - Calipers (Aftermarket)</option>
							<!--		<option value="10">Upgraded - Rear - Disc - Rotors (Stock)</option> -->
										<option value="11">Upgraded - Rear - Disc - Rotors (Aftermarket)</option>
									</select>
									
									
									<select id="mod_suspension_types" class="mod_subselect" name="mod_suspension_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Lifted</option>
										<option value="3">Lowered</option>
									</select>
									<br/>
									
									
								<? }?>
								<br/>
								<input type="text" name="mod_default_input" id="mod_default_input" class="edit_input" placeholder="Enter text here..." style="display:none;"/>
								
							<?
								}
								else if($car->vehicle_type == 1) // MOTORCYCLE
								{
							?>
								
								
								<select class="mod_type_select" tid="1" engine="motor" name="mod_vehicle_select">
									<option value="0">Please Select a mod type...</option>
									<option value="engine">Engine</option>
									<option value="air_intake">Air intake</option>
									<option value="exhaust">Exhaust</option>
									<option value="transmission">Transmission</option>
									<option value="brake">Brakes</option>
									<option value="wheel">Wheels & Tires</option>
									<option value="lighting">Lighting</option>
									<option value="body">Body mods</option>
									<option value="suspension">Suspension</option>
									<option value="other">Other</option>
								</select>
								<br/>
								<select id="motor_mod_engine_types" class="mod_subselect" name="motor_mod_engine_types" style="display:none;">
							<!--	<option value="1">Stock</option> -->
									<option value="2">Internals</option>
									<option value="3">Forced induction</option>
								</select>
								
								
								<select id="mod_default_types" class="mod_subselect" name="mod_default_types" style="display:none;">
							<!--	<option value="1">Stock</option> -->
									<option value="2">Aftermarket</option>
								</select>
								<br/>
								<br/>
								<input type="text" name="mod_default_input" id="mod_default_input" class="edit_input" placeholder="Enter text here..." style="display:none;"/>
								
							<? }
								else // TRUCK
								{ ?>
									<input type="hidden" id="engine_type_select" tid="0" name="mod_vehicle_select" value="<?= $engine_type?>">
									
									<? if($engine_type == "gas"){?>	
										<!-- GAS -->
										<select class="mod_type_select" engine="gas" tid="2" name="mod_type_select" >
											<option value="0">Please Select a mod type...</option>
											<option value="audio">Audio</option>
											<option value="body">Body</option>
											<option value="brake">Brakes</option>
											<option value="chassis">Chassis</option>
											<option value="cooling">Cooling system</option>
											<option value="drive_axle">Drive axle</option>
											<option value="engine">Engine</option>
											<option value="exhaust">Exhaust system</option>
											<option value="fuel">Fuel system</option>
											<option value="ignition">Ignition system</option>
											<option value="lighting">Lighting</option>
											<option value="suspension">Suspension</option>
											<option value="transmission">Transmission</option>
											<option value="tuned">Tuned</option>
											<option value="wheels">Wheels & Tires</option>
											<option value="other">Other</option>
										</select>
										
										<select id="mod_default_types" class="mod_subselect" name="mod_default_types" style="display:none;">
								<!--		<option value="1">Stock</option> -->
											<option value="2">Aftermarket</option>
										</select>
										<br/>
								
										
										<select id="mod_transmission_types" class="mod_subselect" name="mod_transmission_types" style="display:none;">
								<!--		<option value="1">Manual - Clutch (Stock)</option> -->
											<option value="2">Manual - Clutch (Aftermarket)</option>
								<!--		<option value="3">Automatic - Converter (Stock)</option> -->
											<option value="4">Automatic - Converter (Aftermarket)</option>
					
										</select>
									
										
										<select id="mod_body_types" class="mod_subselect" name="mod_body_types" style="display:none;">
								<!--		<option value="1">Stock</option> -->
											<option value="2">Paint - Factory</option>
											<option value="3">Paint - Custom</option>
											<option value="4">Body mods</option>
										</select>
									
										
										<select id="mod_chassis_types" class="mod_subselect" name="mod_chassis_types" style="display:none;">
								<!--		<option value="1">Stock</option> -->
											<option value="2">Aftermarket</option>
											<option value="3">Bracing</option>
										</select>
									
										
										<select id="mod_drive_axle_types" class="mod_subselect" name="mod_drive_axle_types" style="display:none;">
								<!--		<option value="1">2WD (Stock)</option> -->
											<option value="2">2WD (Upgraded internals)</option>
								<!--		<option value="3">4WD (Stock)</option> -->
											<option value="4">4WD (Upgraded internals)</option>
								<!--		<option value="5">AWD (Stock)</option> -->
											<option value="6">AWD (Upgraded internals)</option>
										</select>
									
										
										<select id="mod_brake_types" class="mod_subselect" name="mod_brake_types" style="display:none;">
								<!--		<option value="1">Stock</option> -->
								<!--		<option value="2">Upgraded - Front - Calipers (Stock)</option> -->
											<option value="3">Upgraded - Front - Calipers (Aftermarket)</option>
								<!--		<option value="4">Upgraded - Front - Rotors (Stock)</option> -->
											<option value="5">Upgraded - Front - Rotors (Aftermarket)</option>
								<!--		<option value="6">Upgraded - Rear - Drum (Stock)</option> -->
											<option value="7">Upgraded - Rear - Drum (Aftermarket)</option>
								<!--		<option value="8">Upgraded - Rear - Disc - Calipers (Stock)</option> -->
											<option value="9">Upgraded - Rear - Disc - Calipers (Aftermarket)</option>
								<!--		<option value="10">Upgraded - Rear - Disc - Rotors (Stock)</option> -->
											<option value="11">Upgraded - Rear - Disc - Rotors (Aftermarket)</option>
										</select>
									
										
										<select id="mod_exhaust_types" class="mod_subselect" name="mod_exhaust_types" style="display:none;">
								<!--		<option value="1">Stock</option>
											<option value="2">Header - Stock</option> -->
											<option value="3">Header - Shorty</option>
											<option value="4">Header - Long tube</option>
								<!--		<option value="5">Converter - Stock</option> -->
											<option value="6">Converter - High flow</option>
											<option value="7">Converter - Offroad pipe</option>
								<!--		<option value="8">Mid pipe - Stock</option> -->
											<option value="9">Mid pipe - H-pipe</option>
											<option value="10">Mid pipe - X-pipe</option>
								<!--		<option value="11">Muffler - Stock</option> -->
											<option value="12">Muffler - Aftermarket</option>
										</select>
									
										
										<select id="mod_engine_types" class="mod_subselect" name="mod_engine_types" style="display:none;">
								<!--		<option value="1">Stock</option> -->
											<option value="2">Internals</option>
											<option value="3">Induction - Naturally aspirated</option>
											<option value="4">Induction - Forced - Turbocharged</option>
											<option value="5">Induction - Forced - Supercharged (Positive displacement)</option>
											<option value="6">Induction - Forced - Supercharged (Centrifugal)</option>
											<option value="7">Induction - Nitrous - Dry system</option>
											<option value="8">Induction - Nitrous - Wet system</option>
											
										</select>
									
										
										<select id="mod_suspension_types" class="mod_subselect" name="mod_suspension_types" style="display:none;">
											<option value="1">Stock</option> -->
								<!--		<option value="2">Upgraded - Front - Springs (Stock)</option> -->
											<option value="3">Upgraded - Front - Springs (Aftermarket)</option>
								<!--		<option value="4">Upgraded - Front - Shocks (Stock)</option> -->
											<option value="5">Upgraded - Front - Shocks (Aftermarket)</option>
								<!--		<option value="6">Upgraded - Front - Struts (Stock)</option> -->
											<option value="7">Upgraded - Front - Struts (Aftermarket)</option>
								<!--		<option value="8">Upgraded - Rear - Springs (Stock)</option> -->
											<option value="9">Upgraded - Rear - Springs (Aftermarket)</option>
								<!--		<option value="10">Upgraded - Rear - Shocks (Stock)</option> -->
											<option value="11">Upgraded - Rear - Shocks (Aftermarket)</option>
								<!--		<option value="12">Upgraded - Rear - Struts (Stock)</option> -->
											<option value="13">Upgraded - Rear - Struts (Aftermarket)</option>
										</select>
										<br/>
											
										
									<? }
									else
									{?>	
										<!-- DIESEL -->
										<select class="mod_type_select" engine="diesel" tid="2" name="mod_type_select" >
											<option value="0">Please Select a mod type...</option>
											<option value="air_intake">Air intake</option>
											<option value="audio">Audio</option>
											<option value="body">Body</option>
											<option value="brake">Brakes</option>
											<option value="chassis">Chassis</option>
											<option value="drive_axle">Drive axle</option>
											<option value="engine">Engine</option>
											<option value="exhaust">Exhaust</option>
											<option value="fuellift_pump">Fuel lift pump</option>
											<option value="injection_pump">Injection pump</option>
											<option value="injector">Injectors</option>
											<option value="intercooler">Intercooler</option>
											<option value="lighting">Lighting</option>
											<option value="suspension">Suspension</option>
											<option value="transmission">Transmission</option>
											<option value="tuned">Tuned</option>
											<option value="turbo">Turbo</option>
											<option value="wheels">Wheels & Tires</option>
											<option value="other">Other</option>
										</select>
									<? }?>
									<br/>
									<select id="mod_default_types" class="mod_subselect" name="mod_default_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Aftermarket</option>
									</select>
								
									
									
									<select id="mod_engine_types" class="mod_subselect" name="mod_engine_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Internals</option>	
									</select>
							
									
									<select id="mod_exhaust_types" class="mod_subselect" name="mod_exhaust_types" style="display:none;">
							<!--		<option value="1">Manifold (Stock)</option> -->
										<option value="2">Manifold (Aftermarket)</option>
							<!--		<option value="3">Exhaust system (Stock)</option> -->
										<option value="4">Exhaust system (Aftermarket)</option>
									</select>
								
									
									<select id="mod_transmission_types" class="mod_subselect" name="mod_transmission_types" style="display:none;">
							<!--		<option value="1">Manual - Clutch (Stock)</option> -->
										<option value="2">Manual - Clutch (Aftermarket)</option>
							<!--		<option value="3">Automatic - Converter (Stock)</option> -->
										<option value="4">Automatic - Converter (Aftermarket)</option>
				
									</select>
							
									
									<select id="mod_body_types" class="mod_subselect" name="mod_body_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Paint - Factory</option>
										<option value="3">Paint - Custom</option>
										<option value="4">Body mods</option>
									</select>
									
									
									<select id="mod_chassis_types" class="mod_subselect" name="mod_chassis_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Aftermarket</option>
										<option value="3">Bracing</option>
									</select>
								
									<select id="mod_drive_axle_types" class="mod_subselect" name="mod_drive_axle_types" style="display:none;">
							<!--		<option value="1">2WD (Stock)</option> -->
										<option value="2">2WD (Upgraded internals)</option>
							<!--		<option value="3">4WD (Stock)</option> -->
										<option value="4">4WD (Upgraded internals)</option>
									</select>
								
									
									
									<select id="mod_brake_types" class="mod_subselect" name="mod_chassis_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
							<!--		<option value="2">Upgraded - Front - Calipers (Stock)</option> -->
										<option value="3">Upgraded - Front - Calipers (Aftermarket)</option>
							<!--		<option value="4">Upgraded - Front - Rotors (Stock)</option> -->
										<option value="5">Upgraded - Front - Rotors (Aftermarket)</option>
							<!--		<option value="6">Upgraded - Rear - Drum (Stock)</option> -->
										<option value="7">Upgraded - Rear - Drum (Aftermarket)</option>
							<!--		<option value="8">Upgraded - Rear - Disc - Calipers (Stock)</option> -->
										<option value="9">Upgraded - Rear - Disc - Calipers (Aftermarket)</option>
							<!--		<option value="10">Upgraded - Rear - Disc - Rotors (Stock)</option> -->
										<option value="11">Upgraded - Rear - Disc - Rotors (Aftermarket)</option>
									</select>
							
									
									<select id="mod_suspension_types" class="mod_subselect" name="mod_suspension_types" style="display:none;">
							<!--		<option value="1">Stock</option> -->
										<option value="2">Lifted</option>
										<option value="3">Lowered</option>
									</select>
									<br/>
								<br/>
								<input type="text" name="mod_default_input" id="mod_default_input" class="edit_input" placeholder="Enter text here..." style="display:none;"/>
						
							<?	}
							?>
					
						</div>
					</div>	
					<div style="overflow:hidden">
						<div id="add_mod_piece" engine="<?= $engine_type?>" cid="<?= $car->id?>" class="button" style="width:120px;float:left;margin-top:0px;margin-left:160px;">ADD</div>				
						<div id="cancel_mod_piece" class="button" style="width:120px;margin-top:0px;margin-right: 160px;float:right;">CANCEL</div>
					</div>
					<div id="mod_error" class="regular" style="margin-top:30px;text-align:center;"></div>
				</div>
				
				</div>
	<? }?>
	
	
	
	
	

	<div id="message_overlay">
		<div class="overlayBG"></div>
		<div style="position:relative;z-index:100">
			<img class="close_overlay" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />		
			<div class="titboldit" style="font-size:27px;margin-top:30px;">SEND MESSAGE</div>
				<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">
				<div style="width:400px;margin:30px auto;">
					<textarea id="send_message_input" style="width:100%;height:70px;resize:none;"></textarea>	
					<div class="button" id="send_message_to_owner" cid="<?= $car->id?>">SEND</div>
				</div>
			</div>
			<div class="regular" id="group_event_error_message"></div>
		</div>
	</div>

	
	<div id="BkgEr" style=""></div>
	
		<!--	<div id="show_comments" cid="<?= $car->id;?>" class="button" style="">COMMENTS</div>
			<div id="show_story" cid="<?= $car->id;?>" class="button" style="">STORY</div> -->
			<?  if($is_my_car){?>
				<a style="text-decoration:none;" href="<?= site_url('edit_vehicle/'.$car->id);?>">
					<div class="button" style="position:absolute;right: 24px;top: 335px; width:180px;">EDIT</div>
				</a>
			<? }
				else{
			?>
					<div id="follow_vehicle" cid="<?= $car->id;?>" class="button" style="<? if($following){ echo 'display:none';}?>">FOLLOW</div>
					<div id="unfollow_vehicle" cid="<?= $car->id;?>" class="button" style="<? if($following){ echo 'display:block';}?>">UNFOLLOW</div>
					
					
					
					<div id="message_owner" cid="<?= $car->id;?>" class="button" style="position:absolute;right:23px;top:390px;width:180px;">MESSAGE THE OWNER</div>
			<? }?>	
				<img class="small_chrome" src="<?= site_url('items/frontend/img/small_chrome.png')?>" />
				<div class="button" id="report_btn" style="margin:0px;position:absolute;right:35px;top:3px;width:100px;z-index:100;" rtype="vehicle_profile" eid="<?= $car->pretty_url;?>">REPORT</div>
				<? if($badge):?>
					<img id="mod_badge" src="<?= site_url('items/frontend/img/'. $badge )?>" border="0" />
				<? endif;?>
				<img class="car_cover_photo" src="<?= site_url('items/uploads/coverpictures/'. $cover )?>" border="0">
				<img  class="car_profile_photo" src="<?= site_url('items/uploads/profilepictures/'. $profile )?>" border="0">
				<div style="height:132px;width:400px;margin-left:200px;">
					<div class="car_detail_nickname playbold"><?= $car->nickname;?></div>
					<div class="car_detail_make regular"><?= $model->year." <span style='text-transform:capitalize'>".$model->make."</span> ".$model->model." "; ?><? if($model->model_trim != NULL && $model->model_trim!= '' && $model->model_trim!= 0 && $model->model_trim!= '0') echo $model->model_trim;?></div>
				</div>
				
				
				<div style="position:relative;">
					<img class="" style="position:relative;left:0px;top:0px;" src="<?= site_url('items/frontend/img/car_detail_chrome.png')?>" border="0">
					<div style="position:absolute;color:#000000;z-index:1;top:20px;left:75px;" class="car_detail_owner dosisextralight">
						<span class="dosissemi">OWNER:</span><a style="text-decoration:none;" href="<?= site_url('profile/'.$owner->username);?>"> <?= $owner->first_name." ".$owner->last_name;?></a> </div>
					
					<? if($car->for_sale == 1):?>	
						<div style="position:absolute;color:#000000;z-index:1;top:20px;left:320px;" class="car_detail_owner dosisextralight">
							<span class="dosissemi">FOR SALE </span>		
						</div>
					<? endif;?>
					
					<? if($car->for_sale == 1):?>	
						<div style="position:absolute;color:#000000;z-index:1;top:20px;left:570px;" class="car_detail_owner dosisextralight">
							<span class="dosissemi">PRICE: </span>
							<span class="dosisextralight"><?= $car->price;?> $</span>		
						</div>
					<? endif;?>
					
					<? if($car->plate != "" && $car->plate != NULL):?>	
						<div style="position:absolute;color:#000000;z-index:1;top:60px;left:75px;" class="car_detail_owner dosisextralight">
							<span class="dosissemi">PLATE: </span>
							<span class="dosisextralight"><?= $car->plate;?></span>
						</div>
					<? endif;?>
					
					<? if($car->label != "" && $car->label != NULL):?>
						<div style="position:absolute;color:#000000;z-index:1;top:60px;left:320px;" class="car_detail_owner dosisextralight">
							<span class="dosissemi">LABEL: </span>
							<span class="dosisextralight"><?= $car->label;?></span>
						</div>
					<? endif;?>
					
					
						<div style="position:absolute;color:#000000;z-index:1;top:60px;left:570px;" class="car_detail_owner dosisextralight">
							<span class="dosissemi">FOLLOWERS: </span>
							<span class="dosisextralight"><?= $follower_count;?></span>
						</div>
					
					
				</div>
				
				
				
						
				
				<div style="overflow:hidden;margin-top:30px;">
					<div class="detail_left">
						<? if($car->zerosixty != "" && $car->zerosixty != NULL):?>
							<div class="user_section">
								<div class="dosissemi" style="font-size:18px;">0-60MPH</div>
								<div class="dosisextralight"><?= $car->zerosixty;?> seconds</div>
							</div>
							<br/>
						<? endif;?>
						
						<? if($car->hp_ranges != "" && $car->hp_ranges != NULL && $car->hp_ranges != 0 ):?>
							<div class="user_section" style="width:400px;">
								<div class="dosissemi" style="font-size:18px;">HP Ranges</div>
								<div class="dosisextralight">
								<?
									switch($car->hp_ranges)
									{
										case 50: { echo "50-150 HP";};break;
										case 151: { echo "151-250 HP";};break;
										case 251: { echo "251-400 HP";};break;
										case 401: { echo "401-700 HP";};break;
										case 701: { echo "701-1000 HP";};break;
										case 1001: { echo ">1000 HP";};break;
										
									}
								?></div>
							</div>
							<br/>
						<? endif;?>
						
						<? if($car->actual_hp != "" && $car->actual_hp != NULL):?>
							<div class="user_section" style="width:400px;">
								<div class="dosissemi" style="font-size:18px;">Actual HP</div>
								<div class="dosisextralight"><?= $car->actual_hp;?></div>
							</div>
							<br/>
						<? endif;?>
						
						<? if($car->quarter_mile != "" && $car->quarter_mile != NULL):?>
							<div class="user_section" style="width:400px;">
								<div class="dosissemi" style="font-size:18px;">Quarter Mile</div>
								<div class="dosisextralight"><?= $car->quarter_mile;?></div>
							</div>
							<br/>
						<? endif;?>
						
						<? if($car->torque != "" && $car->torque != NULL):?>
							<div class="user_section" style="width:400px;">
								<div class="dosissemi" style="font-size:18px;">Torque</div>
								<div class="dosisextralight"><?= $car->torque;?></div>
							</div>
							<br/>
						<? endif;?>
						
						<? if($car->description != "" && $car->description != NULL):?>
							<div class="user_section" style="width:400px;">
								<div class="dosissemi" style="font-size:18px;">Description</div>
								<div class="dosisextralight"><?= $car->description;?></div>
							</div>
							<br/>
						<? endif;?>
						
						
						
					</div>
					
	
					
					<div class="detail_right" style="margin-bottom:50px;margin-right:0px;margin-left:22px;">
						
						<div class="dosissemi" style="font-size:18px;text-align:left;margin-bottom:40px;">MODS</div>
							<div style="position:relative;overflow:hidden;">
								<? $x = 1; $prev = ''; $counter = count($mods);foreach($mods as $mod):
									
									if($mod['type'] != "Stock" && $mod['text'] !== NULL)
									{
										
										$text = $mod['type']." ".$mod['text'];
										
									/*	if($mod['text'] == " - " ||  $mod['text'] == ""):
											$text = "Stock";
										endif; */
								?>
										
										<? if($mod['title'] != $prev){
											
											if($prev != "")
											{
												echo '</div>';
											}
										?>
											<div class="user_section" style="width:400px;">
												<img style="width:40px;float:left;margin-right:10px;" src="<?= site_url('items/frontend/img/mods')."/".$mod['category'].".png";?>"/>												
												<div class="dosissemi" style="font-size:18px;text-transform:capitalize;line-height:40px;"><?= $mod['title'];?></div>
												
												<div class="dosisextralight" style="overflow:hidden">
													<?= $text;?>
													<? if($is_my_car):?>
														<div class="delete_mod" mid="<?= $mod['id'];?>">  X</div>
													<? endif;?>   
												</div> 										
												
										<? }
											else
											{?>
												<div class="dosisextralight" style="overflow:hidden">
													<?= $text;?>
													<? if($is_my_car):?>
														<div class="delete_mod" mid="<?= $mod['id'];?>">  X</div>
													<? endif;?>  
												</div>
											<?}
										?>
								<? 
									$prev = $mod['title'];
									
									if($x == $counter)
									{
										echo '</div>';
									}
									
									$x++;
									}
								endforeach;?>
							</div>
						<?  if($is_my_car){?>
							<div id="add_modification"  class="button" style="margin-top: 0px;position: absolute;width: 120px;">ADD MOD</div>
						<? }?>
					</div>
					
					<div class="detail_left" style="width:100%;overflow:hidden">
						<a style="text-decoration:none;" href="<?= site_url('albums/'.$car->id);?>"><div class="dosissemi" style="font-size:18px;text-align:left;float:left;">ALBUMS</div></a>
						
						<?  if($is_my_car):?>
						<br/><br/>
							<div style="float:left;">
								<div class="regular" style="float:left;">New Album:</div>
								<input type="text"  style="float:left;top:-4px;left:20px;" class="edit_input" name="new_album_title" id="new_album_title" placeholder="Title..." value="" />
								<div id="add_album" cid="<?= $car->id;?>" class="button" style="margin-left:40px;margin-top:-2px;float:left;">ADD ALBUM</div>
							</div>
						<? endif;?>
						<div id="gallery_images" style="margin-left:0px;width:100%">
							<? foreach($albums as $album):?>
								<a href="<?= site_url('album/'.$album['id']);?>">
									<div class="album_teaser_item">
										<div class="album_item_image" style="background-image: url('<?= site_url('items/uploads/'.$album['folder'].'/'.$album['fname']);?>')"></div>
										<div class="album_item_title"><?= $album['title'];?></div>
									</div>
									
								</a>
							<? endforeach;?>
						</div>
					</div>
					
					
					
				</div>
				<br/>
				
				<img class="" style="position:relative;left:0px;top:0px;" src="<?= site_url('items/frontend/img/small_chrome.png')?>" border="0">
				<div id="story_holder" style="position:relative;top:0px;">
					<?  if($is_my_car){?>
						<div id="add_story_overlay">
							<div class="overlayBG"></div>
							<div style="position:relative;z-index:100">
								<img class="close_overlay" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />		
								<div class="titboldit" style="font-size:27px;margin-top:30px;">BLOG UPDATE</div>
									<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">
										<textarea placeholder="Type the story here..." id="story_piece_text"></textarea>
										<div class="button" id="upload_blog_button" style="margin-top:0px;">ADD IMAGES</div>
										<input type="file" name="upload_blog" id="upload_blog" multiple style="display:none;">
										<div id="story_thumb_holder">
											<!--<div class="story_thumb_img" style="background-image: url('<?= site_url('items/uploads/images/0f907df4c0a262d9d67224a4475550a3.jpg')?>');"></div>
											-->
										</div>
									</div>
									<div style="overflow:hidden;">
										<div id="add_story_piece" cid="<?= $car->id?>" class="button" style="float:left;margin-top:0px;margin-left:120px;width:120px;">SAVE</div>				
										<div id="cancel_story_piece" class=" button" style="float:right;margin-top: 0px;margin-right: 120px;width:120px;">CANCEL</div>
									</div>
								</div>
						</div>
						<div id="add_story"  class="button" style="width:130px;position:absolute;right:50px;top:3px;margin:0px;">ADD AN UPDATE</div>
					<? }?>
					
					<div class="playbold gold page_title" style="font-size:30px;position:absolute;top:-42px;width:100%;">PROJECT BLOG</div>
					
					<br/>
					<input type="file" preview="" accept="image/*" id="story_image_file" name="story_image_file" style="display:none;" multiple>
					<input type="file" preview="" accept="image/*" id="story_image_file_up" name="story_image_file_up" style="display:none;" multiple>
					<? $x = 1; foreach($stories as $story):?>
						<div class="story_item">
							<div class="dosisextralight"><?= $story['created_date'];?> 
								<?  if($is_my_car){?>
									<div class="story_upload_btn"  sid="<?= $story['id'];?>">UPLOAD IMAGE</div>
								<? }?>
							</div>
							<br/>
							<div class="regular"><?= nl2br($story['story']);?></div>
							<div class="story_gallery">
								<?   foreach($story['gallery'] as $pic):?>
									<a href="<?= site_url('items/uploads/images/'.$pic['fname']);?>" data-lightbox2="story_gallery_<?= $x;?>">
										<img class="story_gallery_img" src="<?= site_url('items/uploads/images/'.$pic['fname'])?>" />
									</a>
								<? endforeach;?>
							</div>
							
						</div>
					<? $x++; endforeach;?>
				</div>
</div>
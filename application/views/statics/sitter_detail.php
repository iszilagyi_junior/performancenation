<script src="<?= site_url("items/frontend/js/booking_user.js");?>" type="text/javascript"></script>
<script>
	
	var zone_geo_location = "<?= $sitter->geo_location?>";
	$('#sidemenu').css({top: 35});
</script>
<style>
	.gm-style-cc { display:none; }
</style>
<script src="<?= site_url("items/frontend/js/detail_map.js"); ?>" type="text/javascript"></script>
<script>	
		
</script>
			<div id="content">
				<div id="vet_detail_header">
					<div id="zone_map_canvas">
						
					</div>
					<div class="statics_subheader">
						<?= $sitter->first_name." ".$sitter->last_name ?>
					</div>

				</div>
				<div id="vets_details">
				<? if($this->ion_auth->logged_in()){?>
					<div id="statics_left" class="statics_half">
						<div id="vet_details_desc">
							<p class="vets_details_subheader"><?= $this->lang->line('sitter_detail_desc')?></p>
							
							<?= $sitter->sitter_availability ?>
						</div>
					</div>
					<? } ?>
					
					<div id="statics_right" class="statics_half">
						
						<div class="featured_vets_list_item" style='margin-right:0px' >
							<? if($sitter->profile_picture){?>
								<img class="vets_list_item_image" src="<?= site_url('items/uploads/profilepictures/'.$sitter->profile_picture)?>" border="0">	
							<?} else{?>
								<img class="vets_list_item_image" src="<?= site_url('items/uploads/profilepictures/default-profile.jpg')?>" border="0">
							<?}?>							
						</div>
									
					</div>	
					<div id="statics_right" class="statics_half">
					<? if($this->ion_auth->logged_in()){?>
						<div id="vet_details_times">
							<p class="vets_details_subheader"><?= $this->lang->line('sitter_detail_contact')?></p>
							
							<?= $sitter->city ?><br/>
							<?= $sitter->email ?><br/>
							<?= $sitter->phone ?><br/>
						</div>
						<? }?>
					</div>
					
					<div id="statics_right" class="statics_half">
						<div id="sitter_details_rating">
							<p class="vets_details_subheader">
								<?= $this->lang->line('sitter_detail_rating')?> 						
							</p>
							<div class="sansItalic"><?= $ratingSum;?> <?= $this->lang->line('points_from')?> <?= $number_of_votes;?> <?= $this->lang->line('users')?></div> 
							
							<? if($this->ion_auth->logged_in()){?>
							<div id="rating_holder">
								
								<? if(!$already_rated){?>
								<p class="vets_details_subheader"><?= $this->lang->line('sitter_detail_rate')?></p>
									<div class="rate" who="sitter" rating="1" sitter="<?= $sitter->id;?>">
										<img class="circle_empty" id="empty_circle_1" src="<?= site_url('items/frontend/img/circle_empty.png')?>" border="0">
										<img class="circle_full" id="full_circle_1" src="<?= site_url('items/frontend/img/circle_full.png')?>" border="0">
									</div>
									<div class="rate" who="sitter" rating="2" sitter="<?= $sitter->id;?>">
										<img class="circle_empty" id="empty_circle_2" src="<?= site_url('items/frontend/img/circle_empty.png')?>" border="0">
										<img class="circle_full" id="full_circle_2" src="<?= site_url('items/frontend/img/circle_full.png')?>" border="0">
									</div>
									<div class="rate" who="sitter" rating="3" sitter="<?= $sitter->id;?>">
										<img class="circle_empty" id="empty_circle_3" src="<?= site_url('items/frontend/img/circle_empty.png')?>" border="0">
										<img class="circle_full" id="full_circle_3" src="<?= site_url('items/frontend/img/circle_full.png')?>" border="0">
									</div>
									<div class="rate" who="sitter" rating="4" sitter="<?= $sitter->id;?>">
										<img class="circle_empty" id="empty_circle_4" src="<?= site_url('items/frontend/img/circle_empty.png')?>" border="0">
										<img class="circle_full" id="full_circle_4" src="<?= site_url('items/frontend/img/circle_full.png')?>" border="0">
									</div>
									<div class="rate" who="sitter" rating="5" sitter="<?= $sitter->id;?>">
										<img class="circle_empty" id="empty_circle_5" src="<?= site_url('items/frontend/img/circle_empty.png')?>" border="0">
										<img class="circle_full" id="full_circle_5" src="<?= site_url('items/frontend/img/circle_full.png')?>" border="0">
									</div>
								<? }
								else{?>
									<br/>
									<div><?= $this->lang->line('sitter_detail_already_rated')?> </div>
									<br/>
									<?
										for($i=1;$i <= 5; $i++)
										{
											
											if(intval($i) <= intval($ratingSum))
											{ 
											?>
												<img style="display:inline;" class="circle_full" src="<?= site_url('items/frontend/img/circle_full.png')?>" border="0">
										<?	}
											else
											{ ?>
												<img class="circle_empty"  src="<?= site_url('items/frontend/img/circle_empty.png')?>" border="0">
										<?	}
										}
										
									?>
								<? }?>
							</div>
							<? }?>
						</div>
					</div>	
					
					<br/>
					<? if(!$this->ion_auth->logged_in()){?>
						<div class="reservation_description"><?= $this->lang->line('log_in_further_info')?></div>
					<? }?>
										
				</div>
				
				<div id="statics_left" class="statics_half">
					<? if($this->ion_auth->logged_in()){?>
					<p class="vets_details_subheader"><?= $this->lang->line('book_this_sitter')?></p>
					
						<div class="reservation_description"><?= $this->lang->line('sitter_available_dates')?></div>
						<div id="calendar_holder" style="left:100px;" sid="<?= $sitter->id;?>">
								<?= $calendar?>
								<div id="calendar_legend">
									<img class="legend_img"  src="<?= site_url('items/frontend/img/red_full.png')?>" border="0"><span class="legend_text" style="color:#D40042;"><?= $this->lang->line('available')?></span>
									<!-- <img class="legend_img"  src="<?= site_url('items/frontend/img/red_booked.png')?>" border="0"><span class="legend_text" style="color:#df8ba3;">Exception</span>-->
									<img class="legend_img"  src="<?= site_url('items/frontend/img/grey.png')?>" border="0"><span class="legend_text" style="color:#b4b4b4;"><?= $this->lang->line('unavailable')?></span>
								</div>
						</div>
						<br />
						
					<? }?>
				</div>
				
				
			</div>

<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; 
?>

<link href="<?=site_url('items/backend/css/jquery.minicolors.css');?>" rel="stylesheet">
<script src="<?=site_url('items/backend/js/jquery.minicolors.js');?>"></script>
<script>
$(document).ready(function() {
	$('input[name=bgcolor]').minicolors({
		defaultValue: $(this).attr('data-default-value') || '#f0f0f0',
		opacity: $(this).hasClass('opacity'),
		theme: $(this).attr('data-theme') || 'default',
		
	});
});
</script>
<?php echo $output; ?>

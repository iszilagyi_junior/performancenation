$(document).ready(function() {
	resize();
	animateIn();
});

$(window).resize(function(){
	resize();
});

function resize(){
	var logoLeft=50;
	var logoTop=50;
	var logoWidth=100;
	var logoHeight=100;
	var screenWidth=$(window).width();
	var screenHeight=$(window).height();
	var sliderHeight=screenHeight/1.5;
	var sliderTop=(screenHeight-sliderHeight)/2;
	$('#wrapper,#slider').width(screenWidth);
	$('#slider,#sliderLeft,#sliderRight').height(sliderHeight);
	$('#sliderLeft').width(screenWidth/7*3);
	$('#sliderRight').width(screenWidth/7*4);
	$('#slider').css('top',sliderTop+'px');
	$('#wrapper').height(screenHeight);

	var sliderLeftWidth=$('#sliderLeft').width();
	var sliderLeftHeight=$('#sliderLeft').height();

	
	$('#sliderL').width(logoLeft);
	$('#sliderL').height(sliderLeftHeight);
	
	$('#sliderT').width(sliderLeftWidth);
	$('#sliderT').height(logoTop);
	
	$('#sliderB').css({top:logoTop+logoHeight,left:logoLeft});
	$('#sliderB').width(sliderLeftWidth-logoLeft);
	$('#sliderB').height(sliderLeftHeight-logoTop-logoHeight);
	
	
	$('#sliderR').css({top:logoTop,left:logoLeft+logoWidth});
	$('#sliderR').width(sliderLeftWidth-logoLeft-logoWidth);
	$('#sliderR').height(sliderLeftHeight-logoTop);
	
	
		
}



function animateIn() {
	//$('#slider').css('top');
	acutalTop = $('#slider').css('top');
	$('#slider').css('top', $(window).height()+1000);
	
	$('#slider').fadeIn('fast');
	$('#slider').animate({
		top: acutalTop
	}, '60000', 'easeOutQuint');
}
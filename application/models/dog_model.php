<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Car_Model extends CI_Model
{
	function getDogs($user_id)
	{
		$sql = "SELECT d.* FROM users_dogs ud, dogs d WHERE ud.user_id = ? AND ud.dog_id = d.id;";
		$resultset = $this->db->query($sql, array($user_id));
		return $resultset;
	}
	
	function getRandomDogs()
	{
		$sql = "SELECT * FROM dogs ORDER BY RAND() LIMIT 30";
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	
	function createDog($data)
	{
		$this->db->trans_start();	
			
		$sql = "INSERT INTO dogs (name, nickname, pretty_url, breed, created_by, modified_by) VALUES (?, ?, ?, ?, ?, ?);";
		$resultset = $this->db->query($sql, array($data['name'], $data['nickname'], $data['pretty_url'], $data['breed'], $data['created_by'], $data['created_by']));
		$insert_id = $this->db->insert_id();
		$sql = "INSERT INTO users_dogs (dog_id, user_id, modified_by, created_by) VALUES (?, ?, ?, ?);";
		$resultset = $this->db->query($sql, array($this->db->insert_id(), $data['created_by'], $data['created_by'], $data['created_by']));
		
		$this->db->trans_complete();
		
		return $insert_id;
	}
	
	function createCar($data)
	{
		$this->db->insert('cars', $data);
		return  $this->db->insert_id();
	}
	
	
	function createPrettyUrl($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('dogs', $data);
		
	}
	
	function getPrettyUrl($pretty_url)
	{
		$this->db->where('pretty_url', $pretty_url);
		$return = $this->db->get('dogs');
		return $return;
	}
	
	function getDog($dog_id)
	{
		$sql = "SELECT d.id, d.name, d.nickname, d.pretty_url, DATE_FORMAT(d.birthday, '%d.%m.%Y') as birthday, d.profile_picture, d.cover_picture, d.gender, d.breed, d.color, d.coat, d.type, d.size, d.characteristics, d.about, d.chip_nr, d.doctor, d.vaccines, d.modified_date, d.modified_by, d.created_date, d.created_by, b.name as breed_name, d.lost FROM dogs d, breeds b WHERE d.id = ? AND b.id = d.breed";
		$resultset = $this->db->query($sql, array($dog_id));
		return $resultset->row();
	}
	
	function getDogByName($name, $pic_id)
	{
		$sql = "SELECT * FROM dogs  WHERE name LIKE ? AND id NOT IN(SELECT dog_id FROM tagged_dogs WHERE image_id = ?)LIMIT 10";
		$resultset = $this->db->query($sql, array('%'.$name.'%', $pic_id));
		return $resultset;
	}
	
	
	function getDogsTagged($pic_id)
	{
		$sql = "SELECT * FROM dogs d, tagged_dogs td  WHERE  td.image_id = ? AND d.id = td.dog_id";
		$resultset = $this->db->query($sql, array( $pic_id));
		return $resultset;
	}
	
	
	function editDog($data)
	{	
		$this->db->trans_start();
		$this->db->where('id', $data['id']);
		$this->db->update('dogs', $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
		    return "FAIL"; // generate an error... or use the log_message() function to log your error
		}
		else{
			return "OK";
		}
						
	}
	
	function registerDogMobile($data)
	{
		$this->db->trans_start();	
			
		$sql = "INSERT INTO dogs (name, nickname, breed, created_by, modified_by) VALUES (?, ?, ?, ?, ?);";
		$resultset = $this->db->query($sql, array($data['name'], $data['nickname'], $data['breed'], $data['created_by'], $data['created_by']));
		$insert_id = $this->db->insert_id();
		$sql = "INSERT INTO users_dogs (dog_id, user_id, modified_by, created_by) VALUES (?, ?, ?, ?);";
		$resultset = $this->db->query($sql, array($this->db->insert_id(), $data['created_by'], $data['created_by'], $data['created_by']));
		
		$this->db->trans_complete();
		
		return $insert_id;
	}
	
	
	function getFriends($dog_id, $limit = NULL)
	{
		$sql = "SELECT f.id, d.id, d.name, d.pretty_url, d.profile_picture, d.breed, d.birthday FROM friends f, dogs d WHERE f.dog_id = ? AND d.id = f.friend_dog_id ";
		if($limit != NULL)
			$sql .= " LIMIT " . $limit;
		$resultset = $this->db->query($sql, array($dog_id));
		return $resultset; 
	}
	
	function getEnemies($dog_id, $limit = NULL)
	{
		$sql = "SELECT f.id, d.id, d.name, d.pretty_url, d.profile_picture, d.breed, d.birthday FROM enemies f, dogs d WHERE f.dog_id = ? AND d.id = f.enemy_dog_id ";
		if($limit != NULL)
			$sql .= " LIMIT " . $limit;
		$resultset = $this->db->query($sql, array($dog_id));
		return $resultset; 
	}
	
	function getFriendsTotal($dog_id)
	{
		$this->db->from('friends');	
		$this->db->where('dog_id', $dog_id);
		return $this->db->count_all_results();
	}	
	
	function getEnemiesTotal($dog_id)
	{
		$this->db->from('enemies');	
		$this->db->where('dog_id', $dog_id);
		return $this->db->count_all_results();
	}		
	
	function getMutualFriends($dog_id, $friend_dog_id)
	{
		$sql = "SELECT count(a.friendID) as mutualfriends
				FROM
				      ( SELECT CASE WHEN dog_id = ?
				                      THEN friend_dog_id 
				                      ELSE dog_id 
				               END AS friendID 
				        FROM friends 
				        WHERE dog_id = ?
				      ) a
				  JOIN
				      ( SELECT CASE WHEN dog_id = ?
				                      THEN friend_dog_id 
				                      ELSE dog_id 
				               END AS friendID 
				        FROM friends 
				        WHERE dog_id = ?
				      ) b
				    ON b.friendID = a.friendID ";
		$resultset = $this->db->query($sql, array($dog_id, $dog_id, $friend_dog_id, $friend_dog_id));	
		return $resultset->row()->mutualfriends;
	}
	
	function getMutualEnemies($dog_id, $enemy_dog_id)
	{
		$sql = "SELECT count(a.friendID) as mutualfriends
				FROM
				      ( SELECT CASE WHEN dog_id = ?
				                      THEN enemy_dog_id 
				                      ELSE dog_id 
				               END AS friendID 
				        FROM enemies 
				        WHERE dog_id = ?
				      ) a
				  JOIN
				      ( SELECT CASE WHEN dog_id = ?
				                      THEN enemy_dog_id 
				                      ELSE dog_id 
				               END AS friendID 
				        FROM enemies 
				        WHERE dog_id = ?
				      ) b
				    ON b.friendID = a.friendID ";
		$resultset = $this->db->query($sql, array($dog_id, $dog_id, $enemy_dog_id, $enemy_dog_id));	
		return $resultset->row();
	}	

	function getShortinfo($dog_id)
	{
		$sql = "SELECT breed, birthday, name, profile_picture FROM dogs WHERE id = ?";
		$resultset = $this->db->query($sql, array($dog_id));
		return $resultset->row();
	}	
	
	function getOwners($dog_id)
	{
		$sql = "SELECT u.first_name, u.last_name, u.id, u.phone, u.email FROM users u, users_dogs ud WHERE ud.dog_id = ? and ud.user_id = u.id";
		$resultset = $this->db->query($sql, array($dog_id));
		return $resultset;
	}
	

	
	function favoriteVet($dog_id, $vet_id)
	{
		$data = array(
					'dog_id' => $dog_id,
					'vet_id' => $vet_id,
					'created_by' => $this->session->userdata('user_id'));
		$this->db->insert('dogs_vets', $data);
	}
	
	function unfavoriteVet($dog_id, $vet_id)
	{
		$this->db->delete('dogs_vets', array('dog_id' => $dog_id, 'vet_id' => $vet_id));
	}	
	
	function dogSearchProfile($search)
	{
		$sql = "SELECT d.id, d.name, d.pretty_url, d.nickname, DATE_FORMAT(d.birthday, '%d.%m.%Y') as birthday, d.profile_picture, d.cover_picture, d.gender, d.breed, d.color, d.coat, d.type, d.size, d.characteristics, d.about, d.chip_nr, d.modified_date, d.modified_by, d.created_date, d.created_by from dogs d WHERE (d.name LIKE '%".$search."%') OR (d.nickname LIKE '%".$search."%')";
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function checkDog($mydog_id,$selected_dog_id)
	{
		$sql = "SELECT * FROM ( SELECT * FROM enemies en WHERE en.enemy_dog_id = ? AND en.dog_id = ? UNION SELECT * FROM friends f  WHERE f.friend_dog_id = ? AND f.dog_id = ?) a;";
		$result = $this->db->query($sql, array($mydog_id,$selected_dog_id,$mydog_id,$selected_dog_id));
		if ($result->num_rows() > 0) {return true;}else {return false;}
	}
	
	function addDogFriend($mydog_id,$selected_dog_id)
	{
		$data = array(
					'friend_dog_id' => $selected_dog_id,
					'dog_id' => $mydog_id,
					'created_by' => $this->session->userdata('user_id'));
		$this->db->insert('friends', $data);
		
	}
	
	function addDogEnemy($mydog_id,$selected_dog_id)
	{
		$data = array(
					'enemy_dog_id' => $mydog_id,
					'dog_id' => $selected_dog_id,
					'created_by' => $this->session->userdata('user_id'));
		$this->db->insert('enemies', $data);
	}
	
	function checkActivity($activity_id)
	{
		$sql = "SELECT * FROM activities_invites ai WHERE ai.activity_id = ?";
		$result = $this->db->query($sql, array($activity_id));
		return $result;
	}
	
	
	function getBreeds()
	{
		$sql = "SELECT * FROM breeds WHERE accepted = '1' ORDER BY name ASC";
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function getBreedName($id)
	{
		$sql = "SELECT * FROM breeds WHERE id = ? LIMIT 1";
		$resultset = $this->db->query($sql, array($id));
		return $resultset->row();
	}
	
	function addBreedFromUser($name)
	{
		$data = array(
					'name' => $name,
					);
		$this->db->insert('breeds', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
		
	}
	
	function addTag($picture_id, $dog_id)
	{
		$data = array(
					'image_id' => $picture_id,
					'dog_id' => $dog_id,
					);
		$this->db->insert('tagged_dogs', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;		
	}
	
	function checkNearbyDog($dog_id, $user_id)
	{
		$sql = "SELECT * FROM users_dogs WHERE dog_id = ? AND user_id = ?";
		$result = $this->db->query($sql, array($dog_id,$user_id));
		if ($result->num_rows() > 0) {return true;}else {return false;}
	}
	
	function dogSearchProfileBreed($search,$breed)
	{
		$sql = "SELECT id, name, nickname, pretty_url, DATE_FORMAT(birthday, '%d.%m.%Y') as birthday, profile_picture, cover_picture, gender, breed, color, coat, type, size, characteristics, about, chip_nr, modified_date, modified_by, created_date, created_by from dogs WHERE name LIKE '%".$search."%' AND breed = '".$breed."'";
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	
	function deleteFriend($dog_id, $friend_id)
	{
		$sql = "DELETE FROM friends WHERE dog_id = ? AND friend_dog_id = ?";
		$this->db->query($sql, array($dog_id, $friend_id));
		
		$sql = "DELETE FROM friends WHERE dog_id = ? AND friend_dog_id = ?";
		$this->db->query($sql, array($friend_id, $dog_id));
	}
	
	function deleteEnemy($dog_id, $enemy_id)
	{
		$sql = "DELETE FROM enemies WHERE dog_id = ? AND enemy_dog_id = ?";
		$this->db->query($sql, array($dog_id, $enemy_id));
		
		$sql = "DELETE FROM enemies WHERE dog_id = ? AND enemy_dog_id = ?";
		$this->db->query($sql, array($enemy_id, $dog_id));
	}
	
	function deleteDogProfile($dog_id)
	{
		$sql = "DELETE FROM enemies WHERE dog_id = ?";
		$this->db->query($sql, array($dog_id));
		
		$sql = "DELETE FROM enemies WHERE enemy_dog_id = ?";
		$this->db->query($sql, array($dog_id));
		
		$sql = "DELETE FROM friends WHERE dog_id = ? ";
		$this->db->query($sql, array($dog_id));
		
		$sql = "DELETE FROM friends WHERE friend_dog_id = ?";
		$this->db->query($sql, array($dog_id));
		
		$sql = "DELETE FROM activities_invites WHERE dog_id = ?";
		$this->db->query($sql, array($dog_id));
		
		$sql = "DELETE FROM dogs WHERE id = ?";
		$this->db->query($sql, array($dog_id));
		
		$sql = "DELETE FROM tagged_dogs WHERE dog_id = ?";
		$this->db->query($sql, array($dog_id));
		
		$sql = "DELETE FROM users_dogs WHERE dog_id = ?";
		$this->db->query($sql, array($dog_id));
		
		$sql = "DELETE FROM event_dogs WHERE dog_id = ?";
		$this->db->query($sql, array($dog_id));
		
		$sql = "DELETE FROM events WHERE action_id = 1 AND entity_id = ?";
		$this->db->query($sql, array($dog_id));
		
		$sql = "DELETE FROM events WHERE action_id = 2 AND entity_id = ?";
		$this->db->query($sql, array($dog_id));
	}

	

}
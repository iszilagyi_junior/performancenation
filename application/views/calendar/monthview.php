	

	<?php
		//echo print_r($timeslots);
	?>

	<?php if(!$reload): ?>
		<script src="<?= site_url("items/frontend/js/calendar.js");?>" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/calendar.css"); ?>">
		
		<script>
			<?php foreach($formdata as $key => $value): ?>
				var <?=$key?> = "<?= $value ?>";
			<?php endforeach; ?>
		</script>	
	<?php endif; ?>
	
	<div id="calendar" class="<?= $format['container_class']?>" startdate="<?= $startdate?>" calendar_style="<?= $format['style']?>">
		<div class="calendar_header">
			<div class="buttons prevbutton" load_url="<?= $reload_url?>" reload_functions="<?= $custom_functions?>">
				<<
			</div>
			<div id="calender_header_text" class="caption">
				<?= $calendar_header ?>
			</div>
			<div class="buttons nextbutton" load_url="<?= $reload_url?>" reload_functions="<?= $custom_functions?>">
				>>
			</div>
		</div>
		<div id="calendar_content">
			<table id="calendar_table" class="<?= $format['calendar_class']?> calendar_basic">
				<thead>
					
					<?php for($i = 0; $i < 7 ; $i++): ?>
						<th><?= $colheaders[$i]?></th>
					<?php endfor; ?>
				</thead>
				<tbody>
					<tr>
					
					<!-- empty cells before first day of month -->
					<?php for($i = 0 ; $i < $format['startday'] ; $i++) :?>
						<td class="slot_unavailable"></td>
					<?php endfor; ?>
					
					<!-- days of month -->
					<?php $j = $format['startday'];	for($i = 1; $i <= $format['daycount'] ; $i++): ?>
						
						<?php if(isset($timeslots[$i])): ?>
							<td d="<?= date("Y-m-$i", strtotime($startdate))?>" class="<?= $timeslots[$i]['class']?>" <?= $timeslots[$i]['additional_str']?>><?= $i ?></td>
						<?php else: ?>
							<td d="<?= date("Y-m-$i", strtotime($startdate))?>" class="slot_unoccupied"><?= $i ?></td>
						<?php endif; ?>
						
						<?php if( ++$j == 7 && $i+1 <= $format['daycount']) :?>
							</tr><tr>
						<?php $j = 0; endif;?>
					<?php endfor; ?>
					
					<!-- empty cells after last day of month -->
					<?php if($j != 0): ?>
						<?php for( ; $j < 7 ; $j++): ?>
							<td class="slot_unavailable"></td>
						<?php endfor; ?>
					<?php endif; ?>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="calendar_footer">
			
		</div>
	</div>


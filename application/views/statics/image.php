<? if($loggedin){?>
<div id="comment_holder" pic="">	
	<div class="tag_overlay_close">x</div>
	<div class="tag_overlay_head" style=""><?= $this->lang->line('comments')?></div>
	<div id="add_comment_holder">
		<div class="regular" style="color:#ffffff;margin:10px 0px;font-size:14px;">Add Comment</div>
		<textarea id="comment_photo_input" name="comment_photo_input" ></textarea>
		<div id="add_comment_btn" class="button" style="float:none;top:5px;width:100%;">ADD</div>
	</div>
	<hr style="border:0px;height:1px;background:#fff;width:50%;margin:15px 25% 5px 25%;"/>	
	<div id="user_comments">
					
	</div>
</div>

<? }?>
<div id="content" style="background:none;">
	
	<div id="single_image_holder" style="width:100%;position:relative;top:35px;">
		<img class="single_image" src="<?= site_url('items/uploads/'.$picture['type'].'/'.$picture['fname'])?>" style="width:100%;"/>
	</div>
	<div class="lightbox_image_text_holder">
		<div class="image_text"><?= $picture['title']; ?></div>
		<div class="image_share_holder_top">
			<? if($loggedin){?>
				<img class="image_social_icon image_social_comment" pid="<?= $picture['id']; ?>" src="<?= site_url('items/frontend/img/photo_icons_comment_white-01.png');?>"/>
				
				<? if($already == 0){?>
					<img class="image_social_icon image_social_like" pid="<?= $picture['id']; ?>" src="<?= site_url('items/frontend/img/photo_icons_love_white-01.png');?>"/>
				<? }?>
		<?}
			else{
		?>
			<a style="text-decoration:none;color:#ffffff;" href="<?= site_url('')?>"><div style="font-size: 12px;position: absolute;right: 0px;width: 1000px;top: 0px;text-align:center;">Register to like or comment this image!</div></a>
		<? }?>
		</div>
		<div class="image_social_like_count">LIKES: <span class="the_likes"><?= $picture['likes']; ?></span></div>
		<div class="image_share_holder">
			<img class="image_share_icon share_image_tw" pid="<?= $picture['id']; ?>" src="<?= site_url('items/frontend/img/white_tw.png');?>"/>
			<img class="image_share_icon share_image_fb" pid="<?= $picture['id']; ?>"  fname="<?= $picture['fname']; ?>" title="<?= $picture['title']; ?>" style="margin-left:18px;" src="<?= site_url('items/frontend/img/white_fb.png');?>"/>	
		</div>
	</div>
	<br>
	
</div>
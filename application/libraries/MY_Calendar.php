<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class MY_Calendar
{
	const CALENDAR_CHANGE_NEXT = 0;
	const CALENDAR_CHANGE_PREVIOUS = 1;	
	const CALENDAR_CHANGE_NOCHANGE = 2;
	
	const CALENDAR_STYLE_WEEK = 0;
	const CALENDAR_STYLE_MONTH = 1;
	
	
	protected $ci = null;
	protected $calendar_style = null;
	protected $calendar_startdate = null;
	protected $calendar_header = "";
	protected $reload_url = null;
	protected $reload = false;
	protected $formdata = array();
	protected $custom_js_function = array();
	
	protected $timeslots = null;


    public function __construct()
    {
		$this->ci = &get_instance();
		$this->reload_url = site_url('calendar/loadCalendar');
    }


	/**********************************************************************************************
	 * SETTERS
	 **********************************************************************************************/
	public function setCalendarStyle($style)
	{
		$this->calendar_style = $style;
	}
	
	public function setCalendarHeader($header)
	{
		$this->calendar_header = $header;
	}
	
	public function setCalendarStartdate($date)
	{
		$this->calendar_startdate = $date;
	}
	
	public function setReloadUrl($url)
	{
		$this->reload_url = $url;
	}
	
	public function setReload($reload)
	{
		$this->reload = $reload;
	}
	
	public function setCustomJSFunction($functions)
	{
		$this->custom_js_function = $functions;
	}
	
	public function setTimeslots($timeslots)
	{
		$this->timeslots = $timeslots;
	}
	
	public function setFormData($array)
	{
		$this->formdata = $array;
	}
	
	/********************************************************************************************
	 * RANDOM FUNCTIONS
	 ********************************************************************************************/
	public function getWeekday($date)
	{
		$wd = date('w', strtotime($date));
		$wd--;
		if($wd < 0)
			$wd = 6;
		
		return $wd;
	}
	
	public function calculateNewStartdate($startdate, $change, $style)
	{
		
		switch($change)
		{
			case MY_Calendar::CALENDAR_CHANGE_NEXT:
				$multiplier = 1;
				break;
			case MY_Calendar::CALENDAR_CHANGE_PREVIOUS:
				$multiplier = -1;
				break;
			case MY_Calendar::CALENDAR_CHANGE_NOCHANGE:
				$multiplier = 0;
				break;								
		}
		
		$cd = strtotime($startdate);
		switch($style)
		{
			case My_Calendar::CALENDAR_STYLE_WEEK:
				$newdate = date('Y-m-d h:i:s', 
								mktime(date('h',$cd), 
								date('i',$cd), 
								date('s',$cd), 
								date('m',$cd),
    							date('d',$cd) + (7 * $multiplier), 
    							date('Y',$cd)));
				break;
			case My_Calendar::CALENDAR_STYLE_MONTH:
				$newdate = date('Y-m-d h:i:s', 
								mktime(date('h',$cd), 
								date('i',$cd), 
								date('s',$cd), 
								date('m',$cd) + (1 * $multiplier),
								date('d',$cd), 
								date('Y',$cd)));
				break;	
		}			
		
		return $newdate;
	}

	public function lastDayofMonth($date)
	{
		return date("Y-m-t", strtotime($date)); 
	}
	
	
	public function getWeekdaysFromRange($start, $end, $weekday)
	{
		
		$start = strtotime($start);
		$end = strtotime($end);
		
		$weekdays = array();
		
		while($start <= $end)
		{

			if($this->getWeekday(date('Y-m-d', $start)) == $weekday)
				$weekdays[] = date('j', $start);
			
			$start = strtotime('+1 day', $start);
		}
		return $weekdays;
	}

	/*********************************************************************************************
	 * RENDERING
	 *********************************************************************************************/
	public function render($params = null)
	{
		$data['calendar_header'] = $this->createCalenderHeader();
		$data['format'] = $this->getCalendarStyle();
		$data['rowheaders'] = $this->getRowHeaders();
		$data['colheaders'] = $this->getColHeaders();
		$data['reload_url'] = $this->reload_url;
		$data['startdate'] = date('Y-m-d', strtotime($this->calendar_startdate));
		$data['reload'] = $this->reload;
		$data['formdata'] = $this->formdata;
		$data['custom_functions'] = $this->custom_js_function;
		
		if($this->timeslots != null)
			$this->prepareTimeslots();
		$data['timeslots'] = $this->timeslots;
		
		return $this->ci->load->view('calendar/' . $data['format']['container_class'], $data, true);
	}
	
	private function getCalendarStyle()
	{
		$data = array();
		switch($this->calendar_style)
		{
			case My_Calendar::CALENDAR_STYLE_WEEK:
				$data['calendar_class'] = "calendar_weekly";
				$data['row_count'] = 48;
				$data['container_class'] = "weekview";
				$data['style'] = My_Calendar::CALENDAR_STYLE_WEEK;
				break;
			case My_Calendar::CALENDAR_STYLE_MONTH:
				$data['style'] = My_Calendar::CALENDAR_STYLE_MONTH;
				$data['calendar_class'] = "calendar_monthly";
				$data['col_count'] = 5;
				$data['container_class'] = "monthview";
				$data['daycount'] = date_format(new DateTime($this->calendar_startdate), 't');
				$data['startday'] = $this->getWeekday($this->calendar_startdate);
				$data['startweek'] = date('W', strtotime($this->calendar_startdate)); 
				break;
		}
		
		return $data;
	}

	private function getRowHeaders()
	{
		switch($this->calendar_style)
		{
			case My_Calendar::CALENDAR_STYLE_WEEK:
				$data[0] = "00:00";
				$data[1] = "";//"00:30";
				$data[2] = "01:00";
				$data[3] = "";//"01:30";
				$data[4] = "02:00";
				$data[5] = "";//"02:30";
				$data[6] = "03:00";
				$data[7] = "";//"03:30";
				$data[8] = "04:00";
				$data[9] = "";//"04:30";
				$data[10] = "05:00";
				$data[11] = "";//"05:30";
				$data[12] = "06:00";
				$data[13] = "";//"06:30";
				$data[14] = "07:00";
				$data[15] = "";//"07:30";
				$data[16] = "08:00";
				$data[17] = "";//"08:30";
				$data[18] = "09:00";
				$data[19] = "";//"09:30";
				$data[20] = "10:00";
				$data[21] = "";//"10:30";
				$data[22] = "11:00";
				$data[23] = "";//"11:30";
				$data[24] = "12:00";
				$data[25] = "";//"12:30";
				$data[26] = "13:00";
				$data[27] = "";//"13:30";
				$data[28] = "14:00";
				$data[29] = "";//"14:30";
				$data[30] = "15:00";
				$data[31] = "";//"15:30";
				$data[32] = "16:00";
				$data[33] = "";//"16:30";
				$data[34] = "17:00";
				$data[35] = "";//"17:30";
				$data[36] = "18:00";
				$data[37] = "";//"18:30";
				$data[38] = "19:00";
				$data[39] = "";//"19:30";
				$data[40] = "20:00";
				$data[41] = "";//"20:30";
				$data[42] = "21:00";
				$data[43] = "";//"21:30";
				$data[44] = "22:00";								
				$data[45] = "";//"22:30";
				$data[46] = "23:00";
				$data[47] = "";//"23:30";
				break;
			case My_Calendar::CALENDAR_STYLE_MONTH:
				$data = array();
				break;
		}
		return $data;		
	}

	private function getColHeaders()
	{
		switch($this->calendar_style)
		{
			case My_Calendar::CALENDAR_STYLE_WEEK:
				$data = array();
				break;
			case My_Calendar::CALENDAR_STYLE_MONTH:
				$data[0] = "Mon";
				$data[1] = "Tue";
				$data[2] = "Wed";
				$data[3] = "Thu";
				$data[4] = "Fri";
				$data[5] = "Sat";
				$data[6] = "Sun";
				break;
		}
		return $data;			
	}	
	
	private function createCalenderHeader()
	{
		if($this->calendar_header == "")
		{
			switch($this->calendar_style)
			{
				case My_Calendar::CALENDAR_STYLE_WEEK:
					break;
				case My_Calendar::CALENDAR_STYLE_MONTH:
					$header = date('F Y', strtotime($this->calendar_startdate));
					break;					
			}
		}
		return $header;
	}
	
	
	/*********************************************************************************************************************
	 * TIMESLOT SHENANNIGANS
	 *********************************************************************************************************************/
	private function prepareTimeslots()
	{
		switch($this->calendar_style)
		{
			case My_Calendar::CALENDAR_STYLE_WEEK:		
				foreach($this->timeslots as $j => $days)
				{
					foreach($days as $i => $timeslot)
					{
						$this->timeslots[$j][$i]['additional_str'] = "";
						
						if(isset($timeslot['additional']))
						{
							foreach($timeslot['additional'] as $key => $value)
							{
								$this->timeslots[$j][$i]['additional_str'] .= ' ' . $key . '=' . '"' . $value . '"';
							}
						}
					}
				}
				break;
			
			case My_Calendar::CALENDAR_STYLE_MONTH:
				foreach($this->timeslots as $day => $timeslot)
				{
					$this->timeslots[$day]['additional_str'] = "";
					
					if(isset($timeslot['additional']))
					{
						foreach($timeslot['additional'] as $key => $value)
						{
							if($key == 'bookings')
							{
								$bookings = "";
								foreach($value as $booking)
								{
									$bookings .= ($bookings == "" ? "" : ",") . $booking;
								}
								$value = $bookings;
							}
							
							$this->timeslots[$day]['additional_str'] .= ' ' . $key . '=' . '"' . $value . '"';
							
							
						}
					}
				}
				break;
		}
	}

}
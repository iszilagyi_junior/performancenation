<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statics extends MY_Controller {

    function __construct()
    {
        parent::__construct();

		$this->config->load('dog');

		$this->load->model('statics_model');
		$this->load->model('car_model');
		
		$this->checkLogin();
		
    }  	 
	 
	public function index()
	{
		
		$this->checkIPaddress();
		$this->loadDefaultView('frontend/home');
	}
	
	public function checkCountry()
	{
		$advertisement = array();
		$partners = $this->statics_model->getPartners()->result();
		$user_id = 3;
		$user = $this->statics_model->getUserData($user_id)->row();
		
		$user_country = $user->country;
		$user_state = $user->state;
		if($user_country === NULL || $user_country == "")
		{
		
			if($user->geo_location !== NULL && $user->geo_location != "")
			{
				
				$geocode=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$user->geo_location.'&sensor=false');

		        $output= json_decode($geocode);
				
				for($j=0;$j<count($output->results[0]->address_components);$j++)
				{
						if($output->results[0]->address_components[$j]->types[0] == "country")
						{
							
							$user_country = $output->results[0]->address_components[$j]->short_name;
							$data = array('id' => $user_id,
									'country' => $user_country);
							$this->statics_model->updateUser($data);
						}
						
		                if($output->results[0]->address_components[$j]->types[0] == "administrative_area_level_1")
						{
							$user_state = $output->results[0]->address_components[$j]->short_name;
							$data = array('id' => $user_id,
										'state' => $user_state);
							$this->statics_model->updateUser($data);
						}
						
									
		        }
 
			}
		}
		
				
		
		
		if($user_country !== null && $user_country !== "")
		{
			
			foreach($partners as $partner)
			{
				if($user_country != "US" && $user_country == $partner->country)
				{
					
					$partner_id = $partner->id;
				}
				else
				{
					if($user_country == "US" && $partner->country == "US")
					{
					
						if($user_state == $partner->state)
						{
						
							$partner_id = $partner->id;
						}
					}
				}
				
			}
			
		    $advertisement = $this->statics_model->getAdvertisement($partner_id)->row();
		    
		    
		    
		  /*  if($template_name == 'frontend/home' && $this->ion_auth->logged_in())
			{
				$this->statics_model->updateImpressions($partner_id);
			}*/
		}
	}
	
	
	public function welcome_mail()
	{
		
	/*	$this->checkIPaddress();
		$user = $this->statics_model->getUserData($this->session->userdata('user_id'))->row();
	
		$mailname = $user->first_name." ".$user->last_name;
		$data['mailname'] = $mailname;
		$this->load->view('mail/invite_mail', $data);
	
	 */ 
	}
	
		
	
	public function places()
	{

		$places_data = array();
		$places = $this->statics_model->getPlaces()->result();
		
		foreach($places as $place)
		{
			$type = $this->statics_model->getPlaceTypeByID($place->id)->row()->name;
			$place_item = array('id' => $place->id,
								'name' => $place->name,
								'pretty' => $place->pretty_url,
								'type' => $type);
			$places_data[] = $place_item;
		}
		
		$featured_data = array();
		$featured = $this->statics_model->getPlacesFeatured()->result();
		
		foreach($featured as $place)
		{
			$type = $this->statics_model->getPlaceTypeByID($place->id)->row()->name;
			$place_item = array('id' => $place->id,
								'name' => $place->name,
								'pretty' => $place->pretty_url,
								'type' => $type);
			$featured_data[] = $place_item;
		}
		
		
		$data['places'] = $places_data;
		$data['featured'] = $featured_data;
		$this->loadDefaultView('statics/places', $data);		
	}
	
	public function place_detail_pretty($pretty_url)
	{
		
		$data = null;
		$place = $this->statics_model->getPrettyUrlPlace($pretty_url)->row();
		
		$data['place'] = $place;
		
		$this->loadDefaultView('statics/place_detail', $data);	
	}
	
	

	
	
	public function rate()
	{
		$rating = $_POST['rating'];
		$poi_id = $_POST['poi_id'];
		
		$action_id = 12;
		

		if(!$this->statics_model->checkRating($poi_id,$this->session->userdata('user_id'),'poi_ratings','poi_id')){
			$entity_id = $poi_id;
			$event_id = $this->statics_model->addEvent($action_id, $entity_id);		
			$vehicles = $this->car_model->getVehicles($this->session->userdata('user_id'));
			
			foreach($vehicles->result() as $car){
				$this->statics_model->addEventVehicle($car->id,$event_id);
			}

			$this->statics_model->rate($poi_id,$this->session->userdata('user_id'),$rating,'poi_ratings','poi_id');
		}
		
	}	
	
	public function share()
	{
		$data = null;	
		$this->loadDefaultView('statics/shares', $data);		
	}
	
	public function vehicle_search()
	{
		$this->checkLogin();
		$data = null;	
		$vehicles = $this->car_model->getAllVehicles()->result();
		$all_vehicles = array();
		
		$follow_ids = array();
		
		$my_follows = $this->car_model->getMyFollows($this->session->userdata('vehicle_id'));
		
	/*	foreach($my_follows as $follower)
		{

			$follow_ids[] = $follower['vehicle_id'];

		}
		*/
		
		
		
		foreach($vehicles as $vehicle)
		{
			$fname = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
			
			switch($vehicle->vehicle_type)
			{
				case 0:{
							 $model = $this->car_model->getCarModelById($vehicle->model_id)->row();
				};break;
				
				case 1:{
							 $model = $this->car_model->getMotorModelById($vehicle->model_id)->row();
				};break;
				
				case 2:{
							 $model = $this->car_model->getTruckModelById($vehicle->model_id)->row();
				};break;
			}	
			
			if(!in_array($vehicle->id, $follow_ids))
			{
				$followers = $this->car_model->getFollowers($vehicle->id);
				
				$item = array('id' => $vehicle->id,
							'nickname' => $vehicle->nickname,
							'type' => $vehicle->vehicle_type,
							'pretty_url' => $vehicle->pretty_url,
							'geo_location' => $vehicle->geo_location,
							'zerosixty' => $vehicle->zerosixty,
							'followers' => count($followers),
							'model' => $model,
							'fname' => $fname);
				
				$all_vehicles[] = $item;
			}
		}
		$data['vehicles_count'] = count($vehicles);	
		
		$data['all_vehicles'] = json_encode($all_vehicles);
		
		$vehicles = $this->car_model->getRandomVehicles()->result();
		$data['vehicles'] = array();
		
		$follow_ids = array();
		
		$my_follows = $this->car_model->getMyFollows($this->session->userdata('vehicle_id'));
		
	/*	foreach($my_follows as $follower)
		{

			$follow_ids[] = $follower['vehicle_id'];

		}
		*/
		
		
		
		foreach($vehicles as $vehicle)
		{
			$fname = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
			
			switch($vehicle->vehicle_type)
			{
				case 0:{
							 $model = $this->car_model->getCarModelById($vehicle->model_id)->row();
				};break;
				
				case 1:{
							 $model = $this->car_model->getMotorModelById($vehicle->model_id)->row();
				};break;
				
				case 2:{
							 $model = $this->car_model->getTruckModelById($vehicle->model_id)->row();
				};break;
			}	
			
			if(!in_array($vehicle->id, $follow_ids))
			{
				$followers = $this->car_model->getFollowers($vehicle->id);
				
				$item = array('id' => $vehicle->id,
							'nickname' => $vehicle->nickname,
							'type' => $vehicle->vehicle_type,
							'pretty_url' => $vehicle->pretty_url,
							'geo_location' => $vehicle->geo_location,
							'zerosixty' => $vehicle->zerosixty,
							'followers' => count($followers),
							'model' => $model,
							'fname' => $fname);
				
				$data['vehicles'][] = $item;
			}
		}
		
		
		
		$this->loadDefaultView('statics/vehicle_search', $data);		
	}
	
	
	public function group_search()
	{
		$this->checkLogin();
		$data = null;	
		$groups = $this->statics_model->getGroups();
		$data['groups'] = array();
		
		
		foreach($groups as $group)
		{
			$members = $this->statics_model->getGroupMembers($group['id'])->result();
			
			$category = $this->statics_model->getGroupCategory($group['category_id'])->row()->name;
			
			$item = array('id' => $group['id'],
						'name' => $group['name'],
						'description' => $group['description'],
						'members' => count($members),
						'cover' => $group['cover_image'],
						'category' => $category,
						'logo' => $group['logo'],
						'geo_location' => $group['geo_location']);
			
			$data['groups'][] = $item;
		}
		$data['group_count'] = count($groups);	
		$data['all_clubs'] = json_encode($data['groups']);
		$data['group_categories'] = $this->statics_model->getGroupCategories();
		
		
		$this->loadDefaultView('statics/group_search', $data);		
	}
	
	
	public function event_search()
	{
		$this->checkLogin();
		$data = null;	
		$events = $this->statics_model->getUserEvents();
		$group_events = $this->statics_model->getPublicGroupEvents();
		$data['events'] = array();
		$data['all_events_holder'] = array();
		
		$today = new DateTime('today'); // This object represents current date/time
		
		foreach($events as $event)
		{
			$participants = $this->statics_model->getVehiclesFromUserEvent($event['id']);
			$vehicle = $this->car_model->getVehicle($event['vehicle_id'])->row();
			
			$image = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
			$image = "profilepictures/".$image;					
			if($event['image'] != NULL && $event['image'] != '')
			{
				$image = "coverpictures/".$event['image'];
			}
			
			$match_date = new DateTime($event['start_time']);
			$match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
			
			$diff = $today->diff( $match_date );
			$diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval
			$visible = false;
			if($diffDays >= 0)
			{
				$visible = true;
			}
			$item = array('id' => $event['id'],
						'title' => $event['title'],
						'address' => $event['address'],
						'type' => 'user',
						'geo_location' => $event['geo_location'],
						'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
						'date' => $event['start_time'],
						'logo' => $image,
						'visible' => $visible,
						'participants' => count($participants));
			
			$data['all_events_holder'][] = $item;

				
							
				if($diffDays >= 0)
				{
					$data['events'][] = $item;
				}	
		}
			
		foreach($group_events as $event)
		{
			$participants = $this->statics_model->getVehiclesFromEvent($event['id']);
			$group = $this->statics_model->getGroupById($event['group_id'])->row();
			
			$image = $group->logo;
			$image = "profilepictures/".$image;
					
			if($group->image != NULL && $group->image != '')
			{
				$image = "coverpictures/".$group->image;
			}
			
			$match_date = new DateTime($event['start_time']);
			$match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
			
			$diff = $today->diff( $match_date );
			$diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval
			
			$visible = false;
			if($diffDays >= 0)
			{
				$visible = true;
			}
			
			$item = array('id' => $event['id'],
						'title' => $event['title'],
						'type' => 'group',
						'address' => $event['address'],
						'geo_location' => $event['geo_location'],
						'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
						'date' => $event['start_time'],
						'logo' => $image,
						'visible' => $visible,
						'participants' => count($participants));
			$data['all_events_holder'][] = $item;
			
						
			if($diffDays >= 0)
			{
				$data['events'][] = $item;
			}
		}
		
		
		function date_compared($a, $b)
		{
		    $t1 = strtotime($a['date']);
		    $t2 = strtotime($b['date']);
		    return $t2 - $t1;
		}      
		 
		usort($data['events'], 'date_compared');
		
		
		$data['all_events'] = json_encode($data['all_events_holder']);
		$data['group_count'] = count($groups);	
		$this->loadDefaultView('statics/event_search', $data);		
	}
	
	
	
	public function event_search_result()
	{
		$search = $_POST['search'];
		$search = trim($search);
		
		$name = $_POST['name'];
		$name = trim($name);
		
		$start = $_POST['start'];
		$start = trim($start);
		
		$end = $_POST['end'];
		$end = trim($end);
		
		$search_distance = $_POST['distance'];
		$search_distance = trim($search_distance);
		
		$geo_location = $_POST['geo_location'];
		$show_past = $_POST['show_past'];
		$data['events'] = array();
		$events = array();
		
		$table = "user_created_events";
		
		
		
		if($start == '' && $end =='')
		{
			
			$user_event_container = $this->statics_model->eventSearchTitle($search, $table)->result_array();
		}
		else if($start != '' && $end =='')
		{
			
			$start = date('y-m-d H:i:s', strtotime($start));	
		
			$user_event_container = $this->statics_model->eventSearchTitleStart($search, $table, $start)->result_array();
		}
		else if($start == '' && $end !='')
		{
			$end = date('y-m-d H:i:s', strtotime($end));	
			$user_event_container = $this->statics_model->eventSearchTitleEnd($search, $table, $end)->result_array();
		}
		else if($start != '' && $end !='')
		{
			$end = date('y-m-d H:i:s', strtotime($end));
			$start = date('y-m-d H:i:s', strtotime($start));
				
			$user_event_container = $this->statics_model->eventSearchTitleBetween($search, $table, $start, $end)->result_array();
		}
		
	
		$today = new DateTime('today'); // This object represents current date/time
		
		foreach($user_event_container as $event){
			
			$participants = $this->statics_model->getVehiclesFromUserEvent($event['id']);
			$vehicle = $this->car_model->getVehicle($event['vehicle_id'])->row();
			
			$image = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
			$image = "profilepictures/".$image;					
			if($event['image'] != NULL && $event['image'] != '')
			{
				$image = "coverpictures/".$event['image'];
			}
			
			if($name == '' ||  strpos(strtolower($vehicle->nickname), strtolower($name)) !== false )
			{
					
				if($geo_location == "")
				{
						
						
						if($show_past == 0)
						{
							

							$match_date = new DateTime($event['start_time']);
							$match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
							
							$diff = $today->diff( $match_date );
							$diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval
							
							if($diffDays >= 0)
							{
								$item = array('id' => $event['id'],
									'title' => $event['title'],
									'type' => 'user',
									'address' => $event['address'],
									'geo_location' => $event['geo_location'],
									'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
									'logo' => $image,
									'participants' => count($participants));
			
								$data['events'][] = $item;
							}

						}
						else
						{
							$item = array('id' => $event['id'],
									'title' => $event['title'],
									'type' => 'user',
									'address' => $event['address'],
									'geo_location' => $event['geo_location'],
									'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
									'logo' => $image,
									'participants' => count($participants));
			
							$data['events'][] = $item;	
						}
				
						
						
						
							
						
						
						
						
				}
				else
				{
					$to_arr = explode(",", $event['geo_location']);
						$from_arr = explode(",", $geo_location);
						
	
						$from_lat = $from_arr[0];
						$from_lon = trim($from_arr[1]);
						$to_lat = $to_arr[0];
						$to_lon = trim($to_arr[1]);
						
						
						$distance = $this->distance($to_lat, $to_lon, $from_lat, $from_lon, "K");
						$distance = intval($distance*1000);
						$nearby_limit = 15000;
						if($search_distance != '')
						{
							$nearby_limit = $search_distance*1609.344;
						}
				
						if($distance !== NULL && $distance <= $nearby_limit)
						{
								
							if($show_past == 0)
							{
							

								$match_date = new DateTime($event['start_time']);
								$match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
								
								$diff = $today->diff( $match_date );
								$diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval
								
								if($diffDays >= 0)
								{
									$item = array('id' => $event['id'],
										'title' => $event['title'],
										'type' => 'user',
										'address' => $event['address'],
										'geo_location' => $event['geo_location'],
										'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
										'logo' => $image,
										'participants' => count($participants));
				
									$data['events'][] = $item;	
								}
	
							}
							else
							{
								$item = array('id' => $event['id'],
										'title' => $event['title'],
										'type' => 'user',
										'address' => $event['address'],
										'geo_location' => $event['geo_location'],
										'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
										'logo' => $image,
										'participants' => count($participants));
				
								$data['events'][] = $item;
							}
							
						}
				}	

			}
		}
								
		
		$table = "managed_group_events";
		
		
		
		if($start == '' && $end =='')
		{
			$group_event_container = $this->statics_model->eventSearchTitle($search, $table)->result_array();
		}
		else if($start != '' && $end =='')
		{
			
			$start = date('y-m-d H:i:s', strtotime($start));	
			$group_event_container = $this->statics_model->eventSearchTitleStart($search, $table, $start)->result_array();
		}
		else if($start == '' && $end !='')
		{
			$end = date('y-m-d H:i:s', strtotime($end));	
			$group_event_container = $this->statics_model->eventSearchTitleEnd($search, $table, $end)->result_array();
		}
		else if($start != '' && $end !='')
		{
			$end = date('y-m-d H:i:s', strtotime($end));
			$start = date('y-m-d H:i:s', strtotime($start));	
			$group_event_container = $this->statics_model->eventSearchTitleBetween($search, $table, $start, $end)->result_array();
		}
		
		
		
		
		foreach($group_event_container as $event){
			
			$participants = $this->statics_model->getVehiclesFromEvent($event['id']);
			$group = $this->statics_model->getGroupById($event['group_id'])->row();
			
			$image = $group->logo;
			$image = "profilepictures/".$image;
					
			if($group->image != NULL && $group->image != '')
			{
				$image = "coverpictures/".$group->image;
			}
			if($name == '' ||  strpos(strtolower($group->name), strtolower($name)) !== false )
			{
				if($geo_location == "")
				{
							
						if($show_past == 0)
						{
						

							$match_date = new DateTime($event['start_time']);
							$match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
							
							$diff = $today->diff( $match_date );
							$diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval
							
							if($diffDays >= 0)
							{
								$item = array('id' => $event['id'],
									'title' => $event['title'],
									'type' => 'user',
									'address' => $event['address'],
									'geo_location' => $event['geo_location'],
									'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
									'date' => $event['start_time'],
									'logo' => $image,
									'participants' => count($participants));
			
								$data['events'][] = $item;		
							}
	
						}
						else
						{
							$item = array('id' => $event['id'],
									'title' => $event['title'],
									'type' => 'user',
									'address' => $event['address'],
									'geo_location' => $event['geo_location'],
									'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
									'date' => $event['start_time'],
									'logo' => $image,
									'participants' => count($participants));
			
							$data['events'][] = $item;	
						}
							
							
						
							
						
						
						
						
				}
				else
				{
					$to_arr = explode(",", $event['geo_location']);
						$from_arr = explode(",", $geo_location);
						
	
						$from_lat = $from_arr[0];
						$from_lon = trim($from_arr[1]);
						$to_lat = $to_arr[0];
						$to_lon = trim($to_arr[1]);
						
						
						$distance = $this->distance($to_lat, $to_lon, $from_lat, $from_lon, "K");
						$distance = intval($distance*1000);
						$nearby_limit = 15000;
						if($search_distance != '')
						{
							$nearby_limit = $search_distance*1609.344;
						}
						
						
							
						if($distance !== NULL && $distance <= $nearby_limit)
						{
								
							if($show_past == 0)
							{
							
	
								$match_date = new DateTime($event['start_time']);
								$match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
								
								$diff = $today->diff( $match_date );
								$diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval
								
								if($diffDays >= 0)
								{
									$item = array('id' => $event['id'],
										'title' => $event['title'],
										'type' => 'user',
										'address' => $event['address'],
										'geo_location' => $event['geo_location'],
										'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
										'date' => $event['start_time'],
										'logo' => $image,
										'participants' => count($participants));
				
									$data['events'][] = $item;
								}
							}
							else
							{
								$item = array('id' => $event['id'],
										'title' => $event['title'],
										'type' => 'user',
										'address' => $event['address'],
										'geo_location' => $event['geo_location'],
										'start' => date('F j, Y, \a\t g:i a',strtotime($event['start_time'])),
										'date' => $event['start_time'],
										'logo' => $image,
										'participants' => count($participants));
				
								$data['events'][] = $item;
							}
									
								
								
								
							
						}
				}
	
			}
		}
		
		
		function date_compared($a, $b)
		{
		    $t1 = strtotime($a['date']);
		    $t2 = strtotime($b['date']);
		    return $t2 - $t1;
		}      
		 
		usort($data['events'], 'date_compared');
		
		
		$data['vehicles_count'] = count($data['events']);	
		
		$returnjson = array('html' => $this->load->view('statics/event_search_result', $data, true),
							'events' => $data['events']);
							
		echo json_encode($returnjson);					
		//return $this->load->view('statics/event_search_result', $data);
				
	}
	
	
	
	
	public function poi_search()
	{
		$this->checkLogin();
		$data = null;	
		
		$data['pois'] = array();
		$poi = $this->statics_model->getPOIs()->result();
		
		
		foreach($poi as $place)
		{
			$categories = $this->statics_model->getPOICategoriesConnection($place->id);
			$ratings = $this->statics_model->getRatings($place->id,$this->session->userdata('user_id'),'poi_ratings','poi_id')->result();
			$rows = count($ratings);
			$ratingSum = 0;
			foreach($ratings as $i=>$row){
				$ratingSum += $row->rating;
			}
			if($rows == 0){
				$rating = "No ratings yet!";
				$rSum = 0;
			}
			else{
				$rSum = $ratingSum/$rows;
				$rSum = round($rSum, 2);
				$number_of_votes = $rows;
				$rating = "Rating: ".$rSum." from ".$number_of_votes." votes";
				
			}
			
			$category_ids = array();
			
			foreach($categories as $cat)
			{
				$category_ids[] = $cat->category_id;
			}
			
			
			
			
			
			
			
			$item = array('id' => $place->id,
							'name' => $place->name,
							'geo_location' => $place->geo_location,	
							'category_ids' => $category_ids,
							'address' => $place->address,
							'rating' => $rating,
							'rating_num' => $rSum);
			
			$data['pois'][] = $item;
		}
		
		$data['pois_json'] = json_encode($data['pois']);
		
		
		
		$data['categories'] = $this->statics_model->getPOICategories()->result();
		$this->loadDefaultView('statics/poi_search', $data);		
	}
	
	
	public function albums($vehicle_id)
	{
		$this->checkLogin();
		
		$albums = $this->statics_model->getAlbums($vehicle_id);
		$data['count_albums'] = count($albums);
		$albums_data = array();
		
		$data['my_vehicle'] = false;
		
		$user = $this->session->userdata('user_id');
		
		$my_vehicles = $this->car_model->getVehicles($user)->result();
		$data['my_vehicle_id'] = $vehicle_id;
		foreach($my_vehicles as $vehicle)
		{
			
			if($vehicle->id == $vehicle_id)
			{
				
				$data['my_vehicle'] = true;
			}
		}
		
		
		
		foreach($albums as $album)
		{
			$title = $album['title'];
			$about = $album['about'];
			$pictures = $this->statics_model->getPicturesFromAlbum($album['id'])->result();
			$pictures_data = array();
			foreach($pictures as $picture)
			{
				$filename = $picture->fname;	
				if($picture->cover_image == 1)	
				{
					$folder = 'coverpictures';
				}
				else if($picture->profile_image == 1)
				{
					$folder = 'profilepictures';
				}
				else
				{
					$folder = 'images';
				}		
					$pictures_data[] = array('fname' => $picture->fname,
											'folder' => $folder);
				
			}	
			
			$albums_data[] = array('id' => $album['id'],
								  'title' => $title,
								  'about' => $about,
								  'pictures' => $pictures_data);
		}
		
		$data['albums'] = $albums_data;
		$this->session->set_userdata('album_user', $vehicle_id);		
		$this->loadDefaultView('statics/vehicle_albums', $data);		
	}
	
	
	public function photos($user_id)
	{
		$this->checkLogin();
		$albums = $this->statics_model->getAlbums($user_id);
		$albums_data = array();
		$data['user_id'] = $user_id;
		foreach($albums->result() as $album)
		{
			$title = $album->title;
			$about = $album->about;
			$pictures = $this->statics_model->getPicturesFromAlbum($album->id)->result();
			$pictures_data = array();
			foreach($pictures as $picture)
			{
				$filename = $picture->fname;			
				$pictures_data[] = array('fname' => $picture->fname);
			}	
			
			$albums_data[] = array('id' => $album->id,
								  'title' => $title,
								  'about' => $about,
								  'pictures' => $pictures_data);
		}
		$data['albums'] = $albums_data;
				
		$this->loadDefaultView('statics/photos', $data);		
	}
	
	
	public function edit_album($album_id, $vehicle_id)
	{
		$this->checkLogin();
		$data['album'] = $this->statics_model->editSingleAlbum($album_id,$vehicle_id);
		
		
		
		$pictures = $this->statics_model->getPicturesFromAlbum($album_id)->result();
		foreach($pictures as $picture)
		{
			$already = $this->statics_model->checkPictureLike($picture->id, $this->session->userdata('user_id'));
			
			if($picture->cover_image == 1)	
			{
				$folder = 'coverpictures';
			}
			else if($picture->profile_image == 1)
			{
				$folder = 'profilepictures';
			}
			else
			{
				$folder = 'images';
			}
			
			$data['pictures'][] = array('id' =>$picture->id,
										'title' =>$picture->title,
										'likes' =>$picture->likes,
										'folder' =>$folder,
										'fname' =>$picture->fname,
										'already' =>$already);
											
										
		}
			
		$data['count_pictures'] = count($data['pictures']);
		$this->session->set_userdata('album_id', $album_id);
		$this->loadDefaultView('statics/edit_album', $data);		
	}
	
	public function show_album($album_id)
	{
		$this->checkLogin();
		$data['pictures'] = $this->statics_model->getPicturesFromAlbum($album_id)->result();	
		$data['album'] = $this->statics_model->getSingleAlbum($album_id);
		$data['count_pictures'] = count($data['pictures']);
		$user_id = $this->statics_model->getSingleAlbum($album_id)->created_by;
		$this->session->set_userdata('album_user', $user_id);	
		$data['user'] = $this->session->userdata('album_user');
		
		$this->loadDefaultView('statics/show_album', $data);		
	}

	public function update_photo_title()
	{
		$this->checkLogin();
		$pid = $_POST['pid'];
		$title = $_POST['title'];
		$data = array('title' => $title);
		$this->statics_model->updatePictureTitle($data,$pid);					
	}
	
	
	public function album($album_id)
	{
		$this->checkLogin();
		$data['album'] = $this->statics_model->getSingleAlbum($album_id);
		$pictures = $this->statics_model->getPicturesFromAlbum($album_id)->result();
		foreach($pictures as $picture)
		{
			$already = $this->statics_model->checkPictureLike($picture->id, $this->session->userdata('user_id'));
			
			if($picture->cover_image == 1)	
			{
				$folder = 'coverpictures';
			}
			else if($picture->profile_image == 1)
			{
				$folder = 'profilepictures';
			}
			else
			{
				$folder = 'images';
			}
			
			$data['pictures'][] = array('id' =>$picture->id,
										'title' =>$picture->title,
										'folder' =>$folder,
										'likes' =>$picture->likes,
										'fname' =>$picture->fname,
										'already' =>$already);
		}
			
		$data['count_pictures'] = count($data['pictures']);	
		$owners_id = $data['album']->vehicle_id;
		
		$my_vehicles = $this->car_model->getVehicles($this->session->userdata('user_id'))->result();
		
		$data['can_edit'] = false;
		$data['pretty'] = "";
		foreach($my_vehicles as $vehicle)
		{
			if($vehicle->id == $owners_id)
			{
				$data['can_edit'] = true;
				$data['pretty'] = $vehicle->pretty_url;
			}
		}
		
		
		
			
		$this->loadDefaultView('statics/album', $data);		
	}
	
	public function add_album()
	{
		$title = $_POST['title'];
		$vehicle_id = $_POST['cid'];

		$about = "";
		$new_album_id = $this->statics_model->addAlbum($title,$about,$vehicle_id);

			$image_id = $this->statics_model->getAlbumImage($new_album_id);
			
			$album = $this->statics_model->getAlbumData($new_album_id);
			
			$fname = $this->statics_model->getImageByID($image_id)->row()->fname;
			$item = array('id' => $album->id,
						  'title' => $album->title,
						  'fname' => $fname);
	
		echo json_encode($item);
		
		
			
			
	}
	
	public function newsfeedUpload()
	{
		$this->checkLogin();
		
	}
	
	
	public function edit_album_info()
	{
		//validate form input
		$this->form_validation->set_rules('album_title', $this->lang->line('album_new_title'), 'required');
		$album_id = $this->input->post('album_id');

		if ($this->form_validation->run() == true)
		{
			$title = $this->input->post('album_title');
			$about = $this->input->post('album_about');
						$data = array(
						'id' => $album_id,
						'title' => $title,
						'about' => $about);
						
			$this->statics_model->editAlbum($data);
			redirect('edit_album/'.$album_id);
		}
		else
		{
			//Validation error
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			redirect('edit_album/'.$album_id);
		}	
			
	}
	
	
	
	
	
	public function add_photo()
	{
		
		$user_id = $this->session->userdata('user_id');
		$album_id = $_POST['aid'];
		$vehicle_id = $_POST['cid'];
		$output_dir = "items/uploads/images/";

		if(isset($_FILES["myfile"]))
		{
			$ret = array();
			$action_id = 8;
			$error =$_FILES["myfile"]["error"];				
	
		    	if(!is_array($_FILES["myfile"]['name'])) //single file
		    	{
		       	 	$fileName = $_FILES["myfile"]["name"];
		       	 	$this->load->library('upload');
		       	
		       	 	$uploadconfig['upload_path'] = 'items/uploads/images/';
					$uploadconfig['max_size'] = '10000000000';
					$uploadconfig['allowed_types'] = 'jpg|png|jpeg';
					$uploadconfig['encrypt_name'] = true;
						
					
					$this->upload->initialize($uploadconfig);
					
					if ( ! $this->upload->do_upload('myfile'))
					{
						//var_dump($this->upload->display_errors());
						 
						// uploading failed. $error will holds the errors.
					}
					
					
					$data = $this->upload->data();
					
					$this->load->library('image_moo');
					$fname = 'items/uploads/images/'.$data['file_name'];
					
					//FIX IMAGE ROTATION 
					
					
					 $exif = exif_read_data($fname);
	
			         $ort = $exif['Orientation'];
						
		            switch($ort)
		            {
		
		                case 3: // 180 rotate left
		                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
		                    break;
		                case 6: // 90 rotate right
		                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
		                    break;			
		                case 8:    // 90 rotate left
		                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
		                    break;
						default:{};
		            }
					
					
					if($data['image_width']>1000 || $data['image_height']>1000)
					{

						$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
						
					}
		       	 		$fname_fb = 'items/uploads/images/fb_'.$data['file_name'];
						$this->image_moo->load($fname)->resize(400,400)->save($fname_fb,true);
		       	 	
						$fname_thumb = 'items/uploads/images/thumb__'.$data['file_name'];
						$this->image_moo->load($fname)->resize(380,440)->save($fname_thumb,true);
		       	 			       	 	 	
			       	 	 $ret[$fileName]= $output_dir.$data['file_name'];

						 
			       	 	 $entity_id = $this->statics_model->addPicture($data['file_name'],$album_id,$this->session->userdata('user_id'),0,0,$vehicle_id);
			       	 	 $event_id = $this->statics_model->addEvent($action_id, $entity_id);
						
				   	 	 $this->statics_model->addEventVehicle($vehicle_id,$event_id);
						
		    	}
		    	else
		    	{
		    		
		    	    	$fileCount = count($_FILES["myfile"]['name']);
		    		  for($i=0; $i < $fileCount; $i++)
		    		  {
		    		  	$fileName = $_FILES["myfile"]["name"][$i];
			       	 	 $ret[$fileName]= $output_dir.$fileName;
		    		    move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName );
		    		     
		    		     $entity_id = $this->statics_model->addPicture($fileName,$album_id,$this->session->userdata('user_id'),0,0,$vehicle_id);
						 $event_id = $this->statics_model->addEvent($action_id, $entity_id);
						 $this->statics_model->addEventVehicle($vehicle_id,$event_id);	
						 
		    		  }
		    	
		    	}
		 }
		    echo $entity_id;

	}
	
	
	public function add_photo_newsfeed()
	{
		
		$user_id = $this->session->userdata('user_id');
		$vehicle_id = $this->session->userdata('vehicle_id');
		$newsfeed_album = $this->statics_model->getNewsfeedAlbum($vehicle_id);
		if(!$newsfeed_album)
		{
			$newsfeed_album_id = $this->statics_model->addAlbumNewsfeed("Newsfeed uploads","",$vehicle_id);
		}
		else 
		{
			$newsfeed_album_id = $newsfeed_album->row()->id;
		}
		$output_dir = "items/uploads/images/";
		
	
		if(isset($_FILES["myfile"]))
		{
			$ret = array();
			$action_id = 8;
			$error =$_FILES["myfile"]["error"];				
				$album_id = $newsfeed_album_id;
		    	if(!is_array($_FILES["myfile"]['name'])) //single file
		    	{
		       	 	$fileName = $_FILES["myfile"]["name"];
		       	 	$this->load->library('upload');
		       	
		       	 	$uploadconfig['upload_path'] = 'items/uploads/images/';
					$uploadconfig['max_size'] = '10000000000';
					$uploadconfig['allowed_types'] = 'jpg|png|jpeg';
					$uploadconfig['encrypt_name'] = true;
						
					
					$this->upload->initialize($uploadconfig);
					
					if ( ! $this->upload->do_upload('myfile'))
					{
						//var_dump($this->upload->display_errors());
						 
						// uploading failed. $error will holds the errors.
					}
					
					
					$data = $this->upload->data();
					
					$this->load->library('image_moo');
					$fname = 'items/uploads/images/'.$data['file_name'];
					
					//FIX IMAGE ROTATION 
					
					
					 $exif = exif_read_data($fname);
	
			         $ort = $exif['Orientation'];
						
		            switch($ort)
		            {
		
		                case 3: // 180 rotate left
		                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
		                    break;
		                case 6: // 90 rotate right
		                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
		                    break;			
		                case 8:    // 90 rotate left
		                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
		                    break;
						default:{};
		            }
					
					
					if($data['image_width']>1000 || $data['image_height']>1000)
					{

						$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
						
					}
		       	 		$fname_fb = 'items/uploads/images/fb_'.$data['file_name'];
						$this->image_moo->load($fname)->resize(400,400)->save($fname_fb,true);
		       	 	
						$fname_thumb = 'items/uploads/images/thumb__'.$data['file_name'];
						$this->image_moo->load($fname)->resize(380,440)->save($fname_thumb,true);
		       	 			       	 	 	
			       	 	 $ret[$fileName]= $output_dir.$data['file_name'];

						 $title = urldecode($this->session->userdata('image_text'));
			       	 	 $entity_id = $this->statics_model->addPicture($data['file_name'],$album_id,$this->session->userdata('user_id'),0,0,$vehicle_id,$title);
			       	 	 $event_id = $this->statics_model->addEvent($action_id, $entity_id);
						
				   	 	 $this->statics_model->addEventVehicle($vehicle_id,$event_id);
						
		    	}
		    	else
		    	{
		    		
		    	    	$fileCount = count($_FILES["myfile"]['name']);
		    		  for($i=0; $i < $fileCount; $i++)
		    		  {
		    		  	$fileName = $_FILES["myfile"]["name"][$i];
			       	 	 $ret[$fileName]= $output_dir.$fileName;
		    		    move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName );
		    		     
		    		     $entity_id = $this->statics_model->addPicture($fileName,$album_id,$this->session->userdata('user_id'),0,0,$vehicle_id, $title);
						 $event_id = $this->statics_model->addEvent($action_id, $entity_id);
						 $this->statics_model->addEventVehicle($vehicle_id,$event_id);	
						 
		    		  }
		    	
		    	}
		 }

	}
	
	
	
	public function add_photo_album()
	{
		$album_id = $this->uri->segment(3);
		
		$user_id = $this->session->userdata('user_id');
		$vehicle_id = $this->session->userdata('vehicle_id');
		
		$output_dir = "items/uploads/images/";

		if(isset($_FILES["myfile"]))
		{
			$ret = array();
			$action_id = 8;
			$error =$_FILES["myfile"]["error"];				
				
		    	if(!is_array($_FILES["myfile"]['name'])) //single file
		    	{
		       	 	$fileName = $_FILES["myfile"]["name"];
		       	 	$this->load->library('upload');
		       	
		       	 	$uploadconfig['upload_path'] = 'items/uploads/images/';
					$uploadconfig['max_size'] = '10000000000';
					$uploadconfig['allowed_types'] = 'jpg|png|jpeg';
					$uploadconfig['encrypt_name'] = true;
						
					
					$this->upload->initialize($uploadconfig);
					
					if ( ! $this->upload->do_upload('myfile'))
					{
						//var_dump($this->upload->display_errors());
						 
						// uploading failed. $error will holds the errors.
					}
					
					
					$data = $this->upload->data();
					
					$this->load->library('image_moo');
					$fname = 'items/uploads/images/'.$data['file_name'];
					
					//FIX IMAGE ROTATION 
					
					
					 $exif = exif_read_data($fname);
	
			         $ort = $exif['Orientation'];
						
		            switch($ort)
		            {
		
		                case 3: // 180 rotate left
		                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
		                    break;
		                case 6: // 90 rotate right
		                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
		                    break;			
		                case 8:    // 90 rotate left
		                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
		                    break;
						default:{};
		            }
					
					
					if($data['image_width']>1000 || $data['image_height']>1000)
					{

						$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
						
					}
		       	 		$fname_fb = 'items/uploads/images/fb_'.$data['file_name'];
						$this->image_moo->load($fname)->resize(400,400)->save($fname_fb,true);
		       	 	
						$fname_thumb = 'items/uploads/images/thumb__'.$data['file_name'];
						$this->image_moo->load($fname)->resize(380,440)->save($fname_thumb,true);
		       	 			       	 	 	
			       	 	 $ret[$fileName]= $output_dir.$data['file_name'];

						 
			       	 	 $entity_id = $this->statics_model->addPicture($data['file_name'],$album_id,$user_id,0,0,$vehicle_id);
			       	 	 $event_id = $this->statics_model->addEvent($action_id, $entity_id);
						
				   	 	 $this->statics_model->addEventVehicle($vehicle_id,$event_id);
						
		    	}
		    	else
		    	{
		    		
		    	    	$fileCount = count($_FILES["myfile"]['name']);
		    		  for($i=0; $i < $fileCount; $i++)
		    		  {
		    		  	$fileName = $_FILES["myfile"]["name"][$i];
			       	 	 $ret[$fileName]= $output_dir.$fileName;
		    		    move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName );
		    		     
		    		     $entity_id = $this->statics_model->addPicture($fileName,$album_id,$user_id,0,0,$vehicle_id);
						 $event_id = $this->statics_model->addEvent($action_id, $entity_id);
						 $this->statics_model->addEventVehicle($vehicle_id,$event_id);	
						 
		    		  }
		    	
		    	}
		 }

	}
	
	
	
	public function add_photo_poi($poi_id)
	{
		
		$user_id = $this->session->userdata('user_id');
		$vehicle_id = $this->session->userdata('vehicle_id');
	
		
		$output_dir = "items/uploads/images/";

		if(isset($_FILES["myfile"]))
		{
			$ret = array();
			$error =$_FILES["myfile"]["error"];				
				
		    	if(!is_array($_FILES["myfile"]['name'])) //single file
		    	{
		       	 	$fileName = $_FILES["myfile"]["name"];
		       	 	$this->load->library('upload');
		       	
		       	 	$uploadconfig['upload_path'] = 'items/uploads/images/';
					$uploadconfig['max_size'] = '10000000000';
					$uploadconfig['allowed_types'] = 'jpg|png|jpeg';
					$uploadconfig['encrypt_name'] = true;
						
					
					$this->upload->initialize($uploadconfig);
					
					if ( ! $this->upload->do_upload('myfile'))
					{
						//var_dump($this->upload->display_errors());
						 
						// uploading failed. $error will holds the errors.
					}
					
					
					$data = $this->upload->data();
					
					$this->load->library('image_moo');
					$fname = 'items/uploads/images/'.$data['file_name'];
					
					//FIX IMAGE ROTATION 
					
					
					 $exif = exif_read_data($fname);
	
			         $ort = $exif['Orientation'];
						
		            switch($ort)
		            {
		
		                case 3: // 180 rotate left
		                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
		                    break;
		                case 6: // 90 rotate right
		                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
		                    break;			
		                case 8:    // 90 rotate left
		                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
		                    break;
						default:{};
		            }
					
					
					if($data['image_width']>1000 || $data['image_height']>1000)
					{

						$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
						
					}
		       	 		
		       	 			       	 	 	
			       	 	 $ret[$fileName]= $output_dir.$data['file_name'];

						 
			       	 	 $this->statics_model->addPOIPicture($data['file_name'],$poi_id,$vehicle_id);
			       	 	 
						
		    	}
		    	
		 }
	
	}
	
	
	
	
	
	
	public function add_photo_newsfeed_ie()
	{
		
		$user_id = $this->session->userdata('user_id');
		$newsfeed_album = $this->statics_model->getNewsfeedAlbum($user_id);
		if(!$newsfeed_album)
		{
			$newsfeed_album_id = $this->statics_model->addAlbumNewsfeed("Newsfeed uploads","",$user_id);
		}
		else 
		{
			$newsfeed_album_id = $newsfeed_album->row()->id;
		}
		
		
			
		$output_dir = "items/uploads/images/";
		
		if(isset($_FILES["myfile"]))
		{
			$ret = array();
			$album_id = $newsfeed_album_id;
			$action_id = 8;
			$error =$_FILES["myfile"]["error"];				
			$dogs = $this->dog_model->getDogs($user_id);
			
		 
		    
		    	if(!is_array($_FILES["myfile"]['name'])) //single file
		    	{
		       	 	$fileName = $_FILES["myfile"]["name"];
		       	 	$this->load->library('upload');
		       	 	
		       	 	$uploadconfig['upload_path'] = 'items/uploads/images/';
					$uploadconfig['max_size'] = '10000000000';
					$uploadconfig['allowed_types'] = 'jpg|png|jpeg';
					$uploadconfig['encrypt_name'] = true;
						
					
					$this->upload->initialize($uploadconfig);
					
					if ( ! $this->upload->do_upload('myfile'))
					{
						
						 
						// uploading failed. $error will holds the errors.
					}
					
					
					$data = $this->upload->data();
					
					$this->load->library('image_moo');
					$fname = 'items/uploads/images/'.$data['file_name'];
					
					//FIX IMAGE ROTATION 
					
					
					 $exif = exif_read_data($fname);
	
			         $ort = $exif['Orientation'];
						
		            switch($ort)
		            {
		
		                case 3: // 180 rotate left
		                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
		                    break;
		                case 6: // 90 rotate right
		                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
		                    break;			
		                case 8:    // 90 rotate left
		                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
		                    break;
						default:{};
		            }
					
					
					if($data['image_width']>1000 || $data['image_height']>1000)
					{

						$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
						
					}
		       	 		$fname_fb = 'items/uploads/images/fb_'.$data['file_name'];
						$this->image_moo->load($fname)->resize(400,400)->save($fname_fb,true);
		       	 	
						$fname_thumb = 'items/uploads/images/thumb__'.$data['file_name'];
						$this->image_moo->load($fname)->resize(380,440)->save($fname_thumb,true);
		       	 			       	 	 	
			       	 	 $ret[$fileName]= $output_dir.$data['file_name'];
			       	 	 $entity_id = $this->statics_model->addPicture($data['file_name'],$album_id,$this->session->userdata('user_id'));
			       	 	 $event_id = $this->statics_model->addEvent($action_id, $entity_id);
						 foreach($dogs->result() as $dog){
						 	$mydog_id = $dog->id;
							 $this->statics_model->addEventDog($mydog_id,$event_id);
						 }	
		    	}
		    	else
		    	{
		    	    	$fileCount = count($_FILES["myfile"]['name']);
		    		  for($i=0; $i < $fileCount; $i++)
		    		  {
		    		  	$fileName = $_FILES["myfile"]["name"][$i];
			       	 	 $ret[$fileName]= $output_dir.$fileName;
		    		    move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName );
		    		     
		    		     $entity_id = $this->statics_model->addPicture($fileName,$album_id,$this->session->userdata('user_id'));
						 $event_id = $this->statics_model->addEvent($action_id, $entity_id);
						 foreach($dogs->result() as $dog){
						 	$mydog_id = $dog->id;
							 $this->statics_model->addEventDog($mydog_id,$event_id);
						 }	
		    		  }
		    	
		    	}
		    }
		  
		  
		  redirect('');
	}
	
	
	public function add_photo_albums_ie()
	{
		
		$user_id = $this->session->userdata('user_id');
		$newsfeed_album = $this->statics_model->getNewsfeedAlbum($user_id);
		if(!$newsfeed_album)
		{
			$newsfeed_album_id = $this->statics_model->addAlbumNewsfeed("Newsfeed uploads","",$user_id);
		}
		else 
		{
			$newsfeed_album_id = $newsfeed_album->row()->id;
		}
		
		
			
		$output_dir = "items/uploads/images/";
		
		if(isset($_FILES["myfile"]))
		{
			$ret = array();
			$album_id = $newsfeed_album_id;
			$action_id = 8;
			$error =$_FILES["myfile"]["error"];				
			$dogs = $this->dog_model->getDogs($user_id);
			
		 
		    
		    	if(!is_array($_FILES["myfile"]['name'])) //single file
		    	{
		       	 	$fileName = $_FILES["myfile"]["name"];
		       	 	$this->load->library('upload');
		       	 	
		       	 	$uploadconfig['upload_path'] = 'items/uploads/images/';
					$uploadconfig['max_size'] = '10000000000';
					$uploadconfig['allowed_types'] = 'jpg|png|jpeg';
					$uploadconfig['encrypt_name'] = true;
						
					
					$this->upload->initialize($uploadconfig);
					
					if ( ! $this->upload->do_upload('myfile'))
					{
						var_dump($this->upload->display_errors());
						 
						// uploading failed. $error will holds the errors.
					}
					
					
					$data = $this->upload->data();
					
					$this->load->library('image_moo');
					$fname = 'items/uploads/images/'.$data['file_name'];
					
					//FIX IMAGE ROTATION 
					
					
					 $exif = exif_read_data($fname);
	
			         $ort = $exif['Orientation'];
						
		            switch($ort)
		            {
		
		                case 3: // 180 rotate left
		                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
		                    break;
		                case 6: // 90 rotate right
		                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
		                    break;			
		                case 8:    // 90 rotate left
		                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
		                    break;
						default:{};
		            }
					
					
					if($data['image_width']>1000 || $data['image_height']>1000)
					{

						$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
						
					}
		       	 		$fname_fb = 'items/uploads/images/fb_'.$data['file_name'];
						$this->image_moo->load($fname)->resize(400,400)->save($fname_fb,true);
		       	 	
						$fname_thumb = 'items/uploads/images/thumb__'.$data['file_name'];
						$this->image_moo->load($fname)->resize(380,440)->save($fname_thumb,true);
		       	 			       	 	 	
			       	 	 $ret[$fileName]= $output_dir.$data['file_name'];
			       	 	 $entity_id = $this->statics_model->addPicture($data['file_name'],$album_id,$this->session->userdata('user_id'));
			       	 	 $event_id = $this->statics_model->addEvent($action_id, $entity_id);
						 foreach($dogs->result() as $dog){
						 	$mydog_id = $dog->id;
							 $this->statics_model->addEventDog($mydog_id,$event_id);
						 }	
		    	}
		    	else
		    	{
		    	    	$fileCount = count($_FILES["myfile"]['name']);
		    		  for($i=0; $i < $fileCount; $i++)
		    		  {
		    		  	$fileName = $_FILES["myfile"]["name"][$i];
			       	 	 $ret[$fileName]= $output_dir.$fileName;
		    		    move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName );
		    		     
		    		     $entity_id = $this->statics_model->addPicture($fileName,$album_id,$this->session->userdata('user_id'));
						 $event_id = $this->statics_model->addEvent($action_id, $entity_id);
						 foreach($dogs->result() as $dog){
						 	$mydog_id = $dog->id;
							 $this->statics_model->addEventDog($mydog_id,$event_id);
						 }	
		    		  }
		    	
		    	}
		    }
		  
		  
		  redirect('albums');
		 
		

		
		
	}
	
	
	public function add_photo_to_album_ie()
	{
		
		$user_id = $this->session->userdata('user_id');
		$album_id = $_POST['album_id'];
		
		
			
		$output_dir = "items/uploads/images/";
		
		if(isset($_FILES["myfile"]))
		{
			$ret = array();
			
			$action_id = 8;
			$error =$_FILES["myfile"]["error"];				
			$dogs = $this->dog_model->getDogs($user_id);
			
		 
		    
		    	if(!is_array($_FILES["myfile"]['name'])) //single file
		    	{
		       	 	$fileName = $_FILES["myfile"]["name"];
		       	 	$this->load->library('upload');
		       	 	
		       	 	$uploadconfig['upload_path'] = 'items/uploads/images/';
					$uploadconfig['max_size'] = '10000000000';
					$uploadconfig['allowed_types'] = 'jpg|png|jpeg';
					$uploadconfig['encrypt_name'] = true;
						
					
					$this->upload->initialize($uploadconfig);
					
					if ( ! $this->upload->do_upload('myfile'))
					{
						var_dump($this->upload->display_errors());
						 
						// uploading failed. $error will holds the errors.
					}
					
					
					$data = $this->upload->data();
					
					$this->load->library('image_moo');
					$fname = 'items/uploads/images/'.$data['file_name'];
					
					//FIX IMAGE ROTATION 
					
					
					 $exif = exif_read_data($fname);
	
			         $ort = $exif['Orientation'];
						
		            switch($ort)
		            {
		
		                case 3: // 180 rotate left
		                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
		                    break;
		                case 6: // 90 rotate right
		                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
		                    break;			
		                case 8:    // 90 rotate left
		                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
		                    break;
						default:{};
		            }
					
					
					if($data['image_width']>1000 || $data['image_height']>1000)
					{

						$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
						
					}
		       	 		$fname_fb = 'items/uploads/images/fb_'.$data['file_name'];
						$this->image_moo->load($fname)->resize(400,400)->save($fname_fb,true);
		       	 	
						$fname_thumb = 'items/uploads/images/thumb__'.$data['file_name'];
						$this->image_moo->load($fname)->resize(380,440)->save($fname_thumb,true);
		       	 			       	 	 	
			       	 	 $ret[$fileName]= $output_dir.$data['file_name'];
			       	 	 $entity_id = $this->statics_model->addPicture($data['file_name'],$album_id,$this->session->userdata('user_id'));
			       	 	 $event_id = $this->statics_model->addEvent($action_id, $entity_id);
						 foreach($dogs->result() as $dog){
						 	$mydog_id = $dog->id;
							 $this->statics_model->addEventDog($mydog_id,$event_id);
						 }	
		    	}
		    	else
		    	{
		    	    	$fileCount = count($_FILES["myfile"]['name']);
		    		  for($i=0; $i < $fileCount; $i++)
		    		  {
		    		  	$fileName = $_FILES["myfile"]["name"][$i];
			       	 	 $ret[$fileName]= $output_dir.$fileName;
		    		    move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName );
		    		     
		    		     $entity_id = $this->statics_model->addPicture($fileName,$album_id,$this->session->userdata('user_id'));
						 $event_id = $this->statics_model->addEvent($action_id, $entity_id);
						 foreach($dogs->result() as $dog){
						 	$mydog_id = $dog->id;
							 $this->statics_model->addEventDog($mydog_id,$event_id);
						 }	
		    		  }
		    	
		    	}
		    }
		  
		  
		  redirect('edit_album/'.$album_id);
		 
		

		
		
	}
	
	
	public function activities()
	{
		$this->checkLogin();
		$data['user_id'] = $this->session->userdata('user_id');
		$activities = $this->statics_model->getActivities($this->session->userdata('user_id'))->result();
		
		$pending_activities_data = array();
		$activity_ids = array();
		$my_dogs = $this->dog_model->getDogs($this->session->userdata('user_id'))->result();
		
		foreach($my_dogs as $dog){
			
			$pending_result = $this->statics_model->getPendingActivities($dog->id)->result();
			
			foreach($pending_result as $pending)
			{
				$time = $pending->time;
				$time = date('H:i',strtotime($time));
				$date = $pending->date;
				$about = $pending->about;
				$place = $pending->place;
				$user_result = $this->statics_model->getUserData($pending->created_by);
				$name = $user_result->row()->first_name." ".$user_result->row()->last_name;
				if(!in_array($pending->id, $activity_ids)){
					$pending_activities_data[] = array('id' => $pending->id,
									  'place' => $place,
									  'about' => $about,
									  'time' => $time,
									  'date' => $date,
									  'created_by' => $name);
									  
					$activity_ids[] = $pending->id;
				}
			}
	
		}
		
		$data['pending_activities'] = $pending_activities_data;
		
		
		$activities_data = array();
		$accepted_ids = array();
		foreach($my_dogs as $dog){
			
			$accepted_result = $this->statics_model->getAcceptedActivities($dog->id)->result();
			
			foreach($accepted_result as $accepted)
			{
				$time = $accepted->time;
				$time = date('H:i',strtotime($time));
				$date = $accepted->date;
				$about = $accepted->about;
				$place = $accepted->place;
				$user_result = $this->statics_model->getUserData($accepted->created_by);
				$name = $user_result->row()->first_name." ".$user_result->row()->last_name;
				if(!in_array($accepted->id, $accepted_ids)){
					$activities_data[] = array('id' => $accepted->id,
									  'place' => $place,
									  'about' => $about,
									  'time' => $time,
									  'date' => $date,
									  'created_by' => $name);
					$accepted_ids[] = $accepted->id;
				}
			}
		
		}
		
		foreach($activities as $activity)
			{
				
				$time = $activity->time;
				$time = date('H:i',strtotime($time));
				$date = $activity->date;
				$about = $activity->about;
				$place = $activity->place;
				$user_result = $this->statics_model->getUserData($activity->created_by);
				$name = $user_result->row()->first_name." ".$user_result->row()->last_name;
				if($user_result->row()->id == $this->session->userdata('user_id'))
					{
						$same_user = 1;
					}
					else 
					{
						$same_user = 0;
					}
				if(!in_array($activity->id, $accepted_ids)){
					$activities_data[] = array('id' => $activity->id,
									  'place' => $place,
									  'about' => $about,
									  'time' => $time,
									  'date' => $date,
									   'same_user' => $same_user,
									  'created_by' => $name);
					$accepted_ids[] = $activity->id;
				}
			}
		
			
		$data['activities'] = $activities_data;
				
		$this->loadDefaultView('statics/activities', $data);	
			
	}
	
	public function add_activity()
	{
		//validate form input
		$this->form_validation->set_rules('activity_place', $this->lang->line('new_activity_place'), 'required');
		$this->form_validation->set_rules('register_birthday_day', $this->lang->line('create_user_validation_birthday_day_label'), 'xss_clean|required');
		$this->form_validation->set_rules('register_birthday_month', $this->lang->line('create_user_validation_birthday_month_label'), 'xss_clean|required');
		$this->form_validation->set_rules('register_birthday_year', $this->lang->line('create_user_validation_birthday_year_label'), 'xss_clean|required');
	
		if ($this->form_validation->run() == true)
		{
			$place = $this->input->post('activity_place');
			$about = $this->input->post('activity_about');
			$time = $this->input->post('activity_time');
			$date = $this->input->post('activity_date');
			
			$birthday_day = $this->input->post('register_birthday_day') != "" ? $this->input->post('register_birthday_day') : null;
			$birthday_month = $this->input->post('register_birthday_month') != "" ? $this->input->post('register_birthday_month') : null; 
			$birthday_year = $this->input->post('register_birthday_year') != "" ? $this->input->post('register_birthday_year') : null;  
			$birthday = $birthday_year."-".$birthday_month."-".$birthday_day;
			$date = date("Y-m-d", strtotime($birthday));
			
			$last_id = $this->statics_model->addActivity($place,$date,$time,$about,$this->session->userdata('user_id'));
			redirect('activity/'.$last_id);
		}
		else
		{
			$data['user_id'] = $this->session->userdata('user_id');
			$activities = $this->statics_model->getActivities($this->session->userdata('user_id'))->result();
			
			$pending_activities_data = array();
			$activity_ids = array();
			$my_dogs = $this->dog_model->getDogs($this->session->userdata('user_id'))->result();
			
			foreach($my_dogs as $dog){
				
				$pending_result = $this->statics_model->getPendingActivities($dog->id)->result();
				
				foreach($pending_result as $pending)
				{
					$time = $pending->time;
					$time = date('H:i',strtotime($time));
					$date = $pending->date;
					$about = $pending->about;
					$place = $pending->place;
					$user_result = $this->statics_model->getUserData($pending->created_by);
					$name = $user_result->row()->first_name." ".$user_result->row()->last_name;
					if(!in_array($pending->id, $activity_ids)){
						$pending_activities_data[] = array('id' => $pending->id,
										  'place' => $place,
										  'about' => $about,
										  'time' => $time,
										  'date' => $date,
										  'created_by' => $name);
										  
						$activity_ids[] = $pending->id;
					}
				}
		
			}
			
			$data['pending_activities'] = $pending_activities_data;
			
			
			$activities_data = array();
			$accepted_ids = array();
			foreach($my_dogs as $dog){
				
				$accepted_result = $this->statics_model->getAcceptedActivities($dog->id)->result();
				
				foreach($accepted_result as $accepted)
				{
					$time = $accepted->time;
					$time = date('H:i',strtotime($time));
					$date = $accepted->date;
					$about = $accepted->about;
					$place = $accepted->place;
					$user_result = $this->statics_model->getUserData($accepted->created_by);
					$name = $user_result->row()->first_name." ".$user_result->row()->last_name;
					
					
					
					if(!in_array($accepted->id, $accepted_ids)){
						$activities_data[] = array('id' => $accepted->id,
										  'place' => $place,
										  'about' => $about,
										  'time' => $time,
										  'date' => $date,
										  'created_by' => $name);
						$accepted_ids[] = $accepted->id;
					}
				}
			
			}
			
			foreach($activities as $activity)
				{
					
					$time = $activity->time;
					$time = date('H:i',strtotime($time));
					$date = $activity->date;
					$about = $activity->about;
					$place = $activity->place;
					$user_result = $this->statics_model->getUserData($activity->created_by);
					$name = $user_result->row()->first_name." ".$user_result->row()->last_name;
					
					
					
					if(!in_array($activity->id, $accepted_ids)){
						$activities_data[] = array('id' => $activity->id,
										  'place' => $place,
										  'about' => $about,
										  'time' => $time,
										  'date' => $date,									  
										  'created_by' => $name);
						$accepted_ids[] = $activity->id;
					}
				}
			
				
			$data['activities'] = $activities_data;
		
		
		
			//Validation error
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->loadDefaultView('statics/activities', $data);
		}	
			
	}
	
	public function activity($activity_id)
	{
		$this->checkLogin();
		$activity = $this->statics_model->getActivity($activity_id);
		$guests_data = array();
		
		$data['user_id'] = $this->session->userdata('user_id');
		$data['created_by'] = $activity->created_by;
		$data['time'] = date('H:i',strtotime($activity->time));
		$data['date'] = date("Y M d",strtotime($activity->date));
		$data['about'] = $activity->about;
		$data['place'] = $activity->place;
		
		
		$data['guests'] = array();
		
		$guests = $this->statics_model->getGuestsForActivity($activity_id)->result();
		
		foreach($guests as $friend){
				$friendName = $friend->name;
				$friendPic = $friend->profile_picture;
				$friendId = $friend->id;
				
				if(is_numeric($friendPic))
				{
					$dogprofile = $this->statics_model->getImageByID($friendPic)->row()->fname;
				}
				else
				{
					$dogprofile = $friendPic;
				}
				
		
				$data['guests'][] = array('id' => $friendId,
									'name' => $friendName,
									'picture' => $dogprofile,
									'pending' => $friend->pending);
					
				
			}
		
		
		
		
		
		
		$data['pending_count'] = 0;
		$data['accepted_count'] = 0;
		foreach($guests as $guest)
		{
			if($guest->pending == 1)
			{
				$data['pending_count'] += 1;
			}
			else
			{
				$data['accepted_count'] += 1;
			}
		}
		$data['activity_id'] = $activity_id;
		
		
		$myDogs = $this->dog_model->getDogs($this->session->userdata('user_id'))->result();		
		$friend_dogs = array();
		$added_friends = $this->dog_model->checkActivity($activity_id)->result();
		$dog_ids = array();
		
		foreach($added_friends as $dog)
		{
			if(!in_array($dog->dog_id, $dog_ids)){
									
					$dog_ids[] = $dog->dog_id;											
				}
		}
		

		foreach($myDogs as $dog){
			$friends = $this->dog_model->getFriends($dog->id)->result();
			foreach($friends as $friend){
				$friendName = $friend->name;
				$friendPic = $friend->profile_picture;
				$friendId = $friend->id;
				
				if(is_numeric($friendPic))
				{
					$dogprofile = $this->statics_model->getImageByID($friendPic)->row()->fname;
				}
				else
				{
					$dogprofile = $friendPic;
				}
				
				if(!in_array($friendId, $dog_ids)){
					$friend_dogs[]=array('id' => $friendId,
									'name' => $friendName,
									'picture' => $dogprofile);
					
					$dog_ids[] = $friendId;											
				}
			}
		}
		$data['friends']  = $friend_dogs;
		
		
		$this->loadDefaultView('statics/activity', $data);		
	}
	
	public function edit_activity($activity_id)
	{
		$this->checkLogin();
		$data['activity_id'] = $activity_id;
		$activity = $this->statics_model->getActivityForEdit($activity_id,$this->session->userdata('user_id'));
		$guests_data = array();	
		$data['time'] = date('H:i',strtotime($activity->time));
		$birthday = $activity->date;
		
		if($birthday!=NULL){
			$arr = explode("-",$birthday);	
			$data['year'] = $arr[0];
			$data['month'] = $arr[1];
			$data['day'] = $arr[2];
		}	
		
		
		
		$data['about'] = $activity->about;
		$data['place'] = $activity->place;
		$myDogs = $this->dog_model->getDogs($this->session->userdata('user_id'))->result();		
		$friend_dogs = array();
		$added_friends = $this->dog_model->checkActivity($activity_id)->result();
		$dog_ids = array();
		
		foreach($added_friends as $dog)
		{
			if(!in_array($dog->dog_id, $dog_ids)){
									
					$dog_ids[] = $dog->dog_id;											
				}
		}
		

		foreach($myDogs as $dog){
			$friends = $this->dog_model->getFriends($dog->id)->result();
			foreach($friends as $friend){
				$friendName = $friend->name;
				$friendPic = $friend->profile_picture;
				$friendId = $friend->id;
				
				
				if(!in_array($friendId, $dog_ids)){
					$friend_dogs[]=array('id' => $friendId,
									'name' => $friendName,
									'picture' => $friendPic);
					
					$dog_ids[] = $friendId;											
				}
			}
		}
		$data['guests']  = $this->statics_model->getGuestsForActivity($activity_id)->result();
		$data['friends']  = $friend_dogs;
			
		$this->loadDefaultView('statics/edit_activity', $data);		
	}
	
	
	public function update_activity($activity_id)
	{
		$data['activity_id'] = $activity_id;
		$activity = $this->statics_model->getActivityForEdit($activity_id,$this->session->userdata('user_id'));
		$guests_data = array();	
		$data['time'] = date('H:i',strtotime($activity->time));
		$birthday = $activity->date;
		
		if($birthday!=NULL){
			$arr = explode("-",$birthday);	
			$data['year'] = $arr[0];
			$data['month'] = $arr[1];
			$data['day'] = $arr[2];
		}	
		
		
		
		$data['about'] = $activity->about;
		$data['place'] = $activity->place;
		$myDogs = $this->dog_model->getDogs($this->session->userdata('user_id'))->result();		
		$friend_dogs = array();
		$added_friends = $this->dog_model->checkActivity($activity_id)->result();
		$dog_ids = array();
		

		foreach($myDogs as $dog){
			$friends = $this->dog_model->getFriends($dog->id)->result();
			foreach($friends as $friend){
				$friendName = $friend->name;
				$friendPic = $friend->profile_picture;
				$friendId = $friend->id;
				
				
				if(!in_array($friendId, $dog_ids)){
					$friend_dogs[]=array('id' => $friendId,
									'name' => $friendName,
									'picture' => $friendPic);
					
					$dog_ids[] = $friendId;											
				}
			}
		}
		$data['guests']  = $this->statics_model->getGuestsForActivity($activity_id)->result();
		$data['friends']  = $friend_dogs;
	
	
	
	
	
		//validate form input
		$this->form_validation->set_rules('register_birthday_day', $this->lang->line('create_user_validation_birthday_day_label'), 'xss_clean|required');
		$this->form_validation->set_rules('register_birthday_month', $this->lang->line('create_user_validation_birthday_month_label'), 'xss_clean|required');
		$this->form_validation->set_rules('register_birthday_year', $this->lang->line('create_user_validation_birthday_year_label'), 'xss_clean|required');	
		$this->form_validation->set_rules('activity_place', $this->lang->line('new_activity_place'), 'required');

		if ($this->form_validation->run() == true)
		{
			$place = $this->input->post('activity_place');
			$about = $this->input->post('activity_about');
			$time = $this->input->post('activity_time');
			$birthday_day = $this->input->post('register_birthday_day') != "" ? $this->input->post('register_birthday_day') : null;
			$birthday_month = $this->input->post('register_birthday_month') != "" ? $this->input->post('register_birthday_month') : null; 
			$birthday_year = $this->input->post('register_birthday_year') != "" ? $this->input->post('register_birthday_year') : null;  
			$birthday = $birthday_year."-".$birthday_month."-".$birthday_day;
			$date = date("Y-m-d", strtotime($birthday));
			
			$data = array(
						'id' => $activity_id,
						'place' => $place,
						'date' => $date,
						'time' => $time,
						'about' => $about);
						
			$this->statics_model->editActivity($data);
			
			redirect('edit_activity/'.$activity_id);
		}
		else
		{
			//Validation error
			
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->loadDefaultView('statics/edit_activity', $data);
		}	
	
	}


	public function addActivityGuest(){
		$dog_id = $_POST['dog_id'];
		$activity_id = $_POST['activity_id'];
		$this->statics_model->addActivityGuest($dog_id,$activity_id);
	}
	
	
	public function accept_activity()
	{	
		$myDogs = $this->dog_model->getDogs($this->session->userdata('user_id'))->result();	
		foreach($myDogs as $dog){
			$myInvites = $this->statics_model->getMyInvites($_POST['activity_id'], $dog->id)->result();
			
			foreach($myInvites as $invite){
				$data = array(
					'id' =>  $invite->id,				
					'pending' => 0,
					'accepted' => 1);
					
				$this->statics_model->editInvites($data);
			}
			
			
		}
		

	}
	
	public function decline_activity()
	{	
		$myDogs = $this->dog_model->getDogs($this->session->userdata('user_id'))->result();	
		foreach($myDogs as $dog){
			$myInvites = $this->statics_model->getMyInvites($_POST['activity_id'], $dog->id)->result();			
			foreach($myInvites as $invite){
				$data = array(
					'id' =>  $invite->id,				
					'pending' => 0,
					'accepted' => 0);
					
				$this->statics_model->editInvites($data);
			}						
		}
	}
	
	
	public function addLike()
	{
		$pic_id = $_POST['pid'];
		
		$user_id = $this->session->userdata('user_id');	
		
		$check = $this->statics_model->checkPictureLike($pic_id, $user_id);	
		
		if($check == 0)
		{
			$this->statics_model->addLike($pic_id,$user_id);
			$vehicles = $this->car_model->getVehicles($user_id);
			
		/*	 $event_id = $this->statics_model->addEvent(13, $pic_id);
			 foreach($vehicles->result() as $vehicle){
			 	$myvehicle_id = $vehicle->id;
				 $this->statics_model->addEventVehicle($myvehicle_id,$event_id);
			 }
			 
			 */
			 
			$event_id = $this->statics_model->addNotification(1, $pic_id);
			$myvehicle_id = $this->session->userdata('vehicle_id');
		    $this->statics_model->addNotificationVehicle($myvehicle_id,$event_id);
			 
			$pic = $this->statics_model->getPicture($pic_id)->row();
			 
			$vehicles = $this->car_model->getVehicles($pic->created_by);
			 
			foreach($vehicles->result() as $vehicle){
				//ADD GAME POINTS
				$this->addPoints($vehicle->id,1,'likes', 'like');
			 }
			
			 echo "OK";
		}
		else
		{
			echo "FAIL";
		}
		
	}
	
	
	public function addEventLike()
	{
		$the_event_id = $_POST['eid'];
		
		$user_id = $this->session->userdata('user_id');	
		
		$check = $this->statics_model->checkEventLike($the_event_id, $user_id);	
		
		if($check == 0)
		{
			$this->statics_model->addEventLike($the_event_id,$user_id);
			/*$event_id = $this->statics_model->addNotification(12, $the_event_id);
			$myvehicle_id = $this->session->userdata('vehicle_id');
		    $this->statics_model->addNotificationVehicle($myvehicle_id,$event_id);
			 */
			 
			$vehicles = $this->car_model->getVehiclesForEvent($the_event_id);
			 
			foreach($vehicles->result() as $vehicle){
				//ADD GAME POINTS
				$this->addPoints($vehicle->id,1,'likes', 'like');
			 }
			
			 echo "OK";
		}
		else
		{
			echo "FAIL";
		}
		
	}
	
	
	public function suggest_zone()
	{
		//validate form input
		$this->form_validation->set_rules('zone_name', $this->lang->line('suggest_zone_name'), 'required');
		$this->form_validation->set_rules('zone_address', $this->lang->line('suggest_zone_address'), 'required');

		if ($this->form_validation->run() == true)
		{
			$name = $this->input->post('zone_name');
			$address = $this->input->post('zone_address');
			$about = $this->input->post('zone_description');
			$size = $this->input->post('zone_size');
						
			$this->statics_model->addSuggestion($name,$address,$about,$size, $this->session->userdata('user_id'));
			$this->session->set_flashdata('message', $this->lang->line('suggestion_sent'));
			redirect('zones/a');
		}
		else
		{
			//Validation error
			$this->session->set_flashdata('message', (validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
			redirect('zones/a');
		}	
	
	}
	
	
	
	public function get_zones()
	{
		$zones = $this->statics_model->getZones()->result();
		echo json_encode($zones);
	}
	
	public function get_sitters()
	{
		$zones = $this->statics_model->getSitters()->result();
		echo json_encode($zones);
	}
	
	public function get_vets()
	{
		$zones = $this->statics_model->getVets()->result();
		echo json_encode($zones);
	}
	
	public function get_places()
	{
		$zones = $this->statics_model->getPlaces()->result();
		$places_container = array();
		foreach($zones as $place)
		{
			$type = $this->statics_model->getPlaceTypeByID($place->type)->row()->name;
			$place_item = array('id' => $place->id,
								'name' => $place->name,
								'pretty_url' => $place->pretty_url,
								'geo_location' => $place->geo_location,
								'type' => $type);
			$places_container[] = $place_item;
		}
		
		echo json_encode($places_container);
	}
	
	
	public function nearby_users()
	{
		$zones = $this->statics_model->nearbyUsers()->result();
		
		echo json_encode($zones);
	}
	
	
	public function territory()
	{
		$this->checkLogin();
		$activity_ids = array();
		$my_dogs = $this->dog_model->getDogs($this->session->userdata('user_id'))->result();
		
		foreach($my_dogs as $dog){
			
			$pending_result = $this->statics_model->getPendingActivities($dog->id)->result();
			
			foreach($pending_result as $result){
			
				if(!in_array($result->id, $activity_ids)){											  
					$activity_ids[] = $result->id;
				}
			}
	
		}
			
		
		$data['pending_count'] = count($activity_ids);	
		$this->loadDefaultView('statics/territory', $data);		
	}
	
	public function showHashtags(){
	
		$oldImages = $this->statics_model->getHashtags()->result();
		
		$image_container = array();
		
		
		foreach($oldImages as $i=>$image){			
			$image_container[] = array('link' => $image->target_url,
		    						'img_url' => $image->fname);
		}
		
		$data['pictures'][] = $image_container;
		$this->load->view('statics/hashtags', $data);
	
	}
	
	public function tagDog()
	{
		$picture_id = $_POST['pic_id'];
		$dog_id = $_POST['dog_id'];
		$event_id = $this->statics_model->addEvent(9, $picture_id);
		
		
		$this->statics_model->addEventDog($dog_id,$event_id);
		
		
		$this->dog_model->addTag($picture_id, $dog_id);
	}		
	
	public function searchName()
	{
		$name = $_POST['name'];
		$pic_id = $_POST['pic_id'];
		$dog = $this->dog_model->getDogByName($name,$pic_id)->result();
		shuffle($dog);
		
		$return = json_encode($dog);
		echo $return;
	}
	
	public function searchTagged()
	{
		$pic_id = $_POST['pic_id'];
		$dog = $this->dog_model->getDogsTagged($pic_id)->result();
		$tagged_dog = $this->dog_model->getDogsTagged($pic_id)->result();
		$tagged_ids = array();
		$not_tagged_dogs = array();
		foreach($tagged_dog as $tagged)
		{
			$tagged_ids[] = $tagged->dog_id;
		}
		foreach($dog as $friend)
		{
			if(!in_array($friend->id, $tagged_ids) )
			{
				$not_tagged_dogs[] = $friend;
			}
		}
		$return = json_encode($not_tagged_dogs);
		echo $return;
	}
	
	public function getFriendsNotTagged()
	{
		$pic_id = $_POST['pic_id'];
		$friends = $this->getFriends();
		$dog = $this->dog_model->getDogsTagged($pic_id)->result();
		$tagged_ids = array();
		$not_tagged_dogs = array();
		foreach($dog as $tagged)
		{
			$tagged_ids[] = $tagged->dog_id;
		}
		foreach($friends as $friend)
		{
			if(!in_array($friend['id'], $tagged_ids) )
			{
				$not_tagged_dogs[] = $friend;
			}
		}
		$return = json_encode($not_tagged_dogs);
		echo $return;
	}
	
	public function searchPlaceName()
	{
		$name = $_POST['name'];
		
		$vets = $this->statics_model->PlaceSearchByName($name)->result();
		shuffle($vets);
		$return = json_encode($vets);
		echo $return;
	}
	
	
	public function searchVetName()
	{
		$name = $_POST['name'];
		
		$vets = $this->statics_model->VetSearchByName($name)->result();
		shuffle($vets);
		$return = json_encode($vets);
		echo $return;
	}
	
	public function searchSitterName()
	{
		$name = $_POST['name'];
		
		$vets = $this->statics_model->SitterSearchByName($name)->result();
		shuffle($vets);	
		
		$return = json_encode($vets);
		
		echo $return;
	}
	
	
	public function searchZoneName()
	{
		$name = $_POST['name'];
		
		$vets = $this->statics_model->ZoneSearchByName($name)->result();
		shuffle($vets);	
		
		$return = json_encode($vets);
		
		echo $return;
	}
	
	public function dog_tagged_photos($dog_id)
	{
		$this->checkLogin();				
		$tagged_pictures = $this->statics_model->getTaggedPicture($dog_id)->result();
		
		$pictures_data = array();
		foreach($tagged_pictures as $picture)
		{
			$pic = $this->statics_model->getPicture($picture->image_id)->result();	
													
			$pictures_data[] = array('fname' => $pic[0]->fname,
									'id' => $pic[0]->id,
									'title' => $pic[0]->title);
		}	
		
		
		$data['pictures'] = $pictures_data;
		$data['dog'] = $this->dog_model->getDog($dog_id);	
		
		$this->loadDefaultView('statics/tagged_photos', $data);		
	}
	
	public function delete_profile_request()
	{
		$user_id = $this->session->userdata('user_id');
		$this->statics_model->addDeleteRequest($user_id);
		
	}
	
	
	public function partner()
	{
		$data = null;
		$data['message'] = "";
		if ($this->session->userdata('partner_id') !== FALSE) {

			$partner_id = $this->session->userdata('partner_id'); 
			$data['partner'] = $this->statics_model->getPartner($partner_id)->row();
			$data['advert'] = $this->statics_model->getPartnerAdvert($partner_id)->row();
			
			$impressions_remaining = $data['partner']->impressions_left;
			$conversion = $this->statics_model->getConversion()->row();
			$impression = $conversion->impressions;
			$price = $conversion->price;
			
			$data['remaining_amount'] = ($impressions_remaining/$impression)*$price;
			$data['impressions_remaining'] = $impressions_remaining;
			
			$this->loadDefaultView('statics/partner', $data);


			
		   
		}
		else{
			$this->form_validation->set_rules('login_username', $this->lang->line('partner_login_username'), 'required');
			$this->form_validation->set_rules('login_password', $this->lang->line('login_login_password'), 'required');
	
			if ($this->form_validation->run() == true)
			{
				$user = $this->input->post('login_username');
				$pass = $this->input->post('login_password');
				if(!$this->checkPartnerLogin($user,$pass))
				{
					$this->loadDefaultView('statics/partner_login', $data);
				}
				else
				{
					$partner_id = $this->session->userdata('partner_id'); 
					$data['partner'] = $this->statics_model->getPartner($partner_id)->row();
					$data['advert'] = $this->statics_model->getPartnerAdvert($partner_id)->row();
					
					$impressions_remaining = $data['partner']->impressions_left;
					
					$data['external'] = $data['advert']->external;
					
					
					
					$conversion = $this->statics_model->getConversion()->row();
					$impression = $conversion->impressions;
					$price = $conversion->price;
					
					$data['remaining_amount'] = ($impressions_remaining/$impression)*$price;
					
					
					$this->loadDefaultView('statics/partner', $data);
				}
			}
			else
			{
				$this->loadDefaultView('statics/partner_login', $data);	
			}		
		}		
			
		
				
	}
	
	public function logoutPartner()
	{
		$this->session->unset_userdata('partner_id');
		redirect('partner');
	}
	
	public function save_advert()
	{
		
		$config['upload_path'] = './items/uploads/partnerimages/';
		$config['allowed_types'] = 'jpg|png';
		$config['max_size']	= '';
		$config['max_width']  = '';
		$config['max_height']  = '';

		$this->load->library('upload', $config);
		
		$field_name = "advert_image";
		
		if ( ! $this->upload->do_upload($field_name))
		{
			$error = array('error' => $this->upload->display_errors());
						
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

					
			$link = $this->input->post('advert_link');
			$file = $data['upload_data'];
			$serverFile = "";
			$rnd = $this->rand_string(12);
			$filename = $file['file_name'];
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			$fileTypes = array('jpg', 'jpeg', 'png'); // Allowed file extensions
			$serverFile	= $filename;
				if (in_array(strtolower($ext), $fileTypes)) 
				{
				/*	$serverFile = time() . "_" .$rnd.".".$ext;
					$fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/fourlegsbook/items/uploads/partnerimages/' . $serverFile,'w'); 
					
					//Prepends timestamp to prevent overwriting
					fwrite($fp, base64_decode($data[1]));
					fclose($fp);*/
					
					if (strtolower($ext) == "jpg" || strtolower($ext) == "jpeg") 
					{
						$img = imagecreatefromjpeg($_SERVER['DOCUMENT_ROOT'] . '/items/uploads/partnerimages/' . $serverFile); 
						imagejpeg($img, $_SERVER['DOCUMENT_ROOT'] . '/items/uploads/partnerimages/' . $serverFile);
					}
					if (strtolower($ext) == "png") 
					{
						$img = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . '/items/uploads/partnerimages/' . $serverFile); 
						$serverFile = time() . "_" .$rnd."."."jpg";
						imagejpeg($img, $_SERVER['DOCUMENT_ROOT'] . '/items/uploads/partnerimages/' . $serverFile);
					}
					
					
					if(!$img)
					{
						$result['success'] = false;
						$result['msg'] = $this->lang->line('profile_uploadpic_error_upload');	
					} 
					else 
					{												
						
					}
				} 
				else 
				{
					$result['success'] = false;
					$result['msg'] = $this->lang->line('profile_uploadpic_wrongdatatype');					
				}
			
			
		}		
		
		$partner_id = $this->session->userdata('partner_id');

		$data = array('partner_id' => $partner_id,
						'image_fname' => $serverFile,
						'link' => $link);
						
		$this->statics_model->updateAdvert($data);
		redirect('partner');
	}
	
	public function bookings()
	{
		$user_id = $this->session->userdata('user_id');
		$data['user_sitter'] = $this->statics_model->getSitter($user_id)->row()->dogsitter;
		if($data['user_sitter'] == 1)
		{
			
			$jsonArray = array();
				
			$eventjson = $this->statics_model->getTimetableSitter($user_id)->result();
			
			foreach($eventjson as $json)
				{
					$timeframe_id = $json->id;
					$number_of_bookings = $this->statics_model->getBookingsForDate($timeframe_id);
					
					$places_left = $json->available_place - $number_of_bookings;
					
					
					$jsonArray[] = array('id' => $timeframe_id,
									  'sitter_id' => $json->sitter_id,
									  'places' => $places_left,
									  'date' => $json->date,
									  'description' => $json->message);
				}	
			$this->load->library('my_booking');
			
			$data['calendar'] = $this->my_booking->getSitterCalendar($user_id);
			$data['sitter_controls'] = $this->my_booking->renderSitterControls($user_id);
			$data['eventJSON'] = json_encode($jsonArray);
			$this->loadDefaultView('statics/bookings', $data);	
		}		
		else 
		{
			redirect('editprofile');
		}
	}
	
	public static function removeEmoji($text) {
      
          return preg_replace('/([0-9|#][\x{20E3}])|[\x{00ae}|\x{00a9}|\x{203C}|\x{2047}|\x{2048}|\x{2049}|\x{3030}|\x{303D}|\x{2139}|\x{2122}|\x{3297}|\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F6FF}][\x{FE00}-\x{FEFF}]?/u', '', $text);
       }  
	
	
	
	
	public function delete_album()
	{
		$album_id = $_POST['album_id'];
		
		
		$images = $this->statics_model->getPicturesFromAlbum($album_id)->result();
		
		foreach($images as $image)
		{
			
			
		
			$cover = $image->cover_image;
			$profile = $image->profile_image;
			
			$event_type = 8; //Regular image;
			
			if($cover == 1)
			{
				$event_type = 7; //Cover image;
			}
			else if($profile == 1)
			{
				$event_type = 6; //Cover image;
			}
			
			
			$event_id = $this->statics_model->getEventId($image->id, $event_type)->row()->id;
			
			$this->statics_model->deleteEventImages($image->id, $event_id, $event_type);
		}
		
		
		$this->statics_model->deleteAlbum($album_id);
	}
	
	public function delete_image()
	{
		$image_id = $_POST['pid'];
		
		$pic = $this->statics_model->getPicture($image_id)->row();
		
		$cover = $pic->cover_image;
		$profile = $pic->profile_image;
		
		$event_type = 8; //Regular image;
		
		if($cover == 1)
		{
			$event_type = 7; //Cover image;
			$event_id = $this->statics_model->getEventIdProfile($image_id,$event_type)->row()->id;
		}
		else if($profile == 1)
		{
			$event_type = 6; //Cover image;
			$event_id = $this->statics_model->getEventIdProfile($image_id,$event_type)->row()->id;
		}
		else
		{
			$event_id = $this->statics_model->getEventId($image_id,$event_type)->row()->id;
		}
		
		echo $event_id;
		$this->statics_model->deleteEventImages($image_id, $event_id, $event_type);
		
	}
	
	public function sample_test()
	{
		$data = array();
		$limit = 10;
		$this->checkLanguage();
		if(isset($_GET['offset']))
			$offset = mysql_real_escape_string($_GET['offset']);
		else
			$offset = 0;
			
		$data['whatsnew'] = array_filter($this->getNewest($limit, $offset));
		
		$instaOffset = $offset / 5;
		$data['hashtags'] = array_filter($this->hashtags(2, $instaOffset));
		$data['countHashtags'] = count($data['hashtags']);
		$data['user_hashtags'] = $this->statics_model->getSitter($this->session->userdata('user_id'))->row()->show_hashtags;
		if(isset($_GET['offset']))
		{
			$html = "";
			
			$html .= $this->load->view('frontend/newsfeed_dynamic', $data, true);
			
			echo json_encode(array('success' => true,
								   'message' => 'load_successful',
								   'html' => $html,
								   'new_offset' => $offset + $limit,
								   'image_count' => count($data['whatsnew'])));
			
		}
		
	}
	
	function ajax_upload($type = "images")
	{
		
		$this->load->library('upload');
	
		$user_id = $this->session->userdata('user_id');
	/*	$vehicle_id = $this->session->userdata('vehicle_id');
		$newsfeed_album = $this->statics_model->getNewsfeedAlbum($vehicle_id);
		
		if(!$newsfeed_album)
		{
			$newsfeed_album_id = $this->statics_model->addAlbumNewsfeed("Uploads","",$vehicle_id);
		}
		else 
		{
			$newsfeed_album_id = $newsfeed_album->row()->id;
		}
		*/
		// if file to upload is selected upload
		if($_FILES['file']['size'] != 0 && $_FILES['file']['error'] == 0)	
		{
			
			$uploadconfig['upload_path'] = 'items/uploads/'.$type;
			$uploadconfig['allowed_types'] = 'gif|jpg|png|jpeg';
			$uploadconfig['encrypt_name'] = true;	
			
			$this->upload->initialize($uploadconfig);
			
			$this->upload->do_upload('file');
			
		
			
			$data = $this->upload->data();
			
			$error = array('error' => $this->upload->display_errors());
			
			$this->load->library('image_moo');
			$fname = 'items/uploads/'.$type.'/'.$data['file_name'];
				
				//FIX IMAGE ROTATION 
				$exif = exif_read_data($fname);
				$ort = $exif['Orientation'];
						
	            switch($ort)
	            {
	
	                case 3: // 180 rotate left
	                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
	                    break;
	                case 6: // 90 rotate right
	                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
	                    break;			
	                case 8:    // 90 rotate left
	                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
	                    break;
					default:{};
	            }
			
			
			
			
			if($data['image_width']<1024 || $data['image_height']<1024)
			{
					
				$height = 1024/($data['image_width']/$data['image_height'])	;
				
				$this->image_moo->load($fname)->stretch(1024, $height)->save($fname,true);
				
				
				
			}
			
			
			
			if($data['image_width']>1024 || $data['image_height']>1024)
			{
				
				$this->image_moo->load($fname)->resize(1024,1024)->save($fname,true);
				
				
				
			}
			
			
			
			
			
			switch($type)
			{
				case 'images': 
				{
					$cover_image = 0;
					$profile_image = 0;
				};break;
				
				case 'coverpictures': 
				{
					$cover_image = 1;
					$profile_image = 0;
				};break;
				
				case 'profilepictures': 
				{
					$cover_image = 0;
					$profile_image = 1;
				};break;
			}
			
			//$image_id = $this->statics_model->addPicture($data['file_name'],$newsfeed_album_id,$this->session->userdata('user_id'), $cover_image, $profile_image);
			
			$ret_data['fname'] = $data['file_name'];
			//$ret_data['image_id'] = $image_id;
			echo json_encode($ret_data);
		}		
		else
		{
			echo "FAIL";
		}
		
	}
	
	function crop_image(){
	
		$x1=$_POST['x1'];
		$y1=$_POST['y1'];
		$x2=$_POST['x2'];
		$y2=$_POST['y2'];
		$w=$_POST['w'];
		$h=$_POST['h'];
		$fname=$_POST['fname'];
		
		$this->load->library('image_moo');

		 $this->image_moo->load($fname)->resize($w,$h)->crop($x1,$y1,$x2,$y2)->save($fname,true);
		var_dump($this->image_moo->display_errors());
		
		return true;
	}
	
	
	function save_coverpicture()
	{
		
		$entity_id = $_POST['car_id'];
		$fname = $_POST['fname'];
				
		$user_id = $this->session->userdata('user_id');
		$newsfeed_album = $this->statics_model->getNewsfeedAlbum($entity_id);
		if(!$newsfeed_album)
		{
			$newsfeed_album_id = $this->statics_model->addAlbumNewsfeed("Uploads","",$entity_id);
		}
		else 
		{
			$newsfeed_album_id = $newsfeed_album->row()->id;
		}
		
		$image_id = $this->statics_model->addPicture($fname,$newsfeed_album_id,$user_id, 1, 0);
			
		
				
		$event_id = $this->statics_model->addEvent("7", $entity_id, $image_id);		
		$this->statics_model->addEventVehicle($entity_id,$event_id);
		
	
		// prepare array for db insert
		$data = array('id' => $entity_id);
					
		$data['cover_image'] = $image_id;
			
			
		$this->car_model->editVehicle($data);
	}
	
	function save_profilepicture()
	{
		
		$entity_id = $_POST['car_id'];	
		$fname = $_POST['fname'];
		//$image_id = $_POST['image_id'];		
		$user_id = $this->session->userdata('user_id');
		$newsfeed_album = $this->statics_model->getNewsfeedAlbum($entity_id);
		if(!$newsfeed_album)
		{
			$newsfeed_album_id = $this->statics_model->addAlbumNewsfeed("Uploads","",$entity_id);
		}
		else 
		{
			$newsfeed_album_id = $newsfeed_album->row()->id;
		}
		
		$image_id = $this->statics_model->addPicture($fname,$newsfeed_album_id,$user_id, 0, 1, '');
		
		
		
		
						
		$event_id = $this->statics_model->addEvent("6", $entity_id, $image_id);		
		$this->statics_model->addEventVehicle($entity_id,$event_id);
		
		$image_data = array('profile_image' => 1);
		$this->statics_model->updateImage($image_id,$image_data);
	
		// prepare array for db insert
		$data = array('id' => $entity_id);
					
		$data['profile_image'] = $image_id;
			
			
		$this->car_model->editVehicle($data);
		
	}
	
	
	function save_profile_vet()
	{
		
		$id = $_POST['uid'];	
		$image = $_POST['fname'];					
		
	
		
					
		$data['profilepicture'] = $image;
			
			
		$this->auth_model->updateVet($id, $data);
	}
	
	
	function save_profilepicture_user()
	{
		
		$user_id = $_POST['user_id'];				
				
		// prepare array for db insert				
		$data['profile_picture'] = $_POST['fname'];
			
			
		$this->ion_auth->update($user_id, $data);
	}
	
	function suggest_zone_description()
	{
		$zone_id = $_POST['zone_id'];
		$description = $_POST['description'];
		
		$data = array('dogzone_id' => $zone_id,
						'description' => $description,
						'ip_address' => $this->input->ip_address());
						
		$this->statics_model->addZoneSuggestion($data);
	}
	
	public function deleteComment()
	{
		$comment_id = $_POST['cid'];
		if($this->session->userdata('user_id'))
		{
			$loggedin_user = $this->session->userdata('user_id');
			$saved_user_id = $this->statics_model->getComment($comment_id)->row()->user_id;
			
			if($loggedin_user == $saved_user_id)
			{
				$this->statics_model->deleteComment($comment_id);
			}
		}
		
		
	}
	
	function add_photo_comment()
	{
		$pic_id = $_POST['pid'];
		$comment = $_POST['comment'];
		$comment = strip_tags($comment);
		
		
		$data = array('image_id' => $pic_id,
						'vehicle_id' => $this->session->userdata('vehicle_id'),
						'comment' => $comment);
						
		
			
		$this->statics_model->addPhotoComment($data);
			
			
		$event = $this->statics_model->getNewsEventByPicId($image_id);
			
		if($event)
		{
	
			$event_id = $event->id;
			
			$data = array('event_id' => $event_id,
				'vehicle_id' => $this->session->userdata('vehicle_id'),
				'comment' => $comment);


			$this->statics_model->addNewsComment($data);
	
		}
		
		$vehicle = $this->car_model->getVehicle($this->session->userdata('vehicle_id'))->row();
		
		$profile = $this->statics_model->getImageByID($vehicle->profile_image)->row();
		
		$return = array('comment' => $comment,
						'nickname' => $vehicle->nickname,
						'profile_image' => $profile->fname);
		
		
		echo json_encode($return);
		
	}
	
	
	function get_photo_comments()
	{
		
		$event_id = $_POST['pid'];
		$data = array();
		$event_comments = $this->statics_model->getImageComments($event_id);
						
						
					
		foreach($event_comments as $comment)
		{
			$vehicle = $this->car_model->getVehicle($comment->vehicle_id)->row();

			$profile = $this->statics_model->getImageByID($vehicle->profile_image)->row();
			
			$new = array('comment' => $comment->comment,
							'nickname' => $vehicle->nickname,
							'profile_image' => $profile->fname);
							
						
						
			$data[] = $new;
		}
		
	
		
		echo json_encode($data);
		
	}
	
	
	function get_picture()
	{
		
		$pic_id = $_POST['pid'];
		$event_id = $_POST['eid'];
		$picture = $this->statics_model->getPicture($pic_id)->row();
		$comments = $this->statics_model->getEventComments($event_id);
		$already = $this->statics_model->checkPictureLike($picture->id, $this->session->userdata('user_id'));
			
		$data[] = array('pid' => $picture->id,
						'eid' => $event_id,
						'title' => $picture->title,
						'likes' => $picture->likes,
						'comments' => count($comments),
						'fname' => $picture->fname,
						'already' => $already);
		

		echo json_encode($data);
	}
	
	
	public function photo($photo_id)
	{
		$data['loggedin'] = false;
		$data['already'] = 0;
		if ($this->ion_auth->logged_in())
		{
			$data['loggedin'] = true;	
			$data['already'] = $this->statics_model->checkPictureLike($photo_id, $this->session->userdata('user_id'));
		}
		
		$picture = $this->statics_model->getPicture($photo_id)->row();
		$type = "images";
		if($picture->cover_image==1)
		{
			$type = "coverpictures";
		}
		else if($picture->profile_image==1)
		{
			$type = "profilepictures";
		}
		
		$data['picture'] = array('id' => $photo_id,
								'type' => $type,
								'likes' => $picture->likes,
								'fname' => $picture->fname,
								'title' => $picture->title);		
				
		$this->loadDefaultView('statics/image', $data);
			
			
	}
	
	
	public function my_instagram()
	{
		$user_id = $_POST['user_id'];
		
		$userdata = $this->statics_model->getUserData($user_id)->row();
		
		if($userdata->import_instagram == 1 && $userdata->user_hashtag != "")
		{
			$this->import_instagram($user_id);
		}
		
		
	}
	
	
	public function vet_interface()
	{
		$data = null;
		$data['message'] = "";
		if ($this->session->userdata('vet_id') !== FALSE) {
			
			
			$vet_id = $this->session->userdata('vet_id'); 
			$data['vet'] = $this->statics_model->getVet($vet_id)->row();
			
			
			$this->loadDefaultView('statics/vet_interface', $data);


			
		   
		}
		else{
			$this->form_validation->set_rules('login_username', $this->lang->line('vet_login_username'), 'required');
			$this->form_validation->set_rules('login_password', $this->lang->line('login_login_password'), 'required');
	
			if ($this->form_validation->run() == true)
			{
				$user = $this->input->post('login_username');
				$pass = $this->input->post('login_password');
				
				
				$checkcheck = $this->checkVetLogin($user,$pass);
				
				if(!$checkcheck)
				{
				
					$this->loadDefaultView('statics/vet_login', $data);
				}
				else
				{
					
					$vet_id = $this->session->userdata('vet_id'); 
					$data['vet'] = $this->statics_model->getVet($vet_id)->row();
										
										
					$this->loadDefaultView('statics/vet_interface', $data);
				}
			}
			else
			{
				
				$this->loadDefaultView('statics/vet_login', $data);	
			}		
		}		
			
		
				
	}
	
	
	public function add_cover_ie($type = "coverpictures")
	{
		$this->load->library('upload');
		$dog_id = $_POST['dog_id'];
		$user_id = $this->session->userdata('user_id');
		$newsfeed_album = $this->statics_model->getNewsfeedAlbum($user_id);
		if(!$newsfeed_album)
		{
			$newsfeed_album_id = $this->statics_model->addAlbumNewsfeed("Newsfeed uploads","",$user_id);
		}
		else 
		{
			$newsfeed_album_id = $newsfeed_album->row()->id;
		}
		
		
		// if file to upload is selected upload
		if($_FILES['cover_image']['size'] != 0 && $_FILES['cover_image']['error'] == 0)	
		{
			$uploadconfig['upload_path'] = 'items/uploads/'.$type;
			$uploadconfig['allowed_types'] = 'gif|jpg|png|jpeg';
			$uploadconfig['encrypt_name'] = true;	
			
			$this->upload->initialize($uploadconfig);
			
			$this->upload->do_upload('cover_image');
			
			$data = $this->upload->data();
			
			$error = array('error' => $this->upload->display_errors());
			
			$this->load->library('image_moo');
			$fname = 'items/uploads/'.$type.'/'.$data['file_name'];
				
				//FIX IMAGE ROTATION 
				$exif = exif_read_data($fname);
				$ort = $exif['Orientation'];
						
	            switch($ort)
	            {
	
	                case 3: // 180 rotate left
	                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
	                    break;
	                case 6: // 90 rotate right
	                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
	                    break;			
	                case 8:    // 90 rotate left
	                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
	                    break;
					default:{};
	            }
			
			if($data['image_width']>1000 || $data['image_height']>1000)
			{
				
				$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
				
			}
			
			switch($type)
			{
				case 'images': 
				{
					$cover_image = 0;
					$profile_image = 0;
				};break;
				
				case 'coverpictures': 
				{
					$cover_image = 1;
					$profile_image = 0;
				};break;
				
				case 'profilepictures': 
				{
					$cover_image = 0;
					$profile_image = 1;
				};break;
			}
			
			
			
			$fname = $data['file_name'];
			
			
			redirect('edit_cover/'.$dog_id.'/'.$fname);
		}		
		else
		{
			redirect('edit_dog/'.$dog_id);
		}
		
	}
	
	
	public function add_dog_profile_ie($type = "profilepictures")
	{
		$this->load->library('upload');
		$dog_id = $_POST['dog_id'];
		$user_id = $this->session->userdata('user_id');
		$newsfeed_album = $this->statics_model->getNewsfeedAlbum($user_id);
		if(!$newsfeed_album)
		{
			$newsfeed_album_id = $this->statics_model->addAlbumNewsfeed("Newsfeed uploads","",$user_id);
		}
		else 
		{
			$newsfeed_album_id = $newsfeed_album->row()->id;
		}
		
		
		// if file to upload is selected upload
		if($_FILES['profile_image']['size'] != 0 && $_FILES['profile_image']['error'] == 0)	
		{
			$uploadconfig['upload_path'] = 'items/uploads/'.$type;
			$uploadconfig['allowed_types'] = 'gif|jpg|png|jpeg';
			$uploadconfig['encrypt_name'] = true;	
			
			$this->upload->initialize($uploadconfig);
			
			$this->upload->do_upload('profile_image');
			
			$data = $this->upload->data();
			
			$error = array('error' => $this->upload->display_errors());
			
			$this->load->library('image_moo');
			$fname = 'items/uploads/'.$type.'/'.$data['file_name'];
				
				//FIX IMAGE ROTATION 
				$exif = exif_read_data($fname);
				$ort = $exif['Orientation'];
						
	            switch($ort)
	            {
	
	                case 3: // 180 rotate left
	                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
	                    break;
	                case 6: // 90 rotate right
	                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
	                    break;			
	                case 8:    // 90 rotate left
	                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
	                    break;
					default:{};
	            }
			
			if($data['image_width']>1000 || $data['image_height']>1000)
			{
				
				$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
				
			}
			
			switch($type)
			{
				case 'images': 
				{
					$cover_image = 0;
					$profile_image = 0;
				};break;
				
				case 'coverpictures': 
				{
					$cover_image = 1;
					$profile_image = 0;
				};break;
				
				case 'profilepictures': 
				{
					$cover_image = 0;
					$profile_image = 1;
				};break;
			}
			
			
			
			$fname = $data['file_name'];
			
			
			redirect('edit_dogprofile/'.$dog_id.'/'.$fname);
		}		
		else
		{
			redirect('edit_dog/'.$dog_id);
		}
		
	}
	
	public function add_profile_ie($type = "profilepictures")
	{
		$this->load->library('upload');
		$user_id = $this->session->userdata('user_id');
		
		
		
		// if file to upload is selected upload
		if($_FILES['profile_image']['size'] != 0 && $_FILES['profile_image']['error'] == 0)	
		{
			$uploadconfig['upload_path'] = 'items/uploads/'.$type;
			$uploadconfig['allowed_types'] = 'gif|jpg|png|jpeg';
			$uploadconfig['encrypt_name'] = true;	
			
			$this->upload->initialize($uploadconfig);
			
			$this->upload->do_upload('profile_image');
			
			$data = $this->upload->data();
			
			$error = array('error' => $this->upload->display_errors());
			
			$this->load->library('image_moo');
			$fname = 'items/uploads/'.$type.'/'.$data['file_name'];
				
				//FIX IMAGE ROTATION 
				$exif = exif_read_data($fname);
				$ort = $exif['Orientation'];
						
	            switch($ort)
	            {
	
	                case 3: // 180 rotate left
	                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
	                    break;
	                case 6: // 90 rotate right
	                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
	                    break;			
	                case 8:    // 90 rotate left
	                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
	                    break;
					default:{};
	            }
			
			if($data['image_width']>1000 || $data['image_height']>1000)
			{
				
				$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
				
			}
			
			switch($type)
			{
				case 'images': 
				{
					$cover_image = 0;
					$profile_image = 0;
				};break;
				
				case 'coverpictures': 
				{
					$cover_image = 1;
					$profile_image = 0;
				};break;
				
				case 'profilepictures': 
				{
					$cover_image = 0;
					$profile_image = 1;
				};break;
			}
			
			
			
			$fname = $data['file_name'];
			
			
			redirect('editprofile/'.$fname);
		}		
		else
		{
			redirect('editprofile');
		}
		
	}
	
	
	public function add_traffic_alert()
	{
		$alert = $_POST['text'];
		$vehicle_id = $this->session->userdata('vehicle_id');
		$entity_id = $this->statics_model->addAlert($alert,$vehicle_id);
		
		$action_id = 15; // traffic alert
		$event_id = $this->statics_model->addEvent($action_id, $entity_id);
		$this->statics_model->addEventVehicle($vehicle_id,$event_id);	
	}
	
	
	public function add_status_update()
	{
		$alert = $_POST['text'];
		$vehicle_id = $this->session->userdata('vehicle_id');
		$entity_id = $this->statics_model->addStatus($alert,$vehicle_id);
		
		$action_id = 16; // status update
		$event_id = $this->statics_model->addEvent($action_id, $entity_id);
		$this->statics_model->addEventVehicle($vehicle_id,$event_id);	
	}
	
	
	public function VehicleSearchByName()
	{
		$search_string = $_POST['search'];
		
		$return = $this->statics_model->VehicleSearchByName($search_string)->result();
		$data = array();
		
		foreach($return as $item)
		{
			$profile = $this->statics_model->getImageByID($item->profile_image)->row()->fname;
			$array_item = array('id' => $item->id,
								'name' => $item->nickname,	
								'profile' => $profile);
			$data[] = $array_item;
		}
		
		echo json_encode($data);
	}
	
	
	
	public function VehicleSearchByMake()
	{
		$search_string = $_POST['search'];
		$data = array();
		
		
		
		$table = 'car_models';
		$column = 'make';
		$return = $this->statics_model->VehicleSearchByMake($search_string, $table, $column)->result();
		foreach($return as $item)
		{
			$array_item = $item->make;
			
			if(!in_array($item->make, $data))
			{
				$data[] = $array_item;
			}
			
		}
				
		$table = 'motorcycle_models';
		$column = 'make';
		$return = $this->statics_model->VehicleSearchByMake($search_string, $table, $column)->result();
		foreach($return as $item)
		{
			$array_item = $item->make;
			
			if(!in_array($item->make, $data))
			{
				$data[] = $array_item;
			}
			
		}
				
		$table = 'motorcycle_models';
		$column = 'make';
		$return = $this->statics_model->VehicleSearchByMake($search_string, $table, $column)->result();
		foreach($return as $item)
		{
			$array_item = $item->make;
			
			if(!in_array($item->make, $data))
			{
				$data[] = $array_item;
			}
			
		}
		
		echo json_encode($data);
	}
	
	
	
	public function VehicleSearchByModel()
	{
		$search_string = $_POST['search'];
		$data = array();
		
		
		
		$table = 'car_models';
		$column = 'model';
		$return = $this->statics_model->VehicleSearchByMake($search_string, $table, $column)->result();
		foreach($return as $item)
		{
			$array_item = $item->model;
			
			if(!in_array($item->model, $data))
			{
				$data[] = $array_item;
			}
			
		}
				
		$table = 'motorcycle_models';
		$column = 'model';
		$return = $this->statics_model->VehicleSearchByMake($search_string, $table, $column)->result();
		foreach($return as $item)
		{
			$array_item = $item->model;
			
			if(!in_array($item->model, $data))
			{
				$data[] = $array_item;
			}
			
		}
				
		$table = 'motorcycle_models';
		$column = 'model';
		$return = $this->statics_model->VehicleSearchByMake($search_string, $table, $column)->result();
		foreach($return as $item)
		{
			$array_item = $item->model;
			
			if(!in_array($item->model, $data))
			{
				$data[] = $array_item;
			}
			
		}
		
		echo json_encode($data);
	}

	
	
	
	/********* MESSAGING **********/
	
	public function UserSearchByName()
	{
		$search_string = $_POST['search'];
		
		$return = $this->statics_model->UserSearchByName($search_string)->result();
		$data = array();
		
		foreach($return as $item)
		{
			$array_item = array('id' => $item->id,
								'name' => $item->first_name." ".$item->last_name,	
								'profile' => $item->profile_picture);
			$data[] = $array_item;
		}
		
		echo json_encode($data);
	}
	
	
	
	public function messages()
	{
		$this->checkLogin();
		$data = null;	
		
		$messages = $this->statics_model->getMessages($this->session->userdata('user_id'));
		
		$data['messages'] = array();
		$userlist = array();
		$data['new_message_counter'] = 0;
		if($messages != false)
		{
			foreach($messages->result() as $message)
			{
				if($message->has_seen == 0 && $message->sender_id != $this->session->userdata('user_id'))
				{
					$data['new_message_counter']++;
				}
				
				$message_text = $message->message;
				if( strlen( $message_text) > 40) {
				    $str = explode( "\n", wordwrap( $message_text, 40));
				    $message_text = $str[0] . '...';
				}
				
				$sender = $this->statics_model->getUserData($message->sender_id)->row();
				$receiver = $this->statics_model->getUserData($message->receiver_id)->row();
				
				if($sender->id != $this->session->userdata('user_id'))
				{
					$name = $sender->first_name." ".$sender->last_name;
					$profile = $sender->profile_picture;
					$partner_id = $sender->id;
				}
				else 
				{
					$name = $receiver->first_name." ".$receiver->last_name;
					$profile = $receiver->profile_picture;
					$partner_id = $receiver->id;
				}
				
				$default_timezone = get_cookie('timezoneoffset');
			        if($default_timezone == null)
			        	date_default_timezone_set('Europe/Vienna');
			        else 
			            date_default_timezone_set($default_timezone);

				
				$today = new DateTime('today'); // This object represents current date/time

				$match_date = new DateTime($message->sent_time);
				$match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
				
				$diff = $today->diff( $match_date );
				$diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval
				
				$thedate =  DateTime::CreateFromFormat('m-d-Y h:i A', $this->set_user_timezone($message->sent_time));
				
				if($diffDays >= 0)
				{
					$thedate = $thedate->format('H:i');
				}
				else
				{
					$thedate = $thedate->format('M d');
				}
				
				
				if($profile === NULL || $profile == "")
				{
					$profile = "default.png";
				}
			
				$message_item = array('id' => $message->id,
										'partner_id' => $partner_id,
										'message' => $message_text,
										'name' => $name,
										'date' => $thedate,
										'profile' => $profile,
										'id' => $message->id);
				if(!in_array($sender->id, $userlist) && !in_array($receiver->id, $userlist) )
				{
					$userlist[] = $partner_id;
					$data['messages'][] = $message_item;
				}						
				
			}
		}
		
		$this->loadDefaultView('statics/messages', $data);		
	}
	
	
	public function sendMessageOwner()
	{
		$this->checkLogin();
		
		$message = strip_tags($_POST['message']);
		$car_id = $_POST['car_id'];
		$sender_id = $this->session->userdata('user_id');
		
		date_default_timezone_set('Europe/Vienna');		
		$now = date('Y-m-d H:i:s', time());
		
		$receiver_id = $this->car_model->getVehicle($car_id)->row()->user_id;
			
		$data = array('sender_id' =>$sender_id,
						'receiver_id' =>$receiver_id,
						'message' =>$message,
						'sent_time' =>$now);
		
		
					
		$this->statics_model->sendMessage($data);
		
		$vehicles = $this->car_model->getVehicles($receiver_id);
		
		$action_id = 2; // message
		$event_id = $this->statics_model->addNotification($action_id, $sender_id, $receiver_id);
		foreach($vehicles->result() as $vehicle){
		 	$myvehicle_id = $vehicle->id;
			 $this->statics_model->addNotificationVehicle($myvehicle_id,$event_id);
		 }
		
		
	
		echo json_encode($data);
	}
	
	
	
	
	public function sendMessage()
	{
		$this->checkLogin();
		
		$message = strip_tags($_POST['message']);
		$receiver_id = $_POST['rec_id'];
		$sender_id = $this->session->userdata('user_id');
		
		date_default_timezone_set('Europe/Vienna');		
		$now = date('Y-m-d H:i:s', time());
		
			
		$data = array('sender_id' =>$sender_id,
						'receiver_id' =>$receiver_id,
						'message' =>$message,
						'sent_time' =>$now);
		
		
					
		$this->statics_model->sendMessage($data);
		
		$vehicles = $this->car_model->getVehicles($receiver_id);
		
		$action_id = 2; // message
		$event_id = $this->statics_model->addNotification($action_id, $sender_id, $receiver_id);
		foreach($vehicles->result() as $vehicle){
		 	$myvehicle_id = $vehicle->id;
			 $this->statics_model->addNotificationVehicle($myvehicle_id,$event_id);
		 }
		
		
	
		echo json_encode($data);
	}
	
	
	public function updateMessageSeen()
	{
		$sender_id = $_POST['sid'];
		
		$data = array('sender_id' => $sender_id,
						'has_seen' => 1);
		
		$this->statics_model->editMessage($data);
	}
	
	
	
	public function getMessagesFromUser()
	{
		$partner_id = $_POST['pid'];
		$user_id = $this->session->userdata('user_id');
		
		$messages = $this->statics_model->getMessagesFromUser($partner_id, $user_id)->result();
		
		foreach($messages as $message)
		{
			
			
			$sender = $this->statics_model->getUserData($message->sender_id)->row();
			$receiver = $this->statics_model->getUserData($message->receiver_id)->row();
			$name = $sender->first_name." ".$sender->last_name;
			$profile = $sender->profile_picture;
			
			
			
			if($sender->id != $this->session->userdata('user_id'))
			{
				
				$own_message = false;
			}
			else 
			{
				
				$own_message = true;
			}
			
			$default_timezone = get_cookie('timezoneoffset');
	        if($default_timezone == null)
	        	date_default_timezone_set('Europe/Vienna');
	        else 
	            date_default_timezone_set($default_timezone);
	            
	          
			$today = new DateTime('today'); // This object represents current date/time

				$match_date = new DateTime($message->sent_time);
				
				$match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
			
				$diff = $today->diff( $match_date );
				$diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval
				
				$thedate =  DateTime::CreateFromFormat('m-d-Y h:i A', $this->set_user_timezone($message->sent_time));

				if($diffDays >= 0)
				{
					$thedate = $thedate->format('H:i');
					
				}
				else
				{		
					$thedate = $thedate->format('M d');
				}
			
			
			
				$message_item = array('id' => $message->id,
									'own_message' => $own_message,
									'message' => $message->message,
									'name' => $name,
									'date' => $thedate,
									'profile' => $profile);
										
				$data['messages'][] = $message_item;
		}
		
		echo json_encode($data['messages']);
	}
	
	
	
	/* NOTIFICATIONS */
	
	public function updateNotificationSeen()
	{
		$not_id = $_POST['sid'];
		
		$data = array('id' => $not_id,
						'has_seen' => 1);
		
		$this->statics_model->editNotification($data);
	}

	
	
	public function notifications()
	{
		//$this->checkLogin();
		$data = null;	
		
		$vehicles = $this->car_model->getVehicles($this->session->userdata('user_id'));
		$friends_container = array();
		foreach($vehicles->result() as $vehicle){
			$friends_container[] = $vehicle->id;
		}
		
		$whatsnew_container = array();
		$event_ids = array();
		$events = $this->statics_model->getAllNotifications()->result();
		
		$data['new_message_counter'] = 0;
		
		if(!empty($friends_container)){
		
			foreach($events as $event){		
			//	$result = $this->statics_model->getEvents($friend_id);
	
				$vehicle = $this->car_model->getVehicle($event->vehicle_id)->row();
				
				if(in_array($event->vehicle_id, $friends_container))
				{
			
					$vehiclename = $vehicle->nickname;
					$vehicleprofile = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
					$vehicle_cover = $this->statics_model->getImageByID($vehicle->cover_image)->row()->fname;
					$pretty_url = $vehicle->pretty_url;
								
					$event_action = $event->action_id;
					
					$event_entity =$event->entity_id;				
					$event_additional_id = $event->additional_id;
					$event_seen = $event->has_seen;
					
					$default_timezone = get_cookie('timezoneoffset');
			        if($default_timezone == null)
			        	date_default_timezone_set('Europe/Vienna');
			        else 
			            date_default_timezone_set($default_timezone);


					
					$thedate =  DateTime::CreateFromFormat('m-d-Y h:i A', $this->set_user_timezone($message->sent_time));
					
					if($diffDays >= 0)
					{
						$thedate = $thedate->format('H:i');
					}
					else
					{
						$thedate = $thedate->format('M d');
					}
					
					
					
					
					
					$today = new DateTime('today'); // This object represents current date/time
					
					$match_date = new DateTime($event->created_date);
					
					$match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
					
					$diff = $today->diff( $match_date );
					$diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval
					
					
					$thedate =  DateTime::CreateFromFormat('m-d-Y h:i A', $this->set_user_timezone($event->created_date));
					
					if($diffDays >= 0)
					{
						$thedate = $thedate->format('H:i');
					}
					else
					{
						$thedate = $thedate->format('M d');
					}

					
					$event_date = $thedate;
					
					
					$image_title = "";
					
					
					
					switch($event_action){
						case 1:{ //image like
							$user = $this->statics_model->getUserData($vehicle->user_id)->row();
							$pretty = "";
							$text = "Liked your image";
							$event_text = "";
							$entity_content ="";
							$entity_link = "photo/".$event->entity_id;
							$entity_name = $user->first_name." ".$user->last_name;
							$sender_link = "profile/".$user->username;
							$the_image = $user->profile_picture;
							$event_type = "image_like";
							
						};break;
						case 2:{ //message
							$user = $this->statics_model->getUserData($event->entity_id)->row();
							
							$text = "Sent you a message";
							$event_text = "";
							$sender_link = "profile/".$user->username;
							$entity_link = "messages";
							$entity_name = $user->first_name." ".$user->last_name;
							$event_picture = "";
							$the_image = $user->profile_picture;
							$event_type = "message";
						};break;
						case 3:{ //image comment
							
							$user = $this->statics_model->getUserData($vehicle->user_id)->row();
							$text = "Commented your image";
							$event_text = "";
							$sender_link = "profile/".$user->username;
							$entity_link = "photo/".$event->entity_id;
							$entity_name = $user->first_name." ".$user->last_name;							
							$event_picture = "";
							$the_image = $user->profile_picture;
							$event_type = "image_comment";
						};break;
						case 4:{ //vehicle comment
							$user = $this->statics_model->getUserData($vehicle->user_id)->row();
							$text = "Commented your vehicle";
							$event_text = "";
							$sender_link = "profile/".$user->username;
							$entity_link = "vehicle_profile/".$vehicle->pretty_url;
							$entity_name = $user->first_name." ".$user->last_name;
							$event_picture = "";
							$the_image = $user->profile_picture;
							$event_type = "vehicle_comment";
						};break;
						case 5:{ //group event created
							$user = $this->statics_model->getUserData($vehicle->user_id)->row();
							$text = 'group event';
							$event_text = 'new group event';
							$sender_link ="";
							
							$entity_link = "".$event_entity;
						
							$entity_name = '';
							$event_picture = "";
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "group_event_created";
							
						};break;
						
						case 6:{ //group invite
							$user = $this->car_model->getVehicle($event->entity_id)->row();
							
							$pic = $this->statics_model->getImageByID($user->profile_image)->row();
							
							$text = "Invited you to a club";
							$event_text = "";
							$sender_link = "vehicle_profile/".$user->pretty_url;
							$entity_link = "group/".$event->additional_id;
							$entity_name = $user->nickname;
							$event_picture = "";
							$the_image = $pic->fname;
							$event_type = "group_invite";
							
						};break;
						
						case 7:{ //user event invite
							$user = $this->car_model->getVehicle($event->entity_id)->row();
							
							$pic = $this->statics_model->getImageByID($user->profile_image)->row();
							
							$text = "Invited you to an event";
							$event_text = "";
							$sender_link = "vehicle_profile/".$user->pretty_url;
							$entity_link = "user_event/".$event->additional_id;
							$entity_name = $user->nickname;
							$event_picture = "";
							$the_image = $pic->fname;
							$event_type = "user_event_invite";
							
						};break;
						
						case 8:{ //group joined
							$user = $this->car_model->getVehicle($event->entity_id)->row();
							
							$pic = $this->statics_model->getImageByID($user->profile_image)->row();
							
							$text = "Joined club";
							$event_text = "";
							$sender_link = "vehicle_profile/".$user->pretty_url;
							$entity_link = "group/".$event->additional_id;
							$entity_name = $user->nickname;
							$event_picture = "";
							$the_image = $pic->fname;
							$event_type = "group_joined";
							
						};break;
						
						case 9:{ //user event joined
							$user = $this->car_model->getVehicle($event->entity_id)->row();
							
							$pic = $this->statics_model->getImageByID($user->profile_image)->row();
							
							$text = "Joined event";
							$event_text = "";
							$sender_link = "vehicle_profile/".$user->pretty_url;
							$entity_link = "user_event/".$event->additional_id;
							$entity_name = $user->nickname;
							$event_picture = "";
							$the_image = $pic->fname;
							$event_type = "user_event_joined";
							
						};break;
						
						case 10:{ //group event joined
							$user = $this->car_model->getVehicle($event->entity_id)->row();
							
							$pic = $this->statics_model->getImageByID($user->profile_image)->row();
							
							$text = "Joined event";
							$event_text = "";
							$sender_link = "vehicle_profile/".$user->pretty_url;
							$entity_link = "group_event/".$event->additional_id;
							$entity_name = $user->nickname;
							$event_picture = "";
							$the_image = $pic->fname;
							$event_type = "group_event_joined";
							
						};break;
						
						case 11:{ //group application
							$user = $this->car_model->getVehicle($event->entity_id)->row();							
							$pic = $this->statics_model->getImageByID($user->profile_image)->row();					
							$text = "Applied to club";
							$event_text = "";
							$sender_link = "vehicle_profile/".$user->pretty_url;
							$entity_link = "group/".$event->additional_id;
							$entity_name = $user->nickname;
							$event_picture = "";
							$the_image = $pic->fname;
							$event_type = "club_application";
							
						};break;
						
						
				/*		case 12:{ //like
							$user = $this->car_model->getVehicle($event->entity_id)->row();							
							$pic = $this->statics_model->getImageByID($user->profile_image)->row();					
							$text = "Liked";
							$event_text = "";
							$sender_link = "vehicle_profile/".$user->pretty_url;
							$entity_link = "";
							$entity_name = $user->nickname;
							$event_picture = "";
							$the_image = $pic->fname;
							$event_type = "event_like";
							
						};break;
						
				*/		
						case 13:{ //group event invite
							$user = $this->car_model->getVehicle($event->entity_id)->row();
							
							$pic = $this->statics_model->getImageByID($user->profile_image)->row();
							
							$text = "Invited you to an event";
							$event_text = "";
							$sender_link = "vehicle_profile/".$user->pretty_url;
							$entity_link = "group_event/".$event->additional_id;
							$entity_name = $user->nickname;
							$event_picture = "";
							$the_image = $pic->fname;
							$event_type = "group_event_invite";
							
						};break;
						
					}
						
						
					if(!in_array($event->id, $event_ids) && $user->id != $this->session->userdata('user_id')){
						
						if($event->has_seen == 0)
						{
							$data['new_message_counter']++;
						}
						
					
						
						$whatsnew_container[] = array('event_id' => $event->id,
											'text' => $text,
											'link' => $entity_link,
											'sender_link' => $sender_link,
											'vehiclename' => $vehiclename,
											'entity_name' => $entity_name,
											'event_text' => $event_text,
											'vehicle_id' => $event->vehicle_id,
											'pretty_url' => $pretty_url,
											'profile' => $the_image,
											'image_id' => $image_id,
											'event_image' => $event_picture,
											'event_seen' => $event->has_seen,
											'event_type' => $event_type,
											'created_date' => $event_date);
						$event_ids[] = $event->id;
						
						
						
					}
					
					
				}
			}
		}
		
		
		
		
		$data['notifications'] = $whatsnew_container;
		
		
				
		
		
		//return array_slice($whatsnew_container, $offset, $limit);
		echo $this->load->view('statics/notifications', $data, true);
				
	}
	
	
	
	
	
	
	public function register_group()
	{
		$name = $_POST['name'];
		$description = $_POST['description'];
		$category = $_POST['category'];
		$address = $_POST['address'];
		$geo_location = $_POST['geo_location'];
		
		$user_id = $this->session->userdata('user_id');
		$vehicle_id = $this->session->userdata('vehicle_id');
		
		
		
		$data = array('name' => $name,
					  'description' => $description,
					  'address' => $address,
					  'geo_location' => $geo_location,
					  'category_id' => $category,
					  'created_by' => $user_id,
					  'accepted' => 1,
					  'vehicle_id' => $vehicle_id);
		$gid = $this->statics_model->addGroup($data);
		
		
		$data = array('vehicle_id' => $vehicle_id,
					  'group_id' => $gid,
					  'role_id' => 1);
		
			
		$this->statics_model->addGroupUserRole($data);
		
		
		if($geo_location != '0, 0' && $category == 1)
		{
			$data = array('name' => $name,
						  'description' => $description,
						  'address' => $address,
						  'geo_location' => $geo_location,
						  'category_id' => 8);
						  
			$this->statics_model->addPOI($data);
		}
		
		
		//ADD GAME POINTS
		//$this->addPoints($vehicle_id,15,'groups_created', 'group created');
		
		
		echo $gid;
	}
	
	
	public function accept_new_group()
	{
		$rid = $_POST['rid'];
		$vehicle_id = $_POST['uid'];
		$data = array('id' => $rid,
					'accepted' => 1	);
					
		$this->statics_model->updateNewGroup($data);
		
		$role_data = array('group_id' => $rid,
							'vehicle_id' => $vehicle_id,
							'role_id' => GROUP_ADMIN);
							
		$this->statics_model->addGroupUserRole($role_data);
	}
	
	
	public function decline_new_group()
	{
		$rid = $_POST['rid'];
		$data = array('id' => $rid,
					'declined' => 1	);
					
		$this->statics_model->updateNewGroup($data);
	}
	
	
	
	public function group_search_name()
	{
		$search = $_POST['search'];
		$search = trim($search);
		
		
		$nearby = $_POST['nearby'];
		
		$users = (isset($_POST['users']))? $_POST['users'] : array();
		$group_array = array();
		
		
		$group_container = $this->statics_model->groupSearch($search)->result();
		
		
		
		
		
		foreach($group_container as $group){
				
				$members = $this->statics_model->getGroupMembers($group->id)->result();
				$category = $this->statics_model->getGroupCategory($group->category_id)->row()->name;								
				$group_array[] = array('id' => $group->id,	
								'name' => $group->name,
								'cover_image' => $group->cover_image,
								'logo' => $group->logo,
								'members' => count($members),
								'category' => $category,
								'description' => $group->description);
		}
								
			
		
		

		$data['groups'] = $group_array;
		$data['group_count'] = count($data['groups']);	
		
		if($data['group_count'] > 0)
		{
			return $this->load->view('statics/group_search_result', $data);
		}
		else 
		{
			return "NO";
		}		
	}
	
	
	
	public function group_detail($group_id)
	{
			
		$this->checkLogin();
		
		$group = $this->statics_model->getGroupById($group_id)->row();
		$data['group_geo'] = $group->geo_location;
		
		$members = $this->statics_model->getGroupMembers($group_id)->result();
					
		$data['members'] = array();
		
		foreach($members as $member)
		{
			$profile_image = $this->statics_model->getImageByID($member->profile_image)->row()->fname;
			
			$item = array('pretty_url' => $member->pretty_url,
							'nickname' => $member->nickname,
							'profile_image' => $profile_image);
			
			$data['members'][] = $item;
		}
		
		$data['group'] = $group;
		$data['user_admin'] = false;
		$data['member_of_grp'] = false;
		
		
		$vehicles = $this->car_model->getVehicles($this->session->userdata('user_id'));
		
		foreach($vehicles->result() as $vehicle){
			$user_role = $this->statics_model->getGroupRole($group_id, $vehicle->id);		
			if($user_role)
			{
				if($user_role->role_id == GROUP_ADMIN)
				{
					$data['user_admin'] = true;
				}
				if($user_role->role_id == GROUP_MEMBER)
				{
					$data['member_of_grp'] = true;		
				}
			}
	
		}
		
		
		
		
		$events = array();
		$posts = array();
		
		$group_events = $this->statics_model->getGroupEvents($group_id);
		
		$group_posts = $this->statics_model->getGroupPosts($group_id);
		
		foreach($group_events as $event)
		{
			$item = array('id' => $event->id,
							'title' => $event->title,
							'address' => $event->address,
							'description' => $event->description,
							'start_time' => date('F j, Y, \a\t g:i a',strtotime($event->start_time)),
							'image' => $event->image,
							'created_date' => $event->created_date);
			
			$events[] = $item;
		}
		
		
		
		foreach($group_posts as $post)
		{
			$vehicle = $this->car_model->getVehicle($post->vehicle_id)->row();
			
			$profile = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
			
			
			$item = array('id' => $post->id,
							'nickname' => $vehicle->nickname,
							'profile_image' => $profile,
							'text' => $post->text,
							'image' => $post->image,
							'created_date' => $this->set_user_timezone($post->created_date));
			
			$posts[] = $item;
		}
		
		
		function date_compare($a, $b)
		{
		    $t1 = strtotime($a['created_date']);
		    $t2 = strtotime($b['created_date']);
		    return $t1 - $t2;
		}   
		 
		usort($events, 'date_compare');
			
		
		$data['events'] = $events;
		$data['posts'] = $posts;
		
		$this->loadDefaultView('statics/group_detail', $data);	
	}
	
	
	
	
	
	
	
	public function edit_group($group_id)
	{
		$this->checkLogin();
		

		$data['message'] = $this->session->flashdata('message');
		$group = $this->statics_model->getGroupById($group_id)->row();
		$data['group'] = $group;
		
		
		$user_id = $this->session->userdata('user_id');
		
		$my_vehicles = $this->car_model->getVehicles($user_id)->result();
		
		$allow_edit = false;
		foreach($my_vehicles as $vehicle)
		{
			$result = $this->statics_model->getGroupRole($group_id, $vehicle->id);
			
			if($result->role_id == GROUP_ADMIN)
			{
				$allow_edit = true;
			}
		}
		
		if(!$allow_edit)
		{
			redirect('group/'.$group_id);
		}
		

		
		
		
		$data['is_ie'] = false;	
		
		$profile = $group->logo;
		
				
		$cover = $group->cover_image;
		
		
		
		$data['profile'] = $profile;
		$data['cover'] = $cover;
		
		$members = $this->statics_model->getGroupMembers($group_id)->result();
					
		$data['members'] = array();
		
		foreach($members as $member)
		{
			$profile_image = $this->statics_model->getImageByID($member->profile_image)->row()->fname;
			
			$item = array(	'id' => $member->id,
							'pretty_url' => $member->pretty_url,
							'nickname' => $member->nickname,
							'profile_image' => $profile_image);
			
			$data['members'][] = $item;
		}
		
		
		
		$applications = $this->statics_model->getGroupApplications($group_id)->result();
					
		$data['applications'] = array();
		
		foreach($applications as $application)
		{
			$profile_image = $this->statics_model->getImageByID($application->profile_image)->row()->fname;
			
			$item = array(	'id' => $application->id,
							'pretty_url' => $application->pretty_url,
							'nickname' => $application->nickname,
							'profile_image' => $profile_image);
			
			$data['applications'][] = $item;
		}
		
		
		
		$this->loadDefaultView('statics/group_edit', $data);
	}
	
	
	
	
	
	public function edit_group_validation()
	{
		//validate form input
		
	/*	$this->form_validation->set_rules('group_description', 'Description', 'xss_clean');
		
		

		$cover_data = NULL;
		$profile_data = NULL;
		$entity_id = $this->input->post('group_id');
		
		if ($this->form_validation->run() == true)
		{
			
			
			
			
			// prepare array for db insert
			$data = array(
						'id' => $entity_id,
						'description' => $this->input->post('group_description'),
						'auto_accept' => $this->input->post('group_auto_accept'));
		
			$this->statics_model->editGroup($data);
		
			$this->session->set_flashdata('message', 'Updated successfully!');
			
		}
		else
		{
		
			$this->session->set_flashdata('message', (validation_errors() ? validation_errors() : $this->session->flashdata('message')));
		}
		
		redirect('edit_group/' . $entity_id,'location');	*/
		
		$group_id = $_POST['gid'];
		$name = $_POST['name'];
		$description = $_POST['description'];
		$auto = $_POST['auto'];
		$address = $_POST['address'];
		$geo_location = $_POST['geo_location'];
		
		$datetime = date('Y-m-d H:i', strtotime($date));
		
		
		$data = array('id' => $group_id,
					'name' => $name,
					'description' => $description,
					'address' => $address,
					'auto_accept' => $auto,
					'geo_location' => $geo_location);
					
		$group_event_id = $this->statics_model->editGroup($data);
		
		
		//redirect('edit_group/' . $group_id,'location');
	}
	
	
	function save_coverpicture_user_event()
	{
		
		$entity_id = $_POST['eid'];
		$fname = $_POST['fname'];
				
		
		// prepare array for db insert
		$data = array(
					'id' => $entity_id,
					'image' => $fname);
	
		$this->statics_model->updateUserEvent($entity_id,$data);
	}
	
	
	function save_coverpicture_group_event()
	{
		
		$entity_id = $_POST['eid'];
		$fname = $_POST['fname'];
				
		
		// prepare array for db insert
		$data = array(
					'id' => $entity_id,
					'image' => $fname);
	
		$this->statics_model->updateGroupEvent($entity_id,$data);
	}
	
	
	function save_coverpicture_group()
	{
		
		$entity_id = $_POST['group_id'];
		$fname = $_POST['fname'];
				
		
		// prepare array for db insert
		$data = array(
					'id' => $entity_id,
					'cover_image' => $fname);
	
		$this->statics_model->editGroup($data);
	}
	
	function save_profilepicture_group()
	{
		
		$entity_id = $_POST['group_id'];
		$fname = $_POST['fname'];
				
		
		// prepare array for db insert
		$data = array(
					'id' => $entity_id,
					'logo' => $fname);
	
		$this->statics_model->editGroup($data);
		
	}
	
	
	
	function leave_group()
	{
		$group_id = $_POST['group_id'];
		$vehicle_id = $_POST['vehicle_id'];
		
		
		$result = $this->statics_model->getGroupRole($group_id, $vehicle_id);
		
		
		
		if($result)
		{
		
			if($result->role_id == GROUP_ADMIN)
			{
				$retval = array('status' => false,
								'message' => "Admin");
								
								
				
			}
			
			if($result->role_id == GROUP_MEMBER)
			{
				$this->statics_model->leaveGroup($result->id);
				
				$retval = array('status' => true,
								'message' => "Vehicle left group!");
								
				
			}
		}
		else
		{
			$retval = array('status' => false,
							'message' => "No such vehicle in group!");
		}
			
		
		echo json_encode($retval);	
		
	}
	
	function leave_group_vehicle()
	{
		$group_id = $_POST['group_id'];
		$user_id = $this->session->userdata('user_id');
		
		
		$owners_vehicles = $this->car_model->getVehicles($user_id)->result();
		foreach($owners_vehicles as $vehicle)
		{
			$result = $this->statics_model->getGroupRole($group_id, $vehicle->id);
			
			if($result)
			{
							
				if($result->role_id == GROUP_MEMBER)
				{
					$this->statics_model->leaveGroup($result->id);
		
									
					
				}
			}	
		}
			
	}
	
	function toggleGroupRights()
	{
		$group_id = $_POST['group_id'];
		$vehicle_id = $_POST['vehicle_id'];
		
		
		$result = $this->statics_model->getGroupRole($group_id, $vehicle_id);
		
		
		
		if($result)
		{
		
			if($result->role_id == GROUP_ADMIN)
			{
				$data = array('role_id' => GROUP_MEMBER);
				$this->statics_model->updateGroupRole($group_id, $vehicle_id, $data);
				
				$retval = array('status' => true,
								'message' => "Admin rights removed!");
								
								
				
			}
			
			if($result->role_id == GROUP_MEMBER)
			{
				$data = array('role_id' => GROUP_ADMIN);
				$this->statics_model->updateGroupRole($group_id, $vehicle_id, $data);
				
				$retval = array('status' => true,
								'message' => "Admin rights granted!");

								
				
			}
		}
		else
		{
			$retval = array('status' => false,
							'message' => "No such vehicle in group!");
		}
			
		
		echo json_encode($retval);	
		
	}
	
	
	function join_group()
	{
		$group_id = $_POST['group_id'];
		$vehicle_id = $this->session->userdata('vehicle_id');
		
		
		$group = $this->statics_model->getGroupById($group_id)->row();
		
		if($group->auto_accept == 1)
		{
			$data = array('group_id' => $group_id,
						'vehicle_id' => $vehicle_id,
						'role_id' => GROUP_MEMBER);
						
			$this->statics_model->addGroupUserRole($data);
			
			$retval = array('status' => true,
							'message' => 'Joined');
							
			
			$event_id = $this->statics_model->addNotification(8, $vehicle_id, $group_id);
			
			$owners_vehicles = $this->car_model->getVehicles($group->created_by)->result();
			foreach($owners_vehicles as $vehicle)
			{
				 $this->statics_model->addNotificationVehicle($vehicle->id,$event_id);	
			}
		   			
			//ADD GAME POINTS
			$this->addPoints($group->vehicle_id,5,'group_members', 'group joined');				
							
		}
		
		if($group->auto_accept == 0)
		{
			$data = array('group_id' => $group_id,
						'vehicle_id' => $vehicle_id);
						
			$this->statics_model->addGroupRequest($data);
			
			
			$event_id = $this->statics_model->addNotification(11, $vehicle_id, $group_id);
			$group = $this->statics_model->getGroupById($group_id)->row();
			$owners_vehicles = $this->car_model->getVehicles($group->created_by)->result();
			foreach($owners_vehicles as $vehicle)
			{
				 $this->statics_model->addNotificationVehicle($vehicle->id,$event_id);	
			}
			
			
			$retval = array('status' => false,
							'message' => 'Request sent');
		}
		
		
		
		
		
		
		echo json_encode($retval);
	}
	
	
	
	function accept_group_request()
	{
		$group_id = $_POST['group_id'];
		$vehicle_id = $_POST['vehicle_id'];
		
		
		$user_id = $this->session->userdata('user_id');
		
		$my_vehicles = $this->car_model->getVehicles($user_id)->result();
		
		$allow_edit = false;
		foreach($my_vehicles as $vehicle)
		{
			$result = $this->statics_model->getGroupRole($group_id, $vehicle->id);
			
			if($result->role_id == GROUP_ADMIN)
			{
				$allow_edit = true;
			}
		}
		
		if(!$allow_edit)
		{
			redirect('group/'.$group_id);
		}
		
		
		$data = array('group_id' => $group_id,
						'vehicle_id' => $vehicle_id,
						'role_id' => GROUP_MEMBER);
						
		$this->statics_model->removeGroupRequest($data);
		$this->statics_model->addGroupUserRole($data);
		
		
		
		
		
		$event_id = $this->statics_model->addNotification(8, $vehicle_id, $group_id);
		$group = $this->statics_model->getGroupById($group_id)->row();
		$owners_vehicles = $this->car_model->getVehicles($group->created_by)->result();
		foreach($owners_vehicles as $vehicle)
		{
			 $this->statics_model->addNotificationVehicle($vehicle->id,$event_id);	
		}
		
		$vehicle = $this->car_model->getVehicle($vehicle_id)->row();
		$profile_image = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
			
		$item = array(	'id' => $vehicle->id,
							'pretty_url' => $vehicle->pretty_url,
							'nickname' => $vehicle->nickname,
							'profile_image' => $profile_image);
		
		//ADD GAME POINTS
		$this->addPoints($group->vehicle_id,5,'group_members', 'group joined');	
		
		echo json_encode($item);
		
	}
	
	
	function decline_group_request()
	{
		$group_id = $_POST['group_id'];
		$vehicle_id = $_POST['vehicle_id'];
		
		
		$user_id = $this->session->userdata('user_id');
		
		$my_vehicles = $this->car_model->getVehicles($user_id)->result();
		
		$allow_edit = false;
		foreach($my_vehicles as $vehicle)
		{
			$result = $this->statics_model->getGroupRole($group_id, $vehicle->id);
			
			if($result->role_id == GROUP_ADMIN)
			{
				$allow_edit = true;
			}
		}
		
		if(!$allow_edit)
		{
			redirect('group/'.$group_id);
		}
		
		
		$data = array('group_id' => $group_id,
						'vehicle_id' => $vehicle_id);
						
		$this->statics_model->removeGroupRequest($data);
		
	}
	
	
	function add_group_event()
	{
		$title = $_POST['title'];
		$description = $_POST['description'];
		$address = $_POST['address'];
		$geo_location = $_POST['geo_location'];
		$date = $_POST['date'];
		
		$image = NULL;
		
		if($_POST['image'] != '' && $_POST['image'] != NULL)
		{
			$image = $_POST['image'];
		}
		
		$datetime = date('Y-m-d H:i', strtotime($date));
		$group_id = $_POST['group_id'];
		
		$data = array('group_id' => $group_id,
					'title' => $title,
					'description' => $description,
					'address' => $address,
					'image' => $image,
					'geo_location' => $geo_location,
					'start_time' => $datetime);
					
		$group_event_id = $this->statics_model->addGroupEvent($data);
		
		$event_data = array('event_id' => $group_event_id,
							'vehicle_id' => $this->session->userdata('vehicle_id'),
							'status_id' => 1);
		
		$this->statics_model->addVehicleToEvent($event_data);
		
		$action_id = 5; //event created
		
		
		$event_id = $this->statics_model->addNotification($action_id, $group_event_id, $group_id);
		
		$vehicles = $this->statics_model->getGroupMembers($group_id)->result();
		
		foreach($vehicles as $vehicle){
			 $this->statics_model->addNotificationVehicle($vehicle->id,$event_id);
		 }
		
		
	}
	
	
	function clone_group_event()
	{
		$event = $this->statics_model->getGroupEventsByID($_POST['event_id']);
		
		
		
		if($event !== false)
		{
			$title = $event->title;
			$description = $event->description;
			$address = $event->address;
			$geo_location = $event->geo_location;
			$date = $event->start_time;
			
			
			
			$datetime = date('Y-m-d H:i', strtotime($date));
			$group_id = $event->group_id;
			
			$vehicle_id = $this->statics_model->getGroupById($group_id)->row()->vehicle_id;
			
			$data = array('group_id' => $group_id,
						'title' => $title,
						'description' => $description,
						'address' => $address,
						'image' => $image,
						'geo_location' => $geo_location,
						'start_time' => $datetime);
						
			$group_event_id = $this->statics_model->addGroupEvent($data);
			
			$event_data = array('event_id' => $group_event_id,
								'vehicle_id' => $vehicle_id,
								'status_id' => 1);
			
			$this->statics_model->addVehicleToEvent($event_data);
			
			$action_id = 5; //event created
			
			
			$event_id = $this->statics_model->addNotification($action_id, $group_event_id, $group_id);
			
			$vehicles = $this->statics_model->getGroupMembers($group_id)->result();
			
			foreach($vehicles as $vehicle){
				 $this->statics_model->addNotificationVehicle($vehicle->id,$event_id);
			 }
			
			echo $group_event_id;
		}
		
		
		
	}
	
	
	
	
	function add_user_event()
	{
		$title = $_POST['title'];
		$description = $_POST['description'];
		$date = $_POST['date'];
		$address = $_POST['address'];
		$pb = $_POST['pb'];
		
		$geo_location = $_POST['geo_location'];
		$datetime = date('Y-m-d H:i', strtotime($date));
		$vehicle_id = $this->session->userdata('vehicle_id');
		
		$image = NULL;
		
		if($_POST['image'] != '' && $_POST['image'] != NULL)
		{
			$image = $_POST['image'];
		}
		
		
		$data = array('vehicle_id' => $vehicle_id,
					'title' => $title,
					'description' => $description,
					'image' => $image,
					'address' => $address,
					'public' => $pb,
					'geo_location' => $geo_location,
					'user_id' => $this->session->userdata('user_id'),
					'start_time' => $datetime);
					
		$group_event_id = $this->statics_model->addUserEvent($data);
		
		$event_data = array('user_event_id' => $group_event_id,
							'vehicle_id' => $this->session->userdata('vehicle_id'),
							'status' => 1);
		
		$this->statics_model->addVehicleToUserEvent($event_data);
		
		$entity_id = $group_event_id;
		$action_id = 5; //event created
		
		
		$event_id = $this->statics_model->addEvent($action_id, $entity_id);				
		$this->statics_model->addEventVehicle($vehicle_id,$event_id);
		

		
		
		//ADD GAME POINTS
		//$this->addPoints($vehicle_id,5,'events_created', 'event created');	
	/*	
		
		
		$event_id = $this->statics_model->addNotification($action_id, $group_event_id, $group_id);
		
		$vehicles = $this->statics_model->getGroupMembers($group_id)->result();
		
		foreach($vehicles as $vehicle){
			 $this->statics_model->addNotificationVehicle($vehicle->id,$event_id);
		 }
		
		*/
	}
	
	
	
	function clone_user_event()
	{
		
		$event = $this->statics_model->getUserEventById($_POST['event_id']);
		
		if($event !== false)
		{
			
			
			$title = $event->title;
			$description = $event->description;
			$date = $event->start_time;
			$address = $event->address;
			
			$geo_location = $event->geo_location;
			$datetime = date('Y-m-d H:i', strtotime($date));
			$vehicle_id = $event->vehicle_id;
			
			$data = array('vehicle_id' => $vehicle_id,
						'title' => $title,
						'description' => $description,
						'address' => $address,
						'image' => $image,
						'user_id' => $event->user_id,
						'geo_location' => $geo_location,
						'start_time' => $datetime);
						
			$group_event_id = $this->statics_model->addUserEvent($data);
			
			$event_data = array('user_event_id' => $group_event_id,
								'vehicle_id' => $vehicle_id,
								'status' => 1);
			
			$this->statics_model->addVehicleToUserEvent($event_data);
			
			$entity_id = $group_event_id;
			$action_id = 5; //event created
			
			
			$event_id = $this->statics_model->addEvent($action_id, $entity_id);				
			$this->statics_model->addEventVehicle($vehicle_id,$event_id);
			
			
			echo $group_event_id;
		}
		
		
		

		
		
		//ADD GAME POINTS
		//$this->addPoints($vehicle_id,5,'events_created', 'event created');	
	/*	
		
		
		$event_id = $this->statics_model->addNotification($action_id, $group_event_id, $group_id);
		
		$vehicles = $this->statics_model->getGroupMembers($group_id)->result();
		
		foreach($vehicles as $vehicle){
			 $this->statics_model->addNotificationVehicle($vehicle->id,$event_id);
		 }
		
		*/
	}
	
	
	
	function update_user_event()
	{
		$event_id = $_POST['eid'];
		$title = $_POST['title'];
		$description = $_POST['description'];
		$date = $_POST['date'];
		$address = $_POST['address'];
		$geo_location = $_POST['geo_location'];
		
		$datetime = date('Y-m-d H:i', strtotime($date));
		
		
		$data = array('title' => $title,
					'description' => $description,
					'address' => $address,
					'geo_location' => $geo_location,
					'start_time' => $datetime);
					
		$group_event_id = $this->statics_model->updateUserEvent($event_id,$data);
		
		
	}
	
	
	
	function update_group_event()
	{
		$event_id = $_POST['eid'];
		$title = $_POST['title'];
		$description = $_POST['description'];
		$date = $_POST['date'];
		$public = $_POST['public'];
		$address = $_POST['address'];
		$geo_location = $_POST['geo_location'];
		
		$datetime = date('Y-m-d H:i', strtotime($date));
		
		
		$data = array('title' => $title,
					'description' => $description,
					'public' => $public,
					'address' => $address,
					'geo_location' => $geo_location,
					'start_time' => $datetime);
					
		$group_event_id = $this->statics_model->updateGroupEvent($event_id,$data);
		
		
	}
	
	
	
	function add_mod()
	{
		$vehicle_type = $_POST['vehicle_type'];
		$column_type = $_POST['column_type'];
		$engine_type = $_POST['engine_type'];
		$vehicle_id = $_POST['vehicle_id'];
		$mod_text = $_POST['mod_text'];
		
		$mod_type = $_POST['mod_type'];
		
		switch($vehicle_type)
		{
			case 0: { // CAR
				
				$data = array('vehicle_id' => $vehicle_id,
								'category' => $column_type,
								'type' => $mod_type,
								'text' => $mod_text);
				
				
				$table = 'diesel_mods';
				
				if($engine_type == "gas")
				{
					$table = 'gas_mods';
				}
								
								
				
				$this->car_model->addVehicleMod($data, $table);
				
				$data = array('vehicle_id' => $vehicle_id,
								'mod_text' => $column_type,
								'mod' => $mod_text);
								
				$history_id = $this->car_model->addVehicleMod($data, 'car_mod_history');

			};break;
			
			case 1: { //MOTORCYCLE
				
					$data = array('vehicle_id' => $vehicle_id,
								'category' => $column_type,
								'type' => $mod_type,
								'text' => $mod_text);
					
					$this->car_model->addVehicleMod($data, 'motorcycle_mods');	
					
					
					$data = array('vehicle_id' => $vehicle_id,
								'mod_text' => $column_type,
								'mod' => $mod_text);
					
					$history_id = $this->car_model->addVehicleMod($data, 'motorcycle_mod_history');
			};break;
			
			case 2: { // TRUCK
				
				$data = array('vehicle_id' => $vehicle_id,
								'category' => $column_type,
								'type' => $mod_type,
								'text' => $mod_text);
				
				
				$table = 'diesel_mods';
				
				if($engine_type == "gas")
				{
					$table = 'gas_mods';
				}

				$this->car_model->addVehicleMod($data, $table);
				
				$data = array('vehicle_id' => $vehicle_id,
								'mod_text' => $column_type,
								'mod' => $mod_text);
								
				$history_id = $this->car_model->addVehicleMod($data, 'truck_mod_history');
							
			};break;
		}
		
		
		$entity_id = $vehicle_id;
		$action_id = 4; //new mod
		$event_id = $this->statics_model->addEvent($action_id, $entity_id, $history_id);
		$this->statics_model->addEventVehicle($vehicle_id,$event_id);
		
		//ADD GAME POINTS
		$this->addPoints($vehicle_id,5,'number_of_mods', 'added mod');
		
		
		
		
		/*$vehicle_type = $_POST['vehicle_type'];
		$column_type = $_POST['column_type'];
		$engine_type = $_POST['engine_type'];
		$vehicle_id = $_POST['vehicle_id'];
		$mod_text = $_POST['mod_text'];
		
		$mod_type = $_POST['mod_type'];
		
		switch($vehicle_type)
		{
			case 0: { // CAR
				
				$data = array('vehicle_id' => $vehicle_id,
								$column_type.'_type' => $mod_type,
								$column_type.'_text' => $mod_text);
				
				
				$table = 'diesel_mods';
				
				if($engine_type == "gas")
				{
					$table = 'gas_mods';
				}
								
								
				$modCheck = $this->car_model->checkVehicleMod($table,$vehicle_id);
				
				
				if(!$modCheck)
				{
					$this->car_model->addVehicleMod($data, $table);
				}
				else
				{
					$this->car_model->updateVehicleMod($data, $table);
				}
				
				$data = array('vehicle_id' => $vehicle_id,
								'mod' => $mod_text,
								'mod_text' => $column_type);
				
				$history_id = $this->car_model->addVehicleMod($data, 'car_mod_history');
				
				
			};break;
			
			case 1: { //MOTORCYCLE
				
				$data = array('vehicle_id' => $vehicle_id,
								$column_type.'_type' => $mod_type,
								$column_type.'_text' => $mod_text);
								
								
				$modCheck = $this->car_model->checkVehicleMod('motorcycle_mods',$vehicle_id);
				
				
				if(!$modCheck)
				{
					$this->car_model->addVehicleMod($data, 'motorcycle_mods');
				}
				else
				{
					$this->car_model->updateVehicleMod($data, 'motorcycle_mods');
				}
				
				
				$data = array('vehicle_id' => $vehicle_id,
								'mod' => $mod_text,
								'mod_text' => $column_type);
				
				$history_id = $this->car_model->addVehicleMod($data, 'motorcycle_mod_history');
				
				

				
				
			};break;
			
			case 2: { // TRUCK
				
				$data = array('vehicle_id' => $vehicle_id,
								$column_type.'_type' => $mod_type,
								$column_type.'_text' => $mod_text);
				
				
				$table = 'diesel_mods';
				
				if($engine_type == "gas")
				{
					$table = 'gas_mods';
				}
								
								
				$modCheck = $this->car_model->checkVehicleMod($table,$vehicle_id);
				
				
				if(!$modCheck)
				{
					$this->car_model->addVehicleMod($data, $table);
				}
				else
				{
					$this->car_model->updateVehicleMod($data, $table);
				}
				
				$data = array('vehicle_id' => $vehicle_id,
								'mod' => $mod_text,
								'mod_text' => $column_type);
				
				$history_id = $this->car_model->addVehicleMod($data, 'truck_mod_history');
				
			};break;
		}
		
		
		$entity_id = $vehicle_id;
		$action_id = 4; //new mod
		$event_id = $this->statics_model->addEvent($action_id, $entity_id, $history_id);
		$this->statics_model->addEventVehicle($vehicle_id,$event_id);
		
		//ADD GAME POINTS
		$this->addPoints($vehicle_id,5,'number_of_mods', 'added mod');
	*/	
	}
	
	
	
	public function delete_mod()
	{
		$vehicle_id = $_POST['car_id'];
		$mod_id = $_POST['mod_id'];
		
		
		$vehicle = $this->car_model->getVehicle($vehicle_id)->row();
		
		switch($vehicle->vehicle_type)
		{
			case 0: {
						if($vehicle->engine_type == "gas")
						{
							$table = "gas_mods";
						}
						else
						{
							$table = "diesel_mods";
						}
					};break;
			
			case 1: {
						$table = "motorcycle_mods";
					};break;
					
			case 2: {
						if($vehicle->engine_type == "gas")
						{
							$table = "gas_mods";
						}
						else
						{
							$table = "diesel_mods";
						}
					};break;
		}
		
		$data = array('visible' => 0);
		
		$this->statics_model->removeMod($mod_id, $data, $table);
	}
	
	
	
	
	public function group_event_detail($event_id)
	{
			
		
		$event = $this->statics_model->getEventById($event_id);
	
		$data['event'] = $event;
		$group_id = $event->group_id;
		$data['event_geo'] = $event->geo_location;
		$data['decided'] = false;
		$data['im_admin'] = false;
		$member_of_grp = false;
		$data['joined_event'] = false;
		
		$data['vehicles_joined'] = array();
		
		
		$data['my_vehicle_id'] = $this->session->userdata('vehicle_id');
		$my_cars = $this->car_model->getVehicles($this->session->userdata('user_id'))->result();
			
		foreach($my_cars as $car)
		{
			$role = $this->statics_model->getGroupRole($group_id, $car->id);		
			
			if($role)
			{
				if($role->role_id == GROUP_ADMIN)
				{
					$member_of_grp = true;
					$data['im_admin'] = true;
					$data['my_vehicle_id'] = $car->id;
				}
				
				
				if($role->role_id == GROUP_MEMBER)
				{
					$member_of_grp = true;
					$data['my_vehicle_id'] = $car->id;
					
				}
			}
			
		}

		$vehicles_joined = $this->statics_model->getVehiclesFromEvent($event_id);
		
		foreach($vehicles_joined as $vid)
		{
			$vehicle = $this->car_model->getVehicle($vid->vehicle_id)->row();
			$profile_image = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
			
			$item = array(	'id' => $vehicle->id,
							'pretty_url' => $vehicle->pretty_url,
							'nickname' => $vehicle->nickname,
							'profile_image' => $profile_image);
			
			$data['vehicles_joined'][] = $item;
		}
		
		
		
		$checkVehicle = $this->statics_model->checkVehicleAtEvent($event_id, $data['my_vehicle_id']);

		
		if($checkVehicle->status_id == 1)
		{
			$data['joined_event'] = true;
		}
		
		
		
	//	if($member_of_grp)
	//	{
			$this->loadDefaultView('statics/group_event_detail', $data);
			
	/*	}
		else
		{
			redirect('news');
		}
		*/

			
	}
	
	
	public function group_event_decision()
	{
		$event_id = $_POST['event_id'];
		$vehicle_id = $_POST['vehicle_id'];
		$status = $_POST['status'];
		
		
		$checkVehicle = $this->statics_model->checkVehicleAtEvent($event_id, $vehicle_id);
		
		if($checkVehicle)
		{
			$data = array('status_id' => $status);
			$this->statics_model->updateVehicleToEvent($event_id,$vehicle_id,$data);
		}
		
		else
		{
			$data = array('event_id' => $event_id,
						'vehicle_id' => $vehicle_id,
						'status_id' => $status);
						
			$this->statics_model->addVehicleToEvent($data);
		}
		
		if($status == 1)
		{
			$saved_event_id = $this->statics_model->addNotification(10, $vehicle_id, $event_id);
			
			$the_event = $this->statics_model->getGroupEventsByID($event_id);
			
			$the_group = $this->statics_model->getGroupById($the_event->group_id)->row();
			
			$owners_vehicles = $this->car_model->getVehicles($the_group->created_by)->result();
			
			$this->addPoints($the_group->vehicle_id,5,'event_participants', 'event joined');
			
			foreach($owners_vehicles as $vehicle)
			{
				 $this->statics_model->addNotificationVehicle($vehicle->id,$saved_event_id);	
			}		
		}
		
	}


	public function user_event_decision()
	{
		$event_id = $_POST['event_id'];
		$vehicle_id = $_POST['vehicle_id'];
		$status = $_POST['status'];
		
		
		$checkVehicle = $this->statics_model->checkVehicleAtUserEvent($event_id, $vehicle_id);
		
		if($checkVehicle)
		{
			$data = array('status' => $status);
			$this->statics_model->updateVehicleToUserEvent($event_id,$vehicle_id,$data);
		}
		
		else
		{
			$data = array('user_event_id' => $event_id,
						'vehicle_id' => $vehicle_id,
						'status' => $status);
						
			$this->statics_model->addVehicleToUserEvent($data);
		}
		
		if($status == 1)
		{
			$saved_event_id = $this->statics_model->addNotification(9, $vehicle_id, $event_id);
			
			
			$user_event = $this->statics_model->getUserEventById($event_id);
			
			$this->statics_model->addNotificationVehicle($user_event->vehicle_id,$saved_event_id);	
			
			
			//ADD GAME POINTS
			$this->addPoints($user_event->vehicle_id,5,'event_participants', 'event joined');
			
		}
		
	}
	
	
	public function send_news_comment()
	{
		$this->checkLogin();
		$event_id = $_POST['eid'];
		$comment = $_POST['comment'];
		$vehicle_id = $this->session->userdata('vehicle_id');
		
		$data = array('event_id' => $event_id,
						'vehicle_id' => $vehicle_id,
						'comment' => $comment);
		
		
		$this->statics_model->addNewsComment($data);
		
		$vehicle = $this->car_model->getVehicle($vehicle_id)->row();
		
		$profile = $this->statics_model->getImageByID($vehicle->profile_image)->row();
		
		$return = array('comment' => $comment,
						'nickname' => $vehicle->nickname,
						'profile_image' => $profile->fname);
		
		
		echo json_encode($return);
			
	}
	
	
	public function send_group_post()
	{
		$this->checkLogin();
		$group_id = $_POST['gid'];
		$text = $_POST['post'];
		$fname = $_POST['fname'];
		$vehicle_id = $this->session->userdata('vehicle_id');
		
		$data = array('group_id' => $group_id,
						'vehicle_id' => $vehicle_id,
						'image' => $fname,
						'text' => $text);
		
		
		$this->statics_model->addGroupPost($data);
		
		$vehicle = $this->car_model->getVehicle($vehicle_id)->row();
		
		$profile = $this->statics_model->getImageByID($vehicle->profile_image)->row();
		
		$return = array('text' => $text,
						'nickname' => $vehicle->nickname,
						'profile_image' => $profile->fname);
		
		
		echo json_encode($return);
			
	}

	
	
	public function invite_vehicles_to_group()
	{
		$vehicles = $_POST['vehicles'];
		$group_id = $_POST['group_id'];
		
		$inviting_vehicle = $this->session->userdata('vehicle_id');
		
		$action_id = 6; //invited to group
		
		
		$event_id = $this->statics_model->addNotification($action_id, $inviting_vehicle, $group_id);
		
		
		
		foreach($vehicles as $vehicle){
			 $this->statics_model->addNotificationVehicle($vehicle,$event_id);
		 }
	}
	
	
	
	
	public function invite_vehicles_to_event()
	{
		$vehicles = $_POST['vehicles'];
		$event = $_POST['event_id'];
		
		$inviting_vehicle = $this->session->userdata('vehicle_id');
		
		$action_id = 7; //invited to event
		
		
		$event_id = $this->statics_model->addNotification($action_id, $inviting_vehicle, $event);
		
		
		
		foreach($vehicles as $vehicle){
			 $this->statics_model->addNotificationVehicle($vehicle,$event_id);
		 }
	}
	
	
	
	public function invite_all_followers()
	{
		$group_id = $_POST['group_id'];
		
		$inviting_vehicle = $this->session->userdata('vehicle_id');
		
		$action_id = 6; //invited to group
		
		
		$event_id = $this->statics_model->addNotification($action_id, $inviting_vehicle, $group_id);
		
		$vehicles = $this->car_model->getFollowers($inviting_vehicle);
		
		foreach($vehicles as $vehicle){
			 $this->statics_model->addNotificationVehicle($vehicle['follower_id'],$event_id);
		 }
		
	}
	
	
	public function invite_all_followers_event()
	{
		$event = $_POST['event_id'];
		
		$inviting_vehicle = $this->session->userdata('vehicle_id');
		
		$action_id = 7; //invited to event
		
		
		$event_id = $this->statics_model->addNotification($action_id, $inviting_vehicle, $event);
		
		$vehicles = $this->car_model->getFollowers($inviting_vehicle);
		
		foreach($vehicles as $vehicle){
			 $this->statics_model->addNotificationVehicle($vehicle['follower_id'],$event_id);
		 }
		
	}
	
	
	
	
	public function user_event_detail($event_id)
	{
			
		
		$event = $this->statics_model->getUserEventById($event_id);
		$data['event'] = $event;
		$vehicle_id = $event->vehicle_id;
		$data['event_geo'] = $event->geo_location;		
		$data['decided'] = false;
		$data['im_admin'] = false;
		$member_of_grp = false;
		$data['joined_event'] = false;
		
		$data['vehicles_joined'] = array();
		
		
		$data['my_vehicle_id'] = $this->session->userdata('vehicle_id');
		$my_cars = $this->car_model->getVehicles($this->session->userdata('user_id'))->result();
			
		foreach($my_cars as $car)
		{
					
			
			
				if($car->id == $vehicle_id)
				{
					$member_of_grp = true;
					$data['im_admin'] = true;
					$data['my_vehicle_id'] = $car->id;
				}
				
				
				
			
			
		}
		
		if($event->user_id == $this->session->userdata('user_id'))
		{
			$data['im_admin'] = true;
		}
		
		$vehicles_joined = $this->statics_model->getVehiclesFromUserEvent($event_id);
		
		foreach($vehicles_joined as $vid)
		{
			$vehicle = $this->car_model->getVehicle($vid->vehicle_id)->row();
			$profile_image = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
			
			$item = array(	'id' => $vehicle->id,
							'pretty_url' => $vehicle->pretty_url,
							'nickname' => $vehicle->nickname,
							'profile_image' => $profile_image);
			
			if($vehicle)
			{
				$data['vehicles_joined'][] = $item;
			}
			
		}
		
		
		
		$checkVehicle = $this->statics_model->checkVehicleAtUserEvent($event_id, $data['my_vehicle_id']);

		
		if($checkVehicle->status == 1)
		{
			$data['joined_event'] = true;
		}
		
		
		
		
		$this->loadDefaultView('statics/user_event_detail', $data);
			
		
		

			
	}
	
	
	
	public function poi_detail($poi_id)
	{
		$poi = $this->statics_model->getPOI($poi_id);
		
		if(!$poi)
		{
			redirect('');
		}
		
		$data['poi'] = $poi;
		$data['poi_id'] = $poi_id;
				
		$data['geo_location'] = $poi->geo_location;
		
		$data['images'] = $this->statics_model->getPoiImages($poi_id);
		
		
		$ratings = $this->statics_model->getRatings($poi_id,$this->session->userdata('user_id'),'poi_ratings','poi_id')->result();
		$rows = count($ratings);
		$ratingSum = 0;
		foreach($ratings as $i=>$row){
			$ratingSum += $row->rating;
		}
		if($rows == 0){
			$data['ratingSum'] = "";
		}
		else{
			$data['ratingSum'] = $ratingSum/$rows;	
		}
		
		
		$data['ratingSum'] = round($data['ratingSum'], 2);
		
		$data['number_of_votes'] = $rows;
		$data['already_rated'] = $this->statics_model->checkRating($poi_id,$this->session->userdata('user_id'),'poi_ratings','poi_id');
		
		
		
		$this->loadDefaultView('statics/poi_detail', $data);					
	}
	
	
	public function add_poi_suggestion()
	{
		$name = $_POST['name'];
		$description = $_POST['description'];
		$address = $_POST['address'];
		$categories = $_POST['category'];
		$geo_location = $_POST['geo_location'];
		
		
		
		
		
		$data = array('name' => $name,
						'description' => $description,
						'address' => $address,
						'geo_location' => $geo_location);
						
		$last_id = $this->statics_model->addPOI($data);
		
		foreach($categories as $category)
		{
			$new_data = array('poi_id' => $last_id,
								'category_id' => $category);
								
			$this->statics_model->addPOICategories($new_data);
		} 
		
		
	}
	
	
	function story_image_upload()
	{
		
		$this->load->library('upload');
	
		$user_id = $this->session->userdata('user_id');
	
		// if file to upload is selected upload
		if($_FILES['file']['size'] != 0 && $_FILES['file']['error'] == 0)	
		{
			
			$uploadconfig['upload_path'] = 'items/uploads/images';
			$uploadconfig['allowed_types'] = 'gif|jpg|png|jpeg';
			$uploadconfig['encrypt_name'] = true;	
			
			$this->upload->initialize($uploadconfig);
			
			$this->upload->do_upload('file');
			
			$data = $this->upload->data();
			
			$error = array('error' => $this->upload->display_errors());
			
			$this->load->library('image_moo');
			$fname = 'items/uploads/images/'.$data['file_name'];
				
				//FIX IMAGE ROTATION 
				$exif = exif_read_data($fname);
				$ort = $exif['Orientation'];
						
	            switch($ort)
	            {
	
	                case 3: // 180 rotate left
	                	$this->image_moo->load($fname)->rotate(180)->save($fname,true);    
	                    break;
	                case 6: // 90 rotate right
	                	$this->image_moo->load($fname)->rotate(-90)->save($fname,true);			                  
	                    break;			
	                case 8:    // 90 rotate left
	                    $this->image_moo->load($fname)->rotate(90)->save($fname,true);
	                    break;
					default:{};
	            }
			
			if($data['image_width']<300 || $data['image_height']<300)
			{
				
				$newimage = $this->image_moo->load($fname)->resize(500,500,true)->save($fname,true);
				
				
				
			}
			
			
			
			if($data['image_width']>1000 || $data['image_height']>1000)
			{
				
				$this->image_moo->load($fname)->resize(1000,1000)->save($fname,true);
				
			}
			
			
			$imgdata = array('fname' => $data['file_name'],
								'story_id' => $_POST['sid']);
			
			
			
			$image_id = $this->statics_model->addStoryPicture($imgdata);
			
			$ret_data['fname'] = $data['file_name'];
			//$ret_data['image_id'] = $image_id;
			echo json_encode($ret_data);
		}		
		else
		{
			echo "FAIL";
		}
		
	}

	
	public function report_entity()
	{
		$eid = $_POST['eid'];
		$type = $_POST['type'];
		$user_id = $this->session->userdata('user_id');
		
		
		$data = array('entity_id' => $eid,
						'user_id' => $user_id,
						'type' => $type);
						
		$this->statics_model->addReport($data);
		
		
		
	}
	
	
	public function mark_all_notifications_read()
	{
		
		$user_id = $this->session->userdata('user_id');
		
		$vehicles = $this->car_model->getVehicles($user_id)->result();
		
		
		foreach($vehicles as $vehicle)
		{
			
			
			$notifications = $this->statics_model->getNotifications($vehicle->id)->result();
			
			
			
			foreach($notifications as $note)
			{
				$data = array('id' => $note->id,
								'has_seen' => 1);
								
				$this->statics_model->editNotification($data);				
			}
			
			
			
		}
		

		
	}
	
	
	
	
	
	/**   MAILS  **/
	public function send_invite_mail(){
		$email = $_POST['email'];
		
		
		$user = $this->statics_model->getUserData($this->session->userdata('user_id'))->row();
		$mailname = $user->first_name." ".$user->last_name;
	 
		$this->load->library('My_phpmailer');
        
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server 
        $mail->SMTPDebug = 1;  // prefix for secure protocol to connect to the server 
        $mail->Host       = "smtp.easyname.eu";      // setting SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to 
        $mail->Username   = "1590mail27";  // user email address
        $mail->Password   = "Kksnoopstr";            // password 
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SetFrom('noreply@performancenation.com', 'Performance Nation');  //Who is sending the email
        $mail->AddReplyTo("noreply@performancenation.com","Performance Nation");  //email address that receives the response
        $mail->CharSet = 'UTF-8';
       
		
		$data['mailname'] = $mailname;
		
		
		$body = $this->load->view('mail/invite_mail', $data, true);	
		
        $mail->Subject    = "Performance Nation Invitation";
		$mail->Body      =  $body;
		$mail->IsHTML(true);
		
        
       
        $mail->AddAddress($email);
		//$mail->Send();
        if(!$mail->Send()) {
            $data["message"] = "Failed to send email, please try again!".$mail->ErrorInfo;
        } else {
            $data["message"] = "Email sent!";
        }
       echo $data['message'];
	}
	
	
	
/*	public function mail_test()
	{
		
		$data['recipient'] = 'Karoly MAteszopszkij';
		$data['hash'] = 'dfasdfasfasdfaf';
					
					
		$this->load->view('mail/password_reset_mail', $data);
	}
*/	
	
	public function delete_vehicle()
	{
		$vehicle_id = $_POST['cid'];
		
		
		$this->car_model->deleteVehicle($vehicle_id);
		
	}
	
	public function delete_club()
	{
		$club_id = $_POST['iid'];
		
		$this->statics_model->deleteClub($club_id);
		
	}
	
	public function delete_user_event()
	{
		$event_id = $_POST['eid'];
		
		$this->statics_model->deleteUserEvent($event_id);
		
	}
	
	public function delete_group_event()
	{
		$event_id = $_POST['eid'];
		
		$this->statics_model->deleteGroupEvent($event_id);
		
	}
	
	public function sendSuggestion()
	{
		$text = $_POST['text'];
		$user_id = $this->session->userdata('user_id');
		
		$data = array('user_id' => $user_id,
						'text' => $text,
						'ip_address' => $this->input->ip_address());
						
		$this->statics_model->addUserSuggestion($data);
		
		echo "OK";
		
	}

	public function saveTitleText()
	{
		$text = $_POST['string'];
		
		$this->session->set_userdata('image_text', urlencode($text));
		
	}
}
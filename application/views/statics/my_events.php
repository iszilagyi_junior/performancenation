<script>	
	
	$(document).ready(function()
	{	
		$('#group_event_date').datetimepicker();
		initAutocomplete();
	});
	
</script>

<? if(!$is_ie){?>
	<div id="image_cropping_overlay">
		<div class="" id="crop_title" style="position:relative;top:0px;text-align: center;font-size:22px;">Cover Picture</div>
		<div class="sansitalic" style="margin:5px auto;text-align: center;">Please select an area</div>
		<div id="image_crop_holder">
			
		</div>
		 <input type="hidden" id="selection_x1" name="selection_x1" value="" />
		 <input type="hidden" id="selection_x2" name="selection_x2" value="" />
		 <input type="hidden" id="selection_y1" name="selection_y1" value="" />
		 <input type="hidden" id="selection_y2" name="selection_y2" value="" />
		<div class="button" type="user" eid="0" id="finish_crop_event" image_type="" style="left:255px;position: absolute;">FINISH</div>
		<div class="button" id="close_crop"  style="left:545px;position: absolute;" >CLOSE</div>
	</div>
<? } ?>
<div id="content" style="width:800px;">
	
	
	<div id="user_event_overlay">
		<div class="overlayBG"></div>
		<div style="position:relative;z-index:100">
			<img class="close_overlay" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />
			<div class="titboldit" style="font-size:27px;margin-top:30px;">CREATE EVENT</div>
			<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">
				<table style="width:400px;margin:30px auto;">
					<tr>
						<td style="regular">
							Title
						</td>
						<td>
							<input type="text" placeholder="Title" class="edit_input" name="group_event_title" id="group_event_title"></input>
						</td>
					</tr> 
					
					<tr>
						<td style="regular">
							Description
						</td>
						<td>
							<textarea  placeholder="Description" class="edit_input" name="group_event_description" id="group_event_description" style="resize:none;height:60px;"></textarea>
						</td>
					</tr>
					<tr>
						<td style="regular">
							Address
						</td>
						<td>
							<input type="text" name="group_address" id="autocomplete" onFocus="geolocate()" class="edit_input" value="" placeholder="Event address" style=""/>
						</td>
					</tr>
					<tr>
						<td style="regular">
							Date
						</td>
						<td>
							<input type="text" placeholder="Date" class="edit_input" name="group_event_date" id="group_event_date"></input>
						</td>
					</tr>
					
					<tr>
						<td style="regular">
							Public
						</td>
						<td>
							<select name="event_public" id="event_public">
								<option value="1">Yes</div>
								<option value="0">No</div>
							</select>	
						</td>
					</tr>
					<input type="hidden"  class="edit_input" name="preupload_cover" id="preupload_cover"></input>
					<img style="display:none;width:20%;margin:5px auto;" id="preupload_cover_holder" />
					<div class="button" id="user_event_image" eid="">UPLOAD COVER</div>
				</table>
				<input type="file" preview="edit_user_cover_upload" accept="image/*" id="edit_user_cover_picture" name="edit_user_cover_picture" class="edit_hidden_upload">
			</div>
			<div class="button" id="create_user_event_button">CREATE</div>
			
			<div class="regular" id="group_event_error_message"></div>
		</div>
	</div>



	<img class="" style="position:absolute;left:0px;top:0px;" src="<?= site_url('items/frontend/img/big_chrome.png')?>" border="0">
	<div class="playbold gold page_title" style="text-align:center;font-size:40px;letter-spacing:1px;z-index:10;position:relative;top:10px;">MY EVENTS</div>
	
	
	
	<div class="button" id="new_user_event" style="margin-top:35px;">CREATE NEW</div>		
	
	
	<div id="resultHolder" style="margin-top:20px;">
		<div class="dosissemi"></div><br/>
		<div id="result_list" style="overflow:hidden;">
			<div class="regular">MY EVENTS</div>
			<br/>
			<? $x = 1; foreach($my_events as $group):?>
			<a  style="text-decoration:none;" href="<?= site_url($group['type'].'_event/' . $group['id'])?>">	
				<div class="result_list_item" style="<? if($x%2 == 0){ echo "margin-right:0px";}?>">	
						<img class="result_item_image" src="<?= site_url('items/uploads/'.$group['logo'])?>" style="">
						<div class="div_white_over"></div>
						<div class="result_item_text" style="">
								<?= $group['title'];?><br/>
								<?= $group['address'];?><br/>
								<br/>
								<?= date('F j, Y, <\b\\r>\a\t g:i a',strtotime($group['start']));?><br/>
								</div>				
					
				</div>
			</a>
			<? $x++; endforeach;?>
			<? if($x == 1) echo "None yet!";?>
			<br/>
			<div class="regular" style="width: 100%;float: left; margin: 20px 0px">EVENTS I JOINED</div>
			<br/>
			<? $x = 1; foreach($joined_events as $group):?>
			<a  style="text-decoration:none;" href="<?= site_url($group['type'].'_event/' . $group['id'])?>">	
				<div class="result_list_item" style="<? if($x%2 == 0){ echo "margin-right:0px";}?>">
						<img class="result_item_image" src="<?= site_url('items/uploads/'.$group['logo'])?>" style="">	
						<div class="div_white_over"></div>
						<div class="result_item_text" style="">
								<?= $group['title'];?><br/>
								<?= $group['address'];?><br/>
								<br/>
								<?= date('F j, Y, <\b\\r>\a\t g:i a',strtotime($group['start']));?><br/>
								</div>				
					
				</div>
			</a>
			<? $x++; endforeach;?>
			<? if($x == 1) echo "None yet!";?>
		</div>		
		<br/>
		
	</div>
	
	
</div>
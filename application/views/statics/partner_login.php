	<div id="content">
				<div id="register_header" class="content_header">
					<?= $this->lang->line('partner_login_header')?>
				</div>				
				<div id="login_error_message">
					<?php echo $message;?>
				</div>
				<div id="login_form">
					<?php 
						echo form_open("partner");
					?>
					<label><?= $this->lang->line('partner_login_username')?></label><input type="text" id="login_username" name="login_username" value="" defaultText="<?= $this->lang->line('login_login_username')?>" class="login_login_input input_default">
					<br>
					<label><?= $this->lang->line('login_login_password')?></label><input type="password" id="login_password" name="login_password" class="login_login_input password_real">
					<br>
					<br>
					<!-- <img class="login_arrow pseudo_form_submit" src="<?= site_url('items/frontend/img/sidemenu_login_arrow.png');?>" action="login" source="login"> -->
					<div class="login_arrow_text pseudo_form_submit" action="login" source="login"><?= $this->lang->line('login_login_confirm')?></div>
					<?php echo form_close();?>	
				</div>
				
			</div>
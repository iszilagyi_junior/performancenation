<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']				= "Das Feld {field} ist ein Pflichtfeld.";
$lang['form_validation_isset']					= "Das Feld {field} muss einen Wert enthalten.";
$lang['form_validation_valid_email']			= "Das Feld {field} muss eine g&uuml;ltige E-Mail-Adresse enthalten.";
$lang['form_validation_valid_emails']			= "Das Feld {field} kann nur g&uuml;ltige E-Mail-Adressen enthalten.";
$lang['form_validation_valid_url']				= "Das Feld {field} muss eine g&uuml;ltige Web-Adresse (URL) enthalten.";
$lang['form_validation_valid_ip']				= "Das Feld {field} muss eine g&uuml;ltige IP enthalten.";
$lang['form_validation_min_length']				= "Das Feld {field} muss mindestens {param} Zeichen lang sein.";
$lang['form_validation_max_length']				= "Das Feld {field} darf nicht l&auml;nger als {param} Zeichen lang sein.";
$lang['form_validation_exact_length']			= "Das Feld {field} muss genau {param} Zeichen lang sein.";
$lang['form_validation_alpha']					= "Das Feld {field} darf nur Buchstaben enthalten.";
$lang['form_validation_alpha_numeric']			= "Das Feld {field} darf nur Buchstaben und/oder Zahlen enthalten.";
$lang['form_validation_alpha_numeric_spaces']	= 'Das Feld {field} darf nur Buchstaben, Zahlen und Leerzeichen enthalten.';
$lang['form_validation_alpha_dash']				= "Das Feld {field} darf nur Buchstaben, Zahlen, Bindestriche und Unterstriche enthalten.";
$lang['form_validation_numeric']				= "Das Feld {field} darf nur Zahlen enthalten.";
$lang['form_validation_is_numeric']				= "Das Feld {field} darf nur zahlen&auml;hnliche Zeichen enthalten.";
$lang['form_validation_integer']				= "Das Feld {field} darf nur ganze Zahlen enthalten.";
$lang['form_validation_regex_match']			= "Das Feld {field} ist nicht im richtigen Format.";
$lang['form_validation_matches']				= "Das Feld {field} muss mit dem Feld {param} &uuml;bereinstimmen.";
$lang['form_validation_differs']				= 'Das Feld {field} muss sich von dem Feld {param} unterscheiden.';
$lang['form_validation_is_unique'] 				= "Das Feld {field} muss einen einzigartigen Wert enthalten.";
$lang['form_validation_is_natural']				= "Das Feld {field} darf nur positive Zahlen enthalten.";
$lang['form_validation_is_natural_no_zero']		= "Das Feld {field} darf nur Zahlen gr&ouml;sser als Null enthalten.";
$lang['form_validation_decimal']				= "Das Feld {field} muss eine Dezimalzahl enthalten.";
$lang['form_validation_less_than']				= "Das Feld {field} muss eine Zahl kleiner als {param} enthalten.";
$lang['form_validation_less_than_equal_to']		= "Das Feld {field} muss eine Zahl kleiner oder gleich als {param} enthalten.";
$lang['form_validation_greater_than']			= "Das Feld {field} muss eine Zahl gr&ouml;sser als {param} enthalten.";
$lang['form_validation_greater_than_equal_to']	= "Das Feld {field} muss eine Zahl gr&ouml;sser oder gleich als {param} enthalten.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/german/form_validation_lang.php */

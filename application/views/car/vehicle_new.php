<div id="content" style="background:#000000; width:800px;left:200px;top: 260px;margin-bottom: 280px;">
	<div class="dosisextralight" style="font-size:30px;text-align:center;color:#fff;padding-top:20px;">Add a new vehicle below</div>
						<br><br>
						
						<div id="login_form" style="height:300px;">
							<?php 
								$data = array('id' => 'car_new_form');
								echo form_open("car/create", $data);
							?>
							<input type="text" id="create_car_name" name="create_car_name" value="" placeholder="Vehicle nickname" class="login_input" style="width: 226px;height: 26px;line-height: 26px;">
							<br>					
							<select id="vehicle_type_select" name="vehicle_type_select" class="login_input" style="">									
								<option value="car">Car</option>
								<option value="motorcycle">Motorcycle / U.T.V.</option>
								<option value="truck">Truck</option>
							</select>
							<br>
							<select id="vehicle_engine_select" name="vehicle_engine_select" class="login_input" style="">									
								<option value="0">Engine type...</option>
								<option value="gas">Gas</option>
								<option value="diesel">Diesel</option>
							</select>
							<br/>
							<select id="vehicle_year_select" name="vehicle_year_select" class="login_input" style="width:240px;">									
								<option value="0">Please select a year...</option>
								<? foreach($car_years as $year){?>
									<option value="<?= $year;?>"><?= $year?></option>
								<? }?>
							</select>
							<br>							
							<select id="vehicle_make_select" name="vehicle_make_select" class="login_input" style="text-transform:capitalize">									
								<option value="0">Make...</option>
								<option value="not">Not in the list</option>
							</select>
							<br>
							<select id="vehicle_model_select" name="vehicle_model_select" class="login_input" style="">									
								<option value="0">Model...</option>
								<option value="00">Not in the list</option>
							</select>
						<!--	
							<br/ style="clear:both;">
							<div id="dog_register_breed" style="">
								<div class="sansitalic" style="margin:5px auto;text-align: center;"><?= $this->lang->line('dog_create_breed')?></div>
								<input type="text" id="dog_breed" name="dog_breed" 
									value="<?php if(isset($dog_breed_repop) && $dog_breed_repop != "") echo $dog_breed_repop; else echo $this->lang->line('dog_new_breed')?>" 
									defaultText="<?= $this->lang->line('dog_new_breed')?>" 
									class="suggest_input <?php if(!isset($dog_breed_repop) || $dog_breed_repop == "") echo "input_default";?>">
							</div>
							<br style="clear:both;">
						-->
							<div class="button" id="add_new_car" style="width:240px;">ADD</div>
							<?php echo form_close();?>	
							<div id="car_create_error_message">
								
							</div>
						</div>
</div>
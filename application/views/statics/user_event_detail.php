<style>
	select{width:100%;}
</style>
<script>	
	

	$(document).ready(function()
	{	
		
		setTimeout(function(){
			initGroupPkry();
			//selectize_vehicle_search();
			initAutocomplete();
		},200);
		
		
		var event = "<?= $event_geo;?>";
	
		initMapEvent(event);
	

	});

	
</script>
<? if(!$is_ie){?>
	<div id="image_cropping_overlay">
		<div class="" id="crop_title" style="position:relative;top:0px;text-align: center;font-size:22px;">Cover Picture</div>
		<div class="sansitalic" style="margin:5px auto;text-align: center;">Please select an area</div>
		<div id="image_crop_holder">
			
		</div>
		 <input type="hidden" id="selection_x1" name="selection_x1" value="" />
		 <input type="hidden" id="selection_x2" name="selection_x2" value="" />
		 <input type="hidden" id="selection_y1" name="selection_y1" value="" />
		 <input type="hidden" id="selection_y2" name="selection_y2" value="" />
		<div class="button" type="user" eid="<?= $event->id;?>" id="finish_crop_event" image_type="" style="left:255px;position: absolute;">FINISH</div>
		<div class="button" id="close_crop"  style="left:545px;position: absolute;" >CLOSE</div>
	</div>
<? } ?>
<div id="content" style="width:800px;margin-bottom: 100px;color:#ffffff;">

	<div id="BkgEr" style=""></div>
			<? if($event->image != NULL && $event->image != ''){?>
				<img class="car_cover_photo" style="z-index:10000;" src="<?= site_url('items/uploads/coverpictures/'.$event->image);?>" border="0">
			
			<? }
				else
				{
			?>
				<div id="event_map"></div>
			<? }?>
		
			<?  if($im_admin){?>
				
				<script>	
	
					$(document).ready(function()
					{	
						$('#group_event_date').datetimepicker();
					});
					
				</script>
				
				<div id="group_event_overlay">
					<div class="overlayBG"></div>
					<div style="position:relative;z-index:100">
						<img class="close_overlay" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />
						<div class="titboldit" style="font-size:27px;margin-top:30px;">EDIT EVENT</div>
						<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">
				
				
				
				
					
						<table style="width:400px;margin:30px auto;">
							<tr>
								<td style="regular">
									Title
								</td>
								<td>
									<input type="text" placeholder="Title" class="edit_input" name="group_event_title" id="group_event_title" value="<?= $event->title; ?>"></input>
								</td>
							</tr> 
							
							<tr>
								<td style="regular">
									Description
								</td>
								<td>
									<textarea  placeholder="Description" class="edit_input" name="group_event_description" id="group_event_description" style="resize:none;height:60px;"><?= $event->description;?></textarea>
								</td>
							</tr>
							<tr>
								<td style="regular">
									Address
								</td>
								<td>
									<input type="text" name="group_address" id="autocomplete" onFocus="geolocate()" class="edit_input"  placeholder="Hangout address" style="" value="<?= $event->address; ?>"/>
								</td>
							</tr>
							
							<tr>
								<td style="regular">
									Date
								</td>
								<td>
									<input type="text" placeholder="Date" class="edit_input" name="group_event_date" id="group_event_date" value="<?= $event->start_time;?>"></input>
								</td>
							</tr>
							<div class="button" id="user_event_image" eid="<?= $event->id;?>">UPLOAD COVER</div>
						</table>
						<input type="file" preview="edit_user_cover_upload" accept="image/*" id="edit_user_cover_picture" name="edit_user_cover_picture" class="edit_hidden_upload">
					</div>	
					<div class="button" id="edit_event_button" eid="<?= $event->id;?>">SAVE</div>
					
					<div class="regular" id="group_event_error_message"></div>
					
					<div id="vehicle_delete_holder" style="margin:5px auto;left:2px;position:relative">					
						<div id="vehicle_delete" class="button" style="margin:5px 20px;">DELETE EVENT</div>
						<div id="vehicle_delete_confirm">
							Are you sure you wish to delete this event?<br/>
							<div id="event_delete_yes" eid="<?= $event->id;?>" class="button" style="width:80px;float: left;margin-left: 20px;">YES</div>
							<div id="vehicle_delete_no" class="button" style="width:80px;float:right;margin-right:20px;">NO</div>
						</div>
					</div>
					</div>
				</div>
				
				
				<div id="invite_overlay" style="top:0px;">
					<div class="overlayBG"></div>
					<div style="position:relative;z-index:100">
						<img class="close_overlay" src="<?= site_url('items/frontend/img/overlay_close.png')?>" />
						<div class="titboldit" style="font-size:27px;margin-top:30px;">INVITE TO EVENT</div>
						<div style="width:440px;margin:30px auto;border-top:2px dotted #ffffff; border-bottom:2px dotted #ffffff;">
						<div style="width:400px;margin:30px auto;">
								<div style="overflow:hidden;">
									<div class="button" style="float:left;width:180px;" id="invite_followers_group_button" gid="<?= $event->id;?>">INVITE ALL FOLLOWERS</div>						
									<div class="regular" style="float:left;margin-left:30px;margin-top:30px;">or add vehicles:</div>
								</div>
								
								<div style="position:relative;left: 0px;top: 0px;margin-bottom:20px;">
									<select id="invite_vehicle_select" name="invite_vehicle_select" placeholder="Type in name..."></select>
								</div>
								<div class="button" id="invite_event_button" gid="<?= $event->id;?>">SEND</div>
						</div>
					</div>
					<div class="regular" id="group_event_error_message"></div>
					</div>
				</div>
			
				
				<div class="button clone_event" eid="<?= $event->id;?>" style="">CLONE</div>
				
				<div class="button edit_event" style="">EDIT</div>
				
				<div class="button user_invite_group" style="">INVITE</div>
				
			<!--	<div class="button delete_event" style="">DELETE EVENT</div> -->
			
			<? }
				else{
				?>
				
					<div id="going_event" vid="<?= $my_vehicle_id;?>" cid="<?= $event->id;?>" class="button user_event_decision" status="<?= GROUP_EVENT_GOING;?>" style="<? if($joined_event){ echo 'display:none;';}?>">Join Event</div>
				
					<div id="not_going_event" vid="<?= $my_vehicle_id;?>" cid="<?= $event->id;?>" class="button user_event_decision" status="<?= GROUP_EVENT_NOT;?>" style="display:none;<? if($joined_event){ echo 'display:block';}?>">Leave Event</div>
			<? }?>	
				
				
				<br/>
				
				<div class="car_detail_nickname playbold"><?= $event->title;?></div>
				<div class="car_detail_make regular"><?= $event->address;?></div>
				<div class="car_detail_owner dosissemi"><?= date('F j, Y, \a\t g:i a',strtotime($event->start_time));?></div>		
				
				<div style="overflow:hidden;margin-top:30px;min-height:400px;">
					<div class="detail_left">
						<div class="user_section" style="width:100%;">
							<div class="dosisextralight"><?= nl2br($event->description);?></div>
						</div>
					</div>
					
					<div class="detail_right">
						<div class="dosissemi">PARTICIPANTS</div>
						<div id="gallery_images">
							<? foreach($vehicles_joined as $member):?>
								<a href="<?= site_url('vehicle_profile/'.$member['pretty_url']);?>">
									<div class="member_item">
										<img class="member_item_img" src="<?= site_url('items/uploads/profilepictures/'.$member['profile_image']);?>" />
									<!--	<div class="member_item_title"><?= $member->username;?></div> -->
									</div>
									
								</a>
							<? endforeach;?>
						</div>
						
						<? if($event->image != NULL && $event->image != ''){?>
							<div id="event_map" style="width:360px;height:280px;margin-top:20px;"></div>
						<? }?>
					</div>
					<br/>
				</div>	
					
				
</div>
<script>	
	$('#sidemenu').css({top: 0});
</script>

			<div id="content">
				<div class="statics_subheader" style="margin-top:35px;"><?= $this->lang->line('sidemenu_static_dogzone')?></div>
				<div class="sansitalic" style="text-align:center;margin:20px 0px;"><?= $this->lang->line('zone_search')?></div>
				<hr class="hline">
				
				
				
				<div style="height:55px;">
					<div id="closest_zone_btn_text" style=""><?= $this->lang->line('closest_zones')?></div>
					<div id="closest_zone_btn_search" style="margin-right:15px;"><?= $this->lang->line('profile_search_submit')?></div>
					<input type="text" id="zone_location_input" name="zone_location_input" placeholder="<?= $this->lang->line('location')?>" defaultText="<?= $this->lang->line('location')?>" >

				</div>
				<div class="separator" style="width:172px;height:26px;clear:both;">
						<hr class="hline" style="float:left;margin-top:27px;width:50px">
						<span class="sansitalic" style="float:left;margin:10px 20px;margin-top:20px;"><?= $this->lang->line('register_or')?></span>
						<hr class="hline" style="float:left;margin-top:27px;width:50px">
					</div>
					<div style="width:100%;height:50px;overflow:hidden">
						<input type="text" id="zone_search" name="zone_search" placeholder="<?= $this->lang->line('zone_name')?>" defaultText="<?= $this->lang->line('zone_name')?>" class="search_input">
						<div id="closest_zone_name_search" style="margin-right:15px;"><?= $this->lang->line('profile_search_submit')?></div> 
					</div>
				<div class="sansitalic" style="text-align:center;margin:30px 0px;"><?= $this->lang->line('results')?></div>
				<div id="vets_list"></div>
				<? if($loggedin){?>
				<div>
					<div class="statics_subheader" style="margin-top:15px;font-size:15px;height:40px;line-height:40px;"><?= $this->lang->line('cant_find')?></div>
					<div class="sansitalic" style="text-align:center;margin:10px 0px;"><?= $this->lang->line('suggest_new')?></div>
				</div>
												
					<div id="suggest_zone_holder">
						<span class="content_header" style=""><?= $this->lang->line('suggest_zone')?></span>
						<?php 
							$data = array('id' => 'suggest_zone_form');
							echo form_open("statics/suggest_zone", $data);?>
								<input type="text" id="zone_name" name="zone_name" placeholder="<?= $this->lang->line('suggest_zone_name')?>" class="suggest_input" style="" />
								<input type="text" id="zone_address" name="zone_address" placeholder="<?= $this->lang->line('suggest_zone_address')?>" class="suggest_input" style="margin-right:0px;" />							
								<textarea type="text" id="zone_description" name="zone_description" placeholder="<?= $this->lang->line('suggest_zone_about')?>" class="suggest_input" style="resize:none;height:81px;"></textarea>
								<input type="text" id="zone_size" name="zone_size" placeholder="<?= $this->lang->line('suggest_zone_area')?>" class="suggest_input" style="margin-right:0px;" />
										
							<?php echo form_close();?>
						
							<div id="send_suggestion" style=""><?= $this->lang->line('send_suggestion')?></div>
									
	
					</div>
					<div id="edit_error_message">
						<?php if(isset($message)) echo $message;?>
					</div>
				<? }?>
		
			</div>
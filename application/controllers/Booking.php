<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
		$this->load->model('booking_model');
    }
	/*********************************************************************************************************
	 * LOAD CALENDAR WITH BOOKING
	 *********************************************************************************************************/
	function loadSitterCalendar()
	{
		//TODO: purify
		$this->load->helper('url');
		$user_id =  $this->uri->segment(3);
		
		/*if($user_id == FALSE)
		{
			$user_id = $this->session->userdata('user_id');
		}*/
		$startdate = $_GET['startdate'];
		$change = $_GET['change'];
		$style = $_GET['style'];
		
		$this->load->library('MY_booking');
		
		$data = $this->my_booking->getSitterCalendar($user_id, $this->my_booking->calculateNewStartdate($startdate, $change, $style), true);
		
		echo json_encode(array(	'success' => true,
								'user' => $user_id,
								'message' => 'success',
								'data' => $data));		
	}	


	/*********************************************************************************************************
	 * BOOKING OVERLAYS
	 *********************************************************************************************************/
	public function avail_overlay()
	{
		//TODO: purify
		$data['date'] = $_GET['date'];
		$data['weekday'] = date('l', strtotime($data['date']));
		
		echo json_encode(array('success' => true,
							   'message' => "success",
							   'data' => $this->load->view('booking/avail', $data, true)));				
	}
	
	
	public function avail_overlay_user()
	{
		//TODO: purify
		$data['date'] = $_GET['date'];
		$data['weekday'] = date('l', strtotime($data['date']));
		
		
		
		echo json_encode(array('success' => true,
							   'message' => "success",						  
							   'data' => $this->load->view('booking/avail_user', $data, true)));				
	}
	
	public function availedit_overlay()
	{
		//TODO: purify
		$data['avail_id'] = $_GET['avail_id'];
		
		$data['the_date'] = $_GET['date'];
		$data['weekday'] = date('l', strtotime($data['the_date']));
		
		
		$timeslot = $this->booking_model->getSitterAvailabilityByID($data['avail_id']);
		$data['dog_maxcount'] = $timeslot->row()->dog_limit;
		$data['date'] = date('Y-m-d', strtotime($timeslot->row()->date));
		$data['weekday'] = date('l', strtotime($data['date']));
		echo json_encode(array('success' => true,
							   'message' => "success",
							   'data' => $this->load->view('booking/avail_edit', $data, true)));				
	}	
	
	public function exception_overlay()
	{
		//TODO: purify
		$data['exception_id'] = $_GET['exception_id'];
		
		$exception = $this->booking_model->getSitterException($data['exception_id']);
		$data['reason'] = $exception->row()->description;
		$data['date'] = $exception->row()->date; 
		$data['the_date'] = $_GET['date'];
		$data['weekday'] = date('l', strtotime($data['the_date']));
		echo json_encode(array('success' => true,
							   'message' => "success",
							   'data' => $this->load->view('booking/exception', $data, true)));		
	}
	
	
	/*********************************************************************************************************
	 * BOOKING CRUDS
	 *********************************************************************************************************/
	public function add_avail()
	{
		//TODO: check if currently logged in user is allowed to edit this booking
		
		//TODO: purify
		$date = $_POST['avail_overlay_date'];
		$recurring = $_POST['avail_overlay_recurring'];
		$dogcount = $_POST['avail_overlay_dogcount'];
		$user_id = $this->session->userdata('user_id');
		
		if($recurring != 'recurring' && $recurring != 'date_specific')
			echo json_encode(array(	'success' => false,
									'message' => 'recurring not valid'));
		else 
		{
			if($recurring == 'recurring')
				$recurring = 1;
			else 
				$recurring = 0;
			
			$this->load->library('MY_booking');
			
			$data = array
			(
				'user_id' => $user_id,
				'weekday_id' => $this->my_booking->getWeekday($date),
				'starttime' => '00:00:00',
				'endtime' => '00:00:00',
				'allday' => 1,
				'recurring' => $recurring,
				'date' => $date,
				'dog_limit' => $dogcount,
				'modified_by' => 0,
				'modified_date' => null,
				'created_by' => $user_id,
				'created_date' => date('Y-m-d H:i:s')
			);
			$this->booking_model->insertSitterAvailability($data);			
			
			echo json_encode(array('success' => true,
								   'message' => 'availability added'));
		}									
									
		
	}
	
	public function book_sitter()
	{
		$this->load->helper('url');		
		//TODO: purify
		$date = $_POST['avail_overlay_date'];
		$message = $_POST['avail_overlay_message'];
		$dogcount = $_POST['avail_overlay_dogcount'];
		$sitter_id = $this->uri->segment(3);
		$user_id = $this->session->userdata('user_id');
		$this->load->library('MY_booking');
			
			$data = array
			(
				'user_id' => $user_id,
				'sitter_id' => $sitter_id,
				'starttime' => '00:00:00',
				'endtime' => '00:00:00',
				'allday' => 1,
				'status_id' => 5,
				'message' => $message,
				'date' => $date,
				'num_dogs' => $dogcount,
				'modified_by' => 0,
				'modified_date' => null,
				'created_by' => $user_id
			);
			$this->booking_model->insertBookingRequest($data);			
			
			echo json_encode(array('success' => true,
								   'message' => 'Booking request sent'));
											
			/*
				(select count(`sitter_booking_dogs`.`id`) from `sitter_booking_dogs` where (`sitter_booking_dogs`.`booking_id` = `sb`.`id`))
			*/						
		
	}
	
	
	public function delete_avail()
	{
		//TODO: check if currently logged in user is allowed to edit this booking
		
		//TODO: purify
		$avail_id = $_POST['avail_id'];

		$this->booking_model->deleteSitterAvailabilityByID($avail_id);
		
		echo json_encode(array('success' => true,
							   'message' => 'availability deleted'));
	} 
 
	public function edit_avail()
	{
		//TODO: check if currently logged in user is allowed to edit this booking
		
		//TODO: purify
		$avail_id = $_POST['availedit_overlay_avail_id'];
		$max_dogcount = $_POST['availedit_overlay_dogcount'];
		
		$this->booking_model->updateSitterAvailability($avail_id, array('dog_limit' => $max_dogcount));

		echo json_encode(array('success' => true,
							   'message' => 'availability updated'));			 		
	}

	public function add_exception()
	{
		//TODO: check if currently logged in user is allowed to edit this booking
		
		//TODO: purify
		$date = $_POST['availedit_overlay_date'];		
		$reason = $_POST['availedit_overlay_exceptionreason'];
		$user_id = $this->session->userdata('user_id');
		
		$data = array(	'sitter_id' => $user_id,
						'date' => $date,
						'starttime' => '00:00:00',
						'endtime' => '00:00:00',
						'allday' => 1,
						'description' => $reason,
						'modified_by' => 0,
						'modified_date' => null,
						'created_by' => $user_id,
						'created_date' => date('Y-m-d H:i:s'));
		
		$this->booking_model->insertSitterException($data);	
		
		echo json_encode(array('success' => true,
							   'message' => 'exception added'));			
	} 

	public function edit_exception()
	{
		//TODO: check if currently logged in user is allowed to edit this booking
		
		//TODO: purify
		$exception_id = $_POST['exception_overlay_exception_id'];		
		$reason = $_POST['exception_overlay_reason'];
		$user_id = $this->session->userdata('user_id');
		
		$this->booking_model->updateSitterException($exception_id, array('description' => $reason));
		
		echo json_encode(array('success' => true,
							   'message' => 'exception updated'));				
	}
	
	public function delete_exception()
	{
		//TODO: check if currently logged in user is allowed to edit this booking
		
		//TODO: purify
		$exception_id = $_POST['exception_id'];		

		$this->booking_model->deleteSitterException($exception_id);
		
		echo json_encode(array('success' => true,
							   'message' => 'exception updated'));				
	}

	/*********************************************************************************************************
	 * SITTER REQUEST ACTIONS
	 *********************************************************************************************************/
	public function accept_request()
	{
		//TODO: check if currently logged in user is allowed to edit this booking
		
		//TODO: purify		
		$request_id = $_POST['request_id'];
		$user_id = $this->session->userdata('user_id');	
		
		$this->booking_model->acceptSitterRequest($request_id);
		
		//TODO: email about request change
		
		$this->load->library('MY_booking');
		
		echo json_encode(array('success' => true,
							   'message' => 'request accepted',
							   'data' => $this->my_booking->renderSitterControls($user_id)));
							   			
	}
	
	public function decline_request()
	{
		//TODO: check if currently logged in user is allowed to edit this booking
		
		//TODO: purify		
		$request_id = $_POST['request_id'];
		$user_id = $this->session->userdata('user_id');	
		
		$this->booking_model->declineSitterRequest($request_id);
		
		//TODO: email about request change
		
		$this->load->library('MY_booking');
		
		echo json_encode(array('success' => true,
							   'message' => 'request accepted',
							   'data' => $this->my_booking->renderSitterControls($user_id)));
							   			
	}
	
	
	public function remove_request()
	{
		//TODO: check if currently logged in user is allowed to edit this booking
		
		//TODO: purify		
		$request_id = $_POST['request_id'];
		$user_id = $this->session->userdata('user_id');	
		
		$this->booking_model->removeSitterRequest($request_id);
		
		//TODO: email about request change
		
		$this->load->library('MY_booking');
		
		echo json_encode(array('success' => true,
							   'message' => 'request accepted',
							   'data' => $this->my_booking->renderSitterControls($user_id)));
							   			
	}
	
	
	/*****************************************************************************************************
	 * USER ACTIONS
	 ******************************************************************************************************/
	public function sittersearch()
	{
		//TODO: purify		
		$startdate = strtotime($_GET['sittersearch_from']);
		$enddate = strtotime($_GET['sittersearch_to']);
		$dogcount = $_GET['sittersearch_dogcount'];
		
		$this->load->library('MY_booking');
		
		$sitters = $this->my_booking->sitterSearch($startdate, $enddate, $dogcount);
	
		$data['sitters'] = $this->booking_model->getSitters($sitters);
		
		echo json_encode(array('success' => true,
							   'message' => 'results returned',
							   'data' => $this->load->view('booking/sitter_search_results', $data, true)));		
		
	}	 
		
}
$(document).ready(function() {
	addListeners();	
	
	
});

function addListeners(){
	
	$('.accept_grp_request').click(function(){
	
		var request_id = $(this).attr('rid');
		var user_id = $(this).attr('uid');
		var $this = $(this);
		$this.parent().parent().fadeOut();	
		$.post(rootUrl+'statics/accept_new_group', {rid:request_id, uid: user_id}, function(data) 
		{	
						
			
			
		});
	});
	
	$('.decline_grp_request').click(function(){
	
		var request_id = $(this).attr('rid');
		var $this = $(this);
		$this.parent().parent().fadeOut();	
		$.post(rootUrl+'statics/decline_new_group', {rid:request_id}, function(data) 
		{	
						
			
			
		});
	});
	
	
	
	
	
	
	$('.overwatch_img').click(function(){
	
		var left = $(this).offset().left;
		var top = $(this).offset().top;		
		var pid = $(this).attr("pid");
			
		var typ = $(this).attr('typ'); 
			
		$('#delete_editor').css({left:left, top:top}).fadeIn();
		$('.photo_delete_yes').attr('pid', pid);
			
		$('.photo_delete_yes').attr('typ', typ);
		
	});
	
	
	$('.photo_delete_no').click(function(){			
		var $this = $(this);					
		$this.parent().parent().fadeOut();
	});
	
	$('#close_edit').click(function(){
		$('#comment_edit_holder').fadeOut();
	});
	
	
	$('.photo_delete_yes').click(function(){
	
		var pic_id = $(this).attr('pid');
		var typ = $(this).attr('typ');
		var $this = $(this);
		$this.parent().parent().fadeOut();	
		$.post(rootUrl+'backend/overwatchDeleteImage', {pid:pic_id, type:typ}, function(data) 
		{	
						
			
			$('.overwatch_img[pid="'+pic_id+'"]').hide();
		});
	});
	
	$('.comment_overwatch_item').click(function(){
		var cid = $(this).attr('cid');
		var text = $(this).find('.comment_content').text();
		var left = $(this).offset().left;
		var top = $(this).offset().top;
		
		$('.comment_delete_yes').attr('cid', cid);
		//$('#edited_text').val(text);
		$('#delete_editor').css({left:left, top:top}).fadeIn();
	});
	
	$('.comment_delete_no').click(function(){			
		var $this = $(this);					
		$this.parent().parent().fadeOut();
	});
	
	$('.comment_delete_yes').click(function(){
	
		var comment_id = $(this).attr('cid');
		var $this = $(this);
		$this.parent().parent().fadeOut();	
		$.post(rootUrl+'backend/deleteComment', {cid:comment_id}, function(data) 
		{	
	
			$('.comment_overwatch_item[cid="'+comment_id+'"]').hide();
			
		});
	});
}

function getNewImages()
{
	
	setInterval(function(){
		
		window.location.href = rootUrl+'backend/image_overwatch';
	},30000);
}

function getNewComments()
{
	
	setInterval(function(){
		
		window.location.href = rootUrl+'backend/comment_overwatch';
	},35000);
}
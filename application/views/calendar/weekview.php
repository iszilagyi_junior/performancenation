	
	<script src="<?= site_url("items/frontend/js/calendar.js");?>" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/calendar.css"); ?>">	

	<div id="calendar" class="<?= $format['container_class']?>">
		<div class="calendar_header">
			<div id="calender_prev" class="buttons prevbutton">
				<<
			</div>
			<div id="calender_header_text" class="caption">
				<?= $calendar_header ?>
			</div>
			<div id="calender_prev" class="buttons nextbutton">
				>>
			</div>
		</div>
		<div id="calendar_content">
			<table id="calendar_table" class="<?= $format['calendar_class']?> calendar_basic">
				<thead>
					<th class="rowheader_empty"></th>
					<?php for($i = 0; $i < 7 ; $i++): ?>
						<th><?= date('D n/j', strtotime($startdate . ' + ' . $i . ' days'))?></th>
					<?php endfor; ?>
				</thead>
				<tbody>
					<?php for($i = 0; $i < $format['row_count'] ; $i++): ?>
						<tr class="row">
							<td class="rowheader"><?= $rowheaders[$i]  ?></td>
							<?php for($j = 0 ; $j < 7 ; $j++) :?>
								
								<?php if(isset($timeslots[$j][$i])):?>
									<td class="<?= $timeslots[$j][$i]['class']?>" <?= $timeslots[$j][$i]['additional_str']?>></td>
								<?php else:?>
									<td></td>
								<?php endif;?>
								
							<?php endfor; ?>	
						</tr>
					<?php endfor; ?>
				</tbody>
			</table>
		</div>
		<div id="calendar_footer">
			
		</div>
	</div>


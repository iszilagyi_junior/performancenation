<div id="content" style="top:270px;color:#ffffff;min-height:400px;margin-bottom:150px;">
	<div id="register_header" class="dosisbold" style="font-size:25px;">/////////////////////////////// RESET PASSWORD ///////////////////////////////</div>
	
	<div id="infoMessage" style="text-align:center;"><?php echo $message;?></div>
	
	<div style="margin-top:20px;">
	<?php echo form_open('Auth/reset_password/' . $code);?>
	
		<p>
			<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label> <br />
			<input type="password" name="new" value="" id="new" pattern="^.{8}.*$" class="edit_input">
			
		</p>
	
		<p>
			<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?> <br />
			<input type="password" name="new_confirm" value="" id="new_confirm" pattern="^.{8}.*$" class="edit_input">
		</p>
	
		<input type="hidden" name="name" value="user_id">
		<input type="hidden" name="id" value="user_id">
		<input type="hidden" name="type" value="hidden">
		<input type="hidden" name="user_id" value="<?= $uid?>">
		<?php echo form_hidden($csrf); ?>
	
		<p><input type="submit" class="button regular" action="login" source="login" style="margin-top:10px;line-height:25px;" value="<?= lang('reset_password_submit_btn')?>"></p>
	
	<?php echo form_close();?>
	</div>
</div>



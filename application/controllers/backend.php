<?php 
//error_reporting(E_NOTICE);
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend extends MY_Controller 
{

	public $test_param;

    function __construct()
    {
        parent::__construct();
		
	//	$this->checkAdmin();
        
		$this->load->library('Grocery_CRUD');
	    $this->load->library('image_crud');
        $this->load->model('backend_model');
        $this->load->model('statics_model');
        $this->load->model('car_model');
		
    }
 
    public function index()
    {
		if($this->ion_auth->logged_in())
		{
			$this->load->view('backend/login.php');		
		}
		else redirect ('login/sidemenu');
    }   
	
	public function login()
	{
		$password = $this->input->post('pass');
		if ($password == "Kkhenryhenrypn") {
			$data['preview'] = true;
			$data['admin'] = true;
			$this->session->set_userdata('previewVerified', true);
			$this->session->set_userdata('isAdmin', true);
			$this->load->view('backend/header.php');	
			$this->load->view('backend/footer.php');
			
			redirect('backend/menu');
			
			return;
		} else {
			redirect('backend');
		}
	}
	
	
	public function menu()
	{
		if($this->ion_auth->logged_in())
		{
			$this->load->view('backend/header.php');	
			$this->load->view('backend/menu.php');
			$this->load->view('backend/footer.php');		
		}
		else $this->js_logout();
		
	}
	
	public function users()
	{
		if($this->ion_auth->logged_in())
		{
			$data['users'] = $this->statics_model->getUsers()->result();
			
			$data['sumUsers'] = count($data['users']);
			$this->load->view('backend/header.php');	
			$this->load->view('backend/users.php', $data);
			$this->load->view('backend/footer.php');		
		}
		else $this->js_logout();
		
	}
	
	public function users_suggestions()
	{
		if($this->ion_auth->logged_in())
		{
			$crud = new grocery_CRUD();
			
			$crud->set_table('suggestions');
			$crud->set_subject('Suggestions');
			$crud->columns('text', 'created_date', 'ip_address');
			$crud->display_as('registration_string','Code');
			$crud->display_as('counter','Counter');
			
            
            $crud->required_fields('registration_string');
         
			$crud->field_type('counter', 'readonly');
		      
            $output = $crud->render();
            $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
		}
		else $this->js_logout();
		
	}
	
	
	public function promo_codes()
	{
		if($this->ion_auth->logged_in())
		{
			$crud = new grocery_CRUD();
			
			$crud->set_table('registration_codes');
			$crud->set_subject('Promo code');
			$crud->columns('registration_string', 'counter');
			$crud->display_as('registration_string','Code');
			$crud->display_as('counter','Counter');
			
            
            $crud->required_fields('registration_string');
         
			$crud->field_type('counter', 'readonly');
		      
            $output = $crud->render();
            $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
		}
		else $this->js_logout();
	}
	
	public function reports()
	{
		if($this->ion_auth->logged_in())
		{
			$crud = new grocery_CRUD();
			
			$crud->set_table('reports');
			$crud->set_subject('Reports');
			$crud->columns('entity_id', 'type', 'user_id');
			
			$crud->field_type('user_id', 'readonly');
			$crud->field_type('entity_id', 'readonly');
			$crud->field_type('type', 'readonly');
			$crud->field_type('report_date', 'readonly');
            
            $crud->unset_add();
         
			
		      
            $output = $crud->render();
            $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
		}
		else $this->js_logout();
	}
	
	public function poi_suggestions()
	{
		if($this->ion_auth->logged_in())
		{
			$crud = new grocery_CRUD();
			
			$crud->set_table('poi_suggestions');
			$crud->set_subject('POI Suggestion');
			$crud->columns('name', 'description', 'address');
			//$crud->set_relation('category_id','poi_categories','name');
			$crud->set_relation_n_n('categories', 'poi_category_connection', 'poi_categories', 'poi_id', 'category_id', 'name');
		/*	$crud->field_type('name', 'readonly');
			$crud->field_type('category_id', 'readonly');
			$crud->field_type('description', 'readonly');
			$crud->field_type('address', 'readonly');
			$crud->field_type('created_by', 'readonly');
        */    
            $crud->unset_add();
         
			
		      
            $output = $crud->render();
            $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
		}
		else $this->js_logout();
	}
	
	function add_password_vet($post_array,$primary_key)
	{
		$rnd = $this->rand_string(12);
		
		
		
		$password = $this->my_hash($rnd);
	    $user_insert = array(    
	        "login_pw" => $password,      
	    );
		
		
		$name = $post_array['title']." ".$post_array['firstname']." ".$post_array['lastname'];
		
		$this->db->where('id', $primary_key);
	    $this->db->update('vets',$user_insert);
		
		//Insert pw sending email HERE
		
		$message = $this->send_mail("vet_registration", $post_array['login_email'], $name, $rnd);
		
	    return true;
	    
	    
	}
	
	
    public function vet_images($vet_id)
    {
        if($this->ion_auth->logged_in())
        {
            $crud = new grocery_CRUD();
            $crud->set_table('images');
			$resultset = $this->backend_model->getVetPictures($vet_id);
            $crud->where($this->getImageWhereClause($resultset));
            $crud->set_subject('Bilder');
			$crud->columns('fname');
			$crud->set_field_upload('fname','items/uploads/images');
			
			$crud->unset_edit();
			$crud->unset_read();
			
			$this->test_param = $vet_id;
			$crud->callback_after_insert(array($this, 'insert_vet_images'));
			$crud->callback_after_delete(array($this, 'delete_vet_images'));
			
			$crud->field_type('modified_by', 'hidden', $this->session->userdata('user_id'));
			$crud->field_type('created_by', 'hidden', $this->session->userdata('user_id'));
			$crud->unset_add_fields('modified_by','created_date', 'modified_date');
			$crud->unset_edit_fields('created_by','created_date', 'modified_date');
						
			$output = $crud->render();
             $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
        }
        else $this->js_logout();
    } 
	public function insert_vet_images($post_array,$primary_key)
	{
		$this->backend_model->insertVetsImages($this->test_param, $primary_key);
	}
	public function delete_vet_images($primary_key)
	{
		$this->backend_model->deleteVetsImages($this->test_param, $primary_key);
	}	

	public function breeds()
	{
		if($this->ion_auth->logged_in())
		{
			$crud = new grocery_CRUD();

			$crud->set_table('breeds');
			$crud->set_subject('Hunderassen');
			$crud->columns('name', 'accepted', 'teaser_picture', 'cover_picture', 'content' );

			$crud->display_as('name','Name');
			$crud->display_as('name_de','Name German');
			$crud->display_as('teaser_picture','Teaser Picture');
			$crud->display_as('cover_picture','Cover Picture');
            $crud->display_as('content','About');
            $crud->display_as('accepted','Accepted');
			
            $crud->required_fields('name');

            $crud->set_field_upload('teaser_picture','items/uploads/profilepictures');
            $crud->set_field_upload('cover_picture','items/uploads/coverpictures');
			$crud->field_type('accepted','dropdown',
            array('0' => 'Inactive', '1' => 'Accepted'));

			$crud->add_action('Eidt pictures', site_url('items/backend/img/gallery.png'), 'backend/breed_images');  
			       
            $output = $crud->render();
			 $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
		}
		else $this->js_logout();
	}
    public function breed_images($vet_id)
    {
        if($this->ion_auth->logged_in())
        {
            $crud = new grocery_CRUD();
            $crud->set_table('images');
			$resultset = $this->backend_model->getBreedPictures($vet_id);
            $crud->where($this->getImageWhereClause($resultset));
            $crud->set_subject('Bilder');
			$crud->columns('fname');
			$crud->set_field_upload('fname','items/uploads/images');
			
			$crud->unset_edit();
			$crud->unset_read();
			
			$this->test_param = $vet_id;
			$crud->callback_after_insert(array($this, 'insert_breed_images'));
			$crud->callback_after_delete(array($this, 'delete_breed_images'));
			
			$crud->field_type('modified_by', 'hidden', $this->session->userdata('user_id'));
			$crud->field_type('created_by', 'hidden', $this->session->userdata('user_id'));
			$crud->unset_add_fields('modified_by','created_date', 'modified_date');
			$crud->unset_edit_fields('created_by','created_date', 'modified_date');
						
			$output = $crud->render();
             $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
        }
        else $this->js_logout();
    } 
	public function insert_breed_images($post_array,$primary_key)
	{
		$this->backend_model->insertBreedsImages($this->test_param, $primary_key);
	}
	public function delete_breed_images($primary_key)
	{
		$this->backend_model->deleteBreedsImages($this->test_param, $primary_key);
	}	

	public function dogzones()
	{
		if($this->ion_auth->logged_in())
		{
			
			$crud = new grocery_CRUD();
			$crud->set_table('dogzones');
			$crud->set_subject('Hundezonen');
			$crud->columns('name', 'address','size', 'description');
			$crud->display_as('name','Name');
			$crud->display_as('description','Beschreibung');
            $crud->required_fields('name');
            $crud->set_field_upload('profilepicture','items/uploads/profilepictures');
			$crud->set_field_upload('coverpicture','items/uploads/coverpictures');
			$crud->field_type('modified_by', 'hidden', $this->session->userdata('user_id'));
			$crud->field_type('created_by', 'hidden', $this->session->userdata('user_id'));
			
			$crud->unset_add_fields('modified_by','created_date', 'modified_date');
			$crud->unset_edit_fields('created_by','created_date', 'modified_date');
			$crud->add_action('Bilder bearbeiten', site_url('items/backend/img/gallery.png'), 'backend/vet_images');         
            $output = $crud->render();
			 $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');

		}
		else $this->js_logout();
	}
	
	
	public function poi_categories()
	{
		if($this->ion_auth->logged_in())
		{
			
			$crud = new grocery_CRUD();
			$crud->set_table('poi_categories');
			$crud->set_subject('POI Category');
			$crud->columns('name');
			$crud->display_as('name','Name');
			
            $crud->required_fields('name');
                     
            $output = $crud->render();
			 $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');

		}
		else $this->js_logout();
	}
	
	
	public function managed_group_categories()
	{
		if($this->ion_auth->logged_in())
		{
			
			$crud = new grocery_CRUD();
			$crud->set_table('managed_group_categories');
			$crud->set_subject('Club Category');
			$crud->columns('name');
			$crud->display_as('name','Name');
			
            $crud->required_fields('name');
                     
            $output = $crud->render();
			 $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');

		}
		else $this->js_logout();
	}
	
	
	public function user_event_categories()
	{
		if($this->ion_auth->logged_in())
		{
			
			$crud = new grocery_CRUD();
			$crud->set_table('user_created_event_categories');
			$crud->set_subject('Hangout Category');
			$crud->columns('name');
			$crud->display_as('name','Name');
			
            $crud->required_fields('name');
                     
            $output = $crud->render();
			 $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');

		}
		else $this->js_logout();
	}
	
	
	
	public function zone_suggestion()
	{
		if($this->ion_auth->logged_in())
		{
			$crud = new grocery_CRUD();
			$crud->set_table('zone_suggestions');
			$crud->set_subject('Suggestions');
			$crud->columns('name', 'address','size', 'description');
			$crud->display_as('name','Name');
			$crud->display_as('description','Description');           
			$crud->field_type('modified_by', 'hidden', $this->session->userdata('user_id'));
			$crud->field_type('created_by', 'hidden', $this->session->userdata('user_id'));
			$crud->unset_add();
			$crud->unset_add_fields('modified_by','created_date', 'modified_date');
			$crud->unset_edit_fields('created_by','created_date', 'modified_date');
			        
            $output = $crud->render();
			$this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
		}
		else $this->js_logout();
	}
	
	public function cities()
	{
		if($this->ion_auth->logged_in())
		{
			$crud = new grocery_CRUD();
			$crud->set_table('cities');
			$crud->set_subject('City');
			$crud->columns('name');
			$crud->display_as('name','Name');
            $crud->required_fields('name');       
            $output = $crud->render();
			 $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
		}
		else $this->js_logout();
	}
	
	public function image_overwatch()
	{
		if($this->ion_auth->logged_in())
		{
			$images = $this->backend_model->getImages()->result();
			$users = $this->backend_model->getUsers()->result();
			$dogs = $this->backend_model->getDogs()->result();
			
			$image_ids = array();
			$image_holder = array();
			
			
			
			
			foreach($users as $user)
			{
				if($user->profile_picture !== NULL)
				{
					$date = date("Y-m-d H:i:s", $user->created_on);
					$image_item = array('id' => $user->id,
								'fname' => $user->profile_picture,
								'type' => 'user',
								'folder' => 'profilepictures',
								'cdate' => $date);
					$image_holder[] = $image_item;
				}
				
			}
			
			foreach($dogs as $dog)
			{
				if($dog->profile_picture != "default.png")
				{
					if(is_numeric($dog->profile_picture))
					{
						$dogprofile = $this->statics_model->getImageByID($dog->profile_picture)->row()->fname;
						$cdate = $this->statics_model->getImageByID($dog->profile_picture)->row()->created_date;
						
						if(!in_array($dog->profile_picture, $image_ids))
						{
							$image_item = array('id' => $dog->profile_picture,
										'fname' => $dogprofile,
										'type' => 'profile',
										'folder' => 'profilepictures',
										'cdate' => $cdate);
							$image_holder[] = $image_item;
							$image_ids[] = $dog->profile_picture;
						}
					}
					else
					{
						$dogprofile = $dog->profile_picture;
						
						$image_item = array('id' => $dog->id,
										'fname' => $dogprofile,
										'type' => 'profile',
										'folder' => 'profilepictures',
										'cdate' => $dog->created_date);
						$image_holder[] = $image_item;
					}
					
					
				}
				
				
				if($dog->cover_picture != "default.png")
				{
					if(is_numeric($dog->cover_picture))
					{
						$dogcover = $this->statics_model->getImageByID($dog->cover_picture)->row()->fname;
						$cdate = $this->statics_model->getImageByID($dog->profile_picture)->row()->created_date;
						
						if(!in_array($dog->cover_picture, $image_ids))
						{
							$image_item = array('id' => $dog->cover_picture,
								'fname' => $dogcover,
								'type' => 'cover',
								'folder' => 'coverpictures',
								'cdate' => $cdate);
							$image_holder[] = $image_item;
							$image_ids[] = $dog->cover_picture;
						}
					}
					else
					{
						$dogcover = $dog->cover_picture;
						$image_item = array('id' => $dog->id,
								'fname' => $dogcover,
								'type' => 'cover',
								'folder' => 'coverpictures',
								'cdate' => $dog->created_date);
						$image_holder[] = $image_item;
					}
					
				}
			}
			
			
			foreach($images as $image)
			{
				if(!in_array($image->id, $image_ids))
				{
					
					$image_item = array('id' => $image->id,
								'fname' => $image->fname,
								'type' => 'image',
								'cdate' => $image->created_date);
					
					if($image->cover_image == 1)	
					{
						$image_item['folder'] = 'coverpictures';
					}
					else if($image->profile_image == 1)
					{
						$image_item['folder'] = 'profilepictures';
					}
					else
					{
						$image_item['folder'] = 'images';
					}			
					
					$image_holder[] = $image_item;					
					$image_ids[] = $image->id;
				}
				
				
			}
			
			
			usort($image_holder, function($a, $b) {
			    return strtotime($b['cdate']) - strtotime($a['cdate']);
			});
			
			//var_dump($image_ids);
			$data['images'] = $image_holder;
			
			$this->load->view('backend/image_overwatch.php',$data);
		}
		else $this->js_logout();
	}
	
	public function comment_overwatch()
	{
		if($this->ion_auth->logged_in())
		{
			$comments = $this->backend_model->getComments()->result();
						
			$comment_holder = array();
			foreach($comments as $comment)
			{
				$username = "";
				$comment_item = array('id' => $comment->id,
								'username' => $username,
								'comment' => $comment->comment,
								'visible' => $comment->visible,
								'cdate' => $comment->created_date);
				$comment_holder[] = $comment_item;
				
				
			}
			
			
			$data['comments'] = $comment_holder;
			$this->load->view('backend/comment_overwatch.php',$data);
		}
		else $this->js_logout();
	}
	
	
	public function place_type()
	{
		if($this->ion_auth->logged_in())
		{
			$crud = new grocery_CRUD();
			$crud->set_table('misc_place_type');
			$crud->set_subject('Type');
			$crud->columns('name');
			$crud->display_as('name','Name');
            $crud->required_fields('name');       
            $output = $crud->render();
			 $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
		}
		else $this->js_logout();
	}
	
	public function pois()
	{
		if($this->ion_auth->logged_in())
		{
			$crud = new grocery_CRUD();
			$crud->set_table('poi');
			$crud->set_subject('POI');
			//$crud->display_as('category_id','Category');
			
			$crud->set_relation_n_n('categories', 'poi_category_connection', 'poi_categories', 'poi_id', 'category_id', 'name');
			$crud->columns('name', 'categories', 'address','description', 'geo_location', 'like', 'accepted');
			$crud->field_type('accepted','dropdown', array('0' => 'Declined', '1' => 'Accepted'));
			
			
			$crud->unset_columns('category_id');
			//$crud->set_relation('category_id','poi_categories','name');
            $output = $crud->render();
			 $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
		}
		else $this->js_logout();
	}
	
	public function delete_request()
	{
		if($this->ion_auth->logged_in())
		{
			$crud = new grocery_CRUD();
			$crud->set_table('delete_requests');
			$crud->set_subject('Requests');
			$crud->columns('user_id', 'request_date');
			$crud->display_as('user_id','User');
			$crud->display_as('request_date','Request Date');
			$crud->set_relation('user_id','users','username');
			$crud->unset_add();
            $crud->unset_edit();
            $crud->unset_delete();
            $crud->add_action('Delete Profile', site_url('items/backend/img/gallery.png'), 'backend/delete_user_profile');
            $output = $crud->render();
			 $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
		}
		else $this->js_logout();
	}

	public function delete_user_profile()
	{
		$request_id = $this->uri->segment(3);
		$user_id = $this->auth_model->getRequestData($request_id)->row()->user_id;
		$this->load->model('dog_model');
		$my_dogs = $this->dog_model->getDogs($user_id)->result();
		
		foreach($my_dogs as $dog)
		{
			$this->dog_model->deleteDogProfile($dog->id);
		}
		$this->auth_model->deleteUserProfile($user_id);
		$this->auth_model->deleteRequest($user_id);
		
		redirect('backend/delete_request');
	}
	
	
	public function partner()
	{
		if($this->ion_auth->logged_in())
		{
			$crud = new grocery_CRUD();
			$crud->set_table('partners');
			$crud->set_subject('Partner');
			$crud->columns('name', 'email', 'partner_promo_id', 'phone', 'address','geo_location','impressions_left');
			$crud->display_as('name','Name');
			$crud->display_as('partner_promo_id','Partner Promo ID');
			$crud->display_as('email','Email');			
			$crud->display_as('phone','Phone');
			$crud->display_as('address','Address');
			$crud->display_as('geo_location','Geo Location');
			$crud->display_as('impressions_left','Impressions Left');
			$crud->display_as('image_fname','Image');
			$crud->display_as('link','Link');			
			$crud->unset_fields('image_fname','link', 'password');
			$crud->callback_after_insert(array($this, 'add_password'));
            $output = $crud->render();
			 $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
		}
		else $this->js_logout();
	}
	
	public function partner_ads()
	{
		if($this->ion_auth->logged_in())
		{
			$crud = new grocery_CRUD();
			$crud->set_table('partner_advertisements');
			$crud->set_subject('Partner AD');
			$crud->columns('partner_id', 'impressions', 'link', 'country');
			
			$crud->unset_add();
			
            $output = $crud->render();
			 $this->load->view('backend/header.php');
			$this->load->view('backend/default.php',$output);
			$this->load->view('backend/footer.php');
		}
		else $this->js_logout();
	}
	
	
	function add_password($post_array,$primary_key)
{
	$rnd = $this->rand_string(12);
	
	//Insert pw sending email HERE
	
	$password = $this->my_hash($rnd);
    $user_insert = array(    
        "password" => $password,      
    );
	
	$this->db->where('id', $primary_key);
    $this->db->update('partners',$user_insert);
	
	$data = array(
		'partner_id' => $primary_key,
		'external' => $post_array['external'],
		'vet' => $post_array['vet'],
		'place' => $post_array['place'],
		'internal_id' => $post_array['internal_id'],
		'country' => $post_array['country'],
		'state' => $post_array['state']);

	$this->db->insert('partner_advertisements', $data); 
	
	$message = $this->send_mail("partner_registration", $post_array['email'], $post_array['name'], $rnd, $post_array['partner_promo_id']);
	
    return true;
    
    
}
	public function overwatchDeleteImage()
	{
		$image_id = $_POST['pid'];
		
		$type = $_POST['type'];
		
		
		switch($type)
		{
			case 'user':{
							$this->backend_model->deleteUserProfilePicture($image_id);
						};break;
			case 'image':{
							
							$event_id = $this->backend_model->getEventId(8, $image_id)->row()->id;

							if($event_id !== NULL)
							{
								$this->backend_model->deleteEventImages($image_id, $event_id);
							}
							else
							{
								$this->backend_model->deleteSimpleImages($image_id);
							}
							
						};break;
		  case 'profile':{
			  				$event_id = $this->backend_model->getEventIdAdditional(6, $image_id)->row()->id;
			  				$vehicle_id = $this->backend_model->getEventIdAdditional(6, $image_id)->row()->entity_id;
							$this->backend_model->deleteVehicleProfilePicture($vehicle_id, $event_id, $image_id);
							
			  				
		 				 };break;
		  case 'cover':{
			  				
			  				$event_id = $this->backend_model->getEventIdAdditional(7, $image_id)->row()->id;
			  				$vehicle_id = $this->backend_model->getEventIdAdditional(7, $image_id)->row()->entity_id;
							$this->backend_model->deleteVehicleCoverPicture($vehicle_id, $event_id, $image_id);
														
		 				 };break;
		}
		
		
		
	}
	
	public function deleteComment()
	{
		$comment_id = $_POST['cid'];
		if($this->session->userdata('user_id'))
		{
			$loggedin_user = $this->session->userdata('user_id');
		//	$saved_user_id = $this->backend_model->getComment($comment_id)->row()->user_id;
		//	if($loggedin_user == $saved_user_id)
		//	{
				$this->backend_model->deleteComment($comment_id);
		//	}
		}
		
		
	}
	
	
	
	public function getImageWhereClause($resultset)
	{
		$where = "";
		foreach($resultset->result() as $result)
		{
			if($where != "")
				$where .= ',';
			$where .= $result->image_id;
		}
		
		if($where == "")
			$where = "NULL";
		
		return "id IN(" . $where . ")";
	}
	
	function js_logout()
	{

		echo "<script>";
		echo "parent.location.href='".site_url('auth/login')."'";
		echo "</script>";	
	}
	
	public static function removeEmoji($text) {
      
          return preg_replace('/([0-9|#][\x{20E3}])|[\x{00ae}|\x{00a9}|\x{203C}|\x{2047}|\x{2048}|\x{2049}|\x{3030}|\x{303D}|\x{2139}|\x{2122}|\x{3297}|\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F6FF}][\x{FE00}-\x{FEFF}]?/u', '', $text);
       } 
	
	public function saveHashtagsToDB(){
	
		
		$after = $this->backend_model->getLastImage()->row()->upload_date;
		
		$tags = array("snoopstr");
		$since = strtotime($after);
		
		$instagram_client_id = "e073a87edb2c4491ab3d4fb745f28bcb";
			
		 $settings = array(
		    'oauth_access_token' => "2524565712-UZn7Gi56cAhOrz4Z8TcEsc7GmoEngm8tPInwV9I",
		    'oauth_access_token_secret' => "uOZLvMgwolCMOW1iypt9stim3lglWVPwlbOVtqsmTJVA0",
		    'consumer_key' => "NrzOE3JqIXg6wmGlbbuxhjIUm",
		    'consumer_secret' => "6CKWWnm47D6wa6evgp2xElQdTclo7Bhw4ZplMwHUnOLgU0fn4O"
		);
		$fb_app_id = "289753514536531";
		$fb_app_secret = "4413dea0489c33fcda1fa2b70575a350";
		
	 /*   $this->load->library('TwitterAPIExchange',$settings);
	    $twitter = new TwitterAPIExchange($settings);
	    $twitter_url = 'https://api.twitter.com/1.1/search/tweets.json'; */
		
		foreach($tags as $tag){
			//INSTAGRAM
			$url = 'https://api.instagram.com/v1/tags/'.$tag.'/media/recent?since='.$since.'&client_id='.$instagram_client_id;
			
		    $jsonData = json_decode((file_get_contents($url)));
			 
		    foreach ($jsonData->data as $key=>$value) {	  
		     	
		    	$saved_tags = $this->removeEmoji($value->caption->text);
		    	$user_name = $value->caption->from->username;
		    	$created_time = $value->caption->created_time;
		    	$created_time = date("Y-m-d H:i:s", $created_time);
		    	//date_default_timezone_set('Europe/Vienna');
				$t=$value->created_time;
				$date =  date("Y-m-d H:i:s", $t);
						
				if($t > $since){
				
					echo "New Image added from: ".$date."<br/>";
					$this->backend_model->saveHashtagImage($value->images->standard_resolution->url, $value->link, $date, $saved_tags, $user_name, $created_time);		    	
		    	}
		    }
		   echo "Import finished! <br/>";
		    		   
		    //TWITTER
		   /* $getfield = '?q=%23'.$tag.'&result_type=mixed&include_entities=1';
			$requestMethod = 'GET';
			$twitterData = $twitter->setGetfield($getfield)
			            		   ->buildOauth($twitter_url, $requestMethod)
								   ->performRequest();
		    
		    $jsonData = json_decode($twitterData);
		    
		    foreach($jsonData as $key=>$value){
		    
		    	if($key == "statuses"){		    		
			    	foreach($value as $i=>$val){
			    		
					    	foreach($val as $k=>$meta){
						    	if($k == "created_at"){
						    		$date = date("Y-m-d H:i:s", strtotime($meta));
						    		
						    	}
						    	
						    	if(strtotime($date) > strtotime($after)){			    		
							    	if($k == "entities"){
							    		if(!empty($meta->media)){
							    			echo $date."<br/>";
							    			$this->mainmodel->saveHashtagImage($meta->media[0]->media_url, $meta->media[0]->expanded_url, $date);	
								    	
								    		
							    		}
								    	
							    	}
						    	}
					    	}
				    	
			    	}
		    	}
			   
		    } */
		    
		}
		
		
	}
	
	
	public function createThumbs()
	{
		
		$images = $this->backend_model->getImages()->result();
		$this->load->library('image_moo');
		foreach($images as $image)
		{
			if(!file_exists('http://fileserver.istvanszilagyi.com/snoopstr/items/uploads/images/thumb__'.$image->fname))
			{
				
					$fname = 'items/uploads/images/'.$image->fname;	
				
					$fname_thumb = 'items/uploads/images/thumb__'.$image->fname;
					$this->image_moo->load($fname)->resize(380,440)->save($fname_thumb,true);
					
					if ($this->image_moo->error) echo $this->image_moo->display_errors();
					
					echo $image->fname."<br/>";
			}
		}
	}
	
	
	
	public function managed_groups()
	{
		$data = null;
		
		$requests = array();
		
		$new_groups = $this->statics_model->getNewGroups();
		
		foreach($new_groups as $group)
		{
			$user = $this->statics_model->getUserData($group['created_by'])->row();
			$vehicle = $this->car_model->getVehicle($group['vehicle_id'])->row();
			
			$item = array('id' => $group['id'],
						'name' => $group['name'],
						'description' => $group['description'],
						'user' => $user->username,
						'user_id' => $group['created_by'],
						'vehicle' => $vehicle->nickname,
						'vehicle_id' => $group['vehicle_id']);
			$requests[] = $item;
		}
		
		$data['groups'] = $requests;
		
		$this->load->view('backend/group_requests.php',$data);
		
		
	}
	
}

/* End of file backend.php */
/* Location: ./application/controllers/backend.php */

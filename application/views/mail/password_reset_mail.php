<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<style>
	   	@import url(http://fonts.googleapis.com/css?family=Open+Sans:700,400italic&subset=latin,latin-ext);
	   	@import url(https://fonts.googleapis.com/css?family=Dosis:400,300,600,700);
	   /* All your usual CSS here */
	  
	  tr, td, table
	  {
	  	margin:0px;
	  	padding:0px;
	  	border-spacing:0;
	  	display:flex;
	  }
	  img
	  {
	  	border:0;
	  }
	</style>
	
	
</head>
<body style="background:#ffffff;">
	<table style="margin:0px;padding:0px;border-spacing:0;display:flex;width:400px; margin:0px auto;border-spacing:0;font-family: 'Dosis', sans-serif;background:#ffffff;">
		<tr>
			<td style="margin-bottom:-4px;">
				 <div style="position:relative;width:400px;">
					 <img style="border:0px" src="<?= site_url('items/frontend/img/mail/mail_header.jpg')?>" />
				 </div>
				 
			</td>
		</tr>
		<tr>
			<td>
				<img style="width:100%;height:4px;border:0px;" src="<?= site_url('items/frontend/img/mail_gold_line.png')?>" />
			</td>
		</tr>
		<tr>
			<td style="position:relative;">				 
				<div style="width:400px;height:60px;background:#000000;color:#ffffff;line-height:60px;font-size:16px;font-style:bold;text-align:center;">DEAR <?= $recipient?></div>
			</td>
		</tr>
		
		<tr style="background:#e7e7e7;">
			<td>
				<div style="width:400px;height:120px;padding: 20px 0px;color:#4b4b4b;line-height:20px;font-size:14px;font-style:italic;text-align:center;">You have requested a password reset on Performance Nation.  
				<br/> 
				Click the link below to reset your password:
				<br/>
				<br/>		
				<a target="_blank" href="<?= site_url('reset_password/'.$hash);?>">LINK</a>
				<br/>
				If you did not ask for a reset, please disregard this email!
				
				
				</div>
			</td>
		</tr>
		<tr style="background:#ffffff;">
			<td>
				
				<div style="width:400px;height:30px;color:#000000;line-height:30px;font-size:17px;font-weight:700;text-align:center;">FOLLOW US</div>
			</td>
		</tr>
		<tr style="background:#ffffff;">
			<td>
				<div style="width:400px;height:20px;color:#000000;line-height:20px;font-size:14px;text-align:center;">on Twitter, Facebook or Instagram</div>
			</td>
		</tr>
		
		
		<tr>
			<td>
				<div style="width:400px;height:50px;text-align:center;padding-bottom:15px;background:#ffffff;">
					<a target="_blank" href=""><img style="margin:5px;border:0px;" src="<?= site_url('items/frontend/img/mail/mail_tw.png')?>" /></a>
					<a target="_blank" href="https://www.facebook.com/Performancenationllc2016/"><img style="margin:5px;border:0px;" src="<?= site_url('items/frontend/img/mail/mail_fb.png')?>" /></a>
					<a target="_blank" href="www.instagram.com/performance_nation_worldwide/"><img style="margin:5px;border:0px;" src="<?= site_url('items/frontend/img/mail/mail_insta.png')?>" /></a>
					
				</div>
				
			</td>
		</tr>
		<tr>
			<td>
				<img src="<?= site_url('items/frontend/img/mail/mail_footer.jpg')?>" />
			</td>
		</tr>
	</table>
</body>






</html>



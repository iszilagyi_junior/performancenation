<script>
	$('#sidemenu').css({top:35});
</script>
			<div id="content" style="position:relative;top:5px;overflow:hidden;">
				
				
				<div id="vets_alphabet" class="statics_alphabet">
					<?= $this->load->view('statics/abc', array('target' => 'breeds')); ?>
				</div>
				<div id="vets_currentletter" class="statics_currentletter">
					<?= $current_letter ?>
				</div>
				<div id="breeds_list">
					<?php $x = 1; foreach ($breeds as $breed): ?>
						<!-- <a href="<?= site_url('breeds/' . $breed->id)?>"> -->
							<div class="breeds_list_item" <? if($x%3 == 0){ echo "style='margin-right:0px'";}?> >
								<? if($breed->teaser_picture != "" && $breed->teaser_picture != 'null'){?>
									<img class="vets_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $breed->teaser_picture)?>" border="0">
									<div class="div_white_over"></div>
								<? }?>
								
								<div class="zones_list_item_desc" style="top:90px;">
									<?= $breed->name;?>
								</div>
							</div>
					<!--	</a> -->
						
					<?php $x++; endforeach; ?>

				</div>
			</div>
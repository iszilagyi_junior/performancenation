<script>
	$(document).ready(function()
	{	
		$("#welcomeImgHolder, #welcomeImg").show();
		$("#downloadBG, #socialBG").hide();
	});
</script>
<div id="content">
<div id="WelcomeHolder">
			
			<div id="BkgEr" style="display:none;"></div>
			<div id="welcomeText">Über Snoopstr</div>
		</div>
		<div class="home_italic">Snoopstr ist das erste soziale Netzwerk für Hunde.</div>
		<hr align="middle" class="shortLine">
		<div class="home_italic">
		Registriere dich jetzt um die Community von Hundebesitzern auf der ganzen Welt zu entdecken, aber speziell in deiner Nachbarschaft!<br/>
		<br/>
		<span class="sansregular">Snoopstr ermöglicht:<br/>
		<br/>
		ein Profil für Hunde anzulegen<br/>
		Fotos hochzuladen und zu teilen<br/>
		neue Hundefreunde zu finden<br/>
		Aktivitäten zusammen zu planen<br/>
		Suche nach Tierärzten, Hundesittern, Hundezonen..<br/>
		<br/>
		Du kannst sogar deine Instagram Fotos hinzufügen!<br/></span>
		<br/>
		<br/>
		<a style="text-decoration:none;" href="<?= site_url('register')?>">
			<div id="sidemenu_register" action="register" style="position: relative;left: 235px;top: 0px;">Jetzt registrieren</div>
		</a>
		<br/>
		Interessiert an Werbeschaltungen?<br/>
		<br/>
		<span class="sansregular">Wir arbeiten ständig daran unsere Community zu vergrößern - wenn du dich für unsere Werbepakete interessierst schicke uns eine Anfrage an <a href="mailto:ads@snoopstr.com">ads@snoopstr.com</a></span><br/>
		<br/>
		Du kannst lokal oder global werben - wir freuen uns dich beraten zu dürfen!</div>
		
</div>

	
</div>
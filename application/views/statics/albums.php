<script>
	$('#sidemenu').css({top:35});
</script>
<div id="content">
	<div style="position:relative;top:35px;margin-bottom: 35px;">
		<div class="statics_subheader"><?= $this->lang->line('albums')?></div>
		
		
		
	</div>
	
	<div id="notIE9">
		<div id="add_photo_btn_albums" >
			<div id="add_photo_btn_text"><?= $this->lang->line('albums_add_photo')?></div>		
		</div>
		<input type="file" preview="add_photo_btn_newsfeed" accept="image/*" id="add_photo_upload_btn" name="add_photo_upload_btn" style="display:none;">
	</div>
	<div id="IE9" style="display:none;margin-top:55px;">
		<div id="add_photo_btn_text" style="margin-right:20px;"><?= $this->lang->line('albums_add_photo')?></div>
		<?php 
		$data = array('id' => 'add_photo_ie');
		echo form_open_multipart("statics/add_photo_albums_ie", $data);?>
			<input type="file" preview="add_photo_btn_newsfeed" accept="image/*" id="add_photo_upload_btn_ie" name="myfile" style="">
		<? echo form_close();?>
	</div>
	
	
	
	<div class="button_skin" id="add_new_album" style="margin:45px 0px 0px 205px;width:190px;"><?= $this->lang->line('add_new_album')?></div>	
	
	<?php 
		$data = array('id' => 'add_album_form');
		echo form_open("statics/add_album", $data);?>
	<div id="album_hl"><?= $this->lang->line('new_album')?></div><br/>
	<input type="text" id="album_title" name="album_title" placeholder="<?= $this->lang->line('album_new_title')?>" class="edit_input" style="margin-top:0px;margin-right:15px;width:185px;">
	<textarea type="text" id="album_about" name="album_about" placeholder="<?= $this->lang->line('album_about')?>" class="edit_input" style="height:85px;resize:none;margin-top:0px;width:185px;"></textarea>
	<div id="add_album_btn" style="top:78px;">
		<div id="add_album" style="top:0px;width:190px;" class="album_arrow_text"><?= $this->lang->line('save_new_album')?></div>
	</div> 
	<?php echo form_close();?>
	<div id="edit_error_message" style="position: relative;top: 112px;">
		<?php if(isset($message)) echo $message;?>
	</div>
	
	<div id="album_holder">
		<?if(isset($albums)){
			$x = 1; foreach($albums as $album){?>
				<a href="<?= site_url('edit_album/'.$album['id']);?>">
					<div class="album_item_outer" <? if($x%3 == 0) echo 'style="margin-right:0px;"'?>>	
						<div class="album_item" >
							<?if($album['pictures']){ 				
								foreach($album['pictures'] as $i=>$pic){
									if($i==0){
										if($pic['fname']!=""){
							?>
											<div class="album_item_image_holder">
												<img class="album_item_image" src="<?= site_url('items/uploads/'.$pic['folder'].'/'.$pic['fname']);?>"/>
											</div>
							<? 			
										}
									}
								}
							}else{?>
								<div class="album_item_image_holder">
									<img class="album_item_image" src="<?= site_url('items/uploads/profilepictures/default.png')?>"/>
								</div>
							<? }?>
							
							<div class="album_item_title"><?= $album['title'];?></div>
							<div class="album_item_about"><?= $album['about'];?></div>
						</div>
					</div>
				</a>
		<? 
		$x++; }
			}?>
	</div>	
	<br>
	
</div>
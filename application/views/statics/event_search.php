<script>

	var all_events = <?= $all_events;?>;

	$(document).ready(function()
	{	
		$('#start_search').datetimepicker();
		$('#end_search').datetimepicker();
		initAutocomplete(false, 'location_search');
		initMapAllEvents(all_events, '48.2165319,16.3608047');
	});
	
</script>

<div id="content" style="width:800px;margin-bottom:100px;">
	<img class="" style="position:absolute;left:0px;top:0px;" src="<?= site_url('items/frontend/img/big_chrome.png')?>" border="0">
	<div class="playbold gold page_title" style="text-align:center;font-size:40px;letter-spacing:1px;z-index:10;position:relative;top:10px;margin-bottom:30px;">EVENT SEARCH</div>
	
	<div style="height:170px;text-align: left;margin:0px auto;width:511px;">
		<input type="text" id="profile_search" name="profile_search" placeholder="Title" defaultText="Title" class="edit_input" style="margin-bottom:5px;width:160px;">
		<input type="text" id="name_search" name="name_search" placeholder="Club or vehicle name" defaultText="Title" class="edit_input" style="margin-bottom:5px;width:160px;">
		<input type="text" id="location_search" name="location_search" placeholder="Location" defaultText="Title" class="edit_input" style="margin-bottom:5px;width:160px;">
		
		<input type="text" id="start_search" name="start_search" placeholder="Start Time" defaultText="Title" class="edit_input" style="margin-bottom:5px;width:160px;">
		<input type="text" id="end_search" name="end_search" placeholder="End Time" defaultText="Title" class="edit_input" style="margin-bottom:5px;width:160px;">
		<input type="text" id="distance_search" name="distance_search" placeholder="Distance in miles" defaultText="Title" class="edit_input" style="margin-bottom:5px;width:160px;">
		<div style="width:140px;margin:0px auto;"><label for="show_past_events" style="color:#ffffff;">Show past events: </label><input type="checkbox" id="show_past_events" name="show_past_events" value="1"/></div>
		
		<div id="event_search_submit" class="button" style="margin:10px auto;">SEARCH</div>
		<div class="button" id="event_locate_me">CENTER MAP ON ME</div>
	</div>
	<div class="error_msg"></div>
	<div id="event_search_list" class="button" style="position:absolute;right:10px;top:185px;width:90px;">LIST VIEW</div>
	<div id="event_search_map" class="button" style="position:absolute;right:10px;top:185px;display:none;width:90px;">MAP VIEW</div>
	
	<div id="event_map_holder"></div>
	<div id="resultHolder" style="display:none;">
		<div class="titit" style="text-align:center;margin:10px 0px;color:#ffffff;font-size:18px;margin-top:60px;">Results</div>
		<div id="result_list">
		<? $x = 1; foreach($events as $event):?>
		<a  style="text-decoration:none;" href="<?= site_url($event['type'].'_event/' . $event['id'])?>">	
			<div class="result_list_item" eid="<?= $event['id'];?>" date="<?= $event['date'];?>" style="<? if($x%2 == 0){ echo "margin-right:0px";}?>">	
					<img class="result_item_image" src="<?= site_url('items/uploads/'.$event['logo'])?>" style="">
					<div class="div_white_over"></div>
					<div class="result_item_text" style=""><?= $event['title'];?><br/><?= $event['address'];?><br/><?= $event['start'];?><br/><br/>Members: <?= $event['participants']?></div>				
				
			</div>
		</a>
		<? $x++; endforeach;?>
		
		</div>		
	</div>
</div>
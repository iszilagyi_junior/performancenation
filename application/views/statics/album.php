<script>	
	
	
	$(document).ready(function()
	{	
		<? if($can_edit):?>
			album_file_upload(<?= $album->id;?>);
		<? endif;?>
	});
	
	
</script>



<!--
<div id="comment_holder" pic="">	
	<div class="tag_overlay_close">x</div>
	<div class="tag_overlay_head" style="">Comments</div>
	<div id="add_comment_holder">
		<div class="regular" style="color:#ffffff;margin:10px 0px;font-size:14px;">Add comment</div>
		<textarea id="comment_photo_input" name="comment_photo_input" ></textarea>
		<div id="add_comment_btn" class="button" style="float:none;top:5px;width:100%;">ADD</div>
	</div>
	<hr style="border:0px;height:1px;background:#fff;width:50%;margin:15px 25% 5px 25%;"/>	
	<div id="user_comments">
					
	</div>
</div>

-->
<div id="content" style="width:800px;margin-bottom:100px;">
	
		<div class="statics_subheader" style="font-size:20px;top:0px;">
			<?= $album->title;?>
		</div>	
		<a href="javascript:history.back()">	
			<div  style="width:90px;top:5px;position:absolute;left:17px;"  class="button">BACK</div>			
		</a>
		<? if($can_edit):?>	
			<div class="button" id="add_photo_album_btn">UPLOAD A PICTURE</div>
			<input type="file" preview="add_photo_btn_newsfeed" accept="image/*" id="add_photo_upload_btn" name="add_photo_upload_btn" style="display:none;">
			
			<div class="button" id="delete_album_btn" style="width:120px;position:absolute;top:0px;right:30px;">DELETE ALBUM</div>
			<div id="delete_album_holder" style="overflow:visible;width:180px;position:absolute;right:0px;top:0px;">
				<div class="regular" style="color:#ffffff;">Are you sure you want to delete this album?</div>
				<div class="button" id="delete_album_yes" style="width:80px;float:left;" aid="<?= $album->id;?>" pretty="<?= $pretty;?>">YES</div>
				<div class="button" id="delete_album_no" style="width:80px;float:right;">NO</div>
			</div>
		<? endif;?>
		<br style="clear:both;">
		<div id="album_pictures_holder">
			<? $x = 1; if(isset($pictures)){
				foreach($pictures as $pic){
					
					
					if(strlen($pic['title'])>40)
					{
						$pos=strpos($pic['title'], ' ', 40);
						$title = substr($pic['title'],0,$pos );
						$title .= "...";
					}
					else
					{
						$title = $pic->title;
					}
				?>
					<div class="album_item_outer" <?if($x%4==0){ echo 'style="margin-right:0px;"';}?>>
						<div class="album_item" >
							<? if($can_edit):?>
								<div class="remove_album_image">X</div>
							<? endif;?>
							<div class="remove_album_image_holder">
								<div class="regular" style="color:#ffffff;">Are you sure you want to delete this image?</div>
								<div class="button delete_image_yes"  style="width:80px;float:left;" pid="<?= $pic['id'];?>">YES</div>
								<div class="button delete_image_no"  style="width:80px;float:right;">NO</div>
							</div>
							
							<a href="<?= site_url('items/uploads/'.$pic['folder'].'/'.$pic['fname']);?>" pic_id="<?= $pic['id']; ?>" data-lightbox="pictures" title="<?= $pic['title']; ?>">
								<div class="album_item_image_holder" style="background-image: url('<?= site_url('items/uploads/'.$pic['folder'].'/'.$pic['fname']);?>');"></div>
							</a>	
																
							
							<div class="album_image_text_holder">
								<div class="image_text"><?= $pic['title']; ?></div>
								<div class="image_share_holder_top">
									<div style="float:left;width:45px;margin-rigth:10px;">
										<img class="image_social_icon image_social_comment" eid="null" pid="<?= $pic['id']; ?>" src="<?= site_url('items/frontend/img/photo_icons_comment_white-01.png');?>"/>
										<div class="the_likes">0</div>
									</div>
									<div style="float:left;width:45px;margin-rigth:10px;">
										<img class="image_social_icon image_social_like" eid="null" pid="<?= $pic['id']; ?>" src="<?= site_url('items/frontend/img/photo_icons_love_white-01.png');?>"/>
										<div class="the_likes"><?= $pic['likes']; ?></div>
									</div>		
								</div>
								
								
								
								
								
								
							<!--	<div class="image_share_holder_top">
									<img class="image_social_icon image_social_comment" pid="<?= $pic['id']; ?>" src="<?= site_url('items/frontend/img/photo_icons_comment_white-01.png');?>"/>
									<? if($pic['already'] == 0){?>
										<img class="image_social_icon image_social_like" pid="<?= $pic['id']; ?>" src="<?= site_url('items/frontend/img/photo_icons_love_white-01.png');?>"/>
									<? }?>
								</div>
								<div class="image_social_like_count">Likes: <span class="the_likes"><?= $pic['likes']; ?></span></div> -->
								<div class="image_share_holder" style="width:35px;left:0px;">
									<img class="image_share_icon share_image_tw" pid="<?= $pic['id']; ?>" src="<?= site_url('items/frontend/img/white_tw.png');?>"/>
									<img class="image_share_icon share_image_fb" pid="<?= $pic['id']; ?>"  fname="<?= $pic['fname']; ?>" title="<?= $pic['title']; ?>" src="<?= site_url('items/frontend/img/white_fb.png');?>"/>							
								</div>
							</div>					
						</div>
					</div>
			<? $x++;
			}
				}?>
		</div>
		
	
</div>
	
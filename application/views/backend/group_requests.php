<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>PN New Group requests</title>
    <link href="<?=site_url("items/backend/css/overwatch.css") ?>" rel="stylesheet" type="text/css" media="screen" />	
	<script type="text/javascript" src="<?=site_url("items/backend/js/jquery.js");?>"></script> 
	<script type="text/javascript" src="<?=site_url("items/backend/js/overwatch.js");?>"></script> 
	<script>
		var rootUrl='<?=site_url();?>';
	</script>
	
</head>
<script>
		
</script>

<body>
	<a style="text-decoration:none;" href="<?= site_url('backend/menu');?>"><div id="menuButton">Menu</div></a>

	<div id="delete_editor" >	
		<div>Are you sure you wish to delete this picture?</div>
		<div style="overflow:hidden">
			<div class="photo_delete_yes button_skin" pid="" typ="" style="width: 80px;margin-top: 3px;float:left;">YES</div>
			<div class="photo_delete_no button_skin" style="width: 80px;margin-top: 3px;float:right;">NO</div>
		</div>					
		
	</div>
	<? foreach($groups as $group):?>
		<div class="group_request_item">
			<div class="group_name"><?= $group['name'];?></div>
			<br/>
			<div class="group_description"><?= $group['description'];?></div>
			<br/>
			<div class="group_user"><?= $group['user']." - ".$group['vehicle'];?></div>
			<div style="margin-top:20px;">
				<div class="accept_grp_request button" uid="<?= $group['vehicle_id']?>" rid="<?= $group['id']?>">Accept</div>
				<div class="decline_grp_request button" uid="<?= $group['vehicle_id']?>" rid="<?= $group['id']?>" style="margin-right:0px;">Decline</div>
			</div>
		</div>
	<? endforeach;?>
</body>
</html>
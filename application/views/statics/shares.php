<script>
	$('#sidemenu').css({top:35});
</script>
<div id="content">
	<div style="position:relative;top:35px;">
		<div class="statics_subheader"><?= $this->lang->line('invite_spread');?></div>
		<div class="sansitalic" style="text-align:center;margin:20px 0px;"><?= $this->lang->line('invite_page_miss');?></div>
		<hr class="hline">
		<div style="height:40px;margin-bottom:35px;width: 355px;margin: 0px auto 30px;">
			<input type="text" id="invite_send_mail" name="invite_send_mail" placeholder="<?= $this->lang->line('register_email')?>"  style="">
			<div id="invite_send_mail_button"><?= $this->lang->line('invite_send_mail_button');?></div>
			
		</div>
		<div id="invite_mail_error" style="text-align: center;"></div>
		<div class="sansitalic" style="text-align:center;margin:20px 0px;"><?= $this->lang->line('invite_page_email_enter');?></div>
		<div style="height: 200px;margin-left:125px;">
			<div class="share_item" id="invite_fb">
				<img class="share_item_img" src="<?= site_url('items/frontend/img/invite_fb.png')?>">
				<div class="share_item_text">Share on<br/> facebook</div>
			</div>
			<div class="share_item" id="invite_tw">
				<img class="share_item_img" src="<?= site_url('items/frontend/img/invite_twitter.png')?>">
				<div class="share_item_text" style="margin-top:29px;">Share on<br/> twitter</div>
			</div>
		</div>		
		

	</div>
</div>
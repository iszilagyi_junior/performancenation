<div id="content" style="width:800px;">
	<img class="" style="position:absolute;left:0px;top:0px;" src="<?= site_url('items/frontend/img/big_chrome.png')?>" border="0">
	<div class="playbold gold page_title" style="text-align:center;font-size:40px;letter-spacing:1px;z-index:10;position:relative;top:10px;">MY CREW</div>

	<div id="resultHolder" style="margin-top:30px;">
		<div class="dosissemi">CARS I FOLLOW</div><br/>
		<div id="result_list" style="overflow:hidden;">
		<? $x = 1; foreach($follows as $follower):?>
		<a  style="text-decoration:none;" href="<?= site_url('vehicle_profile/' . $follower['pretty'])?>">	
			<div class="result_list_item" style="<? if($x%2 == 0){ echo "margin-right:0px";}?>">	
					<img class="result_item_image" src="<?= site_url('items/uploads/profilepictures/'.$follower['logo'])?>" style="">
					<div class="div_white_over"></div>
					<div class="result_item_text" style="">
							<?= $follower['name'];?>
							</div>				
				
			</div>
		</a>
		<? $x++; endforeach;?>
		<? if($x == 1) echo "None yet!";?>
		</div>		
		<br/>
		<div class="dosissemi">MY FOLLOWERS</div><br/>
		<div id="result_list"  style="overflow:hidden;">
		<? $x = 1; foreach($followers as $group):?>
		<a  style="text-decoration:none;" href="<?= site_url('vehicle_profile/' . $group['pretty'])?>">	
			<div class="result_list_item" style="<? if($x%2 == 0){ echo "margin-right:0px";}?>">	
					<img class="result_item_image" src="<?= site_url('items/uploads/profilepictures/'.$group['logo'])?>" style="">
					<div class="div_white_over"></div>
					<div class="result_item_text" style="">
							<?= $group['name'];?>
							</div>				
				
			</div>
		</a>
		<? $x++; endforeach;?>
		<? if($x == 1) echo "None yet!";?>
		</div>
	</div>
	
	
	
	<div class="titit" style="text-align:center;margin:10px 0px;color:#ffffff;font-size:18px;margin-top:50px;">Suggestions</div>
	<div id="vehicle_map_holder"></div>
	<div id="resultHolder" style="margin-top:15px;">
		<div id="result_list">
		<? $x = 1; foreach($vehicles as $vehicle):?>
		<a  style="text-decoration:none;" href="<?= site_url('vehicle_profile/' . $vehicle['pretty_url'])?>">	
			<div class="result_list_item" style="<? if($x%2 == 0){ echo "margin-right:0px";}?>">	
					<img class="result_item_image" src="<?= site_url('items/uploads/profilepictures/'.$vehicle['fname'])?>" style="">
					<img class="follow_circle" refresh="yes" title="Follow" vid="<?= $vehicle['id'];?>" src="<?= site_url('items/frontend/img/circle_plus.png')?>">
					<div class="div_white_over"></div>
					<div class="result_item_text" style=""><?= $vehicle['nickname'];?><br/>
						<br/>
						<?= $vehicle['model']->year;?> 
						<span style="text-transform: capitalize;"><?= $vehicle['model']->make;?></span><br/>
						<span style="text-transform: capitalize;"><?= $vehicle['model']->model;?></span><br/>
						0-60: <? if($vehicle['zerosixty'] === NULL || $vehicle['zerosixty'] == "" || $vehicle['zerosixty'] == "0"){echo "N/A";} else { echo $vehicle['zerosixty']." s";} ?><br/>
						Followers: <?= $vehicle['followers'];?>
					</div>				
				
			</div>
		</a>
		<? $x++; endforeach;?>
		
		</div>		
	</div>
	
	
</div>
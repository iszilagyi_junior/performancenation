<script>
	$(document).ready(function()
	{	
		$("#welcomeImg").show();
		$("#downloadBG, #socialBG").hide();
	});
</script>
<div id="content">
<div id="WelcomeHolder">
			
			<div id="BkgEr" style="display:none;"></div>
			<div id="welcomeText">About</div>
		</div>
		<div class="home_italic">Snoopstr is the first social network for dogs.</div>
		<hr align="middle" class="shortLine">
		<div class="home_italic">
		Register now to enjoy the community of other dog owners all around the world, but specially in your neighbourhood!<br/>
		<br/>
		<span class="sansregular">Snoopstr lets you:<br/>
		<br/>
		create profiles for your dogs<br/>
		upload and share images of your dogs<br/>
		make new dog pals around you<br/>
		plan activities with other dog friends<br/>
		search localised for vets, sitters, dog zones..<br/>
		<br/>
		you can integrate your instagram photos too!<br/></span>
		<br/>
		<br/>
		<a style="text-decoration:none;" href="<?= site_url('register')?>">
			<div id="sidemenu_register" action="register" style="position: relative;left: 235px;top: 0px;">register now</div>
		</a>
		<br/>
		Interested in Ads?<br/>
		<br/>
		<span class="sansregular">We are working hard on building our community - if you are interested in our ad packages, send us your inquiry to <a href="mailto:ads@snoopstr.com">ads@snoopstr.com</a></span><br/>
		<br/>
		You can have localized and global spaces, we advise you gladly!</div>
		
</div>

	
</div>
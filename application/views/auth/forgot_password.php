<script>
	    $('#sidemenu').css({top: -5});
	    
	    $(document).ready(function()
		{	
			var windowWidth = $(window).width();
			
			if(windowWidth <= 800)
			{
				$('#sidemenu').css({top: 150});
			}
			else
			{
				$('#sidemenu').css({top: -5});
			}
		});
</script>
<div id="content">
	<div id="register_header" class="content_header">
		<?= $this->lang->line('forgot_pw_header')?>
	</div>
	
	<p style="text-align:center;" class="sansitalic"><?php echo sprintf($this->lang->line('forgot_sub_header'), $identity_label);?></p>
	
	<div id="infoMessage" style="text-align:center;"><?php echo $message;?></div>
	
	<?php echo form_open("auth/forgot_password");?>
	
	      <div style="margin-left:80px;">
	      		<label for="email" class="sansitalic"><?php echo sprintf($this->lang->line('forgot_email'), $identity_label);?></label> 
	      		<input type="text" name="email" value="" id="email" class="login_login_input">	      
		  		<input type="submit" class="login_arrow_text pseudo_form_submit" action="login" source="login" value="<?= $this->lang->line('forgot_pw_submit')?>" style="border:0px;">
	     </div>
	
	<?php echo form_close();?>
</div>
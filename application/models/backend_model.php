<?php

class backend_model extends CI_Model  
{
	public function getVetPictures($vet_id)
	{
		$sql = "SELECT * FROM vets_images WHERE vet_id = ?";
		$resultset = $this->db->query($sql, array($vet_id));
		return $resultset;
	}
	
	public function insertVetsImages($vet_id, $image_id)
	{
		$sql = "INSERT INTO vets_images (vet_id, image_id, created_by, created_date) VALUES (?,?,?,?)";
		$this->db->query($sql, array($vet_id, $image_id, $this->session->userdata('user_id'), date('Y-m-d H:i:s')));
	}
	
	public function deleteVetsImages($vet_id, $image_id)
	{
		$sql = "DELETE FROM vets_images WHERE vet_id = ? and image_id = ?";
		$this->db->query($sql, array($vet_id, $image_id));
	}	


	public function getBreedPictures($breed_id)
	{
		$sql = "SELECT * FROM breeds_images WHERE breed_id = ?";
		$resultset = $this->db->query($sql, array($breed_id));
		return $resultset;
	}
	
	public function insertBreedsImages($breed_id, $image_id)
	{
		$sql = "INSERT INTO breeds_images (breed_id, image_id, created_by, created_date) VALUES (?,?,?,?)";
		$this->db->query($sql, array($breed_id, $image_id, $this->session->userdata('user_id'), date('Y-m-d H:i:s')));
	}
	
	public function deleteBreedsImages($breed_id, $image_id)
	{
		$sql = "DELETE FROM breeds_images WHERE breed_id = ? and image_id = ?";
		$this->db->query($sql, array($breed_id, $image_id));
	}	
	
	public function getDogzonePictures($zone_id)
	{
		$sql = "SELECT * FROM dogzone_images WHERE dogzone_id = ?";
		$resultset = $this->db->query($sql, array($zone_id));
		return $resultset;
	}
	
	public function insertDogzoneImages($zone_id, $image_id)
	{
		$sql = "INSERT INTO dogzone_images (dogzone_id, image_id, created_by, created_date) VALUES (?,?,?,?)";
		$this->db->query($sql, array($zone_id, $image_id, $this->session->userdata('user_id'), date('Y-m-d H:i:s')));
	}
	
	public function deleteDogzoneImages($zone_id, $image_id)
	{
		$sql = "DELETE FROM dogzone_images WHERE dogzone_id = ? and image_id = ?";
		$this->db->query($sql, array($zone_id, $image_id));
	}
	
	
	public function getImages()
	{
		return $this->db->get('images');
	}
	
	public function getUsers()
	{
		return $this->db->get('users');
	}
	
	public function getDogs()
	{
		return $this->db->get('dogs');
	}
	
	function getEventId($action, $image_id)
	{
		$sql = 'SELECT * FROM events WHERE action_id = ? AND entity_id = ? ';
		$resultset = $this->db->query($sql, array($action, $image_id));
		return $resultset;
	}
	
	function getEventIdAdditional($action, $image_id)
	{
		$sql = 'SELECT * FROM events WHERE action_id = ? AND additional_id = ? ';
		$resultset = $this->db->query($sql, array($action, $image_id));
		return $resultset;
	}
	
	function deleteEventImages($image_id, $event_id)
	{
		$sql = "DELETE FROM albums_images WHERE image_id = ?";
		$this->db->query($sql, array($image_id));
		
		$sql = "DELETE FROM images WHERE id = ?";
		$this->db->query($sql, array($image_id));
		
		$sql = "DELETE FROM events WHERE action_id = 8 AND entity_id = ?";
		$this->db->query($sql, array($image_id));
		
		$sql = "DELETE FROM event_vehicles WHERE event_id = ?";
		$this->db->query($sql, array($event_id));
	}
	
	function deleteSimpleImages($image_id)
	{
		$sql = "DELETE FROM albums_images WHERE image_id = ?";
		$this->db->query($sql, array($image_id));
		
		$sql = "DELETE FROM images WHERE id = ?";
		$this->db->query($sql, array($image_id));
	}
	
	function deleteUserProfilePicture($user_id)
	{
		$this->db->where('id', $user_id);
		$this->db->update('users', array('profile_picture' => 'default.png'));	
	}
	
	function deleteVehicleProfilePicture($vehicle_id, $event_id, $image_id)
	{
		$this->db->where('id', $vehicle_id);
		$this->db->update('vehicles', array('profile_image' => '1'));	
		
		$sql = "DELETE FROM events WHERE action_id = 6 AND id = ?";
		$this->db->query($sql, array($event_id));
		
		$sql = "DELETE FROM event_vehicles WHERE event_id = ?";
		$this->db->query($sql, array($event_id));
		
		$sql = "DELETE FROM images WHERE id = ?";
		$this->db->query($sql, array($image_id));
	}
	
	
	function deleteVehicleCoverPicture($vehicle_id, $event_id, $image_id)
	{
		$this->db->where('id', $vehicle_id);
		$this->db->update('dogs', array('cover_image' => 1));	
		
		$sql = "DELETE FROM events WHERE action_id = 7 AND id = ?";
		$this->db->query($sql, array($event_id));
		
		$sql = "DELETE FROM event_vehicles WHERE event_id = ?";
		$this->db->query($sql, array($event_id));
		
		$sql = "DELETE FROM images WHERE id = ?";
		$this->db->query($sql, array($image_id));
	}	
	
	function getLastImage(){
	    $sql = "SELECT *  FROM  `hashtags` ORDER BY `upload_date` DESC LIMIT 1";
	    $result = $this->db->query($sql);
	    
	    return $result;  
  	}
  	
  	function saveHashtagImage($fname, $url, $date, $tags, $username, $created_date) {
	    $sql = "INSERT INTO  `hashtags` ( `fname`, `target_url`, `upload_date`, `tags`, `username`, `created_date` ) VALUES (".$this->db->escape($fname).", ".$this->db->escape($url).", ".$this->db->escape($date).", ".$this->db->escape($tags).", ".$this->db->escape($username).", ".$this->db->escape($created_date).");";
	    $this->db->query($sql); 
	    return $this->db->insert_id();
    }
    
    public function getComments()
	{
		$this->db->order_by('created_date', 'DESC');
		return $this->db->get('image_comments');
	}
	
	
	function updateComment($comment_id, $comment)
	{
		$this->db->where('id', $comment_id);
		$this->db->update('dogs', array('comment' => $comment));	
		
		
	}
	
	function updateUsers($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('users',$data);	
		
		
	}
	
	
	function getComment($comment_id)
	{
		$this->db->where('id', $comment_id);
		return $this->db->get('image_comments');			
		
	}	
	
	function deleteComment($comment_id)
	{
		$this->db->where('id', $comment_id);
		$this->db->delete('image_comments');			
		
	}
}
?>
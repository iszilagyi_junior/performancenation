<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>PN Image Overwatch</title>
    <link href="<?=site_url("items/backend/css/overwatch.css") ?>" rel="stylesheet" type="text/css" media="screen" />	
	<script type="text/javascript" src="<?=site_url("items/backend/js/jquery.js");?>"></script> 
	<script type="text/javascript" src="<?=site_url("items/backend/js/overwatch.js");?>"></script> 
	<script>
		var rootUrl='<?=site_url();?>';
	</script>
	
</head>
<script>
	$(document).ready(function() {
	
		getNewImages();
		
	});
	
</script>

<body>
	<a style="text-decoration:none;" href="<?= site_url('backend/menu');?>"><div id="menuButton">Menu</div></a>

	<div id="delete_editor" >	
		<div>Are you sure you wish to delete this picture?</div>
		<div style="overflow:hidden">
			<div class="photo_delete_yes button_skin" pid="" typ="" style="width: 80px;margin-top: 3px;float:left;">YES</div>
			<div class="photo_delete_no button_skin" style="width: 80px;margin-top: 3px;float:right;">NO</div>
		</div>					
		
	</div>
	<? foreach($images as $image):?>
		<div class="image_floater">
			<img pid="<?= $image['id'];?>" typ="<?= $image['type'];?>" class="overwatch_img" src="<?= site_url('items/uploads/'.$image['folder'].'/'.$image['fname'])?>">
		</div>
		
	
	<? endforeach;?>
</body>
</html>

<div id="content">
	<div style="position:relative;top:30px;">
		<div class="statics_subheader"><?= $album->title;?></div>
	</div>

	<a href="<?= site_url('albums/'.$user);?>">
		<div class="button_skin" style="width: 75px;position: absolute;top: 35px;left: 220px;z-index:300;">Back</div>				
	</a>
	<div id="pictures_holder" style="top:30px;">
		<? if($count_pictures == 0){?>
			<div class="no_photo_text"><?= $this->lang->line('no_photo_yet')?></div>
		<? }?>
		<?if(isset($pictures)){
			foreach($pictures as $pic){?>
					<div class="album_item">
						<a title="<?= $pic->title; ?>" href="<?= site_url('items/uploads/images/'.$pic->fname);?>" data-lightbox="pictures">
							<div class="album_item_image_holder">
								<img class="album_item_image" src="<?= site_url('items/uploads/images/'.$pic->fname);?>"/>
							</div>
						</a>
						<div class="photo_title_holder">
							
						</div>					
					</div>
		<? 
		}
			}?>
	</div>
</div>
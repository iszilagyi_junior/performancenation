<script>	
	$('#sidemenu').css({top: 0});
</script>
<div id="content">
	<div class="statics_subheader" style="margin-top:35px;">
		<?= $this->lang->line('sidemenu_member_activities')?>
	</div>
	<div class="sansitalic">
		<?= $this->lang->line('activity_plan')?><br/>
		<?= $this->lang->line('activity_invite_friend')?>
	</div>
	<div style="overflow:hidden;">
		<?php 
			$data = array('id' => 'add_activity_form');
			echo form_open("statics/add_activity", $data);?>
		<div id="album_hl"><?= $this->lang->line('new_activity')?></div><br/>
		
		<? if($selected_language == "de"){?>
			<input style="margin-top:0px;height:38px;width:55px;" type="text" id="register_birthday_day" name="register_birthday_day" 
				value="<?php if(isset($birthday_day_repop) && $birthday_day_repop != "") echo $birthday__day_repop; else echo $this->lang->line('register_birthday_day')?>" 
				defaultText="<?= $this->lang->line('register_birthday_day')?>" maxlength="2" class="date_short <?php if(!isset($birthday_day_repop) || $lastname_repop == "") echo "input_default";?>">	

			<input style="margin-top:0px;height:38px;width:55px;" type="text" id="register_birthday_month" name="register_birthday_month" 
				value="<?php if(isset($birthday_month_repop) && $birthday_month_repop != "") echo $birthday_month_repop; else echo $this->lang->line('register_birthday_month')?>" 
				defaultText="<?= $this->lang->line('register_birthday_month')?>" maxlength="2" class="date_short <?php if(!isset($birthday_month_repop) || $lastname_repop == "") echo "input_default";?>">		
						<input style="margin-top:0px;height:38px;width:89px;" type="text" id="register_birthday_year" name="register_birthday_year" 
				value="<?php if(isset($birthday_year_repop) && $birthday_year_repop != "") echo $birthday_year_repop; else echo $this->lang->line('register_birthday_year')?>" 
				defaultText="<?= $this->lang->line('register_birthday_year')?>" maxlength="4" class="date_long <?php if(!isset($birthday_year_repop) || $lastname_repop == "") echo "input_default";?>">					
		<? }
		else
		{?>
			<input style="margin-top:0px;height:38px;width:55px;" type="text" id="register_birthday_month" name="register_birthday_month" 
				value="<?php if(isset($birthday_month_repop) && $birthday_month_repop != "") echo $birthday_month_repop; else echo $this->lang->line('register_birthday_month')?>" 
				defaultText="<?= $this->lang->line('register_birthday_month')?>" maxlength="2" class="date_short <?php if(!isset($birthday_month_repop) || $lastname_repop == "") echo "input_default";?>">		
			<input style="margin-top:0px;height:38px;width:55px;" type="text" id="register_birthday_day" name="register_birthday_day" 
				value="<?php if(isset($birthday_day_repop) && $birthday_day_repop != "") echo $birthday__day_repop; else echo $this->lang->line('register_birthday_day')?>" 
				defaultText="<?= $this->lang->line('register_birthday_day')?>" maxlength="2" class="date_short <?php if(!isset($birthday_day_repop) || $lastname_repop == "") echo "input_default";?>">	
			<input style="margin-top:0px;height:38px;width:89px;" type="text" id="register_birthday_year" name="register_birthday_year" 
				value="<?php if(isset($birthday_year_repop) && $birthday_year_repop != "") echo $birthday_year_repop; else echo $this->lang->line('register_birthday_year')?>" 
				defaultText="<?= $this->lang->line('register_birthday_year')?>" maxlength="4" class="date_long <?php if(!isset($birthday_year_repop) || $lastname_repop == "") echo "input_default";?>">					

	<?	}
		?>					
		<!--<input type="text" id="activity_date" name="activity_date" placeholder="<?= $this->lang->line('new_activity_date')?>" class="edit_input" style="margin-top:0px;font-size:1.2em;"> -->
		<input type="text" id="activity_time" name="activity_time" placeholder="<?= $this->lang->line('new_activity_time')?>" class="suggest_input" style="">
		<input type="text" id="activity_place" name="activity_place" placeholder="<?= $this->lang->line('new_activity_place')?>" class="suggest_input" style="">
		<textarea type="text" id="activity_about" name="activity_about" placeholder="<?= $this->lang->line('new_activity_about')?>" class="suggest_input" style="height:81px;resize:none;font-size:14px;line-height:20px;"></textarea>
		<div id="add_album_btn" style="top:260px;">
			<div id="add_activity" style="top: 0px;width: 220px;" class="album_arrow_text"><?= $this->lang->line('add_new_activity_button')?></div>
		</div> 
		<?php echo form_close();?>
	</div>
	<div id="edit_error_message" style="position: relative;">
		<?php if(isset($message)) echo $message;?>
	</div>
	<div id="activities_holder_main">
		<div class="statics_subheader" style="height:40px;line-height:40px;"><?= $this->lang->line('pending_activities')?></div>
		<div id="pending_activity_holder">
		<? if(count($pending_activities)==0){?>
			<div class="no_photo_text"><?= $this->lang->line('no_pending_activity')?></div>
		<? }?>
			<?if($pending_activities){ $x = 1;
				foreach($pending_activities as $pending){?>				
												
							<div class="activity_item" <? if($x%3 == 0){ echo "style='margin-right:0px'";}?>>	
								<a style="color:#4b4b4b;text-decoration:none;" href="<?= site_url('activity/'.$pending['id']);?>">	
									 
									
									<div class="activity_item_name"><?= $pending['created_by'];?></div>
									<div class="sansitalic" style="margin:5px 0px;"><?= $this->lang->line('invited_you')?></div>
									<div class="activity_item_place"><?= $pending['place'];?></div>
									<div class="sansitalic" style="margin:5px 0px;"><?= $this->lang->line('at')?></div>					
									<div class="activity_item_date"><?= $pending['date'];?></div>									
									<div class="activity_item_time"><?= $pending['time'];?></div>
									
									
								</a>							
								<div class="accept_activity" activity="<?= $pending['id'];?>"><?= $this->lang->line('accept_activity')?></div>
								<div class="decline_activity" activity="<?= $pending['id'];?>"><?= $this->lang->line('decline_activity')?></div>
							</div>
						
					
			<? 
			$x++;}
				}?>
		</div>	
		<br>
		
		<div id="joined_activities">
			<div class="statics_subheader" style="height:40px;line-height:40px;">
				<?= $this->lang->line('joined_activities')?>		
			</div>
			<div id="activity_holder">
			<? if(count($activities)==0){?>
				<div class="no_photo_text"><?= $this->lang->line('no_upcoming_activity')?></div>
			<? }?>
				<?if($activities){
				 $x = 1;
					foreach($activities as $activity){?>							
								<a style="color:#4b4b4b;text-decoration:none;" href="<?= site_url('activity/'.$activity['id']);?>">							
								<div class="activity_item" <? if($x%3 == 0){ echo "style='margin-right:0px'";}?>>		
									<? 
									if($activity['same_user'] == 0)
									{ ?>
										<div class="activity_item_name"><?= $activity['created_by'];?></div>
										<div class="sansitalic" style="margin:5px 0px;"><?= $this->lang->line('invited_you')?></div>
									<?}
									else
									{?>
										<div class="activity_item_name"><?= $this->lang->line('you_created_invitation')?></div>
									<? }?>
									
									
									
									
									
									<div class="activity_item_place"><?= $activity['place'];?></div>
									<div class="sansitalic" style="margin:5px 0px;"><?= $this->lang->line('at')?></div>					
									<div class="activity_item_date"><?= $activity['date'];?></div>									
									<div class="activity_item_time"><?= $activity['time'];?></div>
								</div>
							</a>
						
				<? 
				$x++;}
					}?>
			</div>	
		</div>
		<br>
	</div>
</div>
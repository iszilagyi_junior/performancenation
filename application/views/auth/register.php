<script>
	var text = "<?= $this->lang->line('register_top_header')?>";
	$('.page_header').text(text);
	
	$(document).ready(function()
	{	
		$("#downloadBG, #socialBG, #welcomeImg").hide();
		registerListeners();
		
	});
</script>
			<div id="content">
				<div id="register_header" class="content_header">
					<?= $this->lang->line('register_header')?>
				</div>
				<hr class="hline">
				<div class="sansitalic" style="text-align:center;font-size:13px;color:#4b4b4b;">
					<?= $this->lang->line('register_teaser')?>
				</div>
					
				<div id="login_error_message">
					<?php  echo $message;?>
				</div>
				<div id="login_form">
					<?php 
						$data = array('id' => 'register_form');
						echo form_open("auth/register_user", $data);
					?>
					<input type="text" id="register_email" name="register_email" 
						value="<?php if(isset($email_repop) && $email_repop != "") echo $email_repop; else echo $this->lang->line('register_email')?>" 
						defaultText="<?= $this->lang->line('register_email')?>" 
						class="register_input <?php if(!isset($email_repop) || $email_repop == "") echo "input_default";?>">
					<br>
					<input type="text" id="register_firstname" name="register_firstname" 
						value="<?php if(isset($firstname_repop) && $firstname_repop != "") echo $firstname_repop; else echo $this->lang->line('register_firstname');?>" 
						defaultText="<?= $this->lang->line('register_firstname')?>" 
						class="register_input <?php if(!isset($firstname_repop) || $firstname_repop == "") echo "input_default";?>">
					<br>
					<input type="text" id="register_lastname" name="register_lastname" 
						value="<?php if(isset($lastname_repop) && $lastname_repop != "") echo $lastname_repop; else echo $this->lang->line('register_lastname')?>" 
						defaultText="<?= $this->lang->line('register_lastname')?>" 
						class="register_input <?php if(!isset($lastname_repop) || $lastname_repop == "") echo "input_default";?>">
				
					<div class="" style=""><?= $this->lang->line('register_country')?></div>
					<select style="margin-top:5px;width:190px;" id="register_country" name="register_country" class="edit_input">
						<option value="0"><?= $this->lang->line('edit_user_select_country')?></option>
						<? foreach($countrydata as $country){?>
							<option value="<?= $country->id;?>"><?= $country->name;?></option>
						<?}?>					
					</select>
					
					<div id="state_holder" style="display:none;">
						<div class="" style=""><?= $this->lang->line('register_state')?></div>
						<select style="margin-top:5px;width:190px;" id="register_state" name="register_state" class="edit_input">
							<option value="0"><?= $this->lang->line('edit_user_select_state')?></option>
							<? foreach($statedata as $state){?>
								<option value="<?= $state->abbreviation;?>"><?= $state->name;?></option>
							<?}?>					
						</select>
					</div>	
					<div class="" style=""><?= $this->lang->line('register_pw_length')?></div>
					<input type="password" id="register_password" name="register_password" class="register_input password_real" placeholder="<?= $this->lang->line('register_password')?>">
					<!-- <input type="text" id="register_password_fake" name="register_password_fake" value="<?= $this->lang->line('register_password')?>" defaultText="<?= $this->lang->line('register_password')?>" class="register_input input_default password_fake"> -->
					<br>
					<input type="password" id="register_passconf" name="register_passconf" class="register_input password_real" placeholder="<?= $this->lang->line('register_passconf')?>">
					<!-- <input type="text" id="register_passconf_fake" name="register_passconf_fake" value="<?= $this->lang->line('register_passconf')?>" defaultText="<?= $this->lang->line('register_passconf')?>" class="register_input input_default password_fake">  -->
					
					
					<br/>
					<br/>
					<div class="sansbold" style=""><?= $this->lang->line('register_birthday')?></div>
					
					<? if($selected_language == "de"){?>
					<input type="text" id="register_birthday_day" name="register_birthday_day" 
						value="<?php if(isset($birthday_day_repop) && $birthday_day_repop != "") echo $birthday__day_repop; else echo $this->lang->line('register_birthday_day')?>" 
						defaultText="<?= $this->lang->line('register_birthday_day')?>" maxlength="2" class="date_short <?php if(!isset($birthday_day_repop) || $lastname_repop == "") echo "input_default";?>">

					
					
					<input style="" type="text" id="register_birthday_month" name="register_birthday_month" 
						value="<?php if(isset($birthday_month_repop) && $birthday_month_repop != "") echo $birthday_month_repop; else echo $this->lang->line('register_birthday_month')?>" 
						defaultText="<?= $this->lang->line('register_birthday_month')?>" maxlength="2" class="date_short <?php if(!isset($birthday_month_repop) || $lastname_repop == "") echo "input_default";?>">	
					
										
					<input type="text" id="register_birthday_year" name="register_birthday_year" 
						value="<?php if(isset($birthday_year_repop) && $birthday_year_repop != "") echo $birthday_year_repop; else echo $this->lang->line('register_birthday_year')?>" 
						defaultText="<?= $this->lang->line('register_birthday_year')?>" maxlength="4" class="date_long <?php if(!isset($birthday_year_repop) || $lastname_repop == "") echo "input_default";?>">					
					<? }
					else
					{?>
						<input style="" type="text" id="register_birthday_month" name="register_birthday_month" 
						value="<?php if(isset($birthday_month_repop) && $birthday_month_repop != "") echo $birthday_month_repop; else echo $this->lang->line('register_birthday_month')?>" 
						defaultText="<?= $this->lang->line('register_birthday_month')?>" maxlength="2" class="date_short <?php if(!isset($birthday_month_repop) || $lastname_repop == "") echo "input_default";?>">	
					
					<input type="text" id="register_birthday_day" name="register_birthday_day" 
						value="<?php if(isset($birthday_day_repop) && $birthday_day_repop != "") echo $birthday__day_repop; else echo $this->lang->line('register_birthday_day')?>" 
						defaultText="<?= $this->lang->line('register_birthday_day')?>" maxlength="2" class="date_short <?php if(!isset($birthday_day_repop) || $lastname_repop == "") echo "input_default";?>">
					
					<input type="text" id="register_birthday_year" name="register_birthday_year" 
						value="<?php if(isset($birthday_year_repop) && $birthday_year_repop != "") echo $birthday_year_repop; else echo $this->lang->line('register_birthday_year')?>" 
						defaultText="<?= $this->lang->line('register_birthday_year')?>" maxlength="4" class="date_long <?php if(!isset($birthday_year_repop) || $lastname_repop == "") echo "input_default";?>">					

					
					
					<?	}
					?>			
					<!-- <input type="text" id="register_birthday" name="register_birthday" readonly
						value="<?php if(isset($birthday_repop) && $birthday_repop != "") echo $birthday_repop; else echo $this->lang->line('register_birthday')?>" 
						defaultText="<?= $this->lang->line('register_birthday')?>" 
						class="register_input <?php if(!isset($birthday_repop) || $birthday_repop == "") echo "input_default";?>"> -->
					<br>
					<div id="termsHolder">
						<input type="checkbox" id="terms" name="register_terms" value="1" style="margin-right:10px;"><a target="_blank" style="" href="<?= site_url('mobile_terms')?>"><?= $this->lang->line('register_terms')?></a></input>
						<input type="hidden" id="fbIdHolder" name="fbIdHolder" value="0" />
					</div>	
					<div id="termsHolder">
						<input type="checkbox" checked="checked" id="newsletter" name="register_newsletter" value="1" style="margin-right:10px;"><?= $this->lang->line('register_newsletter')?></input>					
					</div>					
					<br>
					<div class="sansitalic" style=""><?= $this->lang->line('promo_code_desc')?>:</div>
					
					<input style="" type="text" id="register_promo_code" name="register_promo_code" 
						value="<?php if(isset($promo_repop) && $promo_repop != "") echo $promo_repop; else echo $this->lang->line('promo_code')?>" 
						defaultText="<?= $this->lang->line('promo_code')?>"  class="register_input <?php if(!isset($promo_repop) || $promo_repop == "") echo "input_default";?>">	
					<br/>
					<br/>
					<button type="submit" class="register_button" action="register" ><?= $this->lang->line('register_confirm')?></button>
					<?php echo form_close();?>	
				</div>
				
				<div class="separator">
					<hr class="hline" style="float:left;margin-top:9px;width:80px">
					<span class="sansitalic" style="float:left;margin:0px 20px;"><?= $this->lang->line('register_or')?></span>
					<hr class="hline" style="float:left;margin-top:9px;width:80px">
				</div>
				<div style="width:210px;margin:60px auto; 20px auto">
					<img id="fbReg" action="register" src="<?= site_url('items/frontend/img/snoopstr_fb_signin.png');?>">
				</div>
				
			</div>
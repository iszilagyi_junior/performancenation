<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "performancenation";
$route['404_override'] = '';



/* SITE */
$route['home'] = "";
$route['terms'] = "performancenation/terms";
$route['news'] = "performancenation/news";
$route['invitation/(:any)'] = "performancenation/invitation/$1";
$route['help'] = "performancenation/help";
$route['group_create_terms'] = "performancenation/group_create_terms";
$route['logout'] = "Auth/logout";
$route['reset_password/(:any)'] = "Auth/reset_password/$1";
$route['messages'] = "statics/messages";
$route['group_search'] = "statics/group_search";
$route['event_search'] = "statics/event_search";
$route['group/(:any)'] = "statics/group_detail/$1";
$route['edit_group/(:num)'] = "statics/edit_group/$1";
$route['poi_search'] = "statics/poi_search";
$route['poi/(:num)'] = "statics/poi_detail/$1";
$route['group_event/(:num)'] = "statics/group_event_detail/$1";
$route['user_event/(:num)'] = "statics/user_event_detail/$1";
$route['send_news_comment'] = "statics/send_news_comment";
$route['send_group_post'] = "statics/send_group_post";

/* USERS */
$route['ajax_upload/(:any)'] = "statics/ajax_upload/$1";
$route['editprofile'] = "Auth/edit";
$route['my_profile'] = "performancenation/my_profile";
$route['my_groups'] = "performancenation/my_groups";
$route['my_events'] = "performancenation/my_events";
$route['my_connections'] = "performancenation/my_connections";
$route['profile/(:any)'] = 'performancenation/user_detail/$1';


/* VEHICLES */
$route['switch_vehicle'] = "performancenation/switch_vehicle";
$route['my_vehicles'] = "car/my_vehicles";
$route['vehicle_profile/(:any)'] = 'car/vehicle_detail_pretty/$1';
$route['new_vehicle'] = "car/create_view";
$route['edit_vehicle/(:any)'] = 'car/edit_view/$1';
$route['album/(:num)'] = 'statics/album/$1';
$route['edit_album/(:num)'] = 'statics/edit_album/$1';
$route['albums/(:num)'] = "statics/albums/$1";
$route['add_photo_newsfeed'] = "statics/add_photo_newfeed";
$route['vehicle_search'] = "statics/vehicle_search";
$route['add_mod'] = "statics/add_mod";


// api
$route['api/edit_profile/(:num)'] = 'api/edit_user_profile/$1';
$route['api/update_profile'] = 'api/set_user_profile';
$route['api/my_overview'] = 'api/my_vehicles';
$route['api/vehicle_profile/(:num)'] = 'api/vehicle_detail/$1';
$route['api/update_cover/(:num)'] = 'api/upload_cover/$1';
$route['api/update_profile/(:num)'] = 'api/upload_profile/$1';
$route['api/upload_picture/(:num)'] = 'api/upload_picture/$1';
$route['api/get_vehicle_albums/(:num)'] = 'api/get_vehicle_albums/$1';
$route['api/get_album/(:num)'] = 'api/show_single_album/$1';
$route['api/save_vehicle_to_session'] = 'api/save_vehicle_to_session';
$route['api/update_location'] = 'api/update_location';
$route['api/get_event_comments'] = 'api/get_event_comments';
$route['api/send_event_comment'] = 'api/send_event_comment';
$route['api/send_image_comment'] = 'api/send_image_comment';
$route['api/add_like'] = 'api/add_like';
$route['api/message_overview'] = 'api/message_overview';
$route['api/delete_message'] = 'api/delete_message';
$route['api/show_conversation'] = 'api/show_conversation';
$route['api/send_message'] = 'api/send_message';
$route['api/poi_detail'] = 'api/poi_detail';
$route['api/event_detail'] = 'api/event_detail';
$route['api/get_mod_types'] = 'api/get_mod_types';
$route['api/get_sub_mod_types'] = 'api/get_sub_mod_types';
$route['api/get_vehicle_mods'] = 'api/get_vehicle_mods';
$route['api/add_mod'] = 'api/add_mod';
$route['api/rate_poi'] = 'api/rate_poi';
$route['api/add_image_poi'] = 'api/add_image_poi';
$route['api/follow_vehicle'] = 'api/follow_vehicle';
$route['api/unfollow_vehicle'] = 'api/unfollow_vehicle';
$route['api/join_event'] = 'api/join_event';
$route['api/leave_event'] = 'api/leave_event';
$route['api/add_traffic_alert'] = 'api/add_traffic_alert';
$route['api/add_status_update'] = 'api/add_status_update';
$route['api/delete_story_piece'] = 'api/delete_story_piece';
$route['api/getMake'] = 'api/getMake';
$route['api/delete_vehicle'] = 'api/delete_vehicle';
$route['api/get_all_friends'] = 'api/get_all_friends';




$route['backend'] = "backend/index";
$route['impressum'] = "performancenation/impressum";
$route['mobile_terms'] = "performancenation/mobile_terms";
$route['imprint'] = "performancenation/impressum";
$route['contact'] = "performancenation/contact";
$route['about'] = "performancenation/about";
$route['register'] = "Auth/register";
$route['register_success'] = "Auth/register_success";





$route['editprofile/(:any)'] = "Auth/edit_ie/$1";

$route['login/(:any)'] = "Auth/login/$1";


$route['edit_cover/(:num)/(:any)'] = 'car/edit_cover_ie/$1/$2'; 
$route['edit_carprofile/(:num)/(:any)'] = 'car/edit_dogprofile_ie/$1/$2'; 


$route['share'] = "statics/share";

$route['albums'] = "statics/albums";
$route['statics/add_photo_album/(:any)'] = "statics/add_photo_album/$1";



$route['tagged/(:num)'] = "statics/dog_tagged_photos/$1";
$route['my_friends'] = "dog/my_friends";
$route['my_enemies'] = "dog/my_enemies";


//$route['dog/(:num)'] = 'dog/dog_detail/$1';

$route['place/(:any)'] = 'statics/place_detail_pretty/$1';



$route['show_album/(:num)'] = 'statics/show_album/$1';
$route['add_photo'] = "statics/add_photo";
$route['photos/(:num)'] = 'statics/photos/$1';
$route['photos'] = "statics/albums";
$route['photo/(:num)'] = 'statics/photo/$1';
$route['activities'] = "statics/activities";
$route['activity/(:num)'] = 'statics/activity/$1';
$route['edit_activity/(:num)'] = 'statics/edit_activity/$1';
$route['nopreview'] = 'performancenation/nopreview';
$route['territory'] = 'statics/territory';
$route['dog_iframe/(:num)'] = 'dog/dog_iframe/$1';
$route['api/(:num)'] = 'api/index/$1';
$route['friends/(:num)'] = "dog/owner_friends/$1";
$route['enemies/(:num)'] = "dog/owner_enemies/$1";
$route['gethashtags'] = "statics/saveHashtagsToDB";
$route['showhashtags'] = "statics/showHashtags";
$route['partner'] = "statics/partner";
$route['vet_login'] = "statics/vet_interface";
$route['bookings'] = "statics/bookings";

$route['welcome_mail'] = "statics/welcome_mail";
$route['places'] = "statics/places";
$route['rc_gewinnspiel'] = "performancenation/rc_gewinnspiel";






$route['get_friends/(:num)'] = 'api/get_friends/$1';
$route['get_all_friends'] = 'api/get_all_friends';
$route['get_enemies/(:num)'] = 'api/get_enemies/$1';



$route['get_breeds'] = 'api/get_breeds';
$route['add_dog_profile'] = 'api/add_dog_profile';
$route['update_dog_profile/(:num)'] = 'api/update_dog_profile/$1';

$route['api/login'] = 'api/login';
$route['api/loginFB'] = 'api/loginUserFB';
$route['api/register'] = 'api/register';
$route['api/addDogFriend'] = 'api/addDogFriend';
$route['api/nearbyVehicles'] = 'api/nearbyVehicles';
$route['api/nearbyPois'] = 'api/nearbyPois';
$route['api/nearbyEvents'] = 'api/nearbyEvents';
$route['api/activities'] = 'api/activities';
$route['api/activity_detail'] = 'api/activity_detail';
$route['api/update_location'] = 'api/update_location';
$route['api/searchPlaceName'] = 'api/searchPlaceName';
$route['api/searchVetName'] = 'api/searchVetName';
$route['api/searchDogName'] = 'api/searchDogName';
$route['api/searchSitterName'] = 'api/searchSitterName';
$route['api/get_user_pictures'] = 'api/get_user_pictures';
$route['api/nearbyVets'] = 'api/nearbyVets';
$route['api/nearbyPlaces'] = 'api/nearbyPlaces';
$route['api/nearbySitters'] = 'api/nearbySitters';
$route['api/nearbyZones'] = 'api/nearbyZones';
$route['api/searchZoneName'] = 'api/searchZoneName';
$route['api/accept_activity'] = 'api/accept_activity';
$route['api/decline_activity'] = 'api/decline_activity';
$route['api/suggest_zone_description'] = 'api/suggest_zone_description';
$route['api/rate'] = 'api/rate';
$route['api/check_in_zone'] = 'api/check_in_zone';
$route['api/check_in_vet'] = 'api/check_in_vet';

$route['api/add_like'] = 'api/add_like';
$route['api/add_comment'] = 'api/add_comment';
$route['api/get_photo_comments'] = 'api/get_photo_comments';
$route['api/get_photo_likes'] = 'api/get_photo_likes';

$route['api/get_vet'] = 'api/get_vet';
$route['api/get_zone'] = 'api/get_zone';
$route['api/get_sitter'] = 'api/get_sitter';
$route['api/get_place'] = 'api/getPlace';

$route['api/delete_photo'] = 'api/delete_photo';
$route['instagram'] = 'statics/my_instagram';
$route['api/send_invite_mail'] = 'api/send_invite_mail';

$route['api/remove_connection'] = 'api/removeConnection';
$route['api/reportImage'] = 'api/reportImage';
$route['api/reportProfile'] = 'api/reportProfile';
$route['api/randomDogs'] = 'api/randomDogs';
$route['api/getStoredCountries'] = 'api/getStoredCountries';
$route['api/getStoredStates'] = 'api/getStoredStates';

$route['app'] = 'performancenation/app_redirecter';
$route['press'] = "performancenation/press";

$route['checkCountry'] = "statics/checkCountry";


$route['mail_test'] = "statics/mail_test";

$route['Auth/activate_user'] = "Auth/activate_user";
//$route['(:any)'] = "performancenation/$1";

//$route['Auth/(:any)'] = "Auth/$1";
//$route['Auth'] = "Auth/index/$1";

/* End of file routes.php */
/* Location: ./application/config/routes.php */
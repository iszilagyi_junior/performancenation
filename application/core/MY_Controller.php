<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller 
{
    	
    function __construct()
    {
        parent::__construct();
		
		
		$this->connectDB();
		
		$this->load->model('auth_model');
		$this->load->model('statics_model');
		
        date_default_timezone_set('Europe/Vienna');
       
         
        
            
    }  
    
    public function connectDB()
    {
        if(strpos(site_url(), 'localhost') === false)
        {
            if(strpos(site_url(), 'fileserver') === false)
            {
                $this->load->database('productive');
            }
            else
                $this->load->database('fileserver');
        }
        else
            $this->load->database('localhost');
    }  
    
	function checkIPaddress()
	{	
		/*$user_ip = $this->input->ip_address();
		
		
		$ips = $this->auth_model->getIPs()->result();
		$ip_array = array();
		if($ips != false){
			foreach($ips as $ip){
				$ip_array[] = $ip->ip;
			}
		
		
			if(!in_array($user_ip, $ip_array))
			{
				redirect('nopreview');
			}
		
		}	*/
			
	}


	function addPoints($vehicle_id,$plus_points,$count_column, $comment = '')
	{
			
		$points = $plus_points; 
		$points_data = array('vehicle_id' => $vehicle_id,
							'points' => $points,
							'comment' => $comment);
	
		$this->car_model->addVehiclePoints($points_data);
		
		//UPDATE COUNTER
		$game_counter = $this->car_model->checkGameCounter($vehicle_id);
		if(!$game_counter)
		{
			$count = 1; 
			$game_data = array('vehicle_id' => $vehicle_id,
									$count_column => $count);
			
			$this->car_model->addGameCounter($game_data);
		}
		else
		{
			$count = $game_counter->$count_column + 1; 
			$game_data = array('vehicle_id' => $vehicle_id,
									$count_column => $count);
			
			$this->car_model->updateGameCounter($game_data);
		}		
	}
	
	
	function addUserPoints($user_id,$plus_points)
	{
			
		$addpoints = false;	 				
		$getLastlogin = $this->statics_model->checkLastUserPoints($user_id);
		if($getLastlogin)
		{

			$timestamp = $getLastlogin->created_date;
			$timestamp = trim($timestamp);
			
			$today = new DateTime(); // This object represents current date/time
			$today->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison
			
			$match_date = new DateTime($timestamp);
			$match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

			$diff = $today->diff( $match_date );
			$diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval

			if($diffDays < 0)
			{
				$addpoints = true;
			}
			
			
		}
			 
		if($addpoints || !$getLastlogin)
		{
			$points = $plus_points; 
			$points_data = array('user_id' => $user_id,
								'points' => $points);
			$this->statics_model->addUserPoints($points_data);	
		}
		
	}
	
	
	
	
	function my_hash($hash)
	{
		require_once(APPPATH.'libraries/PasswordHash.php');
		
		$hasher = new PasswordHash(8, false);
		
		return $hasher->HashPassword($hash);
	}

	function check_hash($hash, $stored_hash)
	{
		require_once(APPPATH.'libraries/PasswordHash.php');
		
		$hasher = new PasswordHash(8, false);
		
		return $hasher->CheckPassword($hash, $stored_hash);		
	}
	
	function set_user_timezone($date)
	{
		
		$original_datetime = $date;
		$original_timezone = new DateTimeZone('Europe/Vienna');
		
		// Instantiate the DateTime object, setting it's date, time and time zone.
		$datetime = new DateTime($original_datetime, $original_timezone);
		
		
		$default_timezone = get_cookie('timezoneoffset');
        if($default_timezone == null)
        	$default_timezone = 'Europe/Vienna';
        else 
            $default_timezone = $default_timezone;
		
		
		
		// Set the DateTime object's time zone to convert the time appropriately.
		$target_timezone = new DateTimeZone($default_timezone);
		$datetime->setTimeZone($target_timezone);
		
		
		
		
		// Outputs a date/time string based on the time zone you've set on the object.
		$triggerOn = $datetime->format('m-d-Y h:i A');
		
		return $triggerOn;
	}
	
	
	public function hashtags($limit = 2, $offset = 0){
		$checkHashtag = $this->statics_model->checkUserHashtag($this->session->userdata('user_id'))->row()->show_hashtags;
		
		$oldImages = $this->statics_model->getHashtags()->result();
		
		$image_container = array();
		
		
		foreach($oldImages as $i=>$image){			
			$image_container[] = array('link' => $image->target_url,
		    						'img_url' => $image->fname,
		    						'tags' => $image->tags,
		    						'username' => $image->username,
		    						'upload_date' => $image->created_date);
		}

		if($checkHashtag == 1){
			$image_container = $image_container;
		}
		else{
			$image_container = array();
		}
		
		return array_slice($image_container, $offset, $limit);
		
	}
	
	public function getNewest($limit = 11, $offset = 0)
    {
    	$this->load->model('statics_model');
		$this->load->model('car_model');
		$vehicles = $this->car_model->getVehicles($this->session->userdata('user_id'));
		$friends_container = array();
		foreach($vehicles->result() as $vehicle){
			$friends_container[] = $vehicle->id;
			$friends = $this->car_model->getFriends($vehicle->id);
			foreach($friends->result() as $i=>$friend){				
				if (!in_array($friend->id, $friends_container)) {
			   	 	$friends_container[] = $friend->id;
			    }
			}
	
		}
		
		$whatsnew_container[] = array();
		$event_ids = array();
		$events = $this->statics_model->getAllEvents()->result();
		
		
		if(!empty($friends_container)){
		
			foreach($events as $event){		
			//	$result = $this->statics_model->getEvents($friend_id);
	
				$vehicle = $this->car_model->getVehicle($event->vehicle_id)->row();
				
				if(in_array($event->vehicle_id, $friends_container))
				{
			
					$vehiclename = $vehicle->nickname;
					$vehicleprofile = $this->statics_model->getImageByID($vehicle->profile_image)->row()->fname;
					$vehicle_cover = $this->statics_model->getImageByID($vehicle->cover_image)->row()->fname;
					$pretty_url = $vehicle->pretty_url;
					$vehicle_type = $vehicle->vehicle_type;		
					$event_action = $event->action_id;
					
					$event_entity = $event->entity_id;				
					$event_additional_id = $event->additional_id;
					$event_date = $this->set_user_timezone($event->created_date);
					$image_title = "";
					$event_likes = 0;
					
					
					switch($event_action){
						case 1:{ //nickname
						/*	$friend_dog = $this->car_model->getDog($event->entity_id);
							$pretty = $friend_dog->pretty_url;
							$text = $this->lang->line('event_added_friend');
							$event_text = $this->lang->line('event_new_friend');
							$entity_content ="";
							$entity_link = "profile/".$pretty;
							$entity_name = $this->car_model->getDog($event_entity)->name;
							$event_picture = "";
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "nickname_change";
							*/
						};break;
						case 2:{ //story piece
							
							
							$text = " added new Project Blog piece ";
							$event_text = "new Story";
							$entity_content = $this->car_model->getStoryPieceById($event->additional_id)->row()->story;
							$entity_link = "";
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
							$event_picture = "";
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "story_piece";
						};break;
						case 3:{ //new vehicle profile
							
							$usr = $this->statics_model->getUserData($vehicle->user_id)->row();
							$text = "new vehicle by ";
							$event_text = "";
							$entity_content ="";
							$entity_link = "profile/".$usr->username;
							$entity_name = $usr->first_name." ".$usr->last_name;
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;							
							$event_picture = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "new_vehicle";
						};break;
						case 4:{ //new modification
						
					
							switch($vehicle_type)
							{
								case 0:{ $table = "car_mod_history";}break;
								case 1:{ $table = "motorcycle_mod_history";}break;
								case 2:{ $table = "truck_mod_history";}break;
							}
							
							$mod_history = $this->car_model->getMod($table,$event->additional_id);
							$t = preg_replace('/_+/', ' ', $mod_history->mod_text);
							$text = " added new modification: ". ucwords($t);
							$event_text = "new Mod";
							$entity_content = $mod_history->mod;
							$entity_link = "";
							$entity_name = "";
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
							$event_picture = site_url()."items/frontend/img/mods/".$mod_history->mod_text.".png";
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "new_modification";
						};break;
						case 5:{ //created event NEED TO CHANGE
							$text = " created event";
							$event_text = "Created event";
							$entity_content ="";
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
							$entity_link = "user_event/".$event_entity;
							$entity_name = $this->statics_model->getUserEventById($event_entity)->title;
							$event_picture = "";
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "event_created";
							
						};break;
						case 6:{ //changed profile picture
							
							$vehicle_profile = $this->statics_model->getImageByID($event->additional_id)->row()->fname;

							$image_id = $event->additional_id;
							$text = "Changed profile picture";
							$event_text = "New profile picture";
							$entity_content ="";
							$entity_link = "";
							$entity_name = "";
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
						//	$event_likes = $this->statics_model->getPicture($event->additional_id)->row()->likes;
							$event_picture = site_url()."items/uploads/profilepictures/".$vehicle_profile;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "new_profile";
						};break;
						case 7:{ //changed cover picture
							
							$vehiclecover = $this->statics_model->getImageByID($event->additional_id)->row()->fname;
							
							
							$image_id = $event->additional_id;
							$text = "Changed cover picture";
							$event_text = "New cover picture";
							$entity_content ="";
							$entity_link = "";
							$entity_name = "";
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
						//	$event_likes = $this->statics_model->getPicture($event->additional_id)->row()->likes;
							$event_picture = site_url()."items/uploads/coverpictures/".$vehiclecover;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;
							$event_type = "new_cover";
						};break;
						case 8:{ //added a picture
							$text = "uploaded image";
							$event_text = "";
							$entity_content ="";
							$album_id = $this->statics_model->getAlbumFromPicture($event_entity)->row()->album_id;
							
							$entity_link = "album/".$album_id;
							$album = $this->statics_model->getSingleAlbum($album_id);
							$entity_name = ""; //$album->title;							
							$picture = $this->statics_model->getPicturebyID($event_entity)->row()->fname;
							$image_id = $this->statics_model->getPicturebyID($event_entity)->row()->id;
						//	$event_likes = $this->statics_model->getPicture($image_id)->row()->likes;
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
							$image_title = $this->statics_model->getAlbumFromPicture($event_entity)->row()->title;
							$event_picture = site_url()."items/uploads/images/".$picture;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "new_photo";		
						};break;
						case 9:{ //tagged on photo (this should go to notifications)
							$text = " tagged on photo";
							$event_text = "Tagged";
					  		$entity_content ="";														
							$entity_link = "";							
							$entity_name = "";
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
							$picture = $this->statics_model->getPicture($event_entity)->row()->fname;	
							$image_id = $this->statics_model->getPicture($event_entity)->row()->id;									
							$event_picture = site_url()."items/uploads/images/".$picture;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "tagged";		
						};break;
						case 10:{ // started following xy...
							$vh = $this->car_model->getVehicle($event->additional_id)->row();
							$vh_image = $this->statics_model->getImageByID($vh->profile_image)->row()->fname;
							$veh = $this->car_model->getVehicle($event->entity_id)->row();
							$vehiclename = $veh->nickname;
							$vehicleprofile = $this->statics_model->getImageByID($veh->profile_image)->row()->fname;
							$text = " is following";
							$event_text = "Following";
							$entity_content = "follow";
							$entity_link = "vehicle_profile/".$vh->pretty_url;
							$entity_name = $vh->nickname;
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
							$event_picture = site_url()."items/uploads/profilepictures/".$vh_image;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "following";		
						};break;
						case 11:{ // checked in at POI NEED CHANGE
							$text = " checked in at ";
							$event_text = "Checked in";
							$entity_content = "";
							$entity_link = "poi/".$event_entity;
							$entity_name = $this->statics_model->getPOI($event_entity)->name;
							$event_picture = "";
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "checked_in_POI";		
						};break;
						case 12:{ // rated POI
							$text = " rated";
							$event_text = "rated POI";
							$entity_content = "";
							$entity_link = "poi/".$event_entity;
							$entity_name = $this->statics_model->getPOI($event_entity)->name;
							$event_picture = "";
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "rated_POI";	
						};break; 
						case 13:{ //Liked picture (remove?)
							$image = $this->statics_model->getImageByID($event_entity)->row();
							
							$type = "images";
							
							if($image->cover_image == 1)
							{
								$type = "coverpictures";
							}
							else if($image->profile_image == 1)
							{
								$type = "profilepictures";
							}
							$text = $this->lang->line('event_liked_a');
							$event_text = $this->lang->line('event_liked_picture');
							$entity_content = "";
							$entity_link = "photo/".$event_entity;
							$entity_name = "picture";
							//$event_picture = site_url()."items/uploads/".$type."/".$image->fname;
							$event_picture = "";
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "picture_like";	
						};break;
						case 14:{ //Commented (remove/move to notification?)
							$image = $this->statics_model->getImageByID($event_entity)->row();
							
							$type = "images";
							
							if($image->cover_image == 1)
							{
								$type = "coverpictures";
							}
							else if($image->profile_image == 1)
							{
								$type = "profilepictures";
							}
							$text = $this->lang->line('event_commented_on_a');
							$event_text = $this->lang->line('event_commented_picture');
							$entity_content = "";
							$entity_link = "photo/".$event_entity;
							$entity_name = "picture";
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
							$event_picture = "";
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "comment";	
						};break;
						case 15:{ //traffic alert
							
							$alert = $this->statics_model->getAlert($event->entity_id)->row()->the_alert;
							
							$text = "posted a traffic alert";
							$event_text = "";
							$entity_content = $alert;
							$entity_link = "";
							$entity_name = "";					
							$event_picture = "";
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "traffic_alert";	
						};break;
						
						case 16:{ //update status
							
							$alert = $this->statics_model->getStatus($event->entity_id)->row()->status;
							
							$text = "updated status";
							$event_text = "";
							$entity_content = $alert;
							$entity_link = "";
							$entity_name = "";					
							$event_picture = "";
							$event_likes = $this->statics_model->getNewsEventById($event->id)->likes;
							$the_image = site_url()."items/uploads/profilepictures/".$vehicleprofile;	
							$event_type = "status_update";	
						};break;
						
						
					}
						
						
					if(!in_array($event->id, $event_ids) && ($event_type != "nickname_change" && $event_type != "picture_like" && $event_type != "comment")){
						$comments_holder = array();
						$event_comments = $this->statics_model->getEventComments($event->id);
						
						
						
						foreach($event_comments as $comment)
						{
							$vehicle = $this->car_model->getVehicle($comment->vehicle_id)->row();
		
							$profile = $this->statics_model->getImageByID($vehicle->profile_image)->row();
							
							$new = array('comment' => $comment->comment,
											'nickname' => $vehicle->nickname,
											'profile_image' => $profile->fname);
											
										
										
							$comments_holder[] = $new;
						}
						
						if(strlen($entity_content)>350)
						{
							$pos=strpos($entity_content, ' ', 350);
							$entity_content = substr($entity_content,0,$pos );
							$entity_content = $entity_content."...";
						}
						 
						
						
						$whatsnew_container[] = array('entity_link' => $entity_link,
											'text' => $text,
											'content' => $entity_content,
											'vehiclename' => $vehiclename,
											'entity_name' => $entity_name,
											'event_text' => $event_text,
											'vehicle_id' => $event->vehicle_id,
											'pretty_url' => $pretty_url,
											'profile' => $the_image,
											'image_id' => $image_id,
											'event_image' => $event_picture,
											'image_title' => $image_title,
											'event_type' => $event_type,
											'event_id' => $event->id,
											'created_date' => $event_date,
											'comments' => $comments_holder,
											'likes' => $event_likes);
						$event_ids[] = $event->id;
						
					}
					
					
				}
			}
		}
		
				
		return array_slice($whatsnew_container, $offset, $limit);
		
			
		
		
	}
    
    public function getFriends()
    {
    	$this->load->model('statics_model');
		$this->load->model('car_model');
		$dogs = $this->car_model->getVehicles($this->session->userdata('user_id'));
		
		$x = 0;
		$friends_container = array();
		$friend_ids = array();
		foreach($dogs->result() as $dog){
			$friends = $this->car_model->getFriends($dog->id);
			foreach($friends->result() as $i=>$friend){	
							
					if (!in_array($friend->id, $friend_ids)) {
				   	 	 
				   	 	$friends_container[] = array('id' => $friend->id,											
													'name' => $friend->name,
													'fname' => $friend->profile_picture);
													
						$friend_ids[] =  $friend->id;
				    }
			    
			}
	
		}
	
		
		shuffle($friends_container);
		return $friends_container;
	}
    
    public function getRandomVehiclesForNewsFeed()
    {
	    $vehicles = $this->car_model->getVehicles($this->session->userdata['user_id']);
		$data['random_vehicles'] = array();
		$random_vehicles = $this->car_model->getRandomVehicles()->result();

		$vehicles_data = array();
		$friends_ids_added = array();
		$friends_data = array();
		foreach($vehicles->result() as $vehicle)
		{
			$friends_ids_added[] = $vehicle->id;
			
			
			$my_follows = $this->car_model->getMyFollows($vehicle->id);

			foreach($my_follows as $friend)
			{
				
				if (!in_array($friend->id, $friends_ids_added))
				{
			  
					$friends_ids_added[] = 	$friend->vehicle_id;		  
				}					  
			}	
			
		
		}
		
		foreach($random_vehicles as $random_vehicle)
		{
			if (!in_array($random_vehicle->id, $friends_ids_added))
			{
				
				$profile = $this->statics_model->getImageByID($random_vehicle->profile_image)->row()->fname;
				
				
				$vehicle_data = array('id' => $random_vehicle->id,
								'nickname' => $random_vehicle->nickname,
								'pretty_url' => $random_vehicle->pretty_url,
								'profile_picture' => $profile);
				$data['random_vehicles'][] = $vehicle_data;
			}
			
		}
		
		return $data['random_vehicles'];
    }
    
    public function checkLanguage()
   	{
   		$lang = $this->session->userdata('language');
   		
   		switch($lang)
   		{
	   		case 'de': {
	   					$this->lang->load('doggylicious', 'german');
	   					
	   					};break;
	   		
	   		case 'en': {
	   					$this->lang->load('doggylicious', 'english');
	   					
	   					};break;
	   		
	   		default: { $this->lang->load('doggylicious', 'english');}

   		}
	  	
   	} 
    
    
    public function getAdvertisement($template_name)
    {
	   $advertisement = array();
		$partners = $this->statics_model->getPartners()->result();
		$user_id = $this->session->userdata('user_id');
		$user = $this->statics_model->getUserData($user_id)->row();
		
		$user_country = $user->country;
		$user_state = $user->state;
		if($user_country === NULL || $user_country == "")
		{
		
			if($user->geo_location !== NULL && $user->geo_location != "")
			{
				
				$geocode=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$user->geo_location.'&sensor=false');

		        $output= json_decode($geocode);
				
				for($j=0;$j<count($output->results[0]->address_components);$j++)
				{
						if($output->results[0]->address_components[$j]->types[0] == "country")
						{
							
							$user_country = $output->results[0]->address_components[$j]->short_name;
							$data = array('id' => $user_id,
									'country' => $user_country);
							$this->statics_model->updateUser($data);
						}
						
		                if($output->results[0]->address_components[$j]->types[0] == "administrative_area_level_1")
						{
							$user_state = $output->results[0]->address_components[$j]->short_name;
							$data = array('id' => $user_id,
										'state' => $user_state);
							$this->statics_model->updateUser($data);
						}
						
									
		        }
 
			}
		}
		
				
		
		
		if($user_country !== null && $user_country !== "")
		{
			
			foreach($partners as $partner)
			{
				if($user_country != "US" && $user_country == $partner->country)
				{
					
					$partner_id = $partner->id;
				}
				else
				{
					if($user_country == "US" && $partner->country == "US")
					{
					
						if($user_state == $partner->state)
						{
						
							$partner_id = $partner->id;
						}
					}
				}
				
			}
			
		   $saved_advertisement = $this->statics_model->getAdvertisement($partner_id)->row();
			
			if($saved_advertisement->accepted == 1)
			{
				 $advertisement = $saved_advertisement;
			}

		    if($template_name == 'frontend/home' && $this->ion_auth->logged_in())
		    {
				$this->statics_model->updateImpressions($partner_id);
		    }
		}		
		return $advertisement;
    }
    
    
   public function checkGeoIP()
   {
	   $country_code = false;
		
		$this->load->library('geoip_lib');
		$user_ip = $this->input->ip_address();
		
		
		if($this->geoip_lib->InfoIP($user_ip)) {
	
	    	$country_code = $this->geoip_lib->result_country_code();
	    }
	    
	    return $country_code;
   }
    
	public function loadDefaultView($template_name, $vars = array(), $return = FALSE)
    {
    	$this->checkIPaddress();
    
    	
		  	$vars['country_ip'] = "US";	
		
			//if(!$this->session->userdata('country_ip')) {
				$this->load->library('geoip_lib');
				$user_ip = $this->input->ip_address();
				
				
			/*	if($this->geoip_lib->InfoIP($user_ip)) {
		 
		        	$vars['country_ip'] = $this->geoip_lib->result_country_code();
		        	
		        	if($vars['country_ip'] == 'AT' || $vars['country_ip'] == 'DE')
		        	{
			            if ($this->session->userdata('language') === FALSE ) {
							$this->session->set_userdata('language', 'de');
						}
		            }
		            
		            $this->session->set_userdata('country_ip', $vars['country_ip']);
	
		        }
		
    	*/

		if($this->ion_auth->logged_in()){
			$this->load->model('statics_model');
			$this->load->model('car_model');
			$vars['user_id'] = $this->session->userdata('user_id');
			
			$userdata = $this->statics_model->getUserData($vars['user_id'])->row();
			$vars['user_first_name'] = $userdata->first_name;
			$vars['user_last_name'] = $userdata->last_name;
			$vars['user_profile_picture'] = $userdata->profile_picture;
			$vars['user_hashtags'] = $userdata->show_hashtags;
			
			$activity_ids = array();
			
			
					
		
			$vars['car_years'] = array();
			for($i = date("Y"); $i>= 1936; $i--)
			{
				
				$vars['car_years'][] = $i;
				
			}
		
			
			$vars['pending'] = count($activity_ids);
			
			$vars['whatsnew'] = array_filter($this->getNewest());
			$vars['random_vehicles_news'] = array_filter($this->getRandomVehiclesForNewsFeed());
			
			$vars['home_friends'] = array_filter($this->getFriends());
			$vars['hashtags'] = array_filter($this->hashtags());
			$vars['countHashtags'] = count($vars['hashtags']);
			
			$vehicles = $this->car_model->getVehicles($this->session->userdata('user_id'));
		$friends_container = array();
		foreach($vehicles->result() as $vehicle){
			$friends_container[] = $vehicle->id;
		}
		
		
		$event_ids = array();
		$events = $this->statics_model->getAllNotifications()->result();
		
		$notification_counter = 0;
		
		if(!empty($friends_container)){
		
			foreach($events as $event){		
	
				$vehicle = $this->car_model->getVehicle($event->vehicle_id)->row();
				
				if(in_array($event->vehicle_id, $friends_container))
				{
					
						
					if(!in_array($event->id, $event_ids)){
						
						
						if($event->has_seen == 0 && $event->action_id != 1)
						{
							
							$notification_counter++;
							$event_ids[] = $event->id;
						}
	
						
					}
					
					
				}
			}
		}
		
		
		$vars['notification_counter'] = $notification_counter;
			
		$vars['selected_language'] = 'en';
			
		}
		else
		{
			$vars['user_id'] = 0;  	
			$vars['pending'] = 0;
			
		}
		
	   //  }
		
		if($template_name == 'dog/dog_detail' && !$this->ion_auth->logged_in())
		{
		
			$content  = $this->load->view('frontend/header', $vars, $return);
			$content .= $this->load->view('frontend/sidemenu', $vars, $return);
	        $content .= $this->load->view('dog/dog_detail_nologin', $vars, $return);
			$content .= $this->load->view('frontend/footer', $vars, $return);
			
		}
		else if(!$this->ion_auth->logged_in())
		{
			$content  = $this->load->view('frontend/header', $vars, $return);
	
	        $content .= $this->load->view($template_name, $vars, $return);
	        	
			$content .= $this->load->view('frontend/footer', $vars, $return);
		}
		else
		{
			
			
			$vehicleData = $this->statics_model->getVehicleData($this->session->userdata('vehicle_id'))->row();
			$vars['vehicle_nickname'] = $vehicleData->nickname;
			$vars['vehicle_profile'] = $this->statics_model->getImageByID($vehicleData->profile_image)->row()->fname;
			
		
			
			$content  = $this->load->view('frontend/header_loggedin', $vars, $return);
			
				
	        $content .= $this->load->view($template_name, $vars, $return);
	        	
			$content .= $this->load->view('frontend/footer', $vars, $return);
			
		}
        

        if ($return)
        {
            return $content;
        }
    }
	
	public function checkLogin()
	{
		if (!$this->ion_auth->logged_in())
			redirect('#login','location');
	}
	
	public function checkPartnerLogin($user = NULL , $pw = NULL)
	{
		$email = $this->purify($user);
		$partner =  $this->auth_model->getPartner($email)->row();
		if($partner != false)
		{
			$pwcheck = $this->check_hash($pw, $partner->password);
			if($pwcheck)
			{
				$this->session->set_userdata('partner_id', $partner->id);
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		
		
			
		
	}
	
	
	public function checkVetLogin($user = NULL , $pw = NULL)
	{
	
		$email = $this->purify($user);
		$partner =  $this->auth_model->getVet($email);
		
		
		if($partner != false)
		{
		
			$pwcheck = $this->check_hash($pw, $partner->row()->login_pw);
			if($pwcheck)
			{
				
				$this->session->set_userdata('vet_id', $partner->row()->id);
				return true;
			}
			else
			{
				
				return false;
			}
		}
		else
		{
		
			return false;
		}
		
		
			
		
	}
	
	
	function purify($dirty_html)
	{
		
		require_once(APPPATH.'libraries/htmlPurifier/HTMLPurifier.standalone.php');
    
    	$purifier = new HTMLPurifier();
    	$clean_html = $purifier->purify($dirty_html);
		
		return $clean_html;
	}
	
	public function checkAdmin()
	{
		if(!$this->ion_auth->is_admin())
			redirect('home','location');
	}
	
	function rand_string( $length ) 
	{
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";	
	$str = "";

	$str = base64_encode(openssl_random_pseudo_bytes($length, $strong));
	$str = substr($str, 0, $length);
	$str = preg_replace("/[^a-zA-Z0-9\s]/", "", $str);
	return $str;
	}
	
		
	 public function send_mail_report($type, $user_id, $entity_id) {
	 	$this->load->library('My_phpmailer');
        
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server 
        $mail->SMTPDebug = 1;  // prefix for secure protocol to connect to the server 
        $mail->Host       = "smtp.easyname.eu";      // setting SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to 
        $mail->Username   = "1590mail27";  // user email address
        $mail->Password   = "Kksnoopstr";            // password 
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SetFrom('noreply@performancenation.com', 'Performancenation');  //Who is sending the email
        $mail->AddReplyTo("noreply@performancenation.com", "Performancenation");  //email address that receives the response
        $mail->CharSet = 'UTF-8';
       
		$data['user'] = $this->statics_model->getUserData($user_id)->row();
		
		
       
        switch($type)
        {
	        case "image":{
	        		$data['image_id'] = $entity_id;
	        		$image = $this->statics_model->getImageByID($entity_id)->row();
	        		
	        		$src = site_url()."items/uploads/";
	        		$folder = "images";
	        		
	        		if($image->profile_image == 1)
	        		{
		        		$folder = "profilepictures";
	        		}
	        		else if($image->cover_image == 1)
	        		{
		        		$folder = "coverpictures";
	        		}
	        		$data['image_src'] = $src.$folder."/".$image->fname;						
					$body = $this->load->view('mail/report_image_mail', $data, true);
					
			        $mail->Subject    = "Performancenation reported image";
					$mail->Body      =  $body;
					$mail->IsHTML(true);
				};break;
	        case "profile":{
	       		 	$data['profile_id'] = $entity_id;
	        		//$dog = $this->car_model->getDog($entity_id);
	        		
	        		$data['profile_src'] = site_url()."profile/".$dog->pretty_url;
		       		$body = $this->load->view('mail/report_profile_mail', $data, true);
					
			        $mail->Subject    = "Performancenation reported profile";
					$mail->Body      =  $body;
					$mail->IsHTML(true);
	        	};break;
        }
        $email = "head@istvanszilagyi.com";
        $recipient = "Image Overwatch";
		
        $mail->AddAddress($email, $recipient);
		//$mail->Send();
        if(!$mail->Send()) {
            $data["message"] = "Error: " . $mail->ErrorInfo;
        } else {
            $data["message"] = "Message sent correctly!";
        }
       return $data['message'];
    }
	
	
	 public function send_mail($type, $email, $recipient, $pass, $promo = NULL, $mailname = NULL) {
	 	$this->load->library('My_phpmailer');
        
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server 
        $mail->SMTPDebug = 1;  // prefix for secure protocol to connect to the server 
        $mail->Host       = "smtp.easyname.eu";      // setting SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to 
        $mail->Username   = "1590mail27";  // user email address
        $mail->Password   = "Kksnoopstr";            // password 
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SetFrom('noreply@performancenation.com', 'Performance Nation');  //Who is sending the email
        $mail->AddReplyTo("noreply@performancenation.com","Performance Nation");  //email address that receives the response
        $mail->CharSet = 'UTF-8';
        switch($type)
        {
	    	case "registration":{
	        		
					$data['mailname'] = $mailname;
					$body = $this->load->view('mail/welcome_mail', $data, true);
			        $mail->Subject    = "Performance Nation registration";
					$mail->Body      =  $body;
					
				};break;
					
			case "no_code":{
		       			
					$data['mailname'] = $mailname;
					$body = $this->load->view('mail/no_code_mail', $data, true);
			        $mail->Subject    = "Performance Nation registration";
					$mail->Body      =  $body;
						
	        	};break;
        }
        
        $mail->IsHTML(true);
        $mail->AddAddress($email, $recipient);
		//$mail->Send();
        if(!$mail->Send()) {
            $data["message"] = "Error: " . $mail->ErrorInfo;
        } else {
            $data["message"] = "Message sent correctly!";
        }
       return $data['message'];
    }
    
    public function send_mail_booking($type, $email, $recipient, $date) {
	 	$this->load->library('My_phpmailer');
        
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server 
        $mail->SMTPDebug = 1;  // prefix for secure protocol to connect to the server 
        $mail->Host       = "smtp.easyname.eu";      // setting SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to 
        $mail->Username   = "1590mail27";  // user email address
        $mail->Password   = "Kksnoopstr";            // password 
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SetFrom('noreply@performancenation.com', 'performancenation');  //Who is sending the email
        $mail->AddReplyTo("noreply@performancenation.com","performancenation");  //email address that receives the response
        
        switch($type)
        {
	        case "accept":{
			        $mail->Subject    = "performancenation booking";
			        $mail->Body      = "<img src='".site_url('items/frontend/img/snoopstr_logo.jpg')."'/><br/>";
					$mail->Body      .= "<div style='text-align:center;'>You have successfully registered on performancenation</div>";
					$mail->AltBody    = "Plain text message";
				};break;
	        case "decline":{
		       		$mail->Subject    = "performancenation partner registration";
					$mail->Body      = "<p>Hello ".$recipient."!</p><br><p>You have been registered as a partner on performancenation</p><p>Your password is:</p><br/><p>You can use the following promo code: </p>";
					$mail->AltBody    = "Hello ".$recipient."! You have been registered as a partner on performancenation. Your password is: . You can use the following promo code: ";
	        	};break;
        }
        
       
        $mail->AddAddress($email, $recipient);
		//$mail->Send();
        if(!$mail->Send()) {
           // $data["message"] = "Error: " . $mail->ErrorInfo;
        } else {
           // $data["message"] = "Message sent correctly!";
        }
      // return $data['message'];
    }
    
    
    
    public function import_instagram($user_id)
    {
	    
	    $userdata = $this->statics_model->getUserData($user_id)->row();
	    $album_id = $this->statics_model->getUserInstagramAlbum($user_id)->row()->id;
	    
	    if($album_id == NULL)
	    {
	    	$title = "Instagram photos";
	    	$about = "";
		    $album_id = $this->statics_model->addInstagramAlbum($title,$about,$user_id);
	    }
	    
	    
	    
	    $after = $this->statics_model->getUserInstagramAlbumLastImage($album_id)->row()->created_date;
		
		if($after == NULL)
		{
			$after = "2000-01-01 08:11:36";
		}
		
		
		$tags = array($userdata->user_hashtag);
		$since = strtotime($after);
		
		
		$instagram_client_id = "e073a87edb2c4491ab3d4fb745f28bcb";
			
		 $settings = array(
		    'oauth_access_token' => "2524565712-UZn7Gi56cAhOrz4Z8TcEsc7GmoEngm8tPInwV9I",
		    'oauth_access_token_secret' => "uOZLvMgwolCMOW1iypt9stim3lglWVPwlbOVtqsmTJVA0",
		    'consumer_key' => "NrzOE3JqIXg6wmGlbbuxhjIUm",
		    'consumer_secret' => "6CKWWnm47D6wa6evgp2xElQdTclo7Bhw4ZplMwHUnOLgU0fn4O"
		);
		
		foreach($tags as $tag){
			//INSTAGRAM
			$url = 'https://api.instagram.com/v1/tags/'.$tag.'/media/recent?since='.$since.'&client_id='.$instagram_client_id;
		    $jsonData = json_decode((file_get_contents($url)));
			
		    foreach ($jsonData->data as $key=>$value) {	 
		    	   	
		    	$saved_tags = $this->removeEmoji($value->caption->text);
		    	$user_name = $value->caption->from->username;
		    	$created_time = $value->caption->created_time;
		    	$created_time = date("Y-m-d H:i:s", $created_time);
		    	//date_default_timezone_set('Europe/Vienna');
				$t=$value->created_time;
				$date =  date("Y-m-d H:i:s", $t);
							
				if($t > $since){
					$fname = substr($value->images->standard_resolution->url, strrpos($value->images->standard_resolution->url, '/') + 1);
					
					$copy_path = './items/uploads/images/'.$fname;
					
					
					
					copy($value->images->standard_resolution->url, $copy_path);
					
					$this->statics_model->addPictureInstagram($fname,$album_id,$user_id, $saved_tags);
					//echo $value->images->standard_resolution->url." ".$value->link." ".$date." ".$saved_tags." ".$user_name." ".$created_time;
						    	
		    	}
		    }
		
		    
		}
		
    }
    
	function distance($lat1, $lon1, $lat2, $lon2, $unit) {

	  $theta = $lon1 - $lon2;
	
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	
	  $dist = acos($dist);
	
	  $dist = rad2deg($dist);
	
	  $miles = $dist * 60 * 1.1515;
	
	  $unit = strtoupper($unit);
	
	 
	
	  if ($unit == "K") {
	
	    return ($miles * 1.609344);
	
	  } else if ($unit == "N") {
	
	      return ($miles * 0.8684);
	
	    } else {
	
	        return $miles;
	
	      }
	
	}
	
	
    
   
	function toAscii($str, $replace=array(), $delimiter='-') {
	 setlocale(LC_ALL, 'en_US.UTF8');
	 if( !empty($replace) ) {
	  $str = str_replace((array)$replace, ' ', $str);
	 }
	
	 $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	 $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	 $clean = strtolower(trim($clean, '-'));
	 $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
	
	 return $clean;
	}
}
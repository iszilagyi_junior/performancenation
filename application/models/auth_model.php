<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Auth_model extends CI_Model
{
	function getProfile($id)
	{
		$sql = "SELECT * FROM users WHERE id = ?";
		$resultset = $this->db->query($sql, array($id));
		return $resultset->row();
	}
	
	function getPW($email)
	{
		$this->db->where('email', $email);
		$this->db->select('password');
		return $this->db->get('users');
	}
	
	function getCountries()
	{
		$sql = "SELECT * FROM country ORDER BY name ASC";
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function getStates()
	{
		$sql = "SELECT * FROM state ORDER BY name ASC";
		$resultset = $this->db->query($sql);
		return $resultset;
	}
	
	function checkLoginUserFB($email){
	    $sql = "SELECT *  FROM  users  WHERE  `email` = ?";
	    $result = $this->db->query($sql, array($email));
		if ($result->num_rows() > 0) {return $result->row();}else {return false;}
    }
	
	function checkLoginUserFBID($fbId){
	    $sql = "SELECT *  FROM  users  WHERE  `fb_id` = ?";
	    $result = $this->db->query($sql, array($fbId));
		if ($result->num_rows() > 0) {return $result->row();}else {return false;}
    }
	
	
	function checkPromoCode($promo){
	    $sql = "SELECT *  FROM  partners  WHERE  `partner_promo_id` = ?";
	    $result = $this->db->query($sql, array($promo));
		if ($result->num_rows() > 0) {return $result;}else {return false;}
    }
	
	function getPromoRate(){
	    $sql = "SELECT *  FROM  promo_code_rate  WHERE  `id` = 1";
	    $result = $this->db->query($sql);
		return $result;
    }
	
	function savePromoRegistration($data){		
		$this->db->insert('promo_registrations', $data);
		return $this->db->insert_id();
	}
	
	
	function updateImpressions($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('partners', $data);
	}
	
	function updateVet($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('vets', $data);
	}
	
	
	function checkMobilePW($email){
	    $sql = "SELECT *  FROM  users  WHERE  `email` = ?";
	    $result = $this->db->query($sql, array($email));
		if ($result->num_rows() > 0) {return $result;}else {return false;}
    }
	
	function checkUnderDelete($user_id){
	    $sql = "SELECT *  FROM  delete_requests  WHERE  `user_id` = ?";
	    $result = $this->db->query($sql, array($user_id));
		if ($result->num_rows() > 0) {return $result;}else {return false;}
    }
    
	function getRequestData($request_id){
	    $sql = "SELECT *  FROM  delete_requests  WHERE  `id` = ?";
	    $result = $this->db->query($sql, array($request_id));
		return $result;
    }
	
	function updateProfilePic($email, $data)
	{
		$this->db->where('email', $email);
		$this->db->update('users', $data);
	}
	
	function getIPs(){
	    $sql = "SELECT *  FROM  ip_addresses ";
	    $result = $this->db->query($sql);
		if ($result->num_rows() > 0) {return $result;}else {return false;}
    }

	function saveSession($data){		
		$this->db->insert('sessions', $data);
		return $this->db->insert_id();
	}	
	
	function checkSession($session_id)
	{
		$sql = 'SELECT * FROM sessions WHERE session_id = ?';
		$resultset = $this->db->query($sql, array($session_id));
		
		if ($resultset->num_rows() > 0) {return $resultset->row();}else {return "NO";}
	}
	
	function sessionData($session_id)
	{
		$sql = 'SELECT * FROM sessions WHERE session_id = ?';
		$resultset = $this->db->query($sql, array($session_id));
		
		if ($resultset->num_rows() > 0) {return $resultset->row();}else {return false;}
	}
	
	function checkSessionUser($session_id, $user_id)
	{
		$sql = 'SELECT * FROM sessions WHERE session_id = ? AND user_id = ?';
		$resultset = $this->db->query($sql, array($session_id, $user_id));
		if ($resultset->num_rows() > 0) {return $resultset->row();}else {return "NO";}
	}
	
	function updateSession($session_id)
	{
		$this->db->where('session_id', $session_id);
		$this->db->update('sessions', array('session_id' => $session_id));
	}
	
	function updateSessionVehicle($session_id, $vehicle_id)
	{
		$this->db->where('session_id', $session_id);
		$this->db->update('sessions', array('vehicle_id' => $vehicle_id));
	}
	
	function checkFbId($fbId){
	    $sql = "SELECT *  FROM  users  WHERE  `fb_id` = ?";
	    $result = $this->db->query($sql, array($fbId));
		if ($result->num_rows() > 0) {return $result;}else {return false;}
    }
	
	function checkFbToEmail($fbId, $email){
	    $sql = "SELECT *  FROM  users  WHERE  `fb_id` = ? AND `email` = ?";
	    $result = $this->db->query($sql, array($fbId, $email));
		if ($result->num_rows() > 0) {return true;}else {return false;}
    }
    
    function getPartner($email){
	    $sql = "SELECT *  FROM  partners  WHERE  `email` = ? ";
	    $result = $this->db->query($sql, array($email));
		if ($result->num_rows() > 0) {return $result;}else {return false;}
    }
    
    function getVet($email){
	    $sql = "SELECT *  FROM  vets  WHERE  `login_email` = ? ";
	    $result = $this->db->query($sql, array($email));
		if ($result->num_rows() > 0) {return $result;}else {return false;}
    }
    
    function getVetbyID($id){
	    $sql = "SELECT *  FROM  vets  WHERE  `id` = ? ";
	    $result = $this->db->query($sql, array($id));
		if ($result->num_rows() > 0) {return $result;}else {return false;}
    }
    
    function deleteUserProfile($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('users');
	}
	
	
	function activateUserProfile($id, $promo)
	{
		$this->db->where('id', $id);
		$this->db->update('users', array('active' => 1, 'promo_code' => $promo));
	}
	
	function deleteRequest($id)
	{
		$this->db->where('user_id', $id);
		$this->db->delete('delete_requests');
	}
	
	
	
	function checkRegistrationCode($code)
	{
		$this->db->where('registration_string', $code);
		$result = $this->db->get('registration_codes');
		if ($result->num_rows() > 0) {return $result->row(); }else {return false;}
	}
	
	function updateRegistrationCode($code)
	{
		$this->db->set('counter', 'counter+1', FALSE);
		$this->db->where('registration_string', $code);
		$this->db->update('registration_codes');
	}
}

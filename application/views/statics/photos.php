<script>
	$('#sidemenu').css({top:35});
</script>
<div id="content">
	<div style="position:relative;top:35px;">
		<div class="statics_subheader"><?= $this->lang->line('albums')?></div>
	</div>
	<div id="album_holder" style="top:0px;margin-top:35px;">
		<?if(isset($albums)){
			$x = 1;
			foreach($albums as $album){?>
				<a href="<?= site_url('album/'.$album['id']);?>">
					<div class="album_item_outer" <? if($x%3 == 0) echo 'style="margin-right:0px;"'?>>	
						<div class="album_item">
							<?if($album['pictures']){ 				
								foreach($album['pictures'] as $i=>$pic){
									if($i==0){
										if($pic['fname']!=""){
							?>
											<img class="album_item_image" src="<?= site_url('items/uploads/images/'.$pic['fname']);?>"/>
							<? 			
										}
									}
								}
							}else{?>
								<img class="album_item_image" src="<?= site_url('items/uploads/images/default_album.png')?>"/>
							<? }?>
							
							<div class="album_item_title"><?= $album['title'];?></div>
							<div class="album_item_about"><?= $album['about'];?></div>
						</div>
					</div>
				</a>
		<? $x++;
		}
			}?>
	</div>	
	<br>
	
</div>
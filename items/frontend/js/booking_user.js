$(document).ready(function() 
{
	
	addSitterControlListeners();
	addSitterCalendarListeners();
	addUserControlListeners();
	
});

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function addClickListeners()
{
	$('.overlay_close').unbind("click");
	$('.overlay_close').click(function()
	{
		$(this).parent().fadeOut();
	});
	
	
	$('.popup_select_empty').click(function(){
		$this = $(this);
		
		$('.popup_select_full').hide();
		$('.popup_select_empty').show();
		$this.hide();
		$this.parent().find('.popup_select_full').show();
		
		if($this.attr('fe') == 1)
		{
			$('#fe1' ).prop('checked',true);
		}
		else if($this.attr('fe') == 2)
		{
			$('#fe2' ).prop('checked',true);
		}
	});
}



function addSitterCalendarListeners()
{

	$('.slot_available').click(function()
	{
		showAvailOverlay($(this));
	});
	
/*	$('.slot_available').click(function()
	{
		showAvailEditOverlay($(this));
	});*/
	
	$('.slot_exception').click(function()
	{
		showExceptionOverlay($(this));
	});

}
function addSitterControlListeners()
{
	$('.accept_request').click(function()
	{
		acceptSitterRequest($(this));
	});	
	
	$('.decline_request').click(function()
	{
		declineSitterRequest($(this));
	});		
}
function addUserControlListeners()
{
	$('#sittersearch_form').submit(function(event)
	{
		event.preventDefault();
		submitSitterSearch();
	});
}

function showAvailOverlay(slot)
{
	// check if allowed to edit
	if(edit_unoccupied)
	{
		$('.calendar_overlay').remove();
		
		var left = slot.position().left;
		var top = slot.position().top;
		var width = slot.width();
		var height = slot.height();

		$.ajax(
		{
			url: rootUrl + 'booking/avail_overlay_user',
			data: {date: slot.attr('d')},
			method: 'GET',
			success: function(data)
			{
				ret = $.parseJSON(data);
				if(ret.success)
				{
					if($('.avail_overlay').length > 0)
						$('.avail_overlay').replaceWith(ret.data);
					else
						$('#calendar').append(ret.data);
						
					$('.avail_overlay').css({'left': left-(128)+'px', 'top': top + height/2 + 50 + 'px'});
					
					addClickListeners();
					
					// submit handler
					$('#avail_overlay_form').submit(function(event)
					{
						event.preventDefault();
						var sitter = $("#calendar_holder").attr("sid");
						submitAvailForm(sitter);
					});						
				}
			}
		});			
	}	
}
function submitAvailForm(sitter_id)
{
	$.ajax(
	{
		url: rootUrl + 'booking/book_sitter/'+sitter_id,
		data: $('#avail_overlay_form').serializeObject(),
		method: 'POST',
		success: function(data)
		{
			ret = $.parseJSON(data);
			if(ret.success)
			{
				$('.calendar_overlay').remove();
				var loadurl = $('.nextbutton').attr('load_url');
				
				reloadCalendar($('#calendar').attr('startdate'), $('#calendar').attr('calendar_style'), 2, $('.nextbutton').attr('load_url'), $('.nextbutton').attr('reload_functions'));
			}
		}
	});			
}


function showAvailEditOverlay(slot)
{
	$('.calendar_overlay').remove();
	
	var left = slot.position().left;
	var top = slot.position().top;
	var width = slot.width();
	var height = slot.height();

	$.ajax(
	{
		url: rootUrl + 'booking/availedit_overlay',
		data: {avail_id: slot.attr('avail_id'), date: slot.attr('d')},
		method: 'GET',
		success: function(data)
		{
			ret = $.parseJSON(data);
			if(ret.success)
			{
				if($('.availedit_overlay').length > 0)
					$('.availedit_overlay').replaceWith(ret.data);
				else
					$('#calendar').append(ret.data);
					
				$('.availedit_overlay').css({'left': left-(128)+'px', 'top': top + height/2 + 50 + 'px'});
				
				addClickListeners();
				
				// submit handler
				$('#availedit_overlay_form').submit(function(event)
				{
					event.preventDefault();
					submitAvailEditForm();
				});
				
				$('#availedit_overlay_clear').click(function()
				{
					deleteAvailEdit();
				});		
				
				$('#availedit_overlay_exception').click(function()
				{
					addException();
				});		
										
			}
		}
	});			
}
function submitAvailEditForm()
{
	$.ajax(
	{
		url: rootUrl + 'booking/edit_avail',
		data: $('#availedit_overlay_form').serializeObject(),
		method: 'POST',
		success: function(data)
		{
			ret = $.parseJSON(data);
			if(ret.success)
			{
				$('.calendar_overlay').remove();
				var loadurl = $('.nextbutton').attr('load_url');
				
				reloadCalendar($('#calendar').attr('startdate'), $('#calendar').attr('calendar_style'), 2, $('.nextbutton').attr('load_url'), $('.nextbutton').attr('reload_functions'));
			}
		}
	});			
}
function deleteAvailEdit()
{
	$.ajax(
	{
		url: rootUrl + 'booking/delete_avail',
		data: {avail_id: $('#availedit_overlay_avail_id').val()},
		method: 'POST',
		success: function(data)
		{
			ret = $.parseJSON(data);
			if(ret.success)
			{
				$('.calendar_overlay').remove();
				var loadurl = $('.nextbutton').attr('load_url');
				
				reloadCalendar($('#calendar').attr('startdate'), $('#calendar').attr('calendar_style'), 2, $('.nextbutton').attr('load_url'), $('.nextbutton').attr('reload_functions'));
			}
		}
	});				
}
function addException()
{
	$.ajax(
	{
		url: rootUrl + 'booking/add_exception',
		data: $('#availedit_overlay_form').serializeObject(),
		method: 'POST',
		success: function(data)
		{
			ret = $.parseJSON(data);
			if(ret.success)
			{
				$('.calendar_overlay').remove();
				var loadurl = $('.nextbutton').attr('load_url');
				
				reloadCalendar($('#calendar').attr('startdate'), $('#calendar').attr('calendar_style'), 2, $('.nextbutton').attr('load_url'), $('.nextbutton').attr('reload_functions'));
			}
		}
	});	
}


function showExceptionOverlay(slot)
{
	$('.calendar_overlay').remove();
	
	var left = slot.position().left;
	var top = slot.position().top;
	var width = slot.width();
	var height = slot.height();

	$.ajax(
	{
		url: rootUrl + 'booking/exception_overlay',
		data: {exception_id: slot.attr('excep_id'), date: slot.attr('d')},
		method: 'GET',
		success: function(data)
		{
			ret = $.parseJSON(data);
			if(ret.success)
			{
				if($('.availedit_overlay').length > 0)
					$('.availedit_overlay').replaceWith(ret.data);
				else
					$('#calendar').append(ret.data);
					
				$('.exception_overlay').css({'left': left-(128)+'px', 'top': top + height/2 + 50 + 'px'});
				
				addClickListeners();
				
				// submit handler
				$('#exception_overlay_form').submit(function(event)
				{
					event.preventDefault();
					submitExceptionForm();
				});
				
				$('#exception_overlay_clear').click(function()
				{
					deleteException();
				});
			}
		}
	});			
}
function submitExceptionForm()
{
	$.ajax(
	{
		url: rootUrl + 'booking/edit_exception',
		data: $('#exception_overlay_form').serializeObject(),
		method: 'POST',
		success: function(data)
		{
			ret = $.parseJSON(data);
			if(ret.success)
			{
				$('.calendar_overlay').remove();
			}
		}
	});			
}
function deleteException()
{
	$.ajax(
	{
		url: rootUrl + 'booking/delete_exception',
		data: {exception_id: $('#exception_overlay_exception_id').val()},
		method: 'POST',
		success: function(data)
		{
			ret = $.parseJSON(data);
			if(ret.success)
			{
				$('.calendar_overlay').remove();
				var loadurl = $('.nextbutton').attr('load_url');
				
				reloadCalendar($('#calendar').attr('startdate'), $('#calendar').attr('calendar_style'), 2, $('.nextbutton').attr('load_url'), $('.nextbutton').attr('reload_functions'));
			}
		}
	});			
}


function acceptSitterRequest(request)
{
	$.ajax(
	{
		url: rootUrl + 'booking/accept_request',
		data: {request_id: request.attr('request_id')},
		method: 'POST',
		success: function(data)
		{
			ret = $.parseJSON(data);
			if(ret.success)
			{
				$('.sitter_controls').replaceWith(ret.data);
				
				var loadurl = $('.nextbutton').attr('load_url');
				
				reloadCalendar($('#calendar').attr('startdate'), $('#calendar').attr('calendar_style'), 2, $('.nextbutton').attr('load_url'), $('.nextbutton').attr('reload_functions'));
			}
		}
	});		
}

function declineSitterRequest(request)
{
	$.ajax(
	{
		url: rootUrl + 'booking/decline_request',
		data: {request_id: request.attr('request_id')},
		method: 'POST',
		success: function(data)
		{
			ret = $.parseJSON(data);
			if(ret.success)
			{
				$('.sitter_controls').replaceWith(ret.data);
				
				var loadurl = $('.nextbutton').attr('load_url');
				
				reloadCalendar($('#calendar').attr('startdate'), $('#calendar').attr('calendar_style'), 2, $('.nextbutton').attr('load_url'), $('.nextbutton').attr('reload_functions'));
			}
		}
	});		
}

function submitSitterSearch()
{
	$.ajax(
	{
		url: rootUrl + 'booking/sittersearch',
		data: $('#sittersearch_form').serializeObject(),
		method: 'GET',
		success: function(data)
		{
			ret = $.parseJSON(data);
			if(ret.success)
			{
				$('#sitter_search_results').fadeOut(100, function()
				{
					$('#sitter_search_results').empty();
					$('#sitter_search_results').html(ret.data);
					$('#sitter_search_results').fadeIn(100);					
				});

			}
		}
	});		
}

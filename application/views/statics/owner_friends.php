<script>	
	$('#sidemenu').css({top: 0});
</script>
<div id="content">
	<div class="statics_subheader" style="margin-top:35px;">
		<?= $this->lang->line('dog_friends_of')." ".$dog->name;?>
	</div>

	<?php
	if(empty($friends)){?>
		<div class="sansitalic" style="width:100%;text-align:center;,argin-top:10px;"><?= $this->lang->line('dog_no_friends');?></div>
	<?}
	else{ ?>
	
			<div style="overflow:hidden;margin-bottom:20px;">
				<input type="text" id="friend_search_input" name="friend_search_input" placeholder="<?= $this->lang->line('dog_new_name')?>" defaultText="<?= $this->lang->line('dog_new_name')?>" class="search_input">
				<div id="friend_search_button" style="margin-top:22px;"><?= $this->lang->line('profile_search_submit')?></div> 
			</div>	
			<div style="overflow:hidden;">		
	<?	 $x = 1; foreach($friends as $dog): 
			if(is_numeric($dog->profile_picture))
			{
				$dogprofile = $this->statics_model->getImageByID($dog->profile_picture)->row()->fname;
			}
			else
			{
				$dogprofile = $dog->profile_picture;
			}
		
		
	?>
	 	
			
			<a href="<?= site_url('profile/'.$dog->pretty_url);?>">
				<div class="my_friends_item" id="<?= $dog->id;?>" <?if($x%3==0)echo "style='margin-right:0px;'"?> dog_name="<?= $dog->name?>">
					<div class="div_white_over"></div>
					<img class="friends_list_item_image" src="<?= site_url('items/uploads/profilepictures/' . $dogprofile)?>" border="0">
					<div class="friends_list_item_desc" style="top:55px;"><?= $dog->name;?></div>			
					
										
				</div>
			</a>
							

		
	<?php $x++; endforeach; 
		
		}
	?>
	</div>

</div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<style>
	   	@import url(http://fonts.googleapis.com/css?family=Open+Sans:700,400italic&subset=latin,latin-ext);
	   /* All your usual CSS here */
	  
	  tr, td, table
	  {
	  	margin:0px;
	  	padding:0px;
	  	border-spacing:0;
	  	display:flex;
	  }
	  img
	  {
	  	border:0;
	  }
	</style>
	
</head>
<body style="background:#ffffff;">
	<table style="width:400px; margin:0px auto;border-spacing:0;font-family: 'Open Sans', sans-serif;background:#e7e7e7;">
		<tr>
			<td>
				<?= $user->first_name." ".$user->last_name."(".$user->id.") reported the following image (image_id: ".$image_id.")"?>
			</td>
		</tr>
		<tr>
			<td>
				<img style="width:400px;" src="<?= $image_src;?>"/>
			</td>
		</tr>
		
	</table>
</body>
</html>



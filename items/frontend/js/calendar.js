$(document).ready(function()
{
	addCalendarListeners();	
});

function addCalendarListeners()
{
	$('.prevbutton, .nextbutton').click(function()
	{
		var startdate = $('#calendar').attr('startdate');
		var style = $('#calendar').attr('calendar_style');
		var change = null;
		if($(this).hasClass('nextbutton'))
			change = 0;
		if($(this).hasClass('prevbutton'))
			change = 1;
			
		var reload_functions = $(this).attr('reload_functions');
			
		reloadCalendar(startdate, style, change, $(this).attr('load_url'), reload_functions);
	});
}


function reloadCalendar(date, style, change, loadurl, reload_functions)
{
	$.ajax
	({
		url: loadurl,
		data: {startdate: date, style: style, change: change},
		type: 'GET',
		success: function(data)
		{
			ret = $.parseJSON(data);
			if(ret.success)
			{
				$('#calendar').replaceWith(ret.data);
				addCalendarListeners();
				window[reload_functions]();
			}
		}
	});	
}

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
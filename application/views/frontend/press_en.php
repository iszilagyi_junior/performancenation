<script>
	$(document).ready(function()
	{	
		$("#welcomeImg, #welcomeImgHolder").show();
		
	});
</script>
<div id="content">
    <div id="WelcomeHolder">
		<div id="BkgEr" style="display:none;"></div>
		<div id="welcomeText">Press</div>
	</div>

	<div class="home_italic">	
			Download our key images, logos and german and english press release here:
	</div>
	<div style="width:400px;text-align:center;margin:10px auto;">
		<a target="_blank" href="<?= site_url('items/uploads/press/PresseAussendung.pdf')?>">Press release in german, (PDF 1 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/PresseAussendung_EN.pdf')?>">Press release in english, (PDF 1 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/snoopstr_logo.eps')?>">Logo, (EPS 1 MB)</a><br/>
		<br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC00492_branded.jpg')?>">Testimonial Image Henry, HD, branded (15 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC00492.jpg')?>">Testimonial Image Henry, HD, unbranded (15 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC00497_branded.jpg')?>">Testimonial Image Boy, HD, branded (15 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC00497.jpg')?>">Testimonial Image Boy, HD, unbranded (15 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/iMacScreenshot_de.png')?>">Screenshot in press quality from our German user interface (1 MB)</a><br/>
		
		<a target="_blank" href="<?= site_url('items/uploads/press/Portrait_Istvan.jpg')?>">Founder of snoopstr (12 MB)</a><br/>
		<br/>
		Images of founder and his dogs:<br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC02444.jpg')?>">Image 1 (12 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC02457.jpg')?>">Image 2 (12 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC02479.jpg')?>">Image 3 (13 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC02493.jpg')?>">Image 4 (14 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC02504.jpg')?>">Image 5 (13 MB)</a><br/>
		<a target="_blank" href="<?= site_url('items/uploads/press/DSC02548.jpg')?>">Image 6 (14 MB)</a><br/>
	</div>
</div>

	
</div>
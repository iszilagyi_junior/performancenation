<div id="content" style="width:100%;left:0px;top:260px;background:#363636;margin-bottom:100px;">
		<div class="bold" style="font-size:24px;color:#ffffff;">Terms & Conditions</div>
		<br/>
		<div style="color:#ffffff;padding:0px 20px;text-align:left;">
			<div class="bold">End-User License Agreement (EULA)</div>
				<div class="terms_text">
		            This Statement of Rights and Responsibilities ("Statement", "Terms") defines the basis of usage of our site performancenation.com and the guidelines for usage. By using our site services you agree with above terms. A registration is not necessary to agree with these terms.<br />
		            <br />
		            <ol>
		                <li>
		                    <b>Privacy</b><br />
		                    You acknowledge that we do not take responsibility of your data. Please do not store any crucial information on our site, that should be kept private. We use latest encoding services to protect your profile, but are not handling your data in a sensitive manner, as derived from the type of content this site should hold. (car related informations)
		                </li><br />
		                <li>
		                    <b>Registration</b><br />
		                    You can register to access features on our site. Your registration data and e-mail address will NOT be given away for any marketing purposes. Our system might send you occasional system updates. If you subscribe to the newsletter, you will receive regular updates. You can unsubscribe from the newsletter anytime.<br />
		                    If you choose to unregister, your profile and the related data will be deleted from our servers.
		                </li><br />
		                <li>
		                    <b>Image upload</b><br />
		                    You hereby accept that picture uploads are monitored to ensure that images are safe. There is no nudity, violent or pornographic content allowed, and we reserve our right to remove and delete any photo at any time from our servers. Copyright infringements will be also removed immediately. Please note that repeated upload of images that need to be deleted can lead to banning you profile from our site. You also accept, that the images stored on our server might be exposed through a direct URL, and we do not handle privacy setting for your files.
		                </li><br />
		                <li>
		                    <b>Advertisement</b><br />
		                    Our site might show you localized ads and other services. You private data will not be used for these ads. 
		                </li><br />
		            </ol>
				</div>
				<div class="bold">Cookie policy</div><br/>
				<div class="titit">What are cookies?</div>
				<div class="terms_text">
		            A cookie is a tiny data file, which is stored on your computer within the web browser, when certain web pages are visited. A cookie does not contain or collect information in isolation, but when read by a server via a web browser; it can give information to facilitate a more user friendly service by registering, among other data, users' preferences, detect errors and or collect data for statistics. A cookie will not harm your computer and no personally identifiable information is stored from the use of this site. For more information on how cookies are used on this site, please see How We Use cookies. The full use of our website requires the instalment of cookies. If you wish to disable any of the installed cookies, you will probably still browse the site; however certain features of the site may be deactivated. Most web browsers (e.g. Internet Explorer, Mozilla Firefox, Safari, and Google Chrome) have cookies automatically enabled. You can decide on whether and to what extent cookies will access your computer by changing the settings of the browser you use. See Managing cookies where you can find out how you can check and change your cookie settings. Please note that if you do not make any changes it will mean you accept the default browser settings and agree to the activity of cookies. If you do not agree please change your browser settings and limit or disable cookies.<br/>
		            <br/>
		            This information is outlined as part of our efforts to comply with recent legislation and to ensure that we are open, honest and clear about user privacy. Please refer to our Privacy Policy for more information. <br/>
		            <br/>
				</div>
				<div class="titit">How we use cookies</div>
				<div class="terms_text">
		            You may notice the presence of different types of cookies generated on your visit to this website. <br/>
		            We use three types of cookies:<br/>
		            <ul>
		                <li>Session cookies, which are automatically deleted after every visit.</li>
		                <li>Persistent cookies that will remain in place during multiple visits to the site.</li>
		                <li>Third party cookies, which are used by partner websites that are embedded within our site or that we link to.</li>
		            </ul>
				</div>
				<div class="titit">Session cookies</div>
				<div class="terms_text">
		            These enable you to perform essential functions on the site, such as remembering a repeat form field item within a browsing session. In addition, they help by limiting the need to transfer information across the Internet. They are not stored on your computer and will expire when you terminate your browser session. 
				</div><br/>
				<div class="titit">Persistent cookies</div>
				<div class="terms_text">
		            These enable us to recognise anonymous repeat visitors to the site. By matching an anonymous, randomly generated identifier, a record can be taken of specific browsing information such as how you arrive at the site, the pages you view, options you select, and the path taken through the site. By monitoring this information we're able to make improvements to the site, including the fixing of errors and the enhancement of content.<br/>
		            We use Google Analytics software to analyse page use, page interactions and the routes taken through our sites. These are known as 'website metrics' or 'analytics'. We do not record any personal information as part of this process.<br/>
				</div><br/>
				<div class="titit">3rd Party cookies</div>
				<div class="terms_text">
		            When you visit our site you may notice some cookies that are not related to performancenation. If you go on to a web page that contains embedded content, for example from Facebook or YouTube, you may be sent cookies from these websites. We do not control the generation of these cookies, so we suggest that you check the individual third-party websites for more information about their cookies and how to manage them.
				</div><br/>
				<div class="bold">Managing cookies</div>
				<div class="titit">To enable or disable cookies</div>
				<div class="terms_text">
		            If you do not know the type and version of the web browser you use to access the Internet, click on 'Help' at the top of your browser window, then click 'About'. The relevant information you require will then be shown.
				</div><br/>
				<div class="titit">Internet Explorer</div>
				<div class="terms_text">
		            Microsoft Internet Explorer 6.0, 7.0, 8.0<br/>
		            Click on the 'Tools' option at the top of your browser window and select 'Internet options', then click on the 'Privacy' tab. Ensure that your Privacy level is set to Medium or below, which will then enable cookies in your browser. Settings above Medium will disable cookies.
				</div><br/>
				<div class="titit">Google Chrome (Latest version)</div>
				<div class="terms_text">
		            Click on the spanner icon at the top right of your browser window and select 'Settings'. Click 'Show advanced settings' at the bottom and locate the 'Privacy' section. Click 'Content settings' and within the cookies section at the top, select 'allow local data to be set' to enables cookies. To disable cookies please check the 'Block sites from setting any data', 'Block third-party cookies and site data' and 'Clear cookies and other site and plug-in data when I close my browser' options.
				</div>	<br/>	
				<div class="titit">Mozilla Firefox</div>
				<div class="terms_text">
		            Click on 'Tools' at the top of your browser window and select Options. Next, select the Privacy icon at the top of the overlay that appears. Check the "Accept cookies from sites" option to enable cookies. If you wish to disable cookies, please uncheck this box. 
				</div><br/>
				<div class="titit">Safari</div>
				<div class="terms_text">
		            Click on the cog icon located top right of your browser window and select 'Preferences'. Click on the 'Privacy' icon at the top of the overlay that appears. Check the option that says 'Block third-party and advertising cookies'. If you wish to completely disable cookies, please check the 'Never' box.
				</div>			
			</div>

		</div>
	
</div>		
		
		
		
		